

		
  <!-- Start menu section -->
  <section id="menu-area">
    
    <nav class="navbar navbar-default main-navbar" role="navigation">  
      
	  <div class="container">
        
        <div class="navbar-header">
          
          <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!-- LOGO -->    
                                                     
           <a class="navbar-brand logo" href="index">Portal Name<!-- <img src="resources/indexResources/assets/images/logo.jpg" alt="logo"> --></a>                      
       
        </div>
        
        <div id="navbar" class="navbar-collapse collapse">
          <ul id="top-menu" class="nav navbar-nav main-nav menu-scroll">
            <li id="indexMenu1"><a href="index">Home</a></li>
            <li id="indexMenu2"><a href="aboutUs" title="About us">About Us</a></li>                  
            <li id="indexMenu3"><a href="#" title="Services we offer" onclick="showServiceDiv()">Services</a></li> <!-- href="service"  -->
            <li id="indexMenu4"><a href="searchLoan">Search </a>
            <li id="indexMenu5"><a href="contactUs" title="Contact us">Contact Us</a></li>
            <li id="indexMenu6"><a href="borrowerLogin" title="Signin & Signup">BORROWER</a></li>    
            <li id="indexMenu7"><a href="lenderLogin" title="Signin & Signup">LENDER</a></li>
          </ul> 
            
                                     
        </div>
       
	  	
      </div> 
      
    </nav> 
  </section>
  <!-- End menu section -->
	