<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<body>



	<div class="row">


		<!-- previous Design part -->


		<!-- START OF PROFILE DIV -->


		<div class="vd_content-section clearfix" style="padding: 30px;">

			<div class="row">


				<div class="col-sm-12">

					<div class="tabs widget">

						<div class="tab-content"
							style="border-radius: 4px; box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), 0 0 0 1px rgba(63, 63, 68, 0.1);">

							<div id="profile-tab" class="tab-pane active">

								<div class="pd-20">
									
									<c:forEach items="${dealInformation}" var="deal">
														
										<button style="line-height: 1.1;" class="btn btn-info btn-fill pull-right" type="button" onclick="printBorrowerRequest('${deal.deal_id}','${deal.policy_id}','${deal.borrower_id}')">Print</button>
									
									</c:forEach>
									
									<h3 class="mgbt-xs-15 mgtp-10 font-semibold">Borrower Profile</h3>
									
									<div id="borrowerProfile" style="width:100%;">
									
										<div id="photoContent" style="width:20%;float:left;">
												
												<c:set var="userId" value="${borrowerBean.borrower_id}" />
												<% int userId = (Integer)pageContext.getAttribute("userId"); %>
				
												<c:set var="bowPhoto" value="${borrowerBean.borrower_profile_photo}" />
												<% String bowPhoto = (String)pageContext.getAttribute("bowPhoto"); %>
												
												<c:set var="gender" value="${borrowerBean.borrower_gender}" />
												<% String gender = (String)pageContext.getAttribute("gender"); %>
										        
												<div class="sideMenuPhoto" style="box-shadow: 0 0 0 #888888;
																				  height: 128px;margin-bottom: 36px;
																				  margin-left: 6px;margin-top: -6px;">
							                          
													  <div class="photoStyle">				  
													  		<img src="${pageContext.request.contextPath}<%="/previewUserPhoto?fileName="+bowPhoto+"&id="+userId+"&gender="+gender+"&userType=Borrower"%>" width="172px;" height="138px">				  
													  </div>
									            </div>
									            											    										
										</div>
										
										
										<div id="detailContent" style="width:80%;float:left;">
										
												<div class="row">
			
													<div class="col-sm-6">
														<div class="row mgbt-xs-0">
															<label class="col-xs-4 control-label manageFormComponent">First Name</label>
															<div class="col-xs-7 controls">${borrowerBean.borrower_first_name}</div>
															<!-- col-sm-10 -->
														</div>
													</div>
			
													<div class="col-sm-6">
														<div class="row mgbt-xs-0">
															<label class="col-xs-4 control-label manageFormComponent">Last Name</label>
															<div class="col-xs-7 controls">${borrowerBean.borrower_last_name}</div>
															<!-- col-sm-10 -->
														</div>
													</div>
			
												</div>
			
												<div class="row">
			
													<div class="col-sm-6">
														<div class="row mgbt-xs-0">
															<label class="col-xs-4 control-label manageFormComponent">Email</label>
															<div class="col-xs-7 controls">${borrowerBean.borrower_emailid}</div>
															<!-- col-sm-10 -->
														</div>
													</div>
			
													<div class="col-sm-6">
														<div class="row mgbt-xs-0">
															<label class="col-xs-4 control-label manageFormComponent">Mobile no</label>
															<div class="col-xs-7 controls">${borrowerBean.borrower_phno}</div>
															<!-- col-sm-10 -->
														</div>
													</div>
			
												</div>
			
			
												<div class="row">
			
													<div class="col-sm-6">
														<div class="row mgbt-xs-0">
															<label class="col-xs-4 control-label manageFormComponent">Date of Join</label>
															<div class="col-xs-7 controls"><fmt:formatDate pattern="dd/MM/yyyy" value="${borrowerBean.borrower_reg_time}" />
															</div>
															<!-- col-sm-10 -->
														</div>
													</div>
			
			
													<div class="col-sm-6">
														<div class="row mgbt-xs-0">
															<label class="col-xs-4 control-label manageFormComponent">Gender
															</label>
															<div class="col-xs-7 controls">${borrowerBean.borrower_gender}</div>
															<!-- col-sm-10 -->
														</div>
													</div>
			
												</div>
	
										</div>
									
									</div>
									
									
									
									<hr class="pd-10">


									<div class="row">

										<div class="col-sm-12">
											<div class="row mgbt-xs-0">
												<label class="col-xs-2 control-label manageFormComponent">Present
													Address</label>
												<c:if test="${borrowerBean.borrower_present_door !=''}">

													<div class="col-xs-9 controls">${borrowerBean.borrower_present_door},&nbsp;${borrowerBean.borrower_present_building},&nbsp;${borrowerBean.borrower_present_street},&nbsp;${borrowerBean.borrower_present_area},&nbsp;${borrowerBean.borrower_present_city},&nbsp;${borrowerBean.borrower_present_pincode}</div>

												</c:if>
												<!-- col-sm-10 -->
											</div>
										</div>

									</div>


									<div class="row">

										<div class="col-sm-12">
											<div class="row mgbt-xs-0">
												<label class="col-xs-2 control-label manageFormComponent">Permanent
													Address </label>
												<c:if test="${borrowerBean.borrower_permanent_door !=''}">

													<div class="col-xs-9 controls">${borrowerBean.borrower_permanent_door},&nbsp;${borrowerBean.borrower_permanent_building},&nbsp;${borrowerBean.borrower_permanent_street},&nbsp;${borrowerBean.borrower_permanent_area},&nbsp;${borrowerBean.borrower_permanent_city},&nbsp;${borrowerBean.borrower_permanent_pincode}</div>

												</c:if>
												<!-- col-sm-10 -->
											</div>
										</div>
									</div>


									<div class="row">

										<div class="col-sm-6">
											<div class="row mgbt-xs-0">
												<label class="col-xs-4 control-label manageFormComponent">Alt
													Phone No</label>
												<div class="col-xs-7 controls">${borrowerBean.borrower_alt_phno}</div>
												<!-- col-sm-10 -->
											</div>
										</div>

										<div class="col-sm-6">
											<div class="row mgbt-xs-0">
												<label class="col-xs-4 control-label manageFormComponent">Date
													of Birth</label>
												<div class="col-xs-7 controls">${borrowerBean.borrower_dob}</div>
												<!-- col-sm-10 -->
											</div>
										</div>

									</div>

									<div class="row">

										<div class="col-sm-6">
											<div class="row mgbt-xs-0">
												<label class="col-xs-4 control-label manageFormComponent">Identity
													Type</label>
												<div class="col-xs-7 controls">${borrowerBean.borrower_identity_id_type}</div>
												<!-- col-sm-10 -->
											</div>
										</div>
										<div class="col-sm-6">
											<div class="row mgbt-xs-0">
												<label class="col-xs-4 control-label manageFormComponent">Identity
													Id </label>
												<div class="col-xs-7 controls">${borrowerBean.borrower_identity_card_id}</div>
												<!-- col-sm-10 -->
											</div>
										</div>

									</div>
									
									
									
									
									
									
									
									
									
									
									
									<h3 class="mgbt-xs-15 mgtp-10 font-semibold">Policy Details</h3>
									
									<c:forEach items="${dealInformation}" var="deal">
										
										<div class="row">
	
											<div class="col-sm-6">
												<div class="row mgbt-xs-0">
													<label class="col-xs-4 control-label manageFormComponent">Loan Type</label>
													<div class="col-xs-7 controls">${deal.policy_type}</div>
													<!-- col-sm-10 -->
												</div>
											</div>
											<div class="col-sm-6">
												<div class="row mgbt-xs-0">
													<label class="col-xs-4 control-label manageFormComponent">Loan Name </label>
													<div class="col-xs-7 controls">${deal.policy_name} </div>
													<!-- col-sm-10 -->
												</div>
											</div>
	
										</div>
										
										<div class="row">
	
											<div class="col-sm-6">
												<div class="row mgbt-xs-0">
													<label class="col-xs-4 control-label manageFormComponent">Request Date</label>
													<div class="col-xs-7 controls"><fmt:formatDate pattern="dd/MM/yyyy" value="${deal.deal_initiated_date}" /></div>
													<!-- col-sm-10 -->
												</div>
											</div>
											<div class="col-sm-6">
												<div class="row mgbt-xs-0">
													<label class="col-xs-4 control-label manageFormComponent">Finalize Date</label>
													<div class="col-xs-7 controls"><fmt:formatDate pattern="dd/MM/yyyy" value="${deal.deal_finalize_date}" /></div>
													<!-- col-sm-10 -->
												</div>
											</div>
	
										</div>
										
										
										<div class="row">
	
											<div class="col-sm-6">
												<div class="row mgbt-xs-0">
													<label class="col-xs-4 control-label manageFormComponent">Request Amount</label>
													<div class="col-xs-7 controls">Rs. &nbsp; ${deal.deal_amount}</div>
													<!-- col-sm-10 -->
												</div>
											</div>
											<div class="col-sm-6">
												<div class="row mgbt-xs-0">
													<label class="col-xs-4 control-label manageFormComponent">Loan Tenure </label>
													<div class="col-xs-7 controls">${deal.deal_duration} &nbsp; months</div>
													<!-- col-sm-10 -->
												</div>
											</div>
	
										</div>
										
										<div class="row">
	
											<div class="col-sm-6">
												<div class="row mgbt-xs-0">
													<label class="col-xs-4 control-label manageFormComponent">Income Type</label>
													<div class="col-xs-7 controls">${deal.income_type}</div>
													<!-- col-sm-10 -->
												</div>
											</div>
											<div class="col-sm-6">
												<div class="row mgbt-xs-0">
													<label class="col-xs-4 control-label manageFormComponent">Annual Income </label>
													<div class="col-xs-7 controls">Rs. &nbsp; ${deal.income_annualy} .00</div>
													<!-- col-sm-10 -->
												</div>
											</div>
	
										</div>
										
										<div class="row">
	
											<div class="col-sm-6">
												<div class="row mgbt-xs-0">
													<label class="col-xs-4 control-label manageFormComponent">Own House</label>
													<div class="col-xs-7 controls">${deal.house_status}</div>
													<!-- col-sm-10 -->
												</div>
											</div>
											<div class="col-sm-6">
												<div class="row mgbt-xs-0">
													<label class="col-xs-4 control-label manageFormComponent">Own 4 Wheeler </label>
													<div class="col-xs-7 controls">${deal.four_wheeler_status}</div>
													<!-- col-sm-10 -->
												</div>
											</div>
	
										</div>
										
										
										<div class="row">
	
											<div class="col-sm-6">
												<div class="row mgbt-xs-0">
													<label class="col-xs-4 control-label manageFormComponent">Own 2 Wheeler</label>
													<div class="col-xs-7 controls">${deal.two_wheeler_status}</div>
													<!-- col-sm-10 -->
												</div>
											</div>
											
											<div class="col-sm-6">
												<div class="row mgbt-xs-0">
													<label class="col-xs-4 control-label manageFormComponent">Amount Dispouse</label>
													<div class="col-xs-7 controls">${deal.amount_dispouse}</div>
													<!-- col-sm-10 -->
												</div>
											</div>
											
	
										</div>
										
										
										<div class="row">
	
											<div class="col-sm-12">
												<div class="row mgbt-xs-0">
													<label class="col-xs-2 control-label manageFormComponent">Borrower Message</label>
													<div class="col-xs-7 controls">${deal.note_to_lender}</div>
													<!-- col-sm-10 -->
												</div>
											</div>
											
	
										</div>
										
										
									
									</c:forEach>
									
									
									
									
									
									
									
									
									
									
									<!-- Attached Documents -->
			
									<%String policyNeedDoc = (String)request.getAttribute("policyNeedDoc"); %>
									
										<h3 class="mgbt-xs-15 mgtp-10 font-semibold">Attached documents</h3>
										
										
										<div class="content all-icons">
				
											<div class="row">
				
				
												<c:if test="${!empty attachedDocument}">
				
				
													<c:forEach items="${attachedDocument}" var="det">
														
														<c:set var="docType" value="${det.document_type}" />
														<% String docType = (String)pageContext.getAttribute("docType"); %>
														
														<c:set var="docName" value="${det.file_name}" />
														<% String docName = (String)pageContext.getAttribute("docName"); %>
				
														<c:set var="borrowerId" value="${det.borrower_id}" />
														<% int borrowerId = (Integer)pageContext.getAttribute("borrowerId"); %>
				
														
														<%if(policyNeedDoc.contains(docType)) {%>
														
															<div class="font-icon-list col-lg-2 col-md-3 col-sm-4 col-xs-6 col-xs-6">
					
																<div class="font-icon-detail">
					
																	<input value="${det.document_type}" disabled type="text">
					
																	<img src="${pageContext.request.contextPath}<%="/viewAttachedDocByOther?fileName="+docName+"&userId="+borrowerId+"&userType=Borrower"%>" height="120px" width="142px;" /> 
																    
																    <input class="btn btn-info btn-fill pull-right" value="Download" onclick="downloadDocuments('<%=docName%>','<%=borrowerId%>','Borrower')" /> 
					
																</div>
					
															</div>
														
														<%}%>
														
													</c:forEach>
				
												</c:if>
				
												<c:if test="${empty attachedDocument}">
				
													<div>
														<p style="margin-left: 16px;">As of now you didn't
															attached any documents.</p>
													</div>
				
												</c:if>
											</div>
				
										</div>
				
				
				
							<!-- End of Attached Documents -->
									
									
									
									<div class="row">
									
										<div class="col-sm-12">
											
											<c:forEach items="${dealInformation}" var="deal">
												
												<c:if test="${deal.amount_dispouse =='No'}">
																						 
												     <button class="btn btn-info btn-fill pull-right"  type="button" onclick="releaseAmountBorrowerRequest('${deal.deal_id}')">Amount Dispouse</button>
												
												</c:if>
												
											</c:forEach>
													
										</div>
										
									</div>
								
									
									
									
									
									
									
									
								</div>
								<!-- pd-20 -->
								
								
							</div>
							<!-- home-tab -->



							<!-- photos tab -->
							<!-- photos tab -->
							<!-- groups tab -->

						</div>
						<!-- tab-content -->
					</div>
					<!-- tabs-widget -->
				</div>




			</div>
			<!-- row -->



			

		</div>



		<!-- END OF PROFILE DIV -->

	</div>


</body>
</html>