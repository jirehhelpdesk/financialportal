<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Search Loan</title>

    <link rel="shortcut icon" type="image/icon" href="resources/indexResources/assets/images/favicon.ico"/>
    <!-- Font Awesome -->
    <link href="resources/indexResources/assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="resources/indexResources/assets/css/bootstrap.css" rel="stylesheet">
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="resources/indexResources/assets/css/slick.css"/> 
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="resources/indexResources/assets/css/jquery.fancybox.css" type="text/css" media="screen" /> 
    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="resources/indexResources/assets/css/animate.css"/>  
     <!-- Theme color -->
    <link id="switcher" href="resources/indexResources/assets/css/theme-color/default.css" rel="stylesheet">

    <!-- Main Style -->
    <link href="resources/indexResources/style.css" rel="stylesheet">

    <!-- Fonts -->
    <!-- Open Sans for body font -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Raleway for Title -->
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <!-- Pacifico for 404 page   -->
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    
</head>



<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<body onload="activeIndexMenu('Menu4')">

     <%@include file="index_header.jsp" %>


 
  <!-- Start about section -->
 
  
  <% String nameList = (String)request.getAttribute("nameList"); 
     
        String policyListArray[] = nameList.split("/");
     %>
     
     
  
  <section id="about" style="padding: 0px 0 ! important;">
    
    <div class="container">
     
      <div class="row">
        <div class="col-md-12">
          <div class="about-area">
            <div class="row">
              
              <!-- <div class="title-area">
	              <h2 class="tittle">Search Loan</h2>              
	              <p></p>
	           </div> -->
            
            	<div class="col-md-1 col-sm-5 col-xs-12">
            	
            	</div>
            
              <div class="col-md-5 col-sm-5 col-xs-12">
                <div class="about-left wow fadeInLeft" style="height: 296px ! important;">
                  <img src="resources/indexResources/assets/images/searchLoan.jpg" alt="img">
                  <!-- <a class="introduction-btn" href="#">Introduction</a> -->
                </div>
              </div>
              
              <div class="col-md-5 col-sm-5 col-xs-12">
                
              	  <form:form  class="contact-form" id="searchLoanForm" modelAttribute="searchLoanModel">
              
		              <div class="form-group">                
		                <select class="form-control" id="loan_type" name="loan_type" path="loan_type" style="height:33px ! important;font-size:15px;">
		                			<option value="Select loan type">Select loan type</option>
		                			<%for(int i=0;i<policyListArray.length;i++) {%>
	                                                		
                                        <option value="<%=policyListArray[i]%>"><%=policyListArray[i]%></option>
                                        		
                                    <%} %>
		                </select>
		                <div id="loanTypeErrId" class="errorStyle"></div>
		              </div>  
		              
		              <div class="form-group">                
		                <select class="form-control" id="loan_tenure" name="loan_tenure" path="loan_tenure" style="height:33px ! important;font-size:15px;">
		                			<option value="Select tenure">Select tenure</option>
		                			<option value="6">6 Months</option>
                               		<option value="12">12 Months</option>
                               		<option value="18">18 Months</option>
                               		<option value="24">24 Months</option>    
                               		<option value="24+">more then 24 Months</option>  
		                </select>
		                <div id="tenureErrId" class="errorStyle"></div>
		              </div>  
		              
		              <div class="form-group">                
		                <input type="text" class="form-control" id="loan_amount" placeholder="loan amount in rupees" name="loan_amount" path="loan_amount" onkeyup="amountFormat('loan_amount')">
		              	<div id="amountErrId" class="errorStyle"></div>
		              </div>  
		              
		              
		              <!-- <div class="form-group">                
		                <input type="text" class="form-control" id="full_name" placeholder="Full Name" name="full_name" path="full_name" >
		              	<div id="nameErrId" class="errorStyle"></div>
		              </div> 
		              
		              <div class="form-group">                
		                <input type="email" class="form-control" id="email_id" placeholder="Email Id" name="email_id" path="email_id" >
		              	<div id="emailErrId" class="errorStyle"></div>
		              </div>  
		              
		              <div class="form-group">                
		                <input type="text" class="form-control" id="phone_number" placeholder="phone number" name="phone_number" path="phone_number" >
		              	<div id="phoneErrId" class="errorStyle"></div>
		              </div>   -->
		              
		              
		            
		            </form:form>
		            
		             <div class="form-group">
		            	<button class="btn contactBtn" onclick="searchLoanForGuest()"><span>Search</span></button>
		              </div>
              
              
              </div>
              
              
              
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </section> 
  
  
  <!-- End about section -->


   <div id="resultDivId"> </div>



   <%@include file="index_fotter.jsp" %>



  <!-- initialize jQuery Library --> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <!-- Bootstrap -->
  <script src="resources/indexResources/assets/js/bootstrap.js"></script>
  <!-- Slick Slider -->
  <script type="text/javascript" src="resources/indexResources/assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="resources/indexResources/assets/js/waypoints.js"></script>
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.counterup.js"></script>
  <!-- mixit slider -->
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->        
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.fancybox.pack.js"></script>
  <!-- Wow animation -->
  <script type="text/javascript" src="resources/indexResources/assets/js/wow.js"></script> 

  <!-- Custom js -->
  <script type="text/javascript" src="resources/indexResources/assets/js/custom.js"></script>
   
  <script type="text/javascript" src="resources/indexResources/index_registration_script.js"></script>
    
  </body>
</html>