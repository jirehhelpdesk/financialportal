<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Why Borrower</title>

    <link rel="shortcut icon" type="image/icon" href="resources/indexResources/assets/images/favicon.ico"/>
    <!-- Font Awesome -->
    <link href="resources/indexResources/assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="resources/indexResources/assets/css/bootstrap.css" rel="stylesheet">
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="resources/indexResources/assets/css/slick.css"/> 
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="resources/indexResources/assets/css/jquery.fancybox.css" type="text/css" media="screen" /> 
    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="resources/indexResources/assets/css/animate.css"/>  
     <!-- Theme color -->
    <link id="switcher" href="resources/indexResources/assets/css/theme-color/default.css" rel="stylesheet">

    <!-- Main Style -->
    <link href="resources/indexResources/style.css" rel="stylesheet">

    <!-- Fonts -->
    <!-- Open Sans for body font -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Raleway for Title -->
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <!-- Pacifico for 404 page   -->
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    
    
</head>



<body onload="showTheDiv('<%=request.getAttribute("div")%>')">

  
  <!-- Start menu section -->
   <%@include file="index_header.jsp" %>
  <!-- End menu section -->

  
  <!-- Start service section -->
  <section id="service">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="service-area">
            <div class="title-area">
              <h2 class="tittle">Why Borrower</h2>          
              <p></p>
            </div>
            
            
            
            <!-- service content -->
            
            <div class="service-content">
              				
              				
              				<div id="whyBorrower">
              				
		                            <h5>1.Why Borrower ?</h5>
		              
					              
									<p>
									    Any one can be a lender it might be a individual person or 
									</p>
					
										
										<p>							
								            These terms and conditions shall govern your use of our
											website.By using our website, you accept these terms and
											conditions in full; accordingly, if you disagree with these terms
											and conditions or any part of these terms and conditions, you
											must not use our website.If you [register with our website,
											submit any material to our website or use any of our website
											services], we will ask you to expressly agree to these terms and
											conditions. You must be at least [18] years of age to use our
											website; by using our website or agreeing to these terms and
											conditions, you warrant and represent to us that you are at least
											[18] years of age. Our website uses cookies; by using our
											website or agreeing to these terms and conditions, you consent to
											our use of cookies in accordance with the terms of our [privacy
											and cookies policy].										
										</p>
								</div>
								
								
								<div id="bowEligibility">
									<h5>2.Borrowers Eligibility</h5>
									
									<ul>
		              
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              
					               </ul>
								</div>
								
								<div id="purposeLoan">
								
								    <h5>3.Purpose for Loan</h5>
									
									<ul>
		              
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              
					               </ul>
					               
				               </div>
				               
				               <div id="BowFaq">
				               
					                <h5>4.Borrower FAQ</h5>
									
									<ul>
		              
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              
					               </ul>
								</div>
								
								<div id="bowTutorial">
								
								    <h5>5.Tutorial for Borrower</h5>
								
									<ul >
		              
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              <li>1.1	These terms and conditions shall govern your use of our website.</li>
						              
					               </ul>
								
								</div>

					</div>
            
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End service section -->





  <!-- Start Footer -->    
   <%@include file="index_fotter.jsp" %>
  <!-- End Footer -->




  <!-- initialize jQuery Library --> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <!-- Bootstrap -->
  <script src="resources/indexResources/assets/js/bootstrap.js"></script>
  <!-- Slick Slider -->
  <script type="text/javascript" src="resources/indexResources/assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="resources/indexResources/assets/js/waypoints.js"></script>
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.counterup.js"></script>
  <!-- mixit slider -->
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->        
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.fancybox.pack.js"></script>
  <!-- Wow animation -->
  <script type="text/javascript" src="resources/indexResources/assets/js/wow.js"></script> 

  <!-- Custom js -->
  <script type="text/javascript" src="resources/indexResources/assets/js/custom.js"></script>
    
  </body>
</html>