<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manage Loan Type</title>

 <!-- META SECTION -->
           
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="resources/adminResources/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->     
        
</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>



<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>



<body onload="activeSideMenu('menu5',1)">


 <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           
           
            <!-- START PAGE SIDEBAR -->
              
              <%@include file="admin_side_bar.jsp" %>
            
            <!-- END PAGE SIDEBAR -->
            
            
            
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
               
               
               
                
                <!-- START X-NAVIGATION VERTICAL -->
                    
                    <%@include file="admin_header_bar.jsp" %>
                
                <!-- END X-NAVIGATION VERTICAL -->                     




                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>                    
                    <li >Finance Policy</li>
                    <li class="active">Manage Loan Type</li>
                </ul>
                <!-- END BREADCRUMB -->                       
                
                
                
                 <div class="page-title">                    
                    <h2><span class="fa fa-list-alt"></span> Manage Loan Type <small></small></h2>
                </div>
                
                
                <!-- PAGE CONTENT WRAPPER -->
                
                
                <div class="page-content-wrap">
                    
                    
                   	<div class="row">
                        
                       <div class="col-md-12">
                            
                          <form:form class="form-horizontal" modelattribute="financialPolicy" id="createFinPolicyForm">
                           
                            <div class="panel panel-default">
                                
                                <div class="panel-heading ui-draggable-handle">
                                    <h3 class="panel-title"><strong>Finance Type Form</strong></h3>   
                                </div>
                                
                                <div class="panel-body">
                                    <p>Please provide the appropriate details to manage the portal. </p>
                                </div>
                                
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Finance Type</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <select id="typeId" name="policy_type_name" path="policy_type_name"  class="form-control">
                                            			<option value="Select finance type">Select finance type</option>
                                            			<option value="Loan">Loan</option>
                                            			<option value="Insurance">Insurance</option>
                                            			
                                            	</select>
                                            </div>                                            
                                            <span class="help-block" id="errorTypeId"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Finance Name</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" id="nameId" name="sub_policy_type" path="sub_policy_type" placeholder="Sub Policy Type" class="form-control">
                                            </div>                                            
                                            <span class="help-block" id="errorNameId"></span>
                                        </div>
                                    </div>
                                    
                                     
                                    <div class="form-group">
                                        
                                        <label class="col-md-3 col-xs-12 control-label">Wants to Add Component</label>
										<div class="col-md-1" style="margin-top:5px;" onclick="addComponent('Yes')">
											<input type="radio" id="yesId" onclick="addComponent('Yes')"> Yes
										</div>
										<div class="col-md-1" style="margin-top:5px;" onclick="addComponent('No')">
											<input type="radio" id="noId"  value="No" onclick="addComponent('No')"> No
										</div>

									</div>
									
									<div id="extraComp" style="display:none;border:1px solid #cecece; padding: 10px;border-radius:5px;">
										
											<div class="form-group">
		                                        <label class="col-md-3 col-xs-12 control-label">Component Type</label>
		                                        <div class="col-md-6 col-xs-12">                                            
		                                            <div class="input-group">
		                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
				                                         <select id="compTypeId"   class="form-control" onchange="showOptionBox(this.value)">
		                                            			<option value="Select component type">Select component type</option>
		                                            			<option value="TB">Text Box</option>
		                                            			<option value="DD">Drop Down</option>
		                                            			
		                                            	</select>
		                                            </div>                                            
		                                            <span class="help-block" id="errorcompTypeId"></span>
		                                        </div>
	                                    	</div>
	                                    	
	                                    	<div class="form-group" id="reqdComp1" style="display:none;">
		                                        <label class="col-md-3 col-xs-12 control-label">Component Name</label>
		                                        <div class="col-md-6 col-xs-12">                                            
		                                            <div class="input-group">
		                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
				                                        <input type="text" id="compName1" placeholder="Enter Component Name" class="form-control">
		                                            </div>                                            
		                                            <span class="help-block" id="errorReqdComp1Id"></span>
		                                        </div>
	                                    	</div>
	                                    	
	                                    	<div class="form-group" id="reqdComp2" style="display:none;">
		                                        <label class="col-md-3 col-xs-12 control-label">Component Variable</label>
		                                        <div class="col-md-4 col-xs-12">                                            
		                                            <div class="input-group">
		                                                <span class="input-group-addon"><span class="fa fa-plus"></span></span>
				                                        <input type="text" id="compOption" name="sub_policy_type" path="sub_policy_type" placeholder="Sub Policy Type." class="form-control">
		                                            </div>                                            
		                                            <span class="help-block" id="errorReqdComp2Id"></span>
		                                        </div>
		                                        <div class="col-md-3 col-xs-12">                                            
		                                            <div class="input-group">
		                                                <span class="input-group-addon"><span class="fa fa-plus"></span></span>
				                                        <button class="btn btn-primary active" type="button" onclick="addOptionValue()">Add Option Value</button>
		                                            </div>                                            
		                                            <span class="help-block" id="errorReqdComp2Id"></span>
		                                        </div>
		                                        <div>
		                                        	  
		                                        </div>
	                                    	</div>
	                                    	
	                                    	<div class="form-group" id="reqdComp3">
		                                        <label class="col-md-3 col-xs-12 control-label"></label>
		                                        <div class="col-md-5 col-xs-12">                                            
		                                            
		                                        </div>
		                                        
		                                        <div class="col-md-3 col-xs-12">                                            
		                                            <div class="input-group">
		                                                
				                                        <button class="btn btn-info" type="button" onclick="addGivenComponent()">Add Component</button>
		                                            </div>                                            
		                                            <span class="help-block" id="errorReqdComp2Id"></span>
		                                        </div>
		                                        <div>
		                                        	  
		                                        </div>
	                                    	</div>
	                                    	
	                                    	
	                                    	<div class="form-group" >
	                                    		
	                                    		<input type="hidden" id="addOptionId" name="addOption" >
	                                    		
	                                    		<input type="hidden" id="addCompId" name="addedComp" >
	                                    		
	                                    	</div>
									
									</div>
									

									<div class="form-group" id="addedCompId" style="display:none;margin-top:10px;">
		                                       
	                                        <label class="col-md-3 col-xs-12 control-label">Added Component</label>
	                                        
	                                        <div class="col-md-6 col-xs-12">                                            
	                                            <div class="input-group">
	                                                <span class="input-group-addon"><span class="fa fa-plus"></span></span>
			                                        
			                                        <div id="addCompDisplayId" style="margin-left:10px;">
				                                      
	                                            	</div>
	                                            	
	                                            </div>                                            	                                           
	                                        </div>
	                                       
	                               </div>
									
									
									
									
                                </div>
                                       
                                <div class="panel-footer">
                                    
                                    <input class="btn btn-primary pull-right" type="button" value="Save Details" onclick="savePolicyTypeDetails()">                               
                                    
                                </div>
                               
                            </div>
                            </form:form>
                            
                        </div>
                    </div>
                    
                   
                       
                       
                   <div class="row">
                        <div class="col-md-12">
                            
                            <div class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-heading ui-draggable-handle">
                                    <h3 class="panel-title"><strong>Search Form</strong></h3>
                                    
                                </div>
                                <div class="panel-body">
                                    <p>Please enter all the respective option to get the result.</p>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Search Type</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-search"></span></span>
                                                <select id="searchTypeId" name="policy_type_name" path="policy_type_name"  class="form-control">
                                            			<option value="Select finance type">Select finance type</option>
                                            			<option value="Loan">Loan</option>
                                            			<option value="Insurance">Insurance</option>
                                            			
                                            	</select>
                                            </div>                                            
                                            <span class="help-block" id="errorNameId"></span>
                                        </div>
                                    </div>
                                    
                                    <!-- 
		                                    <div class="form-group">
		                                        <label class="col-md-3 col-xs-12 control-label">Search Value</label>
		                                        <div class="col-md-6 col-xs-12">                                            
		                                            <div class="input-group">
		                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
		                                                <input type="text" placeholder="Who are you looking for?" id="searchValueId" class="form-control">
		                                            </div>                                            
		                                            <span class="help-block" id="errorValueId"></span>
		                                        </div>
		                                    </div>
                                     -->

                                </div>
                                <div class="panel-footer">                                                               
                                    <button class="btn btn-primary pull-right" onclick="searchPolicyTypeDetails()">Search<span class="fa fa-search fa-right"></span></button>
                                </div>
                            </div>
                            </div>
                            
                        </div>
                    </div>
                    
                   
                   <div class="row" id="hiddenDiv" style="display:none;">
                        <div class="col-md-12">
                            <div class="panel panel-default">

                                <div class="panel-heading ui-draggable-handle">
                                    <h3 class="panel-title">Search Result of Finance Type</h3>
                                </div>

                                <div class="panel-body panel-body-table" id="finTypeSearchResultId">

                                    
                                    
                                    
                                </div>
                            </div>                                                

                        </div>
                    </div>
                   
                   
                   
                   
                   
                   
                   
                   
			        <!-- FOOTER CONTENT -->
       
			         <%@include file="admin_footer.jsp" %>
			       
			       <!-- END FOOTER CONTENT -->
                    
                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            
            
            </div>            
            <!-- END PAGE CONTENT -->
       
        </div>
        <!-- END PAGE CONTAINER -->

       
       
       
       
       
       
       <!-- BELOW COMMON CONTENT -->
       
         <%@include file="admin_down_common.jsp" %>
       
       <!-- END BELOW COMMON CONTENT -->
       
	
    </body>
</html>