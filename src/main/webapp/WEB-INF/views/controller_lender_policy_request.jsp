<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" href="resources/paging/other.css">

</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>


<body>

<c:if test="${!empty dealDetails}">	


<div class="table-responsive">
                                        
                                        
                                        
                                        <table id="tableData" class="table table-bordered table-striped table-actions">
                                            
                                           
                                            <thead>
                                                <tr>
                                                    <th width="50">Sl No</th>
                                                    <th  width="100">Name</th>
                                                    <th width="100">Email Id</th>
                                                    <th width="100">Ph No</th>
                                                    <th width="100">Policy Type</th>
                                                    <th width="100">Policy Name</th>
                                                    <th width="100">Finance Amount</th>
                                                    <th width="100">Finance Duration</th>
                                                    <th width="100">Status</th>
                                                    <th width="100">Manage</th>
                                                </tr>
                                            </thead>
                                            
                                            
                                            <tbody>                                            
                                               
                                               <%int i=1; %> 
		               							<c:forEach items="${dealDetails}" var="det">	    
				                                           
	                                                <tr id="trow_1">
	                                                    <td><%=i++%></td>
	                                                    <td><strong>${det.lender_name}</strong></td>
	                                                    <td>${det.lender_emailid}</td>
	                                                    <td>${det.lender_phno}</td>
	                                                    <td>${det.policy_type}</td>
	                                                    <td>${det.policy_name}</td>
	                                                    <td>Rs. ${det.deal_amount}</td>
	                                                    <td>${det.deal_duration} months</td>
	                                                    <td><span class="label label-success" style=" font-size: 14px;">${det.deal_status}</span></td>
	                                                    
	                                                    <td>
	                                                        <button class="btn btn-info" type="button" style=" border-radius: 20px;" onclick="manageBorrowerRequest('${det.deal_id}')">View Details</button>
	                                                    </td>
	                                                    
	                                                </tr>
	                                                
                                                </c:forEach>
                                                
                                            </tbody>
                                        </table>
                                        
                                        
                                    </div>      
                                    
                                    
                                    <!-- Paging zone -->
          							
          							
									<script type="text/javascript" src="resources/paging/jquery.min.js"></script> 
									
									<script src="resources/paging/jquery-ui.min.js"></script>
									
									<script type="text/javascript" src="resources/paging/paging.js"></script> 
									
									<script type="text/javascript">
									            $(document).ready(function() {
									                $('#tableData').paging({limit:10});
									            });
									        </script>
									        <script type="text/javascript">
									
									  var _gaq = _gaq || [];
									  _gaq.push(['_setAccount', 'UA-36251023-1']);
									  _gaq.push(['_setDomainName', 'jqueryscript.net']);
									  _gaq.push(['_trackPageview']);
									
									  (function() {
									    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
									    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
									    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
									  })();
									
									</script>
									
				                  
				                   <!-- End of  Paging zone -->
    </c:if>
    
    
    <c:if test="${empty dealDetails}">	
    
    
			   <div class="col-md-12" style="margin-top:20px;">

                                                     
                            <div class="messages messages-img" >
                                
                                <div class="item item-visible">
                                                                    
                                    <div class="text customMsgText">
                                        <div class="heading">
                                            <a href="#"></a>
                                            <span class="date"></span>
                                        </div>                                    
                                        As of now there is no policy deal found in the portal.
                                    </div>
                                </div>
                            </div>

                        </div>
    
    </c:if>                                           
</body>
</html>