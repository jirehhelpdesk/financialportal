<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<body>

   
                        <div class="row">
                        		
                        		<div class="col-md-12">
			                        <div class="card">
			                            
			                            
			                            <div class="content table-responsive table-full-width" >
			                                
			                                <div class="table table-hover table-striped" style="padding:10px;">
			                                   
													<c:if test="${!empty notification}">	
					                                        
					                                        <%int n=1; %>
					                                        
					                                        <c:forEach items="${notification}" var="det">	
					                                        
						                                        <div class="alert alert-info alert-with-icon" data-notify="container">
								                                   
								                                    <button id="buttonId_<%=n%>"  class="close" onclick="showBorrowerNotification('${det.notify_id}','<%=n%>');">See</button>
								                                    
								                                    <span data-notify="icon" class="pe-7s-mail"></span>
								                                    
								                                    <span data-notify="message"><b style="float: left;"> &nbsp;&nbsp; ${det.notify_subject} &nbsp; on &nbsp;</b>  <fmt:formatDate pattern="dd/MM/yyyy" value="${det.notify_date}" /> </span>
								                                
								                                </div>
								                                
								                                <div id="loanDetail_<%=n%>" class="loanStatusDiv">
								                                		
								                                </div>
							                                
							                                    <% n = n + 1; %>
							                                    
							                                </c:forEach>
							                                
							                         </c:if>
							                         
							                         <c:if test="${empty notification}">	       
							                         
							                         		
							                                
							                         </c:if>  
							                             
			                                </div>
			
			                            </div>
			                        </div>
			                    </div>
                        		
                        </div>
                        
                        
                        

</body>
</html>