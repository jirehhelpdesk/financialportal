<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>My Profile</title>

    <link href="resources/borrower_lender_resources/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="resources/borrower_lender_resources/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="resources/borrower_lender_resources/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="resources/borrower_lender_resources/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    
    <link rel="stylesheet" type="text/css" href="resources/datePicker/jquery.datetimepicker.css"/>
    
</head>


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body onload="activeSideMenu('menu2')">


<div class="wrapper">
    
    
      <!-- Start of Sidemenu Division -->
    
         <%@include file="borrower_sidemenu.jsp" %>

      <!-- End of Sidemenu Division -->


    <div class="main-panel">
        
        
         <!-- Start of Header Division -->
         
         <%@include file="borrower_header.jsp" %>
        
		 <!-- End of Header Division -->

       
       
        <div class="content">
          
        
        		<div class="container-fluid">
                
                
                <div class="row">
                
                   <div class="col-md-8" style="width:100%;">
                
                		
                		<div class="card">
                            <div class="header">
                                <h4 class="title">Change Password</h4>
                            </div>
                            <div class="content">
                                <form id="passwordForm">
                                    
                                    <div class="row">
                                    	
                                    	<div class="col-md-4">
                                            <div class="form-group">
                                                <label>Current Password</label>
                                                <input type="password" autocomplete="off" class="form-control" placeholder="Current Password" name="curPassword" id="curPasswordId" >
                                                <div id="curPasswordErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>New Password</label>
                                                <input type="password" autocomplete="off" class="form-control" placeholder="New Password" name="password" id="passwordId" >
                                                <div id="passwordErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Confirm Password</label>
                                                <input type="password" autocomplete="off" class="form-control" placeholder="Confirm Password" name="cnfPassword"  id="cnfPasswordId">
                                                <div id="cnfPasswordErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                    </div>

                                </form>
                                
                                
                                    <button type="button" class="btn btn-info btn-fill pull-right" style="margin-top:21px;]" onclick="changePassword()">Update</button>
                                    <div class="clearfix"></div>
                                    
                            </div>
                        </div>
                        
                        
                        </div>
                </div>
                
                
                
                
                
                
            </div>
        		
        		
        </div>

       <!-- Start of Footer Division -->
 
         <%@include file="borrower_footer.jsp" %>
        
	   <!-- Start of Footer Division -->
	   
	   
    </div>
</div>


</body>
      
       <!-- Start of Common Division -->
 
         <%@include file="common_content.jsp" %>
        
	   <!-- Start of Common Division -->   
   
	
</html>
