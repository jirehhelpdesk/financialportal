<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<body>


<section id="pricing-table" style="padding: 15px 0;">
    
    <div class="container">
      
      <div class="row">
        
        <div class="col-md-2"> </div>
         
         <div class="col-md-8">
          
          <div class="pricing-table-area">
            
            <div class="title-area">
              <h2 class="tittle">Search Result</h2>
              <span class="tittle-line"></span>
              <p>As per your required information we got <%=request.getAttribute("resultSize")%> lenders for lending money.</p>
            </div>
            
            <!-- service content -->
            
              <c:if test="${!empty policyResult}">	
            
		            <div class="pricing-table-content" style="font-size:12px;margin-top: 15px;">
		                
		                <ul class="price-table">
		                 
			                 <c:forEach items="${policyResult}" var="det">	
			                  
				                  <li class="wow slideInUp" style="width:100%;">
				                    
					                    <div class="single-price">                      
					                      
						                      <p style="padding: 12px 18px 6px;font-size:14px;text-align:left;color:#00b6f5;">Amount <t>Rs.${det.policy_min_amount}</t> to <t>${det.policy_max_amount}</t> with minimum loan period of  <t>${det.policy_min_duration}</t> to <t>${det.policy_max_duration}</t> months with interest  <t>${det.policy_interest_rate} %</t>
		                                           
		                                           <a data-text="SIGN IN" style="margin-top:0px;float:right;" href="borrowerLogin">View Details</a>
						                      
						                      </p>
					                    
					                    </div>
				                    
				                  </li>
				                  
			                 </c:forEach>
			                
		                </ul>  
		                     
		            </div>
		            
		      </c:if>
		                 
              <c:if test="${empty policyResult}">	
               	
               				
	            	<div style="width:100%;"  class="col-md-8">
					    
					    <div class="card">
							              
							<div class="content table-responsive table-full-width">
							     <p style="text-align: center;color:#00b6f5;font-size:14px;">No loan was found as per the search criteria.We will update you if something will comes.</p>
							</div>         
							                
						</div>
																						
				   </div>
				   
			   	   	               
               </c:if>
		                 
		             
          </div>
          
        </div>
        
        
        <div class="col-md-2"> </div>
        
        
      </div>
      
    </div>
    
  </section>
  
  <!-- End Pricing Table section -->
  
</body>
</html>