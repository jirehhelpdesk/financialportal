<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>


<style>
.gapLength
{
margin-bottom: -22px;
}

</style>

</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>



<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<body>

        <div class="content">
           
                <div class="container-fluid">
                
                
                <div class="row">
                
                   <div style="width:100%;" class="col-md-8">
                		
                		
                		<c:forEach items="${policyInfo}" var="det">	
                             
                             
                		 <div class="header">
                             <h4 class="formHeading title"><t>Application form for  ${det.policy_name}::</t></h4>
                         </div>
                		
                		<div class="card" style="margin-bottom: 5px !important;">
                            
                            
                            <div class="content">
                                
                            
                               
                                <form id="financeApplicationForm" modelattribute="dealModel">
                                   
                                   	
                                   
                                    <div class="row">
                                    	
                                   		<div class="header">
			                                <h4 class="formSubHeading title">Basic Details of <t>${borrowerBean.borrower_first_name} ${borrowerBean.borrower_last_name}</t></h4>
			                            </div>
                            
                                        <div class="col-md-3">
                                            <div class="form-group gapLength">
                                                <label>Registered Email id</label>
                                                
                                                <input type="text" value="${borrowerBean.borrower_emailid}" disabled="" placeholder="Registered Email id" class="form-control">
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <div class="form-group gapLength">
                                                <label>Alternate Email id</label>
                                                <input type="text" placeholder="Alternate Email id" name="altEmailId" id="altEmailId" class="form-control">
                                                <div id="altEmailErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <div class="form-group gapLength">
                                                <label for="exampleInputEmail1">Registered mobile no</label>
                                                <input type="text" placeholder="Email" readonly value="${borrowerBean.borrower_phno}" name="regMobileNo" id="regMobileNo" class="form-control">
                                                <div id="regMobileErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <div class="form-group gapLength">
                                                <label for="exampleInputEmail1">Alternate mobile no</label>
                                                <input type="text" placeholder="Alternate mobile no" name="altMobileNo" maxlength="10" id="altMobileNo" class="form-control">
                                                <div id="altMobileErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                    </div>
									
									
									<div class="row">
                                        
                                        <div class="header">
			                                <h4 class="formSubHeading title">Identity Details</h4>
			                            </div>
			                            
                                        <div class="col-md-3">
                                            <div class="form-group gapLength">
                                                <label>Date of Birth</label>
                                                
                                                <input type="text" class="form-control" name="dobId" id="dobId" readonly  value="${borrowerBean.borrower_dob}" />
                                               	<div id="dobErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <div class="form-group gapLength">
                                                <label>Pan Number</label>
                                                
                                                <input type="text" class="form-control" name="panId" id="panId" readonly  value="${borrowerBean.borrower_pan_id}" />
                                               	<div id="panErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                        
                                        <c:set var="idType" value="${borrowerBean.borrower_identity_id_type}"/>
                                    	<% String idType = (String)pageContext.getAttribute("idType"); %>
									
										<c:set var="idNo" value="${borrowerBean.borrower_identity_card_id}"/>
                                    	<% String idNo = (String)pageContext.getAttribute("idNo"); %>
									
										<%String itType[] = {"Voter Id","Aadhar Card","Employee Card"}; %>
										
										
                                        <div class="col-md-3">
                                            <div class="form-group gapLength">
                                                <label>Secondary Id Type</label>
	                                                <select class="form-control" id="idTypeId" name="idTypeId" >
	                                                		<option value="Select Type of Id">Select Type of Id</option>                                                		
	                                                		<%for(int i=0;i<itType.length;i++){%>
	                                                				
	                                                			 <%if(idType.equals(itType[i])) {%>
	                                                			 		<option selected value="<%=itType[i]%>"><%=itType[i]%></option>
	                                                			 <%}else{%>
	                                                			 		<option value="<%=itType[i]%>"><%=itType[i]%></option>
	                                                			 <%} %>	
	                                                			
	                                                		<%}%>
	                                                </select>
                                                <div id="idTypeErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                       
                                        <div class="col-md-3">
                                            <div class="form-group gapLength">
                                                <label for="exampleInputEmail1">Secondary Identity Id</label>
                                                <%if(idNo.equals("Not Updated")) {%>
                                     			 		<input type="text" id="idNoId" name="idNoId" class="form-control"  placeholder="Enter Id no" />
                                     			 <%}else{%>
                                     			 		<input type="text" id="idNoId" name="idNoId" class="form-control"  placeholder="Enter Id no" value="${borrowerBean.borrower_identity_card_id}" />
                                     			 <%} %>	
                                                
                                            	<div id="idErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                    </div>
									
									
									<div class="row">
									    
									    <div class="header">
			                                <h4 class="formSubHeading title">Present Address</h4>
			                            </div>
									    
	                                     <div class="">
	                                    	
	                                        <div class="col-md-4">
	                                            <div class="form-group">
	                                                <label>Door No</label>
	                                                <input type="text" id="preDoorId" class="form-control" value="${borrowerBean.borrower_present_door}" name="borrower_present_door" path="borrower_present_door" placeholder="Door number" >
	                                            	<div id="preDoorErrId" class="errorStyle"></div>
	                                            </div>
	                                        </div>
	                                        
	                                        <div class="col-md-4">
	                                            <div class="form-group">
	                                                <label>Building Name</label>
	                                                <input type="text" id="preBuildingId" class="form-control" value="${borrowerBean.borrower_present_building}" name="borrower_present_building" path="borrower_present_building" placeholder="Building name" >
	                                            	<div id="preBuildingErrId" class="errorStyle"></div>
	                                            </div>
	                                        </div>
	                                        
	                                         <div class="col-md-4">
	                                            <div class="form-group">
	                                                <label>Street Name</label>
	                                                <input type="text" id="preStreetId" class="form-control" value="${borrowerBean.borrower_present_street}" name="borrower_present_street" path="borrower_present_street" placeholder="Street name" >
	                                            	<div id="preStreetErrId" class="errorStyle"></div>
	                                            </div>
	                                         </div>
	                                       
	                                      </div> 
	                                      
	                                      <div class="" style="margin-top:-20px;"> 
	                                         
	                                         <div class="col-md-4">
	                                            <div class="form-group">
	                                                <label>Area Name</label>
	                                                <input type="text" id="preAreaId" class="form-control" value="${borrowerBean.borrower_present_area}" name="borrower_present_area" path="borrower_present_area" placeholder="Area name" >
	                                            	<div id="preAreaErrId" class="errorStyle"></div>
	                                            </div>
	                                         </div>
	                                         
	                                         <div class="col-md-4">
	                                            <div class="form-group">
	                                                <label>City Name</label>
	                                                <input type="text" id="preCityId" class="form-control" value="${borrowerBean.borrower_present_city}" name="borrower_present_city" path="borrower_present_city" placeholder="City name" >
	                                            	<div id="preCityErrId" class="errorStyle"></div>
	                                            </div>
	                                         </div>
	                                         
	                                         <div class="col-md-4">
	                                            <div class="form-group">
	                                                <label>Pincode</label>
	                                                <input type="text" id="prePincodeId" class="form-control" value="${borrowerBean.borrower_present_pincode}" name="borrower_present_pincode" path="borrower_present_pincode" maxlength="6" placeholder="Present pincode" >
	                                            	<div id="prePinErrId" class="errorStyle"></div>
	                                            </div>
	                                         </div>
	                                         
	                                        
	                                    </div>
	                                   
	                                 </div> 
	                                   
	                                 <div class="row">   
	                                    
	                                    <div class="header">
	                                       
	                                        
			                                <h4 class="formSubHeading title">Permanent Address      <l style="font-size: 13px;color:rgb(0, 182, 245); margin-left: 52px;"> <input type="checkbox" id="checkId"  onclick="copyToPermanent()"/> Do the same</l>
	                                        </h4>
			                               
			                            </div>
	                                    
	                                    
	                                    <div class="">
	                                    	
	                                        <div class="col-md-4">
	                                            <div class="form-group">
	                                                <label>Door No</label>
	                                                <input type="text" id="perDoorId" class="form-control" value="${borrowerBean.borrower_permanent_door}" name="borrower_permanent_door" path="borrower_permanent_door" placeholder="Door number" >
	                                            	<div id="perDoorErrId" class="errorStyle"></div>
	                                            </div>
	                                        </div>
	                                        
	                                        <div class="col-md-4">
	                                            <div class="form-group">
	                                                <label>Building Name</label>
	                                                <input type="text" id="perBuildingId" class="form-control" value="${borrowerBean.borrower_permanent_building}" name="borrower_permanent_building" path="borrower_permanent_building" placeholder="Building name" >
	                                            	<div id="perBuildingErrId" class="errorStyle"></div>
	                                            </div>
	                                        </div>
	                                        
	                                         <div class="col-md-4">
	                                            <div class="form-group">
	                                                <label>Street Name</label>
	                                                <input type="text" id="perStreetId" class="form-control" value="${borrowerBean.borrower_permanent_street}" name="borrower_permanent_street" path="borrower_permanent_street" placeholder="Street name" >
	                                            	<div id="perStreetErrId" class="errorStyle"></div>
	                                            </div>
	                                         </div>
	                                         
	                                         
	                                       </div>
	                                       
	                                       <div class="">
	                                       
	                                         <div class="col-md-4">
	                                            <div class="form-group">
	                                                <label>Area Name</label>
	                                                <input type="text" id="perAreaId" class="form-control" value="${borrowerBean.borrower_permanent_area}" name="borrower_permanent_area" path="borrower_permanent_area" placeholder="Area name" >
	                                            	<div id="perAreaErrId" class="errorStyle"></div>
	                                            </div>
	                                         </div>
	                                         
	                                         <div class="col-md-4">
	                                            <div class="form-group">
	                                                <label>City Name</label>
	                                                <input type="text" id="perCityId" class="form-control" value="${borrowerBean.borrower_permanent_city}" name="borrower_permanent_city" path="borrower_permanent_city" placeholder="City name" >
	                                            	<div id="perCityErrId" class="errorStyle"></div>
	                                            </div>
	                                         </div>
	                                         
	                                         <div class="col-md-4">
	                                            <div class="form-group">
	                                                <label>Pincode</label>
	                                                <input type="text" id="perPincodeId" class="form-control" value="${borrowerBean.borrower_permanent_pincode}" name="borrower_permanent_pincode" path="borrower_permanent_pincode" maxlength="6" placeholder="Permanent pincode" >
	                                            	<div id="perPinErrId" class="errorStyle"></div>
	                                            </div>
	                                         </div>
	                                         
	                                        
	                                    </div>
	                                    
										 
                                    </div>
                                	
                                	
                                	<div class="row">
                                        
                                        <div class="header">
			                                <h4 class="formSubHeading title">Income Details</h4>
			                            </div>
			                            
                                        <div class="col-md-6">
                                            <div class="form-group gapLength">
                                                <label>Type of Income</label>
                                                
                                                 <select class="form-control" name="income_type" path="income_type" id="income_type" >
                                                        <option value="Select type of income">Select type of income</option>
                                                        <option value="Salaried">Salaried</option>
                                                        <option value="Business">Business</option>
                                                        <option value="Self Salaried">Self Salaried</option>
                                                 </select>
                                           		
                                           		<div id="incomeErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <div class="form-group gapLength">
                                                <label>Annual Income</label>
                                                
                                                <input class="form-control"  type="text" name="income_annualy" placeholder="Enter your annual income" path="income_annualy" id="income_annualy"/> 
                                               
                                            	<div id="incomeAmtErrId" class="errorStyle"></div>
                                            	
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                    <c:set var="minAmount" value="${det.policy_min_amount}"/>
                                    <% long minAmount = (Long)pageContext.getAttribute("minAmount"); %>
									
                                    <c:set var="maxAmount" value="${det.policy_max_amount}"/>
									<% long maxAmount = (Long)pageContext.getAttribute("maxAmount"); %>
									
                                    <c:set var="minTime" value="${det.policy_min_duration}"/>
                                    <% int minTime = (Integer)pageContext.getAttribute("minTime"); %>
									
                                    <c:set var="maxTime" value="${det.policy_max_duration}"/>
									<% int maxTime = (Integer)pageContext.getAttribute("maxTime"); %>
									
									
									
                                    <div class="row">
                                        
                                        <div class="header">
			                                <h4 class="formSubHeading title">Finance Details</h4>
			                            </div>
			                            
                                        <div class="col-md-6">
                                            <div class="form-group gapLength">
                                                 <label>Finance Amount</label>
                                                 <input type="text" class="formatNumber form-control" value="<%=session.getAttribute("requestAmount")%>" disabled name="deal_amount" placeholder="Enter your required amount"  path="deal_amount" id="deal_amount" onkeyup="amountFormat('deal_amount')" />
                                                                         		 
                                           		 <div id="amountErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <div class="form-group gapLength">                               
                                                <label>Time</label>
                                                
                                                 <select class="form-control" name="deal_duration" path="deal_duration" id="deal_duration">
                                                        <option value="Select interested duration">Select interested duration</option>
                                                        <%for(int i=minTime;i<=maxTime;i++){ %>
                                                        			
                                                        		<option value="<%=i%>">for <%=i%> months</option>
                                                        			
                                                        <% } %>
                                                 </select>
                                                 
                                                 
                                            	<div id="timeErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                    
                                     <div class="row">
                                        
                                        <div class="col-md-3">
                                            <div class="form-group manageFormComponent">
                                                <label>Do you have own house?</label>
                                             </div>
                                        </div>
                                        
                                        <div class="col-md-1">
                                            <div class="form-group manageFormComponent">
                                                
                                                <input type="radio" value="Yes" name="house_status" path="house_status" id="houseId" >Yes 
                                                
                                            </div>
                                            <div class="form-error" id="docErrorId"></div>
                                        </div>
                                        
                                        <div class="col-md-1">
                                            <div class="form-group manageFormComponent">
                                                
                                                <input type="radio" value="No"  name="house_status" path="house_status" id="houseId" >No 
                                                
                                            </div>
                                            <div class="form-error" id="docErrorId"></div>
                                        </div>
                                        
                                        
                                    </div>
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-3">
                                            <div class="form-group manageFormComponent">
                                                <label>Do you have 4 wheeler vehicle?</label>
                                             </div>
                                        </div>
                                        
                                        <div class="col-md-1">
                                            <div class="form-group manageFormComponent">
                                                
                                                <input type="radio" value="Yes" name="four_wheeler_status" path="four_wheeler_status" id="whe4Id" >Yes 
                                                
                                            </div>
                                            <div class="form-error" id="docErrorId"></div>
                                        </div>
                                        
                                        <div class="col-md-1">
                                            <div class="form-group manageFormComponent">
                                                
                                                <input type="radio" value="No" name="four_wheeler_status" path="four_wheeler_status" id="whe4Id" >No 
                                                
                                            </div>
                                            <div class="form-error" id="docErrorId"></div>
                                        </div>
                                        
                                        
                                    </div>
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-3">
                                            <div class="form-group manageFormComponent">
                                                <label>Do you have 2 wheeler vehicle?</label>
                                             </div>
                                        </div>
                                        
                                        <div class="col-md-1">
                                            <div class="form-group manageFormComponent">
                                                
                                                <input type="radio" value="Yes" name="two_wheeler_status"  path="two_wheeler_status" id="whe2Id" >Yes 
                                               
                                            </div>
                                            <div class="form-error" id="docErrorId"></div>
                                        </div>
                                        
                                        <div class="col-md-1">
                                            <div class="form-group manageFormComponent">
                                                
                                                <input type="radio" value="No" name="two_wheeler_status" path="two_wheeler_status" id="whe2Id" >No 
                                               
                                            </div>
                                            <div class="form-error" id="docErrorId"></div>
                                        </div>
                                        
                                    </div>
                                    
                                	
                                    <!-- Document status section is attach with different page  -->
                                   
                                	<div class="row" id="borrowerAvailDocId">
                                        
                                         <%@include file="borrower_policy_avail_doc.jsp" %> 
                                         
									</div>   
                                	
                                    <!-- End  of Document attach with different page -->
                                    
                                    
                                    
                                    <div class="row">
                                        
                                        <div class="header">
			                                <h4 class="formSubHeading title">Any note to lender (Optional)</h4>
			                            </div>
			                            
                                        <div class="col-md-12">
                                            <div class="form-group">                                              
                                                 <textarea placeholder="Send any message to lender" name="note_to_lender" path="note_to_lender" class="form-control" rows="2"></textarea>
                                           	</div>
                                        </div>
                                        
                                    </div>
                                    
                                    
                                   
                                	
                                	<input type="hidden" value="${det.policy_id}" name="policy_id" path="policy_id"  />
                                	<input type="hidden" value="${det.lender_id}" name="lender_id" path="lender_id" />
                                	<input type="hidden" value="${det.policy_name}" name="policy_name" path="policy_name"  />
                                	<input type="hidden" value="${det.policy_type}" name="policy_type" path="policy_type" />
                                	<input type="hidden" value="${det.policy_interest_rate}" name="deal_interest_rate" path="deal_interest_rate" />
                                	
                                	<input type="hidden" value="${borrowerBean.borrower_id}" name="borrower_id" path="borrower_id"  />
                                	
                                	<input type="button" value="Apply"  class="btn btn-info btn-fill pull-right"  onclick="applyForFinance()">
                                    
                                	<!-- <input type="button" value="Save"  class="btn btn-info btn-fill pull-right" style="margin-right: 12px;" onclick="saveFinanceForm()"> -->
                                    
                                    <div class="clearfix"></div>
                                    
                                	
                                </form>
                              
                              
                            </div>
                        
                        </div>
                        
                        
                     </c:forEach>
                              
                              
                     </div>
                        
                </div>
                
                
            </div>
        
        
        
        
        		
        </div>
		
		
		
		<div id="popContentId" class="modal">
		
					  
					
		</div>
		
		
		
		
		
		
		
		
		
		
			
	<script src="resources/datePicker/jquery.js"></script>
	<script src="resources/datePicker/jquery.datetimepicker.js"></script>
	<script>
	
	$('.some_class').datetimepicker();
	
	$('#some_class3').datetimepicker();
	
	/* $('#datetimepicker_dark').datetimepicker({theme:'dark'}) */
		
	</script>
	
	
	
	
</body>
</html>