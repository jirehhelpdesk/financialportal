<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Borrower Request</title>

    <link href="resources/borrower_lender_resources/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="resources/borrower_lender_resources/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="resources/borrower_lender_resources/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="resources/borrower_lender_resources/assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="resources/borrower_lender_resources/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    
    
    <link href="resources/borrower_lender_resources/profile_theme_style.css" rel="stylesheet" >
    <link href="resources/borrower_lender_resources/profile_theme_style1.css" rel="stylesheet" >
    
    
 <link rel="stylesheet" type="text/css" href="resources/datePicker/jquery.datetimepicker.css"/>
 
</head>


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<body onload="lenderActiveSideMenu('menu3'),loadPage()">


<div class="wrapper">
    
    
      <!-- Start of Sidemenu Division -->
    
         <%@include file="lender_sidemenu.jsp" %>

      <!-- End of Sidemenu Division -->


    <div class="main-panel">
        
        
         <!-- Start of Header Division -->
         
         <%@include file="lender_header.jsp" %>
        
		 <!-- End of Header Division -->

       
        <div class="content">
            
            
            <div class="container-fluid">
            
                 <div class="card">
                 
                           <div class="header">
                               <h4 class="title">Filter Borrower Request</h4>
                           </div>
                           
                           <%String lenderPolicyType = (String)request.getAttribute("lenderPolicyType"); %>
                           <%if(!lenderPolicyType.equals("No Data")) {%>
                           
                          <div class="content" style="width: 55%; margin-left: 230px;">
                               
                           
                             <form id="borrowerRequestSearchForm" >
                                   
								
								<div class="row">
                                       <div class="col-md-12">
                                           <div class="form-group">
                                               <label>Search Type</label>
                                               
                                               <select class="form-control" id="searchType" onchange="showBwrReqOption(this.value)">
                                               			<option value="Select Search Type">Select Search Type</option>
                                               			<option value="Via Duration">Via Duration</option>
                                               			<option value="Via Loan Type">Via Loan Type</option>
                                               			<option value="All">All</option>
                                               </select>
                                               
                                           </div>
                                       </div>
                                   </div>
                                   
                                   <div class="row" id="durationType" style="display:none;">
                                       <div class="col-md-6">
                                           <div class="form-group">
                                               <label>From Date</label>
                                               <input type="text"  placeholder="From Date" readonly  id="fromDateId" class="form-control some_class">
                                           </div>
                                       </div>
                                       <div class="col-md-6">
                                           <div class="form-group">
                                               <label>To Date</label>
                                               <input type="text"  placeholder="To Date" readonly  id="toDateId" class="form-control some_class">
                                           </div>
                                       </div>
                                   </div>

                                   <div class="row" id="loanType" style="display:none;">
                                       <div class="col-md-12">
                                           <div class="form-group">
                                                
                                                <label>Loan Type</label>
                                                
                                                  <%String policyTypeArray[] = lenderPolicyType.split("/"); %>
                                         	  
                                         	      <select class="form-control" id="policyTypeValue">
                                        			    <option value="Select Loan Type">Select Loan Type</option>
                                        			
                                        				<%for(int i=0;i<policyTypeArray.length;i++) {%>
                                        				
                                        						<option value="<%=policyTypeArray[i]%>"><%=policyTypeArray[i]%></option>
                                        				<%}%>
                                        		  </select>	
                                                
                                           </div>
                                       </div>
                                   </div>

                                   <button class="btn btn-info btn-fill pull-right" type="button" onclick="searchBorrowerReqByLender()">Search</button>
                                   
                                   <div class="clearfix"></div>
                                   
                               </form>
                               
                           </div>
                           
                           <%}else{ %>
                           
                           		<div class="content recordNotFoundDiv"><p class="recordNotFound">Please add some financial feature before you get the request from Borrower.<a href="addFeature">Add Feature</a></p></div>
                           
                           <%} %>
                             
                 </div>









					<!-- Table Div -->
					
					<div class="row" id="contentDiv"> 

						   <%@include file="lender_borrower_loan_request.jsp" %>   

					</div>
					
					<!-- Table Div -->
					
					
					


				</div>
            
            
            
        </div>
        
        

       <!-- Start of Footer Division -->
 
         <%@include file="lender_footer.jsp" %>
        
	   <!-- Start of Footer Division -->
	   
	   
    </div>
</div>


</body>

       <!-- Start of Footer Division -->
 
         <%@include file="common_content.jsp" %>
        
	   <!-- Start of Footer Division -->   
       
       
       
       
       
       
       
</html>
