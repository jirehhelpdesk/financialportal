<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<style type="text/css">

.pg-normal { 
color: #fff; 
font-size: 15px; 
cursor: pointer; 
background: #00b6f5; 
padding: 2px 4px 2px 4px; 
float:left;
}

.pg-selected { 
color: #fff; 
font-size: 15px; 
background: #000000; 
padding: 2px 4px 2px 4px; 
float:left;
}


</style>


<script type="text/javascript" src="resources/paging/pageingScript.js"></script> 

</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<body>


<c:if test="${!empty borrowerRequestBean}">	


                   <div style="width:100%;"  class="col-md-8">
                
                		
                		<div class="card">
                            
                            <div class="header">
                                <h4 class="title">Borrower Request</h4>
                            </div>
                            
                            <div id="pagination"></div>
                            
                            <div class="content table-responsive table-full-width">
                                
                                <table class="table table-hover" role="grid" id="tablepaging"  >
                                    
                                    
                                    <thead>
                                        
                                        <tr>
                                        	<th>Borrower Name</th>
                                        	<th>Borrower City</th>
											<th>Loan Type</th>
											<th>Policy Name</th>
											<th>Requested Date</th>
											<th>Amount</th>
											<th>Tenure</th>
											
											<th>Interested</th>
										</tr>
                                    
                                    
                                    </thead>
                                    
                                    
                                    <tbody id="myTableBody">
                                        
                                         <c:forEach items="${borrowerRequestBean}" var="det">		
										
												<tr>
													
													<td>${det.borrower_name}</td>
													<td>${det.borrower_city}</td>
													
													<td>${det.policy_type}</td>
													<td>${det.policy_name}</td>
													<td><fmt:formatDate pattern="dd/MM/yyyy" value="${det.deal_initiated_date}" /></td>
													<td>Rs.&nbsp; ${det.deal_amount}</td>
													<td>${det.deal_duration} &nbsp; months</td>
													
													<td><a href="#" style="color:green;float:left;" title="Click if you interested on this." onclick="yesBorrowerRequest('${det.deal_id}','Yes')"><u>Yes</u></a> &nbsp;&nbsp; <a href="#" title="Click if you not interested on this." style="color:red;float:left;margin-left:18px;" onclick="noBorrowerRequest('${det.deal_id}','No')"><u>No</u></a>
													</td>
	                                            
												</tr>
												
										</c:forEach>
                                        
                                    </tbody>
                                    
                                    
                                </table>
                                
                                <div id="pageNavPosition" style="padding-top: 20px;float: right;" align="center">
								</div>
								<script type="text/javascript">
								
								var pager = new Pager('tablepaging', 10);
								pager.init();
								pager.showPageNav('pager', 'pageNavPosition');
								pager.showPage(1);
								</script>
                              
                            </div>
                            
                            
                        </div>
                        
                        
                        </div>
               
</c:if>
  
<c:if test="${empty borrowerRequestBean}">	

 <div style="width:100%;"  class="col-md-8">
    
    <div class="card">
		
		 <div class="header">
             <h4 class="title">Borrower Request</h4>
         </div>
                            
                            
		<div class="content table-responsive table-full-width">
		     <p style="margin-left:16px;">As of now you didn't get any request from 'Borrower'.</p>
		</div>         
		                
	</div>
																	
 </div>
 
</c:if>                
                
</body>
</html>