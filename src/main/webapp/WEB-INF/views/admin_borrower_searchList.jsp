<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" href="resources/paging/other.css">

</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<body>

<div class="table-responsive">
                                        
   
 <c:if test="${!empty borrowerDetails}">                                       
                                        
                                  <table aria-describedby="DataTables_Table_0_info" role="grid" id="tableData" class="table datatable dataTable no-footer">
                                           
                                        <thead>
											
											<tr role="row">
												
												<th width="50">Sl No</th>
												
												<th  width="180">Full Name</th>
													
												<th  width="80">Email Id</th>
												
												<th  width="60">Mobile No</th>
												
												<th  width="50">Status</th>
												
												<th  width="150">Manage</th>
											
											</tr>
											
										</thead>
										
                                            <tbody>   
                                                                                     
		                                            <%int i=1; %> 
               										<c:forEach items="${borrowerDetails}" var="det">	
               						 
			                                            <tr class="odd" role="row" style="display: table-row;">
															
															<td class="sorting_1"><%=i++%></td>
															
															
															<c:set var="photo" value="${det.borrower_profile_photo}"/>
															 <% String photo = (String)pageContext.getAttribute("photo"); %>
															  
															  <c:set var="gender" value="${det.borrower_gender}"/>
															 <% String gender = (String)pageContext.getAttribute("gender"); %>
															  
															 <c:set var="id" value="${det.borrower_id}"/>
															 <% int id = (Integer)pageContext.getAttribute("id"); %>
															 
															  <c:set var="status" value="${det.borrower_status}"/>
													 		  <% String status = (String)pageContext.getAttribute("status"); %>
													   
													   
															<td class="sorting_1">
															   
															  <img class="searchBorrowerImg" src="${pageContext.request.contextPath}<%="/previewUserPhoto?fileName="+photo+"&id="+id+"&userType=Borrower"+"&gender="+gender+""%>" >
				                                          
															   ${det.borrower_name}
															</td>
															<td>${det.borrower_emailid}</td>
															<td>${det.borrower_phno}</td>
															<td>${det.borrower_status}</td>
															
															<td>
															
																<button onclick="viewBorrowerProfile('${det.borrower_id}','${det.borrower_first_name}')" class="btn btn-info">View</button>
																
																<%-- <button onclick="manageBorrowerDetails('${det.borrower_id}','${det.borrower_status}')" class="btn btn-primary">Manage</button>
																 --%>
																<%if(status.equals("Active")){ %>
																	<button onclick="updateBorrowerAccess('${det.borrower_id}','${det.borrower_status}')" class="btn btn-primary">Block</button>
																<%}else {%>
																	<button onclick="updateBorrowerAccess('${det.borrower_id}','${det.borrower_status}')" class="btn btn-primary">Unblock</button>
																<%} %>
															</td>
															
														</tr>
													
													</c:forEach>	
													
                                            </tbody>
                                            
                                        </table>
 
 </c:if>                                       
                                        
                                    </div>                               
                                    <!-- Paging zone -->
          							
          							
									<script type="text/javascript" src="resources/paging/jquery.min.js"></script> 
									
									<script src="resources/paging/jquery-ui.min.js"></script>
									
									<script type="text/javascript" src="resources/paging/paging.js"></script> 
									
									<script type="text/javascript">
									            $(document).ready(function() {
									                $('#tableData').paging({limit:10});
									            });
									        </script>
									        <script type="text/javascript">
									
									  var _gaq = _gaq || [];
									  _gaq.push(['_setAccount', 'UA-36251023-1']);
									  _gaq.push(['_setDomainName', 'jqueryscript.net']);
									  _gaq.push(['_trackPageview']);
									
									  (function() {
									    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
									    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
									    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
									  })();
									
									</script>
									
				                  
				                   <!-- End of  Paging zone -->
                                               
</body>
</html>