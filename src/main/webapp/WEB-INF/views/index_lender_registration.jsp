<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lender Registration</title>

    <link rel="shortcut icon" type="image/icon" href="resources/indexResources/assets/images/favicon.ico"/>
    <!-- Font Awesome -->
    <link href="resources/indexResources/assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="resources/indexResources/assets/css/bootstrap.css" rel="stylesheet">
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="resources/indexResources/assets/css/slick.css"/> 
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="resources/indexResources/assets/css/jquery.fancybox.css" type="text/css" media="screen" /> 
    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="resources/indexResources/assets/css/animate.css"/>  
     <!-- Theme color -->
    <link id="switcher" href="resources/indexResources/assets/css/theme-color/default.css" rel="stylesheet">

    <!-- Main Style -->
    <link href="resources/indexResources/style.css" rel="stylesheet">

    <!-- Fonts -->
    <!-- Open Sans for body font -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Raleway for Title -->
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <!-- Pacifico for 404 page   -->
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    
    
    
    <!-- Registration Style  -->
    		
    	<!-- <link rel="stylesheet" href="resources/indexResources/index_registration_rsc/assets/bootstrap/css/bootstrap.min.css"> -->
        <link rel="stylesheet" href="resources/indexResources/index_registration_rsc/assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="resources/indexResources/index_registration_rsc/assets/css/form-elements.css">
        <link rel="stylesheet" href="resources/indexResources/index_registration_rsc/assets/css/style.css">

    <!-- EOF Registration Style -->
    
</head>



<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<body onload="activeIndexMenu('Menu7')">

     <%@include file="index_header.jsp" %>


  <!-- Start about section -->
 
  <section id="about" style="margin-top:0px ! important;margin-bottom: 46px;">
    
    <div class="container">
          
         <!-- Registration Block -->
      
      		
      <div class="top-content">
        	
            <div class="inner-bg">
               	        
               	                       
                    <div class="row">
                                   
                        
                        <div class="col-sm-5">
		                        			
		                        		<div class="row">
					                        <div class="Heading-align">
					                           
					                            <h1 class="signBox-Heading"> Sign In</h1>
					                            
					                        </div>
					                    </div>    
		                    
			                            <div class="form-bottom" style="color:#00b6f5;font-size: 13px;padding: 25px 25px 18px;">
			                            		
			                            		<div class="form-group">
						                    		<label class="" for="form-username"><i class="fa fa-caret-right"></i> &nbsp; If you are already a register user then click on below.</label>
						                    		            	
						                        </div>
						                        
						                       <div class="row">
	                                    	
			                                        <div class="col-md-12">
			                                            <div class="form-group">
			                                                <label class="" for="form-first-name"></label>
								                        	<img src="resources/indexResources/assets/images/signInInfo.jpg" style="margin-left:48px;" />
			                                            </div>
			                                        </div>
			                                      
			                                    </div>
						                        
						                        <div class="form-group">
						                        	<button class="btn" style="margin-left:176px;margin-top:5px;" onclick="registerRedirect('lenderLogin')">Sign In</button>
						                    	</div>
						                    	
			                            		
			                            		<div class="form-group"> </div>
			                            		
			                            </div>
		                        		 
		                        		 
		                       </div>
		                       
                          
                        
                        <div class="col-sm-7">
                        	
                        	<div class="form-box">
                        		
                        		<div class="row">
			                        <div class="Heading-align">
			                           
			                            <h1 class="signBox-Heading">Lender Registration</h1>
			                            
			                        </div>
			                    </div>    
			                    
	                            <div class="form-bottom signBox-BottomDiv">
				                    
				                    
				                    <form:form class="registration-form" id="lenderRegForm" modelAttribute="lenderRegDetails" method="post" action="saveLenderRegisterDetails" >
				                    
				                    	
				                    	 <div class="row">
	                                    	
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                                <label class="" for="form-first-name">Lender Type</label>
				                    		
						                    		<select name="lender_type" path="lender_type" class="form-lenderType form-control" id="lenderTypeId" onchange="changeNameType(this.value)">
						                    			
						                    			    <option value="Select type of lender">Select type of lender</option>
						                    			    <option value="Individual">Individual</option>
						                    			    <option value="Bank">Bank</option>
						                    				<option value="An Organization">An Organization</option>
						                    				
						                    		</select>
						                    		
						                        	<div id="typeErrId" class="errorStyle"></div>
	                                            </div>
	                                        </div>
	                                        
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                                <label class="" for="form-last-name" id="nameTypeId">Full name</label>
						                        	<form:input type="text" name="full_name" maxlength="80" path="full_name"  class="fullName form-control" id="fullNameId"  />
						                        	<div id="nameErrId" class="errorStyle"></div>
	                                            </div>
	                                        </div>
	                                        
	                                    </div>
	                                    
	                                    
	                                    <div class="row">
	                                    	
	                                        <div class="col-md-12">
	                                            <div class="form-group" id="representTagId" style="display:none;">
	                                               <label class="" for="form-last-name" >Representative name</label>
						                        	<form:input type="text" name="lender_representative_name" maxlength="80" path="lender_representative_name"  class="fullName form-control" id="represNameId"  />
						                        	<div id="represNameErrId" class="errorStyle"></div>
	                                            </div>
	                                        </div>
	                                        
	                                    </div>
	                                      
				                        <div class="row">
	                                    	
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                                <label class="" for="form-email">Email</label>
						                        	<form:input type="text" autocomplete="off" name="lender_emailid" maxlength="50" path="lender_emailid"  class="email form-control" id="emailId"  />
						                       		<div id="emailErrId" class="errorStyle"></div>
	                                            </div>
	                                        </div>
	                                        
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                                <label class="" for="form-mobileno">Mobile No</label>
						                        	<form:input type="text" autocomplete="off" name="lender_phno" maxlength="10" path="lender_phno" class="phno form-control" id="phnoId"  />
						                        	<div id="mobileErrId" class="errorStyle"></div>
	                                            </div>
	                                        </div>
	                                        
	                                    </div>
	                                    
	                                    
	                                    <div class="row">
	                                    	
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                                <label class="" for="form-password">Password</label>
						                        	<form:input type="password" autocomplete="off" name="lender_password" maxlength="30" path="lender_password" class="password form-control" id="passwordId" />
						                        	<div id="passwordErrId" class="errorStyle"></div>
	                                            </div>
	                                        </div>
	                                        
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                                <label class="" for="form-confirmpassword">Confirm Password</label>
						                        	<input type="password" autocomplete="off" name="form-confirmpassword"  maxlength="30" class="cnfPassword form-control" id="cnfPasswordId"  />
						                        	<div id="cnfPasswordErrId" class="errorStyle"></div>
	                                            </div>
	                                        </div>
	                                        
	                                    </div>
	                                    
	                                    
	                                    
				                        <div class="form-group">
				                        
				                            
				                        	<a href="#" style="font-weight: bold; font-size: 14px;"><input type="checkbox"  name="form-confirmpassword" class="terms" id="termsId" />&nbsp; I/We agree with your terms and conditions.</a>
				                        	<div id="termErrId" class="errorStyle"></div>
				                        	
				                        </div>
				                        
				                       				                        
				                    </form:form>
				                    
				                        
				                        <div class="row">
				                            
				                            <div class="col-md-12">
						                    	 
						                    	 <div class="form-group">
						                    	        
						                    	        <button class="btn" style="margin-left:294px;margin-top:1px;" onclick="lenderRegistration()">Sign up</button>
						                    	        
								                 </div>
						                    
						                    </div>
						                    
				                        </div>
			                    </div>
                        	</div>
		                
                        </div>
                        
                        	
                        
                        
                    </div>
                    
                
            </div>
            
        </div>

       
      	 <!-- EOF of registration block  -->	
      
      
      
    </div>
  </section> 
  
  
  <!-- End about section -->



 


        <%@include file="index_fotter.jsp" %>







  <!-- initialize jQuery Library --> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <!-- Bootstrap -->
  <script src="resources/indexResources/assets/js/bootstrap.js"></script>
  <!-- Slick Slider -->
  <script type="text/javascript" src="resources/indexResources/assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="resources/indexResources/assets/js/waypoints.js"></script>
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.counterup.js"></script>
  <!-- mixit slider -->
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->        
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.fancybox.pack.js"></script>
  <!-- Wow animation -->
  <script type="text/javascript" src="resources/indexResources/assets/js/wow.js"></script> 

  <!-- Custom js -->
  <script type="text/javascript" src="resources/indexResources/assets/js/custom.js"></script>
    
   
   
  <script type="text/javascript" src="resources/indexResources/index_registration_script.js"></script> 
 
 
    
      <!-- Registration script -->
    
        <script src="resources/indexResources/index_registration_rsc/assets/js/jquery-1.11.1.min.js"></script>
        <script src="resources/indexResources/index_registration_rsc/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="resources/indexResources/index_registration_rsc/assets/js/scripts.js"></script>
     		
     <!-- EOF Registration script -->
     
      		
  </body>
</html>