<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" href="resources/paging/other.css">

<script type="text/javascript" src="resources/paging/pageingScript.js"></script> 

</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>


<body>

<c:if test="${!empty borrowerDetails}">	


<div class="table-responsive">
                                        
                                        
                                        
                                        <table id="tablepaging" class="table table-bordered table-striped table-actions">
                                            
                                           
                                            <thead>
                                                <tr>
                                                    <th width="10">Sl No</th>
                                                    <th width="100">Name</th>
                                                    <th width="30">Gender</th>
                                                    <th width="70">Email Id</th>
                                                    <th width="70">Ph No</th>                                                    
                                                    <th width="60">Status</th>
                                                    <th width="120">Manage</th>
                                                </tr>
                                            </thead>
                                            
                                            
                                            <tbody id="myTableBody">                                            
                                                
                                               <%int i=1; %> 
		               							<c:forEach items="${borrowerDetails}" var="det">	    
				                                     
				                                     <c:set var="borrower_photo" value="${det.borrower_profile_photo}"/>
													 <% String borrower_photo = (String)pageContext.getAttribute("borrower_photo"); %>
													  
													  <c:set var="gender" value="${det.borrower_gender}"/>
													 <% String gender = (String)pageContext.getAttribute("gender"); %>
													  
													 <c:set var="borrower_id" value="${det.borrower_id}"/>
													 <% int borrower_id = (Integer)pageContext.getAttribute("borrower_id"); %>
													 
													    
				                                           
	                                                <tr id="trow_1">
	                                                    
	                                                    <td><%=i++%></td>
	                                                    
	                                                    <td>
	                                                       
	                                                        <%-- <img class="searchBorrowerImg" src="${pageContext.request.contextPath}<%="/previewUserPhoto?fileName="+borrower_photo+"&id="+borrower_id+"&userType=Borrower"+"&gender="+gender+""%>" > --%>
				                                         
	                                                    	<strong>${det.borrower_name}</strong>
	                                                    
	                                                    </td>
	                                                    
	                                                    <td>${det.borrower_gender}</td>
	                                                    <td>${det.borrower_emailid}</td>
	                                                    <td>${det.borrower_phno}</td>
	                                                    
	                                                    <td>${det.borrower_status}</td>
	                                                    <td>
	                                                    	<button class="btn btn-info" type="button" style=" border-radius: 20px;" onclick="viewUserProfile('${det.borrower_id}','${det.borrower_name}','Borrower')">Profile</button>
	                                                    	
	                                                        <button class="btn btn-info" type="button" style=" border-radius: 20px;" onclick="showBoxToNotify('${det.borrower_id}')">Notify</button>
	                                                        
	                                                        <c:if test="${det.borrower_status =='Active'}">
	                                                          
	                                                          <button class="btn btn-info" type="button" style="border-radius:20px;" onclick="manageBorrowerStatus('${det.borrower_id}','Block')">Block</button>
	                                                        
	                                                        </c:if>
	                                                        
	                                                        <c:if test="${det.borrower_status !='Active'}">
	                                                         
	                                                          <button class="btn btn-info" type="button" style=" border-radius: 20px;" onclick="manageBorrowerStatus('${det.borrower_id}','Unblock')">Unblock</button>
	                                                        
	                                                        </c:if>
	                                                        
	                                                    </td>
	                                                </tr>
	                                                
                                                </c:forEach>
                                                
                                            </tbody>
                                        </table>
                                        
                                         <!-- Paging zone -->
          							
	          							<div id="pageNavPosition" style="padding-top: 20px;float: right;" align="center">
										</div>
										<script type="text/javascript">
										
										var pager = new Pager('tablepaging', 10);
										pager.init();
										pager.showPageNav('pager', 'pageNavPosition');
										pager.showPage(1);
										</script>
		                              
					                  
					                   <!-- End of  Paging zone -->
                                        
                                    </div>      
                                    
                                    
                                   
    </c:if>
    
    
    <c:if test="${empty borrowerDetails}">	
    
    
			   <div class="col-md-12" style="margin-top:20px;">

                                                     
                            <div class="messages messages-img" >
                                
                                <div class="item item-visible">
                                                                    
                                    <div class="text customMsgText">
                                        <div class="heading">
                                            <a href="#"></a>
                                            <span class="date"></span>
                                        </div>                                    
                                        No Borrower was found as per the filter criteria.
                                    </div>
                                </div>
                            </div>

                        </div>
    
    </c:if>                                           
</body>
</html>