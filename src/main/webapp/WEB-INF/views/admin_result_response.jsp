<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


<input type="hidden" id="closeLinkId" />
<input type="hidden" id="conditionId" />
		

<div id="message-box-waiting" class="message-box message-box-success animated fadeIn open" style="display:none;">
     <div class="mb-container1">
           
           <%@include file="loader2.jsp" %>
           
     </div>
 </div>
 
 
<div id="message-box-success" class="message-box message-box-success animated fadeIn open" style="display:none;">
     <div class="mb-container">
         <div class="mb-middle">
             <div class="mb-title"><span class="fa fa-check"></span> Success</div>
             <div class="mb-content">
                 <p id="messageId"></p>
             </div>
             <div class="mb-footer">
                 <button class="btn btn-default btn-lg pull-right mb-control-close" onclick="closeLink()">Close</button>
             </div>
         </div>
     </div>
 </div>
        
        
  <div id="message-box-danger" class="message-box message-box-danger animated fadeIn open" style="display:none;">
       <div class="mb-container">
           <div class="mb-middle">
               <div class="mb-title"><span class="fa fa-times"></span> Danger</div>
               <div class="mb-content">
                   <p id="messageId"></p>
               </div>
               <div class="mb-footer">
                   <button class="btn btn-default btn-lg pull-right mb-control-close" onclick="closeLink()">Close</button>
               </div>
           </div>
       </div>
   </div>
        
              
</body>
</html>