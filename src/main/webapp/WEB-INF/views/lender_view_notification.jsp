<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>



<body>
  
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <h4 class="title">Message</h4>
                <p class="category"></p>
            </div>
            
            <div class="content">


			<c:forEach items="${notifyDetails}" var="det">
					
					<div class="typo-line" style=" margin-bottom: 10px;padding-left:124px;">
						
						<p style="font-size: 14px;">
						    
						    <span class="category" style="color:#00b6f5;">Sent Date</span>
						    
						    <fmt:formatDate pattern="dd/MM/yyyy" value="${det.notify_date}" />
						    
						</p>
						
					</div>
					
					
					<div class="typo-line" style=" margin-bottom: 10px;padding-left:124px;">
						
						<p style="font-size: 14px;">
						    
						    <span class="category" style="color:#00b6f5;">Subject</span>
						    
						    ${det.notify_subject}
							
						</p>
						
					</div>
					
					<div class="typo-line" style=" margin-bottom: 10px;padding-left:124px;">
						
						<p style="font-size: 14px;">
						    
						    <span class="category" style="color:#00b6f5;">Message</span>
						    
						    ${det.notify_message}
							
						</p>

					</div>

					
                
	                <div class="typo-line">
	                   
	                    <button style="margin-right:22px;margin-bottom: 26px;" class="btn btn-info btn-fill pull-right" type="button" onclick="closeNotify()">Close</button>
	                    
	                </div>
                
                
               </c:forEach> 
             
            </div>
        </div>
    </div>
          
</body>
</html>