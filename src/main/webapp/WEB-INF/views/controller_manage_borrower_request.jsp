<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%>
<%@ page isELIgnored="false"%>


<body>


<c:forEach items="${dealDetails}" var="det">	


<div class="row">
                        <div class="col-md-12">
                            
                            <div class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-heading ui-draggable-handle">
                                    <h3 class="panel-title"><strong>Manage Policy Request</strong></h3>                                    
                                </div>
                               
                                <div class="panel-body">

             <div class="timeline timeline-right">
                                
                               
                                <div class="timeline-item timeline-item-right">
                                    <div class="timeline-item-info">Borrower</div>
                                    <div class="timeline-item-icon"><span class="fa fa-user"></span></div>                                   
                                    <div class="timeline-item-content">
                                        
                                        
                                         
										 <c:set var="borrower_photo" value="${det.borrower_profile_photo}"/>
										 <% String borrower_photo = (String)pageContext.getAttribute("borrower_photo"); %>
										  
										 <c:set var="borrower_id" value="${det.borrower_id}"/>
										 <% int borrower_id = (Integer)pageContext.getAttribute("borrower_id"); %>
										    
										                                        
                                        <div style="padding-bottom: 10px;" class="timeline-heading">
                                            <img src="${pageContext.request.contextPath}<%="/previewUserPhoto?fileName="+borrower_photo+"&id="+borrower_id+"&userType=Borrower"%>" >
                                            <a href="#">${det.borrower_name}</a>                                             
                                        </div>  
                                        
                                        <div class="timeline-body">                                            
                                            <b>Email id - </b><i>${det.borrower_emailid}</i>
                                            <br></br>
                                            <b>Contact No - </b><i>+91 &nbsp; ${det.borrower_phno}</i>
                                            <br></br>
                                            <b><a href="#" onclick="viewUserProfile('${det.borrower_id}','${det.borrower_name}','Borrower')">View Profile</a></b>
                                            
                                        </div>
                                        
                                                                       
                                        <div class="timeline-body comments">
                                                 
                                        </div>
                                    </div>                                    
                                </div>       
                                
                                
                                
                                <div class="timeline-item timeline-item-right">
                                    <div class="timeline-item-info">Lender</div>
                                    <div class="timeline-item-icon"><span class="fa fa-user"></span></div>                                   
                                    <div class="timeline-item-content">
                                       
                                        <c:set var="lender_photo" value="${det.lender_profile_photo}"/>
										 <% String lender_photo = (String)pageContext.getAttribute("lender_photo"); %>
										  
										 <c:set var="lender_id" value="${det.lender_id}"/>
										 <% int lender_id = (Integer)pageContext.getAttribute("lender_id"); %>
										    
										                                        
                                        <div style="padding-bottom: 10px;" class="timeline-heading">
                                            <img src="${pageContext.request.contextPath}<%="/previewUserPhoto?fileName="+lender_photo+"&id="+lender_id+"&userType=Lender"%>" >
                                            <a href="#">${det.lender_name}</a>                                             
                                        </div>  
                                        
                                        <div class="timeline-body">                                            
                                            <b>Email id - </b> <i>${det.lender_emailid}</i>
                                            <br></br>
                                            <b>Contact No - </b><i>+91 &nbsp; ${det.lender_phno}</i>
                                            <br></br>
                                            <b><a href="#" onclick="viewUserProfile('${det.lender_id}','${det.lender_name}','Lender')">View Profile</a></b>
                                            
                                        </div>
                                                              
                                        <div class="timeline-body comments">
                                                 
                                        </div>        
                                                                              
                                    </div>  
                                                                      
                                </div>       
                                
                                
                                
                                
                                <div class="timeline-item timeline-item-right">
                                    <div class="timeline-item-info">Deal Details</div>
                                    <div class="timeline-item-icon"><span class="fa fa-list-alt"></span></div>                                   
                                    <div class="timeline-item-content">
                                        
                                        
                                         <div class="timeline-body">                                            
                                            
                                            <b>Policy Type - </b> <i>${det.policy_type}</i>
                                            <br></br>
                                            <b>Policy Name - </b><i> ${det.policy_name}</i>
                                            <br></br>
                                            <b>Interested Amount - </b><i>Rs. &nbsp; ${det.deal_amount}</i>
                                            <br></br>
                                            <b>Finance Time - </b><i> ${det.deal_duration} months</i>
                                            <br></br>
                                            <b>Applied Date - </b><i> ${det.deal_initiated_date}</i>
                                            
                                            <br></br>
                                            <b>Message to lender - </b><i> ${det.note_to_lender}</i>
                                            
                                            
                                            <c:set var="cntrlStatus" value="${det.lender_status}"/>
										    <% String cntrlStatus = (String)pageContext.getAttribute("cntrlStatus"); %>
										    <% String cntrlStatusArray[] = {"Pending","Processing","Approved","Rejected"}; %>
										  
										  
                                            <%-- <br></br>
                                            <b>Controller Status - </b>
	                                            <i>
		                                            
		                                            <select class="form-control" style="width:400px;" id="cntrlStatus">
		                                                			
                                                			<%for(int i=0;i<cntrlStatusArray.length;i++){%>
                                                						
                                               						<%if(cntrlStatus.equals(cntrlStatusArray[i])) {%>
                                               						
                                               							<option selected value="<%=cntrlStatusArray[i]%>"><%=cntrlStatusArray[i]%></option>
                                               						
                                               						<%}else{ %>
                                               						
                                               							<option value="<%=cntrlStatusArray[i]%>"><%=cntrlStatusArray[i]%></option>
                                               							
                                               						<%} %>
                                               						
                                                			<%}%>
                                                			
		                                             </select>  
	                                                
	                                            </i>
                                            
                                            <br> --%>
                                            
                                            <br><br><br>
                                            <c:set var="dealStatus" value="${det.deal_status}"/>
										    <% String dealStatus = (String)pageContext.getAttribute("dealStatus"); %>
										  	<% String dealStatusArray[] = {"Pending","Processing","Onhold","Approved","Rejected"}; %>
										  	
                                            <b>Deal Status - </b>
	                                           
	                                            <i>
		                                            
		                                            <select class="form-control" style="width:400px;" id="dealStatus">
		                                                			
	                                                			<%for(int i=0;i<dealStatusArray.length;i++){ %>
	                                                						
	                                                						<%if(dealStatus.equals(dealStatusArray[i])) {%>
	                                                						
	                                                							<option selected value="<%=dealStatusArray[i]%>"><%=dealStatusArray[i]%></option>
	                                                						
	                                                						<%}else{ %>
	                                                						
	                                                							<option value="<%=dealStatusArray[i]%>"><%=dealStatusArray[i]%></option>
	                                                							
	                                                						<%} %>
	                                                						
	                                                			<%} %>
		                                                			
		                                             </select>  
	                                                
	                                            </i>
	                                            
	                                            
	                                        <br>
                                            
                                         </div>
                                        
                                        
                                        <div class="timeline-body comments">
                                              
                                            <div class="panel-footer">                                                               
			                                    <button onclick="updateDealStatus('${det.deal_id}')" class="btn btn-primary pull-right">Update</button>
			                                </div> 
			                                
                                        </div>
                                        
                                        
                                    </div> 
                                                                       
                                </div>       
                               
                                
                                
                                <!-- <div class="timeline-item timeline-main">
                                    <div class="timeline-date"><a href="#"><span class="fa fa-minus"></span></a></div>
                                </div>  -->  
                                
                                <div class="timeline-item timeline-main">
                                	<div class="timeline-item-icon"><span class="fa fa-minus"></span></div>       
                                </div>                                
                                
                            </div>

                 </div>


                              <!-- 

                                <div class="panel-footer">                                                               
                                    <button onclick="searchBorrowerRequestPolicy()" class="btn btn-primary pull-right">Search<span class="fa fa-search fa-right"></span></button>
                                </div> 
                                
                               -->
                                
                                
                            </div>
                            </div>
                            
                        </div>
                        
                        
                        
                        
                    </div>                  

</c:forEach>
                    
</body>
</html>