<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<div class="row">
    
    <div class="col-md-12">
        <div class="card">
            
            <div class="header">
                <h4 class="title">Reason for rejection</h4>
                <p class="category"></p>
            </div>
            
            <div class="content" style="height:auto;">
			
			  <form id="reasonForm">
				
					<div class="typo-line" style="padding-left: 0px;">
						
						<p style="font-size: 14px;">
						    
						  <textarea id="reasonId" name="reasonContent" placeholder="Please give the reason for rejection."></textarea>
						  <span class="form-error" id="resonErrorId" style="display: none;"></span>
					
						</p>
	
					</div>
	
	                <div class="typo-line">
	                    
	                    <input name="passingDealId" type="hidden" value="<%=request.getAttribute("dealId")%>" >
	                    
	                    <input name="rejectStage" type="hidden" value="<%=request.getAttribute("rejectStage")%>" >
	                    
	                    <button style="margin-top: -29px;" class="btn btn-info btn-fill pull-right" type="button" onclick="submitReason()">Submit</button>
	                    
	                </div>
	                
               </form>
               
            </div>
            
        </div>
        
    </div>

</div>
                
</body>
</html>