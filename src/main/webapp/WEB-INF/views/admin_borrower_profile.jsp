<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>



<body>


<c:set var="borrower_photo" value="${borrower.borrower_profile_photo}"/>
<% String borrower_photo = (String)pageContext.getAttribute("borrower_photo"); %>

<c:set var="gender" value="${borrower.borrower_gender}"/>
<% String gender = (String)pageContext.getAttribute("gender"); %>
  
 <c:set var="borrower_id" value="${borrower.borrower_id}"/>
 <% int borrower_id = (Integer)pageContext.getAttribute("borrower_id"); %>
 
    
													    
<div class="row">


		

		<div class="col-md-3" style="margin-top: 10px;">
   
                            <div class="panel panel-default">
                                
                                <div style="background-color:#f5f5f5 !important ;" class="panel-body profile">
                                    <div class="profile-image">
                                        <img style="border-radius: 3%; height: 219px; width: 217px;" alt="${borrower.borrower_name}" src="${pageContext.request.contextPath}<%="/previewUserPhoto?fileName="+borrower_photo+"&id="+borrower_id+"&userType=Borrower"+"&gender="+gender+""%>">
                                    </div>
                                    <div class="profile-data">
                                        <div class="profile-data-name" style="color:#00b6f5 !important;font-family: Verdana; font-weight: bold;">${borrower.borrower_name}</div>
                                        <!-- <div style="color: #FFF;" class="profile-data-title">Singer-Songwriter</div> -->
                                    </div>
                                                                   
                                </div>                                
                               
                            </div>                            
                            
                        </div>
                        
                        
                        
                        <div class="col-md-9" style="margin-top: 10px;">

                            <!-- START TIMELINE -->
                            
                            <div class="timeline timeline-right">
                                
                                <!-- START TIMELINE ITEM -->
                                <div class="timeline-item timeline-main">
                                    <div class="timeline-date">Today</div>
                                </div>
                                <!-- END TIMELINE ITEM -->                                                  
                                
                                <!-- START TIMELINE ITEM -->
                                <div class="timeline-item timeline-item-right">
                                    <div class="timeline-item-info">Registration Details</div>
                                    <div class="timeline-item-icon"><span class="fa fa-info"></span></div>                                   
                                    <div class="timeline-item-content">
                                                                     
                                        <div class="timeline-body comments">
                                            
                                            <div class="comment-item">                                                
                                                <p class="comment-head">
                                                    <a href="#">Pan Number -</a> <span class="text-muted">${borrower.borrower_pan_id}</span>
                                                </p>
                                                <!-- <p>Thank you so much... I would like to meet you :)</p>
                                                <small class="text-muted">15min ago</small> -->
                                            </div>  
                                                                                      
                                            <div class="comment-item">                                                
                                                <p class="comment-head">
                                                    <a href="#">Email id - </a> <span class="text-muted">${borrower.borrower_emailid}</span>
                                                </p>                                               
                                            </div>  
                                            
                                            <div class="comment-item">                                                
                                                <p class="comment-head">
                                                    <a href="#">Contact No - </a> <span class="text-muted">${borrower.borrower_phno}</span>
                                                </p>                                               
                                            </div>  
                                            
                                            <div class="comment-item">                                                
                                                <p class="comment-head">
                                                    <a href="#">Gender -</a> <span class="text-muted">${borrower.borrower_gender}</span>
                                                </p>                                               
                                            </div> 
                                            
                                            <div class="comment-item">                                                
                                                <p class="comment-head">
                                                    <a href="#">Status -</a> <span class="text-muted">${borrower.borrower_status}</span>
                                                </p>                                               
                                            </div> 
                                            
                                            <div class="comment-item">                                                
                                                <p class="comment-head">
                                                    <a href="#">Joined on -</a> <span class="text-muted">${borrower.borrower_reg_time}</span>
                                                </p>                                               
                                            </div>                                            
                                           
                                        </div>
                                    </div>                                    
                                </div>                                       
                                <!-- END TIMELINE ITEM -->



									
								<!-- START TIMELINE ITEM -->
                                <div class="timeline-item timeline-item-right">
                                    <div class="timeline-item-info">Other Details</div>
                                    <div class="timeline-item-icon"><span class="fa fa-info"></span></div>                                   
                                    <div class="timeline-item-content">
                                        
                                                           
                                        <div class="timeline-body comments">
                                            
                                            <div class="comment-item">                                                
                                                <p class="comment-head">
                                                    <a href="#">Present Address -</a> <span class="text-muted">${borrower.borrower_present_address}</span>
                                                </p>                                               
                                            </div>  
                                                                                      
                                            <div class="comment-item">                                                
                                                <p class="comment-head">
                                                    <a href="#">Pincode - </a> <span class="text-muted">${borrower.borrower_present_pincode}</span>
                                                </p>                                               
                                            </div>  
                                            
                                            <div class="comment-item">                                                
                                                <p class="comment-head">
                                                    <a href="#">Permanent Address - </a> <span class="text-muted">${borrower.borrower_permanent_address}</span>
                                                </p>                                               
                                            </div>  
                                            
                                            <div class="comment-item">                                                
                                                <p class="comment-head">
                                                    <a href="#">Pincode -</a> <span class="text-muted">${borrower.borrower_permanent_pincode}</span>
                                                </p>                                               
                                            </div> 
                                            
                                            <div class="comment-item">                                                
                                                <p class="comment-head">
                                                    <a href="#">Date of Birth -</a> <span class="text-muted">${borrower.borrower_dob}</span>
                                                </p>                                               
                                            </div>
                                            
                                            <div class="comment-item">                                                
                                                <p class="comment-head">
                                                    <a href="#">Id Proof -</a> <span class="text-muted">${borrower.borrower_identity_id_type} - ${borrower.borrower_identity_card_id}</span>
                                                </p>                                               
                                            </div>                                            
                                           
                                        </div>
                                    </div>                                    
                                </div>       
                                <!-- END TIMELINE ITEM -->	
									
           
                                
                            </div>
                            <!-- END TIMELINE -->                            
                            
                            
                            
                        </div>
                        
                        
                        

                           <div class="col-md-12">

                            <!-- START PRIMARY PANEL -->
                            <div class="panel panel-primary" style="border-top:0px">
                                <div class="panel-heading ui-draggable-handle" style="padding:1px;">
                                    <h3 class="panel-title" style="line-height: 15px;padding-left: 2px;margin-bottom: 3px;">Documents</h3>
                                </div>
                                <div class="panel-body">
                                   
                                    	<div class="gallery" id="links">
                        
					                        <c:if test="${!empty documents}">
														                            
														                            
					                               <c:forEach items="${documents}" var="det">
					                               
						                                <c:set var="docName" value="${det.file_name}"/>
												        <% String docName = (String)pageContext.getAttribute("docName"); %>
					
								                            <a class="gallery-item" href="#" title="Music picture 2" data-gallery>
								                                
								                                <div class="image">
								                                    <img src="${pageContext.request.contextPath}<%="/viewAttachedDocByOther?fileName="+docName+"&userId="+borrower_id+"&userType=Borrower"%>"  height="120px" width="142px;" alt="Music picture 2"/>    
								                                    <ul class="gallery-item-controls">
								                                        <li title="Zoom the document" onclick="viewUserDocument('<%=docName%>','<%=borrower_id%>','Borrower')"><label class="check"><span class="glyphicon glyphicon-zoom-in"></span></label></li>
								                                        <li title="Download the document" onclick="downloadUserDocument('<%=docName%>','<%=borrower_id%>','Borrower')"><label class="check"><span class="glyphicon glyphicon-download"></span></label></li>
								                                    </ul>                                                                    
								                                </div>
								                                
								                                <div class="meta">
								                                    <strong>${det.document_type}</strong>	                                   
								                                </div> 
								                                                               
								                            </a>                            
															
												     </c:forEach>
												
											 </c:if>
					
					
					                       </div>
                        
                                </div>                                                           
                            </div>
                            <!-- END PRIMARY PANEL -->

                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                    </div>
                   
</body>
</html>