<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Setting</title>

 <!-- META SECTION -->
           
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="resources/adminResources/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->     
        
</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>




<body onload="activeSideMenu('menu7',3)">


 <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           
           
            <!-- START PAGE SIDEBAR -->
              
              <%@include file="controller_side_bar.jsp" %>
            
            <!-- END PAGE SIDEBAR -->
            
            
            
            
            <!-- PAGE CONTENT -->
            
            <div class="page-content" id="contentDiv">
               
               
               
                
                <!-- START X-NAVIGATION VERTICAL -->
                    
                    <%@include file="controller_header_bar.jsp" %>
                
                <!-- END X-NAVIGATION VERTICAL -->                     




                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Controller</a></li>                    
                    <li >Setting</li>
                    <li class="active"></li>
                </ul>
                <!-- END BREADCRUMB -->                       
                
                
                
                 <div class="page-title">                    
                    <h2><span class="fa fa-gear"></span> Setting  <small></small></h2>
                </div>
                
                
                <!-- PAGE CONTENT WRAPPER -->
                
                
                <div class="page-content-wrap">
                    
                   
                  
                   		<div class="row">
                        <div class="col-md-12">
                            
                            <div class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-heading ui-draggable-handle">
                                    <h3 class="panel-title"><strong>Change Password</strong></h3>
                                    
                                </div>
                                <div class="panel-body">
                                    <p>Please enter all the respective option to get the result.</p>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Current Password</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="password" placeholder="Enter Current Password" id="curPasswordId" class="form-control">
                                            </div>                                            
                                            <span class="help-block" id="curErrorId"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">New Password</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="password" placeholder="Enter New Password" id="newPasswordId" class="form-control">
                                            </div>                                            
                                            <span class="help-block" id="newErrorId"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Confirm Password</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="password" placeholder="Enter Confirm Password" id="cnfPasswordId" class="form-control">
                                            </div>                                            
                                            <span class="help-block" id="cnfErrorId"></span>
                                        </div>
                                    </div>
                                    

                                </div>
                                <div class="panel-footer">                                                               
                                    <button class="btn btn-primary pull-right" onclick="saveControllerPassword()">Change<span class="fa fa-save fa-right"></span></button>
                                </div>
                            </div>
                            </div>
                            
                        </div>
                    </div>
                    
                   
                   
                    
                   
                   <div class="row">
                        <div class="col-md-12">
                            
                            <div class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-heading ui-draggable-handle">
                                    <h3 class="panel-title"><strong>Optional Settings</strong></h3>
                                    
                                </div>
                                <div class="panel-body">
                                    <p>Please enter all the respective option to get the result.</p>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                    
                                   <form id="otherDetailsForm" >
                                   
			                                   <c:forEach items="${controllerData}" var="det">	
			                                   
			                                     <div class="form-group">
			                                        <label class="col-md-3 col-xs-12 control-label">Controller Name</label>
			                                        <div class="col-md-6 col-xs-12">                                            
			                                            <div class="input-group">
			                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
			                                                <input type="text" value="${det.controller_name}" placeholder="Enter Admin Name" name="name" id="nameId" class="form-control">
			                                            </div>                                            
			                                            <span class="help-block" id="nameErrorId"></span>
			                                        </div>
			                                    </div>
			                                    
			                                    <div class="form-group">
			                                        <label class="col-md-3 col-xs-12 control-label">Controller Role</label>
			                                        <div class="col-md-6 col-xs-12">                                            
			                                            <div class="input-group">
			                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
			                                                <input type="text" value="${det.controller_designation}" placeholder="Enter Admin Role" name="role" id="roleId" class="form-control">
			                                            </div>                                            
			                                            <span class="help-block" id="roleErrorId"></span>
			                                        </div>
			                                    </div>
			                                    
			                                    
			                                    <div class="form-group">
			                                        <label class="col-md-3 col-xs-12 control-label">Controller Photo</label>
			                                        <div class="col-md-6 col-xs-12">                                            
			                                            <div class="input-group">
			                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
			                                                <input class="form-control" type="file" name="file" id="file" >
			                                                
			                                            </div>                                            
			                                            <span class="help-block" id="fileErrorId"></span>
			                                        </div>
			                                    </div>
			                                   
			                                   
			                                   
			                                   </c:forEach>
                                   </form> 

                                </div>
                                <div class="panel-footer">                                                               
                                    <button class="btn btn-primary pull-right" onclick="saveControllerProfile()">Change<span class="fa fa-save fa-right"></span></button>
                                </div>
                            </div>
                            </div>
                            
                        </div>
                    </div>
                    
                   
                   
                   
                   
                   
                   
                   
                   
                   
			        <!-- FOOTER CONTENT -->
       
			         <%@include file="admin_footer.jsp" %>
			       
			       <!-- END FOOTER CONTENT -->
                    
                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            
            
            </div>            
            <!-- END PAGE CONTENT -->
       
        </div>
        <!-- END PAGE CONTAINER -->

       
       
       
       
       
       
       <!-- BELOW COMMON CONTENT -->
       
         <%@include file="controller_down_common.jsp" %>
       
       <!-- END BELOW COMMON CONTENT -->
       
	
    </body>
</html>