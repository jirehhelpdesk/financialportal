<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>



<script type="text/javascript" src="resources/paging/pageingScript.js"></script> 

</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<body>


<c:if test="${!empty lenderNotification}">	


                   <div style="width:100%;"  class="col-md-8">
                
                		
                		<div class="card">
                            
                            <div class="header">
                                <h4 class="title">Notification</h4>
                            </div>
                            
                            <div id="pagination"></div>
                            
                            <div class="content table-responsive table-full-width">
                                
                                <table class="table table-hover" role="grid" id="tablepaging"  >
                                    
                                    
                                    <thead>
                                        
                                        <tr>
											<th>Sender</th>
											<th>Subject</th>
											<th>Sent Date</th>
											
											<th>Manage</th>
										</tr>
                                    
                                    
                                    </thead>
                                    
                                    
                                    <tbody id="myTableBody">
                                        
                                         <c:forEach items="${lenderNotification}" var="det">		
										     
										     
										     <c:if test="${det.notify_status eq 'Pending'}">
										     		
										     		<tr>
	
														<td style="font-weight:bold;;color:#333;">${det.notify_sender_type}</td>
														<td style="font-weight:bold;;color:#333;">${det.notify_subject}</td>
														<td style="font-weight:bold;;color:#333;"><fmt:formatDate pattern="dd/MM/yyyy" value="${det.notify_date}" /></td>
														
														<td style="font-weight:bold;;color:#333;"><a href="#" style="color:green;float:left;" onclick="manageNotify('${det.notify_id}','View','lenderNotification')">View</a>  <a href="#" style="color:red;float:left;margin-left:20px;" onclick="manageNotify('${det.notify_id}','Delete','lenderNotification')">Delete</a>
														</td>
		                                            
													</tr>
										     
										     </c:if>
										     
										     <c:if test="${det.notify_status ne 'Pending'}">
										     		
										     		<tr>
	
														<td>${det.notify_sender_type}</td>
														<td>${det.notify_subject}</td>
														<td><fmt:formatDate pattern="dd/MM/yyyy" value="${det.notify_date}" /></td>
														
														<td style="font-weight:bold;;color:#333;"><a href="#" style="color:green;float:left;" onclick="manageNotify('${det.notify_id}','View','lenderNotification')">View</a>  <a href="#" style="color:red;float:left;margin-left:20px;" onclick="manageNotify('${det.notify_id}','Delete','lenderNotification')">Delete</a>
														</td>
		                                            
													</tr>
										     
										     </c:if>
										     
												
												
										</c:forEach>
                                        
                                    </tbody>
                                    
                                    
                                </table>
                                
                                <div id="pageNavPosition" style="padding-top: 20px;float: right;" align="center">
								</div>
								<script type="text/javascript">
								
								var pager = new Pager('tablepaging', 10);
								pager.init();
								pager.showPageNav('pager', 'pageNavPosition');
								pager.showPage(1);
								</script>
                              
                            </div>
                            
                            
                        </div>
                        
                        
                        </div>
               
</c:if>
  
<c:if test="${empty lenderNotification}">	

 <div style="width:100%;"  class="col-md-8">
    
    <div class="card">
		
		 <div class="header">
             <h4 class="title">Borrower Request</h4>
         </div>
                            
                            
		<div class="content table-responsive table-full-width">
		     <p style="margin-left:16px;">As of now you didn't get any request from 'Borrower'.</p>
		</div>         
		                
	</div>
																	
 </div>
 
</c:if>                
                
</body>
</html>