


 <!-- Start slider section -->  
 	
 	<script src="resources/indexResources/index_slider/js/jssor.slider-21.1.6.min.js" type="text/javascript"></script>
    <script src="resources/indexResources/index_slider/slider.js" type="text/javascript"></script>
    
    
     <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1300px; height:435.396px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('resources/indexResources/index_slider/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        
        
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1300px; height:435px; overflow: hidden;">
           
            
            <div data-p="225.00">
                
                <img data-u="image" src="resources/indexResources/index_slider/img/funding.jpg" />
                
                <div style="position: absolute; top: 30px; left: 30px; width:auto; height: 120px; font-size:34px; color: #00b6f5; line-height: 60px;">Get the key of funding</div>
                
                <div style="position: absolute; top: 300px; left: 30px; width:auto; height: 120px; font-size: 30px; color: #ffffff; line-height: 38px;">Unlock the door of opportunity</div>
                
                <!-- <div data-u="caption" data-t="0" style="position: absolute; top: 120px; left: 650px; width: 470px; height: 220px;">
                    <div style="position: absolute; top: 4px; left: 45px; width: 379px; height: 213px; overflow: hidden;">
                    </div>
                </div> -->
            </div>
            
            <div data-p="225.00" style="display: none;">
                <img data-u="image" src="resources/indexResources/index_slider/img/slide_7.png" />
                
                <div style="position: absolute; top:188px; left:100px; width:auto; height: 120px; font-size: 50px; color: #00b6f5; line-height: 60px;">We provide all kind of loans</div>
                
                <div style="position: absolute; top: 300px; left:106px; width:auto; height: 120px; font-size: 30px; color: #ffffff; line-height: 38px;">Connect with us and grow your future</div>
                
                <!-- <div data-u="caption" data-t="0" style="position: absolute; top: 120px; left: 650px; width: 470px; height: 220px;">
                    <div style="position: absolute; top: 4px; left: 45px; width: 379px; height: 213px; overflow: hidden;">
                    </div>
                </div> -->
                
            </div>
            
            
            <div data-p="225.00" data-po="80% 55%" style="display: none;">
                
                <img data-u="image" src="resources/indexResources/index_slider/img/slider_4.jpg" />
                
                <div style="position: absolute; top:-5px; left: 30px; width:auto; height: 120px; font-size:34px; color: #fff; line-height: 60px;">We will provide the funding for you</div>
                
                <div style="position: absolute;top: 363px; left: 30px; width:auto; height: 120px; font-size:30px; color: #fff; line-height: 38px;">Direct communication between lender and borrower</div>
                
                <!-- <div data-u="caption" data-t="0" style="position: absolute; top: 120px; left: 650px; width: 470px; height: 220px;">
                    <div style="position: absolute; top: 4px; left: 45px; width: 379px; height: 213px; overflow: hidden;">
                    </div>
                </div> -->
                
            </div>
            
            <a data-u="any" href="http://www.jssor.com" style="display:none">Full Width Slider</a>
            
        </div>
        
        
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb05" style="bottom:22px;right:16px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype" style="width:16px;height:16px;"></div>
        </div>
        
        
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora22l" style="top:0px;left:8px;width:40px;height:58px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora22r" style="top:0px;right:8px;width:40px;height:58px;" data-autocenter="2"></span>
    
    
    </div>
   
    <script type="text/javascript">jssor_1_slider_init();</script>
    
    <!-- #endregion Jssor Slider End -->
 		
 <!-- End slider section -->

