<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script type="text/javascript" src="resources/paging/pageingScript.js"></script> 

</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>


<body>

<c:if test="${!empty dealDetails}">	


<div class="table-responsive">
                                        
                                        
       
       <table id="tablepaging" class="table table-bordered table-striped table-actions">
           
          
           <thead>
               <tr>
                   <th width="50">Sl No</th>
                   <th  width="100">Name</th>
                   <th width="100">Email Id</th>
                   <th width="100">Ph No</th>
                   <th width="100">Policy Type</th>
                   <th width="100">Policy Name</th>
                   <th width="100">Finance Amount</th>
                   <th width="100">Finance Duration</th>
                 
                   <th width="100">Deal Status</th>
                   <th width="100">Manage</th>
               </tr>
           </thead>
           
           
           <tbody id="myTableBody">                                            
              
              <%int i=1; %> 
             
              <c:forEach items="${dealDetails}" var="det">	    
              
                <tr id="trow_1">
                    <td><%=i++%></td>
                    <td><strong>${det.borrower_name}</strong></td>
                    <td>${det.borrower_emailid}</td>
                    <td>${det.borrower_phno}</td>
                    <td>${det.policy_type}</td>
                    <td>${det.policy_name}</td>
                    <td>Rs. ${det.deal_amount}</td>
                    <td>${det.deal_duration} months</td>
                    
                    <c:if test="${det.deal_status == 'Pending'}">
                    
                    	<td><span class="label alert-warning" style=" font-size: 14px;">${det.deal_status}</span></td>
                    
                    </c:if>
                    
                    <c:if test="${det.deal_status == 'Processing'}">
                    
                    	<td><span class="label alert-warning" style=" font-size: 14px;">${det.deal_status}</span></td>
                    
                    </c:if>
                    
                    <c:if test="${det.deal_status == 'Onhold'}">
                    
                    	<td><span class="label alert-warning" style=" font-size: 14px;">${det.deal_status}</span></td>
                    
                    </c:if>
                    
                    <c:if test="${det.deal_status == 'Approved'}">
                  
                    	<td><span class="label label-success" style=" font-size: 14px;">${det.deal_status}</span></td>
                    
                    </c:if>
                    
                    <c:if test="${det.deal_status == 'Rejected'}">
                    
                    	<td><span class="label alert-danger" style=" font-size: 14px;">${det.deal_status}</span></td>
                    
                    </c:if>
                    
                    
                    
                    <td>
                        <button class="btn btn-info" type="button" style=" border-radius: 20px;" onclick="manageBorrowerRequest('${det.deal_id}')">View Details</button>
                    </td>
                    
                </tr>
                
                
               </c:forEach>
               
           </tbody>
       </table>
       
       
       <div id="pageNavPosition" style="padding-top: 20px;float: right;" align="center"></div>
		<script type="text/javascript">
		
		var pager = new Pager('tablepaging', 10);
		pager.init();
		pager.showPageNav('pager', 'pageNavPosition');
		pager.showPage(1);
		
		</script>
                              
  </div>      
                                    
                                    
                                   
 </c:if>
    
    
    <c:if test="${empty dealDetails}">	
    
    
			   <div class="col-md-12" style="margin-top:20px;">

                                                     
                            <div class="messages messages-img" >
                                
                                <div class="item item-visible">
                                                                    
                                    <div class="text customMsgText">
                                        <div class="heading">
                                            <a href="#"></a>
                                            <span class="date"></span>
                                        </div>                                    
                                        As of now there is no deal found in pending the portal.
                                    </div>
                                </div>
                            </div>

                        </div>
    
    </c:if>                                           
</body>
</html>