


<% String requiredDoc = (String)request.getAttribute("requiredDocument"); %>
<%String docListArray[] = requiredDoc.split(","); %>

	<div class="header">
		<h4 class="formSubHeading title">Attach Documents</h4>
	</div>


	<div class="col-md-12">
		<div class="card">

			<div class="content table-responsive table-full-width">

				<table class="table table-hover" role="grid" id="tableData">
					<thead>
						<tr>
							<th>Document name</th>
							<th>Available</th>
							<th></th>
						</tr>
					</thead>

					<tbody>

						<%for(int d=0;d<docListArray.length;d++){ %>

						<%String docStatus[] = docListArray[d].split("-"); %>

						<%if(docStatus[1].equals("Yes")){ %>
						<tr>
							<td><%=docStatus[0]%></td>
							<td >Yes</td>
							<td><span class="btn btn-info"  onclick="viewAddedDoc('<%=docStatus[0]%>')">View</span></td>
						</tr>
						<%}else{ %>
						<tr>
							<td><%=docStatus[0]%></td>
							<td >No</td>
							<td>
							    <span class="btn btn-info"  onclick="attachDocument('<%=docStatus[0]%>')">Add Document</span>
							
							</td>
						</tr>
						<%} %>
						<%} %>

					</tbody>

				</table>

			</div>
		</div>
	</div>

 <input type="hidden" value="<%=requiredDoc%>" name="requiredDoc" id="requiredDoc" />
 
 

<!-- The Modal -->



