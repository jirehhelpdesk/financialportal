<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<style>
/* 
body {
  width: 100%;
  height: 100%;
  font-size: 16px;
}

body {
  background: #111;
  background: -webkit-radial-gradient(#111111, #000000);
  background: radial-gradient(#111111, #000000);
  -webkit-filter: url("#gooey");
          filter: url("#gooey");
}
 */
 
.blob {
  width: 2rem;
  height: 2rem;
 /*  background: #E5E5E5; */
   background: #1caf9a;
  border-radius: 50%;
  position: absolute;
  left: calc(50% - 1rem);
  top: calc(50% - 1rem);
  box-shadow: 0 0 1rem rgba(255, 255, 255, 0.15);
}

.blob-2 {
  -webkit-animation: animate-to-2 1.5s infinite;
          animation: animate-to-2 1.5s infinite;
}

.blob-3 {
  -webkit-animation: animate-to-3 1.5s infinite;
          animation: animate-to-3 1.5s infinite;
}

.blob-1 {
  -webkit-animation: animate-to-1 1.5s infinite;
          animation: animate-to-1 1.5s infinite;
}

.blob-4 {
  -webkit-animation: animate-to-4 1.5s infinite;
          animation: animate-to-4 1.5s infinite;
}

.blob-0 {
  -webkit-animation: animate-to-0 1.5s infinite;
          animation: animate-to-0 1.5s infinite;
}

.blob-5 {
  -webkit-animation: animate-to-5 1.5s infinite;
          animation: animate-to-5 1.5s infinite;
}

@-webkit-keyframes animate-to-2 {
  25%,
    75% {
    -webkit-transform: translateX(-1.5rem) scale(0.75);
            transform: translateX(-1.5rem) scale(0.75);
  }
  95% {
    -webkit-transform: translateX(0rem) scale(1);
            transform: translateX(0rem) scale(1);
  }
}

@keyframes animate-to-2 {
  25%,
    75% {
    -webkit-transform: translateX(-1.5rem) scale(0.75);
            transform: translateX(-1.5rem) scale(0.75);
  }
  95% {
    -webkit-transform: translateX(0rem) scale(1);
            transform: translateX(0rem) scale(1);
  }
}
@-webkit-keyframes animate-to-3 {
  25%,
    75% {
    -webkit-transform: translateX(1.5rem) scale(0.75);
            transform: translateX(1.5rem) scale(0.75);
  }
  95% {
    -webkit-transform: translateX(0rem) scale(1);
            transform: translateX(0rem) scale(1);
  }
}
@keyframes animate-to-3 {
  25%,
    75% {
    -webkit-transform: translateX(1.5rem) scale(0.75);
            transform: translateX(1.5rem) scale(0.75);
  }
  95% {
    -webkit-transform: translateX(0rem) scale(1);
            transform: translateX(0rem) scale(1);
  }
}
@-webkit-keyframes animate-to-1 {
  25% {
    -webkit-transform: translateX(-1.5rem) scale(0.75);
            transform: translateX(-1.5rem) scale(0.75);
  }
  50%,
    75% {
    -webkit-transform: translateX(-4.5rem) scale(0.6);
            transform: translateX(-4.5rem) scale(0.6);
  }
  95% {
    -webkit-transform: translateX(0rem) scale(1);
            transform: translateX(0rem) scale(1);
  }
}
@keyframes animate-to-1 {
  25% {
    -webkit-transform: translateX(-1.5rem) scale(0.75);
            transform: translateX(-1.5rem) scale(0.75);
  }
  50%,
    75% {
    -webkit-transform: translateX(-4.5rem) scale(0.6);
            transform: translateX(-4.5rem) scale(0.6);
  }
  95% {
    -webkit-transform: translateX(0rem) scale(1);
            transform: translateX(0rem) scale(1);
  }
}
@-webkit-keyframes animate-to-4 {
  25% {
    -webkit-transform: translateX(1.5rem) scale(0.75);
            transform: translateX(1.5rem) scale(0.75);
  }
  50%,
    75% {
    -webkit-transform: translateX(4.5rem) scale(0.6);
            transform: translateX(4.5rem) scale(0.6);
  }
  95% {
    -webkit-transform: translateX(0rem) scale(1);
            transform: translateX(0rem) scale(1);
  }
}
@keyframes animate-to-4 {
  25% {
    -webkit-transform: translateX(1.5rem) scale(0.75);
            transform: translateX(1.5rem) scale(0.75);
  }
  50%,
    75% {
    -webkit-transform: translateX(4.5rem) scale(0.6);
            transform: translateX(4.5rem) scale(0.6);
  }
  95% {
    -webkit-transform: translateX(0rem) scale(1);
            transform: translateX(0rem) scale(1);
  }
}
@-webkit-keyframes animate-to-0 {
  25% {
    -webkit-transform: translateX(-1.5rem) scale(0.75);
            transform: translateX(-1.5rem) scale(0.75);
  }
  50% {
    -webkit-transform: translateX(-4.5rem) scale(0.6);
            transform: translateX(-4.5rem) scale(0.6);
  }
  75% {
    -webkit-transform: translateX(-7.5rem) scale(0.5);
            transform: translateX(-7.5rem) scale(0.5);
  }
  95% {
    -webkit-transform: translateX(0rem) scale(1);
            transform: translateX(0rem) scale(1);
  }
}
@keyframes animate-to-0 {
  25% {
    -webkit-transform: translateX(-1.5rem) scale(0.75);
            transform: translateX(-1.5rem) scale(0.75);
  }
  50% {
    -webkit-transform: translateX(-4.5rem) scale(0.6);
            transform: translateX(-4.5rem) scale(0.6);
  }
  75% {
    -webkit-transform: translateX(-7.5rem) scale(0.5);
            transform: translateX(-7.5rem) scale(0.5);
  }
  95% {
    -webkit-transform: translateX(0rem) scale(1);
            transform: translateX(0rem) scale(1);
  }
}
@-webkit-keyframes animate-to-5 {
  25% {
    -webkit-transform: translateX(1.5rem) scale(0.75);
            transform: translateX(1.5rem) scale(0.75);
  }
  50% {
    -webkit-transform: translateX(4.5rem) scale(0.6);
            transform: translateX(4.5rem) scale(0.6);
  }
  75% {
    -webkit-transform: translateX(7.5rem) scale(0.5);
            transform: translateX(7.5rem) scale(0.5);
  }
  95% {
    -webkit-transform: translateX(0rem) scale(1);
            transform: translateX(0rem) scale(1);
  }
}
@keyframes animate-to-5 {
  25% {
    -webkit-transform: translateX(1.5rem) scale(0.75);
            transform: translateX(1.5rem) scale(0.75);
  }
  50% {
    -webkit-transform: translateX(4.5rem) scale(0.6);
            transform: translateX(4.5rem) scale(0.6);
  }
  75% {
    -webkit-transform: translateX(7.5rem) scale(0.5);
            transform: translateX(7.5rem) scale(0.5);
  }
  95% {
    -webkit-transform: translateX(0rem) scale(1);
            transform: translateX(0rem) scale(1);
  }
}


</style>
</head>
<body>


<svg xmlns="http://www.w3.org/2000/svg" version="1.1">
  <defs>
    <filter id="gooey">
      <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur"></feGaussianBlur>
      <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo"></feColorMatrix>
      <feBlend in="SourceGraphic" in2="goo"></feBlend>
    </filter>
  </defs>
</svg>
<div class="blob blob-0"></div>
<div class="blob blob-1"></div>
<div class="blob blob-2"></div>
<div class="blob blob-3"></div>
<div class="blob blob-4"></div>
<div class="blob blob-5"></div>


</body>
</html>