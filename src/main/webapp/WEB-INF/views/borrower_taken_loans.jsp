<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Taken Policy</title>

    <link href="resources/borrower_lender_resources/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="resources/borrower_lender_resources/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="resources/borrower_lender_resources/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="resources/borrower_lender_resources/assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="resources/borrower_lender_resources/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    
    
    <link href="resources/borrower_lender_resources/profile_theme_style.css" rel="stylesheet" >
    <link href="resources/borrower_lender_resources/profile_theme_style1.css" rel="stylesheet" >
    
    
    
	<link rel="stylesheet" href="resources/paging/other.css">
    
    <script type="text/javascript" src="resources/paging/pageingScript.js"></script> 
    
    
</head>


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<body onload="activeSideMenu('menu4')">


<div class="wrapper">
    
    
      <!-- Start of Sidemenu Division -->
    
         <%@include file="borrower_sidemenu.jsp" %>

      <!-- End of Sidemenu Division -->


    <div class="main-panel">
        
        
        
         <!-- Start of Header Division -->
         
         <%@include file="borrower_header.jsp" %>
        
		 <!-- End of Header Division -->

       
        <div class="content">
           
            <div class="container-fluid">
                
                
				
				
				
				<div class="row">
                
                   <div style="width:100%;" class="col-md-8">
                
                		<div class="card">
                           
                            <div class="header">
                                <h4 class="title">My Taken Policy</h4>
                            </div>
                            
                            
                            <div class="content table-responsive table-full-width">
                           
                             
                             <c:if test="${!empty policyDealBean}">	
                                
                                <table  class="table table-hover" role="grid" id="tablepaging"  >
                                    
                                    <thead>
                                        <tr>
                                        
                                        <th>Sl No</th>
                                    	<th>Policy Type</th>
                                    	<th>Amount</th>
                                    	<th>Duration</th>
                                    	<th>Initiated Date</th>
                                    	<th>Lender Status</th>
                                    	
                                    	<th>View Details</th>
                                    	
                                    </tr></thead>
                                    
                                    <tbody id="myTableBody">
                                       
                                       <%int i=1; %> 
               						   <c:forEach items="${policyDealBean}" var="det">	
                        
                                        <tr>
                                        
                                        	<td><%=i++%></td>
                                        	<td>${det.policy_type}</td>
                                        	<td>Rs. ${det.deal_amount}</td>
                                        	<td>${det.deal_duration} months</td>
                                        	<td><fmt:formatDate pattern="dd/MM/yyyy" value="${det.deal_initiated_date}" /></td>
                                        	<td>${det.lender_status}</td>
                                        	
                                        	<td><button class="btn btn-info btn-fill pull-left" onclick="viewAppliedPolicy('${det.deal_id}')">More</button></td>
                                        	
                                        </tr>
                                        
                                       
                                       </c:forEach>
                                        
                                    </tbody>
                                </table>
								
								
								 <div id="pageNavPosition" style="padding-top: 20px;float: right;" align="center">
								</div>
								<script type="text/javascript">
								
								var pager = new Pager('tablepaging', 10);
								pager.init();
								pager.showPageNav('pager', 'pageNavPosition');
								pager.showPage(1);
								</script>
								
							</c:if>

						    <c:if test="${empty policyDealBean}">	
						    
						    	<div class="tableNolistMessage alert alert-info alert-with-icon">As of now you didn't applied for any finance policy, Go to the 'Search Policy' tab search and apply for any finance as per the need !</div>
						    
						    </c:if>
                           
                           
                           </div>
                           
                        </div>
                        
                        
                        </div>
                </div>
                
                
                
                
                <div class="row" id="dealInfo" style="display:none;margin-top:10px;">
				
				     <%@include file="borrower_applied_policy_details.jsp" %>
				  
				</div> 
				
				
				
                
                
                
             </div>
        </div>


       <!-- Start of Footer Division -->
 
         <%@include file="borrower_footer.jsp" %>
        
	   <!-- Start of Footer Division -->
	   
	   
    </div>
</div>


</body>

      <!-- Start of Common Division -->
 
         <%@include file="common_content.jsp" %>
        
	   <!-- Start of Common Division -->   
   
	
</html>
