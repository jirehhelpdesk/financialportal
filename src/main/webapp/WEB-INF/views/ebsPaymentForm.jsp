<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>E-Billing Solutions Pvt Ltd - Payment Page</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    
    
     
    <style type="text/css">
	    h1       { font-family:'Gentium Book Basic'; font-size:24pt; color:#08185A; font-weight:100; margin-bottom:0.1em}
        h2.co    { font-family:Arial,sans-serif; font-size:24pt; color:#FFFFFF; margin-top:0.1em; margin-bottom:0.1em; font-weight:100}
        h3.co    { font-family:Arial,sans-serif; font-size:16pt; color:#000000; margin-top:0.1em; margin-bottom:0.1em; font-weight:100}
        h3       { font-family:Arial,sans-serif; font-size:30pt; color:#08185A; }
        body     { font-family:Gentium Book Basic; font-size:40pt; color:#08185A; margin-top:0px;}
	    th 		 { font-size:15px;background:#008080;color:#FFFFFF;font-weight:bold;height:30px;}
	    td 		 { font-size:15px;}
	    .pageTitle { font-size:24px;}
        #mode {
            width: 126px;
        }

    </style>


  <script language="JavaScript" type="text/javascript">
        function a2()
        {
            document.getElementById('a1').style.display = "none";
        }
        
        function checkit() {

            var value = document.getElementById('channel').value;
            {
            if (value === '0') {
                document.getElementById('a1').style.display = "none";
            }
            else {
                document.getElementById('a1').style.display = "inline";
               
            }
        }
      }
  </script>
 

 
       
			

</head>

<body onload="a2()" style="background-color:lightgray; margin-left:300px; margin-right:300px; margin-top:0px;">
    
    
    <center>
        
        <div style="background-color:white;">
              
              
        <form action="userPayOnline.jsp" method="post" name="frm" id="theForm" autocomplete = "off" >
            
               <input type="hidden" name="V3URL" value="https://secure.ebs.in/pg/ma/payment/request" />
        <div>
            
            <h1>EBS - JSP Version 3</h1>
            
            <table width="700" style="text-align:left; ">

                 <tr>
                    <th colspan="2">Request Details</th>
                </tr>
                
                <tr>
				      <td>Account Id<span class="red">*</span></td>
				      <td align="left"><input type="text" id="account_id" name="account_id"/></td>
				</tr>
        
                <tr> 
                    <td>Channel<span class="red">*</span></td>
                    <td align="left">
                        <select id="channel" name="channel" onchange ="checkit()">
                            <option value="0">Standard</option>
                            <option value="2">Direct</option>
                        </select>
                    </td>
                </tr>
        
              <tr>
                    <td class="fieldName" width="50%">Currency<span class="red">*</span></td>
                    <td align="left" width="50%"> <select id="currency" name="currency">
                            <option value="INR">INR</option>
                            <option value="GBP">GBP</option>
                         <option value="EUR">EUR</option>
                            <option value="USD">USD</option>
                        </select>
                    </td>
                </tr>
                
      <tr>
      <td>Return URL<span class="red">*</span></td>
      <td align="left"><input type="text" id="return_url" value="https://www.facebook.com" name="return_url"/></td>
      </tr>
                
      <tr>
         <td>Mode<span class="red">*</span></td>
         <td align="left"><input id="mode" name="mode" type="text" value="TEST" readonly="true"/></td>
      </tr>

      <tr>
      
		    <td class="fieldName">Secure Hash Algorithm<span class="red">*</span></td>
		    
		    <td align="left"> <select id="algo" name="algo">
		      <option value="MD5">MD5</option>
		      <option value="SHA1">SHA1</option>    
		      <option value="SHA512">SHA512</option>
		    </select></td>
		    
      </tr>
      
      <tr>
          <th colspan="2">Transaction Details</th>
      </tr>
      
      <tr>
	      <td>Reference Number<span class="red">*</span></td>
	      <td align="left"><input type="text" id="reference_no" name="reference_no" value="a123456"/></td>
	  </tr>
	  
	  
      <tr>
 <td>Amount<span class="red">*</span></td>
      <td align="left"><input type="text" id="amount" name="amount" value="10"/></td>
      </tr>
         
                <tr>
                    <td>Description<span class="red">*</span></td>
                    <td align="left" width="50%"><input id="description" name="description" type="text" value="Test Transaction " /></td>
                </tr>
                <tr>
                    <th colspan="2">Billing Address</th>
                </tr>
                <tr>
                    <td>Name<span class="red">*</span></td>
                    <td align="left"><input id="name" name="name" type="text" value="a123456" /></td>
                </tr>
                <tr>
                    <td>Address<span class="red">*</span></td>
                    <td align="left"><input id="address" name="address" type="text" value="a123456" /></td>
                </tr>
                <tr>
                    <td>City<span class="red">*</span></td>
                    <td align="left"><input id="city" name="city" type="text" value="a123456"  /></td>
                </tr>
                <tr>
                    <td>State/Province<span class="red">*</span></td>
                    <td align="left"><input id="state" name="state" type="text" value="a123456"/></td>
                </tr>
                <tr>
                    <td>ZIP/Postal Code<span class="red">*</span></td>
                    <td align="left"><input id="postal_code" name="postal_code" type="text" value="123456" /></td>
                </tr>
                
                <tr>
                    <td>Country<span class="red">*</span></td>
                    <td align="left">
                        <select name="country" id="country">
                            <option value=""></option>
                            <option value="IND" selected="selected">India</option>                          
                        </select>
                    </td>
                </tr>
                               
                <tr>
                    <td>Email<span class="red">*</span></td>
                    <td align="left"><input id="email" name="email" type="text" value="abinash.raula@jirehsol.com" /></td>
                </tr>
                <tr>
                    <td>Telephone<span class="red">*</span></td>
                    <td align="left"><input id="phone" name="phone" type="text" value="8095035140"/></td>
                </tr>
                <tr>
                    <th colspan="2">Shipping Details</th>
                </tr>
             <tr>
                    <td>Ship Name<span class="red">*</span></td>
                    <td align="left"><input id="ship_name" name="ship_name" type="text" value="a123456" /></td>
                </tr>
                <tr>
                    <td>Ship Address<span class="red">*</span></td>
                    <td align="left"><input id="ship_address" name="ship_address" type="text" value="a123456"  /></td>
                </tr>
                <tr>
                    <td>Ship City<span class="red">*</span></td>
                    <td align="left"><input id="ship_city" name="ship_city" type="text" value="a123456"  /></td>
                </tr>
                <tr>
                    <td>Ship State<span class="red">*</span></td>
                    <td align="left"><input id="ship_state" name="ship_state" type="text" value="a123456" /></td>
                </tr>
                 <tr>
                    <td>Ship Country<span class="red">*</span></td>
                    <td align="left">
                        <select name="ship_country" id="ship_country">
                            <option value=""></option>
                            <option value="IND" selected="selected">India</option>
                        </select>
                    </td>
                </tr>

                  <tr>
                    <td>Ship Phone<span class="red">*</span></td>
                    <td align="left"><input id="ship_phone" name="ship_phone" type="text" value="9036007662" /></td>
                </tr>
                  <tr>
                    <td>Ship Postal Code<span class="red">*</span></td>
                    <td align="left"><input id="ship_postal_code" name="ship_postal_code" type="text" value="123456" /></td>
                </tr>
                     
                <tr>
                    <td valign="top" align="center" colspan="2">
                        <span class="red">*denotes required field</span>
                    </td>
                </tr>
                <tr>
                    <td valign="top" align="center" colspan="2">
                    
                    <input id="submit" name="submit" value="Submit" type="submit"/>&nbsp;
                    
                        <input value="Reset" type="reset" id="reset"/>
                        
                    </td>
                </tr>

   </table> 
        </div>
        

</form>
            </div>
        </center>     
</body>
</html>


