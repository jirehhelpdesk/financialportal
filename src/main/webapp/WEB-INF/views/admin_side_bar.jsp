<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>


<% String photo = "";%>
<% String name = "";%>
<% String role = "";%>


<c:forEach items="${adminData}" var="det">	
            
    <c:set var="photo" value="${det.admin_photo}"/>
    <%  photo += (String)pageContext.getAttribute("photo"); %>
    
    <c:set var="name" value="${det.admin_name}"/>
     <%  name += (String)pageContext.getAttribute("name"); %>
     
     <c:set var="role" value="${det.admin_designation}"/>
     <%  role += (String)pageContext.getAttribute("role"); %>
          
</c:forEach>


<body>

<!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="index.html">Admin</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                           
                           <img src="${pageContext.request.contextPath}<%="/adminPhoto?fileName="+photo+""%>"  alt="<%=name%>"/>
                           
                          <!--  <img src="resources/adminResources/assets/images/users/avatar.jpg" alt="John Doe"/> -->
                            
                            
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                
                                <img src="${pageContext.request.contextPath}<%="/adminPhoto?fileName="+photo+""%>"  alt="<%=name%>"/>
                               
                                <!-- <img src="resources/adminResources/assets/images/users/avatar.jpg" alt="John Doe"/> -->
                                
                                
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"><%=name%></div>
                                <div class="profile-data-title"><%=role%></div>
                            </div>
                            
                           <!--  <div class="profile-controls">
                                <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                                <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                            </div> -->
                            
                        </div>                                                                        
                    </li>
                    
                    <li class="xn-title">Navigation</li>
                    
                    
                    <li id="headerMenu1"><!-- class="active"  -->
                        <a href="adminHome"><span class="fa fa-desktop"></span> <span class="xn-text">Portal Status</span></a>                        
                    </li>                    
                    
                    
                    <li id="headerMenu2"><!-- class="xn-openable"  -->
                        <a href="borrowerInfo" ><span class="fa fa-users"></span><span class="xn-text">Borrower Management</span></a>
                       <!--  <ul>
                            <li id="headerMenu2_1"><a href="borrowerInfo"><span class="fa fa-user"></span> Borrower Information</a></li>
                            <li id="headerMenu2_2"><a href="manageBorrower"><span class="glyphicon glyphicon-user"></span> Manage Borrower</a></li>                                                   
                        </ul> -->
                    </li>
                    
                    
                    
                    <li id="headerMenu3">
                        <a href="lenderInfo"><span class="fa fa-users"></span><span class="xn-text">Lender Management</span></a>
                       <!--  <ul>
                            <li id="headerMenu3_1"><a href="lenderInfo"><span class="fa fa-user"></span>Lender Information</a></li>
                            <li id="headerMenu3_2"><a href="manageLender"><span class="glyphicon glyphicon-user"></span>Manage Lender</a></li>
                        </ul> -->
                    </li>
                    
                    
                    <!--  <li class="xn-title">Components</li> -->
                    
                    
                    <li id="headerMenu4">
                        <a href="createController"><span class="fa fa-user-md"></span><span class="xn-text">Controller Management</span></a>                        
                        <!-- <ul>
                            <li id="headerMenu4_1"><a href="createController"><span class="fa fa-male"></span>New Controller</a></li>                            
                            <li id="headerMenu4_2"><a href="manageController"><span class="fa fa-male"></span>Manage Controller</a></li>
                        </ul> -->
                    </li> 
                    
                    
                    <li class="xn-openable" id="headerMenu5">
                        <a href="#"> <span class="glyphicon glyphicon-asterisk"></span> <span class="xn-text">Finance Policy</span></a>                        
                        <ul>
                            <li id="headerMenu5_1"><a href="manageLoanType"><span class="fa fa-list-alt"></span>Manage Loan Type</a></li>                            
                            <li id="headerMenu5_2"><a href="approveLoans"><span class="fa fa-check-square-o"></span>Approve Loan</a></li>
                        </ul>
                    </li>                    
                    
                    
                    <li class="xn-openable" id="headerMenu6">
                        <a href="#"> <span class="glyphicon glyphicon-list-alt"></span> <span class="xn-text">Finance Reports</span></a>                        
                        <ul>
                            <li id="headerMenu6_1" ><a href="borrowerReport"><span class="fa fa-table"></span>Borrower Report</a></li>                            
                            <li id="headerMenu6_2"><a href="lenderReport"><span class="fa fa-table"></span>Lender Report</a></li>
                        </ul>
                    </li>                    
                    
                    
                    <li class="xn-openable" id="headerMenu7">
                        <a href="#"><span class="fa fa-gear"></span> <span class="xn-text">Admin Setting</span></a>
                        <ul>
                            <li id="headerMenu7_1"><a href="manageEmail"><span class="fa fa-mail-reply"></span>Failed Mails</a></li>
                            <li id="headerMenu7_2"><a href="manageSms"><span class="fa fa-mail-reply"></span> Failed SMS</a></li>
                            <li id="headerMenu7_3"><a href="otherSettings"><span class="fa fa-gears"></span> Setting</a></li>
                         </ul>
                    </li>
                    
                    
                    <!-- <li class="xn-openable">
                        <a href="tables.html"><span class="fa fa-table"></span> <span class="xn-text">Tables</span></a>
                        <ul>                            
                            <li><a href="table-basic.html"><span class="fa fa-align-justify"></span> Basic</a></li>
                            <li><a href="table-datatables.html"><span class="fa fa-sort-alpha-desc"></span> Data Tables</a></li>
                            <li><a href="table-export.html"><span class="fa fa-download"></span> Export Tables</a></li>                            
                        </ul>
                    </li>
                    
                    
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-bar-chart-o"></span> <span class="xn-text">Charts</span></a>
                        <ul>
                            <li><a href="charts-morris.html"><span class="xn-text">Morris</span></a></li>
                            <li><a href="charts-nvd3.html"><span class="xn-text">NVD3</span></a></li>
                            <li><a href="charts-rickshaw.html"><span class="xn-text">Rickshaw</span></a></li>
                            <li><a href="charts-other.html"><span class="xn-text">Other</span></a></li>
                        </ul>
                    </li>                    
                    
                    
                    <li>
                        <a href="maps.html"><span class="fa fa-map-marker"></span> <span class="xn-text">Maps</span></a>
                    </li>                    
                    
                    
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-sitemap"></span> <span class="xn-text">Navigation Levels</span></a>
                        <ul>                            
                            <li class="xn-openable">
                                <a href="#">Second Level</a>
                                <ul>
                                    <li class="xn-openable">
                                        <a href="#">Third Level</a>
                                        <ul>
                                            <li class="xn-openable">
                                                <a href="#">Fourth Level</a>
                                                <ul>
                                                    <li><a href="#">Fifth Level</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>                            
                        </ul>
                    </li> -->
                    
                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
</body>
</html>