<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Borrower Registration</title>

    <link rel="shortcut icon" type="image/icon" href="resources/indexResources/assets/images/favicon.ico"/>
    <!-- Font Awesome -->
    <link href="resources/indexResources/assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="resources/indexResources/assets/css/bootstrap.css" rel="stylesheet">
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="resources/indexResources/assets/css/slick.css"/> 
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="resources/indexResources/assets/css/jquery.fancybox.css" type="text/css" media="screen" /> 
    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="resources/indexResources/assets/css/animate.css"/>  
     <!-- Theme color -->
    <link id="switcher" href="resources/indexResources/assets/css/theme-color/default.css" rel="stylesheet">

    <!-- Main Style -->
    <link href="resources/indexResources/style.css" rel="stylesheet">

    <!-- Fonts -->
    <!-- Open Sans for body font -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Raleway for Title -->
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <!-- Pacifico for 404 page   -->
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    
    
    
    <!-- Registration Style  -->
    		
    	<!-- <link rel="stylesheet" href="resources/indexResources/index_registration_rsc/assets/bootstrap/css/bootstrap.min.css"> -->
        <link rel="stylesheet" href="resources/indexResources/index_registration_rsc/assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="resources/indexResources/index_registration_rsc/assets/css/form-elements.css">
        <link rel="stylesheet" href="resources/indexResources/index_registration_rsc/assets/css/style.css">

    <!-- EOF Registration Style -->
    
</head>



<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<body onload="activeIndexMenu('Menu6')">

     <%@include file="index_header.jsp" %>


  <!-- Start about section -->
 
  <section id="about" style="margin-top:0px ! important; margin-bottom: 46px;">
    
    <div class="container">
          
         <!-- Registration Block -->
      
      		
      <div class="top-content">
        	
            <div class="inner-bg">
               	        
               	                               
                    <div class="row">
                                   
                      
                                 <div class="col-sm-5">
		                        			
		                        		<div class="row">
					                        <div class="Heading-align">
					                           
					                            <h1 class="signBox-Heading"> Sign In</h1>
					                            
					                        </div>
					                    </div>    
		                    
			                            <div class="form-bottom" style="color:#00b6f5;font-size: 13px;padding: 25px 25px 18px;">
			                            		
			                            		<div class="form-group">
						                    		<label class="" for="form-username"><i class="fa fa-caret-right"></i> &nbsp; If you are already a register user then click on below.</label>
						                    		            	
						                        </div>
						                        
						                       <div class="row">
	                                    	
			                                        <div class="col-md-12">
			                                            <div class="form-group">
			                                                <label class="" for="form-first-name"></label>
								                        	<img src="resources/indexResources/assets/images/signInInfo.jpg" style="margin-left:48px;" />
			                                            </div>
			                                        </div>
			                                      
			                                    </div>
						                        
						                        <div class="form-group">
						                        	<button class="btn" style="margin-left:176px;margin-top:73px;" onclick="registerRedirect('borrowerLogin')">Sign In</button>
						                    	</div>
						                    	
			                            		
			                            		<div class="form-group"> </div>
			                            		
			                            </div>
		                        		 
		                        		 
		                       </div>
                        
                        
                        
                           <div class="col-sm-7">
                        	
                        	  <div class="form-box">
                        		
                        		
                        		
                        		<div class="row">
			                        <div class="Heading-align">
			                           
			                            <h1 class="signBox-Heading">Borrower Registration</h1>
			                            
			                        </div>
			                    </div>    
                    
	                            <div class="form-bottom signBox-BottomDiv">
				                    
				                    <form:form class="registration-form" id="borrowerRegForm" modelAttribute="borrowerRegDetails" method="post" action="saveBorrowerRegister" >
				                       
					                       <div class="row">
	                                    	
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                                <label class="" for="form-first-name">First name</label>
						                        	<form:input type="text" name="borrower_first_name" maxlength="50" path="borrower_first_name"  class="form-first-name form-control" id="form-first-name" onchange="checkOnEachBorrowerRegForm()" />
						                        	<div id="fnameErrId" class="errorStyle"></div>
	                                            </div>
	                                        </div>
	                                        
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                                <label class="" for="form-last-name">Last name</label>
						                        	<form:input type="text" name="borrower_last_name" maxlength="50" path="borrower_last_name" class="form-last-name form-control" id="form-last-name" onchange="checkOnEachBorrowerRegForm()" />
						                        	<div id="lnameErrId" class="errorStyle"></div>
	                                            </div>
	                                        </div>
	                                        
	                                      </div>
	                                      
	                                      <div class="row">
	                                    	
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                                <label class="" for="form-email">Email</label>
						                        	<form:input type="text" autocomplete="off" name="borrower_emailid" maxlength="50" path="borrower_emailid"  class="form-email form-control" id="form-email" onchange="checkOnEachBorrowerRegForm()" />
						                       		<div id="emailErrId" class="errorStyle"></div>
	                                            </div>
	                                        </div>
	                                        
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                                <label class="" for="form-mobileno" style="float:left;">Mobile No<small style="float:right;">&nbsp;OTP will send to the number to activate the account</small></label>
						                        	<form:input type="text" name="borrower_phno" maxlength="10" path="borrower_phno"  class="form-email form-control" id="form-mobile" onchange="checkOnEachBorrowerRegForm()" />
						                        	<div id="mobileErrId" class="errorStyle"></div>
	                                            </div>
	                                        </div>
	                                        
	                                      </div>
	                                      
	                                      
	                                      <div class="row">
	                                    	
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                            	
	                                            	<label class="" for="form-mobileno">PAN Id</label>
						                        	<form:input type="text" name="borrower_pan_id" maxlength="10" path="borrower_pan_id"  class="form-email form-control" id="form-panId" onchange="checkOnEachBorrowerRegForm()" />
						                        	<div id="panErrId" class="errorStyle"></div>
	                                                
	                                            </div>
	                                        </div>
	                                        
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                                
	                                                <label class="" for="form-email">Gender</label>
						                        	<form:select name="borrower_gender" maxlength="50" path="borrower_gender" class="form-email form-control" id="form-gender" onchange="checkOnEachBorrowerRegForm()" >
						                        					<option value="Select your gender">Select your gender</option>
						                        					<option value="Female">Female</option>
						                        					<option value="Male">Male</option>
						                        	</form:select>
						                       		<div id="genderErrId" class="errorStyle"></div>
						                       		
	                                            </div>
	                                        </div>
	                                        
	                                      </div>
				                    
				                          
				                          <div class="row">
	                                    	
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                                <label class="" for="form-password">Password</label>
						                        	<form:input type="password" autocomplete="off" name="borrower_password" maxlength="15" path="borrower_password"  class="form-email form-control" id="form-password" />
						                        	<div id="passwordErrId" class="errorStyle"></div>
	                                            </div>
	                                        </div>
	                                        
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                                <label class="" for="form-confirmpassword">Confirm Password</label>
						                        	<input type="password" autocomplete="off" name="form-confirmpassword"  maxlength="15"  class="form-email form-control" id="form-cnfPassword" onchange="checkOnEachBorrowerRegForm()" />
						                        	<div id="cnfPasswordErrId" class="errorStyle"></div>
	                                            </div>
	                                        </div>
	                                        
	                                    </div>
	                                      
	                                    <div class="row">
				                            
				                            <div class="col-md-12">
				                            
						                        <div class="form-group">
						                        	
						                        	<a href="#" style="font-weight: bold; font-size: 14px;"> <input type="checkbox"  name="form-confirmpassword" id="form-terms" onclick="checkOnEachBorrowerRegForm()" /> &nbsp; I/We agree with your terms and conditions.</a>
						                        	<div id="termsErrId" class="errorStyle"></div>
						                        </div>
						                        
				                            </div>
				                            
				                        </div>
				                        
				                    </form:form>
				                    	
				                    	<div class="row">
				                            
				                            <div class="col-md-12">
						                    	 
						                    	 <div class="form-group">
								                        	<button class="btn" style="margin-left:294px;margin-top:8px;" onclick="borrowerRegistration()">Sign Up</button>
								                 </div>
						                    
						                    </div>
						                    
				                        </div>
				                        
				                        
			                    </div>
                        	</div>
		                
                        </div>
                        
                        	
                        
                        
                    </div>
                    
                
            </div>
            
        </div>

       
      	 <!-- EOF of registration block  -->	
      
      
      
    </div>
  </section> 
  
  
  <!-- End about section -->



        <%@include file="index_fotter.jsp" %>


  <!-- initialize jQuery Library --> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <!-- Bootstrap -->
  <script src="resources/indexResources/assets/js/bootstrap.js"></script>
  <!-- Slick Slider -->
  <script type="text/javascript" src="resources/indexResources/assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="resources/indexResources/assets/js/waypoints.js"></script>
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.counterup.js"></script>
  <!-- mixit slider -->
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->        
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.fancybox.pack.js"></script>
  <!-- Wow animation -->
  <script type="text/javascript" src="resources/indexResources/assets/js/wow.js"></script> 

  <!-- Custom js -->
  <script type="text/javascript" src="resources/indexResources/assets/js/custom.js"></script>
    
   
   
  <script type="text/javascript" src="resources/indexResources/index_registration_script.js"></script> 
 
 
    
      <!-- Registration script -->
    
        <script src="resources/indexResources/index_registration_rsc/assets/js/jquery-1.11.1.min.js"></script>
        <script src="resources/indexResources/index_registration_rsc/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="resources/indexResources/index_registration_rsc/assets/js/scripts.js"></script>
     		
     <!-- EOF Registration script -->
     
      		
  </body>
</html>