<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

       <footer class="footer">
            
            <div class="container-fluid">
                <!-- <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="index">
                                Home
                            </a>
                        </li>
                        <li>
                            <a href="aboutUs">
                                Company
                            </a>
                        </li>
                        <li>
                            <a href="portfolio">
                                Portfolio
                            </a>
                        </li>
                        <li>
                            <a href="blog">
                               Blog
                            </a>
                        </li> 
                    </ul>
                </nav> -->
               <p class="copyright pull-right">
                    &copy; 2016 - All rights reserved & developed by<b style="color: #00b6f5; cursor: pointer; float: right;" onclick="goToSite()">Jireh</b>
			<!-- , made with love for a better web -->
                </p>
            </div>
            
        </footer>
        
        
        
        
        
        <div id="preloader" style="display: none;">
	        <div id="loader" class="loader" style="display: none;">&nbsp;</div>
	    </div>
	  
		 <div id="successId" style="display:none;">
		 
		 	<%@include file="notification.jsp" %>
		 			    	    
		 </div>
	 
	 
</body>
</html>