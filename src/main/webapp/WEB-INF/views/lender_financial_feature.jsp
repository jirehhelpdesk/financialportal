<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Financial Feature</title>

    <link href="resources/borrower_lender_resources/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="resources/borrower_lender_resources/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="resources/borrower_lender_resources/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="resources/borrower_lender_resources/assets/css/demo.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="resources/borrower_lender_resources/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    
    
   
    <link href="resources/borrower_lender_resources/profile_theme_style1.css" rel="stylesheet" >
    
    
     <link rel="stylesheet" type="text/css" href="resources/datePicker/jquery.datetimepicker.css"/>
     
     
</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>





<c:if test="${lenderBean.lender_present_city ne null}">

<body onload="lenderActiveSideMenu('menu5')">

</c:if>

<c:if test="${lenderBean.lender_present_city eq null}">

<body onload="lenderActiveSideMenu('menu5'),alertAddressUpdate()">

</c:if>


<% String policyTypeName = (String)request.getAttribute("policyTypeName");

   String policyTypeArray[] = policyTypeName.split("/");
%>


<%String docList = (String)request.getAttribute("docList"); System.out.println("file="+docList);%>  
<%String docListArray[] = docList.split(","); System.out.println("Length="+docListArray.length);%>
 
<% String policyNames = (String)request.getAttribute("policyName"); %>   
<% String policyNameList[] = policyNames.split(","); %>                              
  
  
<% 

Date date = new Date(); 
DateFormat datef = new SimpleDateFormat("dd/MM/yyyy");
Date day = date;        
String startDate = datef.format(day);

%>   
                              
<div class="wrapper">
    
    
      <!-- Start of Sidemenu Division -->
    
         <%@include file="lender_sidemenu.jsp" %>

      <!-- End of Sidemenu Division -->


    <div class="main-panel">
        
        
         <!-- Start of Header Division -->
         
         <%@include file="lender_header.jsp" %>
        
		 <!-- End of Header Division -->

       
        <div class="content">
            
            
            <div class="container-fluid">
               
               	
               	<div class="row" id="financeFeatureFormId">
                
                   <div style="width:100%;" class="col-md-8">
                
                		
                		<div class="card">
                            
                            <div class="header">
                                <h4 class="title">Add Financial Deals</h4>
                            </div>
                            
                            <div class="content">
                                
                                <form:form   modelAttribute="policyModel"  id="policyDetailsForm" >
                                    
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-3">                                            
                                            <div class="form-group manageFormComponent"><label>Name of the feature</label></div>
                                        </div>
                                        
                                        <div class="col-md-7">
                                            
                                            <div class="form-group manageFormComponent">
                                                <input type="text"  placeholder="Feature name" class="form-control" name="policy_name" path="policy_name" id="nameId" onchange="checkLenderPolicyDetails()">
                                            </div>
                                            
                                            <div class="form-error" id="featErrorId"></div>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-3">
                                            <div class="form-group manageFormComponent">
                                                <label>Policy type</label>                                          
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-7">    
                                          
                                           <div class="form-group manageFormComponent">
                                           
                                                <Select class="form-control" id="policySubType" name="policy_type" path="policy_type"  onchange="checkLenderPolicyDetails()">
                                                		
                                                		<option value="Select Policy Type">Select Policy Type</option>
                                                		<%for(int i=0;i<policyNameList.length;i++){ %>
                                                			
                                                			<option value="<%=policyNameList[i]%>"><%=policyNameList[i]%></option>
                                                		
                                                		<%} %>
                                                		
                                                </Select>
                                               
                                            </div>
                                            
                                            <div class="form-error" id="subTypeErrorId"></div>
                                            
                                        </div>
                                       
                                    </div>
									
									
									
									<div class="row">
                                        
                                        <div class="col-md-3">
                                            <div class="form-group manageFormComponent">
                                                <label>Amount limit (in rupees)</label>
                                             </div>
                                        </div>
                                        
                                        <div class="col-md-4" style="width: 29.333% ! important;">
                                            <div class="form-group manageFormComponent">
                                                <input type="text" placeholder="Minimum Amount for Loan" class="form-control" name="policy_min_amount" path="policy_min_amount" id="minAmtId" onchange="checkLenderPolicyDetails()">
                                            </div>
                                            
                                            <div class="form-error" id="minAmtErrorId"></div>
                                            
                                        </div>
                                        
                                        <div class="col-md-4" style="width: 29.333% ! important;">
                                            <div class="form-group manageFormComponent">
                                                <input type="text" placeholder="Maximum Amount for Loan" class="form-control" name="policy_max_amount" path="policy_max_amount" id="maxAmtId" onchange="checkLenderPolicyDetails()">
                                            </div>
                                            
                                            <div class="form-error" id="maxAmtErrorId"></div>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-3">
                                            <div class="form-group manageFormComponent">
                                                <label>Loan Tenure (in months)</label>
                                             </div>
                                        </div>
                                        
                                        <div class="col-md-4" style="width: 29.333% ! important;">
                                            <div class="form-group manageFormComponent">
                                                <input type="text" placeholder="Minimum Duration for Loan in months" maxlength="3" class="form-control" name="policy_min_duration" path="policy_min_duration" id="minDurId" onchange="checkLenderPolicyDetails()">
                                            </div>
                                            <div class="form-error" id="minDurErrorId"></div>
                                        </div>
                                        
                                        <div class="col-md-4" style="width: 29.333% ! important;">
                                            <div class="form-group manageFormComponent">
                                                <input type="text" placeholder="Maximum Duration for Loan in months" maxlength="3" class="form-control" name="policy_max_duration" path="policy_max_duration" id="maxDurId" onchange="checkLenderPolicyDetails()">
                                            	
                                            </div>                                         
                                            <div class="form-error" id="maxDurErrorId"></div>
                                        </div>
                                        
                                       
                                    </div>
                                    
                                    
                                    		
									<div class="row">
											 
                                        <div class="col-md-3">
                                            <div class="form-group manageFormComponent">
                                                <label>Interest rate</label>                                               
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-7">                                             
                                           <div class="form-group manageFormComponent">                                          		
                                           		<input type="text" placeholder="Interest rate" class="form-control" name="policy_interest_rate" path="policy_interest_rate" id="intRateId" onchange="checkLenderPolicyDetails()">                                           		
                                           </div>   
                                           <div class="form-error" id="intErrorId"></div>                                       
                                        </div> 
									
									</div>

									<div class="row">
                                        
                                        <div class="col-md-3">
                                            <div class="form-group manageFormComponent">
                                                <label>Upto Age limit</label>
                                           
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-7">    
                                          
                                           <div class="form-group manageFormComponent">
                                     
                                                <input type="text" placeholder="Please enter age limit from 18 to 60" maxlength="2" class="form-control" id="ageLimit" name="policy_age_limit" path="policy_age_limit"  onkeyup="checkAgeLimit('ageLimit')">
                                                	
                                            </div>
                                            
                                            <div class="form-error" id="ageErrorId"></div>
                                            
                                        </div>
                                       
                                    </div>
                                    
                                    <div class="row" >
                                        
                                        <div class="col-md-3">
                                            <div class="form-group manageFormComponent">
                                                <label>Gender</label>
                                           
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2">
                                            <div class="form-group manageFormComponent" style="color:#333 !important;">
                                                
                                                <input type="radio" value="Male" name="policy_for_gender" path="policy_for_gender" id="genderId" >&nbsp;Male 
                                                
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="col-md-2">
                                            <div class="form-group manageFormComponent" style="color:#333 !important;">
                                                
                                                <input type="radio" value="Female"  name="policy_for_gender" path="policy_for_gender" id="genderId" >&nbsp;Female 
                                                
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="col-md-2">
                                            <div class="form-group manageFormComponent" style="color:#333 !important;">
                                                
                                                <input type="radio" value="Both"  name="policy_for_gender" path="policy_for_gender" id="genderId" >&nbsp;For both 
                                                
                                            </div>
                                            
                                        </div>
                                        <div class="form-error" id="genderErrorId"></div>
                                       
                                    </div>
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-3">
                                            <div class="form-group manageFormComponent">
                                                <label>Income limit (in lacs)</label>
                                           
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-7">    
                                      
                                           <div class="form-group manageFormComponent">
                                           
                                                
                                                <input type="text" class="form-control" id="incomeLimit" name="policy_income_limit" path="policy_income_limit" placeholder="income limit for the policy" onkeyup="checkLenderIncomeLimit('incomeLimit')"/>
                                                
                                            </div>
                                            
                                            <div class="form-error" id="incomeErrorId"></div>
                                            
                                        </div>
                                       
                                    </div>
                                    
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-3">
                                            <div class="form-group manageFormComponent">
                                                	<label>Policy applicable location</label>                                         
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-7">    
                                          
                                            <div class="form-group manageFormComponent">
                                           
                                                <Select class="form-control" id="location" name="policy_applicable_location" path="policy_applicable_location"  onchange="checkLenderPolicyDetails()">
                                                		
                                                		<option value="Select applicable location">Select applicable location</option>
                                                		<option selected value="${lenderBean.lender_present_city}">Current city</option>
                                                		<option value="India">Applicable for entire country</option>
                                                		
                                                </Select>
                                               
                                            </div>
                                            
                                            <div class="form-error" id="locationErrorId"></div>
                                            
                                        </div>
                                       
                                    </div>
                                    
									
									<div class="row">
                                        
                                        <div class="col-md-3">
                                            <div class="form-group manageFormComponent">
                                                <label>Policy duration</label>
                                             </div>
                                        </div>
                                        
                                        <div class="col-md-4" style="width: 29.333% ! important;">
                                            
                                            <div class="form-group manageFormComponent">
                                                <input type="text" class="form-control" readonly value="<%=startDate%>" name="policy_start_date"  >
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="col-md-4" style="width: 29.333% ! important;">
                                            
                                            <div class="form-group manageFormComponent">
                                                <input type="text" placeholder="Policy Expire Date" readonly class="some_class form-control" name="policy_end_date" id="polEndId" onchange="checkLenderPolicyDetails()">
                                            </div>
                                            
                                            <div class="form-error" id="endDateErrorId"></div>
                                            
                                        </div>
                                        
                                        
                                    </div>
                                    
                                    
			
                                    <div class="row">
                                        
                                        <div class="col-md-3">
                                            <div class="form-group manageFormComponent">
                                                <label>Broucher if any</label>
                                             </div>
                                        </div>
                                        
                                        <div class="col-md-7">
                                            <div class="form-group manageFormComponent">
                                                
                                                <input type="file"  placeholder="Broucher for this" name="file" id="file" onchange="checkLenderPolicyDetails()">
                                                
                                            </div>
                                            <div class="form-error" id="docErrorId"></div>
                                        </div>
                                        
                                    </div>

                                    
                                    
                                    
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-3">
                                            <div class="form-group manageFormComponent">
                                                <label>Basic document required</label>
                                             </div>
                                        </div>
                                     
                                   
                                        <div class="col-md-7">
                                            
                                            <div class="form-group ulStyle manageFormComponent" style="min-height:auto ! important;height:auto ! important;">
                                                       
                                                   <ul>
                                                   		
                                                   		<%for(int d=0;d<docListArray.length;d++) {%>
                                                   		
                                                			<li class="liStyle"><input type="checkbox" id="<%=docListArray[d].replaceAll(" ","")%>" value="<%=docListArray[d]%>" onclick="getBasicDocName(this.id)"/>&nbsp;&nbsp;<%=docListArray[d]%></li>
                                                			
                                                		<%} %>
                                              		
                                                   </ul>
                                                   
                                                   &nbsp;                                          	  
                                            </div>
                                            
                                            <div class="form-error" id="basicDocErrorId" ></div> 
                                             
                                        </div>
                                      
                                    
                                    </div>
                                    
                                    
									<input id="documentsNeedId" type="hidden" value="" name="policy_required_doc" path="policy_required_doc" />
									
                                    <input type="button" value="Save"  class="btn btn-info btn-fill pull-right" onclick="saveLenderPolicyDetails()">
                                    
                                         &nbsp; &nbsp; &nbsp;
                                     
                                    <!--  <button class="btn btn-info btn-fill pull-right" type="reset">Reset</button> -->
                                    
                                    <div class="clearfix"></div>
                                    
                                    
                                </form:form>
                                
                            </div>
                            
                        </div>
                        
                        
                        </div>
                </div>
               
               	
               	
               	
            </div>
        </div>
        
        

       <!-- Start of Footer Division -->
 
         <%@include file="lender_footer.jsp" %>
        
	   <!-- Start of Footer Division -->
	   
	   
    </div>
</div>


</body>

     
       <!-- Start of Common Division -->
 
         <%@include file="common_content.jsp" %>
        
	   <!-- Start of Common Division -->   
   
</html>
