<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" href="resources/paging/other.css">

</head>



<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>



<body>



<c:if test="${!empty loanType}">	


<div class="table-responsive">
                                        
                                        
                                        
                                        <table id="tableData" class="table table-bordered table-striped table-actions">
                                            <thead>
                                                <tr>
                                                    <th width="50">Sl No</th>
                                                    <th width="100">TYPE</th>
                                                    <th width="100">NAME</th>
                                                    <th width="100">CREATED DATE</th>
                                                    <th width="100">MANAGE</th>                                                    
                                                </tr>
                                            </thead>
                                            <tbody>                                            
                                               
                                               
                                             <%int i=1; %> 
               								 <c:forEach items="${loanType}" var="det">	
                    
                                                
                                                 <tr id="trow_1">
                                                    
                                                    <td class="text-center"><%=i++%></td>
                                                    
                                                    <td><strong>${det.policy_type_name}</strong></td>
                                                    
                                                    <td>${det.sub_policy_type}</td>
                                                   
                                                    <td>${det.cr_date}</td>
                                                    
                                                    <td>
                                                    	
                                                    	<%-- <button class="btn btn-info btn-rounded" type="button" onclick="editPolicyName('${det.policy_type_id}')">Edit</button>                                   --%>                     
                                                        <button class="btn btn-danger btn-rounded" type="button" onclick="deletePolicyName('${det.policy_type_id}');">Delete</button>
                                                        
                                                    </td>
                                                
                                                </tr>
                                             
                                             </c:forEach>   
                                           
                                            </tbody>
                                        </table>
                                        
                                        
                                    </div>      
                                    
                                    
                                    <!-- Paging zone -->
          							
          							
									<script type="text/javascript" src="resources/paging/jquery.min.js"></script> 
									
									<script src="resources/paging/jquery-ui.min.js"></script>
									
									<script type="text/javascript" src="resources/paging/paging.js"></script> 
									
									<script type="text/javascript">
									            $(document).ready(function() {
									                $('#tableData').paging({limit:10});
									            });
									        </script>
									        <script type="text/javascript">
									
									  var _gaq = _gaq || [];
									  _gaq.push(['_setAccount', 'UA-36251023-1']);
									  _gaq.push(['_setDomainName', 'jqueryscript.net']);
									  _gaq.push(['_trackPageview']);
									
									  (function() {
									    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
									    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
									    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
									  })();
									
									</script>
									
				                  
				                   <!-- End of  Paging zone -->
                                
                                
   </c:if>
  
  
  <c:if test="${empty loanType}">	
						
	    <div>As per your search criteria nothing was found !</div>
		
  </c:if>
	
	
	                                             
</body>
</html>