<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Contact Us</title>


 <link rel="shortcut icon" type="image/icon" href="resources/indexResources/assets/images/favicon.ico"/>
    <!-- Font Awesome -->
    <link href="resources/indexResources/assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="resources/indexResources/assets/css/bootstrap.css" rel="stylesheet">
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="resources/indexResources/assets/css/slick.css"/> 
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="resources/indexResources/assets/css/jquery.fancybox.css" type="text/css" media="screen" /> 
    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="resources/indexResources/assets/css/animate.css"/>  
     <!-- Theme color -->
    <link id="switcher" href="resources/indexResources/assets/css/theme-color/default.css" rel="stylesheet">

    <!-- Main Style -->
    <link href="resources/indexResources/style.css" rel="stylesheet">

    <!-- Fonts -->
    <!-- Open Sans for body font -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Raleway for Title -->
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <!-- Pacifico for 404 page   -->
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    
    <style>
    .subminButton
    {
        box-shadow: 0 1px 0 rgba(255, 255, 255, 0.5) inset, 0 1px 2px rgba(0, 0, 0, 0.3);
	    color: #fff;
	    font-size: 13px;
	    font-weight: bold;
	    width: 138px;
    }
    
    </style>
    
</head>


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<body onload="activeIndexMenu('Menu5')">



   <%@include file="index_header.jsp" %>
   
   
   
  
  <!-- Start Contact section -->
  
  <section id="contact">
    <div class="container">
      <div class="row">
        
        
        
        
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="contact-left wow fadeInLeft">
            <h2>Contact with us</h2>
            
             <address class="single-address">
              <h4>Address:</h4>
              <p>#49,27th main,1st cross,2nd floor,Gold Crest building,BTM 1st Stage,Bangalore,Karnataka-560068</p>
            </address>
             <address class="single-address">
              <h4>Phone</h4>
              <p>Phone Number: (080) 456 7890</p>
              <p>Fax Number: (080) 456 7890</p>
            </address>
             <address class="single-address">
              <h4>E-Mail</h4>
              <p>Support: Support@example.com</p>
              <p>Sales: sales@example.com</p>
            </address>
          </div>
        </div>
        
        
        
        <div class="col-md-8 col-sm-6 col-xs-12">
          <div class="contact-right wow fadeInRight">
            <h2>Send a message</h2>
            
            <form:form  class="contact-form" id="contactUsForm" modelAttribute="contactDetail">
              
              <div class="form-group">                
                <input type="text" class="form-control" id="nameId" placeholder="Full Name" name="contact_name" path="contact_name">
                <div id="nameErrId" class="errorStyle"></div>
              </div>
              
              <div class="form-group">                
                <input type="email" class="form-control" id="emailId" placeholder="Email Id" name="contact_email_id" path="contact_email_id" >
              	<div id="emailErrId" class="errorStyle"></div>
              </div>  
              
              <div class="form-group">                
                <select class="form-control" id="contactForId" name="contact_reason" path="contact_reason" style="height:33px ! important;font-size:15px;">
                			<option value="Select reason for contact">Select reason for contact</option>
                			<option value="Feedback">Feedback</option>
                			<option value="Enquiry">Enquiry</option>
                			<option value="Complain">Complain</option>
                			<option value="Business">Business</option>
                </select>
                <div id="reasonErrId" class="errorStyle"></div>
              </div>  
                          
              <div class="form-group">
                <textarea class="form-control" placeholder="Provide your message in details." id="messageId" name="contact_message" path="contact_message"></textarea>
              	<div id="messageErrId" class="errorStyle"></div>
              </div>
              
              
            
            </form:form>
            	
              <div class="form-group">
            	<button class="btn contactBtn" onclick="sendContactForm()"><span>SUBMIT</span></button>
              </div>
            
          </div>
        </div>
        
        
        
        
      </div>
    </div>
  </section>
  <!-- End Contact section -->
  
  <!-- Start Google Map -->
  <section id="google-map">
  
  <iframe src="https://www.google.com/maps/embed/v1/place?q=Jireh+Software+Solutions,+16th+Main+Road,+MCHS+Colony,+Bengaluru,+Karnataka,+India&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU" width="100%" height="330" frameborder="0" style="border:0" allowfullscreen></iframe>  
  
  </section>
  <!-- End Google Map -->
  
 
   
   
   <%@include file="index_fotter.jsp" %>
   
   
   
   
   
  <!-- initialize jQuery Library --> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <!-- Bootstrap -->
  <script src="resources/indexResources/assets/js/bootstrap.js"></script>
  <!-- Slick Slider -->
  <script type="text/javascript" src="resources/indexResources/assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="resources/indexResources/assets/js/waypoints.js"></script>
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.counterup.js"></script>
  <!-- mixit slider -->
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->        
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.fancybox.pack.js"></script>
  <!-- Wow animation -->
  <script type="text/javascript" src="resources/indexResources/assets/js/wow.js"></script> 

  <!-- Custom js -->
  <script type="text/javascript" src="resources/indexResources/assets/js/custom.js"></script>
    
    
    
  <script type="text/javascript" src="resources/indexResources/index_registration_script.js"></script> 
 
    
</body>
</html>