<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>My Profile</title>

    <link href="resources/borrower_lender_resources/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="resources/borrower_lender_resources/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="resources/borrower_lender_resources/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="resources/borrower_lender_resources/assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="resources/borrower_lender_resources/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    
    
    
    
     
    <link href="resources/borrower_lender_resources/profile_theme_style.css" rel="stylesheet" >
    <link href="resources/borrower_lender_resources/profile_theme_style1.css" rel="stylesheet" >
    
    
</head>


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<body onload="lenderActiveSideMenu('menu2')">


<div class="wrapper">
    
    
      <!-- Start of Sidemenu Division -->
    
         <%@include file="lender_sidemenu.jsp" %>

      <!-- End of Sidemenu Division -->


    <div class="main-panel">
        
        
         <!-- Start of Header Division -->
         
         	<%@include file="lender_header.jsp" %>
        
		 <!-- End of Header Division -->

       
        
 <c:set var="photo" value="${lenderBean.lender_photo}"/>
 <% String photo = (String)pageContext.getAttribute("photo"); %>
   
   
   
        <div class="content" style="padding: 2px 7px ! important;">
            
            <div class="container-fluid">
                
                <div class="row">
                   
                   			
                    	<!-- START OF PROFILE DIV -->
                    	
                    	
                    				<div class="vd_content-section clearfix">
								            
								            <div class="row">
								            
														<div class="col-sm-12">
						
															<div class="tabs widget">
						
																<div class="tab-content" style="border-radius: 4px; box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), 0 0 0 1px rgba(63, 63, 68, 0.1);">
						
																	<div id="profile-tab" class="tab-pane active">
						
																		<div class="pd-20">
																			
																			<h3 class="mgbt-xs-15 mgtp-10 font-semibold">
																				 Basic Details
																				
																				<span class="profileEditButton" onclick="editLenderBasic()">Edit</span>
																			</h3>
																			
																			<c:if test="${lenderBean.lender_type eq 'Individual'}">
																			
																					<div class="row">
																						<div class="col-sm-6">
																							<div class="row mgbt-xs-0">
																							    
																							    <label class="col-xs-4 control-label manageFormComponent">Full name</label>
																								
																								<div class="col-xs-7 controls">${lenderBean.full_name}</div>
																								<!-- col-sm-10 -->
																							</div>
																						</div>
																						
																						<div class="col-sm-6">
																							<div class="row mgbt-xs-0">
																								<label class="col-xs-4 control-label manageFormComponent">Email id</label>
																								<div class="col-xs-7 controls">${lenderBean.lender_emailid}</div>
																								<!-- col-sm-10 -->
																							</div>
																						</div>
																						
																					</div>
																					
																					<div class="row">
																					
																						<div class="col-sm-6">
																							<div class="row mgbt-xs-0">
																								<label class="col-xs-4 control-label manageFormComponent">Mobile no</label>
																								<div class="col-xs-7 controls">${lenderBean.lender_phno}</div>
																								<!-- col-sm-10 -->
																							</div>
																						</div>
																						
																						<div class="col-sm-6">
																							<div class="row mgbt-xs-0">
																								<label class="col-xs-4 control-label manageFormComponent">Date of join</label>
																								<div class="col-xs-7 controls"><fmt:formatDate pattern="dd/MM/yyyy" value="${lenderBean.lender_reg_time}" /></div>
																								<!-- col-sm-10 -->
																							</div>
																						</div>
																						
																					</div>
																					
																					<div class="row">
																					
																						<div class="col-sm-6">
																							<div class="row mgbt-xs-0">
																								<label class="col-xs-4 control-label manageFormComponent">PAN</label>
																								<div class="col-xs-7 controls">${lenderBean.pan_number}</div>
																								<!-- col-sm-10 -->
																							</div>
																						</div>
																						
																					</div>
																					
																			
																			</c:if>
																			
																			
																			<c:if test="${lenderBean.lender_type ne 'Individual'}">
																			
																					<div class="row">
																						
																						
																						<div class="col-sm-6">
																							<div class="row mgbt-xs-0">
																							    
																								<c:if test="${lenderBean.lender_type eq 'Bank'}">
																									<label class="col-xs-5 control-label manageFormComponent">Bank name</label>
																								</c:if>
																								<c:if test="${lenderBean.lender_type eq 'An Organization'}">
																									<label class="col-xs-5 control-label manageFormComponent">Organization name:</label>
																								</c:if>
																								<div class="col-xs-7 controls">${lenderBean.full_name}</div>
																								<!-- col-sm-10 -->
																							</div>
																						</div>
																						
																						<div class="col-sm-6">
																							<div class="row mgbt-xs-0">
																								<label class="col-xs-5 control-label manageFormComponent">Date of
																									Join</label>
																								<div class="col-xs-7 controls">${lenderBean.lender_reg_time}</div>
																								<!-- col-sm-10 -->
																							</div>
																						</div>
																						
																					</div>
																					
																					
																					
																			</c:if>
																			
																			
																			
																			<hr class="pd-10">
																			
																			
																			
																			
																			<div class="row">
																				
																				<div class="col-sm-12">
																					<div class="row mgbt-xs-0">
																						<label class="col-xs-2 control-label manageFormComponent">Address</label>
																						<c:if test="${lenderBean.lender_present_door ne null}">
																						   
																						   <div class="col-xs-10 controls">${lenderBean.lender_present_door},&nbsp;${lenderBean.lender_present_building},&nbsp;${lenderBean.lender_present_street},&nbsp;${lenderBean.lender_present_area},&nbsp;${lenderBean.lender_present_city},&nbsp;${lenderBean.lender_present_pincode}</div>
																						
																						</c:if>
																						<!-- col-sm-10 -->
																					</div>
																				</div>
																				
																			</div>
																			
																		    <c:if test="${lenderBean.lender_type ne 'Individual'}">
																		          
																		       <div class="row">		
																				
																					<div class="col-sm-6">
																					    <p style="font-size:14px;color:#007399;"><u><b>Primary Representative,</b></u></p>
																						<div class="row mgbt-xs-0">
																							<label class="col-xs-3 control-label manageFormComponent">Full name </label>
																							<div class="col-xs-8 controls">${lenderBean.lender_representative_name}</div>
																							<!-- col-sm-10 -->
																						</div>
																					</div>
																					
																					<div class="col-sm-6">
																						<p style="font-size:14px;color:#007399;"><u><b>Second Representative,</b></u></p>
																						<div class="row mgbt-xs-0">
																							<label class="col-xs-3 control-label manageFormComponent">Full name</label>
																							<div class="col-xs-8 controls">${lenderBean.lender_second_representative_name}</div>
																							<!-- col-sm-10 -->
																						</div>
																					</div>
																				
																				 </div>
																			
																				  
																				 <div class="row">		
																				    
																					<div class="col-sm-6">
																						<div class="row mgbt-xs-0">
																							<label class="col-xs-3 control-label manageFormComponent">Email id</label>
																							<div class="col-xs-8 controls">${lenderBean.lender_emailid}</div>
																							<!-- col-sm-10 -->
																						</div>
																					</div>
																				    	
																					<div class="col-sm-6">
																						<div class="row mgbt-xs-0">
																							<label class="col-xs-3 control-label manageFormComponent">Email id</label>
																							<div class="col-xs-8 controls">${lenderBean.lender_second_representative_email_id}</div>
																							<!-- col-sm-10 -->
																						</div>
																					</div>
																				    
																				 </div>
																				 
																				 
																				 <div class="row">		
																				    
																					<div class="col-sm-6">
																						<div class="row mgbt-xs-0">
																							<label class="col-xs-3 control-label manageFormComponent">Mobile number</label>
																							<div class="col-xs-8 controls">${lenderBean.lender_phno}</div>
																							<!-- col-sm-10 -->
																						</div>
																					</div>
																					
																					<div class="col-sm-6">
																						<div class="row mgbt-xs-0">
																							<label class="col-xs-3 control-label manageFormComponent">Mobile number</label>
																							<div class="col-xs-8 controls">${lenderBean.lender_second_representative_mobile_no}</div>
																							<!-- col-sm-10 -->
																						</div>
																					</div>
																				    
																				 </div>
																				 
																				 
																				 
																				 
																				 <div class="row">
																						
																						
																						<div class="col-sm-6">
																							<div class="row mgbt-xs-0">
																								<label class="col-xs-3 control-label manageFormComponent">PAN no.</label>
																								<div class="col-xs-8 controls">${lenderBean.pan_number}</div>
																								<!-- col-sm-10 -->
																							</div>
																						</div>
																						
																						<div class="col-sm-6">
																							<div class="row mgbt-xs-0">
																								<label class="col-xs-3 control-label manageFormComponent">Service tax no.</label>
																								<div class="col-xs-8 controls">${lenderBean.service_tax_number}</div>
																								<!-- col-sm-10 -->
																							</div>
																						</div>
																				</div>
																				
																				<div class="row">		
																						
																						<div class="col-sm-6">
																							<div class="row mgbt-xs-0">
																								<label class="col-xs-3 control-label manageFormComponent">TAN no.</label>
																								<div class="col-xs-8 controls">${lenderBean.tan_number}</div>
																								<!-- col-sm-10 -->
																							</div>
																						</div>
																						
																						
																				</div>
																					
																				
																				 
																				
																		   </c:if>
																		   
																		   
																		   
																		   
																		   
																		
																		</div>
																		<!-- pd-20 -->
																	</div>
																	<!-- home-tab -->
						
						
						
																	<!-- photos tab -->
																	<!-- photos tab -->
																	<!-- groups tab -->
						
																</div>
																<!-- tab-content -->
															</div>
															<!-- tabs-widget -->
														</div>
						
				
											</div>
								            <!-- row --> 
								            
								            
								            
								            
								            
								           <!-- Attached Documents -->
								            
								          	<div class="row">
								          	
											            <div class="col-md-12" >
									                        
									                        <div class="card" style="display:block;">
									                            
									                            <div class="header">
									                               
									                                <span class="profileEditButton" onclick="editLenderDoc()">Add</span>
									                                <h4 class="title">Attached documents</h4>
									                                
									                            </div>
									                            
											                            <div class="content all-icons">
											                                
											                                <div class="row">
											                                  
											                                  
											                                  <c:if test="${!empty attachedDocument}">
									                            
									                            
													                                  <c:forEach items="${attachedDocument}" var="det">
													                                  
														                                  <c:set var="docName" value="${det.file_name}"/>
																						  <% String docName = (String)pageContext.getAttribute("docName"); %>
						
														                                     
														                                  <div class="font-icon-list col-lg-2 col-md-3 col-sm-4 col-xs-6 col-xs-6">
														                                    
														                                    <div class="font-icon-detail">
														                                      
														                                      <input value="${det.document_type}" disabled type="text">
														                                      
														                                      <img src="${pageContext.request.contextPath}<%="/lenAttachedDocuments?fileName="+docName+""%>" height="120px" width="142px;" />				  
														                                      
														                                      <input value="Zoom" type="button" class="btn btn-info btn-fill pull-right" onclick="viewLenAddedDoc('${det.lender_document_id}')">
														                                    
														                                    </div>
														
														                                  </div>
													                                  
													                                  </c:forEach>
											                                  
											                                  		  </c:if>
									                        		
														                        		<c:if test="${empty attachedDocument}">
														                        		
														                        			<div><p style="margin-left: 16px;">As of now you didn't attached any documents.</p></div>                         
																	
														                        		</c:if>
														                         </div>
											
											                            </div>
									                        
									                        </div>
									                    </div>
											           
								          	     </div>
								          
								          	
								            <!-- End of Attached Documents -->
								            
								            
								            
								            
								            
								            
								            
								            
								            
								            
								                 
								                 
								             <div class="row" style="margin-top:-20px;">


													<div class="col-md-12">
														<div class="card">
															<div class="header">
																
																<span class="profileEditButton" onclick="redirectLink('addFeature')">Create more</span>
																
																<h4 class="title">Policies Created</h4>
																<p class="category"></p>
															</div>
															
															
															<div class="content table-responsive table-full-width">
															
															<c:if test="${!empty policyBean}">	
																
																<table class="table table-hover" role="grid" id="tableData">
																	<thead>
																		<tr>
																			
																			<th>Policy Name</th>
																			<th>Loan Type</th>
																			<th>Loan Amount</th>
																			<th>Loan Tenure</th>
																			<th>Interest Rate</th>
																			<th>Created Date</th>
																			<th>Status</th>
																			
																		</tr>
																	</thead>
																	
																	<tbody>
																		
																		<c:forEach items="${policyBean}" var="det">
																			
																			<tr>
																				
																				<td>${det.policy_name}</td>
																				<td>${det.policy_type}</td>
																				<td>Rs. ${det.policy_min_amount} - ${det.policy_max_amount}</td>
																				<td>${det.policy_min_duration} - ${det.policy_max_duration} months</td>
																				<td>${det.policy_interest_rate}</td>
																				<td><fmt:formatDate pattern="dd/MM/yyyy" value="${det.cr_date}" /></td>
																				<td>${det.policy_approval_status}</td>
																			</tr>
																			
																		</c:forEach>	
																		
																	</tbody>
																</table>
																
																</c:if>
																
																<c:if test="${empty policyBean}">
																	
																	<div><p style="margin-left: 16px;">As of now you didn't created any feature. <a href="addFeature">create finance</a></p></div>                         
																												
																</c:if>
																
															</div>
														</div>
													</div>
					
					
					
													
													<%-- <div class="col-md-12">
														<div class="card">
															<div class="header">
																
																 <span class="profileEditButton"  onclick="redirectLink('borrowerRqst')">Know more</span>
																<h4 class="title">Last Borrower requested for loan</h4>
																<p class="category"></p>
															</div>
															
															
															<div class="content table-responsive table-full-width">
															
															<c:if test="${!empty dealBean}">	
																
																<table class="table table-hover" role="grid" id="tableData">
																	<thead>
																		<tr>
																			
																			<th>Loan Name</th>
																			<th>Loan Type</th>
																			<th>Loan Amount</th>
																			
																		</tr>
																	</thead>
																	
																	<tbody>
																		
																		<c:forEach items="${dealBean}" var="det">
																			
																			<tr>
																				
																				<td>${det.policy_name}</td>
																				<td>${det.policy_type}</td>
																				<td>Rs.</td>
																				
																			</tr>
																			
																		</c:forEach>	
																		
																	</tbody>
																</table>
																
																</c:if>
																
																<c:if test="${empty dealBean}">
																	
																	<div><p style="margin-left: 16px;">As of now you didn't deal with any borrower.</p></div>                         
																																						
																</c:if>
																
															</div>
														</div>
													</div> --%>
													
												
										     </div>	
								                 
								              
								     
								     
								     </div>
                    	
                    	
                    	
                    	<!-- END OF PROFILE DIV -->
                   
                   	
                   	
                </div>
                
                
                
                
                
            </div>
        </div>
        
       <!-- Start of Footer Division -->
 
         <%@include file="lender_footer.jsp" %>
        
	   <!-- Start of Footer Division -->
	   
	   
    </div>
</div>


</body>

     
       <!-- Start of Common Division -->
 
         <%@include file="common_content.jsp" %>
        
	   <!-- Start of Common Division -->   
   
	  <div id="viewAttachedDocument" style="display:none;"> </div>
	  
	  
</html>
