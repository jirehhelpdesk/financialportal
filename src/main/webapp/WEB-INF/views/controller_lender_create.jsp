<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lender Create</title>

 <!-- META SECTION -->
           
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="resources/adminResources/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->     
        
</head>



<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<body onload="activeSideMenu('menu3',2)">


 <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           
           
            <!-- START PAGE SIDEBAR -->
              
              <%@include file="controller_side_bar.jsp" %>
            
            <!-- END PAGE SIDEBAR -->
            
            
            
            
            <!-- PAGE CONTENT -->
            
            <div class="page-content" id="contentDiv">
               
               
               
                
                <!-- START X-NAVIGATION VERTICAL -->
                    
                    <%@include file="controller_header_bar.jsp" %>
                
                <!-- END X-NAVIGATION VERTICAL -->                     




                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>                    
                    <li >Lender Management</li>
                    <li class="active">Lender Create</li>
                </ul>
                <!-- END BREADCRUMB -->                       
                
                
                
                 <div class="page-title">                    
                    <h2><span class="fa fa-users"></span> Create Lender  <small></small></h2>
                </div>
                
                
                <!-- PAGE CONTENT WRAPPER -->
                
                
                <div class="page-content-wrap">
                    
                   <div class="row">
                       
                        <div class="col-md-12">
                            
                            <div class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-heading ui-draggable-handle">
                                    <h3 class="panel-title"><strong>Lender Registration Form</strong></h3>
                                    
                                </div>
                                <div class="panel-body">
                                    <p>Please enter all the respective option to get the result.</p>
                                </div>
                                
                                
                                <div class="panel-body">                                                                        
                                
                                
                                <form:form class="registration-form" id="lenderRegForm" modelAttribute="lenderRegDetails" method="post" action="createLenderByCntrl" >
				                    
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Lender Type</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
	                                                <span class="input-group-addon"><span class="fa fa-search"></span></span>
	                                                <select  class="form-control" id="lenderTypeId" name="lender_type" path="lender_type">
                                                    		<option value="Select type of lender">Select type of lender</option>
						                    			    <option value="Individual">Individual</option>
						                    			    <option value="Bank">Bank</option>
						                    				<option value="An Organization">An Organization</option>
						                    				
                                                    </select>
                                            </div>                                            
                                            <span class="help-block" id="errorTypeId"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Lender Name</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" placeholder="Lender Name" id="lenderNameId" class="form-control" name="full_name" path="full_name">
                                            </div>                                            
                                            <span class="help-block" id="errorNameId"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Primary Representative Name</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" placeholder="Enter primary representative name" id="firstRepreNameId" class="form-control" name="lender_representative_name" path="lender_representative_name">
                                            </div>                                            
                                            <span class="help-block" id="error1stRepreNameId"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Primary Representative Email id</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" placeholder="Enter primary representative email id" id="firstRepreEmailId" class="form-control" name="lender_emailid" path="lender_emailid">
                                            </div>                                            
                                            <span class="help-block" id="error1stRepreEmailId"></span>
                                        </div>
                                    </div>
                                    
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Primary Representative Mobile no</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" placeholder="Enter primary representative mobile no" id="firstRepreMobileId" class="form-control" name="lender_phno" path="lender_phno">
                                            </div>                                            
                                            <span class="help-block" id="error1stRepreMobileId"></span>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Secondary Representative Name</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" placeholder="Enter secondary representative name" id="secondRepreNameId" class="form-control" name="lender_second_representative_name" path="lender_second_representative_name">
                                            </div>                                            
                                            <span class="help-block" id="error2ndRepreNameId"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Secondary Representative Email Id</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" placeholder="Enter secondary representative email id" id="secondRepreEmailId" class="form-control" name="lender_second_representative_email_id" path="lender_second_representative_email_id">
                                            </div>                                            
                                            <span class="help-block" id="error2ndRepreEmailId"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Secondary Representative Mobile no</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" placeholder="Enter secondary representative mobile no" id="secondRepreMobileId" class="form-control" name="lender_second_representative_mobile_no" path="lender_second_representative_mobile_no">
                                            </div>                                            
                                            <span class="help-block" id="error2ndRepreMobileId"></span>
                                        </div>
                                    </div>
                                    
                                    <!-- <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Password</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" placeholder="Set a password" id="searchValueId" class="form-control" name="" path="">
                                            </div>                                            
                                            <span class="help-block" id="errorValueId"></span>
                                        </div>
                                    </div> -->
                                    
                                  
                                  
                                  </form:form>
                                   
                                   
                                    
                                </div>
                                
                                
                                
                                <div class="panel-footer">                                                               
                                    <button class="btn btn-primary pull-right" onclick="createLenderByCntrl()">Create<span class="fa fa-search fa-right"></span></button>
                                </div>
                                
                                
                            </div>
                            </div>
                            
                        </div>
                    </div>
                    
                   
                   
                    
                    
                   
                   
                   
                   
			        <!-- FOOTER CONTENT -->
       
			         <%@include file="admin_footer.jsp" %>
			       
			       <!-- END FOOTER CONTENT -->
                    
                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            
            
            </div>            
            <!-- END PAGE CONTENT -->
       
        </div>
        <!-- END PAGE CONTAINER -->

       
       
       
       
       
       
       <!-- BELOW COMMON CONTENT -->
       
         <%@include file="controller_down_common.jsp" %>
       
       <!-- END BELOW COMMON CONTENT -->
       
	
    </body>
</html>