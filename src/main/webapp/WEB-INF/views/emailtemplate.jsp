<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- If you delete this tag, the sky will fall on your head -->
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Biosys Email-1</title>

</head>

<body style="border: 1px solid #004879; margin: 0px auto; padding: 0px; width: 600px; font-size: 14px; font-family: Trebuchet MS, Verdana, Arial, sans-serif;">

	<!-- HEADER -->
	<table bgcolor="#00b6f5" width="600" border="0px"
		bordercolordark="#fff" cellpadding="0px" cellspacing="0"> 
		<tr>
			<td>
				<table width="100%" height="45" border="0px" cellpadding="0px" cellspacing="0">
					<tr>
  <td align="left" style="color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;">Thank you for being a member.</td>
					</tr>
				</table>
			</td>

		</tr>
	</table>
	<!-- /HEADER02 -->
	
	<table bgcolor="#0087bf" width="600" border="0px"
		bordercolordark="#fff" cellpadding="0px" cellspacing="0">
		<tr>
			<td>
				<table width="100%" height="70" border="0px" cellpadding="0px"
					cellspacing="0">
					<tr>
						<td align="left"
							style="font-size: 30px; color: #fffa00; padding-left: 15px; text-shadow: 1px 2px 0 #004879;"><img width="250" height="50" src="resources/indexResources/images/logo.png" /></td>

						<td align="right" style="font-size: 14px; color: #fff;">
							<p
								style="font-size: 14px; color: #fff;  margin-right: 15px;">
								<img src="resources/User_Resources/emailImages/contact_icon.png" /> <strong
									style="font-size: 14px; color: #a3c9e1;">+91
									9857958745</strong> <br> <img
									src="resources/User_Resources/emailImages/mail_icon.png" /> <strong>
									<a style="font-size: 14px; color: #a3c9e1;"
									href="emailto:hseldon@trantor.com">clensee@kyc.com</a>
								</strong>
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<!-- /HEADER02 -->  
	
	
	<!-- BODY -->
	
	<table bgcolor="#FFFFFF"  width="600" border="0px" bordercolordark="#fff" cellpadding="0px" cellspacing="0">
		<tr>
			<td  bgcolor="#FFFFFF">
				<table width="600" border="0px" bordercolordark="#fff"
					cellpadding="0px" cellspacing="0">
					<tr>
						<td><img src="resources/EmailLetter/images/slider_img.png" /></td>
					</tr>
					<tr>
						<td background-color="#fff" style="padding-left: 15px; padding-right: 15px;">
							<!-- Callout Panel -->

							<h3 style="color: #004879;">Content Headeing</h3>
							<p style="text-align: justify">Lorem ipsum dolor sit amet,
								consectetur adipisicing elit, sed do eiusmod tempor incididunt
								ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
								nostrud exercitation ullamco laboris nisi ut aliquip ex ea
								commodo consequat. Duis aute irure dolor in reprehenderit in
								voluptate velit esse cillum dolore eu fugiat nulla pariatur.
								Excepteur sint occaecat cupidatat non proident, sunt in culpa
								qui officia deserunt mollit anim id est laborum.</p>  <!--  i am here -->
							
							
							<p style="text-align: justify">To activate your Clensee account please click below.</p> <br></br>
							
							<a href="" style="cursor:pointer;margin-left:265px;">Activate</a>
									
							<table>
								
								<tr>
									<td width="300"><img width="490" height="160" src="resources/User_Resources/emailImages/belowImage-2.jpg" /></td>
										
									<!-- <td width="300" align="right"><img
										src="resources/EmailLetter/images/image02.png" /></td> -->
								</tr>
								
								<tr><td width='300' align='right'>For futher queries please drop a mail to support@biosys.com</td></tr>
									
									
								<tr>
									<td></td>
									
									
									<td align="right"
										style="color: #777; font-size: 12px; padding-right: 20px;">Best Regards 
										<br>Biosys
									</td>
									
								</tr>
							</table>
						</td>
					</tr>


					<tr>
						<td align="center" style="color: #31576f; font-size: 12px; padding: 5px;background-color:#d3d3d3">							
							
							This Mail ID is auto generator Please do not Reply.For futher queries please drop a mail to support@biosys.com
						
						</td>
					</tr>

				</table> <!-- social & contact -->
				
				
				<table width="600" cellspacing="0" cellpadding="0px" border="0px" bgcolor="#00b6f5" style="padding-left: 15px; padding-right: 15px; margin: 0;" bordercolordark="#fff">
					<tbody><tr>
						<td>
							<!--- column 1 -->
							<table width="285" cellspacing="0" cellpadding="0px" align="left" style="color: #a3c9e1;" class="column">
								<tbody><tr>
									<td>

										<h5 style="color: #ffffff; font-size: 12px; margin: 10px -47px 5px 0;width: 155px;">Copyright
											&copy; 2015 biosys.</h5> <a href="#" style="color: #ffffff;">Terms
											&amp; Conditions </a> | <a href="#" style="color: #ffffff;">Privacy
											Policy</a>
									</td>
								</tr>
							</tbody></table>
							<!-- /column 1 --> <!--- column 2 -->
							
							<table cellspacing="0" cellpadding="0" align="right" style="color: #a3c9e1; width: 150px; text-align: right;">
								<tbody><tr>
									<td>
										<h5 style="margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;">Follow
											Us
										</h5>
									</td>
								</tr>
								
								<tr>
									<td><a href="#"><img src="resources/User_Resources/emailImages/f_icon.png"></a></td>
									<td><a href="#"><img src="resources/User_Resources/emailImages/t_icon.png"></a></td>
									<td><a href="#"><img src="resources/User_Resources/emailImages/g_icon.png"></a></td>
									<td><a href="#"><img src="resources/User_Resources/emailImages/l_icon.png"></a></td>
								</tr>
								
							</tbody></table>
							<!-- /column 2 -->

						</td>
					</tr>
				</tbody>
				
				</table>


			</td>
		</tr>
	</table>
	<!-- /BODY -->
</body>
</html>