<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" href="resources/paging/other.css">

</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>


<body>


<c:if test="${!empty controllerDetails}">	

<div class="table-responsive">
                                       
         <table aria-describedby="DataTables_Table_0_info" role="grid" id="tableData" class="table datatable dataTable no-footer">
             <thead>
                 <tr>
                     <th width="50">Sl No</th>
                     <th  width="150">Name</th>
                     <th width="80">Email Id</th>
                     <th width="60">Mobile No</th>
                     <th width="100">Status</th>
                     <th width="100">Action</th>
                 </tr>
             </thead>
             
             <tbody>                                            
               
              <%int i=1; %> 
              <c:forEach items="${controllerDetails}" var="controller">	
               
                
                 <c:set var="controller_photo" value="${controller.controller_photo}"/>
			  	 <% String controller_photo = (String)pageContext.getAttribute("controller_photo"); %>
				  
				 <c:set var="controller_id" value="${controller.controller_id}"/>
				 <% int controller_id = (Integer)pageContext.getAttribute("controller_id"); %>
				 
				 <c:set var="status" value="${controller.controller_access_status}"/>
				 <% String status = (String)pageContext.getAttribute("status"); %>
				    
				 
													    
                 <tr id="trow_1">
                     <td ><%=i++%></td>
                     <td>                        
                         <img class="searchBorrowerImg" src="${pageContext.request.contextPath}<%="/previewUserPhoto?fileName="+controller_photo+"&id="+controller_id+"&userType=Controller"%>" >
                		 <strong>${controller.controller_name}</strong>                 
				     </td>
                     <td>${controller.controller_user_id}</td>
                     <td>${controller.controller_Phno}</td>
                  
		                     
                    <%if(status.equals("Active")) {%>
		                    
		                     <td><span class="label label-success" style="font-weight: bold;font-size: 100%;">${controller.controller_access_status}</span></td>
		                     <td>
		                         <button onclick="updateControllerAccess('${controller.controller_id}','Deactive')" class="btn btn-primary">Block</button>	
		                         <button onclick="showNotifyBox('${controller.controller_id}','Controller')" class="btn btn-primary">Notify</button>					
		                     </td>
		                     
		            <%}else {%>
		            
		                     <td><span class="label label-success" style="font-weight: bold;font-size: 100%; background-color:#ff3300;">${controller.controller_access_status}</span></td>
		                     <td>                        
								 <button onclick="updateControllerAccess('${controller.controller_id}','Active')" class="btn btn-primary">Unblock</button>	
								 <button onclick="showNotifyBox('${controller.controller_id}','Controller')" class="btn btn-primary">Notify</button>					
		                     </td>
		                     
                     <%} %>
                     
                     
                 </tr>
                 
              </c:forEach>
              
             </tbody>
         </table>
                                        
</div>      
                                    
                             
        <!-- Paging zone -->
   							
   							
		<script type="text/javascript" src="resources/paging/jquery.min.js"></script> 
		
		<script src="resources/paging/jquery-ui.min.js"></script>
		
		<script type="text/javascript" src="resources/paging/paging.js"></script> 
		
		<script type="text/javascript">
		            $(document).ready(function() {
		                $('#tableData').paging({limit:2});
		            });
		        </script>
		        <script type="text/javascript">
		
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-36251023-1']);
		  _gaq.push(['_setDomainName', 'jqueryscript.net']);
		  _gaq.push(['_trackPageview']);
		
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		
		</script>
		
               
                <!-- End of  Paging zone -->

  </c:if>
  
  
  <c:if test="${empty controllerDetails}">	
						
	    <div>As per your search criteria nothing was found !</div>
		
  </c:if>
	
	
						                                             
</body>
</html>