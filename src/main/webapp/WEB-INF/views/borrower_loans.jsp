<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Loans</title>

    <link href="resources/borrower_lender_resources/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="resources/borrower_lender_resources/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="resources/borrower_lender_resources/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="resources/borrower_lender_resources/assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="resources/borrower_lender_resources/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    
    
    <link href="resources/borrower_lender_resources/profile_theme_style.css" rel="stylesheet" >
    <link href="resources/borrower_lender_resources/profile_theme_style1.css" rel="stylesheet" >
    
    
    <link rel="stylesheet" type="text/css" href="resources/datePicker/jquery.datetimepicker.css"/>
    
</head>


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<body onload="activeSideMenu('menu3')">


<div class="wrapper">
    
    
      <!-- Start of Sidemenu Division -->
    
         <%@include file="borrower_sidemenu.jsp" %>

      <!-- End of Sidemenu Division -->


    <div class="main-panel">
        
        
        
         <!-- Start of Header Division -->
         
         <%@include file="borrower_header.jsp" %>
        
		 <!-- End of Header Division -->

     
     <% String nameList = (String)request.getAttribute("nameList"); 
     
        String policyListArray[] = nameList.split("/");
     %>
     
     
       
        <div class="content">
           
            <div class="container-fluid">
                
                
                <div class="row">
                
                   <div style="width:100%;" class="col-md-8">
                
                		
                		<div class="card">
                            
                            
                            <div class="header">
                                <h4 class="title">Search Loans</h4>
                            </div>
                            
                            
                            
                            <div class="content">
                                <form id="searchFinanceForm">
                                    <div class="row">
                                        
                                        
                                        
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Policy Type</label>
                                                
	                                                <select class="form-control" name="type" id="typeId">
	                                                		
	                                                		<option value="Select your loan type">Select your policy type</option>
	                                                		<%for(int i=0;i<policyListArray.length;i++) {%>
	                                                		
	                                                		<option value="<%=policyListArray[i]%>"><%=policyListArray[i]%></option>
	                                                		
	                                                		<%} %>
	                                                		
	                                                </select>
	                                               
	                                                <div class="errorStyle" id="typeErrId" style="display:none;"></div>
	                                                
                                            </div>
                                            
                                            
                                        </div>
                                       
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Your desired loan amount </label>
                                                
                                                <input class="number form-control"  type="text" name="amount"  id="amountId" placeholder="Amount will calculate in Rupees" onkeyup="amountFormat('amountId')" />
                                               
                                                <div class="errorStyle" id="amtErrId" style="display:none;"></div>
                                                
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3">
                                           
                                            <div class="form-group">
                                                 
                                                 <label for="exampleInputEmail1">Loan Tenure</label>
                                                 
                                                 <select class="form-control" name="time" id="timeId">
                                                 		<option value="Select policy duration">Select policy duration</option>
                                                 		<option value="All">All</option>
                                                 		<option value="3">3 Months</option>
                                                 		<option value="6">6 Months</option>
                                                 		<option value="12">12 Months</option>
                                                 		<option value="18">18 Months</option>
                                                 		<option value="24">24 Months</option>    
                                                 		<option value="24+">more then 24 Months</option>                                               		
                                                 </select>
                                                 
                                                 <div class="errorStyle" id="timeErrId" style="display:none;"></div>
                                                 
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="col-md-3">
                                           
                                            <div class="form-group">
                                                 
                                                 <label for="exampleInputEmail1">Location</label>
                                                 	
                                                 	 <c:if test="${borrowerBean.borrower_present_city eq null}">
                                                 	 
                                                 	 	 <select class="form-control" name="location" id="locationId">
	                                                 		<option value="India">All over india</option>                                                		                                      		
	                                                	 </select>
	                                                	 
                                                 	 </c:if>
                                                 	 
                                                 	 <c:if test="${borrowerBean.borrower_present_city ne null}">
		                                                 	 <select class="form-control" name="location" id="locationId">
			                                                 		<option value="${borrowerBean.borrower_present_city}">Current City</option>
			                                                 		<option value="India">All over india</option>                                                		                                      		
			                                                 </select>
                                                 	 </c:if>
                                                 	 
                                                 <div class="errorStyle" id="locationErrId" style="display:none;"></div>
                                                 
                                            </div>
                                            
                                        </div>
                                        
                                        
                                    </div>

                                   
                                </form>
                                
                                
                                    <button class="btn btn-info btn-fill pull-right" style="margin-top:20px;" onclick="searchFinance()">Search</button>
                                    
                                    <div class="clearfix"></div>
                                    
                            </div>
                        </div>
                        
                        
                        </div>
                </div>

				

				<div class="row" id="searchResult" style="display:none;">
				
				  <%@include file="borrower_search_finance_list.jsp" %>
				  
				</div>  
				
				<div class="row" id="financeInfo" style="display:none;">
				
				  <%@include file="borrower_view_policy_info.jsp" %>
				  
				</div>  
				
				<div class="row financeFormStyle" id="financeApplyForm" style="margin-left:0px;width:100%;">
				
				  
				  <%@include file="borrower_finance_apply_form.jsp" %>
				  
				</div>  
				  
				
                
             </div>
        </div>


       <!-- Start of Footer Division -->
 
         <%@include file="borrower_footer.jsp" %>
        
	   <!-- Start of Footer Division -->
	   
	   
    </div>
</div>


</body>

      <!-- Start of Common Division -->
 
         <%@include file="common_content.jsp" %>
        
	   <!-- Start of Common Division -->   
   
	  
	  <div id="viewAttachedDocument" style="display:none;"> </div>
	  
	  
</html>
