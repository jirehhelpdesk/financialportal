<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<body>



<c:set var="image" value="${lenderBean.lender_photo}"/>
<% String image = (String)pageContext.getAttribute("image"); %>
 
<c:set var="type" value="${lenderBean.lender_type}"/>
<% String type = (String)pageContext.getAttribute("type"); %>
 
<c:set var="regDate" value="${lenderBean.lender_reg_time}"/>
<% Date regDate = (Date)pageContext.getAttribute("regDate"); 

DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
Date today = regDate;        
String reportDate = df.format(today);

%>

<div class="sidebar" data-color="purple" data-image="resources/borrower_lender_resources/assets/img/sidebar-5.jpg">


    	<div class="sidebar-wrapper">
            
            
            <div class="logo">
                <a href="lenProfile" class="simple-text">
                    Lender<br>
                    <span style="font-size:10px;font-weight: bold;">(${lenderBean.lender_type})</span>  
                </a>
            </div>
			
			<div class="sideMenuPhoto">
					  
					  
					  <div class="photoStyle">				  
					  		<img height="172px" width="186px;" alt="<%=image%>" src="${pageContext.request.contextPath}<%="/lenderPhoto?fileName="+image+"&type="+type+""%>"   />				  
					  </div>
	                   
				       <p class="photoBelowName" >                     
	                  		${lenderBean.full_name}  
					  </p>
					  
			</div> 
			
            <ul class="nav">
            
            
                <li id="menu2">
                    <a href="lenProfile">
                        <i class="pe-7s-user"></i>
                        <p>My Profile</p>
                    </a>
                </li>
               
                <li id="menu3">
                    <a href="borrowerRqst">
                        <i class="pe-7s-mail-open"></i>
                        <p>Borrower Request</p>
                    </a>
                </li>
                
                <li id="menu6">
                    <a href="borrowerInterestRqst">
                        <i class="pe-7s-pin"></i>
                        <p>Interested Request</p>
                    </a>
                </li>
                
                <li id="menu4">
                    <a href="loanStatus">
                        <i class="pe-7s-news-paper"></i>
                        <p>Approved Loans</p>
                    </a>
                </li>
                
                 <li id="menu5">
                    <a href="addFeature">
                        <i class="pe-7s-display2"></i>
                        <p>Create Policy</p>
                    </a>
                </li>
                
                <li id="menu8">
                    <a href="existPolicy">
                        <i class="pe-7s-ticket"></i>
                        <p>Exist Policy</p>
                    </a>
                </li>
                
                <li id="menu7">
                    <a href="lenderNotification">
                        <i class="pe-7s-mail" style="font-size:23px;"></i>
                        <p>Notification</p>
                    </a>
                </li> 
               
                
            </ul>
            
    	</div>
    	
    </div>
    

</body>
</html>