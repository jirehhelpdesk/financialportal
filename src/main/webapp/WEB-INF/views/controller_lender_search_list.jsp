<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" href="resources/paging/other.css">

<script type="text/javascript" src="resources/paging/pageingScript.js"></script> 

</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>


<body>

      <c:if test="${!empty lenderDetails}">	

      <div class="table-responsive">
                                       
              <table id="tablepaging" class="table table-bordered table-striped table-actions">
                 
                  <thead>
                      <tr>
                          <th width="50">Sl No</th>
                          <th width="150">Name</th>
                          <th width="60">Lender Type</th>
                          <th width="150">Primary Representative</th>
                          <th width="100">Primary Email id</th>
                          <th width="100">Primary Phone</th>
                         <!--  <th width="150">Secondary Representative</th>
                          <th width="100">Secondary Contacts</th>   -->                                               
                          <th width="40">Status</th>
                          <th width="151">Manage</th>
                      </tr>
                  </thead>
                  
                  
                  <tbody  id="myTableBody">                                            
                     
                    <%int i=1; %> 
					
					<c:forEach items="${lenderDetails}" var="det">	    
					               
						<c:set var="lender_photo" value="${det.lender_photo}"/>
						<% String lender_photo = (String)pageContext.getAttribute("lender_photo"); %>
						
						<c:set var="lender_id" value="${det.lender_id}"/>
						<% int lender_id = (Integer)pageContext.getAttribute("lender_id"); %>


                        <tr id="trow_1">
                           
                           <td><%=i++%></td>
                           <td>
                              
                           <%-- <img class="searchBorrowerImg" src="${pageContext.request.contextPath}<%="/previewUserPhoto?fileName="+lender_photo+"&id="+lender_id+"&userType=Lender"%>" > --%>
                   
                           <strong>${det.full_name}</strong>
                           </td>
                           <td>${det.lender_type}</td>
                           <td>${det.lender_representative_name}</td>                           
                           <td>${det.lender_emailid}</td>
                           <td>${det.lender_phno}</td>
                           <%-- <td>${det.lender_second_representative_name}</td>
                           <td>${det.lender_second_representative_email_id}<br>${det.lender_second_representative_mobile_no}</td> --%>
                           <td>${det.lender_status}</td>
                           
                           <td>
                           		 <button class="btn btn-info" type="button" style=" border-radius: 20px;" onclick="viewUserProfile('${det.lender_id}','${det.full_name}','Lender')">Profile</button>	                                     
                           		 
                                 <button class="btn btn-info" type="button" style=" border-radius: 20px;" onclick="showNotifyForLender('${det.lender_id}')">Notify</button>	         
                                 
                                 <c:if test="${det.lender_status =='Active'}">
	                                                          
                                   <button class="btn btn-info" type="button" style="border-radius:20px;" onclick="manageLenderStatus('${det.lender_id}','Block')">Block</button>
                                 
                                 </c:if>
                                 
                                 <c:if test="${det.lender_status !='Active'}">
                                  
                                   <button class="btn btn-info" type="button" style=" border-radius: 20px;" onclick="manageLenderStatus('${det.lender_id}','Unblock')">Unblock</button>
                                 
                                 </c:if>    
                                                    
                           </td>
                           
                       </tr>
                       
                  </c:forEach>
                  
              </tbody>
              
            </table>
            
                <!-- Paging zone -->
          							
	     			<div id="pageNavPosition" style="padding-top: 20px;float: right;" align="center">
					</div>
					<script type="text/javascript">
					
					var pager = new Pager('tablepaging', 10);
					pager.init();
					pager.showPageNav('pager', 'pageNavPosition');
					pager.showPage(1);
					</script>
	                          
                 
                  <!-- End of  Paging zone -->
                                       
          </div>      
              
              
           
    </c:if>


	<c:if test="${empty lenderDetails}">	
	
	
	<div class="col-md-12" style="margin-top:20px;">
	
	                                
	       <div class="messages messages-img" >
	           
	           <div class="item item-visible">
	                                               
	               <div class="text customMsgText">
	                   <div class="heading">
	                       <a href="#"></a>
	                       <span class="date"></span>
	                   </div>                                    
	                   No Lender was found as per the filter criteria.
	               </div>
	           </div>
	       </div>
	
	   </div>
    
    </c:if>                                           
</body>
</html>