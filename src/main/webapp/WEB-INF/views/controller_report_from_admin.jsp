<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Report From Admin</title>

 <!-- META SECTION -->
           
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="resources/adminResources/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->     
        
</head>


<body onload="activeSideMenu('menu4',2)">


 <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           
           
            <!-- START PAGE SIDEBAR -->
              
              <%@include file="controller_side_bar.jsp" %>
            
            <!-- END PAGE SIDEBAR -->
            
            
            
            
            <!-- PAGE CONTENT -->
            
            <div class="page-content" id="contentDiv">
               
                
                <!-- START X-NAVIGATION VERTICAL -->
                    
                    <%@include file="controller_header_bar.jsp" %>
                
                <!-- END X-NAVIGATION VERTICAL -->                     



                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>                    
                    <li >Report to Admin</li>
                    <li class="active">Message from admin</li>
                </ul>
                <!-- END BREADCRUMB -->                       
                
                
                
                 <div class="page-title">                    
                    <h2><span class="fa fa-users"></span> Message from Admin  <small></small></h2>
                </div>
                
                
                <!-- PAGE CONTENT WRAPPER -->
                
                
                <div class="page-content-wrap">
                    
                   
                   
                   		<div class="row">
                        <div class="col-md-12">
                            
                            
                   		<div class="content-frame-body" style="height: 594px;">
                        
                        <div class="panel panel-default">
                            
                            <div class="panel-heading ui-draggable-handle">
                               
                            </div>
                            
                            
                           
                            
                            <div class="panel-body mail" >
                                
                                 
                                <div class="mail-item mail-unread mail-info">                                    
                                    
                                    <div class="mail-checkbox">
                                        <div class="icheckbox_minimal-grey" style="position: relative;"><input class="icheckbox" style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                    </div>
                                    
                                    <div class="mail-star">
                                        <span class="fa fa-star-o"></span>
                                    </div>
                                    
                                    <div class="mail-user">Dmitry Ivaniuk</div>   
                                                                     
                                    <a href="#" class="mail-text">Product development</a>    
                                                                    
                                    <div class="mail-date">Today, 11:21</div>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    <div class="panel panel-default" id="msgContent_sld" style="margin-top: 10px !important;margin-bottom: 10px !important;">
			                            
			                            <div class="panel-body">
			                               
			                                <h3>Re: Product development <small class="pull-right text-muted"><span class="fa fa-clock-o"></span> Today, Sep 18, 14:33</small></h3>
			                                <p>Hello Dmitry,</p>
			                                <p>Lorem ier. Donec ultricies, neque ut vehicula ultrices, ligula velit sodales purus, eget eleifend libero risus sed turpis. Fusce hendrerit vel dui ut pulvinar. Ut sed tristique ante, sed egestas turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			                                
			                                
			                                <p>Nulla ac nisi sodales, auctor dui et, consequat turpis. Cras dolor turpis, sagittis vel elit in, varius elementum arcu. Mauris aliquet lorem ac enim blandit, nec consequat tortor auctor. Sed eget nunc at justo congue mollis eget a urna. Phasellus in mauris quis tortor porta tristique at a risus.</p>
			                                <p class="text-muted"><strong>Best Regards<br>John Doe</strong></p>      
			                                
			                            </div>
			                            
			                        </div>
			                        
                            
                                </div>
                                
                                  
                            </div>
                            
                            
                            <div class="panel-footer">                                
                               
                                
                                	
                            </div>
                            
                                                        
                        </div>
                        
                    </div>
                            
                        </div>
                    </div>
                    
                    
                    
                    
                   
                   
			        <!-- FOOTER CONTENT -->
       
			         <%@include file="admin_footer.jsp" %>
			       
			       <!-- END FOOTER CONTENT -->
                    
                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            
            
            </div>            
            <!-- END PAGE CONTENT -->
       
        </div>
        <!-- END PAGE CONTAINER -->

       
       
       
       
       
       
       <!-- BELOW COMMON CONTENT -->
       
         <%@include file="controller_down_common.jsp" %>
       
       <!-- END BELOW COMMON CONTENT -->
       
	
	
	
    </body>
</html>