<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<style>

.gapLine
{
	margin-bottom:10px;
}
</style>

</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<body>


<c:if test="${!empty policyInfo}">	

  
  <c:forEach items="${policyInfo}" var="det">	
             
            
                    <div class="col-md-12">
                        <div class="card">
                            
                            <div class="header">
                                <h4 class="title">Over View of ${det.policy_name}</h4>
                                <p class="category">See the details and apply as per the instruction</p>
                            </div>
                            
                            <div class="content">

                                
                                <div class="row">	
																		 		
									
									
									<div class="col-sm-12" >
						
										<div class="tabs widget">
	
											<div class="tab-content" >
	
												<div id="profile-tab" class="tab-pane active">
	
													<div class="pd-20">
														
														<div class="row gapLine">
															<div class="col-sm-12">
																<div class="row mgbt-xs-0">
																	<label class="col-xs-2 control-label manageFormComponent">Loan Type</label>
																	<div class="col-xs-8 controls">${det.policy_type}</div>
																	<!-- col-sm-10 -->
																</div>
															</div>
															
														</div>	
														
														<div class="row gapLine">
															<div class="col-sm-12">
																<div class="row mgbt-xs-0">
																	<label class="col-xs-2 control-label manageFormComponent">Loan Amount</label>
																	<div class="col-xs-8 controls">Rs. <t style="color:green">${det.policy_min_amount}</t> to <t style="color:green">${det.policy_max_amount}</div>
																	<!-- col-sm-10 -->
																</div>
															</div>
															
														</div>	
														
														<div class="row gapLine">
															<div class="col-sm-12">
																<div class="row mgbt-xs-0">
																	<label class="col-xs-2 control-label manageFormComponent">Loan Duration</label>
																	<div class="col-xs-8 controls"><t style="color:green"><t style="color:green">${det.policy_min_duration}</t> to <t style="color:green">${det.policy_max_duration} months</div>
																	<!-- col-sm-10 -->
																</div>
															</div>
															
														</div>	
														
														
														<c:set var="broucher" value="${det.policy_broucher}"/>
														<% String broucher = (String)pageContext.getAttribute("broucher"); %>
														
														<c:set var="lender_id" value="${det.lender_id}"/>
														<% int  lender_id = (Integer)pageContext.getAttribute("lender_id"); %>
																			
														
														<div class="row gapLine">
															<div class="col-sm-12">
																<div class="row mgbt-xs-0">
																	<label class="col-xs-2 control-label manageFormComponent">Policy Terms</label>
																	<div class="col-xs-8 controls">
																			
										                                    <%if(broucher.equals("")){ %>
										                                                               
										                                           Terms document not available
										     								
										     								<%}else{ %>
										     								 
										                                            <a href="<%="downloadPolicyDocument?fileName="+broucher+"&id="+lender_id%>">Download</a>
										     								
										     								<%} %>
																			
																	</div>
																	<!-- col-sm-10 -->
																</div>
															</div>
															
														</div>	
														
														<c:set var="docList" value="${det.policy_required_doc}"/>
														<% String docList = (String)pageContext.getAttribute("docList"); %>
														<%String docListArray[] = docList.split(","); %>
	
														<div class="row gapLine">
															<div class="col-sm-12">
																<div class="row mgbt-xs-0">
																	<label class="col-xs-2 control-label manageFormComponent">Required Documents</label>
																	<div class="col-xs-8 controls" style="margin-left:-26px;">
																			 
																			 <ul style="list-style-type:disc">
										                                     <%for(int i=0,j=1;i<docListArray.length;i++,j++){ %>
										                                                        		
										                                  		    <li><%=docListArray[i]%> <br></li>
										                                  		    
										                                  				
										                                  	 <%} %>
										                                  	 </ul>
																	</div>
																	<!-- col-sm-10 -->
																</div>
															</div>
															
														</div>	
	
														
														<div style="width:100%;">
						                     				<ul>
								                     				
								                     				<li style="float: right; width: 8%;">
								                     				    <button  class="btn btn-info btn-fill pull-right" onclick="applyPolicy('${det.policy_id}')">Apply</button>
								                     				</li>
								                     				
								                     				<li style="width:40%;float:right;">
								                     				   <button  style="background-color:#e04b4a ! important;border-color: #e04b4a;" class="btn btn-info btn-fill pull-right" onclick="closePolicyInfo()">Close</button>
								                   	
								                     				</li>
						                     				</ul>
								                      				
								                      </div>
					                                 <div class="clearfix"></div>
					                                 
					                                 
                                 
															
													</div>
													<!-- pd-20 -->
												</div>
												
											</div>
											<!-- tab-content -->
										</div>
										<!-- tabs-widget -->
									</div>
									
								</div>
                                
                                <%-- <div class="typo-line viewFinance">
                                    <p class="category">${det.policy_type}</p>
                                    <blockquote>
                                     <p>
                                     Once you will apply this policy then our representative will notify to you,then your application will go further.
                                     </p>
                                     
                                    </blockquote>
                                </div>
                                
                                
                                 <div class="typo-line viewFinance">
                                    <p class="category">Finance Amount</p>
                                    <blockquote>
                                     <p>
                                     Amount will be in Rs. <t style="color:green">${det.policy_min_amount}</t> to <t style="color:green">${det.policy_max_amount}</t>
                                     </p>
                                  
                                    </blockquote>
                                </div>
                                
                                <div class="typo-line viewFinance">
                                    <p class="category">Finance Period</p>
                                    <blockquote>
                                     <p>
                                     Duration will be in months <t style="color:green">${det.policy_min_duration}</t> to <t style="color:green">${det.policy_max_duration}</t>
                                     </p>
                                    
                                    </blockquote>
                                </div>
                                
                                
								<div class="typo-line viewFinance" >
                                    <p class="category">Policy Terms</p>
                                    <blockquote>
                                     <p>
                                     
                                     
                                    <%if(broucher.equals("")){ %>
                                                               
                                           Terms document not available
     								
     								<%}else{ %>
     								 
                                            <a href="<%="downloadPolicyDocument?fileName="+broucher+"&id="+lender_id%>">Download</a>
     								
     								<%} %>
                                     </p>
                                      
                                    </blockquote>
                                </div>
    										
    										
    							
                                <div class="typo-line viewFinance">
                                    <p class="category">Required Documents</p>
                                    <blockquote>
                                     <p>
                                     List of scanned documents has to be submitted,
                                     </p>
                                     
                                     <%for(int i=0,j=1;i<docListArray.length;i++,j++){ %>
                                                        		
                                  		    <small>   <%=docListArray[i]%></small>
                                  				
                                  	 <%} %>
                                          
                                    </blockquote>
                                </div>
 --%>
                            
                            
                              
			                      
                            </div>
                            
                               
		                    
                        </div>
                        
		                        
		                   
                    </div>

             
  </c:forEach>
  
                  
</c:if>
                
                
</body>
</html>