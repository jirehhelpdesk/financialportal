<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>



<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>


<% String photo = "";%>
<% String name = "";%>
<% String role = "";%>
<% int id = 0;%>


<c:forEach items="${controllerData}" var="det">	
            
    <c:set var="photo" value="${det.controller_photo}"/>
    <%  photo += (String)pageContext.getAttribute("photo"); %>
    
    <c:set var="name" value="${det.controller_name}"/>
     <%  name += (String)pageContext.getAttribute("name"); %>
     
     <c:set var="role" value="${det.controller_designation}"/>
     <%  role += (String)pageContext.getAttribute("role"); %>
     
     <c:set var="id" value="${det.controller_id}"/>
     <%  id = (Integer)pageContext.getAttribute("id"); %>
          
</c:forEach>


<!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="index.html">Controller</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                           
                           <img src="${pageContext.request.contextPath}<%="/previewUserPhoto?fileName="+photo+"&id="+id+"&userType=Controller"%>"  alt="<%=name%>"/>
                           
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                
                                <img src="${pageContext.request.contextPath}<%="/previewUserPhoto?fileName="+photo+"&id="+id+"&userType=Controller"%>" alt="<%=name%>"/>
                               
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"><%=name%></div>
                                <div class="profile-data-title"><%=role%></div>
                            </div>
                            
                        </div>                                                                        
                    </li>
                    
                    <li class="xn-title">Navigation</li>
                    
                    <li id="headerMenu1"><!-- class="active"  -->
                        <a href="controllerHome"><span class="fa fa-desktop"></span> <span class="xn-text">Portal Status</span></a>                        
                    </li>                    
                    
                    <li id="headerMenu2">
                        <a href="borrowerManage" ><span class="fa fa-group"></span> <span class="xn-text">Borrower Maintenance</span></a>
                        <!-- 
                        <ul>
                            <li id="headerMenu2_1"><a href="borrowerRequest"><span class="fa fa-image"></span>Borrower Request</a></li>
                            <li id="headerMenu2_2"><a href="notifyBorrower"><span class="fa fa-user"></span> Notify Borrower</a></li>                                                   
                        </ul>
                        -->
                    </li>
                    
                    <li class="xn-openable" id="headerMenu3">
                        <a href="lenderRequest"><span class="fa fa-user-md"></span> <span class="xn-text">Lender Maintenance</span></a>
                        <ul>
                            <li id="headerMenu3_1"><a href="lenderRequest">Lender Manage</a></li>
                        	<li id="headerMenu3_2"><a href="lenderCreate">Create Lender</a></li>                            
                        </ul> 
                    </li>
                    
                    <!--  <li class="xn-title">Components</li> -->
                    
                    <li id="headerMenu4">
                        <a href="loanProcess"><span class="fa fa-briefcase"></span><span class="xn-text">Loans Master </span></a>                                              
                    </li> 
                    
                    <li id="headerMenu5">
                        <a href="policyApproval"><span class="fa fa-check-square"></span><span class="xn-text">Policies Maintenance </span></a>                                               
                    </li> 
                    
                    <li id="headerMenu6">
                        <a href="controllerSetting"><span class="fa fa-cogs"></span> <span class="xn-text">Setting</span></a>                        
                    </li>
                    
                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
</body>
</html>