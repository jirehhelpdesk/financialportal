<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lender Login</title>

    <link rel="shortcut icon" type="image/icon" href="resources/indexResources/assets/images/favicon.ico"/>
    <!-- Font Awesome -->
    <link href="resources/indexResources/assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="resources/indexResources/assets/css/bootstrap.css" rel="stylesheet">
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="resources/indexResources/assets/css/slick.css"/> 
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="resources/indexResources/assets/css/jquery.fancybox.css" type="text/css" media="screen" /> 
    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="resources/indexResources/assets/css/animate.css"/>  
     <!-- Theme color -->
    <link id="switcher" href="resources/indexResources/assets/css/theme-color/default.css" rel="stylesheet">

    <!-- Main Style -->
    <link href="resources/indexResources/style.css" rel="stylesheet">

    <!-- Fonts -->
    <!-- Open Sans for body font -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Raleway for Title -->
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <!-- Pacifico for 404 page   -->
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    
    
    
    <!-- Registration Style  -->
    		
    	<!-- <link rel="stylesheet" href="resources/indexResources/index_registration_rsc/assets/bootstrap/css/bootstrap.min.css"> -->
        <link rel="stylesheet" href="resources/indexResources/index_registration_rsc/assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="resources/indexResources/index_registration_rsc/assets/css/form-elements.css">
        <link rel="stylesheet" href="resources/indexResources/index_registration_rsc/assets/css/style.css">

    <!-- EOF Registration Style -->
    
</head>



<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<body onload="activeIndexMenu('Menu7')">

     <%@include file="index_header.jsp" %>


  <!-- Start about section -->
 
  <section id="about" style="margin-top:0px ! important;">
    
    <div class="container">
          
         <!-- Registration Block -->
      
      		
      <div class="top-content">
        	
            <div class="inner-bg">
               	        
               	                        
                    <div class="row signBox">
                    
                    
                     <div class="col-sm-1" style="width: 9.333% ! important;"></div>
                     
                     
                        <div class="col-sm-5">
		                        			
		                        		
		                        		<div class="row">
					                        <div class="Heading-align">
					                           
					                            <h1 class="signBox-Heading"> Sign Up</h1>
					                            
					                        </div>
					                    </div>    
		                    
			                            <div class="form-bottom" style="color:#00b6f5;font-size: 13px;padding: 25px 25px 9px;">
			                            		
			                            		<div class="form-group">
						                    		<label class="" for="form-username"><i class="fa fa-caret-right"></i> &nbsp; Register as Lender with free of cost.</label>
						                    		            	
						                        </div>
						                        
						                        <div class="form-group">
						                        	<label class="" for="form-password"><i class="fa fa-caret-right"></i> &nbsp; Create your own policies and start lending.</label>				                        	
						                        </div>
						                        
						                        <div class="form-group">
						                        	<label class="" for="form-password"><i class="fa fa-caret-right"></i> &nbsp; Unlimited creation of policy to lend the money.</label>				                        	
						                        </div>
						                        
						                        <div class="form-group">
						                        	<label class="" for="form-password"><i class="fa fa-caret-right"></i> &nbsp; Join us and get a new way of funding.</label>				                        	
						                        
						                        </div>
						                        
						                        
						                        <div class="form-group">
						                        	<button class="btn" style="margin-left:174px;margin-top:51px;" onclick="registerRedirect('lenderReg')">Sign Up</button>
						                    	</div>
						                    	
			                            		
			                            		<div class="form-group"> </div>
			                            		
			                            </div>
		                        		 
		                        		 
		                        </div>
                       
                       			
                       			<div class="col-sm-5">
                        	
			                        	<div class="form-box">
				                        	
				                        	
				                        	<div class="row">
						                        <div class="Heading-align">
						                           
						                            <h1 class="signBox-Heading">Sign in</h1>
						                            
						                        </div>
						                    </div>   
											
				                            <div class="form-bottom signBox-BottomDiv">
				                            
				                            <% String actionMsg = (String)request.getAttribute("actionMessage");
				                            			
				                            			if(actionMsg.equals("deactive"))
				                            			{
				                            				%>
				                            				<p class="errorMessagePara">Your account is not activated,Please check your registered email id !</p>
				                            				<%
				                            			}else if(actionMsg.equals("failed"))
				                            			{
				                            		%>
				                            				<p class="errorMessagePara">Given credential was invalidate.</p>
				                            			
				                            			<%}else if(actionMsg.equals("Not Exist"))
				                            			{
				                            		%>
				                            				<p class="activeMessagePara">Your account has been activated successfully,Please sign in.</p>
				                            			
				                            			<%}else if(actionMsg.equals("Exist"))
				                            			{
				                            		%>
				                            				<p class="successMessagePara">Your account already activated,Please sign in.</p>
				                            		  <%}else{ %>
				                            		  
				                            		        <p></p>
				                            		  <%} %>
				                            		  
				                            	
				                            	
				                              <form:form class="login-form" id="lenderRegForm" modelAttribute="lenderRegDetails" method="post" action="authLenderCredential" >
							                   		
							                   		
							                    	<div class="form-group">
							                    		<label class="" for="form-username">Email Id</label>
							                        	<input type="text" name="lender_emailid" maxlength="50"  class="form-username form-control" id="login-username" />
							                        </div>
							                        <div class="form-group">
							                        	<label class="" for="form-password">Password</label>
							                        	<form:input type="password" autocomplete="off" name="lender_password" path="lender_password" maxlength="15"  class="form-password form-control" id="login-password" />
							                        </div>
							                        
							                        <div class="linkAlign form-group">
							                             <a href="lenderForgetPwd">Forget password ?</a>
							                        </div>
							                       
							                       
							                         <div class="form-group">
									                        <button type="submit" class="btn" style="margin-left:176px;margin-top:48px;">Sign in</button>
									                 </div>
							                        
							                    	<div class="form-group"> </div>
							                    	<!-- <div class="linkAlign form-group">
							                             <a href="lenderReg">Create an account.</a>
							                        </div> -->
							                        
							                    </form:form>
							                  
					                    </div>
										
				                    </div>
				                
		                        </div>
                       
                        
                    </div>
                    
                
            </div>
            
        </div>

       
      	 <!-- EOF of registration block  -->	
      
      
      
    </div>
  </section> 
  
  
  <!-- End about section -->



        <%@include file="index_fotter.jsp" %>







  <!-- initialize jQuery Library --> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <!-- Bootstrap -->
  <script src="resources/indexResources/assets/js/bootstrap.js"></script>
  <!-- Slick Slider -->
  <script type="text/javascript" src="resources/indexResources/assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="resources/indexResources/assets/js/waypoints.js"></script>
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.counterup.js"></script>
  <!-- mixit slider -->
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->        
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.fancybox.pack.js"></script>
  <!-- Wow animation -->
  <script type="text/javascript" src="resources/indexResources/assets/js/wow.js"></script> 

  <!-- Custom js -->
  <script type="text/javascript" src="resources/indexResources/assets/js/custom.js"></script>
    
    
  <script type="text/javascript" src="resources/indexResources/index_registration_script.js"></script> 
 
    
      <!-- Registration script -->
    
        <script src="resources/indexResources/index_registration_rsc/assets/js/jquery-1.11.1.min.js"></script>
        <script src="resources/indexResources/index_registration_rsc/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="resources/indexResources/index_registration_rsc/assets/js/scripts.js"></script>
     		
     <!-- EOF Registration script -->
     
      		
  </body>
</html>