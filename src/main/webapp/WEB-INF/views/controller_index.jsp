<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Controller Home</title>

 <!-- META SECTION -->
           
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="resources/adminResources/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->     
        
</head>


<body onload="activeSideMenu('menu1',0)">


 <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           
           
            <!-- START PAGE SIDEBAR -->
              
              <%@include file="controller_side_bar.jsp" %>
            
            <!-- END PAGE SIDEBAR -->
            
            
            
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
               
               
               
                
                <!-- START X-NAVIGATION VERTICAL -->
                    
                    <%@include file="controller_header_bar.jsp" %>
                
                <!-- END X-NAVIGATION VERTICAL -->                     




                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>                    
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                       
                
                
                
                
                
                <!-- PAGE CONTENT WRAPPER -->
                
                
                <div class="page-content-wrap">
                    
                   
                    
                    <!-- START WIDGETS -->                    
                   
                   
                   
                     <div class="row">
                        
                        
                        
                        <div class="col-md-3">
                            
                            <!-- START WIDGET MESSAGES -->
	                             <div class="widget widget-default widget-item-icon" >
	                                <div class="widget-item-left">
	                                    <span class="fa fa-users" style="color:green;"></span>
	                                </div>                             
	                                <div class="widget-data">
	                                    <div class="widget-int num-count">${portalStatus.noOfBorrower}</div>
	                                    <div class="widget-title">No.of Borrower</div>
	                                    <div class="widget-subtitle">Total registered borrower</div>
	                                </div>      
	                                <div class="widget-controls">                                
	                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
	                                </div>
	                             </div>                            
                            <!-- END WIDGET MESSAGES -->
                            
                        </div>
                        
                        
                        <div class="col-md-3">
                            
                            <!-- START WIDGET REGISTRED -->
                            <div class="widget widget-default widget-item-icon" >
                                <div class="widget-item-left">
                                    <span class="fa fa-users" style="color:green;"></span>
                                </div>
                                <div class="widget-data">
                                    <div class="widget-int num-count">${portalStatus.noOfLender}</div>
                                    <div class="widget-title">No.of Lender</div>
                                    <div class="widget-subtitle">Total registered Lender</div>
                                </div>
                                <div class="widget-controls">                                
                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                                </div>                            
                            </div>                            
                            <!-- END WIDGET REGISTRED -->
                            
                        </div>
                       
                       
                       <div class="col-md-3">
                            
                            <!-- START WIDGET REGISTRED -->
	                            <div class="widget widget-default widget-item-icon" >
	                                <div class="widget-item-left">
	                                   <span class="fa fa-search" style="color:green;"></span>
	                                </div>
	                                <div class="widget-data">
	                                    <div class="widget-int num-count">${portalStatus.noOfOnSearchLoans}</div>
	                                    <div class="widget-title">No.of searched loan</div>
	                                    <div class="widget-subtitle">Total no.of searched loan</div>
	                                </div>
	                                <div class="widget-controls">                                
	                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
	                                </div>                            
	                            </div>                            
                            <!-- END WIDGET REGISTRED -->
                            
                        </div>
                        
                        <div class="col-md-3">
                            
                            <!-- START WIDGET CLOCK -->
                            <div class="widget widget-info widget-padding-sm" style="min-height:141px;">
                                <div class="widget-big-int plugin-clock">00:00</div>                            
                                <div class="widget-subtitle plugin-date">Loading...</div>
                                <div class="widget-controls">                                
                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="left" title="Remove Widget"><span class="fa fa-times"></span></a>
                                </div>                            
                                                     
                            </div>                        
                            <!-- END WIDGET CLOCK -->
                            
                        </div>
                    </div>
                    
                    
                    <!-- DEAL STATUS  -->
                    
                    <div class="row">
                        
                        <div class="col-md-3">
                            
                            <!-- START WIDGET REGISTRED -->
	                            <div class="widget widget-default widget-item-icon" >
	                                <div class="widget-item-left">
	                                   <span class="fa fa-briefcase" style="color:#ff6666;"></span>
	                                </div>
	                                <div class="widget-data">
	                                    <div class="widget-int num-count">${portalStatus.noOfAppliedDeals}</div>
	                                    <div class="widget-title">No.of Pending Request</div>
	                                    <div class="widget-subtitle">Total pending request for loan</div>
	                                </div>
	                                <div class="widget-controls">                                
	                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
	                                </div>                            
	                            </div>                            
                            <!-- END WIDGET REGISTRED -->
                            
                        </div>
                        
                        
                        <div class="col-md-3">
                            
                            <!-- START WIDGET REGISTRED -->
	                            <div class="widget widget-default widget-item-icon" >
	                                <div class="widget-item-left">
	                                   <span class="fa fa-briefcase" style="color:#ff6666;"></span>
	                                </div>
	                                <div class="widget-data">
	                                    <div class="widget-int num-count">${portalStatus.noOfProcessingDeals}</div>
	                                    <div class="widget-title">No.of Processing Request</div>
	                                    <div class="widget-subtitle">Total processing request for loan</div>
	                                </div>
	                                <div class="widget-controls">                                
	                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
	                                </div>                            
	                            </div>                            
                            <!-- END WIDGET REGISTRED -->
                            
                        </div>
                        
                        <div class="col-md-3">
                            
                            <!-- START WIDGET REGISTRED -->
	                            <div class="widget widget-default widget-item-icon" >
	                                <div class="widget-item-left">
	                                   <span class="fa fa-briefcase" style="color:#ff6666;"></span>
	                                </div>
	                                <div class="widget-data">
	                                    <div class="widget-int num-count">${portalStatus.noOfOnholdDeals}</div>
	                                    <div class="widget-title">No.of Onhold Request</div>
	                                    <div class="widget-subtitle">Total onhold request for loan</div>
	                                </div>
	                                <div class="widget-controls">                                
	                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
	                                </div>                            
	                            </div>                            
                            <!-- END WIDGET REGISTRED -->
                            
                        </div>
                        
                        
                        <div class="col-md-3">
                            
                            <!-- START WIDGET REGISTRED -->
	                            <div class="widget widget-default widget-item-icon" >
	                                <div class="widget-item-left">
	                                   <span class="fa fa-briefcase" style="color:#ff6666;"></span>
	                                </div>
	                                <div class="widget-data">
	                                    <div class="widget-int num-count">${portalStatus.noOfFinalizedDeals}</div>
	                                    <div class="widget-title">No.of Approved Request</div>
	                                    <div class="widget-subtitle">Total approed request for loan</div>
	                                </div>
	                                <div class="widget-controls">                                
	                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
	                                </div>                            
	                            </div>                            
                            <!-- END WIDGET REGISTRED -->
                            
                        </div>
                        
                        
                        
                        
                    </div>
                    
                    
                    
                    
                    <!--  POLICY STATUS   -->
                    
                    
                    
                    
                    <div class="row">
                        
                        <div class="col-md-3">
                            
                            <!-- START WIDGET REGISTRED -->
	                            
	                            <div class="widget widget-default widget-item-icon" >
	                                <div class="widget-item-left">
	                                   <span class="fa fa-book" style="color:#3399ff;"></span>
	                                </div>
	                                <div class="widget-data">
	                                    <div class="widget-int num-count">${portalStatus.noOfPolicy}</div>
	                                    <div class="widget-title">No.of Policy</div>
	                                    <div class="widget-subtitle">Total no.of policy created</div>
	                                </div>
	                                <div class="widget-controls">                                
	                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
	                                </div>                            
	                            </div>  
	                                                      
                            <!-- END WIDGET REGISTRED -->
                            
                        </div>
                        
                        
                        <div class="col-md-3">
                            
                            <!-- START WIDGET REGISTRED -->
	                            <div class="widget widget-default widget-item-icon" >
	                                <div class="widget-item-left">
	                                   <span class="fa fa-book" style="color:#3399ff;"></span>
	                                </div>
	                                <div class="widget-data">
	                                    <div class="widget-int num-count">${portalStatus.noOfPendingPolicy}</div>
	                                    <div class="widget-title">No.of Pending Policy</div>
	                                    <div class="widget-subtitle">Total pending policies</div>
	                                </div>
	                                <div class="widget-controls">                                
	                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
	                                </div>                            
	                            </div>                            
                            <!-- END WIDGET REGISTRED -->
                            
                        </div>
                        
                        <div class="col-md-3">
                            
                            <!-- START WIDGET REGISTRED -->
	                            <div class="widget widget-default widget-item-icon" >
	                                <div class="widget-item-left">
	                                   <span class="fa fa-book" style="color:#3399ff;"></span>
	                                </div>
	                                <div class="widget-data">
	                                    <div class="widget-int num-count">${portalStatus.noOfApprovedPolicy}</div>
	                                    <div class="widget-title">No.of Approved Policy</div>
	                                    <div class="widget-subtitle">Total approved policies</div>
	                                </div>
	                                <div class="widget-controls">                                
	                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
	                                </div>                            
	                            </div>                            
                            <!-- END WIDGET REGISTRED -->
                            
                        </div>
                        
                        
                        <div class="col-md-3">
                            
                            <!-- START WIDGET REGISTRED -->
	                            <div class="widget widget-default widget-item-icon" >
	                                <div class="widget-item-left">
	                                   <span class="fa fa-book" style="color:#3399ff;"></span>
	                                </div>
	                                <div class="widget-data">
	                                    <div class="widget-int num-count">${portalStatus.noOfClosedPolicy}</div>
	                                    <div class="widget-title">No.of Closed Policy</div>
	                                    <div class="widget-subtitle">Total closed policies</div>
	                                </div>
	                                <div class="widget-controls">                                
	                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
	                                </div>                            
	                            </div>                            
                            <!-- END WIDGET REGISTRED -->
                            
                        </div>
                        
                        
                        
                        
                    </div>
                    
                    
                    <!-- END WIDGETS -->                    
                    
                    
                    
                    
			        <!-- FOOTER CONTENT -->
       
			         <%@include file="admin_footer.jsp" %>
			       
			       <!-- END FOOTER CONTENT -->
                    
                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            
            
            </div>            
            <!-- END PAGE CONTENT -->
       
        </div>
        <!-- END PAGE CONTAINER -->

       
       
       
       
       
       <!-- BELOW COMMON CONTENT -->
       
         <%@include file="controller_down_common.jsp" %>
       
       <!-- END BELOW COMMON CONTENT -->
       
	
    </body>
</html>