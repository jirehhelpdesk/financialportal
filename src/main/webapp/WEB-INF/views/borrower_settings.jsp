<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>My Profile</title>

    <link href="resources/borrower_lender_resources/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="resources/borrower_lender_resources/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="resources/borrower_lender_resources/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="resources/borrower_lender_resources/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    
    <link rel="stylesheet" type="text/css" href="resources/datePicker/jquery.datetimepicker.css"/>
    
    
</head>


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body onload="activeSideMenu('menu2')">


<div class="wrapper">
    
    
      <!-- Start of Sidemenu Division -->
    
         <%@include file="borrower_sidemenu.jsp" %>

      <!-- End of Sidemenu Division -->


    <div class="main-panel">
        
        
         <!-- Start of Header Division -->
         
         <%@include file="borrower_header.jsp" %>
        
		 <!-- End of Header Division -->

       
       
        <div class="content">
          
        
        		<div class="container-fluid">
                
                
                <div class="row">
                
                   <div class="col-md-8" style="width:100%;">
                
                		
                		<div class="card">
                            <div class="header">
                                <h4 class="title">Profile Details</h4>
                            </div>
                            <div class="content">
                                
                                
                                <form:form id="borrowerIdnForm" modelAttribute="borrowerIdnDetails" >
                                    
                                    
                                     <label style="color:#00b6f5;">Present Address</label>
                                    
                                     <div class="row">
                                    	
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Door No</label>
                                                <input type="text" id="preDoorId" class="form-control" value="${borrowerBean.borrower_present_door}" name="borrower_present_door" path="borrower_present_door" placeholder="Door number" >
                                            	<div id="preDoorErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Building Name</label>
                                                <input type="text" id="preBuildingId" class="form-control" value="${borrowerBean.borrower_present_building}" name="borrower_present_building" path="borrower_present_building" placeholder="Building name" >
                                            	<div id="preBuildingErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                         <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Street Name</label>
                                                <input type="text" id="preStreetId" class="form-control" value="${borrowerBean.borrower_present_street}" name="borrower_present_street" path="borrower_present_street" placeholder="Street name" >
                                            	<div id="preStreetErrId" class="errorStyle"></div>
                                            </div>
                                         </div>
                                       
                                      </div> 
                                      
                                      <div class="row" style="margin-top:0px;"> 
                                         
                                         <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Area Name</label>
                                                <input type="text" id="preAreaId" class="form-control" value="${borrowerBean.borrower_present_area}" name="borrower_present_area" path="borrower_present_area" placeholder="Area name" >
                                            	<div id="preAreaErrId" class="errorStyle"></div>
                                            </div>
                                         </div>
                                         
                                         <div class="col-md-4">
                                            <div class="form-group">
                                                <label>City Name</label>
                                                <input type="text" id="preCityId" class="form-control" value="${borrowerBean.borrower_present_city}" name="borrower_present_city" path="borrower_present_city" placeholder="City name" >
                                            	<div id="preCityErrId" class="errorStyle"></div>
                                            </div>
                                         </div>
                                         
                                         <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Pincode</label>
                                                <input type="text" id="prePincodeId" class="form-control" value="${borrowerBean.borrower_present_pincode}" name="borrower_present_pincode" path="borrower_present_pincode" maxlength="6" placeholder="Present pincode" >
                                            	<div id="prePinErrId" class="errorStyle"></div>
                                            </div>
                                         </div>
                                         
                                        
                                    </div>
                                    
                                    
                                    <label style="color:#00b6f5;margin-top:20px;">Permanent Address</label>   
                                    
                                    <l style="font-size: 13px;color:rgb(0, 182, 245); margin-left: 52px;"> <input type="checkbox" id="checkId"  onclick="copyToPermanent()"/> Do the same</l>
                                    
                                    <div class="row">
                                    	
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Door No</label>
                                                <input type="text" id="perDoorId" class="form-control" value="${borrowerBean.borrower_permanent_door}" name="borrower_permanent_door" path="borrower_permanent_door" placeholder="Door number" >
                                            	<div id="perDoorErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Building Name</label>
                                                <input type="text" id="perBuildingId" class="form-control" value="${borrowerBean.borrower_permanent_building}" name="borrower_permanent_building" path="borrower_permanent_building" placeholder="Building name" >
                                            	<div id="perBuildingErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                         <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Street Name</label>
                                                <input type="text" id="perStreetId" class="form-control" value="${borrowerBean.borrower_permanent_street}" name="borrower_permanent_street" path="borrower_permanent_street" placeholder="Street name" >
                                            	<div id="perStreetErrId" class="errorStyle"></div>
                                            </div>
                                         </div>
                                         
                                         
                                       </div>
                                       
                                       <div class="row">
                                       
                                         <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Area Name</label>
                                                <input type="text" id="perAreaId" class="form-control" value="${borrowerBean.borrower_permanent_area}" name="borrower_permanent_area" path="borrower_permanent_area" placeholder="Area name" >
                                            	<div id="perAreaErrId" class="errorStyle"></div>
                                            </div>
                                         </div>
                                         
                                         <div class="col-md-4">
                                            <div class="form-group">
                                                <label>City Name</label>
                                                <input type="text" id="perCityId" class="form-control" value="${borrowerBean.borrower_permanent_city}" name="borrower_permanent_city" path="borrower_permanent_city" placeholder="City name" >
                                            	<div id="perCityErrId" class="errorStyle"></div>
                                            </div>
                                         </div>
                                         
                                         <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Pincode</label>
                                                <input type="text" id="perPincodeId" class="form-control" value="${borrowerBean.borrower_permanent_pincode}" name="borrower_permanent_pincode" path="borrower_permanent_pincode" maxlength="6" placeholder="Permanent pincode" >
                                            	<div id="perPinErrId" class="errorStyle"></div>
                                            </div>
                                         </div>
                                         
                                        
                                        </div>
                                    
                                    
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                
                                                <label>Date of Birth</label>
                                                
	                                                <c:if test="${borrowerBean.borrower_dob !=''}">
																							 
	                                                 <input type="text" class="form-control" readonly value="${borrowerBean.borrower_dob}" />
	                                               	 <input type="hidden" id="dobId" class="some_class form-control" readonly name="borrower_dob" path="borrower_dob" placeholder="Date of birth" value="${borrowerBean.borrower_dob}" id="some_class_1"/>
	                                               	 
	                                               	</c:if>
	                                               	
	                                               	<c:if test="${borrowerBean.borrower_dob ==''}">
																							 
	                                                 <input type="text" id="dobId" class="some_class form-control" readonly name="borrower_dob" path="borrower_dob" placeholder="Date of birth" value="${borrowerBean.borrower_dob}" id="some_class_1"/>
	                                               	
	                                               	</c:if>
	                                               	
	                                               	<div id="dobErrId" class="errorStyle"></div>
	                                               	
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            <div class="form-group">
                                               <label>Identity Id Type</label>
                                                <select class="form-control" id="idTypeId" name="borrower_identity_id_type" path="borrower_identity_id_type" >
                                                		<option value="Select Type of Id">Select Type of Id</option>
                                                		<option value="Pan card">Pan card</option>
                                                		<option value="Voter Id">Voter Id</option>
                                                		<option value="Aadhar Card">Aadhar Card</option>
                                                		<option value="Employee Card">Employee Card</option>
                                                </select>
                                                <div id="idTypeErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                       
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Identity Id</label>
                                                <input type="text" id="idNoId" class="form-control" name="borrower_identity_card_id" path="borrower_identity_card_id"  placeholder="Enter Id no" value="${borrowerBean.borrower_identity_card_id}" />
                                            	<div id="idErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Alternate mobile no</label>
                                                <input type="text" id="altPhnoId" name="borrower_alt_phno" value="${borrowerBean.borrower_alt_phno}" placeholder="Alternate mobile no" path="borrower_alt_phno" class="form-control" name="file" >
                                            	<div id="altPhnNoErrId" class="errorStyle"></div>
                                            </div>
                                        </div> 
                                        
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Profile Picture</label>
                                                <input type="file" id="file" class="form-control" name="file" >
                                            	<div id="photoErrId" class="errorStyle"></div>
                                            </div>
                                        </div> 
                                                                             
                                    </div>
                                    
                                    
                                    <input type="button" value="Update"  class="btn btn-info btn-fill pull-right" onclick="saveBorrowerIdnDetails()">
                                    
                                    <div class="clearfix"></div>
                                    
                                </form:form>
                                
                                
                            </div>
                            
                        </div>
                        
                    </div>
                       
                </div>
                
                
            </div>
        		
        		
        		<div class="container-fluid">
                
                
                <div class="row">
                
                   <div class="col-md-8" style="width:100%;">
                
                		
                		<div class="card">
                            <div class="header">
                                <h4 class="title">Change Password</h4>
                            </div>
                            <div class="content">
                                <form id="passwordForm">
                                    
                                    <div class="row">
                                    	
                                    	<div class="col-md-4">
                                            <div class="form-group">
                                                <label>Current Password</label>
                                                <input type="password" autocomplete="off" class="form-control" placeholder="Current Password" name="curPassword" id="curPasswordId" >
                                                <div id="curPasswordErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>New Password</label>
                                                <input type="password" autocomplete="off" class="form-control" placeholder="New Password" name="password" id="passwordId" >
                                                <div id="passwordErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Confirm Password</label>
                                                <input type="password" autocomplete="off" class="form-control" placeholder="Confirm Password" name="cnfPassword"  id="cnfPasswordId">
                                                <div id="cnfPasswordErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                    </div>

                                </form>
                                
                                
                                    <button type="button" class="btn btn-info btn-fill pull-right" onclick="changePassword()">Update</button>
                                    <div class="clearfix"></div>
                                    
                            </div>
                        </div>
                        
                        
                        </div>
                </div>
                
                
                
                
                
                
            </div>
        		
        		
        </div>

       <!-- Start of Footer Division -->
 
         <%@include file="borrower_footer.jsp" %>
        
	   <!-- Start of Footer Division -->
	   
	   
    </div>
</div>


</body>
      
       <!-- Start of Common Division -->
 
         <%@include file="common_content.jsp" %>
        
	   <!-- Start of Common Division -->   
   
	
</html>
