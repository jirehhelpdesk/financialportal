<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>


    <!-- Favicon -->
    <link rel="shortcut icon" type="image/icon" href="assets/images/favicon.ico"/>
    <!-- <link href="http://jirehsol.co.in/img/favicon.ico" rel="shortcut icon" type="image/x-icon"> -->
    <!-- Font Awesome -->
    <link href="resources/indexResources/assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="resources/indexResources/assets/css/bootstrap.css" rel="stylesheet">
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="resources/indexResources/assets/css/slick.css"/> 
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="resources/indexResources/assets/css/jquery.fancybox.css" type="text/css" media="screen" /> 
    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="resources/indexResources/assets/css/animate.css"/>  
     <!-- Theme color -->
    <link id="switcher" href="resources/indexResources/assets/css/theme-color/default.css" rel="stylesheet">

    <!-- Main Style -->
    <link href="resources/indexResources/style.css" rel="stylesheet">

   	
   	<link href='resources/indexResources/index_slider/slider.css' rel='stylesheet' type='text/css'>
   	
   	
   	
</head>
<body>


  <!-- Start Footer -->   
   
  <footer id="footer">
    
    <div class="footer-content">
		<div class="container">
			<div class="row">
				
				
				<div class="footer-col links col-md-3 col-sm-3 col-xs-12">
					<div class="footer-col-inner">
						<h3 class="title" style="color:#00b6f5 ! important;">About Portal</h3>
						<ul class="list-unstyled" style="color:#fff ! important;">
							<li><a href="index"><i class="fa fa-caret-right"></i>&nbsp;Home</a></li>
							<li><a href="services"><i class="fa fa-caret-right"></i>&nbsp;Service</a></li>
							<li><a href="aboutUs"><i class="fa fa-caret-right"></i>&nbsp;About us</a></li>
							<li><a href="contactUs"><i class="fa fa-caret-right"></i>&nbsp;Contact Us</a></li>													
						</ul>
					</div>
					<!--//footer-col-inner-->
				</div>
				<!--//foooter-col-->
				
				
				<div class="footer-col links col-md-3 col-sm-3 col-xs-12">
					<div class="footer-col-inner">
						<h3 class="title" style="color:#00b6f5 ! important;">Lenders</h3>
						<ul class="list-unstyled footerHeading" style="color:#fff ! important;">
							<li><a href="whyLender?div=whyLender" ><i class="fa fa-caret-right"></i>&nbsp;Why Lenders</a></li>
							<li><a href="whyLender?div=lenEligibility" ><i class="fa fa-caret-right"></i>&nbsp;Lenders Eligibility</a></li>
							<li><a href="whyLender?div=howToLend" ><i class="fa fa-caret-right"></i>&nbsp;How to lend</a></li>							
							<li><a href="whyLender?div=lenFaq"><i class="fa fa-caret-right"></i>&nbsp;Lenders FAQ</a></li>
							<li><a href="whyLender?div=tutorial"><i class="fa fa-caret-right"></i>&nbsp;Tutorial for Lenders</a></li>
						</ul>
					</div>
					<!--//footer-col-inner-->
				</div>
				<!--//foooter-col-->
				
				
				<div class="footer-col links col-md-3 col-sm-3 col-xs-12">
					<div class="footer-col-inner">
						<h3 class="title" style="color:#00b6f5 ! important;">Borrowers</h3>
						<ul class="list-unstyled" style="color:#fff ! important;">
							<li><a href="whyBorrower?div=whyBorrower" onclick="footerClick(1)"><i class="fa fa-caret-right"></i>&nbsp;Why Borrow</a></li>
							<li><a href="whyBorrower?div=bowEligibility" onclick="footerClick(2)"><i class="fa fa-caret-right"></i>&nbsp;Borrowers Eligibility</a></li>				
							<li><a href="whyBorrower?div=purposeLoan" onclick="footerClick(4)"><i class="fa fa-caret-right"></i>&nbsp;Purpose of Loan</a></li>							
							<li><a href="whyBorrower?div=BowFaq"><i class="fa fa-caret-right"></i>&nbsp;Borrower FAQ</a></li>
							<li><a href="whyBorrower?div=bowTutorial"><i class="fa fa-caret-right"></i>&nbsp;Tutorial for Borrowers</a></li>
						</ul>
					</div>
					<!--//footer-col-inner-->
				</div>
				
				
				<div class="footer-col links col-md-2 col-sm-3 col-xs-12 sm-break">
					<div class="footer-col-inner">
						<h3 class="title" style="color:#00b6f5 ! important;">Legal</h3>
						<ul class="list-unstyled" style="color:#fff ! important;">
							<li><a href="termsConditions"><i class="fa fa-caret-right"></i>&nbsp;Terms of use</a></li>
							<li><a href="privacyPolicy"><i class="fa fa-caret-right"></i>&nbsp;Privacy policy</a></li>
						</ul>
					</div>
					<!--//footer-col-inner-->
				</div>

				<!--//foooter-col-->
				<div class="clearfix"></div>
			</div>
		</div>
		<!--//container-->
	</div>
    
    
    
    <div class="footer-top">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="footer-top-area">             
                <!-- <a class="footer-logo" href="#"><img src="resources/indexResources/assets/images/logo.jpg" alt="Logo"></a>     -->          
              <div class="footer-social">
                <a class="facebook" href="http://www.facebook.com" target="_"><span class="fa fa-facebook"></span></a>
                <a class="twitter" href="http://www.twitter.com" target="_"><span class="fa fa-twitter"></span></a>               
                <a class="youtube" href="http://www.youtube.com" target="_"><span class="fa fa-youtube"></span></a>
                <a class="linkedin" href="http://www.linkedin.com" target="_"><span class="fa fa-linkedin"></span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    
    <div class="footer-bottom">
      	<p>� 2016 - All rights reserved & developed by <t class="cmyLink" onclick="redirectToWebsite('http://jirehsol.co.in')">Jireh</t></p>
    </div>
    
    
  </footer>  
  
  <!-- End Footer -->
	
	
	<div id="preloader" style="display: none;">
        <div id="loader" class="loader" style="display: none;">&nbsp;</div>
    </div>
  

  	<%-- <div id="successId" style="display:none;"><%@include file="success_message.jsp" %></div>	 --%>
	 
	 
	 <div id="successId" style="display:block;">
	 
	 	<input type="hidden" id="action" />
	 
	    <input type="hidden" id="value" />
	
	 	<%@include file="popUp_1.jsp"%>
	 
	 </div>
	 
	<script type="text/javascript" src="resources/indexResources/js/index_custom_script.js"></script>
			
</body>
</html>