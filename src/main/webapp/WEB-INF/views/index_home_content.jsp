
	<style>

	.gapLine
	{
		margin-bottom:10px;
	}
	</style>


	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<link href="resources/borrower_lender_resources/trackProcess/css/style.css" rel="stylesheet" type="text/css" media="all" />

   	
 

<section id="about">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <!-- Start welcome area -->
          <div class="welcome-area" id="serviceDiv">
            
            
            
            <div class="title-area">
              <h2 class="tittle">Services</h2>
              <p></p>
            </div>
            
            
            
            <div  class="item row" style="margin-left: 0; margin-right: 0;">
				
			   
			   <div class="content col-md-6 col-sm-12 col-xs-12" >				<!--  style="border-right: 1px solid #bbb;" -->
				
				<div class="single-wc-content wow fadeInUp">
                    <img src="resources/indexResources/indexImages/Borrower.png" style="margin-left:422px;margin-bottom: 10px;"/>                    
                </div>
                  	
				<h5 class="forceCenter title" style="color:#00b6f5;font-size:22px;text-align:right;margin-right: 44px;" onclick="">Need a loan ?</h5>
				
				<div class="desc" style="border-right: 1px solid #bbb;margin-right: -16px;">
					<ul class="homePageUl mainColor autoMargin newWidth list-unstyled" style="text-align: right;margin-right:17px;">
						<li>Register as a 'Borrower' with free of cost &nbsp;<i class="fa fa-caret-left"></i></li>
						<li>Fill the required details for verification &nbsp;<i class="fa fa-caret-left"></i></li>
						<li>Search and apply loan as per the need &nbsp;<i class="fa fa-caret-left"></i></li>
						<li>Check your application status &nbsp;<i class="fa fa-caret-left"></i></li>
						<li>Get loan from lender in easy and effective way &nbsp;<i class="fa fa-caret-left"></i></li><!-- fa-square-o -->
						
					</ul>
				</div>
					
				<!-- <div class="text-center register-btn">
					<a class="btn btn-cta btn-cta-primary btn-home-margin" href="borrowerReg"><button class="btn">Register as Borrower</button></a>
					<div class="press-links text-center">
						<a href="borrowers">Learn More</a>
					</div>
				</div> -->
				
				</div>
			
			
				<div class="content col-md-6 col-sm-12 col-xs-12">
					
					<div class="single-wc-content wow fadeInUp" style="float:center;">
	                    <img src="resources/indexResources/indexImages/Lender.png" style="margin-left:68px;margin-bottom: 10px;"/>                    
	               </div>
               
					<h5 class="forceCenter title" style="color:#00b6f5;font-size:22px;text-align:left;margin-left: 25px;">Want to be a Lender ?</h5>
					
					<div class="desc">
						<ul class="homePageUl mainColor autoMargin newWidth list-unstyled" style="margin-left:0px;">
							<li><i class="fa fa-caret-right"></i>&nbsp; Register as a 'Lender' with free of cost or contact us for registration</li>
							<li><i class="fa fa-caret-right"></i>&nbsp; Lender can be individual person,Bank or any kind of financial organization</li>
							<li><i class="fa fa-caret-right"></i>&nbsp; Create your own financial policy and set your amount and interest rate</li>
							<li><i class="fa fa-caret-right"></i>&nbsp; Get verified lead or borrower for loan </li>
							<li><i class="fa fa-caret-right"></i>&nbsp; Lend your money on right borrower </li>
						</ul>
					</div>
					
					<!-- <div class="text-center register-btn">
						<a class="btn btn-cta btn-cta-primary btn-home-margin" href="lenderReg"><button class="btn">Register as Lender</button></a>
						<div class="press-links text-center">
							<a href="lenders">Learn More</a>
						</div>
					</div> -->
				</div>
			<!-- <!--//content-->

			
				
				
			</div>
			
		
		
            <div class="welcome-content">
            
            <div class="title-area">
              <h2 class="tittle">Our Process</h2>             
              <p></p>
            </div>

			

						<div style="background-color: #fff;margin-left:88px;">

							<div class="content3">

								<div class="shipment">

									<div class="confirm">
										<div class="imgcircle processCircle" style="background-color: #fff;border:1px solid #007acc;">
											<img
												src="resources/borrower_lender_resources/trackProcess/images/apply1.png"
												alt="confirm order">
										</div>
										<span class="processLine line"></span>
										<p class="processTag">Registration</p>
									</div>

									<div class="process">
										<div class="imgcircle processCircle" style="background-color: #fff;border:1px solid #007acc;">
											<img
												src="resources/borrower_lender_resources/trackProcess/images/interest1.png"
												alt="process order">
										</div>
										<span class="processLine line"></span>
										<p class="processTag">Verification</p>
									</div>

									<div class="process">
										<div class="imgcircle processCircle" style="background-color: #fff;border:1px solid #007acc;">
											<img
												src="resources/borrower_lender_resources/trackProcess/images/view2.png"
												alt="quality check">
										</div>
										<span class="processLine line"></span>
										<p class="processTag">Under Process</p>
									</div>

									<div class="process">
										<div class="imgcircle processCircle" style="background-color: #fff;border:1px solid #007acc;">
											<img src="resources/borrower_lender_resources/trackProcess/images/approve4.png" alt="delivery">
										</div>
										<span class="processLine line" ></span>
										<p class="processTag">Approved</p>
									</div>

									<div class="process">
										<div class="imgcircle processCircle" style="background-color: #fff;border:1px solid #007acc;">
											<img src="resources/borrower_lender_resources/trackProcess/images/amount4.png" alt="delivery">
										</div>
										<p class="processTag">Amount Disposed</p>
									</div>


									<div class="clear"></div>
								</div>
							</div>
						</div>


				<!-- <ul class="wc-table">
                <li>
                  <div class="single-wc-content wow fadeInUp">
                    <span class="fa fa-users wc-icon"></span>
                    <h4 class="wc-tittle">Registration</h4>
                    <p>Registration with free of cost</p>
                  </div>
                </li>
				
                <li>
                  <div class="single-wc-content wow fadeInUp">
                    <span class="fa fa-remote wc-icon"></span>
                    <h4 class="wc-tittle">Verification</h4>
                    <p>Verified users will provide </p>
                  </div>
                </li>
				
                <li>
                  <div class="single-wc-content wow fadeInUp">
                    <span class="fa fa-gear wc-icon"></span>
                    <h4 class="wc-tittle">Process</h4>
                    <p>Lender can verified too </p>
                  </div>
                </li>
				
                <li>				
                  <div class="single-wc-content wow fadeInUp">
                    <span class="fa fa-mark wc-icon"></span>
                    <h4 class="wc-tittle">Approve</h4>
                    <p>Loan approval with in time</p>
                  </div>				  
                </li>
                
                <li>
				
                  <div class="single-wc-content wow fadeInUp">
                    <span class="fa fa-money wc-icon"></span>
                    <h4 class="wc-tittle">Dispose Money</h4>
                    <p>Amount will to borrower by lender </p>
                  </div>
				  
                </li>
                
                
              </ul> 
               -->
              
            </div>
          </div>
		  
          <!-- End welcome area -->
        </div>
      </div>
      
      
      
      
      
    </div>
  </section> 
 