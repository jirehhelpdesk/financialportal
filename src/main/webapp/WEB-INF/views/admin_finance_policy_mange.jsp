<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" href="resources/paging/other.css">

</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>



<body>

<c:if test="${!empty policyBean}">	

<c:forEach items="${policyBean}" var="det">	
                                 
<div class="row">
                        <div class="col-md-12">
                            
                            <div class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-heading ui-draggable-handle">
                                    <h3 class="panel-title"><strong>Policy Details of ${det.policy_name}</strong> </h3>
                                   
                                </div>
                                
                                <div class="panel-body">                                                                        
                                    
                                    <div class="row">
                                        
                                        
                                        
                                        
                                        <div class="col-md-6">
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Lender Name</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-check-square-o"></span></span>
                                                        <div class="form-control">${det.lender_name}</div>
                                                    </div>                                            
                                                   
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Lender Email Id</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-check-square-o"></span></span>
                                                        <div class="form-control">${det.lender_emailId}</div>
                                                    </div>                                            
                                                   
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Policy Name</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-check-square-o"></span></span>
                                                        <div class="form-control">${det.policy_name}</div>
                                                    </div>                                            
                                                   
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Policy Type</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-check-square-o"></span></span>
                                                        <div class="form-control">${det.policy_type}</div>
                                                    </div>                                            
                                                   
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Interest Rate</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-check-square-o"></span></span>
                                                        <div class="form-control">${det.policy_interest_rate}</div>
                                                    </div>                                            
                                                   
                                                </div>
                                            </div>
                                            
                                            <c:set var="broucher" value="${det.policy_broucher}"/>
    										<% String broucher = (String)pageContext.getAttribute("broucher"); %>
    										
    										<c:set var="lender_id" value="${det.lender_id}"/>
    										<% int  lender_id = (Integer)pageContext.getAttribute("lender_id"); %>
    										
    										
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Policy Terms</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-check-square-o"></span></span>
                                                        <div class="form-control">
                                                        
                                                            <%if(broucher.equals("")){ %>
                                                               
                                                                  Terms document not available
		                      								
		                      								<%}else{ %>
		                      								 
                                                               <a href="<%="downloadPolicyDocument?fileName="+broucher+"&id="+lender_id%>">Download</a>
		                      								
		                      								<%} %>
                                                        </div>
                                                    </div>                                            
                                                   
                                                </div>
                                            </div>
                                            
                                        </div>
                                        
                                        
                                        
                                        <div class="col-md-6">
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Duration Limit</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-check-square-o"></span></span>
                                                        <div class="form-control">${det.policy_min_duration} - ${det.policy_max_duration}</div>
                                                    </div>                                            
                                                   
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Amount Limit</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-check-square-o"></span></span>
                                                        <div class="form-control">${det.policy_min_amount} - ${det.policy_max_amount}</div>
                                                    </div>                                            
                                                   
                                                </div>
                                            </div>
                                            
                                            <c:set var="docList" value="${det.policy_required_doc}"/>
    										<% String docList = (String)pageContext.getAttribute("docList"); %>
    										<%String docListArray[] = docList.split("/"); %>
    
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Required Document</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"></span>
                                                        
                                                        <div class="form-control" style="height: auto;">
                                                        
                                                        		<%for(int i=0,j=1;i<docListArray.length;i++,j++){ %>
                                                        		
                                                        		    <%=j%> - <%=docListArray[i]%><br>
                                                        				
                                                        		<%} %>
                                                                                                                
                                                        </div>
                                                        
                                                    </div>                                            
                                                   
                                                </div>
                                                
                                            </div>
                                            
                                             
                                            <c:set var="status" value="${det.policy_approval_status}"/>
    										<% String status = (String)pageContext.getAttribute("status"); %>
    										
    										<% String statusArray[] = {"Pending","Approved","Rejected"}; %>
    
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Policy Status</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <select class="form-control" id="policyStatusId">
                                                        	
                                                        	<%for(int i=0;i<statusArray.length;i++){ %>
                                                        		
                                                        			<%if(status.equals(statusArray[i])) {%>
                                                        			
                                                        		      <option selected value="<%=statusArray[i]%>"><%=statusArray[i]%></option>
                                                        			
                                                        			<%}else{ %>
                                                        				
                                                        		      <option value="<%=statusArray[i]%>"><%=statusArray[i]%></option>
                                                        			
                                                        			<%} %>	
                                                        	<%} %>
                                                        
                                                        </select>
                                                    </div>                                            
                                                   
                                                </div>
                                            </div>
                                            
                                        </div>
                                        
                                        
                                    </div>

                                </div>
                                <div class="panel-footer">
                                                         
                                    <button class="btn btn-primary pull-right" onclick="updatePolicyByAdmin('${det.policy_id}')">Update Policy</button>
                                    
                                </div>
                            
                             </div>
                           
                           </div>
                            
                        </div>

</div>
                    
                    
                    
</c:forEach>

</c:if>
 
 
<c:if test="${empty policyBean}">	
 
 	<div style="text-align: center; margin-top: 10px; color: rgb(0, 185, 153);">
 	<h4>As per your search criteria nothing was found !</h4>
 	</div>
 
</c:if>                 
                  
                                   
	
	
</body>
</html>