<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>



<script type="text/javascript" src="resources/paging/pageingScript.js"></script> 

</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<body>


<c:if test="${!empty lenderPolicyDetails}">	


                   <div style="width:100%;"  class="col-md-8">
                
                		
                		<div class="card">
                            
                            <div class="header">
                                <h4 class="title">Exist Policy</h4>
                            </div>
                            
                            <div id="pagination"></div>
                            
                            <div class="content table-responsive table-full-width">
                                
                                <table class="table table-hover" role="grid" id="tablepaging"  >
                                    
                                    
                                    <thead>
                                        
                                       <tr>
	                                        
		                                        <th>Sl No</th>
		                                    	<th>Policy Name</th>
		                                    	<th>Policy Type</th>
		                                    	<th>Loan Amount Limit</th>
		                                    	<th>Interest Rate</th>
		                                    	<th>Loan Duration Limit</th>
		                                    	<th>Loan Approval Status</th>
		                                    	<th style="width:100px;">Manage</th>
		                                    	
	                                    </tr>
                                    
                                    
                                    </thead>
                                    
                                    
                                    <tbody id="myTableBody">
                                        
                                        <%int i=1; %> 
                                        
                                         <c:forEach items="${lenderPolicyDetails}" var="var">		
										
												<tr>
	                                        	
	                                        	<td><%=i++%></td>
	                                        	<td><a title="View policy details" href="#" onclick="viewPolicy('${var.policy_id}')">${var.policy_name}</a></td>	                                        	
	                                        	<td>${var.policy_type}</td>
	                                        	<td>${var.policy_min_amount} - ${var.policy_max_amount}</td>
	                                        	<td>${var.policy_interest_rate}</td>
	                                        	<td>${var.policy_min_duration} - ${var.policy_max_duration}</td>
	                                        	<td>${var.policy_approval_status}</td>
	                                        	
	                                        	<td>
	                                        	    
	                                        	    <%-- <a style="float:left;" href="#" onclick="viewPolicy('${var.policy_id}')">View</a> --%>
	                                        	    
	                                        	    <c:if test="${var.policy_approval_status eq 'Pending'}">
	                                        	    
	                                               			<a style="float:left;" href="#" onclick="editFinanceFeature('${var.policy_id}')">Update</a>
	                                               
	                                        	    </c:if>
	                                        	    
	                                        	    <c:if test="${var.policy_approval_status eq 'Approved'}">
	                                        	    
	                                        	    	
	                                               			<a style="float:left;" href="#" onclick="disContinueFinanceFeature('${var.policy_id}')">Discontinue</a>
	                                               
	                                        	    </c:if>
	                                        	    
	                                        	    <c:if test="${var.policy_approval_status eq 'Rejected'}">
	                                        	    
	                                               			<a style="float:left;" href="#" onclick="editFinanceFeature('${var.policy_id}')">Update</a>
	                                               
	                                        	    </c:if>
	                                        	    
	                                            </td>
	                                            
	                                            
	                                        </tr>
												
										</c:forEach>
                                        
                                    </tbody>
                                    
                                    
                                </table>
                                
                                <div id="pageNavPosition" style="padding-top: 20px;float: right;" align="center">
								</div>
								<script type="text/javascript">
								
								var pager = new Pager('tablepaging', 10);
								pager.init();
								pager.showPageNav('pager', 'pageNavPosition');
								pager.showPage(1);
								</script>
                              
                            </div>
                            
                            
                        </div>
                        
                        
                        </div>
               
</c:if>
  
<c:if test="${empty lenderPolicyDetails}">	

 <div style="width:100%;"  class="col-md-8">
    
    <div class="card">
		
		 <div class="header">
             <h4 class="title">Policy</h4>
         </div>
                            
                            
		<div class="content table-responsive table-full-width">
		     <p style="margin-left:16px;">As of now you didn't created any policy.</p>
		</div>         
		                
	</div>
																	
 </div>
 
</c:if>                
                
</body>
</html>