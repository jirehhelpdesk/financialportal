<!DOCTYPE html>
<html>
<head>

	<!--Meta tags-->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!--Title-->
	<title>Pop Up Modal Window</title>
		
	<!--Stylesheets-->
	<link rel="stylesheet" href="resources/borrower_lender_resources/popUpDesign/css/styles.css">
	
	<style>
	        .yesAlign{opacity:0.89;float: right; margin: 0 16px 0 22px ! important;}
	        .noAlign{opacity:0.89;float: right;  margin: 0 23px 0 3px !important;}
	</style>
</head>
<body>

<div id="modal"  style="opacity: 1; visibility: visible; top: 328px;display: none;">
	<div id="heading" >
	
	</div>

	<div id="content">
		
		<p id="contentMsg"></p>
		
		<div id="buttonDiv">	
		      <a class="button blue close" style="opacity:0.89;float: right; margin: 0 16px 0 22px ! important;" href="#" onclick="closeAlert()">Close</a>
		</div>
		<!-- <a class="button red close" style="opacity:0.89;" href="#"><img src="resources/borrower_lender_resources/popUpDesign/images/cross.png">No</a> -->
	</div>
	
</div>





<div id="modalOTP"  style="opacity: 1; visibility: visible; top: 328px;display: none;">
	
	<div id="headingOTP" style="color:#00b6f5;">
	      Enter 6 digit OTP to activate your account
	</div>

	<div id="contentOTP">
		
		<p>			
			<input type="text" name="Otp" id="OtpId" style="width: 212px;" maxlength="6" placeholder="Enter 6 digit OTP sent your mobile" >			
		</p>
		<span id="otpErrId" class="errorStyle" style="display:none;margin-left:75px;height: 30px;width:126px;"></span>			
		
		<br>
		<a href="#" onclick="generateOTPForuser()" style="width: 80px; float: right; margin-right:59px;margin-top: -17px;">Resend OTP</a>
		
		<a class="button blue close" style="opacity:0.89;float: right; margin:17px -80px 4px 4px !important;" href="#" onclick="activateAccountViaOTP()">Activate</a>

		<!-- <a class="button red close" style="opacity:0.89;" href="#"><img src="resources/borrower_lender_resources/popUpDesign/images/cross.png">No</a> -->
	
	</div>
	
</div>






<div class="reveal-modal-bg" id="modelBg" style="display: none; cursor: pointer;"></div>

	<!--jQuery-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="resources/borrower_lender_resources/popUpDesign/js/jquery.reveal.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#button').click(function(e) { // Button which will activate our modal
			   	$('#modal').reveal({ // The item which will be opened with reveal
				  	animation: 'fade',                   // fade, fadeAndPop, none
					animationspeed: 600,                       // how fast animtions are
					closeonbackgroundclick: true,              // if you click background will modal close?
					dismissmodalclass: 'close'    // the class of a button or element that will close an open modal
				});
			return false;
			});
		});
	</script>

</body>
</html>



