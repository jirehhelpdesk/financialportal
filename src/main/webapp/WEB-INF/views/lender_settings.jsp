<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Settings</title>

    <link href="resources/borrower_lender_resources/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="resources/borrower_lender_resources/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="resources/borrower_lender_resources/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="resources/borrower_lender_resources/assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="resources/borrower_lender_resources/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    
    
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>



<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<body onload="lenderActiveSideMenu('menu2')">


<div class="wrapper">
    
    
      <!-- Start of Sidemenu Division -->
    
         <%@include file="lender_sidemenu.jsp" %>

      <!-- End of Sidemenu Division -->


    <div class="main-panel">
        
        
         <!-- Start of Header Division -->
         
         <%@include file="lender_header.jsp" %>
        
		 <!-- End of Header Division -->


        <div class="content">
           
            <div class="container-fluid">
                
                <div class="row">
                
                   <div style="width:100%;" class="col-md-8">
                
                		<div class="card">
                           
                            <div class="header">
                            	
                            	<span class="profileEditButton"  style="float:right;padding: 3px;" onclick="redirectLink('lenProfile')">Back</span>
								
                                <h4 class="title">Profile Details</h4>
                                								
                            </div>
                            
                            <div class="content">
                               
                                <form:form id="profileIdnForm" modelAttribute="lenderIdnDetails" method="post" action="saveLenderIdenDetails" > 
                                    
                                     
                                    
                                     <label style="color:#00b6f5;">Address</label>
                                    
                                     <div class="row">
                                    	
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Door No</label>
                                                <input type="text" id="preDoorId" class="form-control" value="${lenderBean.lender_present_door}" name="lender_present_door" path="lender_present_door" placeholder="Door number" >
                                            	<div id="preDoorErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Building Name</label>
                                                <input type="text" id="preBuildingId" class="form-control" value="${lenderBean.lender_present_building}" name="lender_present_building" path="lender_present_building" placeholder="Building name" >
                                            	<div id="preBuildingErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                         <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Street Name</label>
                                                <input type="text" id="preStreetId" class="form-control" value="${lenderBean.lender_present_street}" name="lender_present_street" path="lender_present_street" placeholder="Street name" >
                                            	<div id="preStreetErrId" class="errorStyle"></div>
                                            </div>
                                         </div>
                                       
                                      </div> 
                                      
                                      <div class="row" style="margin-top:0px;"> 
                                         
                                         <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Area Name</label>
                                                <input type="text" id="preAreaId" class="form-control" value="${lenderBean.lender_present_area}" name="lender_present_area" path="lender_present_area" placeholder="Area name" >
                                            	<div id="preAreaErrId" class="errorStyle"></div>
                                            </div>
                                         </div>
                                         
                                         <div class="col-md-4">
                                            <div class="form-group">
                                                <label>City Name</label>
                                                <input type="text" id="preCityId" class="form-control" value="${lenderBean.lender_present_city}" name="lender_present_city" path="lender_present_city" placeholder="City name" >
                                            	<div id="preCityErrId" class="errorStyle"></div>
                                            </div>
                                         </div>
                                         
                                         <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Pincode</label>
                                                <input type="text" id="prePincodeId" class="form-control" value="${lenderBean.lender_present_pincode}" name="lender_present_pincode" path="lender_present_pincode" maxlength="6" placeholder="Present pincode" >
                                            	<div id="prePinErrId" class="errorStyle"></div>
                                            </div>
                                         </div>
                                         
                                        
                                    </div>
                                    
                                    
                                    <c:if test="${lenderBean.lender_type eq 'Individual'}">
		                                    
		                                   
		                                     <div class="row">
		                                    	
		                                        <div class="col-md-4">
		                                            <div class="form-group">
		                                                <label>Pan number</label>
		                                                <input type="text" id="preDoorId" class="form-control" value="${lenderBean.pan_number}" name="pan_number" path="pan_number" placeholder="Pan number" >
		                                            	<div id="preDoorErrId" class="errorStyle"></div>
		                                            </div>
		                                        </div>
		                                        
		                                        <div class="col-md-4">
		                                            <div class="form-group">
		                                                <label>Photo or Logo</label>
		                                                <input type="file" name="file" class="form-control" id="file">
		                                            	<div class="errorStyle" id="photoErrId"></div>
		                                            </div>
		                                        </div>    
		                                        
		                                     </div> 
		                                    
                                      
		                             </c:if>
		                             
                                    <c:if test="${lenderBean.lender_type ne 'Individual'}">
		                                    
		                                   
		                                     <div class="row">
		                                    	
		                                        <div class="col-md-4">
		                                            <div class="form-group">
		                                                <label>Pan number</label>
		                                                <input type="text" id="preDoorId" class="form-control" value="${lenderBean.pan_number}" name="pan_number" path="pan_number" placeholder="Pan number" >
		                                            	<div id="preDoorErrId" class="errorStyle"></div>
		                                            </div>
		                                        </div>
		                                        
		                                        <div class="col-md-4">
		                                            <div class="form-group">
		                                                <label>Service tax number</label>
		                                                <input type="text" id="preBuildingId" class="form-control" value="${lenderBean.service_tax_number}" name="service_tax_number" path="service_tax_number" placeholder="Service tax number" >
		                                            	<div id="preBuildingErrId" class="errorStyle"></div>
		                                            </div>
		                                        </div>
		                                        
		                                         <div class="col-md-4">
		                                            <div class="form-group">
		                                                <label>Tan number</label>
		                                                <input type="text" id="preStreetId" class="form-control" value="${lenderBean.tan_number}" name="tan_number" path="tan_number" placeholder="Tan number" >
		                                            	<div id="preStreetErrId" class="errorStyle"></div>
		                                            </div>
		                                         </div>
		                                       
		                                      </div> 
		                                      
		                                      
		                                      <div class="row">
                                       
                                        
	                                           <div class="col-md-4">
			                                            <div class="form-group">
			                                                <label>Photo or Logo</label>
			                                                <input type="file" name="file" class="form-control" id="file">
			                                            	<div class="errorStyle" id="photoErrId"></div>
			                                            </div>
			                                        </div>    
			                                                                            
			                                    </div>
                                    
		                                      
		                             </c:if>
                                    
                                    
                                   
                                </form:form>
                                
                                
                                    <button class="btn btn-info btn-fill pull-right" type="button" onclick="saveIdentityDetails()">Update</button>
                                    <div class="clearfix"></div>
                                    
                            </div>
                            
                        </div>
                        
                        
                        </div>
                </div>
                
                
                
            </div>
        </div>

       <!-- Start of Footer Division -->
 
         <%@include file="lender_footer.jsp" %>
        
	   <!-- Start of Footer Division -->
	   
	   
    </div>
</div>


</body>

    
       <!-- Start of Common Division -->
 
         <%@include file="common_content.jsp" %>
        
	   <!-- Start of Common Division -->   
   
	
</html>
