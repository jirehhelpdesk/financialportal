<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>



<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>


<body>


<c:set var="image" value="${borrowerBean.borrower_profile_photo}"/>
<% String image = (String)pageContext.getAttribute("image"); %>
   
<c:set var="sex" value="${borrowerBean.borrower_gender}"/>
<% String sex = (String)pageContext.getAttribute("sex"); %>
 
<c:set var="name" value="${borrowerBean.borrower_name}"/>
<% String name = (String)pageContext.getAttribute("name"); %>
  
<c:set var="regDate" value="${borrowerBean.borrower_reg_time}"/>
<% Date regDate = (Date)pageContext.getAttribute("regDate"); 


DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
Date today = regDate;        
String reportDate = df.format(today);

%>
    
    
<div class="sidebar" data-color="purple" data-image="resources/borrower_lender_resources/assets/img/sidebar-5.jpg">

    
    	<div class="sidebar-wrapper">
            
            <div class="logo">
                <a href="myProfile" class="simple-text">
                    Borrower
                </a>
            </div>
			    
			<div class="sideMenuPhoto">
					
					  <div class="photoStyle">				  
					  		<img height="172px" width="186px;" alt="<%=image%>" src="${pageContext.request.contextPath}<%="/borrowerPhoto?fileName="+image+"&gender="+sex+""%>"  />				  
					  </div>
	                   
				       <p class="photoBelowName" >                     
	                  		<%=name%>	     
					  </p>
			</div> 
			
			
					  
            <ul class="nav">
                
               
                
                <li id="menu2">
                    <a href="myProfile">
                        <i class="pe-7s-user"></i>
                        <p>My Profile</p>
                    </a>
                </li>
               
                <li id="menu3">
                    <a href="loans">
                        <i class="pe-7s-note2"></i>
                        <p>Search Policy</p>
                    </a>
                </li>
                
                <li id="menu4">
                    <a href="myLoanStatus">
                        <i class="pe-7s-cash"></i>
                        <p>Applied Policy</p>
                    </a>
                </li>
                
                <li id="menu5">
                    <a href="notifications">
                        <i class="pe-7s-news-paper"></i>
                        <p>Notification</p>
                    </a>
                </li>
                
               <!--  <li  id="menu6">
                    <a href="settings">
                        <i class="pe-7s-settings"></i>
                        <p>Settings</p>
                    </a>
                </li> -->
               	
            </ul>
            
    	</div>
    	
    </div>
    

</body>
</html>