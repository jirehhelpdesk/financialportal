<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<div class="row">
    
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <h4 class="title">Approval Terms and Conditions</h4>
                <p class="category"></p>
            </div>
            
            <div class="content" style="height: 380px;overflow-y: scroll;">



					<div class="typo-line" style="padding-left: 0px;">
						
						<p style="font-size: 14px;">
						    
						    Lorem ipsum dolor sit amet,
							consectetuer adipiscing elit, sed diam nonummy nibh euismod
							tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi
							enim ad minim veniam. Lorem ipsum dolor sit amet, consectetuer
							adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
							laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim
							veniam. Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
							sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna
							aliquam erat volutpat. Ut wisi enim ad minim veniam. Lorem ipsum
							dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
							nibh euismod tincidunt ut laoreet dolore magna aliquam erat
							volutpat. Ut wisi enim ad minim veniam. Lorem ipsum dolor sit
							amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
							tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi
							enim ad minim venia.
						</p>

					</div>

					<div class="typo-line" style="padding-left: 0px;">
						
						<p style="font-size: 14px;">
						    
						    Lorem ipsum dolor sit amet,
							consectetuer adipiscing elit, sed diam nonummy nibh euismod
							tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi
							enim ad minim veniam. Lorem ipsum dolor sit amet, consectetuer
							adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
							laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim
							veniam. Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
							sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna
							aliquam erat volutpat. Ut wisi enim ad minim veniam. Lorem ipsum
							dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
							nibh euismod tincidunt ut laoreet dolore magna aliquam erat
							volutpat. Ut wisi enim ad minim veniam. Lorem ipsum dolor sit
							amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
							tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi
							enim ad minim venia.
						</p>

					</div>
					
                    <div class="typo-line" style="padding-left: 0px;">
						
						<p style="font-size: 14px;">
						    
						    Lorem ipsum dolor sit amet,
							consectetuer adipiscing elit, sed diam nonummy nibh euismod
							tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi
							enim ad minim veniam. Lorem ipsum dolor sit amet, consectetuer
							adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
							laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim
							veniam. Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
							sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna
							aliquam erat volutpat. Ut wisi enim ad minim veniam. Lorem ipsum
							dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
							nibh euismod tincidunt ut laoreet dolore magna aliquam erat
							volutpat. Ut wisi enim ad minim veniam. Lorem ipsum dolor sit
							amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
							tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi
							enim ad minim venia.
						</p>

					</div>
                
                
                
                <div class="typo-line">
                    
                    <button style="margin-bottom: 26px;" class="btn btn-info btn-fill pull-right" type="button" onclick="approvalTermCond('No')">I Disagree</button>
                   
                    <button style=" margin-right:22px;margin-bottom: 26px;" class="btn btn-info btn-fill pull-right" type="button" onclick="approvalTermCond('Yes')">I Agree</button>
                    
                </div>
                
                
             
            </div>
        </div>
    </div>

</div>
                
</body>
</html>