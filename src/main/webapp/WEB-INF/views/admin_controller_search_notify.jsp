<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<c:if test="${!empty controllerDetails}">	

     
     <div class="col-md-6 widget_1_box2 headingSize" >
 	 	<div class="wid_blog headingContentSize">
 	 		<h1>List of controller accounts to notify  </h1>
 	 	</div>	   	 	
     </div>
					         

</c:if>



     <div class="widget_4">
   	
   	
			   	 <div style="width:100% ! important;" class="col-md-4 stats-info3"> 
			   	 	
			   	 	
			   	 	<div class="online">
						
						<c:if test="${!empty controllerDetails}">	
												
						     <c:forEach items="${controllerDetails}" var="controller">	
							  
									<a href="#"><div class="online-top">
											
											<!-- <div class="top-at">
												<img class="img-responsive" src="resources/admin_resources/images/2.png" alt="">
											</div> -->
											
											<div class="top-on">
												<div class="top-on1">
													<p>${controller.controller_name}</p>
													<span>${controller.controller_user_id}</span>
													<p></p>
													<b>Status :</b><span> ${controller.controller_access_status}</span>
												</div>
												<button style="float:right;" type="button" class="btn btn-primary">Notify </button>
												<!-- <label class="round"> </label> -->
												<div class="clearfix"> </div>
											</div>
											<div class="clearfix"> </div>
										</div>
						
									</a>
												
							 </c:forEach>
													
						</c:if>
												
												
						<c:if test="${empty controllerDetails}">	
						
						         <div>As per your search criteria nothing was found !</div>
						
						</c:if>
								
						
					</div>
					
					
			   	 </div>
   	 
   	 <div class="clearfix"> </div>
   	 
   	 
   </div>
   
</body>
</html>