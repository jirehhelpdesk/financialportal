<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Policies Maintenance</title>

 <!-- META SECTION -->
           
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="resources/adminResources/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->     
        
</head>


<body onload="activeSideMenu('menu5',2)">


 <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           
           
            <!-- START PAGE SIDEBAR -->
              
              <%@include file="controller_side_bar.jsp" %>
            
            <!-- END PAGE SIDEBAR -->
            
            
            
            
            <!-- PAGE CONTENT -->
            
            <div class="page-content" id="contentDiv">
               
               
               
                
                <!-- START X-NAVIGATION VERTICAL -->
                    
                    <%@include file="controller_header_bar.jsp" %>
                
                <!-- END X-NAVIGATION VERTICAL -->                     




                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>                                       
                    <li class="active">Policies Maintenance</li>
                </ul>
                <!-- END BREADCRUMB -->                       
                
                
                
                 <div class="page-title">                    
                    <h2><span class="fa fa-money"></span> Manage Policy  <small></small></h2>
                </div>
                
                
                <!-- PAGE CONTENT WRAPPER -->
                
                
                <div class="page-content-wrap">
                    
                   
                   
                   		<div class="row">
                        <div class="col-md-12">
                            
                            <div class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-heading ui-draggable-handle">
                                    <h3 class="panel-title"><strong>Search Form</strong></h3>
                                    
                                </div>
                                <div class="panel-body">
                                    <p>Please enter all the respective option to get the result.</p>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Search Type</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-search"></span></span>
                                                <select  class="form-control" id="filterStep1" onchange="showFilterOption(this.value)">
                                                    		<option value="Select Search Type">Select Search Type</option>
                                                    		<option value="Identity">Via Identity</option>
                                                    		<option value="Policy Type">Via Policy Type</option>                                                    		
                                                </select>
                                            </div>                                            
                                            <span class="help-block" id="errorTypeId"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group" id="filterDiv1-1" style="display:none;">
                                        <label class="col-md-3 col-xs-12 control-label">Search Type</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                           <div class="input-group">
                                              <span class="input-group-addon"><span class="fa fa-search"></span></span>
                                                <select  class="form-control" id="filterStep1-1">
                                                   		<option value="Name">Via Name</option>
                                                   		<option value="Email">Via Email id</option>
                                                </select>
                                            </div>                                            
                                            <span class="help-block" id="errorType1Id"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group" id="filterDiv1-2" style="display:none;">
                                        <label class="col-md-3 col-xs-12 control-label">Search Type</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-search"></span></span>
                                                <select  class="form-control" id="filterStep1-2">
                                                    		                                              		
                                                </select>
                                            </div>                                            
                                            <span class="help-block" id="errorType2Id"></span>
                                        </div>
                                    </div>

                                    <div class="form-group" id="typeBoxId" style="display:none;">
                                        <label class="col-md-3 col-xs-12 control-label">Search Value</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" placeholder="Who are you looking for?" id="searchValueId" class="form-control">
                                            </div>                                            
                                            <span class="help-block" id="errorValueId"></span>
                                        </div>
                                    </div>
                                    

                                </div>
                                <div class="panel-footer">                                                               
                                    <button class="btn btn-primary pull-right" onclick="searchPolicyAsPerCriteria()">Search<span class="fa fa-search fa-right"></span></button>
                                </div>
                            </div>
                            </div>
                            
                        </div>
                   
                   </div>
                    
                   
                   
                   <div id="policyMnageDivId" style="display:none;">
                        
                   
                   </div>
                   
                   
                   
                   <div class="row" id="hiddenDiv" style="display:none;">
                        <div class="col-md-12">
                            <div class="panel panel-default">

                                <div class="panel-heading ui-draggable-handle">
                                    <h3 class="panel-title"><strong>List of Policy Plans</strong></h3>
                                </div>

                                <div class="panel-body panel-body-table" id="searchDataId">

                                    
                                    
                                    
                                </div>
                            </div>                                                

                        </div>
                    </div>
                   
                   
                   
			        <!-- FOOTER CONTENT -->
       
			         <%@include file="admin_footer.jsp" %>
			       
			       <!-- END FOOTER CONTENT -->
                    
                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            
            
            </div>            
            <!-- END PAGE CONTENT -->
       
        </div>
        <!-- END PAGE CONTAINER -->

       
       
       
       
       
       
       <!-- BELOW COMMON CONTENT -->
       
         <%@include file="controller_down_common.jsp" %>
       
       <!-- END BELOW COMMON CONTENT -->
       
	
	
	
    </body>
</html>