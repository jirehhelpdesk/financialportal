<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" href="resources/paging/other.css">

</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>



<body>

<c:if test="${!empty lenderDetails}">	


<div class="table-responsive">
                                        
                                        
                                        
                                        <table aria-describedby="DataTables_Table_0_info" role="grid" id="tableData" class="table datatable dataTable no-footer">
                                            
                                           
                                            <thead>
                                                <tr>
                                                    <th width="50">Sl No</th>
                                                    <th  width="200">Name</th>
                                                    <th  width="60">Lender Type</th>
                                                    <th width="100">Email Id</th>
                                                    <th width="100">Ph No</th>                                                    
                                                    <th width="100">Status</th>
                                                    <th width="140">Manage</th>
                                                </tr>
                                            </thead>
                                            
                                            
                                            <tbody>                                            
                                               
                                               <%int i=1; %> 
		               							<c:forEach items="${lenderDetails}" var="det">	    
				                                     
				                                    <c:set var="lender_photo" value="${det.lender_photo}"/>
													 <% String lender_photo = (String)pageContext.getAttribute("lender_photo"); %>
													  
													 <c:set var="lender_id" value="${det.lender_id}"/>
													 <% int lender_id = (Integer)pageContext.getAttribute("lender_id"); %>
													 
													  <c:set var="status" value="${det.lender_status}"/>
													 <% String status = (String)pageContext.getAttribute("status"); %>
													    
				                                           
	                                                <tr id="trow_1">
	                                                    <td><%=i++%></td>
	                                                    <td>
	                                                       
	                                                    <img class="searchBorrowerImg" src="${pageContext.request.contextPath}<%="/previewUserPhoto?fileName="+lender_photo+"&id="+lender_id+"&userType=Lender"%>" >
				                                         
	                                                    <strong>${det.full_name}</strong>
	                                                    </td>
	                                                    <td>${det.lender_type}</td>
	                                                    <td>${det.lender_emailid}</td>
	                                                    <td>${det.lender_phno}</td>
	                                                    <td>${det.lender_status}</td>
	                                                    <td>
															
																<button onclick="viewLenderProfile('${det.lender_id}','${det.full_name}')" class="btn btn-info">View</button>
																
																<%if(status.equals("Active")){ %>
																	<button onclick="updateLenderAccess('${det.lender_id}','${det.lender_status}')" class="btn btn-primary">Block</button>
																<%}else {%>
																	<button onclick="updateLenderAccess('${det.lender_id}','${det.lender_status}')" class="btn btn-primary">Unblock</button>
																<%} %>
														</td>
	                                                </tr>
	                                                
                                                </c:forEach>
                                                
                                            </tbody>
                                        </table>
                                        
                                        
                                    </div>      
                                    
                                    
                                    <!-- Paging zone -->
          							
          							
									<script type="text/javascript" src="resources/paging/jquery.min.js"></script> 
									
									<script src="resources/paging/jquery-ui.min.js"></script>
									
									<script type="text/javascript" src="resources/paging/paging.js"></script> 
									
									<script type="text/javascript">
									            $(document).ready(function() {
									                $('#tableData').paging({limit:10});
									            });
									        </script>
									        <script type="text/javascript">
									
									  var _gaq = _gaq || [];
									  _gaq.push(['_setAccount', 'UA-36251023-1']);
									  _gaq.push(['_setDomainName', 'jqueryscript.net']);
									  _gaq.push(['_trackPageview']);
									
									  (function() {
									    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
									    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
									    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
									  })();
									
									</script>
									
				                  
				                   <!-- End of  Paging zone -->
    </c:if>
    
    
    <c:if test="${empty lenderDetails}">	
    
    
			   <div class="col-md-12" style="margin-top:20px;">

                                                     
                            <div class="messages messages-img" >
                                
                                <div class="item item-visible">
                                                                    
                                    <div class="text customMsgText">
                                        <div class="heading">
                                            <a href="#"></a>
                                            <span class="date"></span>
                                        </div>                                    
                                        No Lender was found as per the filter criteria.
                                    </div>
                                </div>
                            </div>

                </div>
    
    </c:if>                                           
</body>


</html>