<!DOCTYPE html>

	<head>
		<meta charset="utf-8" />
		<title>POP Up</title>
		
		<!-- Attach our CSS -->
	  	<link rel="stylesheet" href="resources/borrower_lender_resources/popUpDesign/reveal.css">	
	  	
		<!-- Attach necessary scripts -->		
		<script type="text/javascript" src="resources/borrower_lender_resources/popUpDesign/jquery.reveal.js"></script>
		
		<style type="text/css">
			body { font-family: "HelveticaNeue","Helvetica-Neue", "Helvetica", "Arial", sans-serif; }
			.big-link { display:block; margin-top: 100px; text-align: center; font-size: 70px; color: #06f; }
		</style>
		
		
		<script>
		
		
		</script>
		
		
	</head>
	<body>
		
		
		<div id="myModal" class="reveal-modal" style=" opacity: 1;top: 100px;visibility: visible;display:none;">
			
			<h1 id="popHeadId"></h1>
			
			
			
			<div id="popContentId">
			
			<p>This is a default modal in all its glory, but any of the styles here can easily be changed in the CSS.</p>
			
			</div>
			
			
			
			<a class="close-reveal-modal" onclick="closeRevealModel()">&#215;</a>
		
		</div>
		
		
		
		
		<div id="reasonDivId" class="reveal-modal" style=" opacity: 1;top: 100px;visibility: visible;display:none;">
			
			<h1 id="popHeadId"></h1>
			
			
			<div id="resonContentId" >
			
			        
			</div>
			
			
			<a class="close-reveal-modal" onclick="closeReasonBox()">&#215;</a>
		
		</div>
		
		
		
		
		
		
		
		<div id="myModalBg" class="reveal-modal-bg" style="display:none; cursor: pointer;"></div>
		
		
	</body>
	
</html>