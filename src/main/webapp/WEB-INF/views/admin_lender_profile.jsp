<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>



<body>


<c:set var="lender_photo" value="${lender.lender_photo}"/>
<% String lender_photo = (String)pageContext.getAttribute("lender_photo"); %>
  
 <c:set var="lender_id" value="${lender.lender_id}"/>
 <% int lender_id = (Integer)pageContext.getAttribute("lender_id"); %>
 
    
													    
<div class="row">


		               <div class="col-md-3" style="margin-top: 10px;">
                            
                            <div class="panel panel-default">
                                <div style="background-color:#f5f5f5 !important ;" class="panel-body profile">
                                    <div class="profile-image">
                                        <img style="border-radius: 3%; height: 219px; width: 217px;" alt="${lender.full_name}" src="${pageContext.request.contextPath}<%="/previewUserPhoto?fileName="+lender_photo+"&id="+lender_id+"&userType=Lender"%>">
                                    </div>
                                    <div class="profile-data">
                                        <div class="profile-data-name" style="color:#00d999 !important;font-family: Verdana; font-weight: bold;">${lender.full_name}</div>
                                        <!-- <div style="color: #FFF;" class="profile-data-title">Singer-Songwriter</div> -->
                                    </div>
                                                                    
                                </div>                                
                                
                            </div>                            
                            
                        </div>
                        
                        
                        
                        <div class="col-md-9" style="margin-top: 10px;">

                            <!-- START TIMELINE -->
                            
                            <div class="timeline timeline-right">
                                
                                <!-- START TIMELINE ITEM -->
                                <div class="timeline-item timeline-main">
                                    <div class="timeline-date">Today</div>
                                </div>
                                <!-- END TIMELINE ITEM -->                                                  
                                
                                
                                
                              <!-- START TIMELINE ITEM -->
                              
                                <div class="timeline-item timeline-item-right">
                                    
                                    <div class="timeline-item-info">Registration Details</div>
                                    <div class="timeline-item-icon"><span class="fa fa-info"></span></div>                                   
                                    
                                    <div class="timeline-item-content">
                                                                            
                                        <div class="timeline-body comments">
                                            
                                            <div class="comment-item">                                                
                                                <p class="comment-head">
                                                    <a href="#">Lender Type - </a> <span class="text-muted">${lender.lender_type}</span>
                                                </p>                                               
                                            </div> 
                                                  
                                            <div class="comment-item">                                                
                                                <p class="comment-head">
                                                    <a href="#">Name - </a> <span class="text-muted">${lender.full_name}</span>
                                                </p>                                               
                                            </div> 
                                                                         
                                            <div class="comment-item">                                                
                                                <p class="comment-head">
                                                    <a href="#">Email id - </a> <span class="text-muted">${lender.lender_emailid}</span>
                                                </p>                                               
                                            </div>  
                                            
                                            <div class="comment-item">                                                
                                                <p class="comment-head">
                                                    <a href="#">Contact No - </a> <span class="text-muted">${lender.lender_phno}</span>
                                                </p>                                               
                                            </div>  
                                            
                                            <div class="comment-item">                                                
                                                <p class="comment-head">
                                                    <a href="#">Status -</a> <span class="text-muted">${lender.lender_status}</span>
                                                </p>                                               
                                            </div> 
                                            
                                            <div class="comment-item">                                                
                                                <p class="comment-head">
                                                    <a href="#">Joined on -</a> <span class="text-muted">${lender.lender_reg_time}</span>
                                                </p>                                               
                                            </div>                                            
                                           
                                        </div>
                                        
                                    </div> 
                                                                       
                                </div>                                       
                                
                                
                              <!-- END TIMELINE ITEM -->

	
							  <!-- START TIMELINE ITEM -->
							  
							  
							  
                                <div class="timeline-item timeline-item-right">
                                    
                                    <div class="timeline-item-info">Other Details</div>
                                    <div class="timeline-item-icon"><span class="fa fa-info"></span></div>                                   
                                    <div class="timeline-item-content">
                                        
                                                           
                                        <div class="timeline-body comments">
                                            
                                            <div class="comment-item">
												<p class="comment-head">
													<a href="#">Address -</a> <span class="text-muted">${lender.lender_present_door},${lender.lender_present_door},${lender.lender_present_door},${lender.lender_present_door},${lender.lender_present_door}</span>
												</p>
											</div>  
                                                                                      
                                            <div class="comment-item">                                                
                                                <p class="comment-head">
                                                    <a href="#">Pincode - </a> <span class="text-muted">${lender.lender_present_pincode}</span>
                                                </p>                                               
                                            </div>  
                                            
                                           
                                        </div>
                                    </div>                                    
                                </div>       
                                <!-- END TIMELINE ITEM -->	
									
           						
           						<c:if test="${lender.lender_type !='Individual'}">
		           						
		           						
		           						<div class="timeline-item timeline-item-right">
		                                    
		                                    <div class="timeline-item-info">Second Representative</div>
		                                    <div class="timeline-item-icon"><span class="fa fa-info"></span></div>                                   
		                                    <div class="timeline-item-content">
		                                        
		                                                           
		                                        <div class="timeline-body comments">
		                                            
		                                            
		                                             <div class="comment-item">                                                
		                                                <p class="comment-head">
		                                                    <a href="#">Name -</a> <span class="text-muted">${lender.lender_second_representative_name}</span>
		                                                </p>                                               
		                                            </div>  
		                                                                                      
		                                            <div class="comment-item">                                                
		                                                <p class="comment-head">
		                                                    <a href="#">Email id - </a> <span class="text-muted">${lender.lender_second_representative_email_id}</span>
		                                                </p>                                               
		                                            </div>  
		                                            
		                                            <div class="comment-item">                                                
		                                                <p class="comment-head">
		                                                    <a href="#">Phone - </a> <span class="text-muted">${lender.lender_second_representative_mobile_no}</span>
		                                                </p>                                               
		                                            </div>                                    
		                                            
		                                        </div>
		                                        
		                                    </div>                                    
		                                </div>    
		                                
		                                   
                                   </c:if>
                                
                                
                                
                                <!-- START TIMELINE ITEM -->
	                                <div class="timeline-item timeline-main">
	                                    <div class="timeline-date"><a href="#"><span class="fa fa-ellipsis-h"></span></a></div>
	                                </div>                                
                                <!-- END TIMELINE ITEM -->
                                
                                
                                
                            </div>
                            <!-- END TIMELINE -->                            
                            
                        </div>
                        
                        
                        
                        
                        <div class="col-md-12">

                            <!-- START PRIMARY PANEL -->
                            <div class="panel panel-primary" style="border-top:0px">
                                
                                <div class="panel-heading ui-draggable-handle" style="padding:1px;">
                                    <h3 class="panel-title" style="line-height: 15px;padding-left: 2px;margin-bottom: 3px;">Documents</h3>
                                </div>
                                
                                <div class="panel-body">
                                   
                                    	<div class="gallery" id="links">
                        
					                        <c:if test="${!empty documents}">
														                            
														                            
					                               <c:forEach items="${documents}" var="det">
					                               
						                                <c:set var="docName" value="${det.file_name}"/>
												        <% String docName = (String)pageContext.getAttribute("docName"); %>
					
								                            <a class="gallery-item" href="#" title="Music picture 2" data-gallery>
								                                
								                                <div class="image">
								                                    <img src="${pageContext.request.contextPath}<%="/viewAttachedDocByOther?fileName="+docName+"&userId="+lender_id+"&userType=Lender"%>"  height="120px" width="142px;" alt="Music picture 2"/>    
								                                    <ul class="gallery-item-controls">
								                                        <li title="Zoom the document" onclick="viewUserDocument('<%=docName%>','<%=lender_id%>','Lender')"><label class="check"><span class="glyphicon glyphicon-zoom-in"></span></label></li>
								                                        <li title="Download the document" onclick="downloadUserDocument('<%=docName%>','<%=lender_id%>','Lender')"><label class="check"><span class="glyphicon glyphicon-download"></span></label></li>
								                                    </ul>                                                                    
								                                </div>
								                                
								                                <div class="meta">
								                                    <strong>${det.document_type}</strong>	                                   
								                                </div> 
								                                                               
								                            </a>                            
															
												     </c:forEach>
												
											 </c:if>
					
					
					                       </div>
                        
                                </div>                                                           
                            </div>
                            <!-- END PRIMARY PANEL -->

                        </div>
                        
                        
                        
                        
                        
                        
                    </div>
                   
</body>
</html>