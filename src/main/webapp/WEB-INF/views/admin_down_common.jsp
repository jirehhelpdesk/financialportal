<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

		
		
        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="adminLogout" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->
        
  
  
  
	     <!-- RESPONSE PAGE  -->
           
           <%@include file="admin_result_response.jsp" %>
         
         <!-- END OF RESPONSE PAGE  -->
            	
  
  
  
        <!-- START PRELOADS -->
        <audio id="audio-alert" src="resources/adminResources/audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="resources/adminResources/audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                  
        
        <!-- START SCRIPTS -->
       
        <!-- START PLUGINS -->
        <script type="text/javascript" src="resources/adminResources/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="resources/adminResources/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resources/adminResources/js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->



        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src='resources/adminResources/js/plugins/icheck/icheck.min.js'></script>        
        <script type="text/javascript" src="resources/adminResources/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="resources/adminResources/js/plugins/scrolltotop/scrolltopcontrol.js"></script>
        
        <script type="text/javascript" src="resources/adminResources/js/plugins/morris/raphael-min.js"></script>
        <script type="text/javascript" src="resources/adminResources/js/plugins/morris/morris.min.js"></script>       
        <script type="text/javascript" src="resources/adminResources/js/plugins/rickshaw/d3.v3.js"></script>
        <script type="text/javascript" src="resources/adminResources/js/plugins/rickshaw/rickshaw.min.js"></script>
        <script type='text/javascript' src="resources/adminResources/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script type='text/javascript' src="resources/adminResources/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>                
        <script type='text/javascript' src="resources/adminResources/js/plugins/bootstrap/bootstrap-datepicker.js"></script>                
        <script type="text/javascript" src="resources/adminResources/js/plugins/owl/owl.carousel.min.js"></script>                 
        
        <script type="text/javascript" src="resources/adminResources/js/plugins/moment.min.js"></script>
        <script type="text/javascript" src="resources/adminResources/js/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- END THIS PAGE PLUGINS-->        



                
	<!-- THIS PAGE PLUGINS -->
        <script type='text/javascript' src='resources/adminResources/js/plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="resources/adminResources/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        
        <script type="text/javascript" src="resources/adminResources/js/plugins/datatables/jquery.dataTables.min.js"></script>    
        <!-- END PAGE PLUGINS -->
	
	
	



        <!-- START TEMPLATE -->
        <script type="text/javascript" src="resources/adminResources/js/settings.js"></script>
        
        <script type="text/javascript" src="resources/adminResources/js/plugins.js"></script>        
        <script type="text/javascript" src="resources/adminResources/js/actions.js"></script>
        
        <script type="text/javascript" src="resources/adminResources/js/demo_dashboard.js"></script>
       
        <!-- END TEMPLATE -->
    
    <!-- END SCRIPTS -->        





		
		<script src="resources/admin_resources/js/metisMenu.min.js"></script>
		<script src="resources/admin_resources/js/custom.js"></script>
		
		<script src="resources/admin_resources/js/admin_custom_script.js"></script>

		<script src="resources/adminResources/operationScript/admin_operation_script.js"></script>

		<script src="resources/adminResources/operationScript/controller_operation_script.js"></script>
		
</body>
</html>