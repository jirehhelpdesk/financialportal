<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

 <!--   Core JS Files   -->
    
    <script src="resources/borrower_lender_resources/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	
	<script src="resources/borrower_lender_resources/assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="resources/borrower_lender_resources/assets/js/bootstrap-checkbox-radio-switch.js"></script>

	<!--  Charts Plugin -->
	<script src="resources/borrower_lender_resources/assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="resources/borrower_lender_resources/assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="resources/borrower_lender_resources/assets/js/googleMapApi.js"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="resources/borrower_lender_resources/assets/js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="resources/borrower_lender_resources/assets/js/demo.js"></script>

   
   
	<script src="resources/borrower_lender_resources/assets/js/borrower_custom_script.js"></script>
	
	<script src="resources/borrower_lender_resources/lender_operation_script.js"></script>
	

	
	
  
	<script src="resources/datePicker/jquery.js"></script>
	<script src="resources/datePicker/jquery.datetimepicker.js"></script>
	<script>
	
	$('.some_class').datetimepicker();
	
	$('#some_class3').datetimepicker();
	
	/* $('#datetimepicker_dark').datetimepicker({theme:'dark'}) */
		
	</script>
	
	
	
	
	<input type="hidden" id="action" />
	
	<input type="hidden" id="value" />
	
	
	<!-- POP UP Design  -->
	
	
	<%@include file="popUp_1.jsp" %>
	
	<%@include file="popUp_2.jsp" %>
	
	
	<!-- END OF POP DESIGN  -->
	
</body>
</html>