<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<body>

	<div class="col-md-12">

		<div class="card" style="display: block;">

			<div class="header">
			
				<h4 class="title">Policy Detail</h4>

			</div>

			<div class="content all-icons">

				<c:forEach items="${lenderPolicyDetails}" var="var">
							
							<div class="row viewGap">
								
								<div class="col-sm-6">
									
									<div class="row mgbt-xs-0">

										<label class="col-xs-5 control-label manageFormComponent">Policy Type</label>

										<div class="col-xs-7 controls">${var.policy_type}</div>
										<!-- col-sm-10 -->
									</div>
									
								</div>

								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										
										<label class="col-xs-5 control-label manageFormComponent">Policy Name</label>
										
										<div class="col-xs-7 controls">${var.policy_name}</div>
										<!-- col-sm-10 -->
									</div>
								</div>

							</div>

							<div class="row viewGap">

								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										<label class="col-xs-5 control-label manageFormComponent">Amount </label>
										<div class="col-xs-7 controls">Rs.&nbsp; ${var.policy_min_amount} - ${var.policy_max_amount}</div>
										<!-- col-sm-10 -->
									</div>
								</div>

								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										<label class="col-xs-5 control-label manageFormComponent">Loan Tenure</label>
										<div class="col-xs-7 controls">${var.policy_min_duration} - ${var.policy_max_duration} &nbsp; months</div>
										<!-- col-sm-10 -->
									</div>
								</div>
							
							</div>

							<div class="row viewGap">

								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										<label class="col-xs-5 control-label manageFormComponent">Interest Rate</label>
										<div class="col-xs-7 controls">${var.policy_interest_rate}</div>
										<!-- col-sm-10 -->
									</div>
								</div>
								
								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										<label class="col-xs-5 control-label manageFormComponent">Approval Status</label>
										<div class="col-xs-7 controls">${var.policy_approval_status}</div>
										<!-- col-sm-10 -->
									</div>
								</div>
								
							</div>
							
							<div class="row viewGap">

								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										<label class="col-xs-5 control-label manageFormComponent">Age for policy</label>
										<div class="col-xs-7 controls">${var.policy_age_limit}</div>
										<!-- col-sm-10 -->
									</div>
								</div>
								
								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										<label class="col-xs-5 control-label manageFormComponent">Gender</label>
										<div class="col-xs-7 controls">${var.policy_for_gender}</div>
										<!-- col-sm-10 -->
									</div>
								</div>
								
							</div>
							
							<div class="row viewGap">

								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										<label class="col-xs-5 control-label manageFormComponent">Policy Interval</label>
										<div class="col-xs-7 controls"><fmt:formatDate pattern="dd/MM/yyyy" value="${var.policy_start_date}" /> to <fmt:formatDate pattern="dd/MM/yyyy" value="${var.policy_end_date}" /></div>
										<!-- col-sm-10 -->
									</div>
								</div>
								
								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										<label class="col-xs-5 control-label manageFormComponent">Location For</label>
										<div class="col-xs-7 controls">${var.policy_applicable_location}</div>
										<!-- col-sm-10 -->
									</div>
								</div>
								
							</div>
							
							<div class="row viewGap">

								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										<label class="col-xs-5 control-label manageFormComponent">Policy Brouncher</label>
										<div class="col-xs-7 controls"></div>
										<!-- col-sm-10 -->
									</div>
								</div>
								
								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										<label class="col-xs-5 control-label manageFormComponent">Created Date</label>
										<div class="col-xs-7 controls"><fmt:formatDate pattern="dd/MM/yyyy" value="${var.cr_date}" /></div>
										<!-- col-sm-10 -->
									</div>
								</div>
								
							</div>
						
						
						</c:forEach>
						

			</div>

		</div>
	</div>


</body>
</html>