<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>


<link rel="stylesheet" href="resources/paging/other.css">


</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<body>


<c:if test="${!empty policyResult}">	


                   <div style="width:100%;"  class="col-md-8">
                
                		
                		<div class="card">
                            
                            <div class="header">
                                <h4 class="title">Search result of loans</h4>
                            </div>
                            
                            
                            <div class="content table-responsive table-full-width">
                                
                                <table class="table table-hover" role="grid" id="tableData" >
                                    
                                    
                                    <thead>
                                        
                                        <tr><th>Sl No</th>
                                    	<th>Policy Name</th>
                                    	<th>Policy Type</th>
                                    	<th>Finance Amount</th>
                                    	<th>Duration</th>
                                    	<th>Interest</th>
                                    	<th>Policy Available</th>
                                    	
                                    	<th>See More</th>
                                    	
                                    </tr></thead>
                                    
                                    
                                    <tbody>
                                        
                                    <%int i=1; %> 
               						<c:forEach items="${policyResult}" var="det">	
                        
                                        <tr>
                                        	
                                        	<td><%=i++%></td>
                                        	
                                        	<td>${det.policy_name}</td>
                                        	<td>${det.policy_type}</td>
                                        	<td>Rs.${det.policy_min_amount} - ${det.policy_max_amount}</td>
                                        	<td>${det.policy_min_duration} - ${det.policy_max_duration} months</td>
                                        	<td>${det.policy_interest_rate} %</td>
                                        	<td>${det.policy_applicable_location}</td>
                                        	
                                        	<td>
                                        	
                                        	<button class="btn btn-info" onclick="showPolicyInfoToBorrower('${det.policy_id}')">View</button>
                                        	
                                        	<button class="btn btn-info" onclick="applyPolicy('${det.policy_id}')">Apply</button>
                                        	
                                        	</td>
                                        	
                                        </tr>
                                    
                                    </c:forEach>
                                        
                                    </tbody>
                                    
                                    
                                </table>

                            </div>
                            
                            
                            
                            
                            <!-- Paging zone -->
          							
          							
									<script type="text/javascript" src="resources/paging/jquery.min.js"></script> 
									
									<script src="resources/paging/jquery-ui.min.js"></script>
									
									<script type="text/javascript" src="resources/paging/paging.js"></script> 
									
									<script type="text/javascript">
									            $(document).ready(function() {
									                $('#tableData').paging({limit:10});
									            });
									        </script>
									        <script type="text/javascript">
									
									  var _gaq = _gaq || [];
									  _gaq.push(['_setAccount', 'UA-36251023-1']);
									  _gaq.push(['_setDomainName', 'jqueryscript.net']);
									  _gaq.push(['_trackPageview']);
									
									  (function() {
									    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
									    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
									    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
									  })();
									
									</script>
									
				                  
				                   <!-- End of  Paging zone -->
                                               
                            
                            
                            
                            
                        </div>
                        
                        
                        </div>
               
</c:if>
  
<c:if test="${empty policyResult}">	

 <div style="width:100%;"  class="col-md-8">
    
    <div class="card">
		
		 <div class="header">
             <h4 class="title">Search result of loans</h4>
         </div>
                            
                            
		<div class="content table-responsive table-full-width">
		     <p style="margin-left:16px;">No loan was found as per the search criteria.</p>
		</div>         
		                
	</div>
																	
 </div>
 
</c:if>                
                
</body>
</html>