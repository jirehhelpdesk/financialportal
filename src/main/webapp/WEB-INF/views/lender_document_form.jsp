<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>My Profile</title>

    <link href="resources/borrower_lender_resources/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="resources/borrower_lender_resources/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="resources/borrower_lender_resources/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="resources/borrower_lender_resources/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    
    <link rel="stylesheet" type="text/css" href="resources/datePicker/jquery.datetimepicker.css"/>
    
</head>


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body onload="lenderActiveSideMenu('menu2')">


<div class="wrapper">
    
    
      <!-- Start of Sidemenu Division -->
    
         <%@include file="lender_sidemenu.jsp" %>

      <!-- End of Sidemenu Division -->


    <div class="main-panel">
        
        
         <!-- Start of Header Division -->
         
         <%@include file="lender_header.jsp" %>
        
		 <!-- End of Header Division -->

       
         <div class="content">
           
            <div class="container-fluid">
                
                
                
                
                
                <div class="row">
                
                   <div class="col-md-12">
                
                		<div class="card">
                            
                            <div class="header">
                            	
                            	<span class="profileEditButton"  style="float:right;padding: 3px;" onclick="redirectLink('lenProfile')">Back</span>
								
                                <h4 class="title">Attach Documents</h4>
                            </div>
                            
                            <div class="content">
                             
                            <%String docList = (String)request.getAttribute("docList");%>  
                            <%String docListArray[] = docList.split(","); System.out.println("Length="+docListArray.length);%>
                                
                                <form id="lenderDocForm" name="lenderDocForm" >
                                    
                                     <div class="row">
                                        
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Document Type</label>
                                                <select  id="docType" class="form-control" name="docType">
                                                		<option value="Select document type">Select document type</option>
                                                		<%for(int d=0;d<docListArray.length;d++) {%>
                                                			<option value="<%=docListArray[d]%>"><%=docListArray[d]%></option>
                                                		<%} %>
                                                		
                                                </select>
                                           		<div id="typeErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Lender Document</label>
                                                <input type="file" id="file" class="form-control" name="file">
                                           		<div id="fileErrId" class="errorStyle"></div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                <input style="margin-top:23px;margin-right:37px;" type="button" value="Save"  class="btn btn-info btn-fill pull-right" onclick="saveLenderDoc()">
                                    
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                  
                                    <div class="clearfix"></div>
                                    
                                </form>
                                
                                
                            </div>
                            
                        </div>
                        
                    </div>
                       
                </div>
                
                
                	
				
				
				<div class="row">
                    
                    
                   <div style="width:100%;" class="col-md-8">
                
                		
                		<div class="card">
                            <div class="header">
                                <h4 class="title">Attached Documents</h4>
                            </div>
                            
                            
                            <div class="content table-responsive table-full-width">
                             
                             <c:if test="${!empty attachedDocument}"> 
                             
                                
                                <table class="table table-hover">
                                    
                                    <thead>
                                        
	                                     <tr>
	                                        <th>Document Type</th>
	                                    	<th>Uploaded Date</th>
	                                    	<th>View</th>	                                    	
	                                     </tr>
	                                    
                                    </thead>
                                    
                                    
                                    <tbody>
                                                                   
					                     <c:forEach items="${attachedDocument}" var="det">
					                         
						                    <tr>
	                                        	
	                                        	<td>${det.document_type}</td>
	                                        	
	                                        	<td>${det.cr_date}</td>
	                                        	
	                                        	<td><button class="btn btn-info" onclick="viewLenAddedDoc('${det.lender_document_id}')">View</button></td>
	                                        	
	                                        </tr>
					
									     </c:forEach>
										
                                    </tbody>
                                   
                                </table>

								
							   </c:if>		
							   
							   <c:if test="${empty attachedDocument}"> 
							   
							   		<div class="content table-responsive table-full-width">
									     <p style="margin-left:16px;">As of now you didn't added any documents !</p>
									</div>
							   
							   </c:if>

                            </div>
                            
                            
                        </div>
                        
                        
                        </div> 
                    
                   
                       
                </div>
                
                
                
                
                
                
                
                
                
                
            </div>
        		
        
        	
        		
        </div>

       <!-- Start of Footer Division -->
 
         <%@include file="borrower_footer.jsp" %>
        
	   <!-- Start of Footer Division -->
	   
	   
    </div>
</div>


</body>
      
       <!-- Start of Common Division -->
 
         <%@include file="common_content.jsp" %>
        
	   <!-- Start of Common Division -->   
   
   
   
   <div id="viewAttachedDocument" style="display:none;"> </div>
   
   
   
	
</html>





