<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>


<style>

.gapLine
{
	margin-bottom:10px;
}
</style>


<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href="resources/borrower_lender_resources/trackProcess/css/style.css" rel="stylesheet" type="text/css" media="all" />



</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<body>


<div class="col-md-12">

		<div class="card" style="display: block;">

			<div class="header">
			
				<h4 class="title">Applied Policy Detail</h4>

			</div>

			<div class="content all-icons">

				<c:forEach items="${dealDetails}" var="det">	
							
							<div class="row viewGap">
								
								<div class="col-sm-6">
									
									<div class="row mgbt-xs-0">

										<label class="col-xs-4 control-label manageFormComponent">Policy Type</label>

										<div class="col-xs-7 controls">${det.policy_type}</div>
										<!-- col-sm-10 -->
									</div>
									
								</div>

								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										
										<label class="col-xs-4 control-label manageFormComponent">Policy Name</label>
										
										<div class="col-xs-7 controls">${det.policy_name}</div>
										<!-- col-sm-10 -->
									</div>
								</div>

							</div>

							<div class="row viewGap">

								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										<label class="col-xs-4 control-label manageFormComponent">Requested Amount </label>
										<div class="col-xs-7 controls">Rs.&nbsp; ${det.deal_amount}</div>
										<!-- col-sm-10 -->
									</div>
								</div>

								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										<label class="col-xs-4 control-label manageFormComponent">Tenure</label>
										<div class="col-xs-7 controls">${det.deal_duration} &nbsp; months</div>
										<!-- col-sm-10 -->
									</div>
								</div>
							
							</div>

							<div class="row viewGap">

								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										<label class="col-xs-4 control-label manageFormComponent">Interest Rate</label>
										<div class="col-xs-7 controls">${det.deal_interest_rate}</div>
										<!-- col-sm-10 -->
									</div>
								</div>
								
								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										<label class="col-xs-4 control-label manageFormComponent">Applied Date</label>
										<div class="col-xs-7 controls"><fmt:formatDate pattern="dd/MM/yyyy" value="${det.deal_initiated_date}" /></div>
										<!-- col-sm-10 -->
									</div>
								</div>
								
							</div>
							
							
							<div class="row viewGap">

								<div class="col-sm-12">
									<div class="row mgbt-xs-0">
										<label class="col-xs-2 control-label manageFormComponent">Status</label>
										<div class="col-xs-10 controls">
										
												<div style="background-color: #f7f7f7;"> <!-- class="content" -->
													<!-- <div class="content1">
														<h2>Order Tracking: Order No</h2>
													</div> -->
													
													<div class="content2">
														
														
														<div class="content2-header1">
															<p>${det.lender_status}<span></span></p>
														</div>
														
														<div class="content2-header1">
															<p><span></span></p>
														</div>
														
														<div class="content2-header1">
															<p><span></span></p>
														</div>
														
														<div class="content2-header1">
															<p><span></span></p>
														</div>														
														
														<div class="content2-header1">
															<p> <span></span></p>
														</div>
														
														
														<div class="clear"></div>
													</div>
													
													
													<div class="content3">
												        
												        <div class="shipment">
															
														 <c:if test="${det.lender_status =='Pending'}">
															
																<div class="confirm">
													                <div class="imgcircle">
													                    <img src="resources/borrower_lender_resources/trackProcess/images/apply1.png" alt="confirm order">
													            	</div>
																	<span class="line"></span>
																	<p>Applied Loan</p>
																</div>
																
																<div class="dispatch">
													           	 	<div class="imgcircle">
													                	<img src="resources/borrower_lender_resources/trackProcess/images/interest1.png" alt="process order">
													            	</div>
																	<span class="line"></span>
																	<p>Lender Priview</p>
																</div>
																
																<div class="dispatch">
																	<div class="imgcircle">
													                	<img src="resources/borrower_lender_resources/trackProcess/images/view2.png" alt="quality check">
													            	</div>
																	<span class="line"></span>
																	<p>Under Process</p>
																</div>
																
																<div class="dispatch">
																	<div class="imgcircle">
													                	<img src="resources/borrower_lender_resources/trackProcess/images/approve4.png" alt="delivery">
																	</div>
																	<span class="line"></span>
																	<p>Approved</p>
																</div>
																
																<div class="delivery">
																	<div class="imgcircle">
													                	<img src="resources/borrower_lender_resources/trackProcess/images/amount4.png" alt="delivery">
																	</div>
																	<p>Amount Disposed</p>
																</div>
																
															</c:if>
															
															<c:if test="${det.lender_status =='Processing'}">
															
																<div class="confirm">
													                <div class="imgcircle">
													                    <img src="resources/borrower_lender_resources/trackProcess/images/apply1.png" alt="confirm order">
													            	</div>
																	<span class="line"></span>
																	<p>Applied Loan</p>
																</div>
																
																<div class="process">
													           	 	<div class="imgcircle">
													                	<img src="resources/borrower_lender_resources/trackProcess/images/interest1.png" alt="process order">
													            	</div>
																	<span class="line"></span>
																	<p>Lender Preview</p>
																</div>
																
																<div class="process">
																	<div class="imgcircle">
													                	<img src="resources/borrower_lender_resources/trackProcess/images/view2.png" alt="quality check">
													            	</div>
																	<span class="line"></span>
																	<p>Under Process</p>
																</div>
																
																<div class="dispatch">
																	<div class="imgcircle">
													                	<img src="resources/borrower_lender_resources/trackProcess/images/approve4.png" alt="delivery">
																	</div>
																	<span class="line"></span>
																	<p>Approved</p>
																</div>
																
																<div class="delivery">
																	<div class="imgcircle">
													                	<img src="resources/borrower_lender_resources/trackProcess/images/amount4.png" alt="delivery">
																	</div>
																	<p>Amount Disposed</p>
																</div>
																
																
															</c:if>
															
															<c:if test="${det.lender_status =='Approved'}">
															
																<div class="confirm">
													                <div class="imgcircle">
													                    <img src="resources/borrower_lender_resources/trackProcess/images/apply1.png" alt="confirm order">
													            	</div>
																	<span class="line"></span>
																	<p>Applied Loan</p>
																</div>
																
																<div class="process">
													           	 	<div class="imgcircle">
													                	<img src="resources/borrower_lender_resources/trackProcess/images/interest1.png" alt="process order">
													            	</div>
																	<span class="line"></span>
																	<p>Lender Preview</p>
																</div>
																
																<div class="process">
																	<div class="imgcircle">
													                	<img src="resources/borrower_lender_resources/trackProcess/images/view2.png" alt="quality check">
													            	</div>
																	<span class="line"></span>
																	<p>Under Process</p>
																</div>
																
																<div class="quality">
																
																	<div class="imgcircle">
													                	<img src="resources/borrower_lender_resources/trackProcess/images/approve4.png" alt="delivery">
																	</div>
																	<span class="line"></span>
																	<p>Approved</p>
																</div>
																
																
																<div class="delivery">
																	<div class="imgcircle">
													                	<img src="resources/borrower_lender_resources/trackProcess/images/amount4.png" alt="delivery">
																	</div>
																	<p>Amount Disposed</p>
																</div>
																
																
															</c:if>
															
															
															
															
															
															<c:if test="${det.lender_status =='Rejected'}">
															    
															    <c:set var="pendingReason" value="${det.pending_reject_reason}"/>
            													<% String pendingReason = (String)pageContext.getAttribute("pendingReason"); %>
															    
															    		<div class="confirm">
															                <div class="imgcircle">
															                    <img src="resources/borrower_lender_resources/trackProcess/images/apply1.png" alt="confirm order">
															            	</div>
																			<span class="line"></span>
																			<p>Applied Loan</p>
																		</div>
																		
																		<div class="process">
															           	 	<div class="imgcircle">
															                	<img src="resources/borrower_lender_resources/trackProcess/images/interest1.png" alt="process order">
															            	</div>
																			<span class="line"></span>
																			<p>Lender Preview</p>
																		</div>
																		
																		<div class="process">
																			<div class="imgcircle">
															                	<img src="resources/borrower_lender_resources/trackProcess/images/view2.png" alt="quality check">
															            	</div>
																			<span class="line"></span>
																			<p>Under Process</p>
																		</div>
																		
																		<div class="quality">
																		
																			<div class="imgcircle">
															                	<img src="resources/borrower_lender_resources/trackProcess/images/reject.png" alt="delivery">
																			</div>
																			<p>Rejected</p>
																		</div>
																		
															   
															</c:if>
															
															
															<div class="clear"></div>
														</div>
													</div>
												</div>
										
										</div>
										<!-- col-sm-10 -->
									</div>
								</div>
								
							</div>
							
							
						</c:forEach>
						

			</div>

		</div>
	</div>
	
	
	
	
	
	
	
	
	
	


<%-- <c:if test="${!empty dealDetails}">	

  
  <c:forEach items="${dealDetails}" var="det">	
                     
                    
                    
                    <div class="col-md-12">
                        
                        <div class="card">
                            
                            <div class="header">
                                <h4 class="title">Applied Finance details</h4>
                                <p class="category">See the status of the finance deal</p>
                            </div>
                            
                            <div class="content">

                                 
                                 <div class="row">	
																		 		
									
									
									<div class="col-sm-12" >
						
										<div class="tabs widget">
	
											<div class="tab-content" >
	
												<div id="profile-tab" class="tab-pane active">
	
													<div class="pd-20">
														
														<div class="row gapLine">
															<div class="col-sm-12">
																<div class="row mgbt-xs-0">
																	<label class="col-xs-3 control-label manageFormComponent">Requested Finance Amount</label>
																	<div class="col-xs-8 controls">${det.deal_amount}</div>
																	<!-- col-sm-10 -->
																</div>
															</div>
															
														</div>	
														
														<div class="row gapLine">
															<div class="col-sm-12">
																<div class="row mgbt-xs-0">
																	<label class="col-xs-3 control-label manageFormComponent">Interest Rate</label>
																	<div class="col-xs-8 controls"><t style="color:green">${det.deal_interest_rate}</t>  %</div>
																	<!-- col-sm-10 -->
																</div>
															</div>
															
														</div>	
														
														<div class="row gapLine">
															<div class="col-sm-12">
																<div class="row mgbt-xs-0">
																	<label class="col-xs-3 control-label manageFormComponent">Requested Finance Period</label>
																	<div class="col-xs-8 controls"><t style="color:green">${det.deal_duration}</t> months </div>
																	<!-- col-sm-10 -->
																</div>
															</div>
															
														</div>	
														
														<div class="row gapLine">
															<div class="col-sm-12">
																<div class="row mgbt-xs-0">
																	<label class="col-xs-3 control-label manageFormComponent">Finance Applied Date</label>
																	<div class="col-xs-8 controls"><t style="color:green">${det.deal_initiated_date}</t>  </div>
																	<!-- col-sm-10 -->
																</div>
															</div>
															
														</div>	
														
														<div class="row gapLine">
															<div class="col-sm-12">
																<div class="row mgbt-xs-0">
																	<label class="col-xs-3 control-label manageFormComponent">Deal Status</label>
																	<div class="col-xs-8 controls"><t style="color:orange">${det.deal_status}</t>   </div>
																	<!-- col-sm-10 -->
																</div>
															</div>
															
														</div>	
														
														
														 <div style="width:100%;">
						                     				<ul>	
								                     				<li style="float: right; width: 14%;">
								                     				    <button  class="btn btn-info btn-fill pull-right" onclick="applyPolicy('${det.deal_id}')">Cancel Deal</button>
								                     				</li>
								                     				
								                     				<li style="width:40%;float:right;">
								                     				   <button  style="background-color:#e04b4a ! important;border-color: #e04b4a;" class="btn btn-info btn-fill pull-right" onclick="closeDealInfo()">Close</button>
								                   	
								                     				</li>
						                     				</ul>
			                      				
									                      </div>
						                                 <div class="clearfix"></div>
                                 
															
													</div>
													<!-- pd-20 -->
												</div>
												
											</div>
											<!-- tab-content -->
										</div>
										<!-- tabs-widget -->
									</div>
									
								</div>
                                 
			                     
                            </div>
                            
                               
		                    
                        </div>
                        
		                        
		                   
                    </div>

             
  </c:forEach>
  
                  
</c:if> --%>
                
                
</body>
</html>