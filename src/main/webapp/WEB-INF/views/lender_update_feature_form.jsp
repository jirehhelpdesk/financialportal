
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<% 
   String policyType = (String)request.getAttribute("policyTypeName");

   String policyTypeList[] = policyType.split("/");
%>


<%String docList = (String)request.getAttribute("docList");%>  
<%String docListArray[] = docList.split(",");%>
 
<% String policyNames = (String)request.getAttribute("policyName"); %>   
<% String policyNameList[] = policyNames.split(","); %>                              
  
  
<% 

Date date = new Date(); 
DateFormat datef = new SimpleDateFormat("dd/MM/yyyy");
Date day = date;        
String startDate = datef.format(day);

%>   

<c:forEach items="${policyDetails}" var="var">	

<div style="width:100%;" class="col-md-8">
                
                		
   		<div class="card">
               
               <div class="header">
                   <h4 class="title">Add Financial Deals</h4>
               </div>
               
               <div class="content">
                   
                   <form:form   modelAttribute="policyModel"  id="policyDetailsForm" >
                       
                       
                       <div class="row">
                           
                           <div class="col-md-3">                                            
                               <div class="form-group manageFormComponent"><label>Name of the feature</label></div>
                           </div>
                           
                           <div class="col-md-7">
                               
                               <div class="form-group manageFormComponent">
                                   <input type="text"  placeholder="Feature name" value="${var.policy_name}" class="form-control" name="policy_name" path="policy_name" id="nameId" onchange="checkLenderPolicyDetails()">
                               </div>
                               
                               <div class="form-error" id="featErrorId"></div>
                               
                           </div>
                           
                       </div>
                       
                       
                       <div class="row">
                           
                           <div class="col-md-3">
                               <div class="form-group manageFormComponent">
                                   <label>Policy type</label>
                              
                               </div>
                           </div>
                           
                           <div class="col-md-7">    
                             
                              <div class="form-group manageFormComponent">
                                   
                                    <c:set var="currentPolicyType" value="${var.policy_type}"/>
 									<% String currentPolicyType = (String)pageContext.getAttribute("currentPolicyType"); %>
 
                                   
                                   <Select class="form-control" id="policySubType" name="policy_type" path="policy_type"  onchange="checkLenderPolicyDetails()">
                                   		
                                   		<option value="Select Policy Type">Select Policy Type</option>
                                   		<%for(int i=0;i<policyNameList.length;i++){ %>
                                   			
                                   			<%if(currentPolicyType.equals(policyNameList[i])) {%>
                                   			
                                   					<option selected value="<%=policyNameList[i]%>"><%=policyNameList[i]%></option>
                                   			<%}else{ %>
                                   			
                                   					<option value="<%=policyNameList[i]%>"><%=policyNameList[i]%></option>
                                   			<%} %>						
                                   			
                                   		<%} %>
                                       		
                                       		
                                       </Select>
                                      
                                   </div>
                                   
                                   <div class="form-error" id="subTypeErrorId"></div>
                                   
                               </div>
                              
                           </div>

						   
						   <div class="row">
                               
                               <div class="col-md-3">
                                   <div class="form-group manageFormComponent">
                                       <label>Amount limit (in rupees)</label>
                                    </div>
                               </div>
                               
                               <div class="col-md-4" style="width: 29.333% ! important;">
                               <div class="form-group manageFormComponent">
                                   <input type="text" placeholder="Minimum Amount for Loan" value="${var.policy_min_amount}" class="form-control" name="policy_min_amount" path="policy_min_amount" id="minAmtId" onchange="checkLenderPolicyDetails()">
                               </div>
                               
                               <div class="form-error" id="minAmtErrorId"></div>
                               
                               </div>
                           
                           <div class="col-md-4" style="width: 29.333% ! important;">
                                   <div class="form-group manageFormComponent">
                                       <input type="text" placeholder="Maximum Amount for Loan" value="${var.policy_max_amount}"  class="form-control" name="policy_max_amount" path="policy_max_amount" id="maxAmtId" onchange="checkLenderPolicyDetails()">
                                   </div>
                                   
                                   <div class="form-error" id="maxAmtErrorId"></div>
                                   
                               </div>
                               
                           </div>	
							
							<div class="row">
                               
                               <div class="col-md-3">
                                   <div class="form-group manageFormComponent">
                                       <label>Duration limit (in months)</label>
                                    </div>
                               </div>
                               
                               <div class="col-md-4" style="width: 29.333% ! important;">
                               <div class="form-group manageFormComponent">
                                   <input type="text" placeholder="Minimum Duration for Loan in months" value="${var.policy_min_duration}" maxlength="3" class="form-control" name="policy_min_duration" path="policy_min_duration" id="minDurId" onchange="checkLenderPolicyDetails()">
                               </div>
                               <div class="form-error" id="minDurErrorId"></div>
                           </div>
                           
                           <div class="col-md-4" style="width: 29.333% ! important;">
                               <div class="form-group manageFormComponent">
                                   <input type="text" placeholder="Maximum Duration for Loan in months" value="${var.policy_max_duration}" maxlength="3" class="form-control" name="policy_max_duration" path="policy_max_duration" id="maxDurId" onchange="checkLenderPolicyDetails()">
                               </div>
                               <div class="form-error" id="maxDurErrorId"></div>
                           </div>
                           
                           
                       </div>
                       
                       
                      <div class="row">
		 
                               <div class="col-md-3">
                                   <div class="form-group manageFormComponent">
                                       <label>Interest rate</label>                                               
                                   </div>
                               </div>
                               
                               <div class="col-md-7">                                             
                                  <div class="form-group manageFormComponent">                                          		
                                  		<input type="text" placeholder="Interest rate" class="form-control" value="${var.policy_interest_rate}"  name="policy_interest_rate" path="policy_interest_rate" id="intRateId" onchange="checkLenderPolicyDetails()">                                           		
                                  </div>   
                                  <div class="form-error" id="intErrorId"></div>                                       
                               </div> 

                       </div>

						<div class="row">
                                        
                             <div class="col-md-3">
                                 <div class="form-group manageFormComponent">
                                     <label>Upto Age limit</label>
                                
                                 </div>
                             </div>
                             
                             <div class="col-md-7">    
                               
                                <div class="form-group manageFormComponent">
                          
                                     <input placeholder="Please enter age limit from 18 to 60" maxlength="2" class="form-control" id="ageLimit" name="policy_age_limit" path="policy_age_limit" onkeyup="checkAgeLimit('ageLimit')" type="text">
                                     	
                                 </div>
                                 
                                 <div class="form-error" id="ageErrorId"></div>
                                 
                             </div>
                            
                         </div>
                                    
                         <div class="row">
                                        
                             <div class="col-md-3">
                                 <div class="form-group manageFormComponent">
                                     <label>Gender</label>
                                
                                 </div>
                             </div>
                             
                             <div class="col-md-2">
                                 <div class="form-group manageFormComponent" style="color:#333 !important;">
                                     
                                     <input value="Male" name="policy_for_gender" path="policy_for_gender" id="genderId" type="radio">&nbsp;Male 
                                     
                                 </div>
                                 
                             </div>
                             
                             <div class="col-md-2">
                                 <div class="form-group manageFormComponent" style="color:#333 !important;">
                                     
                                     <input value="Female" name="policy_for_gender" path="policy_for_gender" id="genderId" type="radio">&nbsp;Female 
                                     
                                 </div>
                                 
                             </div>
                             
                             <div class="col-md-2">
                                 <div class="form-group manageFormComponent" style="color:#333 !important;">
                                     
                                     <input value="Both" name="policy_for_gender" path="policy_for_gender" id="genderId" type="radio">&nbsp;For both 
                                     
                                 </div>
                                 
                             </div>
                             <div class="form-error" id="genderErrorId"></div>
                            
                         </div>
                         
                         
                         <div class="row">
                               
                               <div class="col-md-3">
                                   <div class="form-group manageFormComponent">
                                       <label>Income limit (in lacs)</label>
                                  
                                   </div>
                               </div>
                               
                               <div class="col-md-7">    
                             
                                  <div class="form-group manageFormComponent">
                                  
                                       
                                       <input type="text" class="form-control" id="incomeLimit" name="policy_income_limit" value="${var.policy_income_limit}" path="policy_income_limit" placeholder="income limit for the policy" onkeyup="checkLenderIncomeLimit('incomeLimit')"/>
                                       
                                   </div>
                                   
                                   <div class="form-error" id="incomeErrorId"></div>
                                   
                               </div>
                                       
                           </div>
                                    
                                    
                           <div class="row">
                               
                               <div class="col-md-3">
                                   <div class="form-group manageFormComponent">
                                       <label>Policy applicable location</label>
                                  
                                   </div>
                               </div>
                               
                               <div class="col-md-7">    
                                 
                                  <div class="form-group manageFormComponent">
                                  
                                       <Select class="form-control" id="location" name="policy_applicable_location" path="policy_applicable_location"  onchange="checkLenderPolicyDetails()">
                                       		
                                       		<option value="Select applicable location">Select applicable location</option>
                                       		<option selected value="Current city">Current city</option>
                                       		<option value="Applicable for entire country">Applicable for entire country</option>
                                       		
                                       </Select>
                                      
                                   </div>
                                   
                                   <div class="form-error" id="locationErrorId"></div>
                                   
                               </div>
                              
                           </div>
                                    
						
						
						
						   <div class="row">
                               
                               <div class="col-md-3">
                                   <div class="form-group manageFormComponent">
                                       <label>policy duration</label>
                                    </div>
                               </div>
                               
                               <div class="col-md-4" style="width: 29.333% ! important;">
                               
                               <div class="form-group manageFormComponent">
                                   <input type="text" class="form-control" readonly value="<fmt:formatDate pattern="dd/MM/yyyy" value="${var.policy_start_date}"/>" name="policy_start_date"  >
                               </div>
                               
                           </div>
                           
                           <div class="col-md-4" style="width: 29.333% ! important;">
                                   
                                   <div class="form-group manageFormComponent">
                                       <input type="text" placeholder="Policy Expire Date" value="<fmt:formatDate pattern="dd/MM/yyyy" value="${var.policy_end_date}"/>" readonly class="some_class form-control" name="policy_end_date" id="polEndId" onchange="checkLenderPolicyDetails()">
                                   </div>
                                   
                                   <div class="form-error" id="endDateErrorId"></div>
                                   
                               </div>
                               
                               
                           </div>
                           
                           
                           <div class="row">
                               
                               <div class="col-md-3">
                                   <div class="form-group manageFormComponent">
                                       <label>Broucher if any</label>
                                    </div>
                               </div>
                               
                               <div class="col-md-7">
                                   <div class="form-group manageFormComponent">
                                       
                                       <input type="file"  placeholder="Broucher for this" name="file" id="file" onchange="checkLenderPolicyDetails()">
                                       
                                   </div>
                                   <div class="form-error" id="docErrorId"></div>
                               </div>
                               
                           </div>

                           
                        <c:set var="currentRequiredDoc" value="${var.policy_required_doc}"/>
 						<% String currentRequiredDoc = (String)pageContext.getAttribute("currentRequiredDoc"); %>
 						<% String currentDocArray[] = currentRequiredDoc.split(","); %>
 
 
                       <div class="row">
                           
                           <div class="col-md-3">
                               <div class="form-group manageFormComponent">
                                   <label>Basic document required</label>
                                </div>
                           </div>
                        
                      
                           <div class="col-md-7">
                               
                               <div class="form-group ulStyle manageFormComponent" style="min-height:auto ! important;height:auto ! important;">
                                          
                                        <ul>
                                        		
                                        	<%for(int d=0;d<docListArray.length;d++) {%>
                                        		
                                        		<%if(currentRequiredDoc.contains(docListArray[d])) {%>
                                        		
                                     					<li class="liStyle"><input type="checkbox" checked id="<%=docListArray[d].replaceAll(" ","")%>" value="<%=docListArray[d]%>" onclick="getBasicDocName(this.id)"/>&nbsp;&nbsp;<%=docListArray[d]%></li>
                                     			
                                     			<%}else{ %>
                                     			
                                     					<li class="liStyle"><input type="checkbox" id="<%=docListArray[d].replaceAll(" ","")%>" value="<%=docListArray[d]%>" onclick="getBasicDocName(this.id)"/>&nbsp;&nbsp;<%=docListArray[d]%></li>
                                     			
                                     			<%} %>
                                     		<%} %>
                                       		
                                            </ul>
                                          
                                                   &nbsp;                                          	  
                                   </div>
                                   
                                   <div class="form-error" id="basicDocErrorId" ></div> 
                                    
                               </div>
                             
                           
                           </div>
                           
                           
                           <input type="hidden" value="${var.policy_id}" name="policy_id" path="policy_id" />
                           
                           <input id="documentsNeedId" type="hidden" value="<%=currentRequiredDoc%>" name="policy_required_doc" path="policy_required_doc" />

                           <input type="button" value="Save"  class="btn btn-info btn-fill pull-right" onclick="saveLenderPolicyDetails()">
                       
                            &nbsp; &nbsp; &nbsp;
                        
                       <!--  <button class="btn btn-info btn-fill pull-right" type="reset">Reset</button> -->
                       
                       <div class="clearfix"></div>
                       
                       
                   </form:form>
                   
               </div>
               
           </div>
                        
</div>

</c:forEach>

<script src="resources/datePicker/jquery.js"></script>
	<script src="resources/datePicker/jquery.datetimepicker.js"></script>
	<script>
	
	$('.some_class').datetimepicker();
	
	$('#some_class3').datetimepicker();
	
	/* $('#datetimepicker_dark').datetimepicker({theme:'dark'}) */
		
	</script>
	