<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<body>


 <%String emailid = (String)request.getAttribute("emailId");%>
 <%String userType = (String)request.getAttribute("userType"); %>

				   <!-- START CONTENT FRAME BODY -->
                    <div class="content-frame-body">
                        <div class="block">
                        <form role="form" class="form-horizontal" id="composeMessageForm" >
                            
                            <div class="form-group">
                                <label class="col-md-2 control-label">To:</label>
                                <div class="col-md-10">                                        
                                    <div class="form-control select">
                                     <%=emailid%>
                                       <input type="hidden" value="<%=emailid%>" name="receiver_emailid"  />  
                                       <input type="hidden" value="<%=userType%>" name="receiver_type"  />                         
                                    </div>
                                </div>
                            </div> 
                            
                                                   
                           
                            <div class="form-group">
                                <label class="col-md-2 control-label">Subject:</label>
                                <div class="col-md-10">                                        
                                    <input type="text" name="notifySubject" id="notifySubject"  class="form-control" placeholder="Please enter the subject" />                                
                                </div>                                
                            </div>
                            
                            
                            <div class="form-group">
                                
                                <label class="col-md-2 control-label">Message:</label>
                                
                                <div class="col-md-10">                            
                                    
									<textarea id="notifyMessage" rows="10" class="col-md-12" name="notifyMessage"></textarea>          
									              
                                </div>
                                
                            </div>
                            
                            
                            
                        </form>
                        
                        <div class="form-group">
                                <div class="col-md-12">
                                    
                                    <div class="pull-right" style="margin: 13px -16px 0 0;" >
                                        <button class="btn btn-danger" onclick="sendMessageToUser()"><span class="fa fa-envelope"></span> Send Message</button>
                                    </div>            
                                                            
                                </div>
                            </div>
                            
                            
                        </div>
                        
                        
                        
                    </div>
                    <!-- END CONTENT FRAME BODY -->
                    
            
</body>


</html>