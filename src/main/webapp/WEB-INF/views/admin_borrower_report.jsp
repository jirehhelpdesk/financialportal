<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Borrower Report</title>

 <!-- META SECTION -->
           
 <meta http-equiv="X-UA-Compatible" content="IE=edge" />
 <meta name="viewport" content="width=device-width, initial-scale=1" />
 
 <link rel="icon" href="favicon.ico" type="image/x-icon" />
 <!-- END META SECTION -->
 
 <!-- CSS INCLUDE -->        
 <link rel="stylesheet" type="text/css" id="theme" href="resources/adminResources/css/theme-default.css"/>
 <!-- EOF CSS INCLUDE -->     
        
</head>


<body onload="activeSideMenu('menu6',1)">


 <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           
           
            <!-- START PAGE SIDEBAR -->
              
              <%@include file="admin_side_bar.jsp" %>
            
            <!-- END PAGE SIDEBAR -->
            
            
            
            
            <!-- PAGE CONTENT -->
            
            <div class="page-content" id="contentDiv">
               
               
               
                
                <!-- START X-NAVIGATION VERTICAL -->
                    
                    <%@include file="admin_header_bar.jsp" %>
                
                <!-- END X-NAVIGATION VERTICAL -->                     




                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>                    
                    <li >Finance Report</li>
                    <li class="active">Borrower Report</li>
                </ul>
                <!-- END BREADCRUMB -->                       
                
                
                
                 <div class="page-title">                    
                    <h2><span class="fa fa-users"></span> Generate Report  <small></small></h2>
                </div>
                
                
                <!-- PAGE CONTENT WRAPPER -->
                
                
                <div class="page-content-wrap">
                    
                   	<div class="row">
                        
                        <div class="col-md-12">
                            
                            <div class="form-horizontal">
                            
	                            <div class="panel panel-default">
	                                
	                                <div class="panel-heading ui-draggable-handle">
	                                    <h3 class="panel-title"><strong>Report Form</strong></h3>                                    
	                                </div>
	                                
	                                <div class="panel-body">
	                                    <p>Please fill all the respective option to get the result.</p>
	                                </div>
	                                
	                                <div class="panel-body">                                                                        
	                                   
	                                    <div class="form-group">
	                                        <label class="col-md-3 col-xs-12 control-label">Borrower Filter</label>
	                                        <div class="col-md-6 col-xs-12">                                            
	                                            <div class="input-group">
	                                                <span class="input-group-addon"><span class="fa fa-search"></span></span>
		                                                <select  class="form-control" id="searchTypeId">
		                                                   		<option value="Select Borrower Filter">Select Borrower Filter</option>
		                                                   		<option value="All">All</option>
		                                                   		<option value="Name">Search via name</option>
		                                                   		<option value="EmailId">Search via email id</option>
		                                                		<option value="MobileNo">Search via mobile no</option>
		                                                </select>
	                                            </div>                                            
	                                            <span class="help-block" id="errorFilter1Id"></span>
	                                        </div>
	                                    </div>
	                                    
	                                    <div class="form-group">
	                                        <label class="col-md-3 col-xs-12 control-label">Report Type</label>
	                                        <div class="col-md-6 col-xs-12">                                            
	                                            <div class="input-group">
	                                                <span class="input-group-addon"><span class="fa fa-search"></span></span>
		                                                <select class="form-control" id="searchTypeId">
		                                                   		<option value="Select Report Type">Select Report Type</option>
		                                                   		<option value="Info">Borrower Information</option>
		                                                   		<option value="Finance Info">Borrower Finance Information</option>	                                                   		
		                                                </select>
	                                            </div>                                            
	                                            <span class="help-block" id="errorFilter2Id"></span>
	                                        </div>
	                                    </div>
	                                    
	                                    <div class="form-group">
	                                        <label class="col-md-3 col-xs-12 control-label">Report Type</label>
	                                        <div class="col-md-6 col-xs-12">                                            
	                                            <div class="input-group">
	                                                <span class="input-group-addon"><span class="fa fa-search"></span></span>
	                                                <select  class="form-control" id="searchTypeId">
	                                                  		<option value="Borrower List">Borrower List</option>
	                                                  		<option value="Finance Applied">Finance Applied Borrower</option>
	                                                  		<option value="Finance Approved">Finance Approved Borrower</option>
	                                              		    <option value="Finance Rejected">Finance Rejected Borrower</option>
	                                                </select>
	                                            </div>                                            
	                                            <span class="help-block" id="errorNameId"></span>
	                                        </div>
	                                    </div>
	                                    
	                                    <!-- 2nd level search criteria to put the functionality for filter criteria -->
	                                    
	                                    <div class="form-group">
	                                        <label class="col-md-3 col-xs-12 control-label">Report Value</label>
	                                        <div class="col-md-6 col-xs-12">                                            
	                                            <div class="input-group">
	                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
	                                                <input type="text" placeholder="Who are you looking for?" id="searchValueId" class="form-control">
	                                            </div>                                            
	                                            <span class="help-block" id="errorValueId"></span>
	                                        </div>
	                                    </div>
	                                    
	                                </div>
	                                
	                                <div class="panel-footer">                                                               
	                                    <button class="btn btn-primary pull-right" onclick="genBorrowerReport()">Generate<span class="fa fa-search fa-right"></span></button>
	                                </div>
	                                
	                             </div>
                            
                          </div>
                            
                       </div>
                        
                   </div>
                    
                   
                   <div class="row" id="hiddenDiv" style="display:none;">
                        <div class="col-md-12">
                            <div class="panel panel-default">

                                <div class="panel-heading ui-draggable-handle">
                                    <h3 class="panel-title">Report</h3>
                                </div>

                                <div class="panel-body panel-body-table" id="reportData">
													
					                                  
                                </div>
                                
                            </div>                                                
                        </div>
                    </div>
                   
                   
                   
			        <!-- FOOTER CONTENT -->
       
			         <%@include file="admin_footer.jsp" %>
			       
			        <!-- END FOOTER CONTENT -->
                    
                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            
            
            </div>            
            <!-- END PAGE CONTENT -->
       
        </div>
        <!-- END PAGE CONTAINER -->

       
       
       
       
       
       
       <!-- BELOW COMMON CONTENT -->
       
         <%@include file="admin_down_common.jsp" %>
       
       <!-- END BELOW COMMON CONTENT -->
       
	
	
	
    </body>
</html>