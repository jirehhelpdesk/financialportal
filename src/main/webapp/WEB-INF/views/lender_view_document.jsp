<!DOCTYPE html>

	<head>
		<meta charset="utf-8" />
		<title>View Document</title>
		
		<style>
		.popHeading
		{
		    border-bottom: 1px solid #cecece ! important;
		    color: #00b6f5 ! important;
		    font-size: 22px !important;
		    font-style: normal !important;
		    margin: 0;
		}
		
		</style>
	</head>
	
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>

	
	<body>
		
		<c:set var="docName" value="${docDetails.file_name}"/>
		<% String docName = (String)pageContext.getAttribute("docName"); %>
		
		<c:set var="docType" value="${docDetails.document_type}"/>
		<% String docType = (String)pageContext.getAttribute("docType"); %>
		
		<div id="docContent" class="reveal-modal" style=" margin-left: -278px;padding: 23px 20px 10px;opacity: 1;top: 100px;visibility: visible;display:block;position: fixed;">
			
			<h1 class="popHeading"><%=docType%></h1>
			
			<p> <img src="${pageContext.request.contextPath}<%="/lenAttachedDocuments?fileName="+docName+""%>"  />	</p>
			<a class="close-reveal-modal" onclick="closeDocPopUp()">&#215;</a>
		
		</div>
		
		
		<div id="docContentBg" style="display:block;background: rgba(0, 0, 0, 0.8) none repeat scroll 0 0;
									    height: 100%;
									    left: 0;
									    position: fixed;
									    top: 0;
									    width: 100%;
									    z-index: 100;" onclick="closeDocPopUp()"></div>
		
		
	</body>
	
	
	
</html>