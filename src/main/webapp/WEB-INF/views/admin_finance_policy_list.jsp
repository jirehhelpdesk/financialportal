<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" href="resources/paging/other.css">

</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>



<body>

<c:if test="${!empty policyBean}">	

<div class="table-responsive">
                                        
                                        
                                        
                                        <table  class="table datatable dataTable no-footer"
										id="DataTables_Table_0" role="grid"
										aria-describedby="DataTables_Table_0_info">
                                           
                                        <thead>
											<tr role="row">
												
												<th width="50">Sl No</th>
												
												<th tabindex="0" rowspan="1" colspan="1" >Lender Name</th>
													
												<th tabindex="0" rowspan="1" colspan="1" >Email Id</th>
												
												<th tabindex="0" rowspan="1" colspan="1" >Policy Name</th>
												
												<th tabindex="0" rowspan="1" colspan="1" >Policy Type</th>
													
												<th tabindex="0" rowspan="1" colspan="1" >Amount</th>
												
												<th tabindex="0" rowspan="1" colspan="1" >Duration</th>
												
												<th tabindex="0" rowspan="1" colspan="1" >Interest</th>
												
												<!-- <th tabindex="0" rowspan="1" colspan="1" >Documents</th> -->
												
												<th tabindex="0" rowspan="1" colspan="1" >Status</th>
												
												<th tabindex="0" rowspan="1" colspan="1" >Manage</th>
											
											</tr>
										</thead>
										
                                            <tbody>   
                                                                                     
		                                             <%int i=1; %> 
		               								 <c:forEach items="${policyBean}" var="det">	    
			                                            
			                                            <tr role="row" class="odd">
															
															<td class="text-center"><%=i++%></td>
															
															<td class="sorting_1">${det.lender_name}</td>
															<td>${det.lender_emailId}</td>
															<td>${det.policy_name}</td>
															<td>${det.policy_type}</td>
															<td>${det.policy_min_amount} - ${det.policy_max_amount}</td>
															<td>${det.policy_min_duration} - ${det.policy_max_duration}</td>
															<td>${det.policy_interest_rate}</td>
															<%-- <td>${det.policy_required_doc}</td> --%>
															<td>${det.policy_approval_status}</td>
															
															<td><button class="btn btn-info" onclick="managePolicyDetails('${det.policy_id}')">Manage</button></td>
															
														</tr>
														
													</c:forEach>
													
													
                                            </tbody>
                                        </table>
                                        
                                        
                                    </div>      
                                    
 </c:if>
 
 
<c:if test="${empty policyBean}">	
 
 	<div style="text-align: center; margin-top: 10px; color: rgb(0, 185, 153);">
 	<h4>As per your search criteria nothing was found !</h4>
 	</div>
 
</c:if>                 
                  
                                    <!-- Paging zone -->
          							
          							
									<script type="text/javascript" src="resources/paging/jquery.min.js"></script> 
									
									<script src="resources/paging/jquery-ui.min.js"></script>
									
									<script type="text/javascript" src="resources/paging/paging.js"></script> 
									
									<script type="text/javascript">
									            $(document).ready(function() {
									                $('#DataTables_Table_0').paging({limit:10});
									            });
									        </script>
									        <script type="text/javascript">
									
									  var _gaq = _gaq || [];
									  _gaq.push(['_setAccount', 'UA-36251023-1']);
									  _gaq.push(['_setDomainName', 'jqueryscript.net']);
									  _gaq.push(['_trackPageview']);
									
									  (function() {
									    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
									    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
									    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
									  })();
									
									</script>
									
				                  
				                   <!-- End of  Paging zone -->
	
	
</body>
</html>