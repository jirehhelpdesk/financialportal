<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin Login</title>

<!-- META SECTION -->
              
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="resources/adminResources/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->     
        
</head>


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://localhost:8081/FP/mytags" prefix="mytags"%>


<body>
       
     
        
        <div class="login-container" style="height:657px;">
        
            <div class="login-box animated fadeInDown">
                <div class="login-logo"> ADMIN </div>
                <div class="login-body">
                    <div class="login-title"><strong>Welcome</strong>,Please login</div>
                    
                   
                    <form:form modelAttribute="adminDetails" method="post" action="authAdminCredential" class="form-horizontal" >
		                    
		                    <div class="form-group">
		                        <div class="col-md-12">
		                            <input type="text" autocomplete="off" class="form-control" name="admin_user_id" path="admin_user_id" placeholder="Username"/>
		                        </div>
		                    </div>
		                    
		                    <div class="form-group">
		                        <div class="col-md-12">
		                            <input type="password" autocomplete="off" class="form-control" name="admin_password" path="admin_password" placeholder="Password"/>
		                        </div>
		                    </div>
		                    
		                    <div class="form-group">
		                        <div class="col-md-6">
		                           <a class="btn btn-link btn-block" href="controllerLogin">Controller Login</a>		                           
		                        </div>
		                        <div class="col-md-6">
		                            <input class="btn btn-info btn-block" type="submit" value="Login">
		                        </div>
		                    </div>
		                    
                    </form:form>
                    
                    
                </div>
               
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; 2016 Jireh
                    </div>
                    <div class="pull-right">
                    	
                        <a target="_" href="aboutUs">About</a> |
                        <a target="_" href="service">Service</a> |
                        <a target="_" href="contactUs">Contact Us</a>
                    </div>
                </div>
                
            </div>
            
        </div>
        
        <%--   
            
            <h2>Number Format</h2>

			<mytags:formatNumber number="100050.574" format="#,###.00"/><br><br>
			
			<mytags:formatNumber number="1234.567" format="$# ###.00"/><br><br>
			  
	    --%>      
       
    </body>
</html>