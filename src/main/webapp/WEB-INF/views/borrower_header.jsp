<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<body>


 <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index" title="Go to home page">Portal Name/Logo</a>
                </div>
                
                
                <div class="collapse navbar-collapse">
                    
                   <!--  <ul class="nav navbar-nav navbar-left">
                      
                       
                        
                    </ul> -->

                    <ul class="nav navbar-nav navbar-right">
                        
                        <li>
                           <a href="myProfile">
                               Welcome &nbsp; ${borrowerBean.borrower_name} 
                            </a>
                        </li>
                       
                      	 
                      	 
                      	<li class="dropdown">
                              
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-globe"></i>
                                    <b class="caret"></b>
                                    
                                    <%int notifyCount = 0; %>
                                    <c:if test="${!empty unreadNotification}">	
		                              <c:forEach items="${unreadNotification}" var="det">
		                              			
		                              			<%notifyCount++;%>
		                              			
		                              </c:forEach>
		                              <span class="notification"><%=notifyCount%></span>
		                           </c:if>
		                         
		                           <c:if test="${empty unreadNotification}">	
		                         		
		                         		<span class="notification">0</span>
		                         
		                           </c:if>
                                    
                              </a>
                              
                              <ul class="dropdown-menu">
                              <%int listCount = 0; %>
                              <c:if test="${!empty unreadNotification}">	
	                              <c:forEach items="${unreadNotification}" var="det">
	                              			
	                              			<%if(listCount<6){%>
			                              			<li style="overflow:hidden;"><a href="notifications">${det.notify_subject}</a></li>			                                        
	                                        <%}%>
	                                        <%listCount++;%>
	                              </c:forEach>
	                         </c:if>
	                         
	                         <c:if test="${empty unreadNotification}">	
	                         		
	                         		<li><a href="notifications">No new notification</a></li>
	                         
	                         </c:if>     
                                                       
                              </ul>
                              
                        </li>
                        
                        
                        
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="margin: 6px -2px;">
                                
                                 <i style="font-size:24px;font-weight:bold;" class="pe-7s-user"></i> 
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="settings"><i style="font-size:16px;font-weight:bold;" class="pe-7s-pen"></i>Edit Profile</a></li>
                                <li><a href="borCngpassword"><i style="font-size:17px;font-weight:bold;" class="pe-7s-key"></i>Change password</a></li>
                                <li><a href="borrowerLogout"><i style="font-size:16px;font-weight:bold;" class="pe-7s-power"></i>Sign Out</a></li>                               
                             </ul>
                        </li>
                        
                      
                        
                    </ul>
                    
                </div>
            </div>
        </nav>
        
</body>
</html>