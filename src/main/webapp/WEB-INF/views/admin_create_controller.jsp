<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create Controller</title>

 <!-- META SECTION -->
           
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="resources/adminResources/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->     
        
</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>



<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>



<body onload="activeSideMenu('menu4',1)">


 <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
           
           
            <!-- START PAGE SIDEBAR -->
              
              <%@include file="admin_side_bar.jsp" %>
            
            <!-- END PAGE SIDEBAR -->
            
            
            
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
               
               
               
                
                <!-- START X-NAVIGATION VERTICAL -->
                    
                    <%@include file="admin_header_bar.jsp" %>
                
                <!-- END X-NAVIGATION VERTICAL -->                     




                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>                    
                    <li >Controller Management</li>
                    <li class="active">Create Controller</li>
                </ul>
                <!-- END BREADCRUMB -->                       
                
                
                
                 <div class="page-title">                    
                    <h2><span class="fa fa-users"></span> Create Controller <small></small></h2>
                </div>
                
                
                <!-- PAGE CONTENT WRAPPER -->
                
                
                <div class="page-content-wrap">
                    
                   
                   
                   	<div class="row">
                        <div class="col-md-12">
                            
                            <form:form class="form-horizontal" modelattribute="controllerDetails" id="createControllerForm">
                           
                            <div class="panel panel-default">
                                <div class="panel-heading ui-draggable-handle">
                                    <h3 class="panel-title"><strong>Controller Form</strong></h3>
                                    
                                </div>
                                <div class="panel-body">
                                    <p>Please enter all the details to create a 'Controller'. Password will notify to controller via given Email id.</p>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Full Name</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" id="nameId" name="controller_name" path="controller_name" placeholder="Enter full name of controller" class="form-control">
                                            </div>                                            
                                            <span class="help-block" id="errorNameId"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Email Id</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" id="emailId" name="controller_user_id" path="controller_user_id" placeholder="Enter emailid of controller" class="form-control">
                                            </div>                                            
                                            <span class="help-block" id="errorEmailId"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Mobile No</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-mobile"></span></span>
                                                <input type="text" id="mobileId" maxlength="10" name="controller_Phno" path="controller_Phno" placeholder="Enter Mobile no of controller" class="form-control">
                                            </div>                                            
                                            <span class="help-block" id="errorMobileId"></span>
                                        </div>
                                    </div>
                                  	
                                    
                                    <div class="form-group">                                        
                                        <label class="col-md-3 col-xs-12 control-label">Password</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                                <input type="password" id="passwordId" name="controller_password" path="controller_password" placeholder="Enter Password of controller" class="form-control">
                                            </div>            
                                            <span class="help-block" id="errorPasswordId"></span>
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                               
                                <div class="panel-footer">
                                    
                                    <!-- <i class="fa fa-floppy-o fa-right"></i> -->
                                    <input class="btn btn-primary pull-right" type="button" value="Save Details" onclick="saveControllerDetails()">                               
                                    
                                </div>
                                
                                
                            </div>
                            </form:form>
                            
                        </div>
                    </div>
                    
                   
                       
                       
                   <div class="row">
                        <div class="col-md-12">
                            
                            <div class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-heading ui-draggable-handle">
                                    <h3 class="panel-title"><strong>Search Form</strong></h3>
                                    
                                </div>
                                <div class="panel-body">
                                    <p>Please enter all the respective option to get the result.</p>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Search Type</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-search"></span></span>
                                                <select  class="form-control" id="searchTypeId">
                                                    		<option value="Name">Via Name</option>
                                                    		<option value="Email">Via Email id</option>
                                                    		<option value="MobileNo">Via Mobile No</option>
                                                    </select>
                                            </div>                                            
                                            <span class="help-block" id="errorNameId"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Search Value</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" placeholder="Who are you looking for?" id="searchValueId" class="form-control">
                                            </div>                                            
                                            <span class="help-block" id="errorValueId"></span>
                                        </div>
                                    </div>
                                    

                                </div>
                                <div class="panel-footer">                                                               
                                    <button class="btn btn-primary pull-right" onclick="searchController()">Search<span class="fa fa-search fa-right"></span></button>
                                </div>
                            </div>
                            </div>
                            
                        </div>
                    </div>
                    
                   
                   <div class="row" id="hiddenDiv" style="display:none;">
                        <div class="col-md-12">
                            <div class="panel panel-default">

                                <div class="panel-heading ui-draggable-handle">
                                    <h3 class="panel-title">Search Result of Controller</h3>
                                </div>

                                <div class="panel-body panel-body-table" id="controllerSearchResultId">

                                    
                                    
                                    
                                </div>
                            </div>                                                

                        </div>
                    </div>
                   
                   
                    <div class="row" id="composeDiv" style="display:none;">
                        <div class="col-md-12">
                            <div class="panel panel-default">

                                <div class="panel-heading ui-draggable-handle">
                                    <h3 class="panel-title">Notify to Controller</h3>
                                </div>

                                <div class="panel-body panel-body-table" id="composeMsgDiv">

                                    
                                    
                                    
                                </div>
                            </div>                                                

                        </div>
                    </div>
                    
                   
                   
			        <!-- FOOTER CONTENT -->
       
			         <%@include file="admin_footer.jsp" %>
			       
			       <!-- END FOOTER CONTENT -->
                    
                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            
            
            </div>            
            <!-- END PAGE CONTENT -->
       
        </div>
        <!-- END PAGE CONTAINER -->

       
       
       
       
       
       
       <!-- BELOW COMMON CONTENT -->
       
         <%@include file="admin_down_common.jsp" %>
       
       <!-- END BELOW COMMON CONTENT -->
       
	
    </body>
</html>