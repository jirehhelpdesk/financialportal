<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>About Us</title>

    <link rel="shortcut icon" type="image/icon" href="resources/indexResources/assets/images/favicon.ico"/>
    <!-- Font Awesome -->
    <link href="resources/indexResources/assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="resources/indexResources/assets/css/bootstrap.css" rel="stylesheet">
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="resources/indexResources/assets/css/slick.css"/> 
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="resources/indexResources/assets/css/jquery.fancybox.css" type="text/css" media="screen" /> 
    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="resources/indexResources/assets/css/animate.css"/>  
     <!-- Theme color -->
    <link id="switcher" href="resources/indexResources/assets/css/theme-color/default.css" rel="stylesheet">

    <!-- Main Style -->
    <link href="resources/indexResources/style.css" rel="stylesheet">

    <!-- Fonts -->
    <!-- Open Sans for body font -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Raleway for Title -->
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <!-- Pacifico for 404 page   -->
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    
</head>
<body onload="activeIndexMenu('Menu2')">

     <%@include file="index_header.jsp" %>


 
 
  
 
  <!-- Start about section -->
 
  
  
  
  <section id="about" style="padding: 0px 0 ! important;">
    
    <div class="container">
     
      <div class="row">
        <div class="col-md-12">
          <div class="about-area">
            <div class="row">
              
              <div class="title-area">
	              <h2 class="tittle">About Us</h2>              
	              <p></p>
	           </div>
            
            
              <div class="col-md-5 col-sm-6 col-xs-12">
                <div class="about-left wow fadeInLeft" style="height: 344px ! important;">
                  <img src="resources/indexResources/assets/images/About.jpg" alt="img">
                  <!-- <a class="introduction-btn" href="#">Introduction</a> -->
                </div>
              </div>
              
              <div class="col-md-7 col-sm-6 col-xs-12">
                <div class="about-right wow fadeInRight" style="padding-left: 38px ! important;">
                  <div class="title-area">
                              
                            
                            <p>We are creating a transparent environment for borrowers and lenders.
							</p>
							<p>
								Bank and financial institution can get the lead or borrower in a easy way.
							</p>
							<p>	
								sd lkjfglkjsdfkgdskfj kdjfglkjd;fkj ;dkjfgd
								kdkdsjfglkjsd lfgkjdsfgsdl fkgjdslkfjgdsfg
							</p>	
							<p>
								We are a team of young and energetic professionals who have come together from diverse backgrounds such as investment banking, consulting, technology, e-commerce and startup management to disrupt the personal credit sector in India.
							</p>
							<p>	
								It is designed to cut the middle man and the costs associated with the middle man and help lenders and borrowers interact directly with each other without any hidden charges in the most transparent, convenient and efficient manner.
																		
						   </p>
			
                  </div>
                </div>
              </div>
              
              
              
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </section> 
  
  
  <!-- End about section -->






   <%@include file="index_fotter.jsp" %>



  <!-- initialize jQuery Library --> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <!-- Bootstrap -->
  <script src="resources/indexResources/assets/js/bootstrap.js"></script>
  <!-- Slick Slider -->
  <script type="text/javascript" src="resources/indexResources/assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="resources/indexResources/assets/js/waypoints.js"></script>
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.counterup.js"></script>
  <!-- mixit slider -->
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->        
  <script type="text/javascript" src="resources/indexResources/assets/js/jquery.fancybox.pack.js"></script>
  <!-- Wow animation -->
  <script type="text/javascript" src="resources/indexResources/assets/js/wow.js"></script> 

  <!-- Custom js -->
  <script type="text/javascript" src="resources/indexResources/assets/js/custom.js"></script>
    
  </body>
</html>