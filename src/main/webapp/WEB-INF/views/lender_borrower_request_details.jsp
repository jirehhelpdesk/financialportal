<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>


<c:forEach items="${policyBean}" var="det">		
	    
	    
	    
							
				<div class="tab-content">
								
					<div id="profile-tab" class="tab-pane active">
			
						<div class="pd-20" style="padding:10px;">
						
							<!-- <h3 class="mgbt-xs-15 mgtp-10 font-semibold">
								<i class="icon-user mgr-10 profile-icon"></i> Basic Details
							</h3> -->
							
							<h4 class="title sideHeading" style="margin-bottom:10px;font-size:16px;">Request Details</h4>
							
							<br></br>
							
							<div class="row">
								
								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										<label class="col-xs-4 control-label sideHeading">Requested For :</label>
										<div class="col-xs-7 controls">${det.policy_type}</div>
										<!-- col-sm-10 -->
									</div>
								</div>

								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										<label class="col-xs-4 control-label sideHeading">Requested Date :</label>
										<div class="col-xs-7 controls">${det.deal_initiated_date}</div>
										<!-- col-sm-10 -->
									</div>
								</div>

							</div>
							
							
							<br></br>
							
							<div class="row">

								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										<label class="col-xs-4 control-label sideHeading">Approve Date :</label>
										<div class="col-xs-7 controls">Not Finalized </div>
										<!-- col-sm-10 -->
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row mgbt-xs-0">
										<label class="col-xs-4 control-label sideHeading">Interested Amount :</label>
										<div class="col-xs-7 controls">Rs.&nbsp; ${det.deal_amount} </div>
										<!-- col-sm-10 -->
									</div>
								</div>

							</div>

							<br></br>
							
							<div class="row">
								
								<div class="col-sm-12">
									<div class="row mgbt-xs-0">
										<label class="col-xs-2 control-label sideHeading">Message :</label>
										<div class="col-xs-10 controls">${det.note_to_lender}</div>
										<!-- col-sm-10 -->
									</div>
								</div>

							</div>
							
							
						  </div>
							
					   </div>
									
					</div>
								<!-- tab-content -->
				
	    
	    
	                                		
<%-- <div class="content">
    
     <div>
        
			<h4 class="title" style="margin-bottom:10px;font-size:16px;">Request Details</h4>
			
			<div class="row">
	             <div class="col-md-6">
	                 <div class="form-group">
	                     <label>Requested For</label>
	                     <div class="form-control"> ${det.policy_type} </div>
	                 </div>
	             </div>
	             <div class="col-md-6">
	                 <div class="form-group">
	                     <label>Requested Date</label>
	                     <div class="form-control"> ${det.deal_initiated_date}</div>
	                 </div>
	             </div>
           </div>
         
           <div class="row">            
             <div class="col-md-6">
                 <div class="form-group">
                     <label>Approve Date</label>
                     <div class="form-control"> Not Finalized </div>
                 </div>
             </div>
             <div class="col-md-6">
                 <div class="form-group">
                     <label>Loan Period</label>
                     <div class="form-control"> ${det.deal_duration} </div>
                 </div> 
             </div>
          </div>
         
          <div class="row">
             <div class="col-md-6">
                 <div class="form-group">
                     <label>Payment Date</label>
                     <div class="form-control">  Not finalized </div>
                 </div>
             </div>
             <div class="col-md-6">
                 <div class="form-group">
                     <label>Interested Amount</label>
                     <div class="form-control"> Rs.&nbsp; ${det.deal_amount} </div>
                 </div>
             </div>
          </div>


		 <div class="clearfix"></div>

      </div>
      
  </div> --%>
       
                          
</c:forEach>					                                
					                                
</body>
</html>