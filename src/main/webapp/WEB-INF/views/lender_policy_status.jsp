<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


<%String count = (String)request.getAttribute("countDealInPending").toString();
long pendingRequest = Long.parseLong(count);
%>

<div id="contentMsg">
	
	<div id="contentDiv" style="display: block; padding: 14px 0px 9px 5px;">
		
		<ul style="color:#00b6f5;">
				<li>Request arrived - <%=request.getAttribute("totalDealCount")%></li>
				
				<li>Request pending  - <%=request.getAttribute("countDealInPending")%></li>
				
				<li>Request approved - <%=request.getAttribute("countDealInApproved")%></li>
		</ul>
		
		<%if(pendingRequest>0){ %>
		
		   <span style="display: block; text-align: center; color: red;"> To discontinue please contact to support team.</span>
		
		<%}else{ %>
		   
		   <span style="display: block; text-align: center; color: red;"> Do you still want to discontinue the policy ?</span>
	
		<%} %>
		
	</div>	
	
 	<div id="buttonDiv">
		
		<%if(pendingRequest>0){ %>
		
		    <a class="button blue close" style="opacity:0.89;float: right; margin: 0 16px 0 22px ! important;" href="#" onclick="closeDisContinue('No','<%=request.getAttribute("policyId")%>')">Ok</a>
			
		<%}else{ %>
		   
		    <a class="button blue close" style="opacity:0.89;float: right; margin: 0 16px 0 22px ! important;" href="#" onclick="closeDisContinue('Yes','<%=request.getAttribute("policyId")%>')">Yes</a>
			
			<a class="button blue close" style="opacity:0.89;float: right; margin:0 -7px 0 22px !important;" href="#" onclick="closeDisContinue('No','<%=request.getAttribute("policyId")%>')">No</a>
	
		<%} %>	
			
    </div>
   
</div>



</body>
</html>