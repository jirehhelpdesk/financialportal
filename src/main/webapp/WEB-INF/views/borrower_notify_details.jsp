<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<body>

<c:forEach items="${notifyDetails}" var="det">


	<div class="tab-content">

		<div id="profile-tab" class="tab-pane active">

			<div class="pd-20" style="padding: 10px;">

				<h4 class="title sideHeading"
					style="margin-bottom: 10px; font-size: 16px;">Notification Details</h4>

				<br>

				
				<div class="row">
					
					<div class="col-sm-6">
						<div class="row mgbt-xs-0">
							<label class="col-xs-3 control-label sideHeading">Notify By:</label>
							<div class="col-xs-5 controls">${det.notify_sender_type}</div>
							<!-- col-sm-10 -->
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="row mgbt-xs-0">
							<label class="col-xs-3 control-label sideHeading">Notify Date:</label>
							<div class="col-xs-5 controls"><fmt:formatDate pattern="dd/MM/yyyy" value="${det.notify_date}" /></div>
							<!-- col-sm-10 -->
						</div>
					</div>

				</div>
				
				<br>
				
				
				<div class="row">

					<div class="col-sm-12">
						<div class="row mgbt-xs-0">
							<label class="col-xs-1 control-label sideHeading">Subject:</label>
							<div class="col-xs-9 controls">${det.notify_subject}</div>
							<!-- col-sm-10 -->
						</div>
					</div>

				</div>
				
				<br>
				
				
				
				<div class="row">

					<div class="col-sm-12">
						<div class="row mgbt-xs-0">
							<label class="col-xs-1 control-label sideHeading">Message:</label>
							<div class="col-xs-9 controls">${det.notify_message}</div>
							<!-- col-sm-10 -->
						</div>
					</div>

				</div>
				
				<br>
				
			</div>

		</div>

	</div>
  
</c:forEach>					                                
					                                
</body>
</html>