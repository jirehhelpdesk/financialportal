<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>My Profile</title>

    <link href="resources/borrower_lender_resources/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="resources/borrower_lender_resources/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="resources/borrower_lender_resources/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="resources/borrower_lender_resources/assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="resources/borrower_lender_resources/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    
    <link href="resources/borrower_lender_resources/profile_theme_style.css" rel="stylesheet" >
    <link href="resources/borrower_lender_resources/profile_theme_style1.css" rel="stylesheet" >
    
</head>


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<body onload="activeSideMenu('menu2')">


<div class="wrapper">
    
    
      <!-- Start of Sidemenu Division -->
    
         <%@include file="borrower_sidemenu.jsp" %>

      <!-- End of Sidemenu Division -->


    <div class="main-panel">
        
       
     <!-- Start of Header Division -->
        
        <%@include file="borrower_header.jsp" %>
       
	 <!-- End of Header Division -->


 <c:set var="photo" value="${borrowerBean.borrower_profile_photo}"/>
 <% String photo = (String)pageContext.getAttribute("photo"); %>
 
 <c:set var="gender" value="${borrowerBean.borrower_gender}"/>
 <% String gender = (String)pageContext.getAttribute("gender"); %>
     
    
       
        <div class="">
            
            <div class="container-fluid">
                
                <div class="row">
                   
                     
                    <!-- previous Design part --> 
                     
                    
                    	<!-- START OF PROFILE DIV -->
                    	
                    	
                    				<div class="vd_content-section clearfix">
								            
								            <div class="row">
								            
								            
														<div class="col-sm-12" >
						
															<div class="tabs widget">
						
																<div class="tab-content" style="border-radius: 4px; box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), 0 0 0 1px rgba(63, 63, 68, 0.1);">
						
																	<div id="profile-tab" class="tab-pane active">
						
																		<div class="pd-20">
																			
																			
																			<h3 class="mgbt-xs-15 mgtp-10 font-semibold"> Basic Details
																				 <span class="profileEditButton" onclick="editBorrowerBasic()">Edit</span>
																			</h3>
																			
																			<div class="row">
																				
																				<div class="col-sm-6">
																					<div class="row mgbt-xs-0">
																						<label class="col-xs-4 control-label manageFormComponent">First Name</label>
																						<div class="col-xs-7 controls">${borrowerBean.borrower_first_name}</div>
																						<!-- col-sm-10 -->
																					</div>
																				</div>
																				
																				<div class="col-sm-6">
																					<div class="row mgbt-xs-0">
																						<label class="col-xs-4 control-label manageFormComponent">Last Name</label>
																						<div class="col-xs-7 controls">${borrowerBean.borrower_last_name}</div>
																						<!-- col-sm-10 -->
																					</div>
																				</div>
																				
																			</div>	
																				
																			<div class="row">	
																				
																				<div class="col-sm-6">
																					<div class="row mgbt-xs-0">
																						<label class="col-xs-4 control-label manageFormComponent">Email</label>
																						<div class="col-xs-7 controls">${borrowerBean.borrower_emailid}</div>
																						<!-- col-sm-10 -->
																					</div>
																				</div>
																				
																				<div class="col-sm-6">
																					<div class="row mgbt-xs-0">
																						<label class="col-xs-4 control-label manageFormComponent">Mobile no</label>
																						<div class="col-xs-7 controls">${borrowerBean.borrower_phno}</div>
																						<!-- col-sm-10 -->
																					</div>
																				</div>
																				
																			</div>
																			
																			
																			<div class="row">	
																				
																				<div class="col-sm-6">
																					<div class="row mgbt-xs-0">
																						<label class="col-xs-4 control-label manageFormComponent">Date of Join</label>
																						<div class="col-xs-7 controls">${borrowerBean.borrower_reg_time}</div>
																						<!-- col-sm-10 -->
																					</div>
																				</div>
																				
																				
																				<div class="col-sm-6">
																					<div class="row mgbt-xs-0">
																						<label class="col-xs-4 control-label manageFormComponent">Gender </label>
																						<div class="col-xs-7 controls">${borrowerBean.borrower_gender}</div>
																						<!-- col-sm-10 -->
																					</div>
																				</div>
																				
																			</div>
																			
																			
																			<hr class="pd-10">
																			
																			
																			<div class="row">
																				
																				<div class="col-sm-12">
																					<div class="row mgbt-xs-0">
																						<label class="col-xs-2 control-label manageFormComponent">Present Address</label>
																						<c:if test="${borrowerBean.borrower_present_door ne null}">
																						 
																						 <div class="col-xs-9 controls">${borrowerBean.borrower_present_door},&nbsp;${borrowerBean.borrower_present_building},&nbsp;${borrowerBean.borrower_present_street},&nbsp;${borrowerBean.borrower_present_area},&nbsp;${borrowerBean.borrower_present_city},&nbsp;${borrowerBean.borrower_present_pincode}</div>
																						
																						</c:if>
																						<!-- col-sm-10 -->
																					</div>
																				</div>
																				
																			</div>
																			
																			
																			<div class="row">	
																				
																				<div class="col-sm-12">
																					<div class="row mgbt-xs-0">
																						<label class="col-xs-2 control-label manageFormComponent">Permanent Address </label>
																						<c:if test="${borrowerBean.borrower_permanent_door ne null}">
																						   
																						   <div class="col-xs-9 controls">${borrowerBean.borrower_permanent_door},&nbsp;${borrowerBean.borrower_permanent_building},&nbsp;${borrowerBean.borrower_permanent_street},&nbsp;${borrowerBean.borrower_permanent_area},&nbsp;${borrowerBean.borrower_permanent_city},&nbsp;${borrowerBean.borrower_permanent_pincode}</div>
																						
																						</c:if>
																						<!-- col-sm-10 -->
																					</div>
																				</div>
																			</div>
																			
																			
																			<div class="row">		
																				
																				<div class="col-sm-6">
																					<div class="row mgbt-xs-0">
																						<label class="col-xs-4 control-label manageFormComponent">Alt Phone No</label>
																						<div class="col-xs-7 controls">${borrowerBean.borrower_alt_phno}</div>
																						<!-- col-sm-10 -->
																					</div>
																				</div>
																				
																				<div class="col-sm-6">
																					<div class="row mgbt-xs-0">
																						<label class="col-xs-4 control-label manageFormComponent">Date of Birth</label>
																						<div class="col-xs-7 controls">${borrowerBean.borrower_dob}</div>
																						<!-- col-sm-10 -->
																					</div>
																				</div>
																			
																			</div>
																				
																		 	<div class="row">	
																		 		
																				<div class="col-sm-6">
																					<div class="row mgbt-xs-0">
																						<label class="col-xs-4 control-label manageFormComponent">Identity Type</label>
																						<div class="col-xs-7 controls">${borrowerBean.borrower_identity_id_type}</div>
																						<!-- col-sm-10 -->
																					</div>
																				</div>
																				<div class="col-sm-6">
																					<div class="row mgbt-xs-0">
																						<label class="col-xs-4 control-label manageFormComponent">Identity Id </label>
																						<div class="col-xs-7 controls">${borrowerBean.borrower_identity_card_id}</div>
																						<!-- col-sm-10 -->
																					</div>
																				</div>
																				
																			</div>
																			
																			
																		
																		</div>
																		<!-- pd-20 -->
																	</div>
																	<!-- home-tab -->
						
						
						
																	<!-- photos tab -->
																	<!-- photos tab -->
																	<!-- groups tab -->
						
																</div>
																<!-- tab-content -->
															</div>
															<!-- tabs-widget -->
														</div>
						
														
														
														
											</div>
								            <!-- row --> 
								            
								            
								          
								           <!-- Attached Documents -->
								            
								          	<div class="row">
								          	
											            <div class="col-md-12" >
									                        
									                        <div class="card" style="display:block;">
									                            
									                            <div class="header">
									                               
									                                <span class="profileEditButton" onclick="editBorrowerDoc()">Edit</span>
									                                <h4 class="title"><i class="fa fa-file-text-o"></i> Attached documents</h4>
									                                
									                            </div>
									                            
											                            <div class="content all-icons">
											                                
											                                <div class="row">
											                                  
											                                  
											                                  <c:if test="${!empty attachedDocument}">
									                            
									                            
													                                  <c:forEach items="${attachedDocument}" var="det">
													                                  
														                                  <c:set var="docName" value="${det.file_name}"/>
																						  <% String docName = (String)pageContext.getAttribute("docName"); %>
						
														                                     
														                                  <div class="font-icon-list col-lg-2 col-md-3 col-sm-4 col-xs-6 col-xs-6">
														                                    
														                                    <div class="font-icon-detail">
														                                      
														                                      <input value="${det.document_type}" disabled type="text">
														                                      
														                                      <img src="${pageContext.request.contextPath}<%="/attachedDocuments?fileName="+docName+""%>" height="120px" width="142px;" />				  
														                                      
														                                      <input value="Zoom" type="button" class="btn btn-info btn-fill pull-right" onclick="viewScannedDoc('${det.document_id}')">
														                                    
														                                    </div>
														
														                                  </div>
													                                  
													                                  </c:forEach>
											                                  
											                                  		  </c:if>
									                        		
														                        		<c:if test="${empty attachedDocument}">
														                        		
														                        			<div><p style="margin-left: 16px;">As of now you didn't attached any documents.</p></div>                         
																	
														                        		</c:if>
														                         </div>
											
											                            </div>
									                        
									                        		
									                        </div>
									                    </div>
											           
								          	</div>
								          
								          	
								          	
								          	
								          	
								          	
								            <!-- End of Attached Documents -->







				 <div class="row" style="margin-top:-20px;">


								<div class="col-md-12">
									<div class="card">
										<div class="header">
											
											 <span class="profileEditButton"  onclick="redirectLink('myLoanStatus')">Know more</span>
											<h4 class="title">Current taken loans</h4>
											<p class="category"></p>
										</div>
										
										
										<div class="content table-responsive table-full-width">
										
										<c:if test="${!empty dealBean}">	
											
											<table class="table table-hover" role="grid" id="tableData">
												<thead>
													<tr>
														
														<th>Loan Name</th>
														<th>Loan Type</th>
														<th>Loan Amount</th>
														<th>Status</th>
														
													</tr>
												</thead>
												
												<tbody>
													
													<c:forEach items="${dealBean}" var="det">
														
														<tr>
															
															<td>${det.policy_name}</td>
															<td>${det.policy_type}</td>
															<td>Rs.${det.deal_amount}</td>
															<td>${det.deal_status}</td>
														</tr>
														
													</c:forEach>	
													
												</tbody>
											</table>
											
											</c:if>
											
											<c:if test="${empty dealBean}">
												
												<div><p style="margin-left: 16px;">As of now you didn't applied for any policy or deals. <a href="loans">Search policy and apply</a></p></div>                         
																							
											</c:if>
											
										</div>
									</div>
								</div>



								
								<div class="col-md-12">
									<div class="card">
										<div class="header">
											
											 <span class="profileEditButton"  onclick="redirectLink('loans')">Search more</span>
											<h4 class="title">Last Searched Loan</h4>
											<p class="category"></p>
										</div>
										
										
										<div class="content table-responsive table-full-width">
										
										<c:if test="${!empty searchedBean}">	
											
											<table class="table table-hover" role="grid" id="tableData">
												<thead>
													<tr>
														
														<th>Loan Name</th>
														<th>Loan Type</th>
														<th>Loan Amount</th>
														
													</tr>
												</thead>
												
												<tbody>
													
													<c:forEach items="${searchedBean}" var="det">
														
														<tr>
															
															<td>${det.policy_name}</td>
															<td>${det.policy_type}</td>
															<td>Rs.${det.policy_min_amount} - ${det.policy_max_amount}</td>
															
														</tr>
														
													</c:forEach>	
													
												</tbody>
											</table>
											
											</c:if>
											
											<c:if test="${empty searchedBean}">
												
												<div><p style="margin-left: 16px;">As of now you didn't search and policy or deals. <a href="loans">Search Policy</a></p></div>                         
																																	
											</c:if>
											
										</div>
									</div>
								</div>
								
							
					</div>	
					
					
														
								            
					</div>
                    	
                    	
                    	
                    	<!-- END OF PROFILE DIV -->
                    	
                </div>
                
                
                
                
                
            </div>
        </div>
        
       <!-- Start of Footer Division -->
 
         <%@include file="borrower_footer.jsp" %>
        
	   <!-- Start of Footer Division -->
	   
	   
    </div>
</div>


</body>

     <!-- Start of Common Division -->
 
         <%@include file="common_content.jsp" %>
        
	   <!-- Start of Common Division -->   
   
	
	
	<div id="viewAttachedDocument" style="display:none;"> </div>
	
	
	
								          	
</html>
