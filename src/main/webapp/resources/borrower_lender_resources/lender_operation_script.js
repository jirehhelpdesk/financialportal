



function closeAlert()
{
	var action = $("#action").val();
    var value = $("#value").val();
    
    if(action=="url")
    {
    	window.location = value;
    }
    else
    {
    	value+"()"; 
    }
}


function goToSite()
{
	var url = "http://www.jirehsol.com";
	var win = window.open(url, '_blank');
	win.focus();	
}

// Lender side menu activation script


function alertAddressUpdate()
{
	document.getElementById("action").value = "url";
 	document.getElementById("value").value = "lenderSettings";
 	
    $("#modal").show();
    $("#modelBg").show();
    
    $("#heading").html("Alert Message");
    $("#contentMsg").html("Please fill your address details first.After that you will go for policy creation.");	
}


function lenderActiveSideMenu(menuType)
{ 			
	if(menuType=='menu1')
		{
		     $("#menu1").attr('class', 'active');
		}
	else if(menuType=='menu2')
		{
			 $("#menu2").attr('class', 'active');				
		}
	else if(menuType=='menu3')
		{		
			 $("#menu3").attr('class', 'active');	 
		}
	else if(menuType=='menu4')
		{		  
			 $("#menu4").attr('class', 'active');			  
		}
	else if(menuType=='menu5')
		{		  		 	 
			 $("#menu5").attr('class', 'active');			  
		}
	else if(menuType=='menu6')
		{						 
		     $("#menu6").attr('class', 'active');			 
		}
	else if(menuType=='menu7')
		{						 
		     $("#menu7").attr('class', 'active');			 
		}
	else if(menuType=='menu8')
		{						 
		     $("#menu8").attr('class', 'active');			 
		}
	else
		{
		 	 $("#menu9").attr('class', 'active');		
		}
		
}



function searchBorrowerRequest()
{
	var flag = 'true';
	
	if(flag=='true')
	{
		$("#backgroundId").show();
		$("#loaderId").show();
		
		var formData = new FormData($("#borrowerRequestSearchForm")[0]);
		
		$.ajax({
					type : "POST",
					data : formData,
					url : "searchBorrowerRequest",								
					success : function(response) {
			            
						$("#backgroundId").hide();
						$("#loaderId").hide();
						
						if(response=="success")
						{
							alert("Index file has uploaded successfully.");
							
							window.location  = "adminSetting";
						}
						else
						{
							alert("Please try again later !");
						}
					},
			});
	}
}

function showBwrReqOption(searchType)
{
	if(searchType=="Select Search Type")
	{
		$("#loanType").slideUp();
		$("#durationType").slideUp();
	}
	else if(searchType=="Via Duration")
	{
		$("#loanType").slideUp();
		$("#durationType").slideDown();
	}
	else if(searchType=="Via Loan Type")
	{
		$("#durationType").slideUp();
		$("#loanType").slideDown();
	}
	else
	{
		$("#loanType").slideUp();
		$("#durationType").slideUp();
	}
}

function viewBwrMessage()
{	  
  $("#rowId").slideDown();
  $("#viewBorrowerRequest").slideDown();	  
}




//////////////////////  BORROWER REQUEST DETAILS   ////////////////////////////////


function searchBorrowerReqByLender() 
{
	var flag = 'true';
	
	var searchType = $("#searchType").val();
	
	var fromDateId = $("#fromDateId").val();
	var toDateId = $("#toDateId").val();
	
	var policyTypeValue = $("#policyTypeValue").val();
	
	if (flag=="true") 
	{
		$("#preloader").show();
		$("#loader").show();
		
		$.ajax({

			type : "post",
			url : "searchBorrowerReqByLender",
			data : "searchType="+searchType+"&fromDateId="+fromDateId+"&toDateId="+toDateId+"&policyTypeValue="+policyTypeValue,
			success : function(response) { 
				
				$("#preloader").hide();
				$("#loader").hide();
				
				$("#hiddenDiv").slideDown();
				$("#contentDiv").html(response);
		    	
				$('#contentDiv')[0].scrollIntoView(true);
		   },
		});
	} 
	
	
}

function searchBorInterestedLoans()
{
    var flag = 'true';
	
	var searchType = $("#searchType").val();
	
	var fromDateId = $("#fromDateId").val();
	var toDateId = $("#toDateId").val();
	
	var policyTypeValue = $("#policyTypeValue").val();
	
	if (flag=="true") 
	{
		$("#preloader").show();
		$("#loader").show();
		
		$.ajax({

			type : "post",
			url : "searchBorInterestedLoans",
			data : "searchType="+searchType+"&fromDateId="+fromDateId+"&toDateId="+toDateId+"&policyTypeValue="+policyTypeValue,
			success : function(response) { 
				
				$("#preloader").hide();
				$("#loader").hide();
				
				$("#hiddenDiv").slideDown();
				$("#contentDiv").html(response);
		    	
				$('#contentDiv')[0].scrollIntoView(true);
		   },
		});
	} 
	
}


function viewBorrowerProfileByLender(dealId,policyId,borrowerId)
{
	$("#preloader").show();
	$("#loader").show();
	
	$.ajax({

		type : "post",
		url : "viewBorrowerProfileByLender",
		data : "dealId="+dealId+"&policyId="+policyId+"&borrowerId="+borrowerId,
		success : function(response) { 
			
			$("#preloader").hide();
			$("#loader").hide();
			
			$("#profileDiv").html(response);
			$("#profileDiv").show();
			$('#profileDiv')[0].scrollIntoView(true);
			
			
	    		
	   },
	});
}

function downloadDocuments(fileName,userId,userType)
{
	window.location = "downloadDocuments?fileName="+fileName+"&userId="+userId+"&userType="+userType;
}




function showBorrowerReqDetails(dealId) 
{
	$("#preloader").show();
	$("#loader").show();

	var buttonValue = $("#buttonId_" + dealId).html();

	if (buttonValue == "See") 
	{
		$.ajax({

			type : "post",
			url : "showBorrowerReqDetails",
			data : "dealId="+dealId,
			success : function(response) {

				$("#preloader").hide();
				$("#loader").hide();

				$("#buttonId_" + dealId).html("Close");

				$("#loanDetail_" + dealId).slideDown();
				$("#loanDetail_" + dealId).html(response);

			},
		});
	} 
	else 
	{
		$("#preloader").hide();
		$("#loader").hide();

		$("#buttonId_" + dealId).html("See");

		$("#loanDetail_" + dealId).slideUp();
		$("#loanDetail_" + dealId).html("");
	}
	
}


function showTermsCondition()
{
	 
	$("#preloader").show();
	$("#loader").show();

	$.ajax({

		type : "post",
		url : "showApprovalTermsCondition",
		success : function(response) {

			$("#preloader").hide();
			$("#loader").hide();
			
			
			/*$("#popHeadId").html(response);*/
			$("#popContentId").html(response);
			
			$("#myModal").show();
			$("#myModalBg").show();
			
		}, 
	});
	
	
}

function closeRevealModel()
{
	$('#approveTermId').prop('checked', false);
		
	$("#popContentId").html("");
	
	$("#myModal").hide();
	$("#myModalBg").hide();
	
	$('#approveButtonId').attr('disabled',true);
}

function approvalTermCond(condition)
{
   	if(condition=="Yes")
   	{
   		$('#approveTermId').prop('checked', true);
   		
   		$("#popContentId").html("");
		
		$("#myModal").hide();
		$("#myModalBg").hide();
				
		$('#approveButtonId').attr('disabled',false);
   	}
   	else
   	{
   		$('#approveTermId').prop('checked', false);
   		
   		$("#popContentId").html("");
		
		$("#myModal").hide();
		$("#myModalBg").hide();
		
		$('#approveButtonId').attr('disabled',true);
   	}
}


function approveBorrowerRequest(dealId)
{
	var flag = document.getElementById("approveTermId");	
	
	if(flag.checked)
	{		
		$("#preloader").show();
		$("#loader").show();

		$.ajax({

			type : "post",
			url : "approveBorrowerRequest",
			data: "dealId="+dealId,
			success : function(response) {
					
				 if(response=="success")
		      	 {		
					 	document.getElementById("action").value = "url";
					 	document.getElementById("value").value = "borrowerInterestRqst";
					 	
					    $("#modal").show();
					    $("#modelBg").show();
					    
					    $("#heading").html("Success Message");
					    $("#contentMsg").html("Your have successfully approved the borrower request.");						    
		      	 }		    	 
		      	 else
		      	 {
			      		document.getElementById("action").value = "url";
					 	document.getElementById("value").value = "borrowerInterestRqst";
				 	
					    $("#modal").show();
					    $("#modelBg").show();
					    
					    $("#heading").html("Failed Message");
					    $("#contentMsg").html("There is some problem arise,Please try again later.");						    								    
		      	 }	
				
			},
		});		
	}
	else
	{
		$("#termApproveId").show();	
		$("#termApproveId").html("Please check that you are agree with the terms and conditions.");	
	}	
}



function printBorrowerRequest(dealId,policyId,borrowerId)
{
	$("#preloader").show();
	$("#loader").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "printBorrowerRequest",
	     data : "dealId="+dealId+"&policyId="+policyId+"&borrowerId="+borrowerId,
	     success : function(response) 
	     {  		    	
	    	 $("#preloader").hide();
	 		 $("#loader").hide();
 			    	
             $("#profileDiv").html(response);		
	 		
            
            var printContents = document.getElementById('profileDiv').innerHTML;
    	    var originalContents = document.body.innerHTML;
    	    document.body.innerHTML = "";	 	
    	    document.body.innerHTML = printContents;	 	   
    	    window.print();
    	    
    	    $("#profileDiv").html("");		
    	    
    	    document.body.innerHTML = originalContents;	 	  
    	    window.load();
	 	     		
	     },  
    });  
}

//////////////////////   END OF BORROWER REQUEST DETAILS    ////////////////////////////////






//////////////////////LOAN STATUS   ////////////////////////////////


function showLoanDetailsOfBorrower(dealId,slId) 
{	
	$("#preloader").show();
	$("#loader").show();

	var buttonValue = $("#buttonId_" + slId).html();
	
	if(buttonValue=="Open")
	{
		$.ajax({

			type : "post",
			url : "showLoanDetailsOfBorrower",
			data : "dealId=" + dealId,
			success : function(response) {
				
				$("#preloader").hide();
				$("#loader").hide();

				$("#buttonId_" + slId).html("Close");
				
				$("#loanDetail_" + slId).slideDown();
				$("#loanDetail_" + slId).html(response);
				
			},
		});
	}
	else
	{
		$("#preloader").hide();
		$("#loader").hide();
		
		$("#buttonId_" + slId).html("Open");
		
		$("#loanDetail_" + slId).slideUp();
		$("#loanDetail_" + slId).html("");
	}
	
}

function searchApprovedLoanByLender()
{
	var flag = 'true';
	
	var searchType = $("#searchType").val();
	
	var fromDateId = $("#fromDateId").val();
	var toDateId = $("#toDateId").val();
	
	var policyTypeValue = $("#policyTypeValue").val();
	
	if (flag=="true") 
	{
		$("#preloader").show();
		$("#loader").show();
		
		$.ajax({

			type : "post",
			url : "searchApprovedLoanByLender",
			data : "searchType="+searchType+"&fromDateId="+fromDateId+"&toDateId="+toDateId+"&policyTypeValue="+policyTypeValue,
			success : function(response) { 
				
				$("#preloader").hide();
				$("#loader").hide();
				
				
				$("#contentDiv").html(response);
		    	
				$('#contentDiv')[0].scrollIntoView(true);
		   },
		});
	} 
	
}


function viewApprovedDealByLender(dealId,policyId,borrowerId)
{
	$("#preloader").show();
	$("#loader").show();
	
	$.ajax({

		type : "post",
		url : "viewApprovedDealByLender",
		data : "dealId="+dealId+"&policyId="+policyId+"&borrowerId="+borrowerId,
		success : function(response) { 
			
			$("#preloader").hide();
			$("#loader").hide();
			
			$("#profileDiv").show();
			$('#profileDiv')[0].scrollIntoView(true);
			
			$("#profileDiv").html(response);
	    		
	   },
	});
}



function releaseAmountBorrowerRequest(dealId)
{
	$("#modal").show();
    $("#modelBg").show();
    
    $("#heading").html("Confirmation Message");
    
    $("#contentMsg").html("Is the requested amount release to Borrower?");	
    
    $("#buttonDiv").html("");
    
    $("#buttonDiv").append("<input type='hidden' id='dealIdVal' value='"+dealId+"' />");
   
    $("#buttonDiv").append("<input type='button' value='No' id='noId' class='button blue close noAlign' onclick='amountReleaseStatus(this.value)'/>");											
	
    $("#buttonDiv").append("<input type='button' value='Yes' id='yesId' class='button blue close yesAlign' onclick='amountReleaseStatus(this.value)'/>");

}


function amountReleaseStatus(condition)
{
	var dealId = $("#dealIdVal").val();
	
	    if(condition=="Yes")
	    {
	    	$("#preloader").show();
	   	 	$("#loader").show();
	   	  
			$.ajax({  
			 			
				     type : "POST",   
				     data :"dealId="+dealId+"&status="+condition,	  
				     url : "amountReleaseStatus",   	     	     
				     success : function(response) 
				     {  		
				    	 $("#preloader").hide();
				    	 $("#loader").hide();
				    	 
				    	 $("#buttonDiv").html("<a class='button blue close' style='opacity:0.89;float: right; margin: 0 16px 0 22px ! important;' href='#' onclick='closeAlert()'>Close</a>");
				    		
				    	 
				    	 if(response=="success")
				      		{		
							 	document.getElementById("action").value = "url";
							 	document.getElementById("value").value = "loanStatus";
							 	
							    $("#modal").show();
							    $("#modelBg").show();
							    
							    $("#heading").html("Success Message");
							    $("#contentMsg").html("Amount dispouse status has successfully updated.");							    
				      		}
				      	else
				      		{
					      		document.getElementById("action").value = "url";
							 	document.getElementById("value").value = "loanStatus";
						 	
							    $("#modal").show();
							    $("#modelBg").show();
							    
							    $("#heading").html("Failed Message");
							    $("#contentMsg").html("There is some problem arise,Please try again later.");							    								    
				      		}				    	 
				     },
			     }); 
	    }
	    else
	    {
	    	 $("#modal").hide();
			 $("#modelBg").hide();
			 
			 $("#buttonDiv").html("<a class='button blue close' style='opacity:0.89;float: right; margin: 0 16px 0 22px ! important;' href='#' onclick='closeAlert()'>Close</a>");
		    	
	    }
}


//////////////////////END OF LOAN STATUS    ////////////////////////////////





//////////////////////   FINANCIAL FEATURES   ////////////////////////////////




function showSubPolicyType(policyName)
{
	var flag = 'true';
	
	
	if(policyName=="Select Feature Category")
	{
		flag = 'false';
		return false;
	}
	
	if(flag=='true')
	{
		$("#preloader").show();
		$("#loader").show();
		
		$.ajax({
					type : "POST",
					data : "policyName="+policyName,
					url : "showSubPolicyType",								
					success : function(response) {
			            
						$("#preloader").hide();
						$("#loader").hide();
						
					 if(response!="No Data")
		    		 {
			    		 $("#policySubType").html("");
			    		 var array = response.split("/");
			    		 $("#policySubType").append("<option value='Select Loan Type'>Select Loan Type</option>");
			    		 for(var i=0;i<array.length;i++)
							{
								$("#policySubType").append("<option value='"+array[i]+"'>"+array[i]+"</option>")
							}
		    		 }
			    	 else
		    		 {
			    		 $("#policySubType").html("");
			    		 $("#policySubType").append("<option value='Select Loan Type'>Select Loan Type</option>");
			    		 $("#policySubType").append("<option value='No Record'>No Record</option>");
		    		 }
					
				},
		});
	}
	
}


function checkLenderPolicyDetails()
{	
	var name = $("#nameId").val();
	var typeId = $("#typeId").val();
	var policySubType = $("#policySubType").val();
	var minAmtId = $("#minAmtId").val();
	var maxAmtId = $("#maxAmtId").val();	
	var intRateId = $("#intRateId").val();
	var broucher = $("#file").val();
	var minDurId = $("#minDurId").val();
	var maxDurId = $("#maxDurId").val();
	var basicDocErrorId = $("#basicDocErrorId").val();
	
	var polEndId = $("#polEndId").val();	
	
	
	
	
	if(name!="")
	{
		$("#nameId").removeClass("form-error-box");
		$("#featErrorId").hide();	
	}
	
	
	if(typeId!="Select Feature Category")
	{
		$("#typeId").removeClass("form-error-box");
		$("#featTypeErrorId").hide();	
	}
	
	if(policySubType!="Select Policy Type")
	{
		$("#policySubType").removeClass("form-error-box");
		$("#subTypeErrorId").hide();	
	}
	

	if(polEndId!="")
	{
		$("#polEndId").removeClass("form-error-box");
		$("#endDateErrorId").hide();	
	}
	
	
	if(minAmtId!="")
	{
		$("#minAmtId").removeClass("form-error-box");
		$("#minAmtErrorId").hide();	
	}
	
	if(maxAmtId!="")
	{
		$("#maxAmtId").removeClass("form-error-box");
		$("#maxAmtErrorId").hide();	
	}
	
	
	if(intRateId!="")
	{
		$("#intRateId").removeClass("form-error-box");
		$("#intErrorId").hide();	
	}
	
	
	if(minDurId!="")
	{
		$("#minDurId").removeClass("form-error-box");
		$("#minDurErrorId").hide();	
	}
	
	
	if(maxDurId!="")
	{
		$("#maxDurId").removeClass("form-error-box");
		$("#maxDurErrorId").hide();	
	}
		
	
	if(broucher!="")
	{		
	   var fileDetails = broucher;
	   var ext=fileDetails.substring(fileDetails.lastIndexOf('.')+1);
	   var extension = new Array("JPG","jpg","jpeg","JPEG","gif","GIF","png","PNG","pdf","PDF","doc","DOC","docx","DOCX");	
	    		
	   var condition = "NotGranted";
	    
	    for(var m=0;m<extension.length;m++)
		{
		    if(ext==extension[m])
	    	{				    	    
	    	   condition="Granted";				    	    
	    	}				    
		}
	    
		if(condition=="NotGranted")
		{
		    flag = 'false';	
		    $("#docErrorId").show();
		    $("#docErrorId").html("Image and document files are allowed.");
			return false;				  
		}
		
		 var fileDetails1 = document.getElementById("file");
		 var fileSize = fileDetails1.files[0];
		 var fileSizeinBytes = fileSize.size;
		 var sizeinKB = +fileSizeinBytes / 1024;
  		 var sizeinMB = +sizeinKB / 1024;
  		 
  		 if(sizeinMB>2)
		 {
			$("#docErrorId").show();
			$("#docErrorId").html("choose a file less then 2 MB.");
			return false;	 		   		 
		 }  		 
	}
}

function checkAgeLimit(currentId)
{
	var value = $("#"+currentId).val();
	
	var number = /^[0-9]*$/;	
	
	 if(value!="")
	 {		
    	if(!value.match(number))
		{
    		$("#"+currentId).val(""); 
		} 
    	else
    	{
    		var len = value.length;
    		if(len==2)
    		{
    			if(value<18)
            	{
            		$("#"+currentId).val(""); 
            	}
            	else if(value>60)
            	{
            		$("#"+currentId).val(""); 
            	}
            	else
            	{
            		
            	}
    		}
    		
    	}
	 }
	 
}

function checkLenderIncomeLimit(currentId)
{
	var value = $("#"+currentId).val();
	
	var number = /^[0-9]*$/;	
	
	if(value!="")
	{		
    	if(!value.match(number))
		{
    		$("#"+currentId).val(""); 
		} 
	 }
}

function saveLenderPolicyDetails()
{
	var flag = 'true';
	
	var name = $("#nameId").val();
	var typeId = $("#typeId").val();
	var policySubType = $("#policySubType").val();
	var polEndId = $("#polEndId").val();	
	var minAmtId = $("#minAmtId").val();
	var maxAmtId = $("#maxAmtId").val();	
	var intRateId = $("#intRateId").val();
	var broucher = $("#file").val();
	var minDurId = $("#minDurId").val();
	var maxDurId = $("#maxDurId").val();
	var basicDocErrorId = $("#basicDocErrorId").val();
	
	var ageLimit = $("#ageLimit").val();
	var incomeLimit = $("#incomeLimit").val();
	var location = $("#location").val();
		
	  
	
	if(name=="")
	{
		flag = 'false';
		$("#nameId").addClass("form-error-box");
		$("#featErrorId").show();
		$("#featErrorId").html("Please enter a feature Name.");		
	}
	
	if(name!="")
	{
		$("#nameId").removeClass("form-error-box");
		$("#featErrorId").hide();	
	}
	
	if(typeId=="Select Feature Category")
	{
		flag = 'false';
		$("#typeId").addClass("form-error-box");
		$("#featTypeErrorId").show();
		$("#featTypeErrorId").html("Please enter a feature Name.");		
	}
	
	if(typeId!="Select Feature Category")
	{
		$("#typeId").removeClass("form-error-box");
		$("#featTypeErrorId").hide();	
	}
	
	if(policySubType=="Select Policy Type")
	{
		flag = 'false';
		$("#policySubType").addClass("form-error-box");
		$("#subTypeErrorId").show();
		$("#subTypeErrorId").html("Please select policy type.");		
	}
	
	if(policySubType!="Select Policy Type")
	{
		$("#policySubType").removeClass("form-error-box");
		$("#subTypeErrorId").hide();	
	}
	
	if(polEndId=="")
	{
		flag = 'false';
		$("#polEndId").addClass("form-error-box");
		$("#endDateErrorId").show();
		$("#endDateErrorId").html("Please provide expire date of policy.");		
	}
	
	if(polEndId!="")
	{
		$("#polEndId").removeClass("form-error-box");
		$("#endDateErrorId").hide();	
	}
	
	
	if(minAmtId=="")
	{
		flag = 'false';
		$("#minAmtId").addClass("form-error-box");
		$("#minAmtErrorId").show();
		$("#minAmtErrorId").html("Please provide minimum amount for policy.");		
	}
	
	if(minAmtId!="")
	{
		$("#minAmtId").removeClass("form-error-box");
		$("#minAmtErrorId").hide();	
	}
	
	if(maxAmtId=="")
	{
		flag = 'false';
		$("#maxAmtId").addClass("form-error-box");
		$("#maxAmtErrorId").show();
		$("#maxAmtErrorId").html("Please provide maximum amount for policy.");		
	}
	
	if(maxAmtId!="")
	{
		$("#maxAmtId").removeClass("form-error-box");
		$("#maxAmtErrorId").hide();	
	}
	
	if(intRateId=="")
	{
		flag = 'false';
		$("#intRateId").addClass("form-error-box");
		$("#intErrorId").show();
		$("#intErrorId").html("Please provide interest rate for policy.");		
	}
	
	if(intRateId!="")
	{
		$("#intRateId").removeClass("form-error-box");
		$("#intErrorId").hide();	
	}
	
	if(minDurId=="")
	{
		flag = 'false';
		$("#minDurId").addClass("form-error-box");
		$("#minDurErrorId").show();
		$("#minDurErrorId").html("Please provide minimum duration for policy.");		
	}
	
	if(minDurId!="")
	{
		$("#minDurId").removeClass("form-error-box");
		$("#minDurErrorId").hide();	
	}
	
	if(maxDurId=="")
	{
		flag = 'false';
		$("#maxDurId").addClass("form-error-box");
		$("#maxDurErrorId").show();
		$("#maxDurErrorId").html("Please provide maximum duration for policy.");		
	}
	
	if(maxDurId!="")
	{
		$("#maxDurId").removeClass("form-error-box");
		$("#maxDurErrorId").hide();	
	}
	
	
	if(ageLimit=="")
	{
		flag = 'false';
		$("#ageLimit").addClass("form-error-box");
		$("#ageErrorId").show();
		$("#ageErrorId").html("Please select your age.");		
	}
	
	if(ageLimit!="")
	{
		$("#ageLimit").removeClass("form-error-box");
		$("#ageErrorId").hide();	
	}
	
	if(incomeLimit=="Select income limit for the policy")
	{
		flag = 'false';
		$("#incomeLimit").addClass("form-error-box");
		$("#incomeErrorId").show();
		$("#incomeErrorId").html("Please select income limit.");		
	}
	
	if(incomeLimit!="Select income limit for the policy")
	{
		$("#incomeLimit").removeClass("form-error-box");
		$("#incomeErrorId").hide();	
	}
	
	if(location=="Select applicable location")
	{
		flag = 'false';
		$("#location").addClass("form-error-box");
		$("#locationErrorId").show();
		$("#locationErrorId").html("Please select policy applicable location.");		
	}
	
	if(location!="Select applicable location")
	{
		$("#location").removeClass("form-error-box");
		$("#locationErrorId").hide();	
	}
	
	
	if(broucher!="")
	{		
	   var fileDetails = broucher;
	   var ext=fileDetails.substring(fileDetails.lastIndexOf('.')+1);
	   var extension = new Array("JPG","jpg","jpeg","JPEG","gif","GIF","png","PNG","pdf","PDF","doc","DOC","docx","DOCX");	
	    		
	   var condition = "NotGranted";
	    
	    for(var m=0;m<extension.length;m++)
		{
		    if(ext==extension[m])
	    	{				    	    
	    	   condition="Granted";				    	    
	    	}				    
		}
	    
		if(condition=="NotGranted")
		{
		    flag = 'false';	
		    $("#docErrorId").show();
		    $("#docErrorId").html("Image and document files are allowed.");
			return false;				  
		}
		
		 var fileDetails1 = document.getElementById("file");
		 var fileSize = fileDetails1.files[0];
		 var fileSizeinBytes = fileSize.size;
		 var sizeinKB = +fileSizeinBytes / 1024;
  		 var sizeinMB = +sizeinKB / 1024;
  		 
  		 if(sizeinMB>2)
		 {
			flag = 'false';
			$("#docErrorId").show();
			$("#docErrorId").html("choose a file less then 2 MB.");
			return false;	 		   		 
		 }  	  		 
	}
		
		if(flag=='true')
		{							
			 var formData = new FormData($("#policyDetailsForm")[0]);
			
			 formData.append("file", file.files[0]);
			 
			 $("#preloader").show();
			 $("#loader").show();
				
			  $.ajax({
						type : "POST",
						data : formData,
						url : "saveLenderPolicyDetails",								
						success : function(response) {
				           
							$("#preloader").hide();
							$("#loader").hide();
							
							 if(response=="success")
					      		{		
								 	document.getElementById("action").value = "url";
								 	document.getElementById("value").value = "addFeature";
								 	
								    $("#modal").show();
								    $("#modelBg").show();
								    
								    $("#heading").html("Success Message");
								    $("#contentMsg").html("Your financial policy successfully saved.Wait for Approval from Admin.");
								    
					      		}
					      	else
					      		{
						      		document.getElementById("action").value = "url";
								 	document.getElementById("value").value = "addFeature";
							 	
								    $("#modal").show();
								    $("#modelBg").show();
								    
								    $("#heading").html("Failed Message");
								    $("#contentMsg").html("There is some problem arise,Please try again later.");
								    								    
					      		}
						},
						cache: false,
				        contentType: false,
				        processData: false,
				});			   	
		}
}


function editFinanceFeature(policyId)
{
	 $("#preloader").show();
	 $("#loader").show();
		
	  $.ajax({
				type : "POST",
				data : "policyId="+policyId,
				url : "showFeatureFormToUpdate",								
				success : function(response) {
		           
					$("#preloader").hide();
					$("#loader").hide();
					
					$("#financeFeatureFormId").html(response);
					$("#financeFeatureFormId").show();
					
					$('#financeFeatureFormId')[0].scrollIntoView(true);
					
					
					
				},
		});			
}


function disContinueFinanceFeature(policyId)
{
	 $("#preloader").show();
	 $("#loader").show();
	
	
	  $.ajax({
				type : "POST",
				data : "policyId="+policyId,
				url : "disContinueFinanceFeature",								
				success : function(response) {
		           
					$("#preloader").hide();
					$("#loader").hide();
					
					$("#modal").show();
					$("#modelBg").show();
					    
					$("#heading").html("Policy information");
					$("#content").html(response);							    
					    
				},
		});			
}

function closeDisContinue(condition,policyId)
{
	if(condition=="Yes")
	{
		$("#modal").hide();
	    $("#modelBg").hide();
	    
		$("#preloader").show();
		$("#loader").show();
		
		$.ajax({
			type : "POST",
			data : "policyId="+policyId,
			url : "disContinuePolicy",								
			success : function(response) {
	           
					$("#preloader").hide();
					$("#loader").hide();
					
					if(response=="success")
		      		{
						
					 	document.getElementById("action").value = "url";
					 	document.getElementById("value").value = "addFeature";
					 	
					    $("#modal").show();
					    $("#modelBg").show();
					    
					    $("#heading").html("Success Message");
					   
					    $('#contentDiv').css({ "padding":'36px',"text-align":'center',"color":'#00b6f5'});
					    
					    $("#contentDiv").html("Your financial policy discontinue successfully.");
					    
					    $("#buttonDiv").html("<a class='button blue close' style='opacity:0.89;float: right; margin: 0 16px 0 22px ! important;' href='#' onclick='closeAlert()'>Close</a>");
					    
		      		}
		      	    else
		      		{
		      	    	
			      		document.getElementById("action").value = "url";
					 	document.getElementById("value").value = "addFeature";
				 	
					    $("#modal").show();
					    $("#modelBg").show();
					    
					    $("#heading").html("Failed Message");
					    $("#contentMsg").html("There is some problem arise,Please try again later.");
					    								    
		      		}  
				},
		});	
	}
	else
	{
		$("#modal").hide();
		$("#modelBg").hide();		 
	}
}

//   Add components script for Lender requred Documents

function addComponent(docId)
{
	var value = $("#"+docId).val();	
	
	var existValue = document.getElementById("documentsNeedId").value;
	
	if(existValue!="")
	{
		document.getElementById("documentsNeedId").value = existValue+","+value;
	}
	else
	{
		document.getElementById("documentsNeedId").value = value;
	}
}

// Not using 
function addComponent404(docId)
{
	
	var value = $("#"+docId).val();	
	
	var docTypeId = docId.substring(0, docId.length-1);
	var docTypeName = document.getElementById(docTypeId).value;
	
	var existValue = document.getElementById("documentsNeedId").value;
	
	if(existValue!="")
	{		
		var existArray = existValue.split("/");
		
		// Add the latest pattern on it 
		
		var docPattern = "";
		var flag = "true";
		
		for(var i=0;i<existArray.length;i++)
		{
			var eachValue = existArray[i];
			var eachArray = eachValue.split(":");
			if(eachArray[0]==docTypeName)
			{
				docPattern = docTypeName+":"+eachArray[1]+","+value;
				flag = "false";
			}
		}
		
		if(flag=="true")
		{
			docPattern = docTypeName+":"+value;
		}
		
		// Generate the Latest pattern after update
		
		
		var newPattern = "";
		var antFlag = "true";
		
		for(var i=0;i<existArray.length;i++)
		{
			var eachArray = existArray[i].split(":");
			if(eachArray[0]==docTypeName)
			{
				newPattern += docPattern +"/";
				antFlag = "false";
			}
			else
			{
				newPattern += existArray[i] +"/";
			}
		}
		
		if(antFlag=="true")
		{
			newPattern += docPattern +"/";
		}
		
		newPattern = newPattern.substring(0, newPattern.length-1);
		
		document.getElementById("documentsNeedId").value = newPattern;
	}
	else
	{
		document.getElementById("documentsNeedId").value = docTypeName+":"+value;
	}
}


 ////////////////////     Remove components script for Lender requred Documents


function removeComponent(docId)
{
	var newPattern = "";
	
	var value = $("#"+docId).val();	
	
	var existValue = document.getElementById("documentsNeedId").value;
	
	var valueArray = existValue.split(",");
	
	for(var j=0;j<valueArray.length;j++)
	{
		if(valueArray[j]!=value)
		{
			newPattern += valueArray[j]+",";
		}
	}
	
	if(newPattern!="")
	{
		newPattern = newPattern.substring(0, newPattern.length-1);
	}
	
	document.getElementById("documentsNeedId").value = newPattern;	
}

//Not using 
function removeComponent404(docId)
{
	var value = $("#"+docId).val();	
	
	var docTypeId = docId.substring(0, docId.length-1);
	var docTypeName = document.getElementById(docTypeId).value;
	
	var existValue = document.getElementById("documentsNeedId").value;
	
	if(existValue!="")
	{
		var existArray = existValue.split("/");
		
		// Add the latest pattern on it 
		
		var docPattern = "";
		var valuePattern = "";
		
		for(var i=0;i<existArray.length;i++)
		{
			var eachArray = existArray[i].split(":");
			if(eachArray[0]==docTypeName)
			{
				var valueArray = eachArray[1].split(",");
				
				for(var j=0;j<valueArray.length;j++)
				{
					if(valueArray[j]!=value)
					{
						valuePattern += valueArray[j]+",";
					}
				}
			}
		}
		
		valuePattern = valuePattern.substring(0, valuePattern.length-1);
		
		if(valuePattern!="")
		{
			docPattern = docTypeName+":"+valuePattern;
		}
		
		
		
		// Generate the Latest pattern after update
		
		var newPattern = "";
		
		for(var i=0;i<existArray.length;i++)
		{
			var eachArray = existArray[i].split(":");			
			if(eachArray[0]==docTypeName)
			{
				if(docPattern!="")
				{
					newPattern += docPattern +"/";
				}
			}
			else
			{
				newPattern += existArray[i] + "/";
			}
		}
		
		newPattern = newPattern.substring(0, newPattern.length-1);
		
		document.getElementById("documentsNeedId").value = newPattern;
	}
	
}



function getBasicDocName(docId)
{
	var flag = document.getElementById(docId);	
	
	var value = $("#"+docId).val();	
	if(flag.checked)
	{		
		addComponent(docId)
	}
	else
	{
		removeComponent(docId)				
	}	
}



//////////////////////   FINANCIAL SETTINGS   ////////////////////////////////


function saveIdentityDetails()
{
	 var flag = 'true';
     
	 var pincode = /^[0-9]{6}$/;	
	 var poi= /^[a-zA-Z0-9]{6,20}$/;
		
	    var presentDoor = $("#preDoorId").val();
		var presentBuilding = $("#preBuildingId").val();
		var presentStreet = $("#preStreetId").val();
		var presentArea = $("#preAreaId").val();
		var presentCity = $("#preCityId").val();
		var presentPincode = $("#prePincodeId").val();
		
	    if(presentDoor=="")
		{		
			flag = 'false';
			$("#preDoorId").addClass("form-error-box");
			$("#preDoorErrId").show();
	        $("#preDoorErrId").html("Enter door detail.");	
	        return false;              
		}
	    
	    if(presentDoor!="")
		{		
	    	$("#preDoorId").removeClass("form-error-box");
			$("#preDoorErrId").hide();          
		}
	    
	    if(presentBuilding=="")
		{		
			flag = 'false';
			$("#preBuildingId").addClass("form-error-box");
			$("#preBuildingErrId").show();
	        $("#preBuildingErrId").html("Enter building detail.");	
	        return false;              
		}
	    
	    if(presentBuilding!="")
		{		
	    	$("#preBuildingId").removeClass("form-error-box");
			$("#preBuildingErrId").hide();          
		}
	    
	    if(presentStreet=="")
		{		
			flag = 'false';
			$("#preStreetId").addClass("form-error-box");
			$("#preStreetErrId").show();
	        $("#preStreetErrId").html("Enter street detail.");	
	        return false;              
		}
	    
	    if(presentStreet!="")
		{		
	    	$("#preStreetId").removeClass("form-error-box");
			$("#preStreetErrId").hide();          
		}
	    
	    if(presentArea=="")
		{		
			flag = 'false';
			$("#preAreaId").addClass("form-error-box");
			$("#preAreaErrId").show();
	        $("#preAreaErrId").html("Enter area name.");	
	        return false;              
		}
	    
	    if(presentArea!="")
		{		
	    	$("#preAreaId").removeClass("form-error-box");
			$("#preAreaErrId").hide();          
		}
	    
	    if(presentCity=="")
		{		
			flag = 'false';
			$("#preCityId").addClass("form-error-box");
			$("#preCityErrId").show();
	        $("#preCityErrId").html("Enter city name.");	
	        return false;              
		}
	    
	    if(presentCity!="")
		{		
	    	$("#preCityId").removeClass("form-error-box");
			$("#preCityErrId").hide();          
		}
	    
	    
	    if(presentPincode=="")
		{		
			flag = 'false';
			$("#prePincodeId").addClass("form-error-box");
			$("#prePinErrId").show();
	        $("#prePinErrId").html("Enter your present pincode.");	
	        return false;              
		}
	    
	    if(presentPincode!="")
		{		
	    	if(!presentPincode.match(pincode))
			{
	    		flag = 'false';
	    		$("#prePincodeId").addClass("form-error-box");
	    		$("#prePinErrId").show();
	            $("#prePinErrId").html("Enter a valid pincode.");	
	            return false; 
			}
	    	else
			{
	    		$("#prePincodeId").removeClass("form-error-box");
	    		$("#prePinErrId").hide();          
			}
		}
	    
	    
	    if(flag=='true')
		{
		
	    	 var formData = new FormData($("#profileIdnForm")[0]);
			
			 formData.append("file", file.files[0]);
			
			 
			 $("#preloader").show();
			 $("#loader").show();
	    
			     $.ajax({  
			 			
				     type : "post",   
				     url : "saveProfileIdnDetails", 
				     data :formData,	     	     	     
				     success : function(response) 
				     {  		
				    	 $("#preloader").hide();
				    	 $("#loader").hide();
				    	 
						 if(response=="success")
				      		{		
							 	document.getElementById("action").value = "url";
							 	document.getElementById("value").value = "lenderSettings";
							 	
							    $("#modal").show();
							    $("#modelBg").show();
							    
							    $("#heading").html("Success Message");
							    $("#contentMsg").html("Your profile details has been changed successfully.");
							    
				      		}
				      	else
				      		{
					      		document.getElementById("action").value = "url";
							 	document.getElementById("value").value = "lenderSettings";
						 	
							    $("#modal").show();
							    $("#modelBg").show();
							    
							    $("#heading").html("Failed Message");
							    $("#contentMsg").html("There is some problem arise,Please try again later.");
							    								    
				      		}
						 
				     },  
				     cache: false,
				     contentType: false,
				     processData: false,
			     }); 
				
		}
	    
}

function saveLenderPassword()
{
	var flag = 'true';
		
 var curPassword = $("#curPasswordId").val();
 var newPassword = $("#newPasswordId").val();
 var cnfPassword = $("#cnfPasswordId").val();
 
 
 if(curPassword=="")
	{		
		flag = 'false';
		$("#curPasswordId").addClass("form-error-box");
		$("#currentErrorId").show();
		$("#currentErrorId").html("Please enter your password.");	
		return false;              
	}
 
 if(curPassword!="")
	{		
	    $("#curPasswordId").removeClass("form-error-box");
		$("#currentErrorId").hide();          
	}
 
 if(newPassword=="")
	{		
		flag = 'false';
		$("#newPasswordId").addClass("form-error-box");
		$("#newErrorId").show();
		$("#newErrorId").html("Please enter your password.");	
		return false;              
	}
 
 if(newPassword!="")
	{		
	 	$("#newPasswordId").removeClass("form-error-box");
		$("#newErrorId").hide();          
	}
 
 
 if(cnfPassword=="")
	{		
		flag = 'false';
		$("#cnfPasswordId").addClass("form-error-box");
		$("#cnfErrorId").show();
		$("#cnfErrorId").html("Please confirm your password.");	
		return false;              
	}
 
 if(cnfPassword!="")
	{		
	 	$("#cnfPasswordId").removeClass("form-error-box");
		$("#cnfErrorId").hide();          
	}
 
 
 if(cnfPassword!=newPassword)
	{		
 	  flag = 'false';
	  $("#cnfErrorId").show();
      $("#cnfErrorId").html("Please confirm your password it not matches with password.");	
      return false;     
	}
 
	if(flag=='true')
	{	
	    	$("#preloader").show();
	   	 	$("#loader").show();
	   	  
		    $.ajax({  
		 			
			     type : "post",   
			     url : "changeLenderPassword", 
			     data :$('#passwordForm').serialize(),	     	     	     
			     success : function(response) 
			     {  		
			    	 $("#preloader").hide();
			    	 $("#loader").hide();
			    	 
			    	 if(response=="success")
			      		{		
						 	document.getElementById("action").value = "url";
						 	document.getElementById("value").value = "lenderSettings";
						 	
						    $("#modal").show();
						    $("#modelBg").show();
						    
						    $("#heading").html("Success Message");
						    $("#contentMsg").html("Your password has been changed successfully.");						    
			      		}
			    	 else if(response=="Not Matched")
						{
						    $("#currentErrorId").show();
					        $("#currentErrorId").html("Incorrect current password");	
						}
			      	 else
			      		{
				      		document.getElementById("action").value = "url";
						 	document.getElementById("value").value = "lenderSettings";
					 	
						    $("#modal").show();
						    $("#modelBg").show();
						    
						    $("#heading").html("Failed Message");
						    $("#contentMsg").html("There is some problem arise,Please try again later.");						    								    
			      		}			    	
			     },  
		     }); 			
	}
	
	
	
}




function editLenderBasic()
{
	window.location = "lenderSettings";	
}

function editLenderDoc()
{
	 window.location = "lenderDoc";
}


function saveLenderDoc()
{
	var flag = 'true';
	
	var docType = $("#docType").val();
	var file = $("#file").val();
	
	if(docType=="Select document type")
	{
		flag = 'false';
		$("#typeErrId").show();
		$("#typeErrId").html("Select a document type.")
		
	}
	
	if(docType!="Select document type")
	{
		$("#typeErrId").hide();
	}
	
	if(file=="")
	{
		flag = 'false';
		$("#fileErrId").show();
		$("#fileErrId").html("Browse a file to upload.");	
	}
	
	if(file!="")
	{
		   var fileDetails = file;
		   var ext=fileDetails.substring(fileDetails.lastIndexOf('.')+1);
		   var extension = new Array("JPG","jpg","jpeg","JPEG","gif","GIF","png","PNG");	
		    		
		   var condition = "NotGranted";
		    
		   for(var m=0;m<extension.length;m++)
			{
			    if(ext==extension[m])
		    	{				    	    
		    	   condition="Granted";				    	    
		    	}				    
			}
		    
			if(condition=="NotGranted")
			{
			    flag = 'false';	
			    $("#fileErrId").show();
			    $("#fileErrId").html("only Image files are allowed.");
				return false;				  
			}
			
			 var fileDetails1 = document.getElementById("file");
			 var fileSize = fileDetails1.files[0];
			 var fileSizeinBytes = fileSize.size;
			 var sizeinKB = +fileSizeinBytes / 1024;
	  		 var sizeinMB = +sizeinKB / 1024;
	  		 
	  		 if(sizeinMB>1)
			 {
				$("#fileErrId").show();
				$("#fileErrId").html("choose a file less then 1 MB.");
				return false;	 		   		 
			 }  
	}
	
	if(flag=='true')
	{   	    	
	   	var formData = new FormData($("#lenderDocForm")[0]);
		 
		$("#preloader").show();
   	 	$("#loader").show();
   	  
		$.ajax({  
		 			
			     type : "POST",   
			     data :formData,	  
			     url : "saveLenderDoc",   	     	     
			     success : function(response) 
			     {  		
			    	 $("#preloader").hide();
			    	 $("#loader").hide();
			    	 
			    	 if(response=="success")
			      		{		
						 	document.getElementById("action").value = "url";
						 	document.getElementById("value").value = "lenProfile";
						 	
						    $("#modal").show();
						    $("#modelBg").show();
						    
						    $("#heading").html("Success Message");
						    $("#contentMsg").html("Your documents have been successfully uploaded.");							    
			      		}
			      	else
			      		{
				      		document.getElementById("action").value = "url";
						 	document.getElementById("value").value = "lenProfile";
					 	
						    $("#modal").show();
						    $("#modelBg").show();
						    
						    $("#heading").html("Failed Message");
						    $("#contentMsg").html("There is some problem arise,Please try again later.");							    								    
			      		}				    	 
			     },
				 cache: false,
		         contentType: false,
		         processData: false,  
		     }); 
			
	}
	
}




function viewLenAddedDoc(documentId)
{
	$("#preloader").show();
	$("#loader").show();
	  
	 $.ajax({  
			
	     type : "post",   
	     url : "viewLenAttachDoc", 
	     data :"documentId="+documentId,	     	     	     
	     success : function(response) 
	     {  		
	    	 $("#preloader").hide();
	    	 $("#loader").hide();
	    	 
	    	 $("#viewAttachedDocument").show();
	    	 
	    	 $("#viewAttachedDocument").html(response);	    	 
	     },  
     }); 
}

function yesBorrowerRequest(dealId,interest)
{
	$("#modal").show();
    $("#modelBg").show();
    
    $("#heading").html("Confirmation Message");
    
    $("#contentMsg").html("Are you sure to accept the Borrower request.");	
    
    $("#buttonDiv").html("");
    
    $("#buttonDiv").append("<input type='hidden' id='policyIdVal' value='"+dealId+"' />");
    $("#buttonDiv").append("<input type='hidden' id='interestIdVal' value='"+interest+"' />");
    
    $("#buttonDiv").append("<input type='button' value='No' id='noId' class='button blue close noAlign' onclick='rejectBorrowerRequest(this.value)'/>");											
	
    $("#buttonDiv").append("<input type='button' value='Yes' id='yesId' class='button blue close yesAlign' onclick='checkBorrowerRequest(this.value)'/>");
}


function noBorrowerRequest(dealId,interest)
{
	$("#modal").show();
    $("#modelBg").show();
    
    $("#heading").html("Confirmation Message");
    
    $("#contentMsg").html("Are you sure to reject the Borrower request.");	
    
    $("#buttonDiv").html("");
    
    $("#buttonDiv").append("<input type='hidden' id='policyIdVal' value='"+dealId+"' />");
    $("#buttonDiv").append("<input type='hidden' id='interestIdVal' value='"+interest+"' />");
    
    $("#buttonDiv").append("<input type='button' value='No' id='noId' class='button blue close noAlign'  onclick='rejectBorrowerRequest(this.value)'/>");											
	
    $("#buttonDiv").append("<input type='button' value='Yes' id='yesId' class='button blue close yesAlign' onclick='rejectBorrowerRequest(this.value)'/>");
}




function rejectBorrowerRequest(confirmation)
{
	var dealId = $("#policyIdVal").val();
	var interest = $("#interestIdVal").val();
	
	    if(confirmation=="Yes")
	    {
	    	$("#preloader").show();
	   	 	$("#loader").show();
	   	    
			$.ajax({  			 			
				     type : "POST",   
				     data :"dealId="+dealId+"&interest="+interest,	  
				     url : "showReasonBox",   	     	     
				     success : function(response) 
				     {  		
				    	 $("#preloader").hide();
				    	 $("#loader").hide();
				    	 
				    	 $("#modal").hide();
						 $("#modelBg").hide();		
						 
						 $("#resonContentId").html(response);
						 
						 $("#reasonDivId").show();
						 $("#modelBg").show();
				     },
			     }); 
	    }
	    else
	    {
	    	 $("#modal").hide();
			 $("#modelBg").hide();
			 
			 $("#buttonDiv").html("<a class='button blue close' style='opacity:0.89;float: right; margin: 0 16px 0 22px ! important;' href='#' onclick='closeAlert()'>Close</a>");		    	
	    }
}

function closeReasonBox()
{
	$("#modal").hide();
	$("#modelBg").hide();
	$("#reasonDivId").hide(); 
	$("#buttonDiv").html("<a class='button blue close' style='opacity:0.89;float: right; margin: 0 16px 0 22px ! important;' href='#' onclick='closeAlert()'>Close</a>");		    	
}

function rejectBorrowerProcessRequest(dealId)
{
	$("#preloader").show();
	$("#loader").show();
	    
	$.ajax({  			 			
		     type : "POST",   
		     data :"dealId="+dealId,	  
		     url : "showRejectReasonBox",   	     	     
		     success : function(response) 
		     {  		
		    	 $("#preloader").hide();
		    	 $("#loader").hide();
		    	 
		    	 $("#modal").hide();
				 $("#modelBg").hide();		
				 
				 $("#resonContentId").html(response);
				 
				 $("#reasonDivId").show();
				 $("#modelBg").show();
		     },
	     }); 
}


function submitReason()
{
	$("#preloader").show();
	$("#loader").show();
	  
	$("#modal").hide();
	$("#modelBg").hide();
	$("#reasonDivId").hide();
	 
	var formData = new FormData($("#reasonForm")[0]);
	
	$.ajax({  
	 			
		     type : "POST",   
		     data : formData,	  
		     url : "submitReason",   	     	     
		     success : function(response) 
		     {  		
		    	 $("#preloader").hide();
		    	 $("#loader").hide();
		    	 
		    	 $("#buttonDiv").html("<a class='button blue close' style='opacity:0.89;float: right; margin: 0 16px 0 22px ! important;' href='#' onclick='closeAlert()'>Close</a>");
		    		
		    	 var responseArray = response.split("/");
		    	 
		    	 if(responseArray[1]=="success")
		      		{		
					 	document.getElementById("action").value = "url";
					 	document.getElementById("value").value = responseArray[0];
					 	
					    $("#modal").show();
					    $("#modelBg").show();
					    
					    $("#heading").html("Success Message");
					    $("#contentMsg").html("Your interest has successfully updated.");							    
		      		}
		      	 else
		      		{
			      		document.getElementById("action").value = "url";
					 	document.getElementById("value").value = responseArray[0];
				 	    
					    $("#modal").show();
					    $("#modelBg").show();
					    
					    $("#heading").html("Failed Message");
					    $("#contentMsg").html("There is some problem arise,Please try again later.");							    								    
		      		}				    	 
		     },
		     cache: false,
		     contentType: false,
		     processData: false,
	     }); 
}

function checkBorrowerRequest(confirmation)
{
	var dealId = $("#policyIdVal").val();
	var interest = $("#interestIdVal").val();
	
	    if(confirmation=="Yes")
	    {
	    	$("#preloader").show();
	   	 	$("#loader").show();
	   	  
			$.ajax({  
			 			
				     type : "POST",   
				     data :"dealId="+dealId+"&interest="+interest,	  
				     url : "checkBorrowerRequest",   	     	     
				     success : function(response) 
				     {  		
				    	 $("#preloader").hide();
				    	 $("#loader").hide();
				    	 
				    	 $("#buttonDiv").html("<a class='button blue close' style='opacity:0.89;float: right; margin: 0 16px 0 22px ! important;' href='#' onclick='closeAlert()'>Close</a>");
				    		
				    	 
				    	 if(response=="success")
				      		{		
							 	document.getElementById("action").value = "url";
							 	document.getElementById("value").value = "borrowerRqst";
							 	
							    $("#modal").show();
							    $("#modelBg").show();
							    
							    $("#heading").html("Success Message");
							    $("#contentMsg").html("Your interest has successfully updated.");							    
				      		}
				      	else
				      		{
					      		document.getElementById("action").value = "url";
							 	document.getElementById("value").value = "borrowerRqst";
						 	
							    $("#modal").show();
							    $("#modelBg").show();
							    
							    $("#heading").html("Failed Message");
							    $("#contentMsg").html("There is some problem arise,Please try again later.");							    								    
				      		}				    	 
				     },
			     }); 
	    }
	    else
	    {
	    	 $("#modal").hide();
			 $("#modelBg").hide();
			 
			 $("#buttonDiv").html("<a class='button blue close' style='opacity:0.89;float: right; margin: 0 16px 0 22px ! important;' href='#' onclick='closeAlert()'>Close</a>");
		    	
	    }
	
}

function searchExistPolicy()
{
	var searchType = $("#searchType").val();
	var policyTypeValue = $("#policyTypeValue").val();
	
	$("#preloader").show();
	$("#loader").show();
	  
	 $.ajax({  
			
	     type : "post",   
	     url : "searchExistPolicy", 
	     data :"searchType="+searchType+"&policyTypeValue="+policyTypeValue,	     	     	     
	     success : function(response) 
	     {  		
	    	 $("#preloader").hide();
	    	 $("#loader").hide();
	    	 
	    	 $("#existPolicyDiv").show();
	    	 
	    	 $('#existPolicyDiv')[0].scrollIntoView(true);
	    	 
	    	 $("#existPolicyDiv").html(response);
	    	 
	     },  
     }); 
}

function viewPolicy(policyId)
{
	$("#preloader").show();
	$("#loader").show();
	  
	 $.ajax({  
			
	     type : "post",   
	     url : "viewPolicy", 
	     data :"policyId="+policyId,	     	     	     
	     success : function(response) 
	     {  		
	    	 $("#preloader").hide();
	    	 $("#loader").hide();
	    	 
	    	 $("#viewPolicyDetailsDiv").show();
	    	 
	    	 $('#viewPolicyDetailsDiv')[0].scrollIntoView(true);
	    	 
	    	 $("#viewPolicyDetailsDiv").html(response);
	    	 
	     },  
     }); 
}



function searchNotification(userId)
{
    var flag = 'true';
	
	var searchType = $("#searchType").val();
	
	var fromDateId = $("#fromDateId").val();
	var toDateId = $("#toDateId").val();
	
	if (flag=="true") 
	{
		$("#preloader").show();
		$("#loader").show();
		
		$.ajax({

			type : "post",
			url : "searchNotification",
			data : "userId="+userId+"&searchType="+searchType+"&fromDateId="+fromDateId+"&toDateId="+toDateId,
			success : function(response) { 
				
				
				
				$("#preloader").hide();
				$("#loader").hide();
				
				$("#hiddenDiv").slideDown();
				$("#contentDiv").html(response);
		    	
				$('#contentDiv')[0].scrollIntoView(true);
				
				
		   },
		});
	} 
	
}



function manageNotify(notifyId,condition,link)
{
	$("#preloader").show();
	$("#loader").show();
	
	if(condition=="View")
	{
		$.ajax({  
			
		     type : "post",   
		     url : "viewNotification", 
		     data :"notifyId="+notifyId,	     	     	     
		     success : function(response) 
		     {  		
		    	 $("#preloader").hide();
		    	 $("#loader").hide();
		    	 
		    	 $("#notificationDiv").html(response);
		    	 $("#notificationDiv").slideDown();
		    	 
		    	 $('#notificationDiv')[0].scrollIntoView(true);
		    	 
		    	 
		    	 
		     },  
	     }); 
	}
	else
	{
		$.ajax({  
			
		     type : "post",   
		     url : "deleteNotification", 
		     data :"notifyId="+notifyId,	     	     	     
		     success : function(response) 
		     {  		
		    	 $("#preloader").hide();
		    	 $("#loader").hide();
		    	 
		    	 if(response=="success")
		      		{		
					 	document.getElementById("action").value = "url";
					 	document.getElementById("value").value = link;
					 	
					    $("#modal").show();
					    $("#modelBg").show();
					    
					    $("#heading").html("Success Message");
					    $("#contentMsg").html("Message deleted successfully..");							    
		      		}
		      	else
		      		{
			      		document.getElementById("action").value = "url";
					 	document.getElementById("value").value = link;
				 	
					    $("#modal").show();
					    $("#modelBg").show();
					    
					    $("#heading").html("Failed Message");
					    $("#contentMsg").html("There is some problem arise,Please try again later.");							    								    
		      		}	
		    	 
		     },  
	     }); 
	}
	 
}

function closeNotify()
{
	 $("#notificationDiv").slideUp();
	 
	 $('#contentDiv')[0].scrollIntoView(true);
}
