

$(document).ready(function(){

        	/*demo.initChartist();

        	$.notify({
            	icon: 'pe-7s-gift',
            	message: "Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for every web developer."

            },{
                type: 'info',
                timer: 4000
            });
        	*/
    	});





// Borrower side menu activation script


function activeSideMenu(menuType)
{ 		
	
	if(menuType=='menu1')
		{
		     $("#menu1").attr('class', 'active');
		}
	else if(menuType=='menu2')
		{
			 $("#menu2").attr('class', 'active');				
		}
	else if(menuType=='menu3')
		{		
			 $("#menu3").attr('class', 'active');	 
		}
	else if(menuType=='menu4')
		{		  
			 $("#menu4").attr('class', 'active');			  
		}
	else if(menuType=='menu5')
		{			
		     $("#menu5").attr('class', 'active');			 
		}
	else 
		{			
		     $("#menu6").attr('class', 'active');			 
		}
		
}



function hideNotification()
{
	$("#successId").hide();
}


// ENd of Borrower side menu activation script




/////////////// START OF BORROWER SEARCH FINANCE AND APPLY //////////////////////////////

function amountFormat(id)
{
	var number = new RegExp('^[0-9_,]+$');
	
	var amountId = $("#"+id).val();
	
	if(amountId!="")
	{
		if(!amountId.match(number))  
        {  			
			$("#"+id).val("");
        }
		else
		{
			$("#"+id).val(amountId.toLocaleString());
			
			$("#amtErrId").hide();
			$("#amountId").removeClass('form-error-box');
		}
	}
	
}

$('input.number').keyup(function(event) {

	  // skip for arrow keys
	  if(event.which >= 37 && event.which <= 40) return;

	  // format number
	  $(this).val(function(index, value) {
	    return value
	    .replace(/\D/g, "")
	    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
	    ;
	  });
	});


$('input.formatNumber').keyup(function(event) {

	  // skip for arrow keys
	  if(event.which >= 37 && event.which <= 40) return;

	  // format number
	  $(this).val(function(index, value) {
	    return value
	    .replace(/\D/g, "")
	    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
	    ;
	  });
	});


function searchFinance()
{
	var flag = 'true';
	
    var typeId = $("#typeId").val();
    var amountId = $("#amountId").val();
    var timeId = $("#timeId").val();
    var locationId = $("#locationId").val();
    
    
    if(typeId=="Select your loan type")
	{		
		flag = 'false';
		
		$("#typeId").addClass("form-error-box");
		$("#typeErrId").show();
        $("#typeErrId").html("Please select policy type.");	
        return false;              
	}
    
    if(typeId!="Select your loan type")
	{		
    	$("#typeId").removeClass("form-error-box");
		$("#typeErrId").hide();          
	}
    
    if(amountId=="")
	{		
		flag = 'false';
		$("#amountId").addClass("form-error-box");
		$("#amtErrId").show();
        $("#amtErrId").html("Please enter your desired amount.");	
        return false;              
	}
    
    if(amountId!="")
	{		
    	$("#amountId").removeClass("form-error-box");
		$("#amtErrId").hide();          
	}
    
   /* if(timeId=="Select policy duration")
	{		
		flag = 'false';
		$("#timeId").addClass("form-error-box");
		$("#timeErrId").show();
        $("#timeErrId").html("Please select amount limit.");	
        return false;              
	}
    
    if(timeId!="Select policy duration")
	{		
    	$("#timeId").removeClass("form-error-box");
		$("#timeErrId").hide();          
	}*/
    
    
    
    
    if(flag=='true')
    {
    	$("#preloader").show();
   	 	$("#loader").show();
   	  
	   	var formData = new FormData($("#searchFinanceForm")[0]);
	   	
	   	
		     $.ajax({  
		 			
			     type : "POST",   
			     data :formData,	  
			     url : "searchFinance",   	     	     
			     success : function(response) 
			     {  		
			    	 $("#preloader").hide();
			    	 $("#loader").hide();
			    	 
			    	 $("#financeApplyForm").slideUp();
			    	 
			    	 $("#financeInfo").slideUp();
			    	 
			    	 $("#searchResult").slideDown();
			    	 
			    	 $("#searchResult").html(response);
			    	 
			     },
					cache: false,
			        contentType: false,
			        processData: false,  
		     }); 
    }
    
}


function showPolicyInfoToBorrower(policyId)
{
	 $.ajax({  
			
	     type : "POST",   
	     data :"policyId="+policyId,	  
	     url : "showPolicyInfoToBorrower",   	     	     
	     success : function(response) 
	     {  		
	    	 $("#preloader").hide();
	    	 $("#loader").hide();
	    	 

	    	 $("#financeApplyForm").slideUp();
	    	 
	    	 
	    	 $("#financeInfo").slideDown();
	    	 
	    	 $("#financeInfo").html(response);
	    	 
	     },	
     }); 
}


function closePolicyInfo()
{
	$("#financeInfo").slideUp();	 
}

function applyPolicy(policyId)
{	
	$.ajax({  
		
	     type : "POST",   
	     data :"policyId="+policyId,	  
	     url : "checkPolicyApplicableCriteria",   	     	     
	     success : function(response) 
	     {  		
	    	 $("#preloader").hide();
	    	 $("#loader").hide();
	    	 
	    	 if(response=="Yes")
	    	 {
	    		 $.ajax({  
	    				
	    		     type : "POST",   
	    		     data :"policyId="+policyId,	  
	    		     url : "applyPolicyForm",   	     	     
	    		     success : function(response) 
	    		     {  		
	    		    	 $("#preloader").hide();
	    		    	 $("#loader").hide();
	    		    	 
	    		    	 $("#financeInfo").slideUp();	 
	    		    	 
	    		    	 $("#financeApplyForm").slideDown();
	    		    	 
	    		    	 $("#financeApplyForm").html(response);
	    		    	 
	    		     },	
	    	    }); 
	    	 }
	    	 else
	    	 {
	    		 
	    	 }
	    	 
	     },	
   }); 
	
	
	
}

function applyForFinance()
{
	var flag = 'true';
	
	var alfanumeric = /^[a-zA-Z0-9]*$/;        	
	var nameWithOutSpace = /^[a-zA-Z]+$/;
	var nameWithSpace = /^[a-zA-Z\s]*$/;	
	var phNo = /^[0-9]{10}$/;	
	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	var poi= /^[a-zA-Z0-9]{6,20}$/;
	
	
	var altEmailId = $("#altEmailId").val();
    var regMobileNo = $("#regMobileNo").val();
    var altMobileNo = $("#altMobileNo").val();
    
    var dobId = $("#dobId").val();
    var idTypeId = $("#idTypeId").val();
    var idNoId = $("#idNoId").val();
    
    var preAddressId = $("#preAddressId").val();
    var perAddressId = $("#perAddressId").val();
    
    var income_type = $("#income_type").val();
    var income_annualy = $("#income_annualy").val();
    
    var deal_amount = $("#deal_amount").val();
    var deal_duration = $("#deal_duration").val();
    
    var requiredDoc = $("#requiredDoc").val();
   
    
    if(altEmailId=="")
	{
    	flag = 'false';
		$("#altEmailErrId").show();
        $("#altEmailErrId").html("Please enter alternate email id.");	
        $("#altEmailId").addClass("form-error-box");    
	}
    
    if(altEmailId!="")
	{
		if(!altEmailId.match(email))  
        {  			
			flag = 'false';
			$("#altEmailErrId").show();
	        $("#altEmailErrId").html("Please enter a valid email id.");	
	        $("#altEmailId").addClass("form-error-box");    
        }
		else
		{
			$("#altEmailErrId").hide();
			$("#altEmailId").removeClass('form-error-box');
		}
	}
    
    if(regMobileNo=="")
	{
    	flag = 'false';
		$("#regMobileErrId").show();
        $("#regMobileErrId").html("Please enter your mobile no.");	
        $("#regMobileNo").addClass("form-error-box"); 
	}
    
    if(regMobileNo!="")
	{
		if(!regMobileNo.match(phNo))  
        {  		
			flag = 'false';
			$("#regMobileErrId").show();
	        $("#regMobileErrId").html("Please enter a valid mobile no.");	
	        $("#regMobileNo").addClass("form-error-box");     
        }
		else
		{
			$("#regMobileErrId").hide();
			$("#regMobileNo").removeClass('form-error-box');
		}
	}
    
    if(altMobileNo=="")
	{
    	flag = 'false';
		$("#altMobileErrId").show();
        $("#altMobileErrId").html("Please enter alternate mobile no.");	
        $("#altMobileNo").addClass("form-error-box");  
	}
    
    
    if(altMobileNo!="")
	{
		if(!altMobileNo.match(phNo))  
        {  			
			flag = 'false';
			$("#altMobileErrId").show();
	        $("#altMobileErrId").html("Please enter a valid mobile no.");	
	        $("#altMobileNo").addClass("form-error-box");    
        }
		else
		{
			$("#altMobileErrId").hide();
			$("#altMobileNo").removeClass('form-error-box');
		}
	}
    
    
    if(dobId=="")
	{
    	flag = 'false';
		$("#dobErrId").show();
        $("#dobErrId").html("Enter your date of birth.");	
        $("#dobId").addClass("form-error-box");     
	}
    
    if(dobId!="")
	{
		$("#dobErrId").hide();	
        $("#dobId").removeClass("form-error-box");     
	}
    
    
    if(idTypeId=="Select Type of Id")
	{
    	flag = 'false';
		$("#idTypeErrId").show();
        $("#idTypeErrId").html("Select Type of Identity.");	
        $("#idTypeId").addClass("form-error-box");     
	}
    
    if(idTypeId!="Select Type of Id")
	{
		$("#idTypeErrId").hide();	
        $("#idTypeId").removeClass("form-error-box");     
	}
    
    if(idNoId=="")
	{
    	flag = 'false';
		$("#idErrId").show();
        $("#idErrId").html("Enter your identity number.");	
        $("#idNoId").addClass("form-error-box");     
	}
    
    if(idNoId!="")
	{
		$("#idErrId").hide();	
        $("#idNoId").removeClass("form-error-box");     
	}
	
    
    
    if(preAddressId=="")
	{
    	flag = 'false';
		$("#preAddErrId").show();
        $("#preAddErrId").html("Enter your detail present address with pincode.");	
        $("#preAddressId").addClass("form-error-box");     
	}
    
    if(preAddressId!="")
	{
		$("#preAddErrId").hide();	
        $("#preAddressId").removeClass("form-error-box");     
	}
    
    if(perAddressId=="")
	{
    	flag = 'false';
		$("#perAddErrId").show();
        $("#perAddErrId").html("Enter your detail permanent address with pincode.");	
        $("#perAddressId").addClass("form-error-box");     
	}
    
    if(perAddressId!="")
	{
		$("#perAddErrId").hide();	
        $("#perAddressId").removeClass("form-error-box");     
	}
    
    
    
    if(income_type=="Select type of income")
	{
    	flag = 'false';
		$("#incomeErrId").show();
        $("#incomeErrId").html("Select type of income.");	
        $("#income_type").addClass("form-error-box");     
	}
    
    if(income_type!="Select type of income")
	{
		$("#incomeErrId").hide();	
        $("#income_type").removeClass("form-error-box");     
	}
    
    if(income_annualy=="Select annually earning")
	{
    	flag = 'false';
		$("#incomeAmtErrId").show();
        $("#incomeAmtErrId").html("Select type of income.");	
        $("#income_annualy").addClass("form-error-box");     
	}
    
    if(income_annualy!="Select annually earning")
	{
		$("#incomeAmtErrId").hide();	
        $("#income_annualy").removeClass("form-error-box");     
	}
    
    if(deal_amount=="Select interested amount")
	{
    	flag = 'false';
		$("#amountErrId").show();
        $("#amountErrId").html("Select interested amount.");	
        $("#deal_amount").addClass("form-error-box");     
	}
    
    if(deal_amount!="Select interested amount")
	{
		$("#amountErrId").hide();	
        $("#deal_amount").removeClass("form-error-box");     
	}
    
    if(deal_duration=="Select interested duration")
	{
    	flag = 'false';
		$("#timeErrId").show();
        $("#timeErrId").html("Select interested duration.");	
        $("#deal_duration").addClass("form-error-box");     
	}
    
    if(deal_duration!="Select interested duration")
	{
		$("#timeErrId").hide();	
        $("#deal_duration").removeClass("form-error-box");     
	}
   
    
    var docArray = requiredDoc.split("/");
   
    
	if(flag=='true')
	{   	    	
	   	 var formData = new FormData($("#financeApplicationForm")[0]);
		 
		/* for(var i=0;i<docArray.length;i++)
	     {
			 var docName = docArray[i].replace(/\s/g,'');
			 alert(docName);
			 //formData.append("file", docName.files[0]);
	     }
		 */
		$("#preloader").show();
   	 	$("#loader").show();
   	  
		$.ajax({  
		 			
			     type : "POST",   
			     data :formData,	  
			     url : "savePolicyForm",   	     	     
			     success : function(response) 
			     {  		
			    	 $("#preloader").hide();
			    	 $("#loader").hide();
			    	 
			    	 if(response=="success")
			      		{		
						 	document.getElementById("action").value = "url";
						 	document.getElementById("value").value = "loans";
						 	
						    $("#modal").show();
						    $("#modelBg").show();
						    
						    $("#heading").html("Success Message");
						    $("#contentMsg").html("Your Application have been successfully forwarded,Very soon we will notify to you.");							    
			      		}
			      	else
			      		{
				      		document.getElementById("action").value = "url";
						 	document.getElementById("value").value = "loans";
					 	
						    $("#modal").show();
						    $("#modelBg").show();
						    
						    $("#heading").html("Failed Message");
						    $("#contentMsg").html("There is some problem arise,Please try again later.");							    								    
			      		}				    	 
			     },
					cache: false,
			        contentType: false,
			        processData: false,  
		     }); 
			
	}
}

/////////////// END OF BORROWER SEARCH FINANCE AND APPLY //////////////////////////////




/////////////////   START SCRIPT FOR BORROWER APPLIED POLICY ///////////////////////////////////


function viewAppliedPolicy(dealId)
{
	 $("#preloader").show();
	 $("#loader").show();
	 
	$.ajax({  
		
	     type : "POST",   
	     data :"dealId="+dealId,	  
	     url : "viewAppliedPolicy",   	     	     
	     success : function(response) 
	     {  		
	    	 $("#preloader").hide();
	    	 $("#loader").hide();
	    	 
	    	 $("#dealInfo").html(response);
	    	 
	    	 $("#dealInfo").show();
	    	 
	    	 $('#dealInfo')[0].scrollIntoView(true);
	     },	
   }); 
}

function closeDealInfo()
{
	$("#dealInfo").slideUp();
}


/////////////////    END OF SCRIPT FOR BORROWER APPLIED POLICY   //////////////////////////



//////////////////  BORROWER NOTIFICATION //////////////////////////////////////


function showBorrowerNotification(notifyId,slNo) 
{
	$("#preloader").show();
	$("#loader").show();

	var buttonValue = $("#buttonId_" + slNo).html();

	if (buttonValue == "See") 
	{
		$.ajax({

			type : "post",
			url : "showBorrowerNotification",
			data : "notifyId="+notifyId,
			success : function(response) {

				$("#preloader").hide();
				$("#loader").hide();

				$("#buttonId_" + slNo).html("Close");

				$("#loanDetail_" + slNo).slideDown();
				$("#loanDetail_" + slNo).html(response);

			},
		});
	} 
	else 
	{
		$("#preloader").hide();
		$("#loader").hide();

		$("#buttonId_" + dealId).html("See");

		$("#loanDetail_" + dealId).slideUp();
		$("#loanDetail_" + dealId).html("");
	}	
}




///////////////////  END OF BORROWER NOTIFICATION   ///////////////////////////////////////



/////////////// START OF BORROWER SETTING SCRIPT //////////////////////////////

function copyToPermanent()
{
	var condition = document.getElementById("checkId");
	
	if(condition.checked)
	{
		var presentDoor = $("#preDoorId").val();
		var presentBuilding = $("#preBuildingId").val();
		var presentStreet = $("#preStreetId").val();
		var presentArea = $("#preAreaId").val();
		var presentCity = $("#preCityId").val();
		var presentPincode = $("#prePincodeId").val();
		
		
		$("#perDoorId").val(presentDoor);
		$("#perBuildingId").val(presentBuilding);
		$("#perStreetId").val(presentStreet);
		$("#perAreaId").val(presentArea);
		$("#perCityId").val(presentCity);
		$("#perPincodeId").val(presentPincode);
	}
	else
	{
		$("#perDoorId").val("");
		$("#perBuildingId").val("");
		$("#perStreetId").val("");
		$("#perAreaId").val("");
		$("#perCityId").val("");
		$("#perPincodeId").val("");
	}
	
}

function saveBorrowerIdnDetails()
{
	var flag = 'true';
		
	var alfanumeric = /^[a-zA-Z0-9]*$/;        	
	var nameWithOutSpace = /^[a-zA-Z]+$/;
	var nameWithSpace = /^[a-zA-Z\s]*$/;	
	var pincode = /^[0-9]{6}$/;	
	var poi= /^[a-zA-Z0-9]{6,20}$/;
	
	
	var presentDoor = $("#preDoorId").val();
	var presentBuilding = $("#preBuildingId").val();
	var presentStreet = $("#preStreetId").val();
	var presentArea = $("#preAreaId").val();
	var presentCity = $("#preCityId").val();
	var presentPincode = $("#prePincodeId").val();
	
	var permanentDoor = $("#perDoorId").val();
	var permanentBuilding = $("#perBuildingId").val();
	var permanentStreet = $("#perStreetId").val();
	var permanentArea = $("#perAreaId").val();
	var permanentCity = $("#perCityId").val();
	var permanentPincode = $("#perPincodeId").val();
	
	
    var dob = $("#dobId").val();
    var idType = $("#idTypeId").val();
    var idNo = $("#idNoId").val();
    
    
    
    if(presentDoor=="")
	{		
		flag = 'false';
		$("#preDoorId").addClass("form-error-box");
		$("#preDoorErrId").show();
        $("#preDoorErrId").html("Enter door detail.");	
        return false;              
	}
    
    if(presentDoor!="")
	{		
    	$("#preDoorId").removeClass("form-error-box");
		$("#preDoorErrId").hide();          
	}
    
    if(presentBuilding=="")
	{		
		flag = 'false';
		$("#preBuildingId").addClass("form-error-box");
		$("#preBuildingErrId").show();
        $("#preBuildingErrId").html("Enter building detail.");	
        return false;              
	}
    
    if(presentBuilding!="")
	{		
    	$("#preBuildingId").removeClass("form-error-box");
		$("#preBuildingErrId").hide();          
	}
    
    if(presentStreet=="")
	{		
		flag = 'false';
		$("#preStreetId").addClass("form-error-box");
		$("#preStreetErrId").show();
        $("#preStreetErrId").html("Enter street detail.");	
        return false;              
	}
    
    if(presentStreet!="")
	{		
    	$("#preStreetId").removeClass("form-error-box");
		$("#preStreetErrId").hide();          
	}
    
    if(presentArea=="")
	{		
		flag = 'false';
		$("#preAreaId").addClass("form-error-box");
		$("#preAreaErrId").show();
        $("#preAreaErrId").html("Enter area name.");	
        return false;              
	}
    
    if(presentArea!="")
	{		
    	$("#preAreaId").removeClass("form-error-box");
		$("#preAreaErrId").hide();          
	}
    
    if(presentCity=="")
	{		
		flag = 'false';
		$("#preCityId").addClass("form-error-box");
		$("#preCityErrId").show();
        $("#preCityErrId").html("Enter city name.");	
        return false;              
	}
    
    if(presentCity!="")
	{		
    	$("#preCityId").removeClass("form-error-box");
		$("#preCityErrId").hide();          
	}
    
    
    if(presentPincode=="")
	{		
		flag = 'false';
		$("#prePincodeId").addClass("form-error-box");
		$("#prePinErrId").show();
        $("#prePinErrId").html("Enter your present pincode.");	
        return false;              
	}
    
    if(presentPincode!="")
	{		
    	if(!presentPincode.match(pincode))
		{
    		flag = 'false';
    		$("#prePincodeId").addClass("form-error-box");
    		$("#prePinErrId").show();
            $("#prePinErrId").html("Enter a valid pincode.");	
            return false; 
		}
    	else
		{
    		$("#prePincodeId").removeClass("form-error-box");
    		$("#prePinErrId").hide();          
		}
	}
    
    
    
    
    
    
    
    
    if(permanentDoor=="")
	{		
		flag = 'false';
		$("#perDoorId").addClass("form-error-box");
		$("#perDoorErrId").show();
        $("#perDoorErrId").html("Enter door detail.");	
        return false;              
	}
    
    if(permanentDoor!="")
	{		
    	$("#perDoorId").removeClass("form-error-box");
		$("#perDoorErrId").hide();          
	}
    
    if(permanentBuilding=="")
	{		
		flag = 'false';
		$("#perBuildingId").addClass("form-error-box");
		$("#perBuildingErrId").show();
        $("#perBuildingErrId").html("Enter building detail.");	
        return false;              
	}
    
    if(permanentBuilding!="")
	{		
    	$("#perBuildingId").removeClass("form-error-box");
		$("#perBuildingErrId").hide();          
	}
    
    if(permanentStreet=="")
	{		
		flag = 'false';
		$("#perStreetId").addClass("form-error-box");
		$("#perStreetErrId").show();
        $("#perStreetErrId").html("Enter street name.");	
        return false;              
	}
    
    if(permanentStreet!="")
	{		
    	$("#perStreetId").removeClass("form-error-box");
		$("#perStreetErrId").hide();          
	}
    
    if(permanentArea=="")
	{		
		flag = 'false';
		$("#perAreaId").addClass("form-error-box");
		$("#perAreaErrId").show();
        $("#perAreaErrId").html("Enter area name.");	
        return false;              
	}
    
    if(permanentArea!="")
	{		
    	$("#perAreaId").removeClass("form-error-box");
		$("#perAreaErrId").hide();          
	}
    
    if(permanentCity=="")
	{		
		flag = 'false';
		$("#perCityId").addClass("form-error-box");
		$("#perCityErrId").show();
        $("#perCityErrId").html("Enter city name.");	
        return false;              
	}
    
    if(permanentCity!="")
	{		
    	$("#perCityId").removeClass("form-error-box");
		$("#perCityErrId").hide();          
	}
    
    
    if(permanentPincode=="")
	{		
		flag = 'false';
		$("#perPincodeId").addClass("form-error-box");
		$("#perPinErrId").show();
        $("#perPinErrId").html("Enter your permanent pincode.");	
        return false;              
	}
    
    if(permanentPincode!="")
	{		
    	if(!permanentPincode.match(pincode))
		{
    		flag = 'false';
    		$("#perPincodeId").addClass("form-error-box");
    		$("#perPinErrId").show();
            $("#perPinErrId").html("Enter a valid pincode.");	
            return false; 
		}
    	else
		{
    		$("#perPincodeId").removeClass("form-error-box");
    		$("#perPinErrId").hide();          
		}
	}
    
    
    if(dob=="")
	{		
		flag = 'false';
		$("#dobId").addClass("form-error-box");
		$("#dobErrId").show();
        $("#dobErrId").html("Please give us your date of birth.");	
        return false;              
	}
    
    if(idType=="Select Type of Id")
	{		
		flag = 'false';
		$("#idTypeId").addClass("form-error-box");
		$("#idTypeErrId").show();
        $("#idTypeErrId").html("Please select an id type.");	
        return false;              
	}
    
    if(idType!="Select Type of Id")
	{		
    	$("#idTypeId").removeClass("form-error-box");
		$("#idTypeErrId").hide();          
	}
    
    if(idNo=="")
	{		
		flag = 'false';
		$("#idNoId").addClass("form-error-box");
		$("#idErrId").show();
        $("#idErrId").html("Please enter your id as per respective the id type.");	
        return false;              
	}
    
    
    if(idNo!="")
	{		
    	if(!idNo.match(poi))
		{
    		flag = 'false';
    		$("#idNoId").addClass("form-error-box");
    		$("#idErrId").show();
            $("#idErrId").html("Please enter a valid id.");	
            return false; 
		}
    	else
		{
    		$("#idErrId").hide();
    		$("#idNoId").removeClass("form-error-box");
		}		             
	}
    
    
    
    if(flag=='true')
    	{   	
	    	$("#preloader").show();
	   	 	$("#loader").show();
	   	  
		   	var formData = new FormData($("#borrowerIdnForm")[0]);
				
			formData.append("file", file.files[0]);
			
			     $.ajax({  
			 			
				     type : "POST",   
				     data :formData,	  
				     url : "updateBorrowerIdentityDetails",   	     	     
				     success : function(response) 
				     {  		
				    	 $("#preloader").hide();
				    	 $("#loader").hide();
				    	 
				    	 if(response=="success")
				      		{		
							 	document.getElementById("action").value = "url";
							 	document.getElementById("value").value = "settings";
							 	
							    $("#modal").show();
							    $("#modelBg").show();
							    
							    $("#heading").html("Success Message");
							    $("#contentMsg").html("Your Identity information have been saved successfully.");							    
				      		}
				      	else
				      		{
					      		document.getElementById("action").value = "url";
							 	document.getElementById("value").value = "lenderSettings";
						 	
							    $("#modal").show();
							    $("#modelBg").show();
							    
							    $("#heading").html("Failed Message");
							    $("#contentMsg").html("There is some problem arise,Please try again later.");							    								    
				      		}				    	 
				     },
					cache: false,
			        contentType: false,
			        processData: false,  
			     }); 
    			
    	}
}


function changePassword()
{
	var flag = 'true';
	
    var password = $("#passwordId").val();
    var cnfPassword = $("#cnfPasswordId").val();
    
    
    
    if(password=="")
	{		
		flag = 'false';
		$("#passwordErrId").show();
        $("#passwordErrId").html("Please enter your password.");	
        return false;              
	}
    
    if(password!="")
	{		
		$("#passwordErrId").hide();          
	}
    
    
    if(cnfPassword=="")
	{		
		flag = 'false';
		$("#cnfPasswordErrId").show();
        $("#cnfPasswordErrId").html("Please confirm your password.");	
        return false;              
	}
    
    if(cnfPassword!="")
	{		
		$("#cnfPasswordErrId").hide();          
	}
    
    
    if(cnfPassword!=password)
	{		
    	flag = 'false';
		$("#cnfPasswordErrId").show();
        $("#cnfPasswordErrId").html("Please confirm your password it not matches with password.");	
        return false;     
	}
    
	if(flag=='true')
	{
	
	    	$("#preloader").show();
	   	 	$("#loader").show();
	   	  
    
		     $.ajax({  
		 			
			     type : "post",   
			     url : "changeBorrowerPassword", 
			     data :$('#passwordForm').serialize(),	     	     	     
			     success : function(response) 
			     {  		
			    	 $("#preloader").hide();
			    	 $("#loader").hide();
			    	 
					 if(response=="success")
						 {			
						 	 $("#successId").show();
						     $("#successMessage").html("Your password has been changed successfully.");								    
						 }
					 else
						 {
						     alert("Please Try again later!");
						     $("#successId").show();
						 }
					 
			     },  
		     }); 
			
	}
}





function editBorrowerBasic()
{
	window.location = "settings";	
}

function editBorrowerDoc()
{
	 window.location = "borrowerDoc";
}

function saveBorrowerDoc()
{
	var flag = 'true';
	
	var docType = $("#docType").val();
	var file = $("#file").val();
	
	if(docType=="Select document type")
	{
		flag = 'false';
		$("#typeErrId").show();
		$("#typeErrId").html("Select a document type.")
		
	}
	
	if(docType!="Select document type")
	{
		$("#typeErrId").hide();
	}
	
	if(file=="")
	{
		flag = 'false';
		$("#fileErrId").show();
		$("#fileErrId").html("Browse a file to upload.");	
	}
	
	if(file!="")
	{
		   var fileDetails = file;
		   var ext=fileDetails.substring(fileDetails.lastIndexOf('.')+1);
		   var extension = new Array("JPG","jpg","jpeg","JPEG","gif","GIF","png","PNG");	
		    		
		   var condition = "NotGranted";
		    
		   for(var m=0;m<extension.length;m++)
			{
			    if(ext==extension[m])
		    	{				    	    
		    	   condition="Granted";				    	    
		    	}				    
			}
		    
			if(condition=="NotGranted")
			{
			    flag = 'false';	
			    $("#fileErrId").show();
			    $("#fileErrId").html("only Image files are allowed.");
				return false;				  
			}
			
			 var fileDetails1 = document.getElementById("file");
			 var fileSize = fileDetails1.files[0];
			 var fileSizeinBytes = fileSize.size;
			 var sizeinKB = +fileSizeinBytes / 1024;
	  		 var sizeinMB = +sizeinKB / 1024;
	  		 
	  		 if(sizeinMB>1)
			 {
				$("#fileErrId").show();
				$("#fileErrId").html("choose a file less then 1 MB.");
				return false;	 		   		 
			 }  
	}
	
	if(flag=='true')
	{   	    	
	   	var formData = new FormData($("#borrowerDocForm")[0]);
		 
		$("#preloader").show();
   	 	$("#loader").show();
   	  
		$.ajax({  
		 			
			     type : "POST",   
			     data :formData,	  
			     url : "saveBorrowerDoc",   	     	     
			     success : function(response) 
			     {  		
			    	 $("#preloader").hide();
			    	 $("#loader").hide();
			    	 
			    	 if(response=="success")
			      		{		
						 	document.getElementById("action").value = "url";
						 	document.getElementById("value").value = "myProfile";
						 	
						    $("#modal").show();
						    $("#modelBg").show();
						    
						    $("#heading").html("Success Message");
						    $("#contentMsg").html("Your documents have been successfully uploaded.");							    
			      		}
			      	else
			      		{
				      		document.getElementById("action").value = "url";
						 	document.getElementById("value").value = "myProfile";
					 	
						    $("#modal").show();
						    $("#modelBg").show();
						    
						    $("#heading").html("Failed Message");
						    $("#contentMsg").html("There is some problem arise,Please try again later.");							    								    
			      		}				    	 
			     },
				 cache: false,
		         contentType: false,
		         processData: false,  
		     }); 
			
	}
	
}

function viewScannedDoc(documentId)
{
	$("#preloader").show();
	$("#loader").show();
	  

     $.ajax({  
 			
	     type : "post",   
	     url : "viewScannedDoc", 
	     data :"documentId="+documentId,	     	     	     
	     success : function(response) 
	     {  		
	    	 $("#preloader").hide();
	    	 $("#loader").hide();
	    	 
	    	 $("#viewAttachedDocument").show();
	    	 
	    	 
	    	 $("#viewAttachedDocument").html(response);
	    	 
	     },  
     }); 
}

function closeDocPopUp()
{
	$("#viewAttachedDocument").hide();
}

function redirectLink(url)
{
	window.location = url;
}





//  Script for ----  Add Borrower Document in pop up div during apply loan form


function attachDocument(docType)
{
	$("#preloader").show();
	$("#loader").show();
	 
	 
	$.ajax({  
			
	     type : "POST",   
	     data :"docType="+docType,	  
	     url : "viewDocFormPopUp",   	     	     
	     success : function(response) 
	     {  		
	    	 $("#preloader").hide();
	    	 $("#loader").hide();
	    	
	    	 $("#popContentId").html(response);
	    	 
	    	 $("#popContentId").show();
	    	 
	     },
    }); 
}

function closePopUpForm()
{
	$("#popContentId").hide();
}

function addBorrowerDoc()
{
	var flag = 'true';
	
	var file = $("#file").val();
	
	
	if(file=="")
	{
		flag = 'false';
		$("#fileErrId").show();
		$("#fileErrId").html("Browse a file to upload.");	
	}
	
	if(file!="")
	{
		   var fileDetails = file;
		   var ext=fileDetails.substring(fileDetails.lastIndexOf('.')+1);
		   var extension = new Array("JPG","jpg","jpeg","JPEG","gif","GIF","png","PNG");	
		    		
		   var condition = "NotGranted";
		    
		   for(var m=0;m<extension.length;m++)
			{
			    if(ext==extension[m])
		    	{				    	    
		    	   condition="Granted";				    	    
		    	}				    
			}
		    
			if(condition=="NotGranted")
			{
			    flag = 'false';	
			    $("#fileErrId").show();
			    $("#fileErrId").html("only Image files are allowed.");
				return false;				  
			}
			
			 var fileDetails1 = document.getElementById("file");
			 var fileSize = fileDetails1.files[0];
			 var fileSizeinBytes = fileSize.size;
			 var sizeinKB = +fileSizeinBytes / 1024;
	  		 var sizeinMB = +sizeinKB / 1024;
	  		 
	  		 if(sizeinMB>1)
			 {
				$("#fileErrId").show();
				$("#fileErrId").html("choose a file less then 1 MB.");
				return false;	 		   		 
			 }  
	}
	
	if(flag=='true')
	{   	    	
	   	var formData = new FormData($("#borrowerDocForm")[0]);
		 
		$("#preloader").show();
   	 	$("#loader").show();
   	  
		$.ajax({  
		 			
			     type : "POST",   
			     data :formData,	  
			     url : "saveBorrowerDoc",   	     	     
			     success : function(response) 
			     {  		
			    	 $("#preloader").hide();
			    	 $("#loader").hide();
			    	 
			    	 if(response=="success")
			      		{							
			    		 	closePopUpForm();
			    		    refreshAvailableDocForm();
			    		    
			      		}
			      	else
			      		{	
			      			closePopUpForm();		      		
			      		    refreshAvailableDocForm();						    								    
			      		}				    	 
			     },
				 cache: false,
		         contentType: false,
		         processData: false,  
		     }); 
			
	}
	
}

function refreshAvailableDocForm()
{
	$("#preloader").show();
	$("#loader").show();
	  
	$.ajax({  
	 			
		     type : "POST",   	  
		     url : "refreshAvailableDocForm",   	     	     
		     success : function(response) 
		     {  		
		    	 $("#preloader").hide();
		    	 $("#loader").hide();
		    	 
		    	 $("#borrowerAvailDocId").html(response);			    	 
		     },
			  
	     }); 
}

function viewAddedDoc(docType)
{
	$("#preloader").show();
	$("#loader").show();
	  

     $.ajax({  
 			
	     type : "post",   
	     url : "viewAddedDoc", 
	     data :"docType="+docType,	     	     	     
	     success : function(response) 
	     {  		
	    	 $("#preloader").hide();
	    	 $("#loader").hide();
	    	 
	    	 $("#viewAttachedDocument").show();
	    	 
	    	 
	    	 $("#viewAttachedDocument").html(response);
	    	 
	     },  
     }); 
}


//  ENd of Script for ----  Add Borrower Document in pop up div during apply loan form
