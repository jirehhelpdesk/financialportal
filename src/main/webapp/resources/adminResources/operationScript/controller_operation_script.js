

function searchBorrowerRequestPolicy()
{	
	var type = $("#searchTypeId").val();	
	var value = $("#searchValueId").val();
	var loanStatusDiv = $("#loanStatusDiv").val();
	
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "searchBorrowerRequestPolicy",
	     data : "searchType="+type+"&searchValue="+value+"&loanStatusDiv="+loanStatusDiv,
	     success : function(response) 
	     {    	 
	    	 	$("#message-box-waiting").hide();
	    	 
	    	 	$("#headingNote").html("Search Result of Borrower applied finance policy to lender");
	    	 	
	    	 	$("#policySearchResultId").html(response);	
	      		
	    	 	$("#hiddenDiv").slideDown();
	    	 	$("#hiddenDiv").show();
		 			 
		 		$('#hiddenDiv')[0].scrollIntoView(true);
	     },  
    }); 
	
	
}


function manageBorrowerRequest(dealId)
{
	
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "manageBorrowerRequest",
	     data : "dealId="+dealId,
	     success : function(response) 
	     {    	 
	    	 	$("#message-box-waiting").hide();
	    	 
	    	 	$("#mangeDiv").html(response);	
	    	 	
	    	 	$("#mangeDiv").slideDown();
	    	 	$("#mangeDiv").show();
	    	 	
		 		$('#mangeDiv')[0].scrollIntoView(true);
	     },  
    }); 
}


function viewUserProfile(userId,userName,userType)
{
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "viewUserProfile",
	     data : "userId="+userId+"&userType="+userType,
	     success : function(response) 
	     {    	 
    	 	$("#message-box-waiting").hide();
    	 	
    	 	$("#profileHeadId").html(userName+"'s Profile");
    	 	
    	 	$("#userViewProfile").html(response);	
    	 	
      		$("#profileHiddenDiv").slideDown();
      		$("#profileHiddenDiv").show();
      		
	 		$('#profileHiddenDiv')[0].scrollIntoView(true);
	 		
	     },  
    });
}


function viewUserDocument(fileName,userId,userType)
{
	$("#iconPreview").show();
	$("#docBack").show();
}

function downloadUserDocument(fileName,userId,userType)
{
	window.location = "downloadDocuments?fileName="+fileName+"&userType="+userType+"&userId="+userId;
}


///////////////////////////////////////      BORROWER  MAINTAINENCE SCRIPT   ///////////////////////////////////////////


function searchBorrowerByController()
{
	
	var type = $("#searchTypeId").val()	
	var value = $("#searchValueId").val()
	
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "searchBorrowerByController",
	     data : "searchType="+type+"&searchValue="+value,
	     success : function(response) 
	     {    	 
	    	 	$("#message-box-waiting").hide();
	    	 
	    	 	$("#borrowerSearchResultId").html(response);		
	      		
	    	 	$("#hiddenDiv").slideDown();
	    	 	$("#hiddenDiv").show();
	      		
		 		$('#hiddenDiv')[0].scrollIntoView(true);
		 		
		 		$('#exportDataId').val(type+"-"+value);
		 		
	     },  
    }); 
}


function manageBorrowerStatus(borrowerId,condition)
{
	
	if (confirm('Are you sure you want to '+condition+' the user ?')) 
	{
	    
		$("#message-box-waiting").show();
		
		$.ajax({  
			
		     type : "post",   
		     url : "manageBorrowerStatus",
		     data : "borrowerId="+borrowerId+"&condition="+condition,
		     success : function(response) 
		     {    	 
		    	 	$("#message-box-waiting").hide();
		    	    
		    	 	$("#closeLinkId").val('borrowerManage');      			
	    		 	$("#conditionId").val('Redirect');
	    		 	
			 		if(condition=="Block")
			 		{
			 			$("#messageId").html("Borrower profile have been successfully deactivated.");
			 		}
			 		else
			 		{
			 			$("#messageId").html("Borrower profile have been successfully activated.");
			 		}	
			 		
			 		$("#message-box-success").show();		 		
		     },  
	    }); 
		
	} 
	else 
	{
	    alert("Its ok mistake happens.");
	}
	
	
	
}


function exportBorrowerData()
{	
	var parameter = $('#exportDataId').val();
	
	if(parameter=="default")
	{
		window.location = "borrowerExportData?parameter="+parameter+"&searchType=No&searchValue=No";
	}
	else
	{
		var values = parameter.split("-");
		
		window.location = "borrowerExportData?parameter="+parameter+"&searchType="+values[0]+"&searchValue="+values[1];
	}
	
}

//////////////////////////////////////////////    END OF BORROWER MAINTAINENCE SCRIPT    ?/////////////////////////////////////////




/////////////////////////////////////////////     LENDER MANAGE SCRIPT   ///////////////////////////////////////////////// 


function exportLenderData()
{	
	var parameter = $('#exportDataId').val();
	
	if(parameter=="default")
	{
		window.location = "lenderExportData?parameter="+parameter+"&searchType=No&searchValue=No";
	}
	else
	{
		var values = parameter.split("-");
		
		window.location = "lenderExportData?parameter="+parameter+"&searchType="+values[0]+"&searchValue="+values[1];
	}
	
}

function manageLenderStatus(lenderId,condition)
{
	if (confirm('Are you sure you want to '+condition+' the user ?')) 
	{
		$("#message-box-waiting").show();
		
		$.ajax({  
			
		     type : "post",   
		     url : "manageLenderStatus",
		     data : "lenderId="+lenderId+"&condition="+condition,
		     success : function(response) 
		     {    	 
		    	 	$("#message-box-waiting").hide();
		    	    
		    	 	$("#closeLinkId").val('lenderRequest');      			
	    		 	$("#conditionId").val('Redirect');
	    		 	
			 		if(condition=="Block")
			 		{
			 			$("#messageId").html("Lender profile have been successfully deactivated.");
			 		}
			 		else
			 		{
			 			$("#messageId").html("Lender profile have been successfully activated.");
			 		}	
			 		
			 		$("#message-box-success").show();		 		
		     },  
	    }); 
	}
	else
	{
		 alert("Its ok mistake happens.");
	}
	
}



function createLenderByCntrl()
{
	var flag = 'true';
	
	var lenderTypeId = $("#lenderTypeId").val();
	var lenderNameId = $("#lenderNameId").val();
	
	var firstRepreNameId = $("#firstRepreNameId").val();
	var firstRepreEmailId = $("#firstRepreEmailId").val();
	var firstRepreMobileId = $("#firstRepreMobileId").val();
	
	var secondRepreNameId = $("#secondRepreNameId").val();
	var secondRepreEmailId = $("#secondRepreEmailId").val(); 
	var secondRepreMobileId = $("#secondRepreMobileId").val();
	
	if(lenderTypeId=="Select type of lender")
	{
		flag = 'false';
		$("#errorTypeId").html("Select type of lender");
		$("#errorTypeId").show();
	}
	if(lenderTypeId!="Select type of lender")
	{
		$("#errorTypeId").hide();
	}
	
	if(lenderNameId=="")
	{
		flag = 'false';
		$("#errorNameId").html("Enter lender name");
		$("#errorNameId").show();
	}
	if(lenderNameId!="")
	{
		$("#errorNameId").hide();
	}
	
	if(firstRepreNameId=="")
	{
		flag = 'false';
		$("#error1stRepreNameId").html("Enter primary representative name");
		$("#error1stRepreNameId").show();
	}
	if(firstRepreNameId!="")
	{
		$("#error1stRepreNameId").hide();
	}
	
	if(firstRepreEmailId=="")
	{
		flag = 'false';
		$("#error1stRepreEmailId").html("Enter primary representative email id");
		$("#error1stRepreEmailId").show();
	}
	if(firstRepreEmailId!="")
	{
		$("#error1stRepreEmailId").hide();
	}
	
	if(firstRepreMobileId=="")
	{
		flag = 'false';
		$("#error1stRepreMobileId").html("Enter primary representative mobile no");
		$("#error1stRepreMobileId").show();
	}
	if(firstRepreMobileId!="")
	{
		$("#error1stRepreMobileId").hide();
	}
	
	
	if(secondRepreNameId=="")
	{
		flag = 'false';
		$("#error2ndRepreNameId").html("Enter second representative name");
		$("#error2ndRepreNameId").show();
	}
	if(secondRepreNameId!="")
	{
		$("#error2ndRepreNameId").hide();
	}
	
	if(secondRepreEmailId=="")
	{
		flag = 'false';
		$("#error2ndRepreEmailId").html("Enter second representative email id");
		$("#error2ndRepreEmailId").show();
	}
	if(secondRepreEmailId!="")
	{
		$("#error2ndRepreEmailId").hide();
	}
	
	if(secondRepreMobileId=="")
	{
		flag = 'false';
		$("#error2ndRepreMobileId").html("Enter second representative mobile no");
		$("#error2ndRepreMobileId").show();
	}
	if(secondRepreMobileId!="")
	{
		$("#error2ndRepreMobileId").hide();
	}
	
	
	if(flag=='true')
	{
		 $("#message-box-waiting").show();
		
		 var formData = new FormData($("#lenderRegForm")[0]);
		
		     $.ajax({  
		 			
			     type : "post",   
			     url : "createLenderByCntrl", 
			     data :formData,	     	     	     
			     success : function(response) 
			     {  		
			    	 $("#message-box-waiting").hide();
			    	 
			    	 if(response=="success")
			      		{			
			      			$("#closeLinkId").val('lenderCreate');
			      			
			    		 	$("#conditionId").val('Redirect');
			    		 	
					 		$("#message-box-success").show();
					 		$("#messageId").html("Lender registration done successfully.");
			      		}
			      	else
			      		{
			      			$("#closeLinkId").val('lenderCreate');
			      		
			      			$("#conditionId").val('Redirect');
			      			
				      		$("#message-box-danger").show();
					 		$("#messageId").html("Some problem arise please try again later.");		 					 		
			      		}			 		
					 
			     },  
			     cache: false,
			     contentType: false,
			     processData: false,
		     });
	
	}
	
	     
}


function searchLenderByController()
{
	var type = $("#searchTypeId").val()	
	var value = $("#searchValueId").val()
	
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "searchLenderByController",
	     data : "searchType="+type+"&searchValue="+value,
	     success : function(response) 
	     {    	 
	    	 	$("#message-box-waiting").hide();
	    	 
	    	 	$("#lenderSearchResultId").html(response);		
	    	 	
	    	 	$("#hiddenDiv").slideDown();
	    	 	$("#hiddenDiv").show();
		 		
		 		$('#hiddenDiv')[0].scrollIntoView(true);
		 		
		 		$('#exportDataId').val(type+"-"+value);
		 		
	     },  
    }); 
}



////////////////////////////////////////////////////////////         END OF LENDER MANAGE SCRIPT    //////////////////////////////////////////
function updateDealStatus(dealId)
{
	var cntrlStatus = $("#cntrlStatus").val();	
	var dealStatus = $("#dealStatus").val();
	var visStatus = $("#visStatus").val();
	
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "updateDealStatusByController",
	     data : "dealId="+dealId+"&cntrlStatus="+cntrlStatus+"&dealStatus="+dealStatus+"&visStatus="+visStatus,
	     success : function(response) 
	     {    	 
	    	 $("#message-box-waiting").hide();
	    	 
	    	 if(response=="success")
	      		{			
	      			$("#closeLinkId").val('borrowerRequest');
	      			
	    		 	$("#conditionId").val('NoRedirect');
	    		 	
			 		$("#message-box-success").show();
			 		$("#messageId").html("Deal Details updated successfully.");
	      		}
	      	else
	      		{
	      			$("#closeLinkId").val('borrowerRequest');
	      		
	      			$("#conditionId").val('NoRedirect');
	      			
		      		$("#message-box-danger").show();
			 		$("#messageId").html("Some problem arise please try again later.");		 					 		
	      		}
		 		
	     },  
    }); 
}


function showBoxToNotify(borrowerId)
{
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "showBoxToNotify",
	     data : "borrowerId="+borrowerId,
	     success : function(response) 
	     {    	 
	    	 	$("#message-box-waiting").hide();
	    	 
	    	 	$("#composeMsgDiv").html(response);	
	    	 	
	      		$("#composeDiv").slideDown();
	      		$("#composeDiv").show();
		 		
		 		$('#composeDiv')[0].scrollIntoView(true);
	     },  
    }); 
}


function showNotifyForLender(lenderId)
{
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "showNotifyForLender",
	     data : "lenderId="+lenderId,
	     success : function(response) 
	     {    	 
	    	 	$("#message-box-waiting").hide();
	    	 
	    	 	$("#composeMsgDiv").html(response);		 
	    	 	
	      		$("#composeDiv").slideDown();
	      		$("#composeDiv").show();
		 		
		 		$('#composeDiv')[0].scrollIntoView(true);
	     },  
    }); 
}


function sendMessageToBorrower()
{
	var flag = 'true';
	
	
	if(flag=='true')
	{
			
		$("#message-box-waiting").show();
		
		 var formData = new FormData($("#composeMessageForm")[0]);
		
		  $.ajax({
					type : "POST",
					data : formData,
					url : "sendMessageToBorrower",								
					success : function(response) {
			            
						$("#message-box-waiting").hide();
						
						 if(response=="success")
				      		{			
				      			$("#closeLinkId").val('notifyBorrower');
				      			
				      			$("#conditionId").val('Redirect');
				      			
						 		$("#message-box-success").show();
						 		$("#messageId").html("Your Message sent to borrower successfully.");
				      		}
				      	else
				      		{
				      			$("#closeLinkId").val('notifyBorrower');
				      		
				      			$("#conditionId").val('Redirect');
				      			
					      		$("#message-box-danger").show();
						 		$("#messageId").html("Operation was stoped due to some problem.Please try again later.");		 					 		
				      		}
					},
					cache: false,
			        contentType: false,
			        processData: false,
			});
		   	
	}
}

function sendMessageToLender()
{
    var flag = 'true';
	
	
	if(flag=='true')
	{
			
		$("#message-box-waiting").show();
		
		 var formData = new FormData($("#composeMessageForm")[0]);
		
		  $.ajax({
					type : "POST",
					data : formData,
					url : "sendMessageToLender",								
					success : function(response) {
			            
						$("#message-box-waiting").hide();
						
						 if(response=="success")
				      		{			
				      			$("#closeLinkId").val('notifyLender');
				      			
				      			$("#conditionId").val('Redirect');
				      			
						 		$("#message-box-success").show();
						 		$("#messageId").html("Your Message sent to Lender successfully.");
				      		}
				      	else
				      		{
				      			$("#closeLinkId").val('notifyLender');
				      		
				      			$("#conditionId").val('Redirect');
				      			
					      		$("#message-box-danger").show();
						 		$("#messageId").html("Operation was stoped due to some problem.Please try again later.");		 					 		
				      		}
					},
					cache: false,
			        contentType: false,
			        processData: false,
			});
		   	
	}
}

function searchLender()
{
	var type = $("#searchTypeId").val()	
	var value = $("#searchValueId").val()
	
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "searchLenderByController",
	     data : "searchType="+type+"&searchValue="+value,
	     success : function(response) 
	     {    	 
	    	 	$("#message-box-waiting").hide();
	    	 
	    	 	$("#lenderSearchResultId").html(response);		 
	      		
	    	 	$("#hiddenDiv").slideDown();
	    	 	$("#hiddenDiv").show();
		 		
		 		$('#hiddenDiv')[0].scrollIntoView(true);
	     },  
    }); 
}




function searchLenderRequestPolicy()
{	
	var type = $("#searchTypeId").val()	
	var value = $("#searchValueId").val()
	
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "searchLenderRequestPolicy",
	     data : "searchType="+type+"&searchValue="+value,
	     success : function(response) 
	     {    	 
	    	 	$("#message-box-waiting").hide();
	    	 
	    	 	$("#headingNote").html("Search Result of Borrower applied finance policy to lender");
	    	 	
	    	 	$("#policySearchResultId").html(response);	
	      		
	    	 	$("#hiddenDiv").slideDown();
	    	 	$("#hiddenDiv").show();
		 		
		 		$('#hiddenDiv')[0].scrollIntoView(true);
	     },  
    }); 
	
	
}


function showLoanStatus(value)
{
	if(value=="Loan Status")
	{
		$("#showStatusDivId").slideDown();
		$("#searchValueDivId").slideUp();
	}
	else if(value=="Name")
	{
		$("#showStatusDivId").slideUp();
		$("#searchValueDivId").slideDown();
	}
	else if(value=="Email")
	{
		$("#showStatusDivId").slideUp();
		$("#searchValueDivId").slideDown();
	}
	else
	{
		$("#showStatusDivId").slideUp();
		$("#searchValueDivId").slideDown();
	}
}



////////////////  CONTROLLER SETTING   ///////////////////////////////////////////////////



function saveControllerPassword()
{
	var flag = 'true';
	
	var curPassword = $("#curPasswordId").val();
	var newPassword = $("#newPasswordId").val();
	var cnfPassword = $("#cnfPasswordId").val();
	
	if(curPassword=="")
	{
		flag = 'false';
		$("#curErrorId").show();
		$("#curErrorId").html("Enter current password.");
		return false;
	}
	
	if(curPassword!="")
	{
		$("#curErrorId").hide();
	}
	
	if(newPassword=="")
	{
		flag = 'false';
		$("#newErrorId").show();
		$("#newErrorId").html("Enter new password.");
		return false;
	}
	
	if(newPassword!="")
	{
		$("#newErrorId").hide();
	}
	
	if(cnfPassword=="")
	{
		flag = 'false';
		$("#cnfErrorId").show();
		$("#cnfErrorId").html("Confirm new password.");
		return false;
	}
	
	if(cnfPassword!="")
	{
		$("#cnfErrorId").hide();
	}
	
	if(cnfPassword!=newPassword)
	{
		flag = 'false';
		$("#cnfErrorId").show();
		$("#cnfErrorId").html("Not matched with new password.");
		return false;
	}
	
	if(flag=='true')
	{
		$("#message-box-waiting").show();
		
		$.ajax({  
			
		     type : "post",   
		     url : "changeControllerPassword",
		     data : "curPassword="+curPassword+"&newPassword="+newPassword,
		     success : function(response) 
		     {    	 
		    	 $("#message-box-waiting").hide();
		    	 
		    	 
		    	 if(response=="success")
		      		{			
		      			$("#closeLinkId").val('controllerSetting');
		      			
		      			$("#conditionId").val('Redirect');
		      			
				 		$("#message-box-success").show();
				 		$("#messageId").html("Your password changed Successfully.");
		      		}
		    	else if(response=="NotMatched")
		      		{			
			    		$("#curErrorId").show();
			    		$("#curErrorId").html("Given current password was not correct.");
		      		}
		      	else
		      		{
		      			$("#closeLinkId").val('controllerSetting');
		      		
		      			$("#conditionId").val('Redirect');
		      			
			      		$("#message-box-danger").show();
				 		$("#messageId").html("Operation was stoped due to some problem.Please try again later.");		 					 		
		      		}
		    	 
		     },  
	    }); 
	}
}



function saveControllerProfile()
{
	var flag = 'true';
	
	var name = $("#nameId").val();
	var role = $("#roleId").val();
	var photo = $("#file").val();
	
	if(name=="")
	{
		flag = 'false';
		$("#nameErrorId").show();
		$("#nameErrorId").html("Please Enter the name.");
		return false;
	}
	
	if(name!="")
	{
		$("#nameErrorId").hide();
	}
	
	
	if(role=="")
	{
		flag = 'false';
		$("#roleErrorId").show();
		$("#roleErrorId").html("Please Enter the role.");
		return false;
	}
	
	if(role!="")
	{
		$("#roleErrorId").hide();
	}

	
	
	if(photo!="")
	{		
	   var fileDetails = photo;
	   $("#otherErrorId").hide();
	    var ext=fileDetails.substring(fileDetails.lastIndexOf('.')+1);
	    var extension = new Array("JPG","jpg","jpeg","JPEG","gif","GIF","png","PNG","pdf","PDF","doc","DOC","docx","DOCX");	
	    		
	    var condition = "NotGranted";
	    for(var m=0;m<extension.length;m++)
			{
			    if(ext==extension[m])
			    	{				    	    
			    	   condition="Granted";				    	    
			    	}				    
			}
		if(condition=="NotGranted")
			{
			    flag = 'false';	
			    $("#fileErrorId").show();
			    $("#fileErrorId").html("Image and document files are allowed.");
				return false;				  
			}
		
		 var fileDetails1 = document.getElementById("file");
		 var fileSize = fileDetails1.files[0];
		 var fileSizeinBytes = fileSize.size;
		 var sizeinKB = +fileSizeinBytes / 1024;
  		 var sizeinMB = +sizeinKB / 1024;
  		 
  		 if(sizeinMB>2)
  			 {
  				flag = 'false';
  				$("#fileErrorId").show();
  				$("#fileErrorId").html("choose a file less then 2 MB.");
				return false;	 		   		 
  			 }  		 
	}
    
    		    
		if(flag=='true')
		{
				
			$("#message-box-waiting").show();
			
			 var formData = new FormData($("#otherDetailsForm")[0]);
			
			 formData.append("file", file.files[0]);
			
			  $.ajax({
						type : "POST",
						data : formData,
						url : "saveControllerDetail",								
						success : function(response) {
				            
							$("#message-box-waiting").hide();
							
							 if(response=="success")
					      		{			
					      			$("#closeLinkId").val('controllerSetting');
					      			
					      			$("#conditionId").val('Redirect');
					      			
							 		$("#message-box-success").show();
							 		$("#messageId").html("Your details saved Successfully.");
					      		}
					      	else
					      		{
					      			$("#closeLinkId").val('controllerSetting');
					      		
					      			$("#conditionId").val('Redirect');
					      			
						      		$("#message-box-danger").show();
							 		$("#messageId").html("Operation was stoped due to some problem.Please try again later.");		 					 		
					      		}
						},
						cache: false,
				        contentType: false,
				        processData: false,
				});
			   	
		}

}


