

function closeLink()
{
	$("#message-box-waiting").show();
	 
	var url = $("#closeLinkId").val();
	
	var condtition = $("#conditionId").val();
	
	if(condtition=="NoRedirect")
		{
			$("#message-box-waiting").hide();
				
  			$("#conditionId").val('');
		}
	else
		{
			window.location = url;	
			$("#message-box-waiting").hide();
		}
	
}



////////////////////////////////////   START SCRIPT FOR BORROWER MANAGEMENT  //////////////////////////////////////////////////////////



function searchBorrower()
{	
	var type = $("#searchTypeId").val();	
	var value = $("#searchValueId").val();
	
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "searchBorrower",
	     data : "searchType="+type+"&searchValue="+value,
	     success : function(response) 
	     {    	 
    	 	$("#message-box-waiting").hide();
    	 
      		$("#hiddenDiv").slideDown();
	 		$("#borrowerSearchResultId").html(response);		 		
	     },  
    }); 
}

function viewBorrowerProfile(borrowerId,borrowerName)
{
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "viewBorrowerProfile",
	     data : "borrowerId="+borrowerId,
	     success : function(response) 
	     {    	 
    	 	$("#message-box-waiting").hide();
    	 	
    	 	$("#profileHeadId").html(borrowerName+"'s Profile");
    	 	
      		$("#profileHiddenDiv").slideDown();
	 		$("#borrowerViewProfile").html(response);		 		
	     },  
    });
}

function updateBorrowerAccess(borrowerId,status)
{
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "updateBorrowerAccess",
	     data : "borrowerId="+borrowerId+"&status="+status,
	     success : function(response) 
	     {    	 
    	 	$("#message-box-waiting").hide();
    	 	
    	 	if(response=="success")
	 		{
    	 		$("#closeLinkId").val('borrowerInfo');
      			
      			$("#conditionId").val('NoRedirect');
      			
		 		$("#message-box-success").show();
		 		$("#messageId").html("Borrower Status has been successfully updated.");
		 		
    	 		searchBorrower();    
	 		}
    	 	else
    	 	{
    	 		$("#closeLinkId").val('borrowerInfo');
	      		
      			$("#conditionId").val('NoRedirect');
      			
	      		$("#message-box-danger").show();
		 		$("#messageId").html("There might be a problem so please try again later.");
    	 	}
    	 		 			 		
	     },  
    });
}


//////////////////////   END OF SCRIPT FOR BORROWER MANAGEMENT       ///////////////////////////////////////


/////////////////////  START SCRIPT FOR LENDER MANAGEMENT ///////////////////////////////////////////

function searchLenderByAdmin()
{
	var type = $("#searchTypeId").val()	
	var value = $("#searchValueId").val()
	
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "searchLenderByAdmin",
	     data : "searchType="+type+"&searchValue="+value,
	     success : function(response) 
	     {    	 
	    	 	$("#message-box-waiting").hide();
	    	 
	    	 	$("#hiddenDiv").slideDown();
		 		$("#lenderSearchResultId").html(response);		 		
	     },  
    }); 
}


function viewLenderProfile(lenderId,lenderName)
{
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "viewLenderProfile",
	     data : "lenderId="+lenderId,
	     success : function(response) 
	     {    	 
    	 	$("#message-box-waiting").hide();
    	 	
    	 	$("#profileHeadId").html(lenderName+"'s Profile");
    	 	
      		$("#profileHiddenDiv").slideDown();
	 		$("#lenderViewProfile").html(response);		 		
	     },  
    });
	
}

function updateLenderAccess(lenderId,status)
{
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "updateLenderAccess",
	     data : "lenderId="+lenderId+"&status="+status,
	     success : function(response) 
	     {    	 
    	 	$("#message-box-waiting").hide();
    	 	
    	 	if(response=="success")
	 		{
    	 		$("#closeLinkId").val('lenderInfo');
      			
      			$("#conditionId").val('NoRedirect');
      			
		 		$("#message-box-success").show();
		 		$("#messageId").html("Lender Status has been successfully updated.");
		 		
		 		searchLenderByAdmin();    
	 		}
    	 	else
    	 	{
    	 		$("#closeLinkId").val('lenderInfo');
	      		
      			$("#conditionId").val('NoRedirect');
      			
	      		$("#message-box-danger").show();
		 		$("#messageId").html("There might be a problem so please try again later.");
    	 	}
    	 		 			 		
	     },  
    });
}












////////////////////////////////    END OF SCRIPT OF LENDER MANAGEMENT    ///////////////////////////////////////////////////////////










//  SCRIPT FUTION RELATED TO CONTROLLER //////////////////////////////////////


function saveControllerDetails()
{
    var flag = 'true';
  
    var alfanumeric = /^[a-zA-Z0-9]*$/;        	
	var nameWithOutSpace = /^[a-zA-Z]+$/;
	var nameWithSpace = /^[a-zA-Z_.\s]*$/;	
	var pincode = /^[0-9]{6}$/;	
	var poi= /^[a-zA-Z0-9]{6,20}$/;		
	var aadhar= /^[0-9]{6,20}$/;		
	var phNo = /^[0-9]{10}$/;	
	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	
	
	var name = $("#nameId").val();
	var emailId = $("#emailId").val();
	var mobile = $("#mobileId").val();
	var password = $("#passwordId").val();
	
	
	if(name=="")
	{
	   flag = 'false';
	   $("#errorNameId").html("Please enter controller name.");
	   return false;
	}
	
	if(name!="")
	{
		if(!name.match(nameWithSpace))  
        {  		
			flag = 'false';
			$("#errorNameId").show();
			$("#errorNameId").html("Only alphabets are allowed.");	
	        return false;       
        }
		else
		{
			$("#errorNameId").hide();
		}  
	}
	
	if(emailId=="")
	{
	   flag = 'false';
	   $("#errorEmailId").html("Please enter email id.");
	   return false;
	}
	
	if(emailId!="")
	{
		if(!emailId.match(email))  
        {  		
			flag = 'false';
			$("#errorEmailId").show();
			$("#errorEmailId").html("Enter a valid email id.");	
	        return false;       
        }
		else
		{
			$("#errorEmailId").hide();
		}
	}
	
	/*if(phNo=="")
	{
	   flag = 'false';
	   $("#errorMobileId").html("Please enter mobile number.");
	   return false;
	}*/
	
	if(mobile!="")
	{
		if(!mobile.match(phNo))  
        {  		
			flag = 'false';
			$("#errorMobileId").show();
			$("#errorMobileId").html("Enter a valid mobile number.");	
	        return false;       
        }
		else
		{
			$("#errorMobileId").hide();
		}  
	}
	
	
	if(flag=='true')
		{
		
		       $("#message-box-waiting").show();
		
				$.ajax({  
					
				     type : "post",   
				     url : "saveControllerDetails",
				     data : $('#createControllerForm').serialize(),
				     success : function(response) 
				     {			
				    	 
				    	$("#message-box-waiting").hide();
				    	
				      	if(response=="success")
				      		{			
				      			$("#closeLinkId").val('createController');
				      			
				      			$("#conditionId").val('Redirect');
				      			
						 		$("#message-box-success").show();
						 		$("#messageId").html("Controller Created Successfully.");
				      		}
				      	else
				      		{
				      			$("#closeLinkId").val('createController');
				      		
				      			$("#conditionId").val('Redirect');
				      			
					      		$("#message-box-danger").show();
						 		$("#messageId").html("Controller Created Successfully.");		 					 		
				      		}
				    	
				     },  
			    });  
		}
	
}


function searchController()
{
	var type = $("#searchTypeId").val();	
	var value = $("#searchValueId").val();
	
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "searchController",
	     data : "searchType="+type+"&searchValue="+value,
	     success : function(response) 
	     {    	 
	    	 $("#message-box-waiting").hide();
	    	 
	    	 $("#hiddenDiv").slideDown();
		 	 $("#controllerSearchResultId").html(response);		 		
	     },  
    }); 
	
}



function showNotifyBox(userId,userType)
{
	$("#message-box-waiting").show();
		
		$.ajax({  
			
		     type : "post",   
		     url : "showNotifyBox",
		     data : "userId="+userId+"&userType="+userType,
		     success : function(response) 
		     {    	 
		    	 $("#message-box-waiting").hide();
		    	 
		    	 $("#composeDiv").slideDown();
			 	 $("#composeMsgDiv").html(response);		 		
		     },  
	    }); 
}


function sendMessageToUser()
{
	var flag = 'true';
	
	if(flag=='true')
	{
			
		$("#message-box-waiting").show();
		
		 var formData = new FormData($("#composeMessageForm")[0]);
		
		  $.ajax({
					type : "POST",
					data : formData,
					url : "sendMessageToUser",								
					success : function(response) {
			            
						$("#message-box-waiting").hide();
						
						 if(response=="success")
				      		{			
				      			alert("Your Message sent to user successfully.");
				      		}
				      	 else
				      		{
				      			alert("Operation was stoped due to some problem.Please try again later.");		 					 		
				      		}
					},
					cache: false,
			        contentType: false,
			        processData: false,
			});
		   	
	}
	
}




function updateControllerAccess(cotrollerId,status)
{
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "updateControllerAccess",
	     data : "cotrollerId="+cotrollerId+"&status="+status,
	     success : function(response) 
	     {    	 
    	 	$("#message-box-waiting").hide();
    	 	
    	 	if(response=="success")
	 		{
    	 		$("#closeLinkId").val('createController');
      			
      			$("#conditionId").val('NoRedirect');
      			
		 		$("#message-box-success").show();
		 		$("#messageId").html("Controller Status has been successfully updated.");
		 		
		 		searchController();  
	 		}
    	 	else
    	 	{
    	 		$("#closeLinkId").val('createController');
	      		
      			$("#conditionId").val('NoRedirect');
      			
	      		$("#message-box-danger").show();
		 		$("#messageId").html("There might be a problem so please try again later.");
    	 	}
    	 		 			 		
	     },  
    });
}

//////////////////////  END OF SCRIPT FOR CONTROLLER /////////////////////////////////////////////////////////////////////





//////////////////////    FINACIAL POLICY DETAILS     ///////////////////////////////////////

function addComponent(value)
{
	if(value=="Yes")
	{
		$('#yesId').prop('checked', true);
		$('#noId').prop('checked', false);
		
		$("#extraComp").slideDown();
	}
	else
	{
		$('#yesId').prop('checked', false);
		$('#noId').prop('checked', true);
		
		$("#extraComp").slideUp();
		$("#reqdComp1").slideUp();
		$("#reqdComp2").slideUp();
	}
}

function showOptionBox(value)
{
	if(value=="TB")
	{
		$("#reqdComp1").slideDown();
		$("#reqdComp2").slideUp();
	}
	else if(value=="DD")
	{
		$("#reqdComp1").slideDown();
		$("#reqdComp2").slideDown();
	}
	else
	{
		$("#reqdComp1").slideUp();
		$("#reqdComp2").slideUp();
	}
}

function addOptionValue()
{
	var compOption = $("#compOption").val();
	
	if(compOption=="")
	{
		$("#errorReqdComp2Id").html("Please enter the option value");
		$("#errorReqdComp2Id").show();
	}
	else
	{
		$("#errorReqdComp2Id").hide();
		
		var addOptionId = $("#addOptionId").val();
		
		addOptionId += compOption + ",";
		
		$("#addOptionId").val(addOptionId);
		
		alert("Option value added successfully,Do the same to enter more.");
		
		$("#compOption").val("");
	}
}


function addGivenComponent()
{
	var addCompId = $("#addCompId").val();
	
	var compType = $("#compTypeId").val();
	var compName = $("#compName1").val();
	
	if(compType=="TB")
	{
		addCompId += compType+":"+compName+"/";
		
		$("#addCompId").val(addCompId);
		
		$("#compName1").val("");
		
		alert("Component added successfully,Do the same to add more.");
		
		displayAddedComp();
		
	}
	else if(compType=="DD")
	{
		var addOptionId = $("#addOptionId").val();
		addOptionId = addOptionId.substring(0, addOptionId.length - 1);
		
		addCompId += compType+":"+compName+":"+addOptionId+"/"; 
		
		$("#addCompId").val(addCompId);
		
		$("#addOptionId").val("");
		$("#compName1").val("");
		
		alert("Component added successfully,Do the same to add more.");
		
		displayAddedComp();
	}
	else
	{
		$("#errorcompTypeId").html("Please Select component type.");
		$("#errorcompTypeId").show();
	}	
}

function displayAddedComp()
{
	$("#addedCompId").slideDown();
	$("#addCompDisplayId").html("");
			
	var addCompId = $("#addCompId").val();
	addCompId = addCompId.substring(0, addCompId.length - 1);
	
	var compArray = addCompId.split("/");
	
	for(var i=0;i<compArray.length;i++)
	{
		var eachComp = compArray[i].split(":");
		
		/*alert("Value-"+compArray[i]);*/
		
		if(eachComp[0]=="TB")
		{
			$("#addCompDisplayId").append(eachComp[1]+" - TextBox <p class='fa fa-times addCompDeleteStyle'  onclick='removeComp()'></p> <br>");
		}
		else
		{
			$("#addCompDisplayId").append(eachComp[1]+" - DropDown - "+eachComp[2]+" <p class='fa fa-times addCompDeleteStyle'  onclick='removeComp()'></p> <br>");
		}
	}	
}

function removeComp(id)
{
	var compData = $("#"+id).val();
	//alert(compData);
	
	var addCompId = $("#addCompId").val();
	addCompId = addCompId.substring(0, addCompId.length - 1);
	
	var tempComp = "";
	
	var compArray = addCompId.split("/");
	for(var i=0;i<compArray.length;i++)
	{
		if(compArray[i]!=compData)
		{
			tempComp += compArray[i] +"/";
		}
	}
	
	$("#addCompId").val(tempComp);
	
	displayAddedComp();
}

function savePolicyTypeDetails()
{
	var flag = 'true';
	
	var type = $("#typeId").val();
	var name = $("#nameId").val();
		
	if(type=="Select finance type")
	{
		flag = 'false';
		$("#errorTypeId").show();
		$("#errorTypeId").html("Enter Financial Policy Type.");	
        return false;  
	}
	
	if(type!="Select finance type")
	{
		$("#errorTypeId").hide(); 
	}
	
	if(name=="")
	{
		flag = 'false';
		$("#errorNameId").show();
		$("#errorNameId").html("Enter Financial Policy Name.");	
        return false;  
	}
	
	if(name=="")
	{
		$("#errorNameId").hide();
	}
	
	if(flag=='true')
	{	
	        $("#message-box-waiting").show();
	
			$.ajax({  
				
			     type : "post",   
			     url : "savePolicyTypeDetails",
			     data : $('#createFinPolicyForm').serialize(),
			     success : function(response) 
			     {						    	 
			    	$("#message-box-waiting").hide();
			    	
			      	if(response=="success")
			      		{			
			      			$("#closeLinkId").val('manageLoanType');
			      			
			      			$("#conditionId").val('Redirect');
			      			
					 		$("#message-box-success").show();
					 		$("#messageId").html("Policy type saved  Successfully.");
			      		}
			      	else
			      		{
			      			$("#closeLinkId").val('manageLoanType');
			      		
			      			$("#conditionId").val('Redirect');
			      			
				      		$("#message-box-danger").show();
					 		$("#messageId").html("There might be a problem so please try again later.");		 					 		
			      		}			    	
			     },  
		    });  
	}
	
	
}


function searchPolicyTypeDetails()
{
	var type = $("#searchTypeId").val()	
	
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "searchPolicyTypeDetails",
	     data : "type="+type,
	     success : function(response) 
	     {    	 
    	 	$("#message-box-waiting").hide();
    	 
      		$("#hiddenDiv").slideDown();
	 		$("#finTypeSearchResultId").html(response);		 		
	     },  
    }); 
}


function editPolicyName(typeId)
{
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "editPolicyName",
	     data : "typeId="+typeId,
	     success : function(response) 
	     {				    	 
	    	$("#message-box-waiting").hide();
	    	
	      	if(response=="success")
	      		{			
	      			$("#closeLinkId").val('manageLoanType');
	      			
	      			$("#conditionId").val('Redirect');
	      			
			 		$("#message-box-success").show();
			 		$("#messageId").html("Policy type edited Successfully.");
	      		}
	      	else
	      		{
	      			$("#closeLinkId").val('manageLoanType');
	      			
	      			$("#conditionId").val('Redirect');
	      			
		      		$("#message-box-danger").show();
			 		$("#messageId").html("There might be a problem so please try again later.");		 					 		
	      		}	    	
	     },  
    });  
}


function deletePolicyName(typeId)
{
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "deletePolicyName",
	     data : "typeId="+typeId,
	     success : function(response) 
	     {				    	 
	    	$("#message-box-waiting").hide();
	    	
	      	if(response=="success")
	      		{			
	      			//$("#closeLinkId").val('manageLoanType');
	      			
	      			$("#conditionId").val('NoRedirect');
	      		
			 		$("#message-box-success").show();
			 		$("#messageId").html("Policy type deleted Successfully.");
			 		
			 		searchPolicyTypeDetails();
	      		}
	      	else
	      		{
	      			$("#closeLinkId").val('manageLoanType');
	      		
	      			$("#conditionId").val('NoRedirect');
	      			
		      		$("#message-box-danger").show();
			 		$("#messageId").html("There might be a problem so please try again later.");		 					 		
	      		}	    	
	     },  
    });  
}

function showFilterOption(filterType)
{
	$("#errorTypeId").hide();
	if(filterType=="Identity")
	{
		$("#filterDiv1-2").slideUp();
		
		$("#filterDiv1-1").slideDown();
		$("#typeBoxId").slideDown();
	}
	else
	{
 		$("#message-box-waiting").show();
 		var value = "";
		
 		$.ajax({  
			
		     type : "post",   
		     url : "searchPolicyTypeName",
		     data : "value="+value,
		     success : function(response) 
		     {		
			    $("#message-box-waiting").hide();
			 	
			    var arrayValue = response.split("/");
			    
			    $("#filterStep1-2").html("");
			    
			    for(var i=0;i<arrayValue.length;i++)
			    {
			    	$("#filterStep1-2").append("<option value='"+arrayValue[i]+"'>"+arrayValue[i]+"</option>");
			    }
			    
		    	$("#filterDiv1-1").slideUp();
		 		
		 		$("#filterDiv1-2").slideDown();
		 		$("#typeBoxId").slideUp();		 		
		     },  
	    });  
	}
}


function searchPolicyAsPerCriteria()
{		
	var filterStep1 = $("#filterStep1").val();
	
	if(filterStep1=="Select Search Type")
	{
		$("#errorTypeId").show();
		$("#errorTypeId").html("Please select a search type to get the result.")
	}
	else if(filterStep1=="Identity")
	{
		var type = $("#filterStep1-1").val();
		var value = $("#searchValueId").val();
		
		 $("#message-box-waiting").show();
			
			$.ajax({  
				
			     type : "post",   
			     url : "searchPolicyAsPerCriteria",
			     data : "type="+type+"&value="+value,
			     success : function(response) 
			     {			
			    	 
			    	$("#message-box-waiting").hide();
			    	
			    	$("#hiddenDiv").show();
			      	$("#searchDataId").html(response);
			    	
			     },  
		    });
	}
	else
	{
		 var type = $("#filterStep1-2").val();
		
		 $("#message-box-waiting").show();
			
		 $.ajax({  
				
			     type : "post",   
			     url : "searchPolicyAsPerCriteria",
			     data : "type="+type,
			     success : function(response) 
			     {			
			    	 
			    	$("#message-box-waiting").hide();
			    	
			    	$("#hiddenDiv").show();
			      	$("#searchDataId").html(response);
			    	
			     },  
		    });
	}
	
	
}


function managePolicyDetails(policyId)
{
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "managePolicyDetails",
	     data : "policyId="+policyId,
	     success : function(response) 
	     {				    	 
	    	$("#message-box-waiting").hide();
	    	
	    	$("#policyMnageDivId").show();
	      	$("#policyMnageDivId").html(response);	    	
	     },  
    });
}

function updatePolicyByAdmin(policyId)
{
	$("#message-box-waiting").show();
	
	var policyStatusId = $("#policyStatusId").val();
	
	$.ajax({  
		
	     type : "post",   
	     url : "updatePolicyByAdmin",
	     data : "policyId="+policyId+"&policyStatus="+policyStatusId,
	     success : function(response) 
	     {				    	 
	    	 if(response=="success")
	      		{			
	      			//$("#closeLinkId").val('manageLoanType');
	      			
			 		$("#message-box-success").show();
			 		$("#messageId").html("Policy type deleted Successfully.");
			 		
			 		searchPolicyTypeDetails();
	      		}
	      	else
	      		{
	      			$("#closeLinkId").val('manageLoanType');
	      		
		      		$("#message-box-danger").show();
			 		$("#messageId").html("There might be a problem so please try again later.");		 					 		
	      		}  	
	     },  
    });
}




///////////////////////////////////      END OF FINANCE POLICY SECTION         //////////////////////////////////











///////////////////////////////////            STATRT OF FINANCE REPORT SECTION //////////////////////////

function generateReport()
{
	var type = "type";
	$("#message-box-waiting").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "generateReport",
	     data : "searchType="+type,
	     success : function(response) 
	     {    	 
	    	 $("#message-box-waiting").hide();
	    	 
	    	 $("#hiddenDiv").show();
		 	 $("#reportData").html(response);		 		
	     },  
    }); 
}










// ADMIN SETTING 


function saveAdminPassword()
{
	var flag = 'true';
	
	var curPassword = $("#curPasswordId").val();
	var newPassword = $("#newPasswordId").val();
	var cnfPassword = $("#cnfPasswordId").val();
	
	if(curPassword=="")
	{
		flag = 'false';
		$("#curErrorId").show();
		$("#curErrorId").html("Enter current password.");
		return false;
	}
	
	if(curPassword!="")
	{
		$("#curErrorId").hide();
	}
	
	if(newPassword=="")
	{
		flag = 'false';
		$("#newErrorId").show();
		$("#newErrorId").html("Enter new password.");
		return false;
	}
	
	if(newPassword!="")
	{
		$("#newErrorId").hide();
	}
	
	if(cnfPassword=="")
	{
		flag = 'false';
		$("#cnfErrorId").show();
		$("#cnfErrorId").html("Confirm new password.");
		return false;
	}
	
	if(cnfPassword!="")
	{
		$("#cnfErrorId").hide();
	}
	
	if(cnfPassword!=newPassword)
	{
		flag = 'false';
		$("#cnfErrorId").show();
		$("#cnfErrorId").html("Not matched with new password.");
		return false;
	}
	
	if(flag=='true')
	{
		$("#message-box-waiting").show();
		
		$.ajax({  
			
		     type : "post",   
		     url : "changeAdminPassword",
		     data : "curPassword="+curPassword+"&newPassword="+newPassword,
		     success : function(response) 
		     {    	 
		    	 $("#message-box-waiting").hide();
		    	 
		    	 
		    	 if(response=="success")
		      		{			
		      			$("#closeLinkId").val('otherSettings');
		      			
		      			$("#conditionId").val('Redirect');
		      			
				 		$("#message-box-success").show();
				 		$("#messageId").html("Admin password changed Successfully.");
		      		}
		    	else if(response=="NotMatched")
		      		{			
			    		$("#curErrorId").show();
			    		$("#curErrorId").html("Given current password was not correct.");
		      		}
		      	else
		      		{
		      			$("#closeLinkId").val('otherSettings');
		      		
		      			$("#conditionId").val('Redirect');
		      			
			      		$("#message-box-danger").show();
				 		$("#messageId").html("Operation was stoped due to some problem.Please try again later.");		 					 		
		      		}
		    	 
		     },  
	    }); 
	}
	
}



function saveAdminProfile()
{
	var flag = 'true';
	
	var name = $("#nameId").val();
	var role = $("#roleId").val();
	var photo = $("#file").val();
	
	if(name=="")
	{
		flag = 'false';
		$("#nameErrorId").show();
		$("#nameErrorId").html("Please Enter the name.");
		return false;
	}
	
	if(name!="")
	{
		$("#nameErrorId").hide();
	}
	
	
	if(role=="")
	{
		flag = 'false';
		$("#roleErrorId").show();
		$("#roleErrorId").html("Please Enter the role.");
		return false;
	}
	
	if(role!="")
	{
		$("#roleErrorId").hide();
	}

	
	
	if(photo!="")
	{		
	   var fileDetails = photo;
	   $("#otherErrorId").hide();
	    var ext=fileDetails.substring(fileDetails.lastIndexOf('.')+1);
	    var extension = new Array("JPG","jpg","jpeg","JPEG","gif","GIF","png","PNG","pdf","PDF","doc","DOC","docx","DOCX");	
	    		
	    var condition = "NotGranted";
	    for(var m=0;m<extension.length;m++)
			{
			    if(ext==extension[m])
			    	{				    	    
			    	   condition="Granted";				    	    
			    	}				    
			}
		if(condition=="NotGranted")
			{
			    flag = 'false';	
			    $("#fileErrorId").show();
			    $("#fileErrorId").html("Image and document files are allowed.");
				return false;				  
			}
		
		 var fileDetails1 = document.getElementById("file");
		 var fileSize = fileDetails1.files[0];
		 var fileSizeinBytes = fileSize.size;
		 var sizeinKB = +fileSizeinBytes / 1024;
  		 var sizeinMB = +sizeinKB / 1024;
  		 
  		 if(sizeinMB>2)
  			 {
  				flag = 'false';
  				$("#fileErrorId").show();
  				$("#fileErrorId").html("choose a file less then 2 MB.");
				return false;	 		   		 
  			 }  		 
	}
    
    		    
		if(flag=='true')
		{
				
			$("#message-box-waiting").show();
			
			 var formData = new FormData($("#otherDetailsForm")[0]);
			
			 formData.append("file", file.files[0]);
			
			  $.ajax({
						type : "POST",
						data : formData,
						url : "saveAdminDetail",								
						success : function(response) {
				            
							$("#message-box-waiting").hide();
							
							 if(response=="success")
					      		{			
					      			$("#closeLinkId").val('otherSettings');
					      			
					      			$("#conditionId").val('Redirect');
					      			
							 		$("#message-box-success").show();
							 		$("#messageId").html("Admin details saved Successfully.");
					      		}
					      	else
					      		{
					      			$("#closeLinkId").val('otherSettings');
					      		
					      			$("#conditionId").val('Redirect');
					      			
						      		$("#message-box-danger").show();
							 		$("#messageId").html("Operation was stoped due to some problem.Please try again later.");		 					 		
					      		}
						},
						cache: false,
				        contentType: false,
				        processData: false,
				});
			   	
		}

}