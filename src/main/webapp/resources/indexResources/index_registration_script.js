
function hideNotification()
{
	$("#successId").hide();
}



function closeAlert()
{
	var action = $("#action").val();
    var value = $("#value").val();     
    
    if(action=="url")
    {
    	window.location = value;
    }
    else
    {
    	// value+"()"; 
    }
}





// ------------ ALL THE BORROWER REGISTRATION SCRIPT  -------------------


function activeUserLink(userType)
{
	if(userType=="Borrower")
		{
			$("#borId").addClass("activeLink");
		}
	else
		{
			$("#lenId").addClass("activeLink");
		}
}

// Borrower registration script


function checkOnEachBorrowerRegForm()
{
	var alfanumeric = /^[a-zA-Z0-9]*$/;        	
	var nameWithOutSpace = /^[a-zA-Z]+$/;
	var nameWithSpace = /^[a-zA-Z\s]*$/;	
	var phNo = /^[0-9]{10}$/;	
	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	var poi= /^[a-zA-Z0-9]{6,20}$/;
	
    var fname = $("#form-first-name").val();
    var lname = $("#form-last-name").val();
    var gender = $("#form-gender").val();
    var emailId = $("#form-email").val();
    var mobileNo = $("#form-mobile").val();
    var password = $("#form-password").val();
    var confirmPassword = $("#form-cnfPassword").val();

    var panId = $("#form-panId").val();
   
    var agree = document.getElementById("form-terms");;
  
    if(fname!="")
	{
		if(!fname.match(nameWithSpace))  
        {  		
			$("#fnameErrId").show();
	        $("#fnameErrId").html("Only alphabets are allowed.");	
	        $("#form-first-name").addClass("errorInputboxStyle");
	        return false;       
        }
		else
		{
			$("#fnameErrId").hide();
			$("#form-first-name").removeClass('errorInputboxStyle');
		}
	}
    
    if(lname!="")
	{
		if(!lname.match(nameWithSpace))  
        {  			
			$("#lnameErrId").show();
	        $("#lnameErrId").html("Only alphabets are allowed");
	        $("#form-last-name").addClass("errorInputboxStyle");
	        return false;       
        }
		else
		{
			$("#lnameErrId").hide();
			$("#form-last-name").removeClass('errorInputboxStyle');
		}
	}
   
    if(gender!="Select your gender")
	{				
		$("#genderErrId").hide();
        $("#genderErrId").html("");	
        $("#form-gender").removeClass("errorInputboxStyle");
        return false;              
	}
    
    if(emailId!="")
	{
		if(!emailId.match(email))  
        {  			
			$("#emailErrId").show();
	        $("#emailErrId").html("Please enter a valid email id.");	
	        $("#form-email").addClass("errorInputboxStyle");
	        return false;       
        }
		else
		{
			$("#emailErrId").hide();
			$("#form-email").removeClass('errorInputboxStyle');
		}
	}
    
    if(mobileNo!="")
	{
		if(!mobileNo.match(phNo))  
        {  			
			$("#mobileErrId").show();
	        $("#mobileErrId").html("Please enter a valid mobile no.");	
	        $("#form-mobile").addClass("errorInputboxStyle");
	        return false;       
        }
		else
		{
			$("#mobileErrId").hide();  
			$("#form-mobile").removeClass('errorInputboxStyle');
		}
	}
    
    if(panId!="")
	{
		if(!panId.match(poi))  
        {  			
			flag='false';
			$("#panErrId").show();
	        $("#panErrId").html("Please enter a valid pan number.");
	        $("#form-panId").addClass("errorInputboxStyle");
	        //return false;       
        }
		else
		{
			$("#form-panId").removeClass("errorInputboxStyle");
			$("#panErrId").hide();
		}
	}
    
    
    if(password!="")
	{
		$("#passwordErrId").hide();
		$("#form-password").removeClass('errorInputboxStyle');
	}
   
    if(confirmPassword!="")
	{
		$("#cnfPasswordErrId").hide();
		$("#form-cnfPassword").removeClass('errorInputboxStyle');
	}
   
    if(agree.checked==true)
    {
		$("#termsErrId").hide();	
    }  
    
}

function borrowerRegistration()
{   			
	var alfanumeric = /^[a-zA-Z0-9]*$/;        	
	var nameWithOutSpace = /^[a-zA-Z]+$/;
	var nameWithSpace = /^[a-zA-Z\s]*$/;	
	var phNo = /^[0-9]{10}$/;	
	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	var poi= /^[a-zA-Z0-9]{6,20}$/;
	
    var fname = $("#form-first-name").val();
    var lname = $("#form-last-name").val();
    var gender = $("#form-gender").val();
    var emailId = $("#form-email").val();
    var mobileNo = $("#form-mobile").val();
    var password = $("#form-password").val();
    var confirmPassword = $("#form-cnfPassword").val();
    
    var panId = $("#form-panId").val();
    
    var agree = document.getElementById("form-terms");
  
    var flag = 'true';
    
    if(fname=="")
	{		
		flag = 'false';
		$("#fnameErrId").show();
        $("#fnameErrId").html("Please enter your first name.");	
        $("#form-first-name").addClass("errorInputboxStyle");
        //return false;              
	}
    
    if(fname!="")
	{
		if(!fname.match(nameWithSpace))  
        {  		
			flag = 'false';
			$("#fnameErrId").show();
	        $("#fnameErrId").html("Only alphabets are allowed.");	
	        $("#form-first-name").addClass("errorInputboxStyle");
	       // return false;       
        }
		else
		{
			$("#fnameErrId").hide();
		}
	}
    
    if(lname=="")
	{		
		flag = 'false';
		$("#lnameErrId").show();
        $("#lnameErrId").html("Please enter your last name.");	
        $("#form-last-name").addClass("errorInputboxStyle");
        //return false;              
	}
    
    if(lname!="")
	{
		if(!lname.match(nameWithSpace))  
        {  			
			flag = 'false';
			$("#lnameErrId").show();
	        $("#lnameErrId").html("Only alphabets are allowed");	
	        $("#form-last-name").addClass("errorInputboxStyle");
	        //return false;       
        }
		else
		{
			$("#lnameErrId").hide();
		}
	}
    
    if(gender=="Select your gender")
	{				
		$("#genderErrId").show();
        $("#genderErrId").html("Please select your gender.");	
        $("#form-gender").addClass("errorInputboxStyle");           
	}
    
    if(gender!="Select your gender")
	{				
		$("#genderErrId").hide();
        $("#genderErrId").html("");	
        $("#form-gender").removeClass("errorInputboxStyle");          
	}
    
    
    if(emailId=="")
	{		
		flag = 'false';
		$("#emailErrId").show();
        $("#emailErrId").html("Please enter your email id.");
        $("#form-email").addClass("errorInputboxStyle");
        //return false;              
	}
    
    if(emailId!="")
	{
		if(!emailId.match(email))  
        {  			
			flag = 'false';
			$("#emailErrId").show();
	        $("#emailErrId").html("Please enter a valid email id.");	
	        $("#form-email").addClass("errorInputboxStyle");
	        //return false;       
        }
		else
		{
			$("#emailErrId").hide();
		}
	}
       
    if(mobileNo=="")
	{		
		flag = 'false';
		$("#mobileErrId").show();
        $("#mobileErrId").html("Please enter your mobile no.");	
        $("#form-mobile").addClass("errorInputboxStyle");
       // return false;              
	}
    
    if(mobileNo!="")
	{
		if(!mobileNo.match(phNo))  
        {  			
			flag='false';
			$("#mobileErrId").show();
	        $("#mobileErrId").html("Please enter a valid mobile no.");
	        $("#form-mobile").addClass("errorInputboxStyle");
	        //return false;       
        }
		else
		{
			$("#mobileErrId").hide();
		}
	}
    
    if(panId=="")
	{		
		flag = 'false';
		$("#panErrId").show();
        $("#panErrId").html("Please enter your pan number.");	
        $("#form-panId").addClass("errorInputboxStyle");
       // return false;              
	}
    
    if(panId!="")
	{
		if(!panId.match(poi))  
        {  			
			flag='false';
			$("#panErrId").show();
	        $("#panErrId").html("Please enter a valid pan number.");
	        $("#form-panId").addClass("errorInputboxStyle");
	        //return false;       
        }
		else
		{
			$("#form-panId").removeClass("errorInputboxStyle");
			$("#panErrId").hide();
		}
	}
   
    if(password=="")
	{
    	flag='false';
		$("#passwordErrId").show();
        $("#passwordErrId").html("Please set a password for the account.");
        $("#form-password").addClass("errorInputboxStyle");
        //return false;
	}
    
    if(password!="")
	{
		$("#passwordErrId").hide();
	}
   
    if(confirmPassword=="")
	{
    	flag='false';
		$("#cnfPasswordErrId").show();
        $("#cnfPasswordErrId").html("Please confirm your password.");
        $("#form-cnfPassword").addClass("errorInputboxStyle");
        //return false;
	}
    
    if(confirmPassword!="")
	{
		$("#cnfPasswordErrId").hide();	 
	}
   
    if(password!=confirmPassword)
	{
    	flag='false';
		$("#cnfPasswordErrId").show();
        $("#cnfPasswordErrId").html("Confirm password not matched with password.");
        $("#form-cnfPassword").addClass("errorInputboxStyle");
        //return false;
	}
    
    if(agree.checked==false)
    {
    	flag='false';
		$("#termsErrId").show();
        $("#termsErrId").html("Are you agree with the terms and conditions.");
       // return false;
    } 
    
    if(agree.checked==true)
    {
		$("#termsErrId").hide();		
    }  
    
    
    if(flag=='true')
    	{  	
	    	 $("#preloader").show();
	    	 $("#loader").show();
	    	     	 
	    	 $.ajax({  
		 			
			     type : "post",   
			     url : "checkBorrowerEmailId", 
			     data :$('#borrowerRegForm').serialize(),	     	     	     
			     success : function(response) 
			     {  		
			    	 $("#preloader").hide();
			    	 $("#loader").hide();
			    	 
					 if(response=="Not Exist")
					 {			
						    
						     $.ajax({  
						 			
							     type : "post",   
							     url : "checkBorrowerPhno", 
							     data :$('#borrowerRegForm').serialize(),	     	     	     
							     success : function(response) 
							     {  		
							    	 $("#preloader").hide();
							    	 $("#loader").hide();
							    	 
									 if(response=="Not Exist")
										 {			
										 
										  $.ajax({  
									 			
										     type : "post",   
										     url : "checkBorrowerPanId", 
										     data :$('#borrowerRegForm').serialize(),	     	     	     
										     success : function(response) 
										     {  		
										    	 $("#preloader").hide();
										    	 $("#loader").hide();
										    	 
										    	 if(response=="Not Exist")
										      		{		
												    		 $("#preloader").show();
													    	 $("#loader").show();
													    	  
											     
																     $.ajax({  
																 			
																	     type : "post",   
																	     url : "saveBorrowerRegister", 
																	     data :$('#borrowerRegForm').serialize(),	     	     	     
																	     success : function(response) 
																	     {  		
																	    	 $("#preloader").hide();
																	    	 $("#loader").hide();
																	    	 
																	    	 if(response=="success")
																	      		{																	    		 	
																	    		   	$("#modalOTP").show();
																	    		   	$("#modelBg").show();
																				    
																				 	/*document.getElementById("action").value = "url";
																				 	document.getElementById("value").value = "borrowerReg";
																				 	
																				    $("#modal").show();
																				    $("#modelBg").show();
																				    
																				    $("#heading").html("Success Message");
																				    $("#contentMsg").html("Your have been successfully registered as a Borrower,Please verify the OTP to activate the account.");	
																				 */
																	      		}
																	      	else
																	      		{
																		      		document.getElementById("action").value = "url";
																				 	document.getElementById("value").value = "borrowerReg";
																				 	
																				    $("#modal").show();
																				    $("#modelBg").show();
																				    
																				    $("#heading").html("Failed Message");
																				    $("#contentMsg").html("There is some problem arise,Please try again later.");							    								    
																	      		}	
																	    	 
																	     },  
																     }); 
										      		}
										      	else
										      		{
												      		$("#preloader").hide();
													    	$("#loader").hide();
													    	
															$("#panErrId").show();
													        $("#panErrId").html("This Pan number already given by another user.");
													        $("#form-panId").addClass("errorInputboxStyle");
										      		}	
										    	 
										     },  
									     }); 
												    
									}
									else
									{		
										$("#preloader").hide();
								    	$("#loader").hide();
								    	 								    	 
										$("#mobileErrId").show();
								        $("#mobileErrId").html("Given mobile no is already register");
								        $("#form-mobile").addClass("errorInputboxStyle");
									}
							     },  
						     }); 
						 }
					 else
						 {							 	
							$("#preloader").hide();
					    	$("#loader").hide();
					    	 					    	 
							$("#emailErrId").show();
					        $("#emailErrId").html("Given emailId is already registered.");	
					        $("#form-email").addClass("errorInputboxStyle");
						 }
			     },  
		     }); 
	    	 
	    	 
    	}
    
}


// Borrower OTP validation for activating account

function activateAccountViaOTP()
{
	 $("#preloader").show();
	 $("#loader").show();
	 
	 $("#modalOTP").hide();
	 $("#modelBg").hide();
	 $("#otpErrId").hide();
	 
	 var otpNumber = $("#OtpId").val();
	 
		 
		     $.ajax({  
		 			
			     type : "post",   
			     url : "activateAccountViaOTP", 
			     data :"otpNumber="+otpNumber,	     	     	     
			     success : function(response) 
			     {  		
			    	 $("#preloader").hide();
			    	 $("#loader").hide();
			    	 
			    	 if(response=="success")
			      		{																	    		 				    		   	
			    		   	document.getElementById("action").value = "url";
						 	document.getElementById("value").value = "borrowerLogin";
						 	
						    $("#modal").show();
						    $("#modelBg").show();
						    
						    $("#heading").html("Success Message");
						    $("#contentMsg").html("Your account successfully activated.");						    
			      		}
			    	else if(response=="NotMached")
			      		{																	    		 				    		   	
			    			$("#modalOTP").show();
			    			$("#modelBg").show();		
			    			
			    			$("#otpErrId").show();
			    			$("#otpErrId").html("Enter OTP is Incorrect.");
			      		}
			      	else
			      		{
				      		document.getElementById("action").value = "url";
						 	document.getElementById("value").value = "borrowerReg";
						 	
						    $("#modal").show();
						    $("#modelBg").show();
						    
						    $("#heading").html("Failed Message");
						    $("#contentMsg").html("There is some problem arise,Please try again later.");							    								    
			      		}				    	 
			     },  
		     }); 
	
}


function generateOTPForuser()
{
	var userId = $("#form-username").val();	 
	 
    $.ajax({  
			
	     type : "post",   
	     url : "generateOTPForuser", 
	     data :"userId="+userId,	     	     	     
	     success : function(response) 
	     {  		
	    	 $("#preloader").hide();
	    	 $("#loader").hide();
	    	 
	    	 if(response=="success")
	      		{																	    		 				    		   	
	    		 	$("#modalOTP").show();
	    		   	$("#modelBg").show();					    
	      		}
	      	 else
	      		{
		      		document.getElementById("action").value = "url";
				 	document.getElementById("value").value = "borrowerReg";
				 	
				    $("#modal").show();
				    $("#modelBg").show();
				    
				    $("#heading").html("Failed Message");
				    $("#contentMsg").html("There is some problem arise,Please try again later.");							    								    
	      		}				    	 
	     },  
    }); 
}






// Borrower forget password script

function forgetPassword(userType)
{
	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;	
    var emailId = $("#form-emailId").val();

    var flag = 'true';
    
    if(emailId=="")
	{
		flag = 'false';
		$("#emailErrId").show();
        $("#emailErrId").html("Please enter your registered email id.");
	}
    
    if(emailId!="")
	{
		if(!emailId.match(email))  
        {  			
			flag = 'false';
			$("#emailErrId").show();
	        $("#emailErrId").html("Please enter a valid email id.");	
	        $("#form-emailId").addClass("errorInputboxStyle");
	        //return false;       
        }
		else
		{
			$("#emailErrId").hide();
		}
	}
       
    
    if(flag=='true')
    	{
		    $("#preloader").show();
		   	$("#loader").show();
		   	
		   	if(userType=='borrower')
		   	
		   		{
		   		
			   		$.ajax({  
						
					     type : "post",   				     
					     data :$('#borrowerForgetForm').serialize(),
					     url : "borrowerForgetPassword", 				     	     	     	     
					     success : function(response) 
					     {  		
					    	 $("#preloader").hide();
					    	 $("#loader").hide();
					    	 
					    	 if(response=="success")
					      		{		
								 	document.getElementById("action").value = "url";
								 	document.getElementById("value").value = "borrowerForgetPwd";
								 	
								    $("#modal").show();
								    $("#modelBg").show();
								    
								    $("#heading").html("Success Message");
								    $("#contentMsg").html("Please check with your email to change the password.");							    
					      		}
					      	else
					      		{
						      		document.getElementById("action").value = "url";
								 	document.getElementById("value").value = "borrowerForgetPwd";
							 	
								    $("#modal").show();
								    $("#modelBg").show();
								    
								    $("#heading").html("Failed Message");
								    $("#contentMsg").html("There is some problem arise,Please try again later.");							    								    
					      		}	
					    	 
					     },  
				     }); 
		   		
		   		}
		   
		   	else 
		   		{
		   		    
			   		$.ajax({  
						
					     type : "post",   				     
					     data :$('#borrowerForgetForm').serialize(),
					     url : "lenderForgetPassword", 				     	     	     	     
					     success : function(response) 
					     {  		
					    	 $("#preloader").hide();
					    	 $("#loader").hide();
					    	 
					    	 if(response=="success")
					      		{		
								 	document.getElementById("action").value = "url";
								 	document.getElementById("value").value = "lenderForgetPwd";
								 	
								    $("#modal").show();
								    $("#modelBg").show();
								    
								    $("#heading").html("Success Message");
								    $("#contentMsg").html("Please check with your email to change the password.");							    
					      		}
					      	else
					      		{
						      		document.getElementById("action").value = "url";
								 	document.getElementById("value").value = "lenderForgetPwd";
							 	
								    $("#modal").show();
								    $("#modelBg").show();
								    
								    $("#heading").html("Failed Message");
								    $("#contentMsg").html("There is some problem arise,Please try again later.");							    								    
					      		}	
					    	 
					     },  
				     }); 
		   		
		   		}
		   		
    	}
}


// Borrower save reset password script


function savePasswordOut(userType)
{	
	 var password = $("#form-password").val();
	 var confirmPassword = $("#form-cnfPassword").val();
	    
	 var flag = 'true';
	 
	    if(password!="")
		{
			$("#passwordErrId").hide();
		}
	   
	    if(confirmPassword=="")
		{
	    	flag='false';
			$("#cnfPasswordErrId").show();
	        $("#cnfPasswordErrId").html("Please confirm your password.");
	        $("#form-cnfPassword").addClass("errorInputboxStyle");
	        //return false;
		}
	    
	    if(confirmPassword!="")
		{
			$("#cnfPasswordErrId").hide();	 
		}
	   
	    if(password!=confirmPassword)
		{
	    	flag='false';
			$("#cnfPasswordErrId").show();
	        $("#cnfPasswordErrId").html("Confirm password not matched with password.");
	        $("#form-cnfPassword").addClass("errorInputboxStyle");
	        //return false;
		}
	    
	    
	    
	    if(flag=='true')
	    	{
	    	
		    	 $("#preloader").show();
		    	 $("#loader").show();
		    	 
	    	 
		    	 if(userType=='borrower')
		    		 {
				    		 $.ajax({  
						 			
							     type : "post",   
							     url : "saveBorrowerResetPassword", 
							     data :$('#resetPassword').serialize(),	     	     	     
							     success : function(response) 
							     {  		
							    	 $("#preloader").hide();
							    	 $("#loader").hide();
							    	 
							    	 if(response=="success")
							      		{		
										 	document.getElementById("action").value = "url";
										 	document.getElementById("value").value = "borrowerLogin";
										 	
										    $("#modal").show();
										    $("#modelBg").show();
										    
										    $("#heading").html("Success Message");
										    $("#contentMsg").html("Your Password changed Successfully.");							    
							      		}
							      	else
							      		{
								      		document.getElementById("action").value = "url";
										 	document.getElementById("value").value = "borrowerLogin";
									 	
										    $("#modal").show();
										    $("#modelBg").show();
										    
										    $("#heading").html("Failed Message");
										    $("#contentMsg").html("There is some problem arise,Please try again later.");							    								    
							      		}
							    	 
							     },  
						     }); 
		    		 }
		    	 else
		    		 {
				    		 $.ajax({  
						 			
							     type : "post",   
							     url : "saveLenderResetPassword", 
							     data :$('#resetPassword').serialize(),	     	     	     
							     success : function(response) 
							     {  		
							    	 $("#preloader").hide();
							    	 $("#loader").hide();
							    	 
							    	 if(response=="success")
							      		{		
										 	document.getElementById("action").value = "url";
										 	document.getElementById("value").value = "lenderLogin";
										 	
										    $("#modal").show();
										    $("#modelBg").show();
										    
										    $("#heading").html("Success Message");
										    $("#contentMsg").html("Your Password changed Successfully.");							    
							      		}
							      	else
							      		{
								      		document.getElementById("action").value = "url";
										 	document.getElementById("value").value = "lenderLogin";
									 	
										    $("#modal").show();
										    $("#modelBg").show();
										    
										    $("#heading").html("Failed Message");
										    $("#contentMsg").html("There is some problem arise,Please try again later.");							    								    
							      		}
							    	
							     },  
						     }); 
		    		 }
		    	 
		    	
	    		   			
	    	}   
	    
}



// EOF of borrower registration script





// --------------  ALL THE LENDER REGISTRATION SCRIPT  -------------------



function changeNameType(selectType)
{
	if(selectType=="An Organization")
	{		
		document.getElementById("action").value = "url";
	 	document.getElementById("value").value = "lenderLogin";
	 	
	    $("#modal").show();
	    $("#modelBg").show();
	    
	    $("#heading").html("Message for registration");
	    $("#contentMsg").html("For <o style='color:#00b6f5;'>'Organization'</o> registration <br>Please contact to <o style='color:#00b6f5;'>'support@fp.com'</o> or <o style='color:#00b6f5;'>'080-8955623'</o>");							    
		
	}
	else if(selectType=="Individual")
	{
		$("#nameTypeId").html("Full Name");
		$("#fullNameId").attr("placeholder", "Enter your Full Name")
		
		$("#representTagId").hide();
	}
	else if(selectType=="Bank")
	{		
		document.getElementById("action").value = "url";
	 	document.getElementById("value").value = "lenderLogin";
	 	
	    $("#modal").show();
	    $("#modelBg").show();
	    
	    $("#heading").html("Message for registration");
	    $("#contentMsg").html("For <o style='color:#00b6f5;'>'Bank'</o> registration <br>Please contact to <o style='color:#00b6f5;'>'support@fp.com'</o> or <o style='color:#00b6f5;'>'080-8955623'</o>");							    
		
	}
	else
	{
		$("#nameTypeId").html("Full Name");
		$("#fullNameId").attr("placeholder", "Enter your Full Name")
		
		$("#representTagId").hide();
	}
}


// Lender Registration Script


function checkOnEachLenderRegForm()
{
	var alfanumeric = /^[a-zA-Z0-9]*$/;        	
	var nameWithOutSpace = /^[a-zA-Z]+$/;
	var nameWithSpace = /^[a-zA-Z\s]*$/;	
	var phNo = /^[0-9]{10}$/;	
	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	var poi= /^[a-zA-Z0-9]{6,20}$/;
	
    var lenderType = $("#form-lender-type").val();
    var fullname = $("#form-full-name").val();
    var emailId = $("#form-email").val();
    var mobileNo = $("#form-mobileno").val();
    var password = $("#form-password").val();
    var confirmPassword = $("#form-cnfPassword").val();
    
    var agree = document.getElementById("form-terms");;
  
    if(fullname!="")
	{
		if(!fullname.match(nameWithSpace))  
        {  		
			$("#nameErrId").show();
	        $("#nameErrId").html("Only alphabets are allowed.");	
	        $("#form-full-name").addClass("errorInputboxStyle");
	        return false;       
        }
		else
		{
			$("#nameErrId").hide();
			$("#form-full-name").removeClass('errorInputboxStyle');
		}
	}
    
   if(lenderType!="Select type of lender")
	{
	   $("#typeErrId").hide();
       $("#form-lender-type").removeClass("errorInputboxStyle");
	}
   
    if(emailId!="")
	{
		if(!emailId.match(email))  
        {  			
			$("#emailErrId").show();
	        $("#emailErrId").html("Please enter a valid email id.");	
	        $("#form-email").addClass("errorInputboxStyle");
	        return false;       
        }
		else
		{
			$("#emailErrId").hide();
			$("#form-email").removeClass('errorInputboxStyle');
		}
	}
    
    if(mobileNo!="")
	{
		if(!mobileNo.match(phNo))  
        {  			
			$("#mobileErrId").show();
	        $("#mobileErrId").html("Please enter a valid mobile no.");	
	        $("#form-mobileno").addClass("errorInputboxStyle");
	        return false;       
        }
		else
		{
			$("#mobileErrId").hide();
			$("#form-mobileno").removeClass('errorInputboxStyle');
		}
	}
    
    if(password!="")
	{
		$("#passwordErrId").hide();
		$("#form-password").removeClass('errorInputboxStyle');
	}
   
    if(confirmPassword!="")
	{
		$("#cnfPasswordErrId").hide();
		$("#form-cnfPassword").removeClass('errorInputboxStyle');
	}
   
    if(agree.checked==true)
    {
		$("#termErrId").hide();	
    }  
    
}

function lenderRegistration()
{
	var alfanumeric = /^[a-zA-Z0-9]*$/;        	
	var nameWithOutSpace = /^[a-zA-Z]+$/;
	var nameWithSpace = /^[a-zA-Z\s]*$/;	
	var phNo = /^[0-9]{10}$/;	
	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	var poi= /^[a-zA-Z0-9]{6,20}$/;
	
    var lenderType = $("#lenderTypeId").val();
    var fname = $("#fullNameId").val();
    var emailId = $("#emailId").val();
    var mobileNo = $("#phnoId").val();
    var password = $("#passwordId").val();
    var confirmPassword = $("#cnfPasswordId").val();
    
    var agree = document.getElementById("termsId");
  
    var flag = 'true';
    
    
    if(lenderType=="Select type of lender")
	{		
		flag = 'false';
		$("#typeErrId").show();
        $("#typeErrId").html("Please select lender type.");	
        $("#form-lenderType").addClass("errorInputboxStyle");
        //return false;              
	}
    
    if(lenderType!="Select type of lender")
	{		
		$("#typeErrId").hide();              
	}
    
    if(fname=="")
	{		
		flag = 'false';
		$("#nameErrId").show();
        $("#nameErrId").html("Please enter your first name.");	
        $("#fullName").addClass("errorInputboxStyle");
        //return false;              
	}
    
    if(fname!="")
	{
		if(!fname.match(nameWithSpace))  
        {  		
			flag = 'false';
			$("#nameErrId").show();
	        $("#nameErrId").html("Only alphabets are allowed.");	
	        $("#fullName").addClass("errorInputboxStyle");
	       // return false;       
        }
		else
		{
			$("#nameErrId").hide();
		}
	}
    
    if(emailId=="")
	{		
		flag = 'false';
		$("#emailErrId").show();
        $("#emailErrId").html("Please enter your email id.");
        $("#email").addClass("errorInputboxStyle");
        //return false;              
	}
    
    if(emailId!="")
	{
		if(!emailId.match(email))  
        {  			
			flag = 'false';
			$("#emailErrId").show();
	        $("#emailErrId").html("Please enter a valid email id.");	
	        $("#email").addClass("errorInputboxStyle");
	        //return false;       
        }
		else
		{
			$("#emailErrId").hide();
		}
	}
       
    if(mobileNo=="")
	{		
		flag = 'false';
		$("#mobileErrId").show();
        $("#mobileErrId").html("Please enter your mobile no.");	
        $("#phno").addClass("errorInputboxStyle");
       // return false;              
	}
    
    if(mobileNo!="")
	{
		if(!mobileNo.match(phNo))  
        {  			
			flag='false';
			$("#mobileErrId").show();
	        $("#mobileErrId").html("Please enter a valid mobile no.");
	        $("#phno").addClass("errorInputboxStyle");
	        //return false;       
        }
		else
		{
			$("#mobileErrId").hide();
		}
	}
   
    if(password=="")
	{
    	flag='false';
		$("#passwordErrId").show();
        $("#passwordErrId").html("Please set a password for the account.");
        $("#password").addClass("errorInputboxStyle");
        //return false;
	}
    
    if(password!="")
	{
		$("#passwordErrId").hide();
	}
   
    if(confirmPassword=="")
	{
    	flag='false';
		$("#cnfPasswordErrId").show();
        $("#cnfPasswordErrId").html("Please confirm your password.");
        $("#cnfPassword").addClass("errorInputboxStyle");
        //return false;
	}
    
    if(confirmPassword!="")
	{
		$("#cnfPasswordErrId").hide();	 
	}
   
    if(password!=confirmPassword)
	{
    	flag='false';
		$("#cnfPasswordErrId").show();
        $("#cnfPasswordErrId").html("Confirm password not matched with password.");
        $("#form-cnfPassword").addClass("errorInputboxStyle");
        //return false;
	}
    
    if(agree.checked==false)
    {
    	flag='false';
		$("#termErrId").show();
        $("#termErrId").html("Are you agree with the terms and conditions.");
       // return false;
    } 
    
    if(agree.checked==true)
    {
		$("#termErrId").hide();		
    }  
   
    
    if(flag=='true')
    	{  	
    	     
	    	 $("#preloader").show();
	    	 $("#loader").show();
	    	     	 
	    	 $.ajax({  
		 			
			     type : "post",   
			     url : "checkLenderEmailId", 
			     data :$('#lenderRegForm').serialize(),	     	     	     
			     success : function(response) 
			     {  		
			    	 $("#preloader").hide();
			    	 $("#loader").hide();
			    	 
					 if(response=="Not Exist")
						 {			
						    
						     $.ajax({  
						 			
							     type : "post",   
							     url : "checkLenderPhno", 
							     data :$('#lenderRegForm').serialize(),	     	     	     
							     success : function(response) 
							     {  		
							    	 $("#preloader").hide();
							    	 $("#loader").hide();
							    	 
									 if(response=="Not Exist")
										 {			
										 
												 $("#preloader").show();
										    	 $("#loader").show();
										    	  
										     
												     $.ajax({  
												 			
													     type : "post",   
													     url : "saveLenderRegister", 
													     data :$('#lenderRegForm').serialize(),	     	     	     
													     success : function(response) 
													     {  		
													    	 $("#preloader").hide();
													    	 $("#loader").hide();
													    	 
													    	 
													    	 if(response=="success")
													      		{		
																 	document.getElementById("action").value = "url";
																 	document.getElementById("value").value = "lenderReg";
																 	
																    $("#modal").show();
																    $("#modelBg").show();
																    
																    $("#heading").html("Success Message");
																    $("#contentMsg").html("You have been Successfully registered as a lender,Please follow up the link sent to your registered email id.");							    
													      		}
													      	else
													      		{
														      		document.getElementById("action").value = "url";
																 	document.getElementById("value").value = "lenderReg";
															 	
																    $("#modal").show();
																    $("#modelBg").show();
																    
																    $("#heading").html("Failed Message");
																    $("#contentMsg").html("There is some problem arise,Please try again later.");							    								    
													      		}
													    	
													     },  
												     }); 
												     
										 }
									 else
										 {		
											$("#preloader").hide();
									    	$("#loader").hide();
									    	 
									    	$("#mobileErrId").show();
									        $("#mobileErrId").html("Given mobile no is already register");
									        $("#phno").addClass("errorInputboxStyle");

										 }
							     },  
						     }); 
						 }
					 else
						 {							 	
							$("#preloader").hide();
					    	$("#loader").hide();
					    	 	
							$("#emailErrId").show();
					        $("#emailErrId").html("Given emailId is already registered.");	
					        $("#email").addClass("errorInputboxStyle");
					        
						 }
			     },  
		     }); 
	    	 
	    	 
    	}
    
}



function sendContactForm()
{
	var flag = 'true';
	
	var nameWithSpace = /^[a-zA-Z\s]*$/;	
	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	
    var fname = $("#nameId").val();
    var emailId = $("#emailId").val();
    var reason = $("#contactForId").val();
    var message = $("#messageId").val();

    if(fname=="")
	{
    	flag = 'false';
		$("#nameErrId").show();
        $("#nameErrId").html("Enter your full name.");	
        $("#nameId").addClass("errorInputboxStyle");
	}
    
    if(fname!="")
	{
		if(!fname.match(nameWithSpace))  
        {  		
			flag = 'false';
			$("#nameErrId").show();
	        $("#nameErrId").html("Only alphabets are allowed.");	
	        $("#nameId").addClass("errorInputboxStyle");     
        }
		else
		{
			$("#nameErrId").hide();
			$("#nameId").removeClass("errorInputboxStyle");    
		}
	}
    
    if(emailId=="")
	{		
		flag = 'false';
		$("#emailErrId").show();
        $("#emailErrId").html("Please enter your email id.");
        $("#emailId").addClass("errorInputboxStyle");            
	}
    
    if(emailId!="")
	{
		if(!emailId.match(email))  
        {  			
			flag = 'false';
			$("#emailErrId").show();
	        $("#emailErrId").html("Please enter a valid email id.");	
	        $("#emailId").addClass("errorInputboxStyle");    
        }
		else
		{
			$("#emailErrId").hide();
			$("#emailId").removeClass("errorInputboxStyle");    
		}
	}
    
    if(reason=="Select reason for contact")
	{
    	flag = 'false';
		$("#reasonErrId").show();
        $("#reasonErrId").html("Select reson for contact.");	
        $("#contactForId").addClass("errorInputboxStyle");
	}
   
    if(reason!="Select reason for contact")
	{
		$("#reasonErrId").hide();
        $("#contactForId").removeClass("errorInputboxStyle");
	}
    
    if(message=="")
	{
    	flag = 'false';
		$("#messageErrId").show();
        $("#messageErrId").html("Please provide some message to know more about the contact reason.");	
        $("#messageId").addClass("errorInputboxStyle");
	}
   
    if(message!="")
	{
		$("#messageErrId").hide();
        $("#messageId").removeClass("errorInputboxStyle");
	}
    
    
	if(flag=='true')
	{		
		$("#preloader").show();
    	$("#loader").show();
    	 
		$.ajax({  
			
		     type : "post",   				     
		     data :$('#contactUsForm').serialize(),
		     url : "saveContactForm", 				     	     	     	     
		     success : function(response) 
		     {  		
		    	 $("#preloader").hide();
		    	 $("#loader").hide();
		    	 
		    	 if(response=="success")
		      		{		
					 	document.getElementById("action").value = "url";
					 	document.getElementById("value").value = "contactUs";
					 	
					    $("#modal").show();
					    $("#modelBg").show();
					    
					    $("#heading").html("Success Message");
					    $("#contentMsg").html("Thank you for contacting us,We will get back to you soon.");							    
		      		}
		      	else
		      		{
			      		document.getElementById("action").value = "url";
					 	document.getElementById("value").value = "contactUs";
				 	
					    $("#modal").show();
					    $("#modelBg").show();
					    
					    $("#heading").html("Failed Message");
					    $("#contentMsg").html("There is some problem arise,Please try again later.");							    								    
		      		}	
		    	 
		     },  
	     }); 
		
	}
	
	
	
}


function searchLoanForGuest()
{
	var flag = 'true';
	
	var nameWithSpace = /^[a-zA-Z\s]*$/;	
	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	
	var loanType = $("#loan_type").val();
	var loanTenure = $("#loan_tenure").val();
	var loanAmount = $("#loan_amount").val();
   
    if(loanType=="Select loan type")
	{
    	flag = 'false';
		$("#loanTypeErrId").show();
        $("#loanTypeErrId").html("Select loan type.");	
        $("#loan_type").addClass("errorInputboxStyle");
	}
   
    if(loanType!="Select loan type")
	{
		$("#loanTypeErrId").hide();
        $("#loan_type").removeClass("errorInputboxStyle");
	}
    
    if(loanTenure=="Select tenure")
	{
    	flag = 'false';
		$("#tenureErrId").show();
        $("#tenureErrId").html("Select loan duration.");	
        $("#loan_tenure").addClass("errorInputboxStyle");
	}
   
    if(loanTenure!="Select tenure")
	{
		$("#tenureErrId").hide();
        $("#loan_tenure").removeClass("errorInputboxStyle");
	}
    
    
    if(loanAmount=="")
	{
    	flag = 'false';
		$("#amountErrId").show();
        $("#amountErrId").html("Enter an amount for loan.");	
        $("#loan_amount").addClass("errorInputboxStyle");
	}
    
    if(loanAmount!="")
	{
    	$("#amountErrId").hide();
        $("#loan_amount").removeClass("errorInputboxStyle");
	}
    
    
	if(flag=='true')
	{		
		 $("#preloader").show();
    	 $("#loader").show();
    	 
		$.ajax({  
			
		     type : "post",   				     
		     data :$('#searchLoanForm').serialize(),
		     url : "resultSearchLoan", 				     	     	     	     
		     success : function(response) 
		     {  		
		    	 $("#preloader").hide();
		    	 $("#loader").hide();
		    	 
		    	 $("#resultDivId").html(response);							    
		      	
		     },  
	     }); 
		
	}

}
