package com.finance.util;

import java.util.ResourceBundle;

public class EmailNotificationUtil {


	public String emailNotification(String fullName,String emailId,String fileName,String link,String action,String reasonFor,String extraInfo)
	{		
		         
		         // Send Email to User that register in the block
		         
				 ResourceBundle resource = ResourceBundle.getBundle(fileName);
				 	 
				 String mailSubject=resource.getString("mailSubject");
				 
				 String headerText=resource.getString("headerText");
			   	 String headerLogo=resource.getString("headerLogo");
			   	 String contactNo=resource.getString("contactNo");
			   	 String contactEmailId=resource.getString("contactEmailId");
			   	 
			   	 String bodyHeading=resource.getString("bodyHeading");
			   	 String bodyMessage1=resource.getString("bodyMessage1");			   	 
			   	 String bodyMessage2=resource.getString("bodyMessage2");
			   	 
			   	 String actionLink=resource.getString("actionLink");
			   	 String belowImage=resource.getString("belowImage");
			   	 
			   	 String footerMessage=resource.getString("footerMessage");
			   	 
			   	 String copyright=resource.getString("copyright");
				 String termscondition=resource.getString("termscondition");		
				 String privacyPolicy = resource.getString("privacyPolicy");
					 
			   	 String facebookicon=resource.getString("facebookicon");
			   	 String twittericon=resource.getString("twittericon");
			   	 String googleicon=resource.getString("googleicon");
			   	 String linkedicon=resource.getString("linkedicon");
			   	 
			   	 String facebookLink=resource.getString("facebookLink");
			   	 String twitterLink=resource.getString("twitterLink");
			   	 String googlePlus=resource.getString("googlePlus");
			   	 String linkedIn=resource.getString("linkedIn");
			   	 
			   	 
			     StringBuilder text = new StringBuilder();
				        			        
				        
					    /* Sending Emails to Register user */
					       
				        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
				        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
				       
				        text.append("<body marginwidth='0' style='font-family:Verdana, Geneva, sans-serif;' marginheight='0' topmargin='0' leftmargin='0'>");
				       
				        text.append("<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>");
				        
						        text.append("<tr>");
						        
						          text.append("<td width='100%' valign='top' bgcolor='' style='padding-top:20px;'>");
							       
						            text.append("<table width='600' border='0' cellpadding='0' cellspacing='0' align='center' style='margin:0 auto;'>");					        
							         
						              text.append("<tr style='background-color: #00b6f5;'>");
							            
						                text.append("<td>");
								       
								            text.append("<table border='0' cellpadding='0' cellspacing='0' align='left' class='title'>");
									        text.append("<tr><td  style='padding:20px 20px 20px 0px;'>");
									        text.append("<a href='#' style='font-size:30px;text-decoration: none;color:#fff !important;'>Financial Site</a>");
									        text.append("</td></tr>");
									        text.append("</table>");
								        
									        text.append("<table border='0' cellpadding='0' cellspacing='0'align='right'>");
									        text.append("<tr><td valign='bottom' style='font-size:12px;font-weight:light;text-align:right;font-family:Verdana, Geneva, sans-serif;line-height:30px;color:#49196f;text-transform:uppercase;vertical-align:bottom;padding:20px 0px 20px 20px;'>");
									        text.append("<a href='#' style='text-decoration:none;padding: 2px 3px;color:#fff !important;' >Home</a>&nbsp;&nbsp;&nbsp;");
									        text.append("<a href='#' style='text-decoration:none;padding: 2px 3px;color:#fff !important;' >About</a>&nbsp;&nbsp;&nbsp;");
									        text.append("<a href='#' style='text-decoration:none;padding: 2px 3px;color:#fff !important;'>Contact</a>");
									        text.append("</td></tr>");
									        text.append("</table>");
								        
								        text.append("</td>");
								        
						              text.append("</tr>");
						             
						              text.append("<tr><td style='text-align: center;'height='3'></td></tr>");
						             
						              text.append("<tr><td height='30' width='100%' bgcolor='#515151' style='vertical-align: top;background-color: #e1e1e1;' valign='top'>");
						              text.append("<a  style='text-decoration: none; font-size: 20px;color: #49196f;font-family: Verdana, Geneva, sans-serif;'>"+bodyHeading+"</a>");
						              text.append("</td></tr>");
						              						              
						            text.append("</table>");
						        
						        text.append("<table bgcolor='#f5f5f5'  width='600'  cellspacing='0' cellpadding='0' border='0' align='center' style='height:auto;margin-top:3px;text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;' >");
						        text.append("<tbody>");
						        text.append("<tr>");
						        text.append("<td width='100%' valign='middle' style='text-align: left; font-family:cursive,Arial,sans-serif; font-size: 14px; color: rgb(63, 67, 69); line-height: 24px;' t-style='not6Text' mc:edit='41' object='text-editable'>");
						        text.append("<p style='font-family:cursive,Arial,sans-serif; font-weight: normal;'>");
						        text.append("Dear "+fullName+",");
						        text.append("</p><br>");
						        text.append("<p style='font-family:cursive,Arial,sans-serif; font-weight: normal;'>");
						        text.append(bodyMessage1);
						        text.append("</p>");
						        text.append("<p style='font-family:cursive,Arial,sans-serif; font-weight: normal;'>");
						        text.append(bodyMessage2);
						        text.append("</p>");
						        
						        
						        if(reasonFor.equals("LenderRegByController"))
						        {
						        	
						        	text.append("<p style='font-family:cursive,Arial,sans-serif; font-weight: normal;'>");
							        text.append("<b>User Name :</b>"+emailId+"<br>");
							        text.append("<b>Password :</b>"+extraInfo+"<br>");
							        text.append("</p>");
						        }
						        
						        
						        if(reasonFor.equals("ControllerReg"))
						        {
						        	String extraInfoArray[] = extraInfo.split("/");
						        	text.append("<p style='font-family:cursive,Arial,sans-serif; font-weight: normal;'>");
							        text.append("<b>User Name :</b>"+extraInfoArray[0]+"<br>");
							        text.append("<b>Password :</b>"+extraInfoArray[1]+"<br>");
							        text.append("</p>");
						        }
						        
						        if(reasonFor.equals("NotifyMessage"))
						        {
						        	text.append("<p style='font-family:cursive,Arial,sans-serif; font-weight: normal;'>");
							        text.append(extraInfo);
							        text.append("</p>");
						        }
						        
						        text.append("</td></tr>");
						        
						        
						        
						        
						        if(!link.equals("NotRequired"))
						        {
							        text.append("<tr><td align='center'>");
							        
							        text.append("<a href='"+link+"' style='font-size:14px;font-weight:bold;text-align:center;text-decoration:none;font-family:Verdana, Geneva, sans-serif;-webkit-text-size-adjust:none;padding: 4px 6px;color:#fff !important;background-color:#00b6f5;'>"+action+"</a>");
							        
							        text.append("</td></tr>");
						        }
						        
						        text.append("<tr><td>&nbsp;&nbsp;</td></tr>");
						        
						        text.append("</tbody></table>");
						        
						        text.append("<table width='600' border='0' cellpadding='0' cellspacing='0' align='center' style='margin:0 auto;'>");
						        text.append("<tr><td height='3'></td></tr>");
						        text.append("<tr><td align='center' style='font-size:15px;' bgcolor='#e1e1e1' >");
						        text.append("<a target='_' href='https://www.facebook.com'><img src='"+facebookicon+"'></a>&nbsp;&nbsp;&nbsp;");
						        text.append("<a target='_' href='https://www.twitter.com'><img src='"+twittericon+"'></a>&nbsp;&nbsp;&nbsp;");
						        text.append("<a target='_' href='https://www.google.com'><img src='"+googleicon+"'></a>&nbsp;&nbsp;&nbsp;");
						        text.append("<a target='_' href='https://www.linkedin.com'><img src='"+linkedicon+"'></a>");
						        text.append("</td></tr>");
						        text.append("<tr><td height='3'></td></tr>");
						        text.append("<tr><td bgcolor='#e1e1e1' align='center'>"+footerMessage+"</td></tr>");
						        text.append("</table>");
						        
						        text.append("<table width='600' border='0' cellpadding='0' cellspacing='0' align='center'  style='margin:0 auto;background-color: #00b6f5;'>");
						        text.append("<tr><td>");
						        text.append("<table width='600' border='0' cellpadding='0' cellspacing='0' align='center' style='margin:0 auto;font-size: 12px;'>");
						        text.append("<tr><td style='padding:5px 0px;line-height:30px;font-size:12px;color:#ffffff;text-align:center;padding:5px;'>");
						        text.append("&#169; 2016 Your Company Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
						        text.append("<span>");
						       /* text.append("<a href='#' style='text-decoration:none;color:#ffffff;'>Privacy</a> | ");
						        text.append("<a href='#' style='text-decoration:none;color:#ffffff;'>Terms</a>");*/
						        text.append("</span>");
						        text.append("<br>");
						        text.append(" Financial Portal <span>Design and Developed by <a href='http://jirehsol.co.in/' target='_' style='text-decoration:underline;color:#ffffff;'>Jireh</a>.</span>");
						        text.append("</td></tr></table>");
						        
						        
						    text.append("</td></tr><table>");
						      
						    text.append("</td></tr>");    
						    
				        text.append("</table>");
				        
				        text.append("</body>");
				        text.append("</html>");
				       
				        /* END OF BODY PART */
				        
				       
				        return text.toString();
				      
	}
	
	
	/*public static void main(String arg[])
	{
		EmailNotificationUtil obj = new EmailNotificationUtil();
		
		String status = obj.emailNotification("Abinash", "abinash.raula@jirehsol.com", "resources/wellcomeRegistration", "http://localhost:8081/FinancialPortal/", "Go to Home");
	
		System.out.println("result="+status);
	}*/
	
	
}
