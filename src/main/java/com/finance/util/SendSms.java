package com.finance.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ResourceBundle;

public class SendSms {
	
    public static String smsAPI(String msg,String number)
    {
    	String response = "";
     
    	ResourceBundle fileResource = ResourceBundle.getBundle("resources/smsParameters");
		String user = fileResource.getString("userName");
		String password = fileResource.getString("password");
		String authkey = fileResource.getString("authKey");
		String senderId = fileResource.getString("senderId");
				
    	//Multiple mobiles numbers separated by comma
    	String mobiles = number;
    	//Sender ID,While using route4 sender id should be 6 characters long.
    	//Your message to send, Add URL encoding here.
    	String message = msg;
    	//define route
    	String route="4";

    	//Prepare Url
    	URLConnection myURLConnection=null;
    	URL myURL=null;
    	BufferedReader reader=null;

    	//encoding message 
    	String encoded_message = "";
    	
		try 
		{
			encoded_message = URLEncoder.encode(message,"UTF-8");
		}
		catch (UnsupportedEncodingException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	//Send SMS API
    	String mainUrl="http://sms.jirehsol.co.in/api/sendhttp.php?";

    	//Prepare parameter string 
    	StringBuilder sbPostData= new StringBuilder(mainUrl);
    	
    	sbPostData.append("authkey="+authkey); 
    	sbPostData.append("&mobiles="+mobiles);
    	sbPostData.append("&message="+encoded_message);
    	sbPostData.append("&route="+route);
    	sbPostData.append("&sender="+senderId);
    	sbPostData.append("&country=91");
    	sbPostData.append("&response=json");

    	//final string
    	mainUrl = sbPostData.toString();
    	
    	
    	try
    	{
    	    //prepare connection
    	    myURL = new URL(mainUrl);
    	    myURLConnection = myURL.openConnection();
    	    myURLConnection.connect();
    	    reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
    	    //reading response 
    	   
    	    while ((response = reader.readLine()) != null) 
    	    //print response 
    	    System.out.println("Result="+response);
    	    
    	    //finally close connection
    	    reader.close();
    	} 
    	catch (IOException e) 
    	{ 
    		response = "failure";
    		e.printStackTrace();   		
    	}
    	
		return response; 

    }
    
    
    
    public static void main(String[] args) throws IOException {        
        
    	String response = smsAPI("Hai DOCOMO did u got my sms.","9036007662");
    	
    	System.out.println(response);
    	
    }
    
    
   
}