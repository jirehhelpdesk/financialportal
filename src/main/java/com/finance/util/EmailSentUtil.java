package com.finance.util;

import java.io.IOException;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailSentUtil {

	
	public String getEmailSent(String emailId,String subject,String messageBody)
	{		 
		    String resetStatus = "";
		  
		    ResourceBundle smsresource = ResourceBundle.getBundle("resources/emailConfig");
			
		    String auth=smsresource.getString("mail.smtp.auth");
			String starttls = smsresource.getString("mail.smtp.starttls.enable");
			String host = smsresource.getString("mail.smtp.host");			
			String port = smsresource.getString("mail.smtp.port");
			String emailusername = smsresource.getString("emailusername");
			String emailpassword = smsresource.getString("emailpassword");

			
		      
		      String from = emailusername;
		      final String username = from;//change accordingly
		      final String password = emailpassword;//change accordingly
		      Properties props = new Properties();
		      
		      
			props.put("mail.smtp.auth", auth);
			props.put("mail.smtp.starttls.enable", starttls);
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.port", port);
			
			Session session = Session.getInstance(props,
					  new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(username, password);
						}
					  });
	 
			try {
	 
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(from));
				message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(emailId));
					
				message.setSubject(subject);
				message.setContent(messageBody, "text/html");
	 
				Transport.send(message);
	 
				resetStatus = "success";	
				
 
			} catch (MessagingException e) {
				
				  resetStatus = "failed";
	 			  System.out.println("Due to certain reason it has not worked");
	 			  e.printStackTrace();	
	 		 	
			}
	      
	      return resetStatus;
	}
	
	
	
	/*public static void main(String arg[])
	{
		EmailSentUtil obj = new EmailSentUtil();
		obj.getEmailSent("abinash.raula@jirehsol.com", "Abc", "def");
	}
	*/
}
