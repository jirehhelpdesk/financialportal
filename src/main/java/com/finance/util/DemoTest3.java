package com.finance.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ResourceBundle;

public class DemoTest3 {
	
    public static String smsAPI(String msg,String number)
    {
    	String response = "";
     
		//String authkey = "A6ef59fef99577d29022194f5d5f916cd";
		String authkey = "A296ee4e2f8fdbba23023dbcb469713cf";
		String senderId = "AGRWAB";
				
    	//Multiple mobiles numbers separated by comma
    	String mobiles = number;
    	//Sender ID,While using route4 sender id should be 6 characters long.
    	//Your message to send, Add URL encoding here.
    	String message = msg;
    	//define route
    	String route="4";

    	//Prepare Url
    	URLConnection myURLConnection=null;
    	URL myURL=null;
    	BufferedReader reader=null;

    	//encoding message 
    	String encoded_message = "";
    	
		try 
		{
			encoded_message = URLEncoder.encode(message,"UTF-8");
		}
		catch (UnsupportedEncodingException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	//Send SMS API
    	String mainUrl="http://sms1.jirehsol.co.in/api/v3/?method=sms";

    	// http://URL/api/v3/?method=sms&api_key=Ad9e5XXXXXXXXXXXXX&to=997XXXXXXX&sender=INFXXX&message=Welcome%20to%messaging
    	
    	//Prepare parameter string 
    	
    	StringBuilder sbPostData= new StringBuilder(mainUrl);
    	
    	sbPostData.append("&api_key="+authkey); 
    	sbPostData.append("&to="+mobiles);
    	sbPostData.append("&sender="+senderId); 	
    	sbPostData.append("&message="+encoded_message);
    	
    	//final string
    	mainUrl = sbPostData.toString();
    	
    	
    	try
    	{
    	    //prepare connection
    	    myURL = new URL(mainUrl);
    	    myURLConnection = myURL.openConnection();
    	    myURLConnection.connect();
    	    reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
    	    //reading response 
    	   
    	    while ((response = reader.readLine()) != null) 
    	    //print response 
    	    System.out.println("Result="+response);
    	    
    	    //finally close connection
    	    reader.close();
    	} 
    	catch (IOException e) 
    	{ 
    		response = "failure";
    		e.printStackTrace();   		
    	}
    	
		return response; 

    }
    
    
    public static String smsStatus(String id)
    {
    	String response = "";
     
    	//String authkey = "A6ef59fef99577d29022194f5d5f916cd";
		String authkey = "A296ee4e2f8fdbba23023dbcb469713cf";
		String senderId = "AGRWAB";
		
    	//Prepare Url
    	URLConnection myURLConnection=null;
    	URL myURL=null;
    	BufferedReader reader=null;

    	//Send SMS API
    	String mainUrl="http://sms1.jirehsol.co.in/api/v3/?method=sms.status";

    	//  http://URL/api/v3/?method=sms.status&api_key=Ad9e5XXXXXXXXXXXXX&format=json&id=2191-1&numberinfo=1
    	
    	//Prepare parameter string 
    	StringBuilder sbPostData= new StringBuilder(mainUrl);
    	
    	sbPostData.append("&api_key="+authkey); 
    	sbPostData.append("&format=json");
    	sbPostData.append("&id="+id);
    	sbPostData.append("&numberinfo=1");
    	
    	
    	//final string
    	mainUrl = sbPostData.toString();
    	
    	
    	try
    	{
    	    //prepare connection
    	    myURL = new URL(mainUrl);
    	    myURLConnection = myURL.openConnection();
    	    myURLConnection.connect();
    	    reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
    	    //reading response 
    	   
    	    while ((response = reader.readLine()) != null) 
    	    //print response 
    	    System.out.println("Result="+response);
    	    
    	    //finally close connection
    	    reader.close();
    	} 
    	catch (IOException e) 
    	{ 
    		response = "failure";
    		e.printStackTrace();   		
    	}
    	
		return response; 

    }
    
    
    
    
    public static void main(String[] args) throws IOException {        
       
      // String response = smsAPI("This SMS API is working fine with my code","9036007662");
    	
    	String response = smsStatus("3758693093-1");
    	
    	System.out.println(response);                  
    }
    
    
    
}