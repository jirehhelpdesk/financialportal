package com.finance.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ResourceBundle;



public class DemoTest {
	
    public static String smsAPI(String msg,String number)
    {
    	String response = "";
    	
    	String user = "blrtrans";
		String password = "blr123";
		String senderId = "testms";
		
    	String mobiles = number;    
    	String message = msg;
    	
    	//Prepare Url
    	URLConnection myURLConnection=null;
    	URL myURL=null;
    	BufferedReader reader=null;

    	//encoding message 
    	String encoded_message = "";
    	
		try 
		{
			encoded_message = URLEncoder.encode(message,"UTF-8");
		}
		catch (UnsupportedEncodingException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	//Send SMS API
    	String mainUrl="http://158.69.130.136:9091/SMSPANELAPI?";

    	//Prepare parameter string 
    	StringBuilder sbPostData= new StringBuilder(mainUrl);
    	
    	sbPostData.append("username="+user);
    	sbPostData.append("&pwd="+password);
    	sbPostData.append("&msisdn="+mobiles);
    	sbPostData.append("&msg="+encoded_message);    	
    	sbPostData.append("&senderid="+senderId);

    	//final string
    	mainUrl = sbPostData.toString();
    	
    	
    	try
    	{
    	    //prepare connection
    	    myURL = new URL(mainUrl);
    	    myURLConnection = myURL.openConnection();
    	    myURLConnection.connect();
    	    reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
    	    //reading response 
    	    
    	    while ((response = reader.readLine()) != null) 
    	    //print response 
    	    System.out.println("Result="+response);
    	    
    	    //finally close connection
    	    reader.close();
    	} 
    	catch (IOException e) 
    	{ 
    		response = "failure";
    		e.printStackTrace();   		
    	}
    	
		return response; 

    }
    
    
    public static void main(String[] args) throws IOException {        
       
    	String response = smsAPI("This API is working fine.","9036007662");
    	
    	System.out.println(response);
    	
    }
    
    
   
}