package com.finance.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;


import com.finance.bean.BorrowerDetailsBean;
import com.finance.bean.LenderDetailsBean;
import com.finance.bean.LenderToBorrowerFinanceDealBean;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;


public class ReportinExcel {
	
	
	public String generateBorrowerReportInExcel(List<BorrowerDetailsBean> eModel)
	{
		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
	 	String reportDirectory = fileResource.getString("BorrowerReport");		
		
	 	String fileName = "BorrowerReport.xls";
	 	
    	try {
    		 
             File exlDirectory = new File(reportDirectory);
             if(!exlDirectory.exists())
         	 {
            	 exlDirectory.mkdirs();
         	 }
             
             File excelReport = new File(reportDirectory + "/" + fileName);
             
             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
             
             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
        	 
             //Add the created Cells to the heading of the sheet
             
             writableSheet.addCell(new Label(0, 0, "SERIAL NO"));
             writableSheet.addCell(new Label(1, 0, "FULL NAME"));
             writableSheet.addCell(new Label(2, 0, "EMAIL ID"));
             writableSheet.addCell(new Label(3, 0, "PHONE NO"));
             writableSheet.addCell(new Label(4, 0, "GENDER"));
             writableSheet.addCell(new Label(5, 0, "STATUS"));
             writableSheet.addCell(new Label(6, 0, "REGISTRATION DATE"));
             writableSheet.addCell(new Label(7, 0, "DATE OF BIRTH"));
             writableSheet.addCell(new Label(8, 0, "ALTERNATE EMAIL ID"));
             writableSheet.addCell(new Label(9, 0, "ALTERNATE PHONE NO"));
             
             writableSheet.addCell(new Label(10, 0, "PRESENT ADDRESS"));
             writableSheet.addCell(new Label(11, 0, "PERMANENT ADDRESS"));
             
             int j = 1;
             
             DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
             
             for(int i=0;i<eModel.size();i++)
             {  	
            	 writableSheet.addCell(new Number(0, j, j));
            	 writableSheet.addCell(new Label(1, j, eModel.get(i).getBorrower_name()));
            	 writableSheet.addCell(new Label(2, j, eModel.get(i).getBorrower_emailid()));
            	 writableSheet.addCell(new Label(3, j, eModel.get(i).getBorrower_phno()));
            	 writableSheet.addCell(new Label(4, j, eModel.get(i).getBorrower_gender()));
            	 writableSheet.addCell(new Label(5, j, eModel.get(i).getBorrower_status()));	            	 
            	 writableSheet.addCell(new Label(6, j, eModel.get(i).getBorrower_dob()));           	 
            	 
            	 writableSheet.addCell(new Label(7, j,df.format(eModel.get(i).getBorrower_reg_time())));	 
            	
            	 writableSheet.addCell(new Label(8, j, eModel.get(i).getBorrower_alt_emailid()));	            	 
            	 writableSheet.addCell(new Label(9, j, eModel.get(i).getBorrower_alt_phno()));   
            	 
            	 writableSheet.addCell(new Label(10, j, eModel.get(i).getBorrower_present_door()+","+eModel.get(i).getBorrower_present_building()+","+eModel.get(i).getBorrower_present_street()+","+eModel.get(i).getBorrower_present_area()+","+eModel.get(i).getBorrower_present_city()+","+eModel.get(i).getBorrower_present_pincode()));	            	 
            	 writableSheet.addCell(new Label(11, j, eModel.get(i).getBorrower_permanent_door()+","+eModel.get(i).getBorrower_permanent_building()+","+eModel.get(i).getBorrower_permanent_street()+","+eModel.get(i).getBorrower_permanent_area()+","+eModel.get(i).getBorrower_permanent_city()+","+eModel.get(i).getBorrower_permanent_pincode()));	            	 
            	 
            	 
            	 j = j + 1;
             }
                	             
             writableWorkbook.write();
             writableWorkbook.close();
  
	         } catch (IOException e) {
	             e.printStackTrace();
	         } catch (RowsExceededException e) {
	             e.printStackTrace();
	         } catch (WriteException e) {
	             e.printStackTrace();
	         }
    	   	    
    	 
		
		return fileName;
	}
	
	public String generateLenderReportInExcel(List<LenderDetailsBean> eModel)
	{		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
	 	String reportDirectory = fileResource.getString("LenderReport");		
		
	 	String fileName = "LenderReport.xls";
	 	
    	try 
    	{   		 
             File exlDirectory = new File(reportDirectory);
             if(!exlDirectory.exists())
         	 {
            	 exlDirectory.mkdirs();
         	 }
             
             File excelReport = new File(reportDirectory + "/" + fileName);
             
             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
             
             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
        	 
             //Add the created Cells to the heading of the sheet
             
             writableSheet.addCell(new Label(0, 0, "SERIAL NO"));
             writableSheet.addCell(new Label(1, 0, "LENDER NAME"));
             writableSheet.addCell(new Label(2, 0, "LENDER TYPE"));
             writableSheet.addCell(new Label(3, 0, "REPRESENTATIVE NAME"));
             writableSheet.addCell(new Label(4, 0, "REPRESENTATIVE EMAIL ID"));
             writableSheet.addCell(new Label(5, 0, "REPRESENTATIVE PHONE NO"));
             writableSheet.addCell(new Label(6, 0, "SECOND REPRESENTATIVE NAME"));
             writableSheet.addCell(new Label(7, 0, "SECOND REPRESENTATIVE EMAIL ID"));
             writableSheet.addCell(new Label(8, 0, "SECOND REPRESENTATIVE PHONE NO"));
             writableSheet.addCell(new Label(9, 0, "ADDRESS"));
             writableSheet.addCell(new Label(10, 0, "REGISTER DATE"));
             writableSheet.addCell(new Label(11, 0, "PAN NUMBER"));
             writableSheet.addCell(new Label(12, 0, "SERVICE TAX NUMBER"));
             writableSheet.addCell(new Label(13, 0, "TAN NUMBER"));
             
             int j = 1;
             
             DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
             
             for(int i=0;i<eModel.size();i++)
             {           	
            	 writableSheet.addCell(new Number(0, j, j));
            	 writableSheet.addCell(new Label(1, j, eModel.get(i).getFull_name()));
            	 writableSheet.addCell(new Label(2, j, eModel.get(i).getLender_type()));
            	 writableSheet.addCell(new Label(3, j, eModel.get(i).getFull_name()));
            	 writableSheet.addCell(new Label(4, j, eModel.get(i).getLender_emailid()));
            	 writableSheet.addCell(new Label(5, j, eModel.get(i).getLender_phno()));
            	 writableSheet.addCell(new Label(6, j, eModel.get(i).getLender_second_representative_name()));	            	 
            	 writableSheet.addCell(new Label(7, j, eModel.get(i).getLender_second_representative_email_id()));           	 
            	 writableSheet.addCell(new Label(8, j, eModel.get(i).getLender_second_representative_mobile_no()));           
            	 writableSheet.addCell(new Label(9, j,eModel.get(i).getLender_present_door()+","+eModel.get(i).getLender_present_building()+","+eModel.get(i).getLender_present_street()+","+eModel.get(i).getLender_present_area()+","+eModel.get(i).getLender_present_city()+","+eModel.get(i).getLender_present_pincode()));	 
            	 writableSheet.addCell(new Label(10, j, df.format(eModel.get(i).getLender_reg_time())));      
            	 writableSheet.addCell(new Label(11, j, eModel.get(i).getPan_number()));           
            	 writableSheet.addCell(new Label(12, j, eModel.get(i).getService_tax_number()));                  	 
            	 writableSheet.addCell(new Label(13, j, eModel.get(i).getTan_number()));           
            	 
            	 j = j + 1;
             }
                	             
             writableWorkbook.write();
             writableWorkbook.close();
  
	         } catch (IOException e) {
	             e.printStackTrace();
	         } catch (RowsExceededException e) {
	             e.printStackTrace();
	         } catch (WriteException e) {
	             e.printStackTrace();
	         }
    	   	    
    	 
		return fileName;
	}
	
	public String generateDealReportInExcel(List<LenderToBorrowerFinanceDealBean> eModel)
	{		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
	 	String reportDirectory = fileResource.getString("DealReport");		
		
	 	String fileName = "DealReport.xls";
	 	
    	try {
    		 
             File exlDirectory = new File(reportDirectory);
             if(!exlDirectory.exists())
         	 {
            	 exlDirectory.mkdirs();
         	 }
             
             File excelReport = new File(reportDirectory + "/" + fileName);
             
             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
             
             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
        	 
             //Add the created Cells to the heading of the sheet
             
             writableSheet.addCell(new Label(0, 0, "SERIAL NO"));
             writableSheet.addCell(new Label(1, 0, "FULL NAME"));
             writableSheet.addCell(new Label(2, 0, "EMAIL ID"));
             writableSheet.addCell(new Label(3, 0, "PHONE NO"));
             writableSheet.addCell(new Label(4, 0, "CONTACT FOR"));
             writableSheet.addCell(new Label(5, 0, "SUBJECT"));
             writableSheet.addCell(new Label(6, 0, "MESSAGE"));
             writableSheet.addCell(new Label(7, 0, "CONTACT DATE"));
             
             int j = 1;
             
             for(int i=0;i<eModel.size();i++)
             {  	
            	 writableSheet.addCell(new Number(0, j, j));
            	 writableSheet.addCell(new Label(1, j, eModel.get(i).getAmount_dispouse()));
            	 
            	 DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            	 Date regDate = eModel.get(i).getAmount_dispouse_date();        
            	 String reportDate = df.format(regDate);

            	 writableSheet.addCell(new Label(7, j,reportDate));	 
            	
            	 j = j + 1;
             }
                	             
             writableWorkbook.write();
             writableWorkbook.close();
  
	         } catch (IOException e) {
	             e.printStackTrace();
	         } catch (RowsExceededException e) {
	             e.printStackTrace();
	         } catch (WriteException e) {
	             e.printStackTrace();
	         }
    	   	    
    	 
		
		return fileName;
	}
	
	
	/*public static void main(String[] args)
	{		
		generateConsolidatedReport obj = new generateConsolidatedReport();
		
	    System.out.println(obj.createConsolidatedReport());
	}*/

}
