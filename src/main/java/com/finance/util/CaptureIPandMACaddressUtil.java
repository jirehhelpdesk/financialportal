package com.finance.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;


public class CaptureIPandMACaddressUtil {

	InetAddress ip;

	public String getIPddress(HttpServletRequest request) throws SocketException, UnknownHostException
	{
		
		HttpServletRequest httpRequest = (HttpServletRequest)request;
		String userIpAddress = httpRequest.getRemoteAddr(); 		
		if(userIpAddress.equals("0:0:0:0:0:0:0:1"))
		{
			String ipAddress="";
			ip = InetAddress.getLocalHost();
			ipAddress += ""+ip.getHostAddress();		
			return ipAddress;
		}
		else
		{			
			return userIpAddress;
		}
		
	}
	 
	
	
	// Capturing MacAddress
	
	public String getMACddress() throws SocketException
	{
		InetAddress ipadd;		
		String macAddress = "";
		try 
	    {		
			ipadd = InetAddress.getLocalHost();
			NetworkInterface network = NetworkInterface.getByInetAddress(ipadd);						 
			byte[] mac = network.getHardwareAddress();						 					 
			StringBuilder sb = new StringBuilder();
			for (int address = 0; address < mac.length; address++) 
			{
				sb.append(String.format("%02X%s", mac[address],(address < mac.length - 1) ? "-" : ""));		
			}
			
			macAddress += sb.toString();	       
		} 
	    catch (SocketException | UnknownHostException e)
	    {						 
			e.printStackTrace();						 
		}
		
		 return macAddress;
	}
	
	
	
	
}
