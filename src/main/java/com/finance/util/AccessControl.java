package com.finance.util;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

public class AccessControl {

	public static boolean isValidSession(HttpServletRequest request) {
		StringBuilder currUserId = new StringBuilder();
		
		currUserId.append("JIR").append(request.getSession().getId()).append("K");
		String currUserAttr = (String) request.getSession().getAttribute("USER_SESSION_ID");
		
		if (StringUtils.isNotBlank(currUserAttr) && (currUserAttr.equals(currUserId.toString()))) {
			return true;
		} else {
			return false;
		}
	}
}
