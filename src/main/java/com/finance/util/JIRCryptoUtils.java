package com.finance.util;

import java.io.File;

import org.springframework.scheduling.quartz.QuartzJobBean;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;


public class JIRCryptoUtils {
	
	private static final String ALGORITHM = "AES";
	private static final String TRANSFORMATION = "AES";

	static String key = "KYC1KEY100000001";
	
	public static void encrypt( File inputFile, File outputFile) throws Exception  
	{
		doCrypto(Cipher.ENCRYPT_MODE, inputFile, outputFile);
	}

	public static void decrypt( File inputFile, File outputFile) throws Exception   {
		
		doCrypto(Cipher.DECRYPT_MODE, inputFile, outputFile);
	}

	@SuppressWarnings("resource")
	private static void doCrypto(int cipherMode, File inputFile, File outputFile) throws Exception   {
		
		// To encrypt a file
		
		if(cipherMode==1)
    	{
    		if ((inputFile.length()) % 16 != 0) 
			{	
    			
    			FileInputStream inputStream = null;
    			FileOutputStream outputStream = null;
    			
    			try{						
						Key secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
						Cipher cipher = Cipher.getInstance(TRANSFORMATION);
						cipher.init(cipherMode, secretKey);
						
						inputStream = new FileInputStream(inputFile);
						byte[] inputBytes = new byte[(int) inputFile.length()];
						
						inputStream.read(inputBytes);
						
						byte[] outputBytes = cipher.doFinal(inputBytes);
	
						outputStream = new FileOutputStream(outputFile);
						outputStream.write(outputBytes);

						inputStream.close();
						outputStream.close();							
					} 
					catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException
							| IOException ex) 
    			    {
						throw new Exception("Error encrypting/decrypting file", ex);
					}    			
			}
    		else
    		{
    					
    		}   	  		   		
    	}
		
		
		// To decrypt a file
		
		if(cipherMode==2)
    	{
    		if ((inputFile.length()) % 16 == 0) 
			{
    			
    			FileInputStream inputStream = null;
    			FileOutputStream outputStream = null;
    			
				try {						
						Key secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
						Cipher cipher = Cipher.getInstance(TRANSFORMATION);
						cipher.init(cipherMode, secretKey);
	
						inputStream = new FileInputStream(inputFile);
						byte[] inputBytes = new byte[(int) inputFile.length()];
												
						inputStream.read(inputBytes);
	
						byte[] outputBytes = cipher.doFinal(inputBytes);
	
						outputStream = new FileOutputStream(outputFile);
						outputStream.write(outputBytes);
	
						inputStream.close();
						outputStream.close();
							
					} 
					catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException
							| IOException ex) {
						throw new Exception("Error encrypting/decrypting file", ex);
					}					
			}
    		else
    		{
    			//System.out.println(inputFile.getName() + " is already encryptd.");
    		}
    	  
    	}
	
	}
	
	
	
	public static void main(String arg[]) throws Exception
	{
		 File inputFile = new File("E:/aaaaa/enc/0C4AD6C4D98BA5E77DEFA5BFA686CB20.D63C0");
		 File outputFile = new File("E:/aaaaa/dec/0C4AD6C4D98BA5E77DEFA5BFA686CB20.doc");
		 
		 
		 
		 while((inputFile.length()) % 16 == 0)
		{
			 doCrypto(2, inputFile, outputFile);
		}
		
		
		
	}
	
	
}
