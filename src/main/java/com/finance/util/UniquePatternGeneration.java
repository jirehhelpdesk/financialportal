package com.finance.util;

import static java.lang.Math.round;
import static java.lang.Math.random;
import static java.lang.Math.pow;
import static java.lang.Math.abs;
import static java.lang.Math.min;
import static org.apache.commons.lang.StringUtils.*;

public class UniquePatternGeneration {
	
  public static String generateUniqueCode(String text) {
	  
    StringBuffer sb = new StringBuffer();
    int length = text.length();
    for (int i = length; i > 0; i -= 12) {
      int n = min(12, abs(i));
      sb.append(leftPad(Long.toString(round(random() * pow(36, n)), 36), n, '0'));
    }
    return sb.toString();
  }
  
  
 /* public static void main(String arg[])
  {
	  System.out.println("Unique String is "+generateUniqueCode("raulaabinash@gmail.com"));
  }*/
  
} 