package com.finance.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class RecordScedulerTime {
	
	static String calendarTime;
	
	@SuppressWarnings("static-access")
	public String getSchedulerStartTime()
	{		
		String formattedDate = calendarTime;
		 			
		return formattedDate;
	}

	@SuppressWarnings("static-access")
	public String getSchedulerEndTime() throws ParseException
	{
		SimpleDateFormat curFormater = new SimpleDateFormat("dd/MM/yy HH:mm:ss"); 
		Date dateObj = curFormater.parse(calendarTime); 
		Calendar calendar = Calendar.getInstance();
		calendar .setTime(dateObj);
		
		// Substract 2 hour from the current time
        //calendar.add(Calendar.HOUR, -2);
        
		calendar.add(calendar.MINUTE, 30);
        
        // Add 300 seconds to the calendar time
        // calendar.add(Calendar.SECOND, 300);
        
        String endTime = calendar.getTime().toString();
        
		return endTime;
	}
	
	
}
