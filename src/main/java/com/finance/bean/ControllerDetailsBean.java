package com.finance.bean;

import java.util.Date;


public class ControllerDetailsBean {

	
	private int controller_id;
	private String controller_name;
	private String controller_user_id;
	private String controller_Phno;
	private String controller_password;
	private Date controller_cr_date;
	private String controller_access_status;
	private String controller_photo;
	private String controller_designation;

	
		
	public int getController_id() {
		return controller_id;
	}

	public void setController_id(int controller_id) {
		this.controller_id = controller_id;
	}

	public String getController_name() {
		return controller_name;
	}

	public void setController_name(String controller_name) {
		this.controller_name = controller_name;
	}

	public String getController_user_id() {
		return controller_user_id;
	}

	public void setController_user_id(String controller_user_id) {
		this.controller_user_id = controller_user_id;
	}

	
	public String getController_Phno() {
		return controller_Phno;
	}

	public void setController_Phno(String controller_Phno) {
		this.controller_Phno = controller_Phno;
	}

	public String getController_password() {
		return controller_password;
	}

	public void setController_password(String controller_password) {
		this.controller_password = controller_password;
	}

	
	public Date getController_cr_date() {
		return controller_cr_date;
	}

	public void setController_cr_date(Date controller_cr_date) {
		this.controller_cr_date = controller_cr_date;
	}

	public String getController_access_status() {
		return controller_access_status;
	}

	public void setController_access_status(String controller_access_status) {
		this.controller_access_status = controller_access_status;
	}

	public String getController_photo() {
		return controller_photo;
	}

	public void setController_photo(String controller_photo) {
		this.controller_photo = controller_photo;
	}

	public String getController_designation() {
		return controller_designation;
	}

	public void setController_designation(String controller_designation) {
		this.controller_designation = controller_designation;
	}
	
	
}
