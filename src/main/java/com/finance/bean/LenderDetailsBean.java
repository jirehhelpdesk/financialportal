package com.finance.bean;

import java.util.Date;

import javax.persistence.Column;

public class LenderDetailsBean {

	
    private int lender_id;
    
    
    private String lender_type;
	private String full_name;
	private String lender_emailid;
	private String lender_phno;
	private String lender_password;
	private Date lender_reg_time;
	private String lender_status;
	
	
	
	private String lender_photo;
	private Date cr_dare;
	
	

	 private String lender_representative_name;
	 
	 private String lender_second_representative_name;
	 private String lender_second_representative_email_id;
	 private String lender_second_representative_mobile_no;
		
	// Present Address Pattern Update	
	
	 private String lender_present_door;
	 private String lender_present_building;
	 private String lender_present_street;
	 private String lender_present_area;
	 private String lender_present_city;
	 private String lender_present_pincode;
	
	 private String pan_number;
	 private String service_tax_number;
	 private String tan_number;
		
		
	public int getLender_id() {
		return lender_id;
	}
		
	public void setLender_id(int lender_id) {
		this.lender_id = lender_id;
	}
	
	
	public String getLender_type() {
		return lender_type;
	}
	public void setLender_type(String lender_type) {
		this.lender_type = lender_type;
	}
	public String getFull_name() {
		return full_name;
	}
	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}
	public String getLender_emailid() {
		return lender_emailid;
	}
	public void setLender_emailid(String lender_emailid) {
		this.lender_emailid = lender_emailid;
	}
	public String getLender_phno() {
		return lender_phno;
	}
	public void setLender_phno(String lender_phno) {
		this.lender_phno = lender_phno;
	}
	public String getLender_password() {
		return lender_password;
	}
	public void setLender_password(String lender_password) {
		this.lender_password = lender_password;
	}
	public Date getLender_reg_time() {
		return lender_reg_time;
	}
	public void setLender_reg_time(Date lender_reg_time) {
		this.lender_reg_time = lender_reg_time;
	}
	public String getLender_status() {
		return lender_status;
	}
	public void setLender_status(String lender_status) {
		this.lender_status = lender_status;
	}
	
	public String getLender_present_pincode() {
		return lender_present_pincode;
	}
	public void setLender_present_pincode(String lender_present_pincode) {
		this.lender_present_pincode = lender_present_pincode;
	}
	
	public String getLender_photo() {
		return lender_photo;
	}
	public void setLender_photo(String lender_photo) {
		this.lender_photo = lender_photo;
	}
	public Date getCr_dare() {
		return cr_dare;
	}
	public void setCr_dare(Date cr_dare) {
		this.cr_dare = cr_dare;
	}

	public String getLender_representative_name() {
		return lender_representative_name;
	}

	public void setLender_representative_name(String lender_representative_name) {
		this.lender_representative_name = lender_representative_name;
	}

	public String getLender_present_door() {
		return lender_present_door;
	}

	public void setLender_present_door(String lender_present_door) {
		this.lender_present_door = lender_present_door;
	}

	public String getLender_present_building() {
		return lender_present_building;
	}

	public void setLender_present_building(String lender_present_building) {
		this.lender_present_building = lender_present_building;
	}

	public String getLender_present_street() {
		return lender_present_street;
	}

	public void setLender_present_street(String lender_present_street) {
		this.lender_present_street = lender_present_street;
	}

	public String getLender_present_area() {
		return lender_present_area;
	}

	public void setLender_present_area(String lender_present_area) {
		this.lender_present_area = lender_present_area;
	}

	public String getLender_present_city() {
		return lender_present_city;
	}

	public void setLender_present_city(String lender_present_city) {
		this.lender_present_city = lender_present_city;
	}

	public String getLender_second_representative_name() {
		return lender_second_representative_name;
	}

	public void setLender_second_representative_name(
			String lender_second_representative_name) {
		this.lender_second_representative_name = lender_second_representative_name;
	}

	public String getLender_second_representative_email_id() {
		return lender_second_representative_email_id;
	}

	public void setLender_second_representative_email_id(
			String lender_second_representative_email_id) {
		this.lender_second_representative_email_id = lender_second_representative_email_id;
	}

	public String getLender_second_representative_mobile_no() {
		return lender_second_representative_mobile_no;
	}

	public void setLender_second_representative_mobile_no(
			String lender_second_representative_mobile_no) {
		this.lender_second_representative_mobile_no = lender_second_representative_mobile_no;
	}

	public String getPan_number() {
		return pan_number;
	}

	public void setPan_number(String pan_number) {
		this.pan_number = pan_number;
	}

	public String getService_tax_number() {
		return service_tax_number;
	}

	public void setService_tax_number(String service_tax_number) {
		this.service_tax_number = service_tax_number;
	}

	public String getTan_number() {
		return tan_number;
	}

	public void setTan_number(String tan_number) {
		this.tan_number = tan_number;
	}

	
	
	
}
