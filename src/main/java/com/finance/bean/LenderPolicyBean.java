package com.finance.bean;


import java.util.Date;

import javax.persistence.Column;

public class LenderPolicyBean {

	private String lender_name;
	private String lender_emailId;
	
	private int policy_id;
	private int lender_id;
	private String policy_name;
	private String policy_category;
	private String policy_type;
	private long policy_min_amount;
	private long policy_max_amount;
	private double policy_interest_rate;
	private String policy_broucher;
	private int policy_min_duration;
	private int policy_max_duration;
	private String policy_required_doc;
	private String policy_approval_status;
	private String policy_intialization_status;
	private Date cr_date;

    private Date policy_start_date;
	private Date policy_end_date;
	private String policy_applicable_location;
	
	private String policy_age_limit;
	private String policy_income_limit;
	private String policy_for_gender;
	
	
	public String getLender_name() {
		return lender_name;
	}

	public void setLender_name(String lender_name) {
		this.lender_name = lender_name;
	}

	public String getLender_emailId() {
		return lender_emailId;
	}

	public void setLender_emailId(String lender_emailId) {
		this.lender_emailId = lender_emailId;
	}

	public int getPolicy_id() {
		return policy_id;
	}

	public void setPolicy_id(int policy_id) {
		this.policy_id = policy_id;
	}

	public int getLender_id() {
		return lender_id;
	}

	public void setLender_id(int lender_id) {
		this.lender_id = lender_id;
	}

	public String getPolicy_name() {
		return policy_name;
	}

	public void setPolicy_name(String policy_name) {
		this.policy_name = policy_name;
	}

	public String getPolicy_category() {
		return policy_category;
	}

	public void setPolicy_category(String policy_category) {
		this.policy_category = policy_category;
	}

	public String getPolicy_type() {
		return policy_type;
	}

	public void setPolicy_type(String policy_type) {
		this.policy_type = policy_type;
	}

	public long getPolicy_min_amount() {
		return policy_min_amount;
	}

	public void setPolicy_min_amount(long policy_min_amount) {
		this.policy_min_amount = policy_min_amount;
	}

	public long getPolicy_max_amount() {
		return policy_max_amount;
	}

	public void setPolicy_max_amount(long policy_max_amount) {
		this.policy_max_amount = policy_max_amount;
	}

	public double getPolicy_interest_rate() {
		return policy_interest_rate;
	}

	public void setPolicy_interest_rate(double policy_interest_rate) {
		this.policy_interest_rate = policy_interest_rate;
	}

	public String getPolicy_broucher() {
		return policy_broucher;
	}

	public void setPolicy_broucher(String policy_broucher) {
		this.policy_broucher = policy_broucher;
	}

	public int getPolicy_min_duration() {
		return policy_min_duration;
	}

	public void setPolicy_min_duration(int policy_min_duration) {
		this.policy_min_duration = policy_min_duration;
	}

	public int getPolicy_max_duration() {
		return policy_max_duration;
	}

	public void setPolicy_max_duration(int policy_max_duration) {
		this.policy_max_duration = policy_max_duration;
	}

	public String getPolicy_required_doc() {
		return policy_required_doc;
	}

	public void setPolicy_required_doc(String policy_required_doc) {
		this.policy_required_doc = policy_required_doc;
	}

	public String getPolicy_approval_status() {
		return policy_approval_status;
	}

	public void setPolicy_approval_status(String policy_approval_status) {
		this.policy_approval_status = policy_approval_status;
	}
	
	public String getPolicy_intialization_status() {
		return policy_intialization_status;
	}

	public void setPolicy_intialization_status(String policy_intialization_status) {
		this.policy_intialization_status = policy_intialization_status;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public Date getPolicy_start_date() {
		return policy_start_date;
	}

	public void setPolicy_start_date(Date policy_start_date) {
		this.policy_start_date = policy_start_date;
	}

	public Date getPolicy_end_date() {
		return policy_end_date;
	}

	public void setPolicy_end_date(Date policy_end_date) {
		this.policy_end_date = policy_end_date;
	}

	public String getPolicy_applicable_location() {
		return policy_applicable_location;
	}

	public void setPolicy_applicable_location(String policy_applicable_location) {
		this.policy_applicable_location = policy_applicable_location;
	}

	public String getPolicy_age_limit() {
		return policy_age_limit;
	}

	public void setPolicy_age_limit(String policy_age_limit) {
		this.policy_age_limit = policy_age_limit;
	}

	public String getPolicy_income_limit() {
		return policy_income_limit;
	}

	public void setPolicy_income_limit(String policy_income_limit) {
		this.policy_income_limit = policy_income_limit;
	}

	public String getPolicy_for_gender() {
		return policy_for_gender;
	}

	public void setPolicy_for_gender(String policy_for_gender) {
		this.policy_for_gender = policy_for_gender;
	}

}
