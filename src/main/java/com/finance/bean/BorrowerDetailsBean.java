package com.finance.bean;

import java.util.Date;

import javax.persistence.Column;

public class BorrowerDetailsBean {

    private int borrower_id;
    
	private String borrower_first_name;
	private String borrower_last_name;
	private String borrower_gender;
	private String borrower_name;
	private String borrower_emailid;
	private String borrower_phno;
	private String borrower_password;
	private Date borrower_reg_time;
	private String borrower_status;
	private String borrower_pan_id;
	
    private String borrower_profile_photo;
	private String borrower_present_address;
	private String borrower_present_pincode;
	private String borrower_permanent_address;
	private String borrower_permanent_pincode;
	private String borrower_dob;
	private String borrower_identity_id_type;
	private String borrower_identity_card_id;
	private Date cr_date;
	
	private String borrower_alt_phno;
	private String borrower_alt_emailid;
	
	
	  // Present Address Pattern Update	
	
		private String borrower_present_door;
		private String borrower_present_building;
		private String borrower_present_street;
		private String borrower_present_area;
		private String borrower_present_city;
		
		
	    // Permanent Address Pattern Update	
		
		private String borrower_permanent_door;
		private String borrower_permanent_building;
		private String borrower_permanent_street;
		private String borrower_permanent_area;
		private String borrower_permanent_city;
		
	
	public int getBorrower_id() {
		return borrower_id;
	}
	public void setBorrower_id(int borrower_id) {
		this.borrower_id = borrower_id;
	}
	public String getBorrower_first_name() {
		return borrower_first_name;
	}
	public void setBorrower_first_name(String borrower_first_name) {
		this.borrower_first_name = borrower_first_name;
	}
	public String getBorrower_last_name() {
		return borrower_last_name;
	}
	public void setBorrower_last_name(String borrower_last_name) {
		this.borrower_last_name = borrower_last_name;
	}	
	public String getBorrower_gender() {
		return borrower_gender;
	}
	public void setBorrower_gender(String borrower_gender) {
		this.borrower_gender = borrower_gender;
	}
	public String getBorrower_name() {
		return borrower_name;
	}
	public void setBorrower_name(String borrower_name) {
		this.borrower_name = borrower_name;
	}
	public String getBorrower_emailid() {
		return borrower_emailid;
	}
	public void setBorrower_emailid(String borrower_emailid) {
		this.borrower_emailid = borrower_emailid;
	}
	public String getBorrower_phno() {
		return borrower_phno;
	}
	public void setBorrower_phno(String borrower_phno) {
		this.borrower_phno = borrower_phno;
	}
	public String getBorrower_password() {
		return borrower_password;
	}
	public void setBorrower_password(String borrower_password) {
		this.borrower_password = borrower_password;
	}
	public Date getBorrower_reg_time() {
		return borrower_reg_time;
	}
	public void setBorrower_reg_time(Date borrower_reg_time) {
		this.borrower_reg_time = borrower_reg_time;
	}
	public String getBorrower_status() {
		return borrower_status;
	}
	public void setBorrower_status(String borrower_status) {
		this.borrower_status = borrower_status;
	}
	public String getBorrower_pan_id() {
		return borrower_pan_id;
	}
	public void setBorrower_pan_id(String borrower_pan_id) {
		this.borrower_pan_id = borrower_pan_id;
	}
	
	
	
	public String getBorrower_profile_photo() {
		return borrower_profile_photo;
	}
	public void setBorrower_profile_photo(String borrower_profile_photo) {
		this.borrower_profile_photo = borrower_profile_photo;
	}
	public String getBorrower_present_address() {
		return borrower_present_address;
	}
	public void setBorrower_present_address(String borrower_present_address) {
		this.borrower_present_address = borrower_present_address;
	}
	public String getBorrower_present_pincode() {
		return borrower_present_pincode;
	}
	public void setBorrower_present_pincode(String borrower_present_pincode) {
		this.borrower_present_pincode = borrower_present_pincode;
	}
	public String getBorrower_permanent_address() {
		return borrower_permanent_address;
	}
	public void setBorrower_permanent_address(String borrower_permanent_address) {
		this.borrower_permanent_address = borrower_permanent_address;
	}
	public String getBorrower_permanent_pincode() {
		return borrower_permanent_pincode;
	}
	public void setBorrower_permanent_pincode(String borrower_permanent_pincode) {
		this.borrower_permanent_pincode = borrower_permanent_pincode;
	}
	public String getBorrower_dob() {
		return borrower_dob;
	}
	public void setBorrower_dob(String borrower_dob) {
		this.borrower_dob = borrower_dob;
	}
	public String getBorrower_identity_id_type() {
		return borrower_identity_id_type;
	}
	public void setBorrower_identity_id_type(String borrower_identity_id_type) {
		this.borrower_identity_id_type = borrower_identity_id_type;
	}
	public String getBorrower_identity_card_id() {
		return borrower_identity_card_id;
	}
	public void setBorrower_identity_card_id(String borrower_identity_card_id) {
		this.borrower_identity_card_id = borrower_identity_card_id;
	}
	public Date getCr_date() {
		return cr_date;
	}
	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}
	public String getBorrower_present_door() {
		return borrower_present_door;
	}
	public void setBorrower_present_door(String borrower_present_door) {
		this.borrower_present_door = borrower_present_door;
	}
	public String getBorrower_present_building() {
		return borrower_present_building;
	}
	public void setBorrower_present_building(String borrower_present_building) {
		this.borrower_present_building = borrower_present_building;
	}
	public String getBorrower_present_street() {
		return borrower_present_street;
	}
	public void setBorrower_present_street(String borrower_present_street) {
		this.borrower_present_street = borrower_present_street;
	}
	public String getBorrower_present_area() {
		return borrower_present_area;
	}
	public void setBorrower_present_area(String borrower_present_area) {
		this.borrower_present_area = borrower_present_area;
	}
	public String getBorrower_present_city() {
		return borrower_present_city;
	}
	public void setBorrower_present_city(String borrower_present_city) {
		this.borrower_present_city = borrower_present_city;
	}
	public String getBorrower_permanent_door() {
		return borrower_permanent_door;
	}
	public void setBorrower_permanent_door(String borrower_permanent_door) {
		this.borrower_permanent_door = borrower_permanent_door;
	}
	public String getBorrower_permanent_building() {
		return borrower_permanent_building;
	}
	public void setBorrower_permanent_building(String borrower_permanent_building) {
		this.borrower_permanent_building = borrower_permanent_building;
	}
	public String getBorrower_permanent_street() {
		return borrower_permanent_street;
	}
	public void setBorrower_permanent_street(String borrower_permanent_street) {
		this.borrower_permanent_street = borrower_permanent_street;
	}
	public String getBorrower_permanent_area() {
		return borrower_permanent_area;
	}
	public void setBorrower_permanent_area(String borrower_permanent_area) {
		this.borrower_permanent_area = borrower_permanent_area;
	}
	public String getBorrower_permanent_city() {
		return borrower_permanent_city;
	}
	public void setBorrower_permanent_city(String borrower_permanent_city) {
		this.borrower_permanent_city = borrower_permanent_city;
	}
	public String getBorrower_alt_phno() {
		return borrower_alt_phno;
	}
	public void setBorrower_alt_phno(String borrower_alt_phno) {
		this.borrower_alt_phno = borrower_alt_phno;
	}
	public String getBorrower_alt_emailid() {
		return borrower_alt_emailid;
	}
	public void setBorrower_alt_emailid(String borrower_alt_emailid) {
		this.borrower_alt_emailid = borrower_alt_emailid;
	}
	
	
	
	
}
