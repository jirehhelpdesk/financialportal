package com.finance.bean;

import java.util.Date;

import javax.persistence.Column;


public class LenderToBorrowerFinanceDealBean {

	private int deal_id;
	
	
	private int lender_id;
	private int borrower_id;
	private String deal_number;
	private String policy_id;
	
	
	private String policy_name;
	private String policy_type;
	private double deal_interest_rate;
	
	private long deal_amount;
	private int deal_duration;
	
	
	private String income_type;
	private String income_annualy;
	private String pending_reject_reason;
	private String processing_reject_reason;
	private String note_to_lender;
	private String scanned_file_name;
	
	
	private String deal_status;
	private Date deal_initiated_date;
	private Date deal_finalize_date;
	private String lender_status;
	private String extra_info;
	
	
	private String search_from_date;
	private String search_to_date;
	
	private String house_status;
	private String four_wheeler_status;
	private String two_wheeler_status;
	
    private String amount_dispouse;
	private Date amount_dispouse_date;
	
	public int getDeal_id() {
		return deal_id;
	}
	public void setDeal_id(int deal_id) {
		this.deal_id = deal_id;
	}
	public int getLender_id() {
		return lender_id;
	}
	public void setLender_id(int lender_id) {
		this.lender_id = lender_id;
	}
	public int getBorrower_id() {
		return borrower_id;
	}
	public void setBorrower_id(int borrower_id) {
		this.borrower_id = borrower_id;
	}
	public String getDeal_number() {
		return deal_number;
	}
	public void setDeal_number(String deal_number) {
		this.deal_number = deal_number;
	}
	public String getPolicy_id() {
		return policy_id;
	}
	public void setPolicy_id(String policy_id) {
		this.policy_id = policy_id;
	}
	public String getPolicy_name() {
		return policy_name;
	}
	public void setPolicy_name(String policy_name) {
		this.policy_name = policy_name;
	}
	public String getPolicy_type() {
		return policy_type;
	}
	public void setPolicy_type(String policy_type) {
		this.policy_type = policy_type;
	}
	public double getDeal_interest_rate() {
		return deal_interest_rate;
	}
	public void setDeal_interest_rate(double deal_interest_rate) {
		this.deal_interest_rate = deal_interest_rate;
	}
	public long getDeal_amount() {
		return deal_amount;
	}
	public void setDeal_amount(long deal_amount) {
		this.deal_amount = deal_amount;
	}
	public int getDeal_duration() {
		return deal_duration;
	}
	public void setDeal_duration(int deal_duration) {
		this.deal_duration = deal_duration;
	}
	public String getIncome_type() {
		return income_type;
	}
	public void setIncome_type(String income_type) {
		this.income_type = income_type;
	}
	public String getIncome_annualy() {
		return income_annualy;
	}
	public void setIncome_annualy(String income_annualy) {
		this.income_annualy = income_annualy;
	}
	
	public String getPending_reject_reason() {
		return pending_reject_reason;
	}
	public void setPending_reject_reason(String pending_reject_reason) {
		this.pending_reject_reason = pending_reject_reason;
	}
	public String getProcessing_reject_reason() {
		return processing_reject_reason;
	}
	public void setProcessing_reject_reason(String processing_reject_reason) {
		this.processing_reject_reason = processing_reject_reason;
	}
	public String getNote_to_lender() {
		return note_to_lender;
	}
	public void setNote_to_lender(String note_to_lender) {
		this.note_to_lender = note_to_lender;
	}
	public String getScanned_file_name() {
		return scanned_file_name;
	}
	public void setScanned_file_name(String scanned_file_name) {
		this.scanned_file_name = scanned_file_name;
	}
	public String getDeal_status() {
		return deal_status;
	}
	public void setDeal_status(String deal_status) {
		this.deal_status = deal_status;
	}
	public Date getDeal_initiated_date() {
		return deal_initiated_date;
	}
	public void setDeal_initiated_date(Date deal_initiated_date) {
		this.deal_initiated_date = deal_initiated_date;
	}
	public Date getDeal_finalize_date() {
		return deal_finalize_date;
	}
	public void setDeal_finalize_date(Date deal_finalize_date) {
		this.deal_finalize_date = deal_finalize_date;
	}
	public String getLender_status() {
		return lender_status;
	}

	public void setLender_status(String lender_status) {
		this.lender_status = lender_status;
	}

	public String getExtra_info() {
		return extra_info;
	}
	public void setExtra_info(String extra_info) {
		this.extra_info = extra_info;
	}

	
	
	private String borrower_name;
	private String borrower_emailid;
	private String borrower_phno;
	private String gender;
	private String borrower_profile_photo;
	private String borrower_city;
	
	private String lender_name;
	private String lender_emailid;
	private String lender_phno;
	private String lender_profile_photo;




	public String getBorrower_name() {
		return borrower_name;
	}
	public void setBorrower_name(String borrower_name) {
		this.borrower_name = borrower_name;
	}
	public String getBorrower_emailid() {
		return borrower_emailid;
	}
	public void setBorrower_emailid(String borrower_emailid) {
		this.borrower_emailid = borrower_emailid;
	}
	public String getBorrower_phno() {
		return borrower_phno;
	}
	public void setBorrower_phno(String borrower_phno) {
		this.borrower_phno = borrower_phno;
	}
	public String getLender_name() {
		return lender_name;
	}
	public void setLender_name(String lender_name) {
		this.lender_name = lender_name;
	}
	public String getLender_emailid() {
		return lender_emailid;
	}
	public void setLender_emailid(String lender_emailid) {
		this.lender_emailid = lender_emailid;
	}
	public String getLender_phno() {
		return lender_phno;
	}
	public void setLender_phno(String lender_phno) {
		this.lender_phno = lender_phno;
	}
	
	
	
	public String getBorrower_profile_photo() {
		return borrower_profile_photo;
	}
	public void setBorrower_profile_photo(String borrower_profile_photo) {
		this.borrower_profile_photo = borrower_profile_photo;
	}
	public String getLender_profile_photo() {
		return lender_profile_photo;
	}
	public void setLender_profile_photo(String lender_profile_photo) {
		this.lender_profile_photo = lender_profile_photo;
	}
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	
	
	
	public String getBorrower_city() {
		return borrower_city;
	}
	public void setBorrower_city(String borrower_city) {
		this.borrower_city = borrower_city;
	}
	public String getSearch_from_date() {
		return search_from_date;
	}

	public void setSearch_from_date(String search_from_date) {
		this.search_from_date = search_from_date;
	}

	public String getSearch_to_date() {
		return search_to_date;
	}

	public void setSearch_to_date(String search_to_date) {
		this.search_to_date = search_to_date;
	}
	public String getHouse_status() {
		return house_status;
	}
	public void setHouse_status(String house_status) {
		this.house_status = house_status;
	}
	public String getFour_wheeler_status() {
		return four_wheeler_status;
	}
	public void setFour_wheeler_status(String four_wheeler_status) {
		this.four_wheeler_status = four_wheeler_status;
	}
	public String getTwo_wheeler_status() {
		return two_wheeler_status;
	}
	public void setTwo_wheeler_status(String two_wheeler_status) {
		this.two_wheeler_status = two_wheeler_status;
	}
	public String getAmount_dispouse() {
		return amount_dispouse;
	}
	public void setAmount_dispouse(String amount_dispouse) {
		this.amount_dispouse = amount_dispouse;
	}
	public Date getAmount_dispouse_date() {
		return amount_dispouse_date;
	}
	public void setAmount_dispouse_date(Date amount_dispouse_date) {
		this.amount_dispouse_date = amount_dispouse_date;
	}

	
}
