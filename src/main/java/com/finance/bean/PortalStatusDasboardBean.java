package com.finance.bean;

import java.util.List;

public class PortalStatusDasboardBean {
	
	
	private long noOfBorrower;
	private long noOfLender;
	private long noOfController;
	
	private long noOfPolicy;
	private long noOfPendingPolicy;
	private long noOfApprovedPolicy;
	private long noOfClosedPolicy;
	
	private long noOfAppliedDeals;
	private long noOfFinalizedDeals;	
	private long noOfProcessingDeals;
	private long noOfOnholdDeals;
	
	
	private List<String> listOfPolicyType;
	
	
	private long noOfOnSearchLoans;
	
	
	
	public long getNoOfBorrower() {
		return noOfBorrower;
	}
	public void setNoOfBorrower(long noOfBorrower) {
		this.noOfBorrower = noOfBorrower;
	}
	public long getNoOfLender() {
		return noOfLender;
	}
	public void setNoOfLender(long noOfLender) {
		this.noOfLender = noOfLender;
	}
	public long getNoOfController() {
		return noOfController;
	}
	public void setNoOfController(long noOfController) {
		this.noOfController = noOfController;
	}
	public long getNoOfPolicy() {
		return noOfPolicy;
	}
	public void setNoOfPolicy(long noOfPolicy) {
		this.noOfPolicy = noOfPolicy;
	}	
	public long getNoOfPendingPolicy() {
		return noOfPendingPolicy;
	}
	public void setNoOfPendingPolicy(long noOfPendingPolicy) {
		this.noOfPendingPolicy = noOfPendingPolicy;
	}
	public long getNoOfApprovedPolicy() {
		return noOfApprovedPolicy;
	}
	public void setNoOfApprovedPolicy(long noOfApprovedPolicy) {
		this.noOfApprovedPolicy = noOfApprovedPolicy;
	}
	public long getNoOfClosedPolicy() {
		return noOfClosedPolicy;
	}
	public void setNoOfClosedPolicy(long noOfClosedPolicy) {
		this.noOfClosedPolicy = noOfClosedPolicy;
	}
	public long getNoOfAppliedDeals() {
		return noOfAppliedDeals;
	}
	public void setNoOfAppliedDeals(long noOfAppliedDeals) {
		this.noOfAppliedDeals = noOfAppliedDeals;
	}
	public long getNoOfFinalizedDeals() {
		return noOfFinalizedDeals;
	}
	public void setNoOfFinalizedDeals(long noOfFinalizedDeals) {
		this.noOfFinalizedDeals = noOfFinalizedDeals;
	}
	public long getNoOfProcessingDeals() {
		return noOfProcessingDeals;
	}
	public void setNoOfProcessingDeals(long noOfProcessingDeals) {
		this.noOfProcessingDeals = noOfProcessingDeals;
	}
	public long getNoOfOnholdDeals() {
		return noOfOnholdDeals;
	}
	public void setNoOfOnholdDeals(long noOfOnholdDeals) {
		this.noOfOnholdDeals = noOfOnholdDeals;
	}
	public List<String> getListOfPolicyType() {
		return listOfPolicyType;
	}
	public void setListOfPolicyType(List<String> listOfPolicyType) {
		this.listOfPolicyType = listOfPolicyType;
	}
	public long getNoOfOnSearchLoans() {
		return noOfOnSearchLoans;
	}
	public void setNoOfOnSearchLoans(long noOfOnSearchLoans) {
		this.noOfOnSearchLoans = noOfOnSearchLoans;
	}
	
	
	
	
}
