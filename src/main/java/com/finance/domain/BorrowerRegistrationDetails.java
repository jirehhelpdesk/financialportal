package com.finance.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

import javax.persistence.Entity;
import javax.persistence.FetchType;

@Entity
@Table(name="borrower_registration_details")
public class BorrowerRegistrationDetails implements Serializable  {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="borrower_id")
	private int borrower_id;
	
	@Column(name="borrower_first_name")
	private String borrower_first_name;
	
	@Column(name="borrower_last_name")
	private String borrower_last_name;
	
	@Column(name="borrower_gender")
	private String borrower_gender;
	
	@Column(name="borrower_name")
	private String borrower_name;
	
	@Column(name="borrower_emailid")
	private String borrower_emailid;
	
	@Column(name="borrower_phno")
	private String borrower_phno;
	
	@Column(name="borrower_pan_id")
	private String borrower_pan_id;
	
	@Column(name="borrower_password")
	private String borrower_password;
	
	@Column(name="borrower_reg_time")
	private Date borrower_reg_time;
	
	@Column(name="borrower_status")
	private String borrower_status;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "borrowerRegDetails")
	@JoinColumn(name="borrower_id")
    private BorrowerIdentityModel borrowerIdentity;
	
		
	public int getBorrower_id() {
		return borrower_id;
	}

	public void setBorrower_id(int borrower_id) {
		this.borrower_id = borrower_id;
	}

	public String getBorrower_first_name() {
		return borrower_first_name;
	}

	public void setBorrower_first_name(String borrower_first_name) {
		this.borrower_first_name = borrower_first_name;
	}

	public String getBorrower_last_name() {
		return borrower_last_name;
	}

	public void setBorrower_last_name(String borrower_last_name) {
		this.borrower_last_name = borrower_last_name;
	}

	public String getBorrower_gender() {
		return borrower_gender;
	}

	public void setBorrower_gender(String borrower_gender) {
		this.borrower_gender = borrower_gender;
	}

	public String getBorrower_name() {
		return borrower_name;
	}

	public void setBorrower_name(String borrower_name) {
		this.borrower_name = borrower_name;
	}

	public String getBorrower_emailid() {
		return borrower_emailid;
	}

	public void setBorrower_emailid(String borrower_emailid) {
		this.borrower_emailid = borrower_emailid;
	}

	public String getBorrower_phno() {
		return borrower_phno;
	}

	public void setBorrower_phno(String borrower_phno) {
		this.borrower_phno = borrower_phno;
	}
	
	public String getBorrower_pan_id() {
		return borrower_pan_id;
	}

	public void setBorrower_pan_id(String borrower_pan_id) {
		this.borrower_pan_id = borrower_pan_id;
	}

	public String getBorrower_password() {
		return borrower_password;
	}

	public void setBorrower_password(String borrower_password) {
		this.borrower_password = borrower_password;
	}

	public Date getBorrower_reg_time() {
		return borrower_reg_time;
	}

	public void setBorrower_reg_time(Date borrower_reg_time) {
		this.borrower_reg_time = borrower_reg_time;
	}

	public String getBorrower_status() {
		return borrower_status;
	}

	public void setBorrower_status(String borrower_status) {
		this.borrower_status = borrower_status;
	}

	public BorrowerIdentityModel getBorrowerIdentity() {
		return borrowerIdentity;
	}

	public void setBorrowerIdentity(BorrowerIdentityModel borrowerIdentity) {
		this.borrowerIdentity = borrowerIdentity;
	}

	
	
}
