package com.finance.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="search_loan_details")
public class SearchLoanModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="search_id")
	private int search_id;
	
	@Column(name="loan_type")
	private String loan_type;
	
	@Column(name="loan_tenure")
	private String loan_tenure;
	
	@Column(name="loan_amount")
	private String loan_amount;
		
	@Column(name="full_name")
	private String full_name;
	
	@Column(name="email_id")
	private String email_id;
	
	@Column(name="phone_number")
	private String phone_number;
	
	@Column(name="search_date")
	private Date search_date;

	
	
	
	
	public int getSearch_id() {
		return search_id;
	}

	public void setSearch_id(int search_id) {
		this.search_id = search_id;
	}

	public String getLoan_type() {
		return loan_type;
	}

	public void setLoan_type(String loan_type) {
		this.loan_type = loan_type;
	}

	public String getLoan_tenure() {
		return loan_tenure;
	}

	public void setLoan_tenure(String loan_tenure) {
		this.loan_tenure = loan_tenure;
	}

	public String getLoan_amount() {
		return loan_amount;
	}

	public void setLoan_amount(String loan_amount) {
		this.loan_amount = loan_amount;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getEmail_id() {
		return email_id;
	}

	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}

	public String getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}

	public Date getSearch_date() {
		return search_date;
	}

	public void setSearch_date(Date search_date) {
		this.search_date = search_date;
	}

	
	
}
