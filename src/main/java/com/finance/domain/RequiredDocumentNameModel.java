package com.finance.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="required_document_name")
public class RequiredDocumentNameModel {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="name_id")
	private int name_id;
	
	@Column(name="type_id")
	private int type_id;
	
	@Column(name="document_name")
	private String document_name;
	
	@Column(name="cr_date")
	private Date cr_date;

	
	
	public int getName_id() {
		return name_id;
	}

	public void setName_id(int name_id) {
		this.name_id = name_id;
	}

	public int getType_id() {
		return type_id;
	}

	public void setType_id(int type_id) {
		this.type_id = type_id;
	}

	public String getDocument_name() {
		return document_name;
	}

	public void setDocument_name(String document_name) {
		this.document_name = document_name;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	
}
