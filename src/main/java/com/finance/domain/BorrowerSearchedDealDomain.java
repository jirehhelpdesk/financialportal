package com.finance.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="borrower_searched_deals")
public class BorrowerSearchedDealDomain {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="search_id")
	private int search_id;
	
	@Column(name="borrower_id")
	private int borrower_id;
	
	@Column(name="policy_id")
	private int policy_id;
	
	@Column(name="searched_date")
	private Date searched_date;

	
	
	public int getSearch_id() {
		return search_id;
	}

	public void setSearch_id(int search_id) {
		this.search_id = search_id;
	}

	public int getBorrower_id() {
		return borrower_id;
	}

	public void setBorrower_id(int borrower_id) {
		this.borrower_id = borrower_id;
	}

	public int getPolicy_id() {
		return policy_id;
	}

	public void setPolicy_id(int policy_id) {
		this.policy_id = policy_id;
	}

	public Date getSearched_date() {
		return searched_date;
	}

	public void setSearched_date(Date searched_date) {
		this.searched_date = searched_date;
	}

	
}
