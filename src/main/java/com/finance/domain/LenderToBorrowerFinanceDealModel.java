package com.finance.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

@Entity
@Table(name="lender_borrower_finance_deal")
public class LenderToBorrowerFinanceDealModel {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="deal_id")
	private int deal_id;
	
	
	@Column(name="lender_id")
	private int lender_id;
	
	@Column(name="borrower_id")
	private int borrower_id;
	
	@Column(name="deal_number")
	private String deal_number;
	
	@Column(name="policy_id")
	private String policy_id;
	
	@Column(name="policy_name")
	private String policy_name;
	
	@Column(name="policy_type")
	private String policy_type;
	
	@Column(name="deal_interest_rate")
	private double deal_interest_rate;
	
	@Column(name="deal_amount")
	private long deal_amount;
	
	@Column(name="deal_duration")
	private int deal_duration;
	
	
	
	
	@Column(name="income_type")
	private String income_type;
	
	@Column(name="income_annualy")
	private String income_annualy;
	
	@Column(name="pending_reject_reason")
	private String pending_reject_reason;
	
	@Column(name="processing_reject_reason")
	private String processing_reject_reason;
	
	@Column(name="note_to_lender")
	private String note_to_lender;
	
	
	@Column(name="house_status")
	private String house_status;
	
	@Column(name="four_wheeler_status")
	private String four_wheeler_status;
	
	@Column(name="two_wheeler_status")
	private String two_wheeler_status;
	
	@Column(name="amount_dispouse")
	private String amount_dispouse;
	
	@Column(name="amount_dispouse_date")
	private Date amount_dispouse_date;
	
	
	public String getIncome_type() {
		return income_type;
	}

	public void setIncome_type(String income_type) {
		this.income_type = income_type;
	}

	public String getIncome_annualy() {
		return income_annualy;
	}

	public void setIncome_annualy(String income_annualy) {
		this.income_annualy = income_annualy;
	}

	public String getPending_reject_reason() {
		return pending_reject_reason;
	}

	public void setPending_reject_reason(String pending_reject_reason) {
		this.pending_reject_reason = pending_reject_reason;
	}

	public String getProcessing_reject_reason() {
		return processing_reject_reason;
	}

	public void setProcessing_reject_reason(String processing_reject_reason) {
		this.processing_reject_reason = processing_reject_reason;
	}

	public String getNote_to_lender() {
		return note_to_lender;
	}

	public void setNote_to_lender(String note_to_lender) {
		this.note_to_lender = note_to_lender;
	}

	public String getScanned_file_name() {
		return scanned_file_name;
	}

	public void setScanned_file_name(String scanned_file_name) {
		this.scanned_file_name = scanned_file_name;
	}

	@Column(name="scanned_file_name")
	private String scanned_file_name;
	
	
	
	@Column(name="deal_status")
	private String deal_status;
	
	@Column(name="deal_initiated_date")
	private Date deal_initiated_date;

	@Column(name="deal_finalize_date")
	private Date deal_finalize_date;

	@Column(name="lender_status")
	private String lender_status;
	
	@Column(name="extra_info")
	private String extra_info;

	
	
	public int getDeal_id() {
		return deal_id;
	}

	public void setDeal_id(int deal_id) {
		this.deal_id = deal_id;
	}

	public int getLender_id() {
		return lender_id;
	}

	public void setLender_id(int lender_id) {
		this.lender_id = lender_id;
	}

	public int getBorrower_id() {
		return borrower_id;
	}

	public void setBorrower_id(int borrower_id) {
		this.borrower_id = borrower_id;
	}

	public String getDeal_number() {
		return deal_number;
	}

	public void setDeal_number(String deal_number) {
		this.deal_number = deal_number;
	}

	public String getPolicy_id() {
		return policy_id;
	}

	public void setPolicy_id(String policy_id) {
		this.policy_id = policy_id;
	}

	public String getPolicy_name() {
		return policy_name;
	}

	public void setPolicy_name(String policy_name) {
		this.policy_name = policy_name;
	}

	public String getPolicy_type() {
		return policy_type;
	}

	public void setPolicy_type(String policy_type) {
		this.policy_type = policy_type;
	}

	public double getDeal_interest_rate() {
		return deal_interest_rate;
	}

	public void setDeal_interest_rate(double deal_interest_rate) {
		this.deal_interest_rate = deal_interest_rate;
	}

	public long getDeal_amount() {
		return deal_amount;
	}

	public void setDeal_amount(long deal_amount) {
		this.deal_amount = deal_amount;
	}

	public int getDeal_duration() {
		return deal_duration;
	}

	public void setDeal_duration(int deal_duration) {
		this.deal_duration = deal_duration;
	}

	public String getDeal_status() {
		return deal_status;
	}

	public void setDeal_status(String deal_status) {
		this.deal_status = deal_status;
	}

	public Date getDeal_initiated_date() {
		return deal_initiated_date;
	}

	public void setDeal_initiated_date(Date deal_initiated_date) {
		this.deal_initiated_date = deal_initiated_date;
	}

	public Date getDeal_finalize_date() {
		return deal_finalize_date;
	}

	public void setDeal_finalize_date(Date deal_finalize_date) {
		this.deal_finalize_date = deal_finalize_date;
	}

	
	public String getLender_status() {
		return lender_status;
	}

	public void setLender_status(String lender_status) {
		this.lender_status = lender_status;
	}

	public String getExtra_info() {
		return extra_info;
	}

	public void setExtra_info(String extra_info) {
		this.extra_info = extra_info;
	}

	public String getHouse_status() {
		return house_status;
	}

	public void setHouse_status(String house_status) {
		this.house_status = house_status;
	}

	public String getFour_wheeler_status() {
		return four_wheeler_status;
	}

	public void setFour_wheeler_status(String four_wheeler_status) {
		this.four_wheeler_status = four_wheeler_status;
	}

	public String getTwo_wheeler_status() {
		return two_wheeler_status;
	}

	public void setTwo_wheeler_status(String two_wheeler_status) {
		this.two_wheeler_status = two_wheeler_status;
	}

	public String getAmount_dispouse() {
		return amount_dispouse;
	}

	public void setAmount_dispouse(String amount_dispouse) {
		this.amount_dispouse = amount_dispouse;
	}

	public Date getAmount_dispouse_date() {
		return amount_dispouse_date;
	}

	public void setAmount_dispouse_date(Date amount_dispouse_date) {
		this.amount_dispouse_date = amount_dispouse_date;
	}
	
	
	
}
