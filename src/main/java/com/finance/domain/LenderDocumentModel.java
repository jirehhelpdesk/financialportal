package com.finance.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="lender_document_detail")
public class LenderDocumentModel {
	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="lender_document_id")
	private int lender_document_id;
	
	@Column(name="lender_id")
	private int lender_id;
	
	@Column(name="document_type")
	private String document_type;
	
	@Column(name="file_name")
	private String file_name;
	
	@Column(name="cr_date")
	private Date cr_date;
	
	@Column(name="document_status")
	private String document_status;
	
	@Column(name="document_count")
	private int document_count;
	
	
	
	
	
	public int getLender_document_id() {
		return lender_document_id;
	}

	public void setLender_document_id(int lender_document_id) {
		this.lender_document_id = lender_document_id;
	}

	public int getLender_id() {
		return lender_id;
	}

	public void setLender_id(int lender_id) {
		this.lender_id = lender_id;
	}

	public String getDocument_type() {
		return document_type;
	}

	public void setDocument_type(String document_type) {
		this.document_type = document_type;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getDocument_status() {
		return document_status;
	}

	public void setDocument_status(String document_status) {
		this.document_status = document_status;
	}

	public int getDocument_count() {
		return document_count;
	}

	public void setDocument_count(int document_count) {
		this.document_count = document_count;
	}

	
	
	
}
