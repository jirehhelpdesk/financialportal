package com.finance.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="admin_details")
public class AdminDetails {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="admin_id")
	private int admin_id;
	
	@Column(name="admin_name")
	private String admin_name;
	
	@Column(name="admin_photo")
	private String admin_photo;
	
	@Column(name="admin_designation")
	private String admin_designation;
		
	@Column(name="admin_user_id")
	private String admin_user_id;
	
	@Column(name="admin_password")
	private String admin_password;
	
	@Column(name="admin_updated_date")
	private Date admin_updated_date;

	
	
	public int getAdmin_id() {
		return admin_id;
	}

	public void setAdmin_id(int admin_id) {
		this.admin_id = admin_id;
	}

	public String getAdmin_name() {
		return admin_name;
	}

	public void setAdmin_name(String admin_name) {
		this.admin_name = admin_name;
	}

	public String getAdmin_photo() {
		return admin_photo;
	}

	public void setAdmin_photo(String admin_photo) {
		this.admin_photo = admin_photo;
	}

	public String getAdmin_designation() {
		return admin_designation;
	}

	public void setAdmin_designation(String admin_designation) {
		this.admin_designation = admin_designation;
	}

	public String getAdmin_user_id() {
		return admin_user_id;
	}

	public void setAdmin_user_id(String admin_user_id) {
		this.admin_user_id = admin_user_id;
	}

	public String getAdmin_password() {
		return admin_password;
	}

	public void setAdmin_password(String admin_password) {
		this.admin_password = admin_password;
	}

	public Date getAdmin_updated_date() {
		return admin_updated_date;
	}

	public void setAdmin_updated_date(Date admin_updated_date) {
		this.admin_updated_date = admin_updated_date;
	}
	
}
