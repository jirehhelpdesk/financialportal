package com.finance.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

import javax.persistence.Entity;
import javax.persistence.FetchType;


@Entity
@Table(name="lender_registration_details")
public class LenderRegistrationDetails implements Serializable {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="lender_id")
	private int lender_id;
	
	@Column(name="lender_type")
	private String lender_type;
	
	@Column(name="full_name")
	private String full_name;
	
	
	@Column(name="lender_representative_name")
	private String lender_representative_name; 
	
	 
	 
	@Column(name="lender_emailid")
	private String lender_emailid;
	
	@Column(name="lender_phno")
	private String lender_phno;
	
	@Column(name="lender_password")
	private String lender_password;
	
	@Column(name="lender_reg_time")
	private Date lender_reg_time;
	
	@Column(name="lender_status")
	private String lender_status;

	
	@Column(name="lender_second_representative_name")
	private String lender_second_representative_name;
	
	@Column(name="lender_second_representative_email_id")
	private String lender_second_representative_email_id;
	
	@Column(name="lender_second_representative_mobile_no")
	private String lender_second_representative_mobile_no;

	@Column(name="lender_second_representative_password")
	private String lender_second_representative_password;
	
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "lenderRegDetails")
	@JoinColumn(name="lender_id")
	private LenderIdentityModel lenderIdentity;
	
	
	
	
	public int getLender_id() {
		return lender_id;
	}

	public void setLender_id(int lender_id) {
		this.lender_id = lender_id;
	}

	public String getLender_type() {
		return lender_type;
	}

	public void setLender_type(String lender_type) {
		this.lender_type = lender_type;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getLender_emailid() {
		return lender_emailid;
	}

	public void setLender_emailid(String lender_emailid) {
		this.lender_emailid = lender_emailid;
	}

	public String getLender_phno() {
		return lender_phno;
	}

	public void setLender_phno(String lender_phno) {
		this.lender_phno = lender_phno;
	}

	public String getLender_password() {
		return lender_password;
	}

	public void setLender_password(String lender_password) {
		this.lender_password = lender_password;
	}

	public Date getLender_reg_time() {
		return lender_reg_time;
	}

	public void setLender_reg_time(Date lender_reg_time) {
		this.lender_reg_time = lender_reg_time;
	}

	public String getLender_status() {
		return lender_status;
	}

	public void setLender_status(String lender_status) {
		this.lender_status = lender_status;
	}

	public LenderIdentityModel getLenderIdentity() {
		return lenderIdentity;
	}

	public void setLenderIdentity(LenderIdentityModel lenderIdentity) {
		this.lenderIdentity = lenderIdentity;
	}

	public String getLender_representative_name() {
		return lender_representative_name;
	}

	public void setLender_representative_name(String lender_representative_name) {
		this.lender_representative_name = lender_representative_name;
	}

	public String getLender_second_representative_name() {
		return lender_second_representative_name;
	}

	public void setLender_second_representative_name(
			String lender_second_representative_name) {
		this.lender_second_representative_name = lender_second_representative_name;
	}

	public String getLender_second_representative_email_id() {
		return lender_second_representative_email_id;
	}

	public void setLender_second_representative_email_id(
			String lender_second_representative_email_id) {
		this.lender_second_representative_email_id = lender_second_representative_email_id;
	}

	public String getLender_second_representative_mobile_no() {
		return lender_second_representative_mobile_no;
	}

	public void setLender_second_representative_mobile_no(
			String lender_second_representative_mobile_no) {
		this.lender_second_representative_mobile_no = lender_second_representative_mobile_no;
	}

	public String getLender_second_representative_password() {
		return lender_second_representative_password;
	}

	public void setLender_second_representative_password(
			String lender_second_representative_password) {
		this.lender_second_representative_password = lender_second_representative_password;
	}
	
}
