package com.finance.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="link_validation")
public class LinkValidationModel {


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="link_serial_id")
	private int link_serial_id;
	
	@Column(name="validate_id")
	private String validate_id;
	
	@Column(name="validate_email_id")
	private String validate_email_id;
	
	@Column(name="validate_status")
	private String validate_status;
	
	@Column(name="validate_init_date")
	private Date validate_init_date;

	
	@Column(name="validate_for")
	private String validate_for;
	
	
	public int getLink_serial_id() {
		return link_serial_id;
	}

	public void setLink_serial_id(int link_serial_id) {
		this.link_serial_id = link_serial_id;
	}

	public String getValidate_id() {
		return validate_id;
	}

	public void setValidate_id(String validate_id) {
		this.validate_id = validate_id;
	}

	public String getValidate_email_id() {
		return validate_email_id;
	}

	public void setValidate_email_id(String validate_email_id) {
		this.validate_email_id = validate_email_id;
	}

	public String getValidate_status() {
		return validate_status;
	}

	public void setValidate_status(String validate_status) {
		this.validate_status = validate_status;
	}

	public Date getValidate_init_date() {
		return validate_init_date;
	}

	public void setValidate_init_date(Date validate_init_date) {
		this.validate_init_date = validate_init_date;
	}

	public String getValidate_for() {
		return validate_for;
	}

	public void setValidate_for(String validate_for) {
		this.validate_for = validate_for;
	}
	
}
