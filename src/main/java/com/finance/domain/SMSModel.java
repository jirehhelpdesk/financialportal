package com.finance.domain;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sent_sms_records")
public class SMSModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="sms_sl_id")
	private int sms_sl_id;
	
	@Column(name="extra_info")
	private String extra_info;
	
	@Column(name="ph_no")
	private String ph_no;
	
	@Column(name="sms_message")
	private String sms_message;

	
	@Column(name="sms_status")
	private String sms_status;
	
	@Column(name="cr_date")
	private Date cr_date;

	
	
	
	public int getSms_sl_id() {
		return sms_sl_id;
	}

	public void setSms_sl_id(int sms_sl_id) {
		this.sms_sl_id = sms_sl_id;
	}

	public String getExtra_info() {
		return extra_info;
	}

	public void setExtra_info(String extra_info) {
		this.extra_info = extra_info;
	}

	public String getPh_no() {
		return ph_no;
	}

	public void setPh_no(String ph_no) {
		this.ph_no = ph_no;
	}

	public String getSms_message() {
		return sms_message;
	}

	public void setSms_message(String sms_message) {
		this.sms_message = sms_message;
	}

	public String getSms_status() {
		return sms_status;
	}

	public void setSms_status(String sms_status) {
		this.sms_status = sms_status;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	
}
