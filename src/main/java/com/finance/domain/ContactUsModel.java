package com.finance.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="contact_us_details")
public class ContactUsModel {

	
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		@Column(name="contact_id")
		private int contact_id;
		
		@Column(name="contact_name")
		private String contact_name;
		
		@Column(name="contact_email_id")
		private String contact_email_id;
		
		@Column(name="contact_reason")
		private String contact_reason;
			
		@Column(name="contact_message")
		private String contact_message;
		
		@Column(name="contact_date")
		private Date contact_date;

		@Column(name="contact_status")
		private String contact_status;
		
		
		
		public int getContact_id() {
			return contact_id;
		}

		public void setContact_id(int contact_id) {
			this.contact_id = contact_id;
		}

		public String getContact_name() {
			return contact_name;
		}

		public void setContact_name(String contact_name) {
			this.contact_name = contact_name;
		}

		public String getContact_email_id() {
			return contact_email_id;
		}

		public void setContact_email_id(String contact_email_id) {
			this.contact_email_id = contact_email_id;
		}

		public String getContact_reason() {
			return contact_reason;
		}

		public void setContact_reason(String contact_reason) {
			this.contact_reason = contact_reason;
		}

		public String getContact_message() {
			return contact_message;
		}

		public void setContact_message(String contact_message) {
			this.contact_message = contact_message;
		}

		public Date getContact_date() {
			return contact_date;
		}

		public void setContact_date(Date contact_date) {
			this.contact_date = contact_date;
		}

		public String getContact_status() {
			return contact_status;
		}

		public void setContact_status(String contact_status) {
			this.contact_status = contact_status;
		}
		
}
