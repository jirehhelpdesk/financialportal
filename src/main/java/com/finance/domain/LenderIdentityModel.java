package com.finance.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name="lender_identity_details")
public class LenderIdentityModel {

	
	 @Id
	 @GeneratedValue(generator="foreign")
	 @GenericGenerator(name="foreign", strategy = "foreign", parameters={@Parameter(name="property", value="lenderRegDetails")})
	 @Column(name="lender_id")
	 private int lender_id;
	 
	 
	 
	// Present Address Pattern Update	
		
		@Column(name="lender_present_door")
		private String lender_present_door;
		
		@Column(name="lender_present_building")
		private String lender_present_building;
		
		@Column(name="lender_present_street")
		private String lender_present_street;
		
		@Column(name="lender_present_area")
		private String lender_present_area;
		
		@Column(name="lender_present_city")
		private String lender_present_city;
		
		@Column(name="lender_present_pincode")
		private String lender_present_pincode;
		
		
	
	@Column(name="lender_present_address")
	private String lender_present_address;
	
	@Column(name="lender_permanent_address")
	private String lender_permanent_address;
	
	@Column(name="lender_photo")
	private String lender_photo;
	
	@Column(name="cr_dare")
	private Date cr_dare;
		
	
	
	
	@Column(name="pan_number")
	private String pan_number;
	
	@Column(name="service_tax_number")
	private String service_tax_number;
	
	@Column(name="tan_number")
	private String tan_number;
	
	
	
	
	@OneToOne(fetch = FetchType.LAZY, optional=true)
	@Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@PrimaryKeyJoinColumn
	private LenderRegistrationDetails lenderRegDetails;
	
	
	/*@OneToOne
	@PrimaryKeyJoinColumn
    private LenderRegistrationDetails lenderRegDetails;
    */
	

	public int getLender_id() {
		return lender_id;
	}

	public void setLender_id(int lender_id) {
		this.lender_id = lender_id;
	}

	public String getLender_present_address() {
		return lender_present_address;
	}

	public void setLender_present_address(String lender_present_address) {
		this.lender_present_address = lender_present_address;
	}

	public String getLender_present_pincode() {
		return lender_present_pincode;
	}

	public void setLender_present_pincode(String lender_present_pincode) {
		this.lender_present_pincode = lender_present_pincode;
	}

	public String getLender_permanent_address() {
		return lender_permanent_address;
	}

	public void setLender_permanent_address(String lender_permanent_address) {
		this.lender_permanent_address = lender_permanent_address;
	}

	

	public String getLender_photo() {
		return lender_photo;
	}

	public void setLender_photo(String lender_photo) {
		this.lender_photo = lender_photo;
	}

	
	public Date getCr_dare() {
		return cr_dare;
	}

	public void setCr_dare(Date cr_dare) {
		this.cr_dare = cr_dare;
	}

	public LenderRegistrationDetails getLenderRegDetails() {
		return lenderRegDetails;
	}


	public void setLenderRegDetails(LenderRegistrationDetails lenderRegDetails) {
		this.lenderRegDetails = lenderRegDetails;
	}

	

	public String getLender_present_door() {
		return lender_present_door;
	}

	public void setLender_present_door(String lender_present_door) {
		this.lender_present_door = lender_present_door;
	}

	public String getLender_present_building() {
		return lender_present_building;
	}

	public void setLender_present_building(String lender_present_building) {
		this.lender_present_building = lender_present_building;
	}

	public String getLender_present_street() {
		return lender_present_street;
	}

	public void setLender_present_street(String lender_present_street) {
		this.lender_present_street = lender_present_street;
	}

	public String getLender_present_area() {
		return lender_present_area;
	}

	public void setLender_present_area(String lender_present_area) {
		this.lender_present_area = lender_present_area;
	}

	public String getLender_present_city() {
		return lender_present_city;
	}

	public void setLender_present_city(String lender_present_city) {
		this.lender_present_city = lender_present_city;
	}

	public String getPan_number() {
		return pan_number;
	}

	public void setPan_number(String pan_number) {
		this.pan_number = pan_number;
	}

	public String getService_tax_number() {
		return service_tax_number;
	}

	public void setService_tax_number(String service_tax_number) {
		this.service_tax_number = service_tax_number;
	}

	public String getTan_number() {
		return tan_number;
	}

	public void setTan_number(String tan_number) {
		this.tan_number = tan_number;
	}

	
	
}
