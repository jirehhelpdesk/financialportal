package com.finance.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name="borrower_identity_details")
public class BorrowerIdentityModel {

	
	@Id
	@GeneratedValue(generator="foreign")
	@GenericGenerator(name="foreign", strategy = "foreign", parameters={@Parameter(name="property", value="borrowerRegDetails")})
	@Column(name="borrower_id")
	private int borrower_id;
	
	@Column(name="borrower_profile_photo")
	private String borrower_profile_photo;
	
	
	
   // Present Address Pattern Update	
	
	@Column(name="borrower_present_door")
	private String borrower_present_door;
	
	@Column(name="borrower_present_building")
	private String borrower_present_building;
	
	@Column(name="borrower_present_street")
	private String borrower_present_street;
	
	@Column(name="borrower_present_area")
	private String borrower_present_area;
	
	@Column(name="borrower_present_city")
	private String borrower_present_city;
	
	@Column(name="borrower_present_pincode")
	private String borrower_present_pincode;
	
	
    // Permanent Address Pattern Update	
	
	@Column(name="borrower_permanent_door")
	private String borrower_permanent_door;
	
	@Column(name="borrower_permanent_building")
	private String borrower_permanent_building;
	
	@Column(name="borrower_permanent_street")
	private String borrower_permanent_street;
	
	@Column(name="borrower_permanent_area")
	private String borrower_permanent_area;
	
	@Column(name="borrower_permanent_city")
	private String borrower_permanent_city;
	
	@Column(name="borrower_permanent_pincode")
	private String borrower_permanent_pincode;
	
	
	@Column(name="borrower_alt_phno")
	private String borrower_alt_phno;
	
	@Column(name="borrower_alt_emailid")
	private String borrower_alt_emailid;
	
	
	@Column(name="borrower_dob")
	private String borrower_dob;

	@Column(name="borrower_identity_id_type")
	private String borrower_identity_id_type;
	
	@Column(name="borrower_identity_card_id")
	private String borrower_identity_card_id;
	
	@Column(name="cr_date")
	private Date cr_date;

	@OneToOne(fetch = FetchType.LAZY, optional=true)
	@Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@PrimaryKeyJoinColumn
    private BorrowerRegistrationDetails borrowerRegDetails;
	
	
	public int getBorrower_id() {
		return borrower_id;
	}

	public void setBorrower_id(int borrower_id) {
		this.borrower_id = borrower_id;
	}

	public String getBorrower_profile_photo() {
		return borrower_profile_photo;
	}

	public void setBorrower_profile_photo(String borrower_profile_photo) {
		this.borrower_profile_photo = borrower_profile_photo;
	}

	

	public String getBorrower_present_pincode() {
		return borrower_present_pincode;
	}

	public void setBorrower_present_pincode(String borrower_present_pincode) {
		this.borrower_present_pincode = borrower_present_pincode;
	}

	
	public String getBorrower_permanent_pincode() {
		return borrower_permanent_pincode;
	}

	public void setBorrower_permanent_pincode(String borrower_permanent_pincode) {
		this.borrower_permanent_pincode = borrower_permanent_pincode;
	}

	public String getBorrower_dob() {
		return borrower_dob;
	}

	public void setBorrower_dob(String borrower_dob) {
		this.borrower_dob = borrower_dob;
	}

	public String getBorrower_identity_id_type() {
		return borrower_identity_id_type;
	}

	public void setBorrower_identity_id_type(String borrower_identity_id_type) {
		this.borrower_identity_id_type = borrower_identity_id_type;
	}

	public String getBorrower_identity_card_id() {
		return borrower_identity_card_id;
	}

	public void setBorrower_identity_card_id(String borrower_identity_card_id) {
		this.borrower_identity_card_id = borrower_identity_card_id;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	
	public BorrowerRegistrationDetails getBorrowerRegDetails() {
		return borrowerRegDetails;
	}

	public void setBorrowerRegDetails(BorrowerRegistrationDetails borrowerRegDetails) {
		this.borrowerRegDetails = borrowerRegDetails;
	}

	public String getBorrower_present_door() {
		return borrower_present_door;
	}

	public void setBorrower_present_door(String borrower_present_door) {
		this.borrower_present_door = borrower_present_door;
	}

	public String getBorrower_present_building() {
		return borrower_present_building;
	}

	public void setBorrower_present_building(String borrower_present_building) {
		this.borrower_present_building = borrower_present_building;
	}

	public String getBorrower_present_street() {
		return borrower_present_street;
	}

	public void setBorrower_present_street(String borrower_present_street) {
		this.borrower_present_street = borrower_present_street;
	}

	public String getBorrower_present_area() {
		return borrower_present_area;
	}

	public void setBorrower_present_area(String borrower_present_area) {
		this.borrower_present_area = borrower_present_area;
	}

	public String getBorrower_present_city() {
		return borrower_present_city;
	}

	public void setBorrower_present_city(String borrower_present_city) {
		this.borrower_present_city = borrower_present_city;
	}

	public String getBorrower_permanent_door() {
		return borrower_permanent_door;
	}

	public void setBorrower_permanent_door(String borrower_permanent_door) {
		this.borrower_permanent_door = borrower_permanent_door;
	}

	public String getBorrower_permanent_building() {
		return borrower_permanent_building;
	}

	public void setBorrower_permanent_building(String borrower_permanent_building) {
		this.borrower_permanent_building = borrower_permanent_building;
	}

	public String getBorrower_permanent_street() {
		return borrower_permanent_street;
	}

	public void setBorrower_permanent_street(String borrower_permanent_street) {
		this.borrower_permanent_street = borrower_permanent_street;
	}

	public String getBorrower_permanent_area() {
		return borrower_permanent_area;
	}

	public void setBorrower_permanent_area(String borrower_permanent_area) {
		this.borrower_permanent_area = borrower_permanent_area;
	}

	public String getBorrower_permanent_city() {
		return borrower_permanent_city;
	}

	public void setBorrower_permanent_city(String borrower_permanent_city) {
		this.borrower_permanent_city = borrower_permanent_city;
	}

	public String getBorrower_alt_phno() {
		return borrower_alt_phno;
	}

	public void setBorrower_alt_phno(String borrower_alt_phno) {
		this.borrower_alt_phno = borrower_alt_phno;
	}

	public String getBorrower_alt_emailid() {
		return borrower_alt_emailid;
	}

	public void setBorrower_alt_emailid(String borrower_alt_emailid) {
		this.borrower_alt_emailid = borrower_alt_emailid;
	}
	
	
}
