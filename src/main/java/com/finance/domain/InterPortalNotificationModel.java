package com.finance.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="inter_portal_notification")
public class InterPortalNotificationModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="notify_id")
	private int notify_id;
	
	
	
	@Column(name="notify_sender")
	private int notify_sender;
	
	@Column(name="notify_sender_type")
	private String notify_sender_type;
	
	@Column(name="notify_reciver")
	private int notify_reciver;
	
	@Column(name="notify_reciver_type")
	private String notify_reciver_type;
	
	
	@Column(name="notify_regarding")
	private String notify_regarding;
	
	@Column(name="notify_subject")
	private String notify_subject;
	
	@Column(name="notify_message")
	private String notify_message;
	
	@Column(name="notify_status")
	private String notify_status;
	
	@Column(name="notify_date")
	private Date notify_date;

	@Column(name="notify_extra_info_1")
	private String notify_extra_info_1;
	
	@Column(name="notify_extra_info_2")
	private String notify_extra_info_2;

	
	
	
	
	
	public int getNotify_id() {
		return notify_id;
	}

	public void setNotify_id(int notify_id) {
		this.notify_id = notify_id;
	}

	public int getNotify_sender() {
		return notify_sender;
	}

	public void setNotify_sender(int notify_sender) {
		this.notify_sender = notify_sender;
	}

	public String getNotify_sender_type() {
		return notify_sender_type;
	}

	public void setNotify_sender_type(String notify_sender_type) {
		this.notify_sender_type = notify_sender_type;
	}

	public int getNotify_reciver() {
		return notify_reciver;
	}

	public void setNotify_reciver(int notify_reciver) {
		this.notify_reciver = notify_reciver;
	}

	public String getNotify_reciver_type() {
		return notify_reciver_type;
	}

	public void setNotify_reciver_type(String notify_reciver_type) {
		this.notify_reciver_type = notify_reciver_type;
	}

	public String getNotify_subject() {
		return notify_subject;
	}

	public void setNotify_subject(String notify_subject) {
		this.notify_subject = notify_subject;
	}

	public String getNotify_message() {
		return notify_message;
	}

	public void setNotify_message(String notify_message) {
		this.notify_message = notify_message;
	}

	public String getNotify_status() {
		return notify_status;
	}

	public void setNotify_status(String notify_status) {
		this.notify_status = notify_status;
	}

	public Date getNotify_date() {
		return notify_date;
	}

	public void setNotify_date(Date notify_date) {
		this.notify_date = notify_date;
	}

	public String getNotify_extra_info_1() {
		return notify_extra_info_1;
	}

	public void setNotify_extra_info_1(String notify_extra_info_1) {
		this.notify_extra_info_1 = notify_extra_info_1;
	}

	public String getNotify_extra_info_2() {
		return notify_extra_info_2;
	}

	public void setNotify_extra_info_2(String notify_extra_info_2) {
		this.notify_extra_info_2 = notify_extra_info_2;
	}

	public String getNotify_regarding() {
		return notify_regarding;
	}

	public void setNotify_regarding(String notify_regarding) {
		this.notify_regarding = notify_regarding;
	}
	
	
}
