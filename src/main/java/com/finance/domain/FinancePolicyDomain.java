package com.finance.domain;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="finance_policy_type")
public class FinancePolicyDomain {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="policy_type_id")
	private int policy_type_id;
	
	@Column(name="policy_type_name")
	private String policy_type_name;
	
	@Column(name="sub_policy_type")
	private String sub_policy_type;
	
	@Column(name="policy_component")
	private String policy_component;
	
	@Column(name="cr_date")
	private Date cr_date;

	
	
	public int getPolicy_type_id() {
		return policy_type_id;
	}

	public void setPolicy_type_id(int policy_type_id) {
		this.policy_type_id = policy_type_id;
	}

	public String getPolicy_type_name() {
		return policy_type_name;
	}

	public void setPolicy_type_name(String policy_type_name) {
		this.policy_type_name = policy_type_name;
	}

	public String getSub_policy_type() {
		return sub_policy_type;
	}

	public void setSub_policy_type(String sub_policy_type) {
		this.sub_policy_type = sub_policy_type;
	}

	public String getPolicy_component() {
		return policy_component;
	}

	public void setPolicy_component(String policy_component) {
		this.policy_component = policy_component;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	
}
