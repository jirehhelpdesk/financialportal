package com.finance.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user_otp_record")
public class UserOTPModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="otp_id")
	private int otp_id;
	
	@Column(name="user_type")
	private String user_type;
	
	@Column(name="user_mobile")
	private String user_mobile;
	
	@Column(name="otp_number")
	private String otp_number;
	
	@Column(name="otp_status")
	private String otp_status;
		
	@Column(name="otp_cr_date")
	private Date otp_cr_date;

	@Column(name="otp_other_info")
	private String otp_other_info;

	
	
	
	
	
	public int getOtp_id() {
		return otp_id;
	}

	public void setOtp_id(int otp_id) {
		this.otp_id = otp_id;
	}

	public String getUser_type() {
		return user_type;
	}

	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}


	public String getUser_mobile() {
		return user_mobile;
	}

	public void setUser_mobile(String user_mobile) {
		this.user_mobile = user_mobile;
	}

	public String getOtp_number() {
		return otp_number;
	}

	public void setOtp_number(String otp_number) {
		this.otp_number = otp_number;
	}

	public String getOtp_status() {
		return otp_status;
	}

	public void setOtp_status(String otp_status) {
		this.otp_status = otp_status;
	}

	public Date getOtp_cr_date() {
		return otp_cr_date;
	}

	public void setOtp_cr_date(Date otp_cr_date) {
		this.otp_cr_date = otp_cr_date;
	}

	public String getOtp_other_info() {
		return otp_other_info;
	}

	public void setOtp_other_info(String otp_other_info) {
		this.otp_other_info = otp_other_info;
	}
	
	
}
