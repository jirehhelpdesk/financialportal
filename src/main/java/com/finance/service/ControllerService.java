package com.finance.service;

import java.util.List;

import com.finance.bean.BorrowerDetailsBean;
import com.finance.bean.ControllerDetailsBean;
import com.finance.bean.LenderDetailsBean;
import com.finance.bean.LenderToBorrowerFinanceDealBean;
import com.finance.domain.InterPortalNotificationModel;
import com.finance.domain.LenderRegistrationDetails;

public interface ControllerService {

	public List<ControllerDetailsBean> getControllerDetails(String controllerEmailId);
	
	
	public String getValidateController(String controller_user_id,String controller_password);
	
	public List<LenderToBorrowerFinanceDealBean> getDealDetails(String condititon,String Parameter);
	
	public List<LenderToBorrowerFinanceDealBean> getSearchDealDetails(String searchType,String searchValue,String loanStatusDiv);
	
	public List<LenderToBorrowerFinanceDealBean> getDealDetailToView(String condititon,String Parameter);
	
	public String updateDealStatusByController(LenderToBorrowerFinanceDealBean dealBean);
	
	public List<BorrowerDetailsBean> getSearchBorrower(String searchType,String searchValue);
	
	public List<BorrowerDetailsBean> getLastBorrowerDetails();
	
	public List<LenderDetailsBean> getSearchLender(String searchType,String searchValue);
	
	public List<LenderDetailsBean> getLastLenderDetails();
	
	public String getSaveNotifyDetails(InterPortalNotificationModel notifyModel);
	
	public String changeControllerPassword(String controllerEmailId,String curPassword,String newPassword);	
	
	
	public String getUpdateControllerDetails(String controllerEmailId,String name,String role,String fileName);
	
	public String createLenderByCntrl(LenderRegistrationDetails lenderRegDetails);
	
	public String manageBorrowerStatus(int borrowerId,String condition);
	
	public String manageLenderStatus(int lenderId,String condition);
	
}
