package com.finance.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finance.bean.BorrowerDetailsBean;
import com.finance.bean.InterPortalNotificationBean;
import com.finance.bean.LenderDetailsBean;
import com.finance.bean.LenderPolicyBean;
import com.finance.bean.LenderToBorrowerFinanceDealBean;
import com.finance.dao.AdminDao;
import com.finance.dao.BorrowerDao;
import com.finance.dao.ControllerDao;
import com.finance.dao.LenderDao;
import com.finance.domain.BorrowerDocumentModel;
import com.finance.domain.BorrowerRegistrationDetails;
import com.finance.domain.EmailModel;
import com.finance.domain.InterPortalNotificationModel;
import com.finance.domain.LenderDocumentModel;
import com.finance.domain.LenderIdentityModel;
import com.finance.domain.LenderPolicyModel;
import com.finance.domain.LenderRegistrationDetails;
import com.finance.domain.LenderToBorrowerFinanceDealModel;
import com.finance.domain.LinkValidationModel;
import com.finance.util.EmailNotificationUtil;
import com.finance.util.EmailSentUtil;
import com.finance.util.PasswordGenerator;
import com.finance.util.UniquePatternGeneration;

@Service("lenderService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class LenderServiceImpl implements LenderService {

	
	@Autowired
	private LenderDao lenderDao;
	
	@Autowired
	private AdminDao adminDao;
	
	@Autowired
	private ControllerDao controllerDao;
	
	
	PasswordGenerator pwGenerator = new PasswordGenerator();
	
	EmailNotificationUtil sendEmailUtil = new EmailNotificationUtil();
	
	
	@SuppressWarnings("static-access")
	public String saveLenderRegdetails(LenderRegistrationDetails lenderRegDetails)
	{
		lenderRegDetails.setLender_reg_time(new Date());
		lenderRegDetails.setLender_status("Deactive");
		lenderRegDetails.setLender_representative_name(lenderRegDetails.getFull_name());
		
		try {
			
		
			LenderIdentityModel lenderIdentity = new LenderIdentityModel();

			lenderIdentity.setLender_present_pincode("");
			lenderIdentity.setCr_dare(new Date());
			lenderIdentity.setLender_photo("No Photo");
			lenderIdentity.setLenderRegDetails(lenderRegDetails);
			
			lenderRegDetails.setLenderIdentity(lenderIdentity);
			
			lenderIdentity.setLender_present_door("");
			lenderIdentity.setLender_present_building("");
			lenderIdentity.setLender_present_street("");
			lenderIdentity.setLender_present_area("");
			lenderIdentity.setLender_present_city("");
			
			lenderIdentity.setPan_number("");
			lenderIdentity.setService_tax_number("");
			lenderIdentity.setTan_number("");
			
			
			lenderRegDetails.setLender_password(pwGenerator.encrypt(lenderRegDetails.getLender_password()));
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String saveInDB = lenderDao.saveLenderRegdetails(lenderRegDetails);
				
		if(saveInDB.equals("success"))
		{
			
			UniquePatternGeneration generatorObject = new UniquePatternGeneration();
			
			String validateId = generatorObject.generateUniqueCode(lenderRegDetails.getLender_emailid());
			
			String link = "http://192.168.1.6:8082/FP/lenActiveAccount?account="+validateId;
			
			String reasonFor = "LenderReg";
			
			String extraInfo = "NotRequired";
			
			String emailBody = sendEmailUtil.emailNotification(lenderRegDetails.getFull_name(),lenderRegDetails.getLender_emailid(), "resources/wellcomeRegistration", link , "Active Account",reasonFor,extraInfo);
			
			
			
			
			String mailStatus = "";
			
			ResourceBundle resource = ResourceBundle.getBundle("resources/wellcomeRegistration");
		 	 
			String mailSubject = resource.getString("mailSubject");
			
			
			/* SENDING MAIL */
		    
			
				    EmailSentUtil emailStatus = new EmailSentUtil();
		 	        
					try {
						mailStatus = emailStatus.getEmailSent(lenderRegDetails.getLender_emailid(),mailSubject,emailBody);
					} 
					catch (Exception e) {
						
						// TODO Auto-generated catch block
						mailStatus = "failed";
						e.printStackTrace();
					}
					
		  
		    /* END OF SENDING MAIL */
					
			
			if(!mailStatus.equals("success"))
			{
				EmailModel emailDetail = new EmailModel();
				
				emailDetail.setEmail_id(lenderRegDetails.getLender_emailid());
				emailDetail.setEmail_subject(mailSubject);
				emailDetail.setEmail_content(emailBody);
				emailDetail.setEmail_status("failed");
				emailDetail.setCr_date(new Date());
				
				adminDao.getSaveEmailDetails(emailDetail);
			}
			
			
			
			
			
			
		    if(mailStatus.equals("success"))
		    {
		    	// Save email sent details
		    	

		    	LinkValidationModel linkValidate = new LinkValidationModel();
				
				linkValidate.setValidate_email_id(lenderRegDetails.getLender_emailid());
				linkValidate.setValidate_id(validateId);
				linkValidate.setValidate_status("Pending");
				linkValidate.setValidate_init_date(new Date());
				linkValidate.setValidate_for("Activation Account");
				
				lenderDao.saveLinkValidatedetails(linkValidate);
				
		    }
		    else
		    {
		    	
		    }
		    
		    return saveInDB;
		}
		else
		{
			return saveInDB;
		}
				
	}
	
	public String getValidateLender(String userId,String password,HttpSession session)
	{
		String status = "";
		
		try {
					
			String decryptPassword = pwGenerator.encrypt(password);
			status = lenderDao.getValidateLender(userId,decryptPassword,session);			
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 			
		return status;
	}
	
	public int getLenderIdViaUserId(String userId,HttpSession session)
	{
		return lenderDao.getLenderIdViaUserId(userId,session);
	}
	
	
	
	@SuppressWarnings("static-access")
	public String sendLinkForgetPassword(String emailId)
	{
		String validateId = "";
		LinkValidationModel linkValidate = new LinkValidationModel();
		
		linkValidate.setValidate_email_id(emailId);
		
		try {
			
			UniquePatternGeneration generatorObject = new UniquePatternGeneration();
			
			validateId += generatorObject.generateUniqueCode(emailId);
			
			linkValidate.setValidate_id(validateId);
						
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		String link = "http://192.168.1.6:8081/FP/lenResetPassword?requnIkedij="+validateId+"";
		
		
		String reasonFor = "ResetPassword";
		
		String extraInfo = "NotRequired";
		
		
		String emailBody = sendEmailUtil.emailNotification("User",emailId,"resources/forgetPassword",link,"Reset Passwod",reasonFor,extraInfo);
		
		
		
		String mailStatus = "";
		
		ResourceBundle resource = ResourceBundle.getBundle("resources/forgetPassword");
	 	 
		String mailSubject = resource.getString("mailSubject");
		
		
		/* SENDING MAIL */
	    
		
			    EmailSentUtil emailStatus = new EmailSentUtil();
	 	        
				try {
					mailStatus = emailStatus.getEmailSent(emailId,mailSubject,emailBody);
				} 
				catch (Exception e) {
					
					// TODO Auto-generated catch block
					mailStatus = "failed";
					e.printStackTrace();
				}
				
	  
	    /* END OF SENDING MAIL */
				
		
		if(!mailStatus.equals("success"))
		{
			EmailModel emailDetail = new EmailModel();
			
			emailDetail.setEmail_id(emailId);
			emailDetail.setEmail_subject(mailSubject);
			emailDetail.setEmail_content(emailBody);
			emailDetail.setEmail_status("failed");
			emailDetail.setCr_date(new Date());
			
			adminDao.getSaveEmailDetails(emailDetail);
		}
		
		
	    if(mailStatus.equals("success"))
	    {
	    	// Save email sent details
	    	
			linkValidate.setValidate_status("Pending");
			linkValidate.setValidate_init_date(new Date());
			linkValidate.setValidate_for("Forget Password");			
			
			lenderDao.saveLinkValidatedetails(linkValidate);
	    }
	    else
	    {
	    	
	    }	    
	    	return mailStatus; 		
	}
	
	
	public String getEmailIdFromForgetPassword(String uniqueId)
	{
		return lenderDao.getEmailIdFromForgetPassword(uniqueId);
	}
	
	
	public String getChangePassword(String emailId,String password)
	{
		String encPassword = "";
		try {
					
			encPassword += pwGenerator.encrypt(password);
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return lenderDao.getChangePassword(emailId,encPassword);
	}
	
	
	public LenderDetailsBean getLenderDetails(int lenderId)
	{
		List<LenderRegistrationDetails> lenderDetails = lenderDao.getAllLenderDetails(lenderId);
		
		LenderDetailsBean lenderBean = new LenderDetailsBean();
		
		for(int i=0;i<lenderDetails.size();i++)
		{
			lenderBean.setLender_id(lenderDetails.get(i).getLender_id());
			lenderBean.setFull_name(lenderDetails.get(i).getFull_name());
			lenderBean.setLender_emailid(lenderDetails.get(i).getLender_emailid());
			lenderBean.setLender_phno(lenderDetails.get(i).getLender_phno());
			lenderBean.setLender_reg_time(lenderDetails.get(i).getLender_reg_time());
			lenderBean.setLender_status(lenderDetails.get(i).getLender_status());
			lenderBean.setLender_type(lenderDetails.get(i).getLender_type());
			lenderBean.setLender_representative_name(lenderDetails.get(i).getLender_representative_name());
			
			lenderBean.setLender_second_representative_name(lenderDetails.get(i).getLender_second_representative_name());
			lenderBean.setLender_second_representative_email_id(lenderDetails.get(i).getLender_second_representative_email_id());
			lenderBean.setLender_second_representative_mobile_no(lenderDetails.get(i).getLender_second_representative_mobile_no());
			
			if(lenderDetails.get(i).getLenderIdentity()!=null)
			{				
				lenderBean.setLender_present_pincode(lenderDetails.get(i).getLenderIdentity().getLender_present_pincode());
				lenderBean.setLender_photo(lenderDetails.get(i).getLenderIdentity().getLender_photo());
				lenderBean.setCr_dare(lenderDetails.get(i).getLenderIdentity().getCr_dare());
				
				lenderBean.setLender_present_door(lenderDetails.get(i).getLenderIdentity().getLender_present_door());
				lenderBean.setLender_present_building(lenderDetails.get(i).getLenderIdentity().getLender_present_building());
				lenderBean.setLender_present_street(lenderDetails.get(i).getLenderIdentity().getLender_present_street());
				lenderBean.setLender_present_area(lenderDetails.get(i).getLenderIdentity().getLender_present_area());
				lenderBean.setLender_present_city(lenderDetails.get(i).getLenderIdentity().getLender_present_city());
				
				lenderBean.setPan_number(lenderDetails.get(i).getLenderIdentity().getPan_number());
				lenderBean.setService_tax_number(lenderDetails.get(i).getLenderIdentity().getService_tax_number());
				lenderBean.setTan_number(lenderDetails.get(i).getLenderIdentity().getTan_number());
			}			
		}
		
		return lenderBean;
	} 

	
	public String changePassword(int lenderId,String currentPassword,String newPassword,String lenderRepresentative)
	{
		String status = "";
		String currentEncryptPassword = "";
		String newEncryptPassword = "";
		
		try {
			
			currentEncryptPassword =  pwGenerator.encrypt(currentPassword);
			newEncryptPassword =  pwGenerator.encrypt(newPassword);
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		status = lenderDao.changePassword(lenderId,currentEncryptPassword,newEncryptPassword,lenderRepresentative);
		
		return status;
	}
	
	public String saveLenderProfileIdnDetails(LenderIdentityModel lenderIdnDetails)
	{
		return lenderDao.saveLenderProfileIdnDetails(lenderIdnDetails);
	}
	
	public String getLenderProfilePhoto(int lenderId)
	{
		return lenderDao.getLenderProfilePhoto(lenderId);
	}
	
	
	public String getSubPolicyTypeName(String policyName)
	{
		String subPolicyNames = "";
		
		List<String> nameList = lenderDao.getSubPolicyTypeName(policyName);
		
		if(nameList.size()>0)
		{
			for(int i=0;i<nameList.size();i++)
			{
				subPolicyNames += nameList.get(i) + "/";
			}
			if(subPolicyNames.length()>0)
			{
				subPolicyNames = subPolicyNames.substring(0, subPolicyNames.length()-1);
			}
		}
		else
		{
			subPolicyNames = "No Data";
		}
		
		return subPolicyNames;
	}
	
	public String saveLenderPolicy(LenderPolicyModel policyModel)
	{
		String saveStatus = "";
		
		
		policyModel.setCr_date(new Date());
		policyModel.setPolicy_category("Loan");
		policyModel.setPolicy_approval_status("Pending");
		policyModel.setPolicy_intialization_status("NotAssign");
		
		saveStatus = lenderDao.saveLenderPolicy(policyModel);
		
		return saveStatus;
	}
	
	public List<LenderPolicyBean> getPolicyDetails(int policyId,int lenderId)
	{
		List<LenderPolicyBean> eBean = null;
		
		List<LenderPolicyModel> eModel = lenderDao.getPolicyDetails(policyId,lenderId);
		
		eBean = convertLenPolicyModelToBean(eModel);
		
		return eBean;
	}
	
	public List<LenderPolicyBean> getLenderPolicyDetails(int lenderId)
	{
		List<LenderPolicyBean> eBean = null;
		
		List<LenderPolicyModel> eModel = lenderDao.getLenderPolicyDetails(lenderId);
		
		eBean = convertLenPolicyModelToBean(eModel);
		
		return eBean;
	}
	
	public List<LenderPolicyBean> getSearchLenderPolicyDetails(int lenderId,String searchType,String policyTypeValue)
	{
		List<LenderPolicyBean> eBean = null;
		
		List<LenderPolicyModel> eModel = lenderDao.getSearchLenderPolicyDetails(lenderId,searchType,policyTypeValue);
		
		eBean = convertLenPolicyModelToBean(eModel);
		
		return eBean;

	}
	
	public List<LenderPolicyBean> getLenPolicyDetailViaPolicyId(int policyId)
	{
		List<LenderPolicyBean> eBean = null;
		
		List<LenderPolicyModel> eModel = lenderDao.getLenPolicyDetailViaPolicyId(policyId);
		
		eBean = convertLenPolicyModelToBean(eModel);
		
		return eBean;
	}
	
	public String getBroucherFile(int policyId)
	{
		return lenderDao.getBroucherFile(policyId);
	}
	
	
	public String getLenderPolicyType(int lenderId)
	{
		String nameOfPolicyType = "";
		
		List<String> policyTypeList = lenderDao.getLenderDistinctPolicyDetails(lenderId);
		
		if(policyTypeList.size()>0)
		{
			for(int i=0;i<policyTypeList.size();i++)
			{
				nameOfPolicyType += policyTypeList.get(i) + "/";
			}			
			if(nameOfPolicyType.length()>1)
			{
				nameOfPolicyType = nameOfPolicyType.substring(0,nameOfPolicyType.length()-1);
			}
		}
		else
		{
			nameOfPolicyType = "No Data";
		}
		
		System.out.println("Policy type-"+nameOfPolicyType);
		
		return nameOfPolicyType;
	}
	
	public List<LenderPolicyBean> getLenderLastFinancialFeature(int lenderId)
	{
		List<LenderPolicyBean> policyBean = new ArrayList<LenderPolicyBean>();
		
		List<LenderPolicyModel> policyModel = lenderDao.getLenderLastPolicyDetails(lenderId);
				
		policyBean = convertLenPolicyModelToBean(policyModel);
		
		return policyBean;
	}
	
	public List<LenderToBorrowerFinanceDealBean> getLenderLastBorrowerRequest(int lenderId)
	{
		List<LenderToBorrowerFinanceDealBean> dealBean = new ArrayList<LenderToBorrowerFinanceDealBean>();
		
		List<LenderToBorrowerFinanceDealModel> dealModel = lenderDao.getLenderLastBorrowerRequest(lenderId);
		
		dealBean = convertPolicyDealModelToBean(dealModel,"Borrower","Access");
				
		return dealBean;
	}
	
	
	public List<LenderToBorrowerFinanceDealBean> getBorrowerPolicyRequest(LenderToBorrowerFinanceDealBean policyReqBean)
	{
		List<LenderToBorrowerFinanceDealBean> eBean = new ArrayList<LenderToBorrowerFinanceDealBean>();
		
		List<LenderToBorrowerFinanceDealModel> eModel = lenderDao.getBorrowerPolicyRequest(policyReqBean);
				
		eBean = convertPolicyDealModelToBean(eModel,"Borrower","Access");
		
		//System.out.println("Bean Length-"+eBean.size()+eBean.get(0).getPolicy_name());		
		
		return eBean;
	}
	
	public List<LenderToBorrowerFinanceDealBean> getBorrowerPolicyApproved(LenderToBorrowerFinanceDealBean policyReqBean)
	{
		List<LenderToBorrowerFinanceDealBean> eBean = new ArrayList<LenderToBorrowerFinanceDealBean>();
		
		List<LenderToBorrowerFinanceDealModel> eModel = lenderDao.getBorrowerPolicyApproved(policyReqBean);
				
		eBean = convertPolicyDealModelToBean(eModel,"Borrower","Access");
		
		return eBean;
	}
	
	
	public List<LenderToBorrowerFinanceDealBean> getBorrowerInterestedRequest(LenderToBorrowerFinanceDealBean policyReqBean)
	{
		List<LenderToBorrowerFinanceDealBean> eBean = new ArrayList<LenderToBorrowerFinanceDealBean>();
		
		List<LenderToBorrowerFinanceDealModel> eModel = lenderDao.getBorrowerInterestedRequest(policyReqBean);
				
		eBean = convertPolicyDealModelToBean(eModel,"Borrower","Access");
		
		//System.out.println("Bean Length-"+eBean.size()+eBean.get(0).getPolicy_name());		
		
		return eBean;
	}
	
	
	public List<LenderToBorrowerFinanceDealBean> getBorrowerPolicyRequestDeatails(LenderToBorrowerFinanceDealBean policyReqBean)
	{
		List<LenderToBorrowerFinanceDealBean> eBean = new ArrayList<LenderToBorrowerFinanceDealBean>();
		
		List<LenderToBorrowerFinanceDealModel> eModel = lenderDao.getBorrowerPolicyRequestDeatails(policyReqBean);
				
		eBean = convertPolicyDealModelToBean(eModel,"Borrower","NoAccess");
		
		return eBean;
	}
	
	
	public List<LenderToBorrowerFinanceDealBean> getLenderAcceptedDealDetails(int lenderId)
	{
		List<LenderToBorrowerFinanceDealBean> eBean = new ArrayList<LenderToBorrowerFinanceDealBean>();
		
		List<LenderToBorrowerFinanceDealModel> eModel = lenderDao.getLenderAcceptedDealDetails(lenderId);
				
		eBean = convertPolicyDealModelToBean(eModel,"Borrower","Access");
		
		return eBean;
	}
	
	
	public List<LenderToBorrowerFinanceDealBean> getDealStatus(int dealId)
	{		
		List<LenderToBorrowerFinanceDealBean> eBean = new ArrayList<LenderToBorrowerFinanceDealBean>();
		
		List<LenderToBorrowerFinanceDealModel> eModel = lenderDao.getDealStatus(dealId);
		
		eBean = convertPolicyDealModelToBean(eModel,"Borrower","Access");
		
		return eBean;
	}
	
	public List<LenderDocumentModel> attachedDocuments(int lenderId)
	{
		return lenderDao.attachedDocuments(lenderId);
	}
	
	public List<LenderDocumentModel> getLastLenderDocRecord(int lenderId)
	{
		return lenderDao.getLastLenderDocRecord(lenderId);
	}
	
	
	
    public int getLastDocSequence(int lenderId,String doc_type)
    {
    	return lenderDao.getLastDocSequence(lenderId,doc_type);
    }
	
	public String saveLenderDocumentDetails(LenderDocumentModel lenderDoc)
	{
		return lenderDao.saveLenderDocumentDetails(lenderDoc);
	}
	
	public String approveBorrowerRequest(int dealId)
	{
		return lenderDao.approveBorrowerRequest(dealId);
	}
	
	public LenderDocumentModel getDocumentDetail(int lenDocId)
	{
		LenderDocumentModel docObj = new LenderDocumentModel();
		
		List<LenderDocumentModel> list = lenderDao.getDocumentDetail(lenDocId);
		
		for(int i=0;i<list.size();i++)
		{
			docObj.setLender_id(list.get(i).getLender_id());;
			docObj.setCr_date(list.get(0).getCr_date());
			docObj.setDocument_type(list.get(0).getDocument_type());
			docObj.setFile_name(list.get(0).getFile_name());
		}
		
		return docObj;
	}
	
	 public long countDealIntiateWithPolicyId(int policyId)
	 {
		 return lenderDao.countDealIntiateWithPolicyId(policyId);
	 }
		
     public long countDealPendingWithPolicyId(int policyId)
     {
		 return lenderDao.countDealPendingWithPolicyId(policyId);
	 }
	
     public long countDealApprovedWithPolicyId(int policyId)
     {
		 return lenderDao.countDealApprovedWithPolicyId(policyId);
	 }

     public String discontinuePolicy(int policyId)
     {
    	 return lenderDao.discontinuePolicy(policyId);
     }
	 
     public List<LenderToBorrowerFinanceDealBean> getBorrowerLoanRequest(int lenderId)
     {
    	 List<LenderToBorrowerFinanceDealBean> requestBean = new ArrayList<LenderToBorrowerFinanceDealBean>();
    	 
    	 List<LenderToBorrowerFinanceDealModel> requestModel = lenderDao.getBorrowerLoanRequest(lenderId);
    	 
    	 requestBean = convertPolicyDealModelToBean(requestModel,"Borrower","Access");
    	 
    	 return requestBean;
     }
     
     public  List<LenderToBorrowerFinanceDealBean> getBorrowerApproveLoan(int lenderId)
     {
    	 List<LenderToBorrowerFinanceDealBean> requestBean = new ArrayList<LenderToBorrowerFinanceDealBean>();
    	 
    	 List<LenderToBorrowerFinanceDealModel> requestModel = lenderDao.getBorrowerApproveLoan(lenderId);
    	 
    	 requestBean = convertPolicyDealModelToBean(requestModel,"Borrower","Access");
    	 
    	 return requestBean;
     }
     
     public String manageBorrowerRequest(int dealId,int lenderId,String interest)
     {
    	 return lenderDao.manageBorrowerRequest(dealId,lenderId,interest);
     }
	
     public String submitReason(int dealId,String reason,String rejectStage)
     {
    	 return lenderDao.submitReason(dealId,reason,rejectStage);
     }
     
     public String getRequiredDocForPolicy(int policyId)
     {
    	 return lenderDao.getRequiredDocForPolicy(policyId);
     }
	
     public List<InterPortalNotificationBean> getLenderNotification(int lenderId)
     {
    	 List<InterPortalNotificationBean> notifyBean = new ArrayList<InterPortalNotificationBean>();
    	 
    	 notifyBean = convertNotificationModelToBean(lenderDao.getLenderNotification(lenderId));
    	 
    	 return notifyBean;
     }
	
     public String deleteNoitification(int notifyId)
     {
    	 return lenderDao.deleteNoitification(notifyId);
     }
	
     public List<InterPortalNotificationBean> getNotification(int notifyId)
     {
    	 List<InterPortalNotificationBean> notifyBean = new ArrayList<InterPortalNotificationBean>();
    	 
    	 notifyBean = convertNotificationModelToBean(lenderDao.getNotification(notifyId));
    	 
    	 return notifyBean;
     }
	
     public String getUpdateNotificationViewStatus(int notifyId)
     {
    	 return lenderDao.getUpdateNotificationViewStatus(notifyId);
     }
     
     public List<InterPortalNotificationBean> getSearchNotification(int userId,String searchType,String fromDateId,String toDateId)
     {
    	 List<InterPortalNotificationBean> notifyBean = new ArrayList<InterPortalNotificationBean>();
    	 
    	 notifyBean = convertNotificationModelToBean(lenderDao.getSearchNotification(userId,searchType,fromDateId,toDateId));
    	 
    	 return notifyBean;
     }
     
     public List<InterPortalNotificationBean> getUnreadNotification(int userId,String userType)
     {
    	 List<InterPortalNotificationBean> notifyBean = new ArrayList<InterPortalNotificationBean>();
    	 
    	 notifyBean = convertNotificationModelToBean(lenderDao.getUnreadNotification(userId,userType));
    	 
    	 return notifyBean;
     }
    
     public String getUpdateAmoundDispouseStatus(int dealId,String status)
     {
    	 return lenderDao.getUpdateAmoundDispouseStatus(dealId,status);
     }
	
	
	// CONVERT POLICYDEAL MODEL TO BEAN 
	
	
	public List<LenderToBorrowerFinanceDealBean> convertPolicyDealModelToBean(List<LenderToBorrowerFinanceDealModel> eModel,String user,String visibility)
	{
		List<LenderToBorrowerFinanceDealBean> eBean = new ArrayList<LenderToBorrowerFinanceDealBean>();
		
		for(int i=0;i<eModel.size();i++)
		{					
			LenderToBorrowerFinanceDealBean dealValue = new LenderToBorrowerFinanceDealBean();
			
			dealValue.setDeal_id(eModel.get(i).getDeal_id());
			dealValue.setLender_id(eModel.get(i).getLender_id());
			dealValue.setBorrower_id(eModel.get(i).getBorrower_id());
			dealValue.setDeal_number(eModel.get(i).getDeal_number());
			dealValue.setPolicy_id(eModel.get(i).getPolicy_id());
			dealValue.setPolicy_name(eModel.get(i).getPolicy_name());
			dealValue.setPolicy_type(eModel.get(i).getPolicy_type());
			dealValue.setDeal_interest_rate(eModel.get(i).getDeal_interest_rate());
			dealValue.setDeal_amount(eModel.get(i).getDeal_amount());
			dealValue.setDeal_duration(eModel.get(i).getDeal_duration());
			dealValue.setIncome_type(eModel.get(i).getIncome_type());
			dealValue.setIncome_annualy(eModel.get(i).getIncome_annualy());			
			dealValue.setNote_to_lender(eModel.get(i).getNote_to_lender());
			dealValue.setScanned_file_name(eModel.get(i).getScanned_file_name());
			dealValue.setDeal_status(eModel.get(i).getDeal_status());
			
			dealValue.setDeal_initiated_date(eModel.get(i).getDeal_initiated_date());
			dealValue.setDeal_finalize_date(eModel.get(i).getDeal_finalize_date());
			dealValue.setExtra_info(eModel.get(i).getExtra_info());
			dealValue.setLender_status(eModel.get(i).getLender_status());
			
			dealValue.setTwo_wheeler_status(eModel.get(i).getTwo_wheeler_status());
			dealValue.setHouse_status(eModel.get(i).getHouse_status());
			dealValue.setFour_wheeler_status(eModel.get(i).getFour_wheeler_status());
			dealValue.setAmount_dispouse(eModel.get(i).getAmount_dispouse());
			dealValue.setAmount_dispouse_date(eModel.get(i).getAmount_dispouse_date());
			
			if(user.equals("Borrower"))
			{
				if(!visibility.equals("NoAccess"))
				{
					List<BorrowerRegistrationDetails> bowDetails = controllerDao.getBorrowerDetails(eModel.get(i).getBorrower_id());
					
					dealValue.setBorrower_name(bowDetails.get(0).getBorrower_name());
					dealValue.setBorrower_emailid(bowDetails.get(0).getBorrower_emailid());
					dealValue.setBorrower_phno(bowDetails.get(0).getBorrower_phno());	
					
					dealValue.setBorrower_city(bowDetails.get(0).getBorrowerIdentity().getBorrower_present_city());
					
					dealValue.setGender(bowDetails.get(0).getBorrower_gender());
				}							
			}
			
			if(user.equals("Lender"))
			{
				List<LenderRegistrationDetails> lenDetails = controllerDao.getLenderDetails(eModel.get(i).getLender_id());
				
				dealValue.setLender_name(lenDetails.get(0).getFull_name());
				dealValue.setLender_emailid(lenDetails.get(0).getLender_emailid());
				dealValue.setLender_phno(lenDetails.get(0).getLender_phno());
			}
			
			eBean.add(dealValue);
		}
		
		return eBean;
	}
		
		
	
	
	
	///   CONVERT LENDER POLICY MODEL TO POLICY BEAN 
	
	public List<LenderPolicyBean> convertLenPolicyModelToBean(List<LenderPolicyModel> eModel)
	{		
        List<LenderPolicyBean> eBean = new ArrayList<LenderPolicyBean>();
		
        for(int i=0;i<eModel.size();i++)
		{			        	
        	LenderPolicyBean lenPolicyValue = new LenderPolicyBean();
        	
        	lenPolicyValue.setPolicy_id(eModel.get(i).getPolicy_id());
        	lenPolicyValue.setLender_id(eModel.get(i).getLender_id());
        	lenPolicyValue.setPolicy_name(eModel.get(i).getPolicy_name());
        	lenPolicyValue.setPolicy_type(eModel.get(i).getPolicy_type());
        	lenPolicyValue.setPolicy_category(eModel.get(i).getPolicy_category());
        	lenPolicyValue.setPolicy_min_amount(eModel.get(i).getPolicy_min_amount());
        	lenPolicyValue.setPolicy_max_amount(eModel.get(i).getPolicy_max_amount());
        	lenPolicyValue.setPolicy_interest_rate(eModel.get(i).getPolicy_interest_rate());
        	lenPolicyValue.setPolicy_broucher(eModel.get(i).getPolicy_broucher());
        	lenPolicyValue.setPolicy_min_duration(eModel.get(i).getPolicy_min_duration());
        	lenPolicyValue.setPolicy_max_duration(eModel.get(i).getPolicy_max_duration());
        	lenPolicyValue.setPolicy_required_doc(eModel.get(i).getPolicy_required_doc());
        	lenPolicyValue.setCr_date(eModel.get(i).getCr_date());
        	lenPolicyValue.setPolicy_approval_status(eModel.get(i).getPolicy_approval_status());
        	lenPolicyValue.setPolicy_intialization_status(eModel.get(i).getPolicy_intialization_status());
        	
        	lenPolicyValue.setPolicy_start_date(eModel.get(i).getPolicy_start_date());
        	lenPolicyValue.setPolicy_end_date(eModel.get(i).getPolicy_end_date());
        	lenPolicyValue.setPolicy_age_limit(eModel.get(i).getPolicy_age_limit());
        	lenPolicyValue.setPolicy_for_gender(eModel.get(i).getPolicy_for_gender());
        	lenPolicyValue.setPolicy_applicable_location(eModel.get(i).getPolicy_applicable_location());
        	
        	
			eBean.add(lenPolicyValue);
			
			lenPolicyValue = null;
			
			// System.gc();
		}
		return eBean;		
	}
	
	
	
	
	
	
	
	
	
	
	// CONVERT NOTIFICATION MODEL TO BEAN 
	
	
			public List<InterPortalNotificationBean> convertNotificationModelToBean(List<InterPortalNotificationModel> eModel)
			{
				List<InterPortalNotificationBean> eBean = new ArrayList<InterPortalNotificationBean>();
				
				for(int i=0;i<eModel.size();i++)
				{					
					InterPortalNotificationBean dealValue = new InterPortalNotificationBean();
					
					dealValue.setNotify_id(eModel.get(i).getNotify_id());
					dealValue.setNotify_extra_info_1(eModel.get(i).getNotify_extra_info_1());
					dealValue.setNotify_date(eModel.get(i).getNotify_date());
					dealValue.setNotify_extra_info_2(eModel.get(i).getNotify_extra_info_2());
					dealValue.setNotify_message(eModel.get(i).getNotify_message());
					dealValue.setNotify_reciver(eModel.get(i).getNotify_reciver());
					dealValue.setNotify_reciver_type(eModel.get(i).getNotify_reciver_type());
					dealValue.setNotify_regarding(eModel.get(i).getNotify_regarding());
					dealValue.setNotify_sender(eModel.get(i).getNotify_sender());
					dealValue.setNotify_sender_type(eModel.get(i).getNotify_sender_type());
					dealValue.setNotify_status(eModel.get(i).getNotify_status());
					dealValue.setNotify_subject(eModel.get(i).getNotify_subject());
					
					eBean.add(dealValue);
				}
				
				return eBean;
			}
			
	
}
