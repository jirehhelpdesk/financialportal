package com.finance.service;

import java.util.List;

import com.finance.bean.BorrowerDetailsBean;
import com.finance.bean.InterPortalNotificationBean;
import com.finance.bean.LenderPolicyBean;
import com.finance.bean.LenderToBorrowerFinanceDealBean;
import com.finance.domain.BorrowerDocumentModel;
import com.finance.domain.BorrowerIdentityModel;
import com.finance.domain.BorrowerRegistrationDetails;
import com.finance.domain.BorrowerSearchedDealDomain;
import com.finance.domain.LenderToBorrowerFinanceDealModel;

public interface BorrowerService {

	public String saveBorrowerRegdetails(BorrowerRegistrationDetails borrowerRegDetails);
	
	public String getValidateBorrower(String userId,String password);
	
	public int getBorrowerIdVieEmailId(String emailId);
	
	public String sendLinkForgetPassword(String emailId);
	
	public String getEmailIdFromForgetPassword(String uniqueId);
	
	public String getChangePassword(String emailId,String password);
	
	public String getEmailIdViaUnqid(String activeAccountId);
	
	public String getActiveAcount(String userType,String emailId,String activeAccountId);
	
	public String checkRegisterEmailId(String emailId,String userType);
	
	public String checkBorrowerPhno(String phno,String userType);
	
	public String checkBorrowerPanId(String panId,String userType);
	
	public BorrowerDetailsBean getBorrowerDetails(int borrowerId);
	
	public String updateBorrowerIdentityDetails(BorrowerIdentityModel borrowerIdnDetails);
	
	public String getProfilePhoto(int borrowerId);
	
	public String getChangePassword(int borrowerId,String password);
	
	public List<LenderPolicyBean> getSearchPolicyList(String type,String amount,String time,String loccation);
	
	public List<LenderPolicyBean> getPolicyInformation(int policyId);
	
	public String getUpdateBorrowerDetails(BorrowerIdentityModel borrowerIdnDetails);
	
	public String savePolicyDealBetweenBorLen(LenderToBorrowerFinanceDealModel dealModel);
	
	public List<LenderToBorrowerFinanceDealBean> getBorrowerPolicyDealDetails(int borrowerId);
	
	public List<LenderToBorrowerFinanceDealBean> getBorrowerPolicyDealDetailsViaDealId(int dealId);
	
	public String getPolicyComponent(String policyType);
	
	public List<LenderToBorrowerFinanceDealBean> getLastDealDetails(int borrowerId);
	
	public List<LenderPolicyBean> getLastSearchDeals(int borrowerId);
	
	public String saveBorrowerSearchPolicy(BorrowerSearchedDealDomain searchModel);
		
	public List<InterPortalNotificationBean> getNotification(String userType,int userId);
	
	public List<InterPortalNotificationBean> getNotifyDetails(String userType,int notifyId);
	
	public List<BorrowerDocumentModel> getLastBorrowerDocRecord(int borrowerId);
	
	public String getBorrowerDocList();
	
	public int getLastDocSequence(int borrowerId,String doc_type);
	
	public String saveDocumentDetails(BorrowerDocumentModel borrowerDocs);
	
	public BorrowerDocumentModel getDocumentDetail(int documentId);
	
	public List<BorrowerDocumentModel> attachedDocuments(int borrowerId);
	
	public String checkDocumentExistence(int borrowerId,String required_doc);
	
	public int getDocumentIdViaDocType(int borrowerId,String docType);
	
	public String activateAccountViaOTP(String otpNumber);
	
	public String generateOTPForuser(String userId);
	
}
