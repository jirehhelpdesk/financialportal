package com.finance.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.finance.bean.BorrowerDetailsBean;
import com.finance.bean.ControllerDetailsBean;
import com.finance.bean.LenderDetailsBean;
import com.finance.bean.LenderPolicyBean;
import com.finance.bean.PortalStatusDasboardBean;
import com.finance.domain.AdminDetails;
import com.finance.domain.ContactUsModel;
import com.finance.domain.ControllerDetails;
import com.finance.domain.FinancePolicyDomain;
import com.finance.domain.InterPortalNotificationModel;
import com.finance.domain.SearchLoanModel;

public interface AdminService {

	
	public List<AdminDetails> getAdminDetails();
	
	public String getValidateAdmin(String user_id,String password);
	
	public String saveControllerdetails(ControllerDetails controllerDetails);
	
	public List<ControllerDetails> searchController(String searchType,String searchValue);
	
	public String updateControllerStatus(int controller_id,String requestedStatus);
	
	public List<BorrowerDetailsBean> searchBorrower(String type,String value);
	
	public String changeAdminPassword(String curPassword,String newPassword);	
	
	public String getUpdateAdminDetails(String name,String role,String fileName);
	
	public String savePolicyTypeDetails(FinancePolicyDomain financialPolicy);
	
	public List<FinancePolicyDomain> getLoanType(String type,String value);
	
	public String deletePolicyType(int typeId);
	
	public String getPolicyType();
	
	public String getPolicyTypeName();
	
	public List<LenderPolicyBean> policyListAsPerFilter(String type,String value);
	
	public List<LenderPolicyBean> getLenPolicyDetailsViaPolicyId(int policyId);
	
	public String updatePolicyByAdmin(int policyId,String policyStatus);
	
	public PortalStatusDasboardBean getUpdateOnPortalStatus();
	
	public String updateBorrowerAccess(int borrowerId,String currentStatus);
	
	public String updateLenderAccess(int lenderId,String currentStatus);
	
	public String getFindUser(String userType,String searchType,String searchValue);
	
	

	public List<BorrowerDetailsBean> getBorrowerDetails(String receiver_emailid);
	
	public List<LenderDetailsBean> getLenderDetails(String receiver_emailid);
	
	public List<ControllerDetailsBean> getControllerDetails(String receiver_emailid);
	
	public String getSaveUserNotifyDetails(InterPortalNotificationModel notifyModel,HttpServletRequest request);
	
	public String saveConatactFormDetails(ContactUsModel contactDetail);
	
	public String getPolicyNames();
	
	public String saveSearchLoanDetails(SearchLoanModel searchLoanModel);
	
	
	
	
	
	
}
