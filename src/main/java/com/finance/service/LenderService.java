package com.finance.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.finance.bean.InterPortalNotificationBean;
import com.finance.bean.LenderDetailsBean;
import com.finance.bean.LenderPolicyBean;
import com.finance.bean.LenderToBorrowerFinanceDealBean;
import com.finance.domain.BorrowerRegistrationDetails;
import com.finance.domain.LenderDocumentModel;
import com.finance.domain.LenderIdentityModel;
import com.finance.domain.LenderPolicyModel;
import com.finance.domain.LenderRegistrationDetails;
import com.finance.domain.LenderToBorrowerFinanceDealModel;

public interface LenderService {

	public String saveLenderRegdetails(LenderRegistrationDetails lenderRegDetails);
	
	public String getValidateLender(String userId,String password,HttpSession session);
	
	public int getLenderIdViaUserId(String userId,HttpSession session);
	
	public String sendLinkForgetPassword(String emailId);
	
	public String getEmailIdFromForgetPassword(String uniqueId);
	
	public String getChangePassword(String emailId,String password);
	
	public LenderDetailsBean getLenderDetails(int lenderId);
	
	public String changePassword(int lenderId,String currentPassword,String newPassword,String lenderRepresentative);
	
	public String saveLenderProfileIdnDetails(LenderIdentityModel lenderIdnDetails);
	
	public String getLenderProfilePhoto(int lenderId);
	
	public String getSubPolicyTypeName(String policyName);
	
	public String saveLenderPolicy(LenderPolicyModel policyModel);
	
	public List<LenderPolicyBean> getPolicyDetails(int policyId,int lenderId);
	
	public List<LenderPolicyBean> getLenderPolicyDetails(int lenderId);
	
	public List<LenderPolicyBean> getSearchLenderPolicyDetails(int lenderId,String searchType,String policyTypeValue);
	
	public String getLenderPolicyType(int lenderId);
	
	public List<LenderToBorrowerFinanceDealBean> getBorrowerPolicyRequest(LenderToBorrowerFinanceDealBean policyReqBean);
	
	public List<LenderToBorrowerFinanceDealBean> getBorrowerPolicyApproved(LenderToBorrowerFinanceDealBean policyReqBean);
	
	public List<LenderToBorrowerFinanceDealBean> getBorrowerPolicyRequestDeatails(LenderToBorrowerFinanceDealBean policyReqBean);
	
	public List<LenderPolicyBean> getLenderLastFinancialFeature(int lenderId);
	
	public List<LenderToBorrowerFinanceDealBean> getLenderLastBorrowerRequest(int lenderId);
	
	public List<LenderToBorrowerFinanceDealBean> getLenderAcceptedDealDetails(int lenderId);
	
	public List<LenderToBorrowerFinanceDealBean> getDealStatus(int dealId);
	
	public List<LenderDocumentModel> attachedDocuments(int lenderId);
	
	public List<LenderDocumentModel> getLastLenderDocRecord(int lenderId);
	
	public int getLastDocSequence(int lenderId,String doc_type);
		
    public String saveLenderDocumentDetails(LenderDocumentModel lenderDoc);
		
    public LenderDocumentModel getDocumentDetail(int lenDocId);	
		
    public String getBroucherFile(int policyId);
    
    
    public long countDealIntiateWithPolicyId(int policyId);
	
    public long countDealPendingWithPolicyId(int policyId);
	
    public long countDealApprovedWithPolicyId(int policyId);
    
    public String discontinuePolicy(int policyId);
    
    public List<LenderToBorrowerFinanceDealBean> getBorrowerLoanRequest(int lenderId);
    
    public String manageBorrowerRequest(int dealId,int lenderId,String interest);
    
    public  List<LenderToBorrowerFinanceDealBean> getBorrowerApproveLoan(int lenderId);
    
    public List<LenderToBorrowerFinanceDealBean> getBorrowerInterestedRequest(LenderToBorrowerFinanceDealBean policyReqBean);
    
    public String getRequiredDocForPolicy(int policyId);
    
    public List<InterPortalNotificationBean> getLenderNotification(int lenderId);
    
    public String approveBorrowerRequest(int dealId);
    
    public String deleteNoitification(int notifyId);
    
    
    public List<InterPortalNotificationBean> getNotification(int notifyId);
    
    public List<InterPortalNotificationBean> getSearchNotification(int userId,String searchType,String fromDateId,String toDateId);
    
    
    public List<InterPortalNotificationBean> getUnreadNotification(int userId,String userType);
    
    public String getUpdateNotificationViewStatus(int notifyId);
    
    public String submitReason(int dealId,String reason,String rejectStage);
    
    public String getUpdateAmoundDispouseStatus(int dealId,String status);
    
    
    
    
}
