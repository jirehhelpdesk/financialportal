package com.finance.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finance.bean.BorrowerDetailsBean;
import com.finance.bean.ControllerDetailsBean;
import com.finance.bean.LenderDetailsBean;
import com.finance.bean.LenderPolicyBean;
import com.finance.bean.PortalStatusDasboardBean;
import com.finance.dao.AdminDao;
import com.finance.dao.ControllerDao;
import com.finance.domain.AdminDetails;
import com.finance.domain.BorrowerRegistrationDetails;
import com.finance.domain.ContactUsModel;
import com.finance.domain.ControllerDetails;
import com.finance.domain.EmailModel;
import com.finance.domain.FinancePolicyDomain;
import com.finance.domain.InterPortalNotificationModel;
import com.finance.domain.LenderPolicyModel;
import com.finance.domain.LenderRegistrationDetails;
import com.finance.domain.SearchLoanModel;
import com.finance.util.EmailNotificationUtil;
import com.finance.util.EmailSentUtil;
import com.finance.util.PasswordGenerator;


@Service("adminService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AdminServiceImpl implements AdminService  {

	
	@Autowired
	private AdminDao adminDao;
	
	@Autowired
	private ControllerDao controllerDao;
	
	
	PasswordGenerator pwGenerator = new PasswordGenerator();
	
	public List<AdminDetails> getAdminDetails()
	{
		return adminDao.getAdminDetails();
	}
	
	@Transactional(readOnly = false)
	public String getValidateAdmin(String user_id,String password)
	{
		String encPassword = "";
		try {
			 encPassword = pwGenerator.encrypt(password);
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return adminDao.getValidateAdmin(user_id,encPassword);
	}
	
	public String getPolicyNames()
	{
		String nameList = "";
		
		List<FinancePolicyDomain> policyDetail = adminDao.getPolicyDetails();
		
		for(int i=0;i<policyDetail.size();i++)
		{
			 nameList += policyDetail.get(i).getSub_policy_type() + ",";
		}
		
		if(!nameList.equals(""))
		{
			nameList = nameList.substring(0, nameList.length()-1);
		}
		
		return nameList;
	}
	
	public String saveConatactFormDetails(ContactUsModel contactDetail)
	{
		contactDetail.setContact_date(new Date());
		contactDetail.setContact_status("Not Viewed");
		
		String saveStatus = adminDao.saveConatactFormDetails(contactDetail);
		
		if(saveStatus.equals("success"))
		{
			EmailNotificationUtil emailUtil = new EmailNotificationUtil();
			
			String link = "http://192.168.1.6:8082/FP";
			
			String reasonFor = "Contact Us";
			
			String extraInfo = "";
			
			String emailBody = emailUtil.emailNotification(contactDetail.getContact_name(),contactDetail.getContact_email_id(),"resources/contactUsNotify",link,"Go To Home",reasonFor,extraInfo);
			
			String mailStatus = "";
			
			ResourceBundle resource = ResourceBundle.getBundle("resources/contactUsNotify");
		 	 
			String mailSubject = resource.getString("mailSubject");
			
			
			/* SENDING MAIL */
		    
			
				    EmailSentUtil emailStatus = new EmailSentUtil();
		 	        
					try {
						mailStatus = emailStatus.getEmailSent(contactDetail.getContact_email_id(),mailSubject,emailBody);
					} 
					catch (Exception e) {
						
						// TODO Auto-generated catch block
						mailStatus = "failed";
						e.printStackTrace();
					}
					
		  
		    /* END OF SENDING MAIL */
					
			
			if(!mailStatus.equals("success"))
			{
				EmailModel emailDetail = new EmailModel();
				
				emailDetail.setEmail_id(contactDetail.getContact_email_id());
				emailDetail.setEmail_subject(mailSubject);
				emailDetail.setEmail_content(emailBody);
				emailDetail.setEmail_status("failed");
				emailDetail.setCr_date(new Date());
				
				adminDao.getSaveEmailDetails(emailDetail);
			}
						
		}
		
		return saveStatus;
	}
	
	
	
	
	
	@Transactional(readOnly = false)
	public String saveControllerdetails(ControllerDetails controllerDetails)
	{
		String encPassword = "";
		
		String password = "";
		
		try {
			 
			if(controllerDetails.getController_password()!=null)
			{
				password = controllerDetails.getController_password();
				encPassword = pwGenerator.encrypt(password);
			}
			else
			{				
				password  = controllerDetails.getController_name().substring(0, 2)+controllerDetails.getController_user_id().substring(0, 2)+"4w6";				
				encPassword = pwGenerator.encrypt(password);
			}
			
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
		
			e.printStackTrace();
		}
		
		controllerDetails.setController_password(encPassword);
		
		String saveStatus = adminDao.saveControllerdetails(controllerDetails);
		
		if(saveStatus.equals("success"))
		{
			EmailNotificationUtil emailUtil = new EmailNotificationUtil();
			
			String link = "http://192.168.1.6:8081/FP/controllerLogin";
			
			String reasonFor = "ControllerReg";
			
			String extraInfo = controllerDetails.getController_user_id()+"/"+password;
			
			String emailBody = emailUtil.emailNotification(controllerDetails.getController_name(),controllerDetails.getController_user_id(),"resources/controllerRegistration",link,"Go To Controller",reasonFor,extraInfo);
			
			String mailStatus = "";
			
			ResourceBundle resource = ResourceBundle.getBundle("resources/controllerRegistration");
		 	 
			String mailSubject = resource.getString("mailSubject");
			
			
			/* SENDING MAIL */
		    
			
				    EmailSentUtil emailStatus = new EmailSentUtil();
		 	        
					try {
						mailStatus = emailStatus.getEmailSent(controllerDetails.getController_user_id(),mailSubject,emailBody);
					} 
					catch (Exception e) {
						
						// TODO Auto-generated catch block
						mailStatus = "failed";
						e.printStackTrace();
					}
					
		  
		    /* END OF SENDING MAIL */
					
			
			if(!mailStatus.equals("success"))
			{
				EmailModel emailDetail = new EmailModel();
				
				emailDetail.setEmail_id(controllerDetails.getController_user_id());
				emailDetail.setEmail_subject(mailSubject);
				emailDetail.setEmail_content(emailBody);
				emailDetail.setEmail_status("failed");
				emailDetail.setCr_date(new Date());
				
				adminDao.getSaveEmailDetails(emailDetail);
			}
						
		}
		
		return saveStatus;
	} 
	
	@Transactional(readOnly = false)
	public List<ControllerDetails> searchController(String searchType,String searchValue)
	{		
		return adminDao.searchController(searchType,searchValue);
	}
	
	@Transactional(readOnly = false)
	public String updateControllerStatus(int controller_id,String requestedStatus)
	{
		return adminDao.updateControllerStatus(controller_id,requestedStatus);
	}
	
	public String getFindUser(String userType,String searchType,String searchValue)
	{
		String resultValue = "";
		
		resultValue = adminDao.getFindUser(userType,searchType,searchValue);
		
		return resultValue;
	}
	
	@Transactional(readOnly = false)
	public List<BorrowerDetailsBean> searchBorrower(String type,String value)
	{
		List<BorrowerRegistrationDetails> borrowerDetails = adminDao.getBorrowerDetails(type,value);
		
		List<BorrowerDetailsBean> beanDetails = new ArrayList<BorrowerDetailsBean>();
		
		beanDetails = convertBorrowerModelToBean(borrowerDetails);
		
		return beanDetails;
	}
	
	public String updateBorrowerAccess(int borrowerId,String currentStatus)
	{
		return adminDao.updateBorrowerAccess(borrowerId,currentStatus);
	}
	
	
	// ADMIN SETTING ???????????????????????
	
	
	public String changeAdminPassword(String curPassword,String newPassword)
	{
		String encCurPassword = "";
		String encNewPassword = "";
		
		String changeStatus = "";
		
		try 
		{
			
			encCurPassword = pwGenerator.encrypt(curPassword);
			encNewPassword = pwGenerator.encrypt(newPassword);
						
		} 
		catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		changeStatus = adminDao.checkAdminPassword(encCurPassword);
		
		if(changeStatus.equals("Matched"))
		{
			changeStatus = adminDao.changeAdminPassword(encNewPassword);
		}
		
		
		return changeStatus;
	}
	
	
	public String getUpdateAdminDetails(String name,String role,String fileName)
	{		
		return adminDao.getUpdateAdminDetails(name,role,fileName);
	}
	
	
	public String savePolicyTypeDetails(FinancePolicyDomain financialPolicy)
	{
		return adminDao.savePolicyTypeDetails(financialPolicy);
	}
	
	public List<FinancePolicyDomain> getLoanType(String type,String value)
	{
		return adminDao.getLoanType(type,value);
	}
	
	public String deletePolicyType(int typeId)
	{
		return adminDao.deletePolicyType(typeId);
	}
	
	public String getPolicyType()
	{
		
		List<String> policyType = adminDao.getPolicyType();
		
		String policyTypeNames = "";
		
		if(policyType.size()>0)
		{
			for(int i=0;i<policyType.size();i++)
			{
				policyTypeNames += policyType.get(i) + "/";
			}
			
			if(!policyTypeNames.equals(""))
			{
				policyTypeNames = policyTypeNames.substring(0, policyTypeNames.length()-1);
			}
		}
		else
		{
			policyTypeNames += "No Data"; 
		}
		
		return policyTypeNames;
	}
	
	
	
	public String getPolicyTypeName()
	{
		String nameList = "";
		
		List<FinancePolicyDomain> nameModel = adminDao.getPolicyTypeName();
		
		for(int i=0;i<nameModel.size();i++)
		{
			nameList += nameModel.get(i).getSub_policy_type() + "/";
		}
		
		if(nameList.length()>0)
		{
			nameList = nameList.substring(0, nameList.length()-1);
		}
		
		return nameList;
	}
	
	public List<LenderPolicyBean> policyListAsPerFilter(String type,String value)
	{
		List<LenderPolicyBean> eBean = new ArrayList<LenderPolicyBean>();
		
		List<LenderPolicyModel> eModel = new ArrayList<LenderPolicyModel>();
		
		String condititonFilter = "";
		
		if(type.equals("Name"))
		{
			condititonFilter = "Identity";
		}
		
		if(type.equals("Email"))
		{
			condititonFilter = "Identity";
		}
		
		if(condititonFilter.equals("Identity"))
		{			
			List<LenderRegistrationDetails> lenderModel = adminDao.searchLenderAsPerFilter(type,value);
			
			List<Integer> lenderIdList = new ArrayList<Integer>();
			
			for(int i=0;i<lenderModel.size();i++)
			{
				lenderIdList.add(lenderModel.get(i).getLender_id());
			}
			
			for(int i=0;i<lenderIdList.size();i++)
			{
				List<LenderPolicyModel> eModel1  = adminDao.getPolicyDetailsViaLenderId(lenderIdList.get(i));
				eModel.addAll(eModel1);
			}	
			
			eBean = convertLenderPolicyModelToBean(eModel);
		}
		else
		{
			 eModel = adminDao.getPolicyDetailsViaType(type);	
			 
			 eBean = convertLenderPolicyModelToBean(eModel);
		}
		
		return eBean;
	}
	
	public List<LenderPolicyBean> getLenPolicyDetailsViaPolicyId(int policyId)
	{
		List<LenderPolicyBean> eBean = new ArrayList<LenderPolicyBean>();
		
		List<LenderPolicyModel> eModel = adminDao.getLenPolicyDetailsViaPolicyId(policyId);
		
		eBean = convertLenderPolicyModelToBean(eModel);
		
		return eBean;
	}
	
	public String updatePolicyByAdmin(int policyId,String policyStatus)
	{
		String status = "";
		
		status = adminDao.updatePolicyByAdmin(policyId,policyStatus);
		
		return status;
	}
	
	
	
	
	
	
	public List<LenderRegistrationDetails> getLenderDetails(int lenderId)
	{
		return adminDao.getLenderDetails(lenderId);
	}
	
	
	public PortalStatusDasboardBean getUpdateOnPortalStatus()
	{		
		PortalStatusDasboardBean statusBean = new PortalStatusDasboardBean();
		
		
		statusBean.setNoOfBorrower(adminDao.countNoOfBorrower());
		statusBean.setNoOfLender(adminDao.countNoOfLender());
		statusBean.setNoOfController(adminDao.countNoOfController());
		statusBean.setNoOfPolicy(adminDao.countNoOfPolicy());
		statusBean.setNoOfAppliedDeals(adminDao.countNoOfAppliedPolicy());
		statusBean.setNoOfFinalizedDeals(adminDao.countNoOfFinalizedPolicy());
		statusBean.setNoOfOnholdDeals(adminDao.countNoOfOnholdPolicy());
		statusBean.setNoOfProcessingDeals(adminDao.countNoOfProcessPolicy());
		statusBean.setNoOfApprovedPolicy(adminDao.countNoOfApprovedPolicy());
		statusBean.setNoOfClosedPolicy(adminDao.countNoOfClosedPolicy());
		statusBean.setNoOfPendingPolicy(adminDao.countNoOfPendingPolicy());
		statusBean.setNoOfOnSearchLoans(adminDao.countNoOfSearchLoan());
		
		
		return statusBean;
	}
	
	
	public String updateLenderAccess(int lenderId,String currentStatus)
	{
		return adminDao.updateLenderAccess(lenderId,currentStatus);
	}
	
	


	public List<BorrowerDetailsBean> getBorrowerDetails(String receiver_emailid)
	{
		List<BorrowerDetailsBean> eBean = new ArrayList<BorrowerDetailsBean>();
		
		List<BorrowerRegistrationDetails> eModel = adminDao.getBorrowerDetails(receiver_emailid);
		
		 for(int i=0;i<eModel.size();i++)
		 {			
	        	BorrowerDetailsBean borrowerValue = new BorrowerDetailsBean();
	        	
				borrowerValue.setBorrower_id(eModel.get(i).getBorrower_id());
				borrowerValue.setBorrower_gender(eModel.get(i).getBorrower_gender());
				borrowerValue.setBorrower_name(eModel.get(i).getBorrower_name());
				borrowerValue.setBorrower_emailid(eModel.get(i).getBorrower_emailid());
				borrowerValue.setBorrower_phno(eModel.get(i).getBorrower_phno());
				borrowerValue.setBorrower_status(eModel.get(i).getBorrower_status());
				borrowerValue.setBorrower_pan_id(eModel.get(i).getBorrower_pan_id());
				
				eBean.add(borrowerValue);				
		 }
		 
		return eBean;
	}
	
	public List<LenderDetailsBean> getLenderDetails(String receiver_emailid)
	{
		List<LenderDetailsBean> eBean = new ArrayList<LenderDetailsBean>();
		
		List<LenderRegistrationDetails> eModel = adminDao.getLenderDetails(receiver_emailid);
		
		for(int i=0;i<eModel.size();i++)
		{
			LenderDetailsBean lenderBean = new LenderDetailsBean();
			
			lenderBean.setLender_id(eModel.get(i).getLender_id());
			lenderBean.setFull_name(eModel.get(i).getFull_name());
			lenderBean.setLender_emailid(eModel.get(i).getLender_emailid());
			lenderBean.setLender_phno(eModel.get(i).getLender_phno());
			lenderBean.setLender_status(eModel.get(i).getLender_status());
			lenderBean.setLender_type(eModel.get(i).getLender_type());
			
			eBean.add(lenderBean);
		}
		
		
		return eBean;
	}
	
	public List<ControllerDetailsBean> getControllerDetails(String receiver_emailid)
	{
		List<ControllerDetailsBean> eBean = new ArrayList<ControllerDetailsBean>();
		
		List<ControllerDetails> eModel = adminDao.getControllerDetails(receiver_emailid);
		
		for(int i=0;i<eModel.size();i++)
		{
			ControllerDetailsBean controllerBean = new ControllerDetailsBean();
			
			controllerBean.setController_id(eModel.get(i).getController_id());
			controllerBean.setController_name(eModel.get(i).getController_name());
			controllerBean.setController_user_id(eModel.get(i).getController_user_id());
			controllerBean.setController_Phno(eModel.get(i).getController_Phno());
			controllerBean.setController_access_status(eModel.get(i).getController_access_status());
			
			eBean.add(controllerBean);
		}
		
		return eBean;
	}
	
	
	public String getSaveUserNotifyDetails(InterPortalNotificationModel notifyModel,HttpServletRequest request)
	{
		String saveStatus = controllerDao.getSaveNotifyDetails(notifyModel);
		
		if(saveStatus.equals("success"))
		{			
			EmailNotificationUtil emailUtil = new EmailNotificationUtil();
			
			String name =(String)request.getAttribute("name");
			
			String notifyEmailId = (String)request.getAttribute("emailId");
			
			String link = "http://192.168.1.6:8081/FP";
			
			String reasonFor = "NotifyMessage";
			
			String extraInfo = notifyModel.getNotify_message();
			
			String emailBody = emailUtil.emailNotification(name,notifyEmailId,"resources/interNotifyMessage",link,"Go to Portal",reasonFor,extraInfo);
			
			String mailStatus = "";
			
			/* SENDING MAIL */
			
			    EmailSentUtil emailStatus = new EmailSentUtil();
	 	        
				try {
					mailStatus = emailStatus.getEmailSent(notifyEmailId,notifyModel.getNotify_subject(),emailBody);
				} 
				catch (Exception e) {
					
					// TODO Auto-generated catch block
					mailStatus = "failed";
					e.printStackTrace();
				}
				
		    /* END OF SENDING MAIL */
					
			
			if(!mailStatus.equals("success"))
			{
				EmailModel emailDetail = new EmailModel();
				
				emailDetail.setEmail_id(notifyEmailId);
				emailDetail.setEmail_subject(notifyModel.getNotify_subject());
				emailDetail.setEmail_content(emailBody);
				emailDetail.setEmail_status("failed");
				emailDetail.setCr_date(new Date());
				
				adminDao.getSaveEmailDetails(emailDetail);
			}
		}
		
		return saveStatus;
	}
	
	public String saveSearchLoanDetails(SearchLoanModel searchLoanModel)
	{
		return adminDao.saveSearchLoanDetails(searchLoanModel);
	}
	
	
	
	
	
	
	
	
	
	// CONVERT LENDERPOLICY MODEL TO BEAN 
	
	
	public List<LenderPolicyBean> convertLenderPolicyModelToBean(List<LenderPolicyModel> eModel)
	{
		List<LenderPolicyBean> eBean = new ArrayList<LenderPolicyBean>();
		
		for(int i=0;i<eModel.size();i++)
		{
			List<LenderRegistrationDetails> lenderDetails = getLenderDetails(eModel.get(i).getLender_id());
					
			LenderPolicyBean policyValue = new LenderPolicyBean();
			
			policyValue.setLender_name(lenderDetails.get(0).getFull_name());
			policyValue.setLender_emailId(lenderDetails.get(0).getLender_emailid());
			
			policyValue.setPolicy_id(eModel.get(i).getPolicy_id());
			policyValue.setLender_id(eModel.get(i).getLender_id());
			policyValue.setPolicy_name(eModel.get(i).getPolicy_name());
			policyValue.setPolicy_category(eModel.get(i).getPolicy_category());
			policyValue.setPolicy_type(eModel.get(i).getPolicy_type());
			policyValue.setPolicy_min_amount(eModel.get(i).getPolicy_min_amount());
			policyValue.setPolicy_max_amount(eModel.get(i).getPolicy_max_amount());
			policyValue.setPolicy_interest_rate(eModel.get(i).getPolicy_interest_rate());
			policyValue.setPolicy_broucher(eModel.get(i).getPolicy_broucher());
			policyValue.setPolicy_min_duration(eModel.get(i).getPolicy_min_duration());
			policyValue.setPolicy_max_duration(eModel.get(i).getPolicy_max_duration());
			policyValue.setPolicy_required_doc(eModel.get(i).getPolicy_required_doc());
			policyValue.setPolicy_approval_status(eModel.get(i).getPolicy_approval_status());
			policyValue.setPolicy_intialization_status(eModel.get(i).getPolicy_intialization_status());
			policyValue.setCr_date(eModel.get(i).getCr_date());
			
			lenderDetails= null;
			
			eBean.add(policyValue);
		}
		
		return eBean;
	}
	
	
	
	// CONVERT BORROWER MODEL TO BEAN 
	
	
	public List<BorrowerDetailsBean> convertBorrowerModelToBean(List<BorrowerRegistrationDetails> eModel)
	{		
        List<BorrowerDetailsBean> eBean = new ArrayList<BorrowerDetailsBean>();
		
        for(int i=0;i<eModel.size();i++)
		{			
        	BorrowerDetailsBean borrowerValue = new BorrowerDetailsBean();
        	
			borrowerValue.setBorrower_id(eModel.get(i).getBorrower_id());
			borrowerValue.setBorrower_first_name(eModel.get(i).getBorrower_first_name());
			borrowerValue.setBorrower_gender(eModel.get(i).getBorrower_gender());
			borrowerValue.setBorrower_last_name(eModel.get(i).getBorrower_last_name());
			borrowerValue.setBorrower_name(eModel.get(i).getBorrower_name());
			borrowerValue.setBorrower_emailid(eModel.get(i).getBorrower_emailid());
			borrowerValue.setBorrower_phno(eModel.get(i).getBorrower_phno());
			borrowerValue.setBorrower_reg_time(eModel.get(i).getBorrower_reg_time());
			borrowerValue.setBorrower_status(eModel.get(i).getBorrower_status());
			borrowerValue.setBorrower_pan_id(eModel.get(i).getBorrower_pan_id());
			
			if(eModel.get(i).getBorrowerIdentity()!=null)
			{
					borrowerValue.setBorrower_profile_photo(eModel.get(i).getBorrowerIdentity().getBorrower_profile_photo());
					
					borrowerValue.setBorrower_present_door(eModel.get(i).getBorrowerIdentity().getBorrower_present_door());
					borrowerValue.setBorrower_present_building(eModel.get(i).getBorrowerIdentity().getBorrower_present_building());
					borrowerValue.setBorrower_present_street(eModel.get(i).getBorrowerIdentity().getBorrower_present_street());
					borrowerValue.setBorrower_present_area(eModel.get(i).getBorrowerIdentity().getBorrower_present_area());
					borrowerValue.setBorrower_present_city(eModel.get(i).getBorrowerIdentity().getBorrower_present_city());
					
					borrowerValue.setBorrower_permanent_door(eModel.get(i).getBorrowerIdentity().getBorrower_permanent_door());
					borrowerValue.setBorrower_permanent_building(eModel.get(i).getBorrowerIdentity().getBorrower_permanent_building());
					borrowerValue.setBorrower_permanent_street(eModel.get(i).getBorrowerIdentity().getBorrower_permanent_street());
					borrowerValue.setBorrower_permanent_area(eModel.get(i).getBorrowerIdentity().getBorrower_permanent_area());
					borrowerValue.setBorrower_permanent_city(eModel.get(i).getBorrowerIdentity().getBorrower_permanent_city());
					
					borrowerValue.setBorrower_permanent_pincode(eModel.get(i).getBorrowerIdentity().getBorrower_permanent_pincode());
					borrowerValue.setBorrower_present_pincode(eModel.get(i).getBorrowerIdentity().getBorrower_present_pincode());
					
					borrowerValue.setBorrower_dob(eModel.get(i).getBorrowerIdentity().getBorrower_dob());
					borrowerValue.setBorrower_identity_id_type(eModel.get(i).getBorrowerIdentity().getBorrower_identity_id_type());
					borrowerValue.setBorrower_identity_card_id(eModel.get(i).getBorrowerIdentity().getBorrower_identity_card_id());
					borrowerValue.setCr_date(eModel.get(i).getBorrowerIdentity().getCr_date());
			}
			
			eBean.add(borrowerValue);
		}

		return eBean;
		
	}
}
