package com.finance.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finance.bean.BorrowerDetailsBean;
import com.finance.bean.InterPortalNotificationBean;
import com.finance.bean.LenderPolicyBean;
import com.finance.bean.LenderToBorrowerFinanceDealBean;
import com.finance.dao.AdminDao;
import com.finance.dao.BorrowerDao;
import com.finance.domain.AttachementDocumentModel;
import com.finance.domain.BorrowerDocumentModel;
import com.finance.domain.BorrowerIdentityModel;
import com.finance.domain.BorrowerRegistrationDetails;
import com.finance.domain.BorrowerSearchedDealDomain;
import com.finance.domain.EmailModel;
import com.finance.domain.InterPortalNotificationModel;
import com.finance.domain.LenderPolicyModel;
import com.finance.domain.LenderRegistrationDetails;
import com.finance.domain.LenderToBorrowerFinanceDealModel;
import com.finance.domain.LinkValidationModel;
import com.finance.domain.UserOTPModel;
import com.finance.util.EmailNotificationUtil;
import com.finance.util.EmailSentUtil;
import com.finance.util.OTPGenerator;
import com.finance.util.PasswordGenerator;
import com.finance.util.SendSms;
import com.finance.util.UniquePatternGeneration;

@Service("borrowerService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class BorrowerServiceImpl implements BorrowerService {

	
	@Autowired
	private BorrowerDao borrowerDao;
	
	@Autowired
	private AdminDao adminDao;
	
	
	PasswordGenerator pwGenerator = new PasswordGenerator();
	
	EmailNotificationUtil emailUtil = new EmailNotificationUtil();
	
	
    public String checkRegisterEmailId(String emailId,String userType)
    {
    	return borrowerDao.checkRegisterEmailId(emailId,userType);
    }
	
	public String checkBorrowerPhno(String phno,String userType)
	{
		return borrowerDao.checkBorrowerPhno(phno,userType);
	}
	
	public String checkBorrowerPanId(String panId,String userType)
	{
		return borrowerDao.checkBorrowerPanId(panId,userType);
		
	}
	
	@SuppressWarnings("static-access")
	public String saveBorrowerRegdetails(BorrowerRegistrationDetails borrowerRegDetails)
	{		
		borrowerRegDetails.setBorrower_name(borrowerRegDetails.getBorrower_first_name()+" "+borrowerRegDetails.getBorrower_last_name());
		borrowerRegDetails.setBorrower_status("Deactive");
		borrowerRegDetails.setBorrower_reg_time(new Date());
		
		
		BorrowerIdentityModel bowrrowerIdn = new BorrowerIdentityModel();
		
		bowrrowerIdn.setBorrower_id(borrowerRegDetails.getBorrower_id());
		bowrrowerIdn.setBorrower_dob("");		
		bowrrowerIdn.setBorrower_permanent_pincode("");		
		bowrrowerIdn.setBorrower_present_pincode("");
		bowrrowerIdn.setBorrower_profile_photo("No Photo");
		bowrrowerIdn.setBorrower_identity_id_type("");
		bowrrowerIdn.setBorrower_identity_card_id("");
		bowrrowerIdn.setCr_date(new Date());
		bowrrowerIdn.setBorrowerRegDetails(borrowerRegDetails);
		bowrrowerIdn.setBorrower_alt_phno("");
		// Address Details
		
		bowrrowerIdn.setBorrower_present_door("");
		bowrrowerIdn.setBorrower_present_building("");
		bowrrowerIdn.setBorrower_present_street("");
		bowrrowerIdn.setBorrower_present_area("");
		bowrrowerIdn.setBorrower_present_city("");
		
		bowrrowerIdn.setBorrower_permanent_door("");
		bowrrowerIdn.setBorrower_permanent_building("");
		bowrrowerIdn.setBorrower_permanent_street("");
		bowrrowerIdn.setBorrower_permanent_area("");
		bowrrowerIdn.setBorrower_permanent_city("");
		
		borrowerRegDetails.setBorrowerIdentity(bowrrowerIdn);
		
		
		try {
			
			borrowerRegDetails.setBorrower_password(pwGenerator.encrypt(borrowerRegDetails.getBorrower_password()));
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		String saveInDB = borrowerDao.saveBorrowerRegdetails(borrowerRegDetails);
		
		if(saveInDB.equals("success"))
		{           			
			String validateId = "";
			
			UniquePatternGeneration generatorObject = new UniquePatternGeneration();
			
			validateId += generatorObject.generateUniqueCode(borrowerRegDetails.getBorrower_emailid());
			
			/*String link = "http://192.168.1.6:8081/FP/activeAccount?account="+validateId;*/
			
			String link = "http://192.168.1.8:8081/FP";
			
			String reasonFor = "BorrowerReg";
			
			String extraInfo = "NotRequired";
						
			String emailBody = emailUtil.emailNotification(borrowerRegDetails.getBorrower_first_name(),borrowerRegDetails.getBorrower_emailid(),"resources/wellcomeRegistration",link,"Go to Login",reasonFor,extraInfo);
				
			
			String mailStatus = "";
			
			ResourceBundle resource = ResourceBundle.getBundle("resources/wellcomeRegistration");
		 	 
			String mailSubject = resource.getString("mailSubject");
			
			String smsMessage = resource.getString("smsMessage");
			String restMessage = resource.getString("restMessage");
			
			/* SENDING MAIL */
		    
			
				    EmailSentUtil emailStatus = new EmailSentUtil();
		 	        
					try {
						mailStatus = emailStatus.getEmailSent(borrowerRegDetails.getBorrower_emailid(),mailSubject,emailBody);
					} 
					catch (Exception e) {
						
						// TODO Auto-generated catch block
						mailStatus = "failed";
						e.printStackTrace();
					}
					
		  
		    /* END OF SENDING MAIL */
					
			
			if(!mailStatus.equals("success"))
			{
				EmailModel emailDetail = new EmailModel();
				
				emailDetail.setEmail_id(borrowerRegDetails.getBorrower_emailid());
				emailDetail.setEmail_subject(mailSubject);
				emailDetail.setEmail_content(emailBody);
				emailDetail.setEmail_status("failed");
				emailDetail.setCr_date(new Date());
				
				adminDao.getSaveEmailDetails(emailDetail);
			}
			
			// VALIDATE OTP WITH USER MOBILE TO ACTIVATE THE 
			
			UserOTPModel otpModel = new UserOTPModel();
			String otpNumber = "";
			OTPGenerator otp = new OTPGenerator();
			
			otpModel.setUser_mobile(borrowerRegDetails.getBorrower_phno());
			
			try {
				otpNumber = otp.generatePin()+"";
				otpModel.setOtp_number(otpNumber);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			otpModel.setOtp_status("Not Used");
			otpModel.setUser_type("Borrower");
			otpModel.setOtp_cr_date(new Date());
			
			String smsStatus = adminDao.getSaveOTPDetails(otpModel);
			
			if(smsStatus.equals("success"))
			{
				SendSms sms = new SendSms();
				sms.smsAPI(smsMessage+" "+otpNumber+" "+restMessage, borrowerRegDetails.getBorrower_phno());
			}
			
		    return saveInDB;
		}
		else
		{
			return saveInDB;
		}
	}	
	
	
	public String activateAccountViaOTP(String otpNumber)
	{
		return borrowerDao.activateAccountViaOTP(otpNumber);
	}
	
	@SuppressWarnings("static-access")
	public String generateOTPForuser(String userId)
	{
		String generateStatus = "";
		String mobileNo = "";
		String otpNumber = "";
		
		UserOTPModel otpModel = new UserOTPModel();
		OTPGenerator otp = new OTPGenerator();
		
		if(userId.contains("@"))
		{
			mobileNo = borrowerDao.getMobileNoViaEmailId(userId);		
		}
		else
		{
			mobileNo = userId;			
		}
		
		try {
			otpNumber = otp.generatePin()+"";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		otpModel.setUser_mobile(mobileNo);
		otpModel.setOtp_number(otpNumber);
		otpModel.setOtp_status("Not Used");
		otpModel.setUser_type("Borrower");
		otpModel.setOtp_cr_date(new Date());
		
		String smsStatus = adminDao.getSaveOTPDetails(otpModel);
		
		if(smsStatus.equals("success"))
		{
			ResourceBundle resource = ResourceBundle.getBundle("resources/wellcomeRegistration");
		 	 
			String smsMessage = resource.getString("smsMessage");
			String restMessage = resource.getString("restMessage");
			
			SendSms sms = new SendSms();
			sms.smsAPI(smsMessage+" "+otpNumber+" "+restMessage, mobileNo);
			
			return smsStatus;
		}
		else
		{
			return "failed";
		}		
	}
	
	public String getEmailIdViaUnqid(String activeAccountId)
	{
		return borrowerDao.getEmailIdViaUnqid(activeAccountId);
	}
	
	public String getActiveAcount(String userType,String emailId,String activeAccountId)
	{
		return borrowerDao.getActiveAcount(userType,emailId,activeAccountId);
	}
	
	public String getValidateBorrower(String userId,String password)
	{
		String status = "";
		
		try {
					
			String decryptPassword = pwGenerator.encrypt(password);
			status = borrowerDao.getValidateBorrower(userId,decryptPassword);			
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 			
		return status;
	}
	
	
	public int getBorrowerIdVieEmailId(String emailId)
	{
		return borrowerDao.getBorrowerIdVieEmailId(emailId);		
	}
		
	
	@SuppressWarnings("static-access")
	public String sendLinkForgetPassword(String emailId)
	{
		String validateId = "";
		LinkValidationModel linkValidate = new LinkValidationModel();
		
		linkValidate.setValidate_email_id(emailId);
		
		try {
			
			UniquePatternGeneration generatorObject = new UniquePatternGeneration();
			
			validateId += generatorObject.generateUniqueCode(emailId);
			
			linkValidate.setValidate_id(validateId);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		String link = "http://192.168.1.6:8081/FP/resetPassword?requnIkedij="+validateId+"";
		
		String reasonFor = "ResetPassword";
		
		String extraInfo = "NotRequired";
		
		String emailBody = emailUtil.emailNotification("User",emailId,"resources/forgetPassword",link,"Reset Passwod",reasonFor,extraInfo);
		
		String mailStatus = "";
		
		ResourceBundle resource = ResourceBundle.getBundle("resources/forgetPassword");
	 	 
		String mailSubject = resource.getString("mailSubject");
		
		/* SENDING MAIL */
	    
			    EmailSentUtil emailStatus = new EmailSentUtil();
	 	        
				try {
					mailStatus = emailStatus.getEmailSent(emailId,mailSubject,emailBody);
				} 
				catch (Exception e) {
					
					// TODO Auto-generated catch block
					mailStatus = "failed";
					e.printStackTrace();
				}
				
	  
	    /* END OF SENDING MAIL */
				
		
		if(!mailStatus.equals("success"))
		{
			EmailModel emailDetail = new EmailModel();
			
			emailDetail.setEmail_id(emailId);
			emailDetail.setEmail_subject(mailSubject);
			emailDetail.setEmail_content(emailBody);
			emailDetail.setEmail_status("failed");
			emailDetail.setCr_date(new Date());
			
			adminDao.getSaveEmailDetails(emailDetail);
		}
		
		
		
	    if(mailStatus.equals("success"))
	    {
	    	// Save email sent details
	    	
	    	
			linkValidate.setValidate_status("Pending");
			linkValidate.setValidate_init_date(new Date());
			linkValidate.setValidate_for("Forget Password");			
			
			borrowerDao.saveLinkValidatedetails(linkValidate);
	    }
	    else
	    {
	    	
	    }	    
	    	return mailStatus;
	}
	
	
	
	public String getEmailIdFromForgetPassword(String uniqueId)
	{
		return borrowerDao.getEmailIdFromForgetPassword(uniqueId);
	}
	
	
	public String getChangePassword(String emailId,String password)
	{
		String encPassword = "";
		try {
					
			System.out.println("password="+password);
			encPassword += pwGenerator.encrypt(password);
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return borrowerDao.getChangePassword(emailId,encPassword);
	}
	
	
	// Capture all borrower details 
	
	
	public BorrowerDetailsBean getBorrowerDetails(int borrowerId)
	{
		List<BorrowerRegistrationDetails> borrowerDetails = borrowerDao.getBorrowerDetails(borrowerId);
		
		BorrowerDetailsBean borrowerValue = new BorrowerDetailsBean();
		
		for(int i=0;i<borrowerDetails.size();i++)
		{
			
			borrowerValue.setBorrower_id(borrowerDetails.get(i).getBorrower_id());
			borrowerValue.setBorrower_first_name(borrowerDetails.get(i).getBorrower_first_name());
			borrowerValue.setBorrower_last_name(borrowerDetails.get(i).getBorrower_last_name());
			borrowerValue.setBorrower_gender(borrowerDetails.get(i).getBorrower_gender());
			borrowerValue.setBorrower_name(borrowerDetails.get(i).getBorrower_first_name()+" "+borrowerDetails.get(i).getBorrower_last_name());
			borrowerValue.setBorrower_emailid(borrowerDetails.get(i).getBorrower_emailid());
			borrowerValue.setBorrower_phno(borrowerDetails.get(i).getBorrower_phno());
			borrowerValue.setBorrower_reg_time(borrowerDetails.get(i).getBorrower_reg_time());
			borrowerValue.setBorrower_status(borrowerDetails.get(i).getBorrower_status());
			borrowerValue.setBorrower_pan_id(borrowerDetails.get(i).getBorrower_pan_id());
			
			if(borrowerDetails.get(i).getBorrowerIdentity()!=null)
			{
					borrowerValue.setBorrower_profile_photo(borrowerDetails.get(i).getBorrowerIdentity().getBorrower_profile_photo());
					//borrowerValue.setBorrower_permanent_address(borrowerDetails.get(i).getBorrowerIdentity().getBorrower_permanent_address());
					borrowerValue.setBorrower_permanent_pincode(borrowerDetails.get(i).getBorrowerIdentity().getBorrower_permanent_pincode());
					//borrowerValue.setBorrower_present_address(borrowerDetails.get(i).getBorrowerIdentity().getBorrower_present_address());
					borrowerValue.setBorrower_present_pincode(borrowerDetails.get(i).getBorrowerIdentity().getBorrower_present_pincode());
					borrowerValue.setBorrower_dob(borrowerDetails.get(i).getBorrowerIdentity().getBorrower_dob());
					borrowerValue.setBorrower_identity_id_type(borrowerDetails.get(i).getBorrowerIdentity().getBorrower_identity_id_type());
					borrowerValue.setBorrower_identity_card_id(borrowerDetails.get(i).getBorrowerIdentity().getBorrower_identity_card_id());
					borrowerValue.setCr_date(borrowerDetails.get(i).getBorrowerIdentity().getCr_date());
					borrowerValue.setBorrower_alt_phno(borrowerDetails.get(i).getBorrowerIdentity().getBorrower_alt_phno());
					
					borrowerValue.setBorrower_present_door(borrowerDetails.get(i).getBorrowerIdentity().getBorrower_present_door());
					borrowerValue.setBorrower_present_building(borrowerDetails.get(i).getBorrowerIdentity().getBorrower_present_building());			
					borrowerValue.setBorrower_present_street(borrowerDetails.get(i).getBorrowerIdentity().getBorrower_present_street());
					borrowerValue.setBorrower_present_area(borrowerDetails.get(i).getBorrowerIdentity().getBorrower_present_area());
					borrowerValue.setBorrower_present_city(borrowerDetails.get(i).getBorrowerIdentity().getBorrower_present_city());
					
					borrowerValue.setBorrower_permanent_door(borrowerDetails.get(i).getBorrowerIdentity().getBorrower_permanent_door());
					borrowerValue.setBorrower_permanent_building(borrowerDetails.get(i).getBorrowerIdentity().getBorrower_permanent_building());			
					borrowerValue.setBorrower_permanent_street(borrowerDetails.get(i).getBorrowerIdentity().getBorrower_permanent_street());
					borrowerValue.setBorrower_permanent_area(borrowerDetails.get(i).getBorrowerIdentity().getBorrower_permanent_area());
					borrowerValue.setBorrower_permanent_city(borrowerDetails.get(i).getBorrowerIdentity().getBorrower_permanent_city());
					
					
			}
			
		}
		
		return borrowerValue;
	}
	
	
	// EOF Capture all borrower details 
	
	
	public String getProfilePhoto(int borrowerId)
	{
		return borrowerDao.getProfilePhoto(borrowerId);
	}
	
	public String updateBorrowerIdentityDetails(BorrowerIdentityModel borrowerIdnDetails)
	{
		return borrowerDao.updateBorrowerIdentityDetails(borrowerIdnDetails);
	}
	
	
	public String getChangePassword(int borrowerId,String password)
	{
		String encPassword = "";
		try {
					
			encPassword += pwGenerator.encrypt(password);
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return borrowerDao.getChangePassword(borrowerId,encPassword);
	}
	
	
	public List<LenderPolicyBean> getSearchPolicyList(String type,String amount,String time,String location)
	{
		List<LenderPolicyBean> eBean = new ArrayList<LenderPolicyBean>();
		
		long givenAmount=0;
		
		int policy_min_duration = 0;
		
		givenAmount = Long.parseLong(amount);			
		
		if(!time.equals("24+"))
		{
			if(time.equals("All"))
			{
				policy_min_duration = 0;
			}
			else
			{
				if(time.equals("Select policy duration"))
				{
					policy_min_duration = 0;
				}
				else
				{
					policy_min_duration = Integer.parseInt(time);
				}				
			}			
		}
		else
		{
			policy_min_duration = 24;
		}
		
		List<LenderPolicyModel> eModel = borrowerDao.getSearchPolicyList(type,amount,givenAmount,time,policy_min_duration,location);
		
		eBean = convertLenderPolicyModelToBean(eModel);
		
		return eBean;
	}
	
	
	
	public List<LenderPolicyBean> getPolicyInformation(int policyId)
	{
		List<LenderPolicyBean> eBean = new ArrayList<LenderPolicyBean>();
		
		List<LenderPolicyModel> eModel = borrowerDao.getPolicyInformation(policyId);
		
		eBean = convertLenderPolicyModelToBean(eModel);
		
		return eBean;
	}
	
	
	
	
	
	public String savePolicyDealBetweenBorLen(LenderToBorrowerFinanceDealModel dealModel)
	{
		dealModel.setLender_status("Pending");
		dealModel.setDeal_initiated_date(new Date());
		dealModel.setDeal_status("Pending");
		dealModel.setPending_reject_reason("");
		dealModel.setProcessing_reject_reason("");
		dealModel.setAmount_dispouse("No");
		
		String saveStatus = borrowerDao.savePolicyDealBetweenBorLen(dealModel);
		
		int borrowerId = dealModel.getBorrower_id();
		
		if(saveStatus.equals("success"))
		{
			EmailNotificationUtil emailUtil = new EmailNotificationUtil();
			
			List<BorrowerRegistrationDetails> bowDetails = borrowerDao.getBorrowerDetails(borrowerId);
			
			String link = "http://192.168.1.6:8081/FP/borrowerLogin";
			
			String reasonFor = "NA";
			
			String extraInfo = "NA";
			
			String emailBody = emailUtil.emailNotification(bowDetails.get(0).getBorrower_name(),bowDetails.get(0).getBorrower_emailid(),"resources/intialDealNotify",link,"Go to Portal",reasonFor,extraInfo);
			
			String mailStatus = "";
			
			ResourceBundle resource = ResourceBundle.getBundle("resources/intialDealNotify");
		 	 
			String mailSubject = resource.getString("mailSubject");
			
			
			/* SENDING MAIL */
		    
			
				    EmailSentUtil emailStatus = new EmailSentUtil();
		 	        
					try {
						mailStatus = emailStatus.getEmailSent(bowDetails.get(0).getBorrower_emailid(),mailSubject,emailBody);
					} 
					catch (Exception e) {
						
						// TODO Auto-generated catch block
						mailStatus = "failed";
						e.printStackTrace();
					}
					
		  
		    /* END OF SENDING MAIL */
					
			
			if(!mailStatus.equals("success"))
			{
				EmailModel emailDetail = new EmailModel();
				
				emailDetail.setEmail_id(bowDetails.get(0).getBorrower_emailid());
				emailDetail.setEmail_subject(mailSubject);
				emailDetail.setEmail_content(emailBody);
				emailDetail.setEmail_status("failed");
				emailDetail.setCr_date(new Date());
				
				adminDao.getSaveEmailDetails(emailDetail);
			}
						
		}
		
		return saveStatus;
	}
	
	
	public List<LenderToBorrowerFinanceDealBean> getBorrowerPolicyDealDetails(int borrowerId)
	{
		List<LenderToBorrowerFinanceDealBean> dealBean = new ArrayList<LenderToBorrowerFinanceDealBean>();
		
		List<LenderToBorrowerFinanceDealModel> dealModel = borrowerDao.getBorrowerPolicyDealDetails(borrowerId);
		
		dealBean = convertPolicyDealModelToBean(dealModel);
		
		return dealBean;
	}
	
	
	public List<LenderToBorrowerFinanceDealBean> getBorrowerPolicyDealDetailsViaDealId(int dealId)
	{
		List<LenderToBorrowerFinanceDealBean> dealBean = new ArrayList<LenderToBorrowerFinanceDealBean>();
		
		List<LenderToBorrowerFinanceDealModel> dealModel = borrowerDao.getBorrowerPolicyDealDetailsViaDealId(dealId);
		
		dealBean = convertPolicyDealModelToBean(dealModel);
		
		return dealBean;	
	}
	
	public String getPolicyComponent(String policyType)
	{
		return borrowerDao.getPolicyComponent(policyType);
	}
	
	public String saveBorrowerSearchPolicy(BorrowerSearchedDealDomain searchModel)
	{
		String saveStatus = "";
		
		List<BorrowerSearchedDealDomain> existSearchData = borrowerDao.checkSearchData(searchModel);
		
		if(existSearchData.size()>0)
		{
			searchModel.setSearch_id(existSearchData.get(0).getSearch_id());
			
			borrowerDao.saveBoroowerSearchDetails(searchModel);
		}
		else
		{
			borrowerDao.saveBoroowerSearchDetails(searchModel);
		}
		
		return saveStatus;
	}
	
	
	public List<LenderToBorrowerFinanceDealBean> getLastDealDetails(int borrowerId)
	{
		List<LenderToBorrowerFinanceDealBean> dealBean = new ArrayList<LenderToBorrowerFinanceDealBean>();
		
		List<LenderToBorrowerFinanceDealModel> dealModel = borrowerDao.getLastDealDetails(borrowerId);
		
		dealBean = convertPolicyDealModelToBean(dealModel);
				
		return dealBean;
	}
	
	public List<LenderPolicyBean> getLastSearchDeals(int borrowerId)
	{
		List<BorrowerSearchedDealDomain> searchModel = borrowerDao.getLastSearchDeals(borrowerId);
		
		List<LenderPolicyBean> searchedPolicyBean = new ArrayList<LenderPolicyBean>();
		
		List<LenderPolicyModel> policyModel = borrowerDao.getPolicyDetails(searchModel);
		
		searchedPolicyBean = convertLenderPolicyModelToBean(policyModel);
		
		return searchedPolicyBean;
	}
	
	
	public List<InterPortalNotificationBean> getNotification(String userType,int userId)
	{
		List<InterPortalNotificationBean> notifyBean = new ArrayList<InterPortalNotificationBean>();
		
		List<InterPortalNotificationModel> notifyModel = borrowerDao.getNotification(userType,userId);
		
		notifyBean = convertNotificationModelToBean(notifyModel);
		
		return notifyBean;
	}
	
	
	public List<InterPortalNotificationBean> getNotifyDetails(String userType,int notifyId)
	{
		List<InterPortalNotificationBean> notifyBean = new ArrayList<InterPortalNotificationBean>();
		
		List<InterPortalNotificationModel> notifyModel = borrowerDao.getNotifyDetails(userType,notifyId);
		
		notifyBean = convertNotificationModelToBean(notifyModel);
		
		return notifyBean;
	}
	
	
	public List<BorrowerDocumentModel> getLastBorrowerDocRecord(int borrowerId)
	{
		return borrowerDao.getLastBorrowerDocRecord(borrowerId);
	}
	
	public String getBorrowerDocList()
	{
		String docList = "";
		
		List<AttachementDocumentModel> docNameList = borrowerDao.getBorrowerDocList();
		
		for(int i=0;i<docNameList.size();i++)
		{
			docList += docNameList.get(i).getDocument_name() + ",";
		}
		
		if(!docList.equals(""))
		{
			docList = docList.substring(0, docList.length()-1);
		}
		
		return docList;
	}
	
	public int getLastDocSequence(int borrowerId,String doc_type)
	{
		return borrowerDao.getLastDocSequence(borrowerId,doc_type);
	}
	
	public String saveDocumentDetails(BorrowerDocumentModel borrowerDocs)
	{
		return borrowerDao.saveDocumentDetails(borrowerDocs);
	}
	
	
	
	
	
	
	
	
	// CONVERT NOTIFICATION MODEL TO BEAN 
	
	
		public List<InterPortalNotificationBean> convertNotificationModelToBean(List<InterPortalNotificationModel> eModel)
		{
			List<InterPortalNotificationBean> eBean = new ArrayList<InterPortalNotificationBean>();
			
			for(int i=0;i<eModel.size();i++)
			{					
				InterPortalNotificationBean dealValue = new InterPortalNotificationBean();
				
				dealValue.setNotify_id(eModel.get(i).getNotify_id());
				dealValue.setNotify_extra_info_1(eModel.get(i).getNotify_extra_info_1());
				dealValue.setNotify_date(eModel.get(i).getNotify_date());
				dealValue.setNotify_extra_info_2(eModel.get(i).getNotify_extra_info_2());
				dealValue.setNotify_message(eModel.get(i).getNotify_message());
				dealValue.setNotify_reciver(eModel.get(i).getNotify_reciver());
				dealValue.setNotify_reciver_type(eModel.get(i).getNotify_reciver_type());
				dealValue.setNotify_regarding(eModel.get(i).getNotify_regarding());
				dealValue.setNotify_sender(eModel.get(i).getNotify_sender());
				dealValue.setNotify_sender_type(eModel.get(i).getNotify_sender_type());
				dealValue.setNotify_status(eModel.get(i).getNotify_status());
				dealValue.setNotify_subject(eModel.get(i).getNotify_subject());
				
				eBean.add(dealValue);
			}
			
			return eBean;
		}
		
		
		
		
	
	
	
	 
	// CONVERT POLICYDEAL MODEL TO BEAN 
	
	
	public List<LenderToBorrowerFinanceDealBean> convertPolicyDealModelToBean(List<LenderToBorrowerFinanceDealModel> eModel)
	{
		List<LenderToBorrowerFinanceDealBean> eBean = new ArrayList<LenderToBorrowerFinanceDealBean>();
		
		for(int i=0;i<eModel.size();i++)
		{					
			LenderToBorrowerFinanceDealBean dealValue = new LenderToBorrowerFinanceDealBean();
			
			
			dealValue.setDeal_id(eModel.get(i).getDeal_id());
			dealValue.setLender_id(eModel.get(i).getLender_id());
			dealValue.setBorrower_id(eModel.get(i).getBorrower_id());
			dealValue.setDeal_number(eModel.get(i).getDeal_number());
			dealValue.setPolicy_id(eModel.get(i).getPolicy_id());
			dealValue.setPolicy_name(eModel.get(i).getPolicy_name());
			dealValue.setPolicy_type(eModel.get(i).getPolicy_type());
			dealValue.setDeal_interest_rate(eModel.get(i).getDeal_interest_rate());
			dealValue.setDeal_amount(eModel.get(i).getDeal_amount());
			dealValue.setDeal_duration(eModel.get(i).getDeal_duration());
			dealValue.setIncome_type(eModel.get(i).getIncome_type());
			dealValue.setIncome_annualy(eModel.get(i).getIncome_annualy());			
			dealValue.setNote_to_lender(eModel.get(i).getNote_to_lender());
			dealValue.setScanned_file_name(eModel.get(i).getScanned_file_name());
			dealValue.setDeal_status(eModel.get(i).getDeal_status());
			
			dealValue.setDeal_initiated_date(eModel.get(i).getDeal_initiated_date());
			dealValue.setDeal_finalize_date(eModel.get(i).getDeal_finalize_date());
			dealValue.setLender_status(eModel.get(i).getLender_status());
			dealValue.setExtra_info(eModel.get(i).getExtra_info());
			
			
			eBean.add(dealValue);
		}
		
		return eBean;
	}
	
	
	
	
	
	public String checkDocumentExistence(int borrowerId,String requiredDoc)
	{
		String docList = "";
		
		List<BorrowerDocumentModel> attachedDoc =  borrowerDao.attachedDocuments(borrowerId);
		
		if(!requiredDoc.equals(""))
		{
			String reqdDocArray[] = requiredDoc.split(",");
			
			for(int d=0;d<reqdDocArray.length;d++)
			{
				String flag = "no";
				
				for(int a=0;a<attachedDoc.size();a++)
				{
					if(reqdDocArray[d].equals(attachedDoc.get(a).getDocument_type()))
					{
						flag = "yes";
					}
				}
				
				if(flag.equals("yes"))
				{
					docList += reqdDocArray[d]+"-Yes" + ",";
				}
				else
				{
					docList += reqdDocArray[d]+"-No" + ",";
				}
			}
			
			if(docList.length()>0)
			{
				docList = docList.substring(0, docList.length()-1);
			}
			
		}
		
		return docList;
	}
	
	
	public int getDocumentIdViaDocType(int borrowerId,String docType)
	{
		return borrowerDao.getDocumentIdViaDocType(borrowerId,docType);
	}
	
	public String getUpdateBorrowerDetails(BorrowerIdentityModel borrowerIdnDetails)
	{
		borrowerIdnDetails.setCr_date(new Date());
		return borrowerDao.getUpdateBorrowerDetails(borrowerIdnDetails);
	}
	
	
	// CONVERT LENDERPOLICY MODEL TO BEAN 
	
	public List<LenderRegistrationDetails> getLenderDetails(int lenderId)
	{
		return adminDao.getLenderDetails(lenderId);
	}
	
	
	
	public List<LenderPolicyBean> convertLenderPolicyModelToBean(List<LenderPolicyModel> eModel)
	{
		List<LenderPolicyBean> eBean = new ArrayList<LenderPolicyBean>();
		
		
		
		for(int i=0;i<eModel.size();i++)
		{
			List<LenderRegistrationDetails> lenderDetails = getLenderDetails(eModel.get(i).getLender_id());
					
			LenderPolicyBean policyValue = new LenderPolicyBean();
			
			policyValue.setLender_name(lenderDetails.get(0).getFull_name());
			policyValue.setLender_emailId(lenderDetails.get(0).getLender_emailid());
			
			policyValue.setPolicy_id(eModel.get(i).getPolicy_id());
			policyValue.setLender_id(eModel.get(i).getLender_id());
			policyValue.setPolicy_name(eModel.get(i).getPolicy_name());
			policyValue.setPolicy_category(eModel.get(i).getPolicy_category());
			policyValue.setPolicy_type(eModel.get(i).getPolicy_type());
			policyValue.setPolicy_min_amount(eModel.get(i).getPolicy_min_amount());
			policyValue.setPolicy_max_amount(eModel.get(i).getPolicy_max_amount());
			policyValue.setPolicy_interest_rate(eModel.get(i).getPolicy_interest_rate());
			policyValue.setPolicy_broucher(eModel.get(i).getPolicy_broucher());
			policyValue.setPolicy_min_duration(eModel.get(i).getPolicy_min_duration());
			policyValue.setPolicy_max_duration(eModel.get(i).getPolicy_max_duration());
			policyValue.setPolicy_required_doc(eModel.get(i).getPolicy_required_doc());
			policyValue.setPolicy_approval_status(eModel.get(i).getPolicy_approval_status());
			policyValue.setPolicy_intialization_status(eModel.get(i).getPolicy_intialization_status());
			policyValue.setCr_date(eModel.get(i).getCr_date());
			policyValue.setPolicy_applicable_location(eModel.get(i).getPolicy_applicable_location());
			
			lenderDetails= null;
			
			eBean.add(policyValue);
		}
		
		return eBean;
	}
	
	
	public BorrowerDocumentModel getDocumentDetail(int documentId)
	{
		BorrowerDocumentModel docObj = new BorrowerDocumentModel();
		
		List<BorrowerDocumentModel> list = borrowerDao.getDocumentDetail(documentId);
		
		for(int i=0;i<list.size();i++)
		{
			docObj.setBorrower_id(list.get(i).getBorrower_id());
			docObj.setCr_date(list.get(0).getCr_date());
			docObj.setDocument_type(list.get(0).getDocument_type());
			docObj.setFile_name(list.get(0).getFile_name());
		}
		
		return docObj;
	}
	
	
	public List<BorrowerDocumentModel> attachedDocuments(int borrowerId)
	{
		return borrowerDao.attachedDocuments(borrowerId);
	}
	
	


}
