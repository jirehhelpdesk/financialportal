package com.finance.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.finance.bean.BorrowerDetailsBean;
import com.finance.bean.ControllerDetailsBean;
import com.finance.bean.LenderDetailsBean;
import com.finance.bean.LenderToBorrowerFinanceDealBean;
import com.finance.dao.AdminDao;
import com.finance.dao.ControllerDao;
import com.finance.dao.LenderDao;
import com.finance.domain.BorrowerRegistrationDetails;
import com.finance.domain.ControllerDetails;
import com.finance.domain.EmailModel;
import com.finance.domain.InterPortalNotificationModel;
import com.finance.domain.LenderIdentityModel;
import com.finance.domain.LenderRegistrationDetails;
import com.finance.domain.LenderToBorrowerFinanceDealModel;
import com.finance.domain.LinkValidationModel;
import com.finance.util.EmailNotificationUtil;
import com.finance.util.EmailSentUtil;
import com.finance.util.PasswordGenerator;
import com.finance.util.UniquePatternGeneration;

@Service("controllerService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ControllerServiceImpl implements ControllerService {

	
	
	@Autowired
	private ControllerDao controllerDao;
	
	@Autowired
	private AdminDao adminDao;
	
	@Autowired
	private LenderDao lenderDao;
	
	PasswordGenerator pwGenerator = new PasswordGenerator();
	
	
	
	@Transactional(readOnly = false)
	public String getValidateController(String controller_user_id,String controller_password)
	{
		String encPassword = "";
		try {
			 encPassword = pwGenerator.encrypt(controller_password);
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return controllerDao.getValidateController(controller_user_id,encPassword);
	}
	
	
	
	public List<LenderToBorrowerFinanceDealBean> getDealDetails(String condititon,String Parameter)
	{
		List<LenderToBorrowerFinanceDealBean> dealBean = new ArrayList<LenderToBorrowerFinanceDealBean>();
		
		
		List<LenderToBorrowerFinanceDealModel> dealModel = controllerDao.getDealDetails(condititon,Parameter);
		
		dealBean = convertPolicyDealModelToBean(dealModel,"Borrower");
		
		return dealBean;
	}
	
	public List<LenderToBorrowerFinanceDealBean> getDealDetailToView(String condititon,String Parameter)
	{
		List<LenderToBorrowerFinanceDealBean> dealBean = new ArrayList<LenderToBorrowerFinanceDealBean>();
		
		List<LenderToBorrowerFinanceDealModel> dealModel = controllerDao.getDealDetails(condititon,Parameter);
		
		dealBean = convertPolicyDealModelToBean(dealModel,"Borrower/Lender");
		
		return dealBean;
	}
	
	
	public List<LenderToBorrowerFinanceDealBean> getSearchDealDetails(String searchType,String searchValue,String loanStatusDiv)
	{
		List<LenderToBorrowerFinanceDealBean> dealBean = new ArrayList<LenderToBorrowerFinanceDealBean>();
		
		List<LenderToBorrowerFinanceDealModel> dealModel = new ArrayList<LenderToBorrowerFinanceDealModel>();
		
		String condititon = "BorrowerId";
		String parameter = "NR";
		
		if(searchType.equals("Loan Status"))
		{
			List<LenderToBorrowerFinanceDealModel> eModel = controllerDao.getDealAsPerStatus(condititon,loanStatusDiv);	
			
			dealBean = convertPolicyDealModelToBean(eModel,"Borrower");
		}
		else
		{
			List<BorrowerRegistrationDetails> getBorrowerIdList = controllerDao.searchBorrowerId(searchType,searchValue,loanStatusDiv);
			
			if(getBorrowerIdList.size()>0)
			{
				for(int i=0;i<getBorrowerIdList.size();i++)
				{
					parameter = getBorrowerIdList.get(i).getBorrower_id()+"";
					List<LenderToBorrowerFinanceDealModel> eModel = controllerDao.getDealDetails(condititon,parameter);
					
					dealModel.addAll(eModel);
				}			
			}
			
			dealBean = convertPolicyDealModelToBean(dealModel,"Borrower");
			
		}
		
		return dealBean;
	}
	
	
	public String updateDealStatusByController(LenderToBorrowerFinanceDealBean dealBean)
	{
		return controllerDao.updateDealStatusByController(dealBean);
	}
	
	
	public List<BorrowerDetailsBean> getSearchBorrower(String searchType,String searchValue)
	{
		List<BorrowerDetailsBean> borrowerBean = new ArrayList<BorrowerDetailsBean>();
		
		List<BorrowerRegistrationDetails> borrowerModel = controllerDao.getSearchBorrower(searchType,searchValue);
		
		borrowerBean = convertBorrowerModelToBean(borrowerModel);
		
		return borrowerBean;
	}
	
	public List<BorrowerDetailsBean> getLastBorrowerDetails()
	{
		List<BorrowerDetailsBean> borrowerBean = new ArrayList<BorrowerDetailsBean>();
		
		List<BorrowerRegistrationDetails> borrowerModel = controllerDao.getLastBorrowerDetails();
		
		borrowerBean = convertBorrowerModelToBean(borrowerModel);
		
		return borrowerBean;
	}
	
	public List<LenderDetailsBean> getSearchLender(String searchType,String searchValue)
	{
		List<LenderDetailsBean> lenderBean = new ArrayList<LenderDetailsBean>();
		
		List<LenderRegistrationDetails> lenderModel = controllerDao.getSearchLender(searchType,searchValue);
		
		lenderBean = convertLenderModelToBean(lenderModel);
		
		return lenderBean;
	}
	
	public List<LenderDetailsBean> getLastLenderDetails()
	{
		List<LenderDetailsBean> lenderBean = new ArrayList<LenderDetailsBean>();
		
		List<LenderRegistrationDetails> lenderModel = controllerDao.getLastLenderDetails();
		
		lenderBean = convertLenderModelToBean(lenderModel);
		
		return lenderBean;
	}
	
	public String manageLenderStatus(int lenderId,String condition)
	{
		return controllerDao.manageLenderStatus(lenderId,condition);
	}
	
	public List<ControllerDetailsBean> getControllerDetails(String controllerEmailId)
	{
		List<ControllerDetailsBean> eBean = new ArrayList<ControllerDetailsBean>();
		
		List<ControllerDetails> eModel = controllerDao.getControllerDetails(controllerEmailId);
		
		for(int i=0;i<eModel.size();i++)
		{
			ControllerDetailsBean internalBean = new ControllerDetailsBean();
			
			internalBean.setController_id(eModel.get(i).getController_id());
			internalBean.setController_user_id(eModel.get(i).getController_user_id());
			internalBean.setController_name(eModel.get(i).getController_name());
			internalBean.setController_designation(eModel.get(i).getController_designation());
			internalBean.setController_Phno(eModel.get(i).getController_Phno());
			internalBean.setController_photo(eModel.get(i).getController_photo());
			
			eBean.add(internalBean);			
		}
		
		return eBean;
	}
	
	
	public String getSaveNotifyDetails(InterPortalNotificationModel notifyModel)
	{
		String saveStatus = controllerDao.getSaveNotifyDetails(notifyModel);
		
		if(saveStatus.equals("success"))
		{			
			EmailNotificationUtil emailUtil = new EmailNotificationUtil();
			
			String notifyType = notifyModel.getNotify_reciver_type();
			String name = "";
			String notifyEmailId = "";
			
			if(notifyType.equals("Borrower"))
			{
				List<BorrowerRegistrationDetails> borrowerModel = controllerDao.getSearchBorrower("BorrowerId",notifyModel.getNotify_reciver()+"");
				name = borrowerModel.get(0).getBorrower_name();
				notifyEmailId  =  borrowerModel.get(0).getBorrower_emailid();
			}
			else
			{
				List<LenderRegistrationDetails> LenderModel = controllerDao.getSearchLender("LenderId",notifyModel.getNotify_reciver()+"");
				name = LenderModel.get(0).getFull_name();
				notifyEmailId  =  LenderModel.get(0).getLender_emailid();
			}
			
			
			
			String link = "http://192.168.1.6:8081/FP";
			
			String reasonFor = "NotifyMessage";
			
			String extraInfo = notifyModel.getNotify_message();
			
			String emailBody = emailUtil.emailNotification(name,notifyEmailId,"resources/interNotifyMessage",link,"Go to Portal",reasonFor,extraInfo);
			
			String mailStatus = "";
			
			/* SENDING MAIL */
		    
			
			    EmailSentUtil emailStatus = new EmailSentUtil();
	 	        
				try {
					mailStatus = emailStatus.getEmailSent(notifyEmailId,notifyModel.getNotify_subject(),emailBody);
				} 
				catch (Exception e) {
					
					// TODO Auto-generated catch block
					mailStatus = "failed";
					e.printStackTrace();
				}
					
		  
		    /* END OF SENDING MAIL */
					
			
			if(!mailStatus.equals("success"))
			{
				EmailModel emailDetail = new EmailModel();
				
				emailDetail.setEmail_id(notifyEmailId);
				emailDetail.setEmail_subject(notifyModel.getNotify_subject());
				emailDetail.setEmail_content(emailBody);
				emailDetail.setEmail_status("failed");
				emailDetail.setCr_date(new Date());
				
				adminDao.getSaveEmailDetails(emailDetail);
			}
		}
		
		return saveStatus;
	}
	
	
	public String changeControllerPassword(String controllerEmailId,String curPassword,String newPassword)
	{
		String encCurPassword = "";
		String encNewPassword = "";
		
		String changeStatus = "";
		try {
			
			encCurPassword = pwGenerator.encrypt(curPassword);
			encNewPassword = pwGenerator.encrypt(newPassword);
			
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		changeStatus = controllerDao.checkControllerPassword(encCurPassword,controllerEmailId);
		
		if(changeStatus.equals("Matched"))
		{
			changeStatus = controllerDao.changeControllerPassword(encNewPassword,controllerEmailId);
		}
		
		
		return changeStatus;
	}
	
	
	public String getUpdateControllerDetails(String controllerEmailId,String name,String role,String fileName)
	{
		return controllerDao.getUpdateControllerDetails(controllerEmailId,name,role,fileName);
	}
	
	
	public String createLenderByCntrl(LenderRegistrationDetails lenderRegDetails)
	{
		
		String primeryPassword = "";
		String secondPassword = "";
		
		try {
			 
				primeryPassword  = lenderRegDetails.getLender_representative_name().substring(0, 2)+lenderRegDetails.getLender_emailid().substring(0, 3)+"4w6";				
				lenderRegDetails.setLender_password(pwGenerator.encrypt(primeryPassword));
				
				secondPassword  = lenderRegDetails.getLender_second_representative_name().substring(0, 2)+lenderRegDetails.getLender_second_representative_email_id().substring(0, 3)+"sRp";				
				lenderRegDetails.setLender_second_representative_password(pwGenerator.encrypt(secondPassword));
				
				
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
		
			e.printStackTrace();
		}
		
		
		lenderRegDetails.setLender_reg_time(new Date());
		lenderRegDetails.setLender_status("Active");
		
		
		// Lender Identity Details 
		
		LenderIdentityModel lenderIdentity = new LenderIdentityModel();

		lenderIdentity.setLender_present_pincode("");
		lenderIdentity.setCr_dare(new Date());
		lenderIdentity.setLender_photo("No Photo");
		lenderIdentity.setLenderRegDetails(lenderRegDetails);
		
		lenderRegDetails.setLenderIdentity(lenderIdentity);
		
		lenderIdentity.setLender_present_door("");
		lenderIdentity.setLender_present_building("");
		lenderIdentity.setLender_present_street("");
		lenderIdentity.setLender_present_area("");
		lenderIdentity.setLender_present_city("");
		
		lenderIdentity.setPan_number("");
		lenderIdentity.setService_tax_number("");
		lenderIdentity.setTan_number("");
		
		
		
		
		
		String saveInDB = lenderDao.saveLenderRegdetails(lenderRegDetails);
				
		if(saveInDB.equals("success"))
		{
			EmailNotificationUtil sendEmailUtil = new EmailNotificationUtil();
			
			String link = "http://192.168.1.6:8082/FP/lenderLogin";
			
			String reasonFor = "LenderRegByController";
			
			
			String mailArray[] = {lenderRegDetails.getLender_representative_name()+"/"+lenderRegDetails.getLender_emailid()+"/"+primeryPassword,lenderRegDetails.getLender_second_representative_name()+"/"+lenderRegDetails.getLender_second_representative_email_id()+"/"+secondPassword};
			
			for(int i=0;i<mailArray.length;i++)
			{
				String mailArrayDetails[] = mailArray[i].split("/");
				
				String extraInfo = mailArrayDetails[2];
				
				String emailBody = "";
				
				emailBody = sendEmailUtil.emailNotification(mailArrayDetails[0],mailArrayDetails[1], "resources/lenderRegistration", link , "Sign in",reasonFor,extraInfo);
				
				
				String mailStatus = "";
				
				ResourceBundle resource = ResourceBundle.getBundle("resources/lenderRegistration");
			 	 
				String mailSubject = resource.getString("mailSubject");
				
				
				/* SENDING MAIL */
			    
				
					    EmailSentUtil emailStatus = new EmailSentUtil();
			 	        
						try {
							mailStatus = emailStatus.getEmailSent(mailArrayDetails[1],mailSubject,emailBody);
						} 
						catch (Exception e) {
							
							// TODO Auto-generated catch block
							mailStatus = "failed";
							e.printStackTrace();
						}
						
			  
			    /* END OF SENDING MAIL */
						
				
				if(!mailStatus.equals("success"))
				{
					EmailModel emailDetail = new EmailModel();
					
					emailDetail.setEmail_id(mailArrayDetails[1]);
					emailDetail.setEmail_subject(mailSubject);
					emailDetail.setEmail_content(emailBody);
					emailDetail.setEmail_status("failed");
					emailDetail.setCr_date(new Date());
					
					adminDao.getSaveEmailDetails(emailDetail);
				}
				
			}
			
		    return saveInDB;
		}
		else
		{
			return saveInDB;
		}
				
		
		
		
	}
	
	public String manageBorrowerStatus(int borrowerId,String condition)
	{
		return controllerDao.manageBorrowerStatus(borrowerId,condition);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	// CONVERT LENDER MODEL TO BEAN 
	
	
	public List<LenderDetailsBean> convertLenderModelToBean(List<LenderRegistrationDetails> eModel)
	{
		List<LenderDetailsBean> eBean = new ArrayList<LenderDetailsBean>();
		
		for(int i=0;i<eModel.size();i++)
		{
			LenderDetailsBean lenderBean = new LenderDetailsBean();
			
			lenderBean.setLender_id(eModel.get(i).getLender_id());
			lenderBean.setFull_name(eModel.get(i).getFull_name());
			lenderBean.setLender_emailid(eModel.get(i).getLender_emailid());
			lenderBean.setLender_phno(eModel.get(i).getLender_phno());
			lenderBean.setLender_reg_time(eModel.get(i).getLender_reg_time());
			lenderBean.setLender_status(eModel.get(i).getLender_status());
			lenderBean.setLender_type(eModel.get(i).getLender_type());
			lenderBean.setLender_representative_name(eModel.get(i).getLender_representative_name());
			lenderBean.setLender_second_representative_name(eModel.get(i).getLender_second_representative_name());
			lenderBean.setLender_second_representative_email_id(eModel.get(i).getLender_second_representative_email_id());
			lenderBean.setLender_second_representative_mobile_no(eModel.get(i).getLender_second_representative_mobile_no());
			
			if(eModel.get(i).getLenderIdentity()!=null)
			{				

				lenderBean.setLender_present_pincode(eModel.get(i).getLenderIdentity().getLender_present_pincode());
				lenderBean.setLender_photo(eModel.get(i).getLenderIdentity().getLender_photo());
				lenderBean.setCr_dare(eModel.get(i).getLenderIdentity().getCr_dare());
				
				lenderBean.setLender_present_door(eModel.get(i).getLenderIdentity().getLender_present_door());
				lenderBean.setLender_present_building(eModel.get(i).getLenderIdentity().getLender_present_building());
				lenderBean.setLender_present_street(eModel.get(i).getLenderIdentity().getLender_present_street());
				lenderBean.setLender_present_area(eModel.get(i).getLenderIdentity().getLender_present_area());
				lenderBean.setLender_present_city(eModel.get(i).getLenderIdentity().getLender_present_city());
				
			}	
			
			eBean.add(lenderBean);
		}
		
		return eBean;
	} 
	
	
	
	
	// CONVERT BORROWER MODEL TO BEAN 
	
	
		public List<BorrowerDetailsBean> convertBorrowerModelToBean(List<BorrowerRegistrationDetails> eModel)
		{		
	        List<BorrowerDetailsBean> eBean = new ArrayList<BorrowerDetailsBean>();
			
	        for(int i=0;i<eModel.size();i++)
			{			
	        	BorrowerDetailsBean borrowerValue = new BorrowerDetailsBean();
	        	
				borrowerValue.setBorrower_id(eModel.get(i).getBorrower_id());
				borrowerValue.setBorrower_first_name(eModel.get(i).getBorrower_first_name());
				borrowerValue.setBorrower_last_name(eModel.get(i).getBorrower_last_name());
				borrowerValue.setBorrower_gender(eModel.get(i).getBorrower_gender());
				borrowerValue.setBorrower_name(eModel.get(i).getBorrower_name());
				borrowerValue.setBorrower_emailid(eModel.get(i).getBorrower_emailid());
				borrowerValue.setBorrower_phno(eModel.get(i).getBorrower_phno());
				borrowerValue.setBorrower_reg_time(eModel.get(i).getBorrower_reg_time());
				borrowerValue.setBorrower_status(eModel.get(i).getBorrower_status());
				
				if(eModel.get(i).getBorrowerIdentity()!=null)
				{
						borrowerValue.setBorrower_profile_photo(eModel.get(i).getBorrowerIdentity().getBorrower_profile_photo());
						//borrowerValue.setBorrower_permanent_address(eModel.get(i).getBorrowerIdentity().getBorrower_permanent_address());
						borrowerValue.setBorrower_permanent_pincode(eModel.get(i).getBorrowerIdentity().getBorrower_permanent_pincode());
						//borrowerValue.setBorrower_present_address(eModel.get(i).getBorrowerIdentity().getBorrower_present_address());
						borrowerValue.setBorrower_present_pincode(eModel.get(i).getBorrowerIdentity().getBorrower_present_pincode());
						borrowerValue.setBorrower_dob(eModel.get(i).getBorrowerIdentity().getBorrower_dob());
						borrowerValue.setBorrower_identity_id_type(eModel.get(i).getBorrowerIdentity().getBorrower_identity_id_type());
						borrowerValue.setBorrower_identity_card_id(eModel.get(i).getBorrowerIdentity().getBorrower_identity_card_id());
						borrowerValue.setCr_date(eModel.get(i).getBorrowerIdentity().getCr_date());
						
						borrowerValue.setBorrower_alt_emailid(eModel.get(i).getBorrowerIdentity().getBorrower_alt_emailid());
						borrowerValue.setBorrower_alt_phno(eModel.get(i).getBorrowerIdentity().getBorrower_alt_phno());
						
						borrowerValue.setBorrower_present_door(eModel.get(i).getBorrowerIdentity().getBorrower_present_door());
						borrowerValue.setBorrower_present_building(eModel.get(i).getBorrowerIdentity().getBorrower_present_building());
						borrowerValue.setBorrower_present_street(eModel.get(i).getBorrowerIdentity().getBorrower_present_street());
						borrowerValue.setBorrower_present_area(eModel.get(i).getBorrowerIdentity().getBorrower_present_area());
						borrowerValue.setBorrower_present_city(eModel.get(i).getBorrowerIdentity().getBorrower_present_city());
						
						borrowerValue.setBorrower_permanent_door(eModel.get(i).getBorrowerIdentity().getBorrower_permanent_door());
						borrowerValue.setBorrower_permanent_building(eModel.get(i).getBorrowerIdentity().getBorrower_permanent_building());
						borrowerValue.setBorrower_permanent_street(eModel.get(i).getBorrowerIdentity().getBorrower_permanent_street());
						borrowerValue.setBorrower_permanent_area(eModel.get(i).getBorrowerIdentity().getBorrower_permanent_area());
						borrowerValue.setBorrower_permanent_city(eModel.get(i).getBorrowerIdentity().getBorrower_permanent_city());
						
				}
				
				eBean.add(borrowerValue);
			}

			return eBean;
			
		}
		
		
		
		
		
	
	
	
	
	
	// CONVERT POLICYDEAL MODEL TO BEAN 
	
	
		public List<LenderToBorrowerFinanceDealBean> convertPolicyDealModelToBean(List<LenderToBorrowerFinanceDealModel> eModel,String user)
		{
			List<LenderToBorrowerFinanceDealBean> eBean = new ArrayList<LenderToBorrowerFinanceDealBean>();
			
			for(int i=0;i<eModel.size();i++)
			{					
				LenderToBorrowerFinanceDealBean dealValue = new LenderToBorrowerFinanceDealBean();
				
				
				dealValue.setDeal_id(eModel.get(i).getDeal_id());
				dealValue.setLender_id(eModel.get(i).getLender_id());
				dealValue.setBorrower_id(eModel.get(i).getBorrower_id());
				dealValue.setDeal_number(eModel.get(i).getDeal_number());
				dealValue.setPolicy_id(eModel.get(i).getPolicy_id());
				dealValue.setPolicy_name(eModel.get(i).getPolicy_name());
				dealValue.setPolicy_type(eModel.get(i).getPolicy_type());
				dealValue.setDeal_interest_rate(eModel.get(i).getDeal_interest_rate());
				dealValue.setDeal_amount(eModel.get(i).getDeal_amount());
				dealValue.setDeal_duration(eModel.get(i).getDeal_duration());
				dealValue.setIncome_type(eModel.get(i).getIncome_type());
				dealValue.setIncome_annualy(eModel.get(i).getIncome_annualy());				
				dealValue.setNote_to_lender(eModel.get(i).getNote_to_lender());
				dealValue.setScanned_file_name(eModel.get(i).getScanned_file_name());
				dealValue.setDeal_status(eModel.get(i).getDeal_status());
				
				dealValue.setDeal_initiated_date(eModel.get(i).getDeal_initiated_date());
				dealValue.setDeal_finalize_date(eModel.get(i).getDeal_finalize_date());
				dealValue.setLender_status(eModel.get(i).getLender_status());
				dealValue.setExtra_info(eModel.get(i).getExtra_info());
				
				if(user.equals("Borrower"))
				{
					List<BorrowerRegistrationDetails> bowDetails = controllerDao.getBorrowerDetails(eModel.get(i).getBorrower_id());
					
					dealValue.setBorrower_name(bowDetails.get(0).getBorrower_name());
					dealValue.setBorrower_emailid(bowDetails.get(0).getBorrower_emailid());
					dealValue.setBorrower_phno(bowDetails.get(0).getBorrower_phno());				
				}
				
				if(user.equals("Lender"))
				{
					List<LenderRegistrationDetails> lenDetails = controllerDao.getLenderDetails(eModel.get(i).getLender_id());
					
					dealValue.setLender_name(lenDetails.get(0).getFull_name());
					dealValue.setLender_emailid(lenDetails.get(0).getLender_emailid());
					dealValue.setLender_phno(lenDetails.get(0).getLender_phno());
				}
				
				if(user.equals("Borrower/Lender"))
				{
					List<BorrowerRegistrationDetails> bowDetails = controllerDao.getBorrowerDetails(eModel.get(i).getBorrower_id());
					
					dealValue.setBorrower_name(bowDetails.get(0).getBorrower_name());
					dealValue.setBorrower_emailid(bowDetails.get(0).getBorrower_emailid());
					dealValue.setBorrower_phno(bowDetails.get(0).getBorrower_phno());		
					dealValue.setBorrower_profile_photo(bowDetails.get(0).getBorrowerIdentity().getBorrower_profile_photo());
					
					List<LenderRegistrationDetails> lenDetails = controllerDao.getLenderDetails(eModel.get(i).getLender_id());
					
					dealValue.setLender_name(lenDetails.get(0).getFull_name());
					dealValue.setLender_emailid(lenDetails.get(0).getLender_emailid());
					dealValue.setLender_phno(lenDetails.get(0).getLender_phno());
					dealValue.setLender_profile_photo(lenDetails.get(0).getLenderIdentity().getLender_photo());
				}
				
				eBean.add(dealValue);
			}
			
			return eBean;
		}
		
		
		
		
}
