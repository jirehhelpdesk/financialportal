package com.finance.dao;

import java.util.List;

import com.finance.bean.LenderToBorrowerFinanceDealBean;
import com.finance.domain.BorrowerRegistrationDetails;
import com.finance.domain.ControllerDetails;
import com.finance.domain.InterPortalNotificationModel;
import com.finance.domain.LenderRegistrationDetails;
import com.finance.domain.LenderToBorrowerFinanceDealModel;

public interface ControllerDao {

	public String getValidateController(String controller_user_id,String controller_password);
	
	public List<LenderToBorrowerFinanceDealModel> getDealDetails(String condititon,String Parameter);
	
	public List<BorrowerRegistrationDetails> searchBorrowerId(String searchType,String searchValue,String loanStatusDiv);
	
	public List<LenderRegistrationDetails> getLenderDetails(int lender_id);
	
	public List<BorrowerRegistrationDetails> getBorrowerDetails(int borrower_id);
	
	public String updateDealStatusByController(LenderToBorrowerFinanceDealBean dealBean);
	
	public List<BorrowerRegistrationDetails> getSearchBorrower(String searchType,String searchValue);
	
	public List<BorrowerRegistrationDetails> getLastBorrowerDetails();
	
	public List<LenderRegistrationDetails> getSearchLender(String searchType,String searchValue);
	
	public List<LenderRegistrationDetails> getLastLenderDetails();
	
	public List<ControllerDetails> getControllerDetails(String controllerEmailId);
	
	public String getSaveNotifyDetails(InterPortalNotificationModel notifyModel);
	
	public String checkControllerPassword(String encCurPassword,String controllerEmailId);
	
	public String changeControllerPassword(String encNewPassword,String controllerEmailId);
	
	public String getUpdateControllerDetails(String controllerEmailId,String name,String role,String fileName);
	
	public String manageBorrowerStatus(int borrowerId,String condition);
	
	public String manageLenderStatus(int lenderId,String condition);
	
	public List<LenderToBorrowerFinanceDealModel> getDealAsPerStatus(String condititon,String loanStatusDiv);	
	
}
