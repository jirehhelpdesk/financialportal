package com.finance.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.ModelAndView;

import com.finance.bean.ControllerDetailsBean;
import com.finance.bean.LenderDetailsBean;
import com.finance.domain.AdminDetails;
import com.finance.domain.BorrowerRegistrationDetails;
import com.finance.domain.ContactUsModel;
import com.finance.domain.ControllerDetails;
import com.finance.domain.EmailModel;
import com.finance.domain.FinancePolicyDomain;
import com.finance.domain.LenderPolicyModel;
import com.finance.domain.LenderRegistrationDetails;
import com.finance.domain.SearchLoanModel;
import com.finance.domain.UserOTPModel;
import com.finance.service.AdminService;

@SuppressWarnings("unused")
@Repository("adminDao")
public class AdminDaoImpl implements AdminDao  {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	@SuppressWarnings("unchecked")
	public List<AdminDetails> getAdminDetails()
	{	
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "from AdminDetails where admin_id=1";
		List<AdminDetails> lsdata = session.createQuery(hql).list();	
		

		tx.commit();
		session.close();
		 
		return lsdata;
	}
	
	
	public long countNoOfBorrower()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		long count = 0;
		
		count = ((Long)session.createQuery("select count(*) from BorrowerRegistrationDetails").uniqueResult()).longValue();
		
		tx.commit();
		session.close();
		 
		return count;
	}
	
	public long countNoOfLender()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		long count = 0;
		
		count = ((Long)session.createQuery("select count(*) from LenderRegistrationDetails").uniqueResult()).longValue();
		
		tx.commit();
		session.close();
		 
		return count;
	}
	
	public long countNoOfController()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		long count = 0;
		
		count = ((Long)session.createQuery("select count(*) from ControllerDetails").uniqueResult()).longValue();
		
		tx.commit();
		session.close();
		 
		return count;
	}
	
	public long countNoOfPolicy()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		long count = 0;
		
		count = ((Long)session.createQuery("select count(*) from LenderPolicyModel").uniqueResult()).longValue();
		
		tx.commit();
		session.close();
		 
		return count;
	}

	public long countNoOfPendingPolicy()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		long count = 0;
		
		count = ((Long)session.createQuery("select count(*) from LenderPolicyModel where policy_approval_status='Pending'").uniqueResult()).longValue();
		
		tx.commit();
		session.close();
		 
		return count;
	}
	
	public long countNoOfApprovedPolicy()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		long count = 0;
		
		count = ((Long)session.createQuery("select count(*) from LenderPolicyModel where policy_approval_status='Approved'").uniqueResult()).longValue();
		
		tx.commit();
		session.close();
		 
		return count;
	}
	
	public long countNoOfClosedPolicy()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		long count = 0;
		
		count = ((Long)session.createQuery("select count(*) from LenderPolicyModel where policy_approval_status='Closed'").uniqueResult()).longValue();
		
		tx.commit();
		session.close();
		 
		return count;
	}
	
	public long countNoOfAppliedPolicy()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		long count = 0;		
		count = ((Long)session.createQuery("select count(*) from LenderToBorrowerFinanceDealModel where deal_status='Pending'").uniqueResult()).longValue();		
		
		tx.commit();
		session.close();
		 
		return count;
	}
	
	public long countNoOfFinalizedPolicy()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		long count = 0;		
		count = ((Long)session.createQuery("select count(*) from LenderToBorrowerFinanceDealModel where deal_status='Approved'").uniqueResult()).longValue();		
		
		tx.commit();
		session.close();
		 
		return count;
	}
	
	public long countNoOfProcessPolicy()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		long count = 0;		
		count = ((Long)session.createQuery("select count(*) from LenderToBorrowerFinanceDealModel where deal_status='Processing'").uniqueResult()).longValue();		
		
		tx.commit();
		session.close();
		 
		return count;
	}
	
	public long countNoOfOnholdPolicy()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		long count = 0;		
		count = ((Long)session.createQuery("select count(*) from LenderToBorrowerFinanceDealModel where deal_status='Onhold'").uniqueResult()).longValue();		
		
		tx.commit();
		session.close();
		 
		return count;
	}
	
	
	public long countNoOfSearchLoan()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		long count = 0;		
		count = ((Long)session.createQuery("select count(*) from SearchLoanModel").uniqueResult()).longValue();		
		
		tx.commit();
		session.close();
		 
		return count;
	}
	
	
	public String updateBorrowerAccess(int borrowerId,String currentStatus)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query hql = session.createQuery("update BorrowerRegistrationDetails set borrower_status = :status where borrower_id = :borrowerId");
		
		if(currentStatus.equals("Active"))
		{
			hql.setParameter("borrowerId", borrowerId);
			hql.setParameter("status", "Deactive");			
		}
		else
		{
			hql.setParameter("borrowerId", borrowerId);
			hql.setParameter("status", "Active");			
		}
		
	    int result  = hql.executeUpdate();
		
	    tx.commit();
		session.close();
		 
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		} 
	}
	
	
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<LenderRegistrationDetails> getLenderDetails(int lenderId)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "from LenderRegistrationDetails where lender_id="+lenderId+"";
		List<LenderRegistrationDetails> lsdata = session.createQuery(hql).list();		
		
		tx.commit();
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<LenderRegistrationDetails> searchLenderAsPerFilter(String type,String value)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		if(type.equals("Name"))
		{
			String hql = "from LenderRegistrationDetails where full_name LIKE '%"+value+"%'";
			List<LenderRegistrationDetails> lsdata = session.createQuery(hql).list();		
			
			tx.commit();
			session.close();
			
			return lsdata;
		}
		else
		{
			String hql = "from LenderRegistrationDetails where lender_emailid='"+value+"'";
			List<LenderRegistrationDetails> lsdata = session.createQuery(hql).list();	
			
			tx.commit();
			session.close();
			
			return lsdata;
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getValidateAdmin(String user_id,String password)
	{	
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "from AdminDetails where admin_id=1";
		List<AdminDetails> lsdata = session.createQuery(hql).list();
		 
		tx.commit();
		session.close();
		
		if(lsdata.get(0).getAdmin_user_id().equals(user_id) && lsdata.get(0).getAdmin_password().equals(password))
		{
			return "correct";
		}
		else
		{
			return "incorrect";
		}
			
	}
	
	public String getSaveOTPDetails(UserOTPModel otpModel)
	{
		String txStatus="";
    	
    	Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(otpModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		
		return txStatus;	
	}
	
	public String saveConatactFormDetails(ContactUsModel contactDetail)
	{
		String txStatus="";
    	
    	Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(contactDetail);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		
		return txStatus;	
	}
	
	public String saveControllerdetails(ControllerDetails controllerDetails)
	{
		controllerDetails.setController_cr_date(new Date());
		controllerDetails.setController_access_status("Active");
		
		String txStatus="";
    	
    	Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(controllerDetails);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		
		return txStatus;		
	}
	
	
	public String savePolicyTypeDetails(FinancePolicyDomain financialPolicy)
	{
		String txStatus="";
    	
    	Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(financialPolicy);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		
		return txStatus;
	}
	
	
	public String getSaveEmailDetails(EmailModel emailDetail)
	{
		String txStatus="";
    	
    	Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(emailDetail);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		
		return txStatus;	
	}
	
	
	@SuppressWarnings("unchecked")
	public List<FinancePolicyDomain> getLoanType(String type,String value)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		if(type.equals("All"))
		{
			String hql = "from FinancePolicyDomain";
			List<FinancePolicyDomain> lsdata = session.createQuery(hql).list();		
			
			tx.commit();
			session.close();
			
			return lsdata;
		}
		else
		{
			String hql = "from FinancePolicyDomain where policy_type_name = '"+type+"'";
			List<FinancePolicyDomain> lsdata = session.createQuery(hql).list();	
			
			tx.commit();
			session.close();
			
			return lsdata;
		}	
	}
	
	public String deletePolicyType(int typeId)
	{
		 Session session = sessionFactory.openSession();
		 Transaction tx = session.beginTransaction();

		  String hql = "DELETE FROM FinancePolicyDomain WHERE policy_type_id = :typeId";	
		  org.hibernate.Query hql1 = session.createQuery(hql);
		  hql1.setParameter("typeId", typeId);
		  int result  = hql1.executeUpdate();
		  
		  tx.commit();
		  session.close();
			
		  if(result==1)
		  {
			  return "success";
		  }
		  else
		  {
			  return "failed";
		  }
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ControllerDetails> searchController(String searchType,String searchValue)
	{
		 Session session = sessionFactory.openSession();
		 Transaction tx = session.beginTransaction();

		if(searchType.equals("Name"))
		{
			String hql = "from ControllerDetails where controller_name LIKE '%"+searchValue+"%'";
			List<ControllerDetails> lsdata = session.createQuery(hql).list();		
			  
			tx.commit();
			session.close();
				
			return lsdata;
		} 
		else if(searchType.equals("Email"))
		{
			String hql = "from ControllerDetails where controller_user_id = '"+searchValue+"'";
			List<ControllerDetails> lsdata = session.createQuery(hql).list();		
			
			tx.commit();
			session.close();
				
			return lsdata;
		}	
		else if(searchType.equals("MobileNo"))
		{
			String hql = "from ControllerDetails where controller_Phno = '"+searchValue+"'";
			List<ControllerDetails> lsdata = session.createQuery(hql).list();		
			
			tx.commit();
			session.close();
			
			return lsdata;
		}
		else
		{
			int controllerId = Integer.parseInt(searchValue);
			String hql = "from ControllerDetails where controller_id = "+controllerId+"";
			List<ControllerDetails> lsdata = session.createQuery(hql).list();		
			
			tx.commit();
			session.close();
				
			return lsdata;
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getFindUser(String userType,String searchType,String searchValue)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String resultValue = "";
		
		if(userType.equals("Borrower"))
		{
			int borrower_id = Integer.parseInt(searchValue);
			String hql = "from BorrowerRegistrationDetails where borrower_id="+borrower_id+"";
			List<BorrowerRegistrationDetails> lsdata = session.createQuery(hql).list();		
			
			if(lsdata.size()>0)
			{
				resultValue = lsdata.get(0).getBorrower_emailid();
			}
		}
		else if(userType.equals("Lender"))
		{
			int lender_id = Integer.parseInt(searchValue);
			
			String hql = "from LenderRegistrationDetails where lender_id= "+lender_id+"";
			List<LenderRegistrationDetails> lsdata = session.createQuery(hql).list();		
			if(lsdata.size()>0)
			{
				resultValue = lsdata.get(0).getLender_emailid();
			}
		}
		else
		{
			int controllerId = Integer.parseInt(searchValue);
			String hql = "from ControllerDetails where controller_id = "+controllerId+"";
			List<ControllerDetails> lsdata = session.createQuery(hql).list();		
			if(lsdata.size()>0)
			{
				resultValue = lsdata.get(0).getController_user_id();
			}
		}
		
		tx.commit();
		session.close();
		 
		return resultValue;
	}
	
	@SuppressWarnings("unchecked")
	public String updateControllerStatus(int controller_id,String requestedStatus)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query hql = session.createQuery("update ControllerDetails set controller_access_status = :requestedStatus where controller_id = :controller_id");
		
		hql.setParameter("controller_id", controller_id);
		hql.setParameter("requestedStatus", requestedStatus);
		
	    int result  = hql.executeUpdate();
		
	    tx.commit();
		session.close();
		 
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		} 
	}
	
	@SuppressWarnings("unchecked")
	public List<BorrowerRegistrationDetails> getBorrowerDetails(String type,String value)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		if(type.equals("Name"))
		{
			System.out.println("sdfsd");
			
			String hql = "from BorrowerRegistrationDetails where borrower_name LIKE '%"+value+"%' ORDER BY borrower_reg_time";
			List<BorrowerRegistrationDetails> lsdata = session.createQuery(hql).list();		
			
			tx.commit();
			session.close();
				 
			return lsdata;
		}
		else if(type.equals("Email"))
		{
			String hql = "from BorrowerRegistrationDetails where borrower_emailid = '"+value+"'";
			List<BorrowerRegistrationDetails> lsdata = session.createQuery(hql).list();
			
			tx.commit();
			session.close();
			
			return lsdata;
		}
		else
		{
			String hql = "from BorrowerRegistrationDetails where borrower_phno = '"+value+"'";
			List<BorrowerRegistrationDetails> lsdata = session.createQuery(hql).list();	

			tx.commit();
			session.close();
			
			return lsdata;
		}	
	}
	
	@SuppressWarnings("unchecked")
	public List<FinancePolicyDomain> getPolicyDetails()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "FROM FinancePolicyDomain";
		List<FinancePolicyDomain> lsdata = session.createQuery(hql).list();	
		
		tx.commit();
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getPolicyType()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "SELECT DISTINCT policy_type_name FROM FinancePolicyDomain";
		List<String> lsdata = session.createQuery(hql).list();		
		
		tx.commit();
		session.close();
		
		return lsdata;
	}
	
	
	
	///////////////////            ADMIN SETTING               ///////////////
	
	
	
	
	@SuppressWarnings("unchecked")
	public String checkAdminPassword(String encCurPassword)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "from AdminDetails where admin_id=1";
		List<AdminDetails> lsdata = session.createQuery(hql).list();
		 
		tx.commit();
		session.close();
		
		if(lsdata.get(0).getAdmin_password().equals(encCurPassword))
		{
			return "Matched";
		}
		else
		{
			return "NotMatched";
		}
	}
	
	@SuppressWarnings("unchecked")
	public String changeAdminPassword(String newPassword)
	{	
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query hql = session.createQuery("update AdminDetails set admin_password = :newPassword where admin_id = :adminId");
		
		hql.setParameter("newPassword", newPassword);
		hql.setParameter("adminId", 1);
		
	    int result  = hql.executeUpdate();
		
	    tx.commit();
		session.close();
		
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		} 
	}
	
	@SuppressWarnings("unchecked")
	public String getUpdateAdminDetails(String name,String role,String fileName)
	{		
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hqlQ = "from AdminDetails where admin_id=1";
		List<AdminDetails> lsdata = session.createQuery(hqlQ).list();
		 
		if(fileName.equals(""))
		{
			if(lsdata.get(0).getAdmin_photo()!=null)
			{
				fileName += lsdata.get(0).getAdmin_photo();
			}
		}
		
		org.hibernate.Query hql = session.createQuery("update AdminDetails set admin_name = :name , admin_designation = :designation , admin_photo = :photo where admin_id = :adminId");
		
		hql.setParameter("name", name);
		hql.setParameter("designation", role);
		hql.setParameter("photo", fileName);
		
		hql.setParameter("adminId", 1);
		
	    int result  = hql.executeUpdate();
		
	    tx.commit();
		session.close();
		
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		} 
	}	
	
	@SuppressWarnings("unchecked")
	public List<FinancePolicyDomain> getPolicyTypeName()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "from FinancePolicyDomain";
		List<FinancePolicyDomain> lsdata = session.createQuery(hql).list();		
		
		tx.commit();
		session.close();
			
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<LenderPolicyModel> getPolicyDetailsViaLenderId(int lenderId)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "from LenderPolicyModel where lender_id="+lenderId+"";
		List<LenderPolicyModel> lsdata = session.createQuery(hql).list();	

		tx.commit();
		session.close();
		
		return lsdata;
	}	
	
	@SuppressWarnings("unchecked")
	public List<LenderPolicyModel> getPolicyDetailsViaType(String subType)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "from LenderPolicyModel where policy_type='"+subType+"'";
		List<LenderPolicyModel> lsdata = session.createQuery(hql).list();		

		tx.commit();
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<LenderPolicyModel> getLenPolicyDetailsViaPolicyId(int policyId)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "from LenderPolicyModel where policy_id="+policyId+"";
		List<LenderPolicyModel> lsdata = session.createQuery(hql).list();	

		tx.commit();
		session.close();
		
		return lsdata;
	}
	
	public String updatePolicyByAdmin(int policyId,String policyStatus)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query hql = session.createQuery("update LenderPolicyModel set policy_approval_status = :policyStatus where policy_id = :policyId");
		
		hql.setParameter("policyId", policyId);
		hql.setParameter("policyStatus", policyStatus);
		
	    int result  = hql.executeUpdate();

		tx.commit();
		session.close();
		
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		} 
	}
	
	
	public String updateLenderAccess(int lenderId,String currentStatus)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query hql = session.createQuery("update LenderRegistrationDetails set lender_status = :status where lender_id = :lenderId");
		
		if(currentStatus.equals("Active"))
		{
			hql.setParameter("lenderId", lenderId);
			hql.setParameter("status", "Deactive");			
		} 
		else
		{
			hql.setParameter("lenderId", lenderId);
			hql.setParameter("status", "Active");			
		}
		
	    int result  = hql.executeUpdate();
		
		tx.commit();
		session.close();
		
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";               
		}  
	}
	
	
	@SuppressWarnings("unchecked")
	public List<BorrowerRegistrationDetails> getBorrowerDetails(String emailid)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "from BorrowerRegistrationDetails where borrower_emailid = '"+emailid+"'";
		List<BorrowerRegistrationDetails> lsdata = session.createQuery(hql).list();		
		
		tx.commit();
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<LenderRegistrationDetails> getLenderDetails(String emailid)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "from LenderRegistrationDetails where lender_emailid = '"+emailid+"'";
		List<LenderRegistrationDetails> lsdata = session.createQuery(hql).list();	
		
		tx.commit();
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<ControllerDetails> getControllerDetails(String emailid)
	{       
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "from ControllerDetails where controller_user_id = '"+emailid+"'";
		List<ControllerDetails> lsdata = session.createQuery(hql).list();		
		
		tx.commit();
		session.close();
		
		return lsdata;
	}
	
	 
	
	public String saveSearchLoanDetails(SearchLoanModel searchLoanModel)
	{
		String txStatus="";
    	
    	Session session=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=session.beginTransaction();
		session.saveOrUpdate(searchLoanModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			session.close();
		}
		
		return txStatus;
	}
	
	
	
}
