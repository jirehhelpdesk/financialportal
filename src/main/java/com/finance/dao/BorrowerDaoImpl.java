package com.finance.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.finance.domain.AttachementDocumentModel;
import com.finance.domain.BorrowerDocumentModel;
import com.finance.domain.BorrowerIdentityModel;
import com.finance.domain.BorrowerRegistrationDetails;
import com.finance.domain.BorrowerSearchedDealDomain;
import com.finance.domain.FinancePolicyDomain;
import com.finance.domain.InterPortalNotificationModel;
import com.finance.domain.LenderPolicyModel;
import com.finance.domain.LenderToBorrowerFinanceDealModel;
import com.finance.domain.LinkValidationModel;
import com.finance.domain.UserOTPModel;

@SuppressWarnings({ "unused", "unchecked" })
@Repository("borrowerDao")
public class BorrowerDaoImpl implements BorrowerDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	
	@SuppressWarnings("unchecked")
	public String checkRegisterEmailId(String emailId,String userType)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		 
		if(userType.equals("Borrower"))
		{
			String hql = "select borrower_emailid from BorrowerRegistrationDetails where borrower_emailid = '"+emailId+"'";
			List<String> lsdata = session.createQuery(hql).list();	
			
			tx.commit();
			session.close();

			if(lsdata.size()>0)
			{
				return "Exist";
			}
			else
			{
				return "Not Exist";
			}
		}
		else
		{
			String hql = "select lender_emailid from LenderRegistrationDetails where lender_emailid = '"+emailId+"'";
			List<String> lsdata = session.createQuery(hql).list();	
			
			tx.commit();
			session.close();

			if(lsdata.size()>0)
			{
				return "Exist";
			}
			else
			{
				return "Not Exist";
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public String checkBorrowerPhno(String phno,String userType)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		if(userType.equals("Borrower"))
		{
			String hql = "select borrower_phno from BorrowerRegistrationDetails where borrower_phno = '"+phno+"'";
			List<String> lsdata = session.createQuery(hql).list();	
			
			tx.commit();
			session.close();

			if(lsdata.size()>0)
			{
				return "Exist";
			}
			else
			{
				return "Not Exist";
			}
		}
		else
		{
			String hql = "select lender_phno from LenderRegistrationDetails where lender_phno = '"+phno+"'";
			List<String> lsdata = session.createQuery(hql).list();	
			
			tx.commit();
			session.close();

			if(lsdata.size()>0)
			{
				return "Exist";
			}
			else
			{
				return "Not Exist";
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public String checkBorrowerPanId(String panId,String userType)
	{
		Session session = sessionFactory.openSession();
		
		if(userType.equals("Borrower"))
		{
			String hql = "select borrower_pan_id from BorrowerRegistrationDetails where borrower_pan_id = '"+panId+"'";
			List<String> lsdata = session.createQuery(hql).list();	
			
			session.close();
			
			if(lsdata.size()>0)
			{
				return "Exist";
			}
			else
			{
				return "Not Exist";
			}
		}
		else
		{
			return "Exist";
		}
	}
	
	
	public String saveBorrowerRegdetails(BorrowerRegistrationDetails borrowerRegDetails)
	{
        String txStatus="";
    	
    	Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(borrowerRegDetails);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		
		return txStatus;	
	}
	
	
	@SuppressWarnings("unchecked")
	public String getProfilePhoto(int borrowerId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "select borrower_profile_photo from BorrowerIdentityModel where borrower_id = "+borrowerId+"";
		List<String> lsdata = session.createQuery(hql).list();	
		
		session.close();
		
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "No Photo";
		}
	}
	
	
	public String updateBorrowerIdentityDetails(BorrowerIdentityModel borrowerIdnDetails)
	{
		 String txStatus="";
	    	
	    	Session signUpSession=sessionFactory.openSession();
			Transaction tx=null;
			try {
			tx=signUpSession.beginTransaction();
			signUpSession.saveOrUpdate(borrowerIdnDetails);
			tx.commit();
			if(tx.wasCommitted()){
				txStatus="success";
			} else {
				txStatus="failure";
			}
			} catch (Exception he) {
				System.out.println(he.getMessage());
			} finally {
				signUpSession.close();
			}
			
			return txStatus;	
	}
	
	
	@SuppressWarnings("unchecked")
	public String getEmailIdViaUnqid(String activeAccountId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "select validate_email_id from LinkValidationModel where validate_id = '"+activeAccountId+"' and validate_status='Pending'";
		List<String> lsdata = session.createQuery(hql).list();	
		
		session.close();
		
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "Not Exist";
		}
	}
	
	
	public String getActiveAcount(String userType,String emailId,String activeAccountId)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		if(userType.equals("Borrower"))
		{
			org.hibernate.Query hql = session.createQuery("update BorrowerRegistrationDetails set borrower_status = :status where borrower_emailid = :emailId");
			hql.setParameter("emailId", emailId);
			
			hql.setParameter("status","Active");
			
			tx.commit();
			
		    int result  = hql.executeUpdate();
			
			if(result==1)
			{		
				org.hibernate.Query updateHql = session.createQuery("update LinkValidationModel set validate_status = :setStatus where validate_id = :uniqueId");
				updateHql.setParameter("uniqueId", activeAccountId);			
				updateHql.setParameter("setStatus", "Over");
				
			    int update  = updateHql.executeUpdate();
				
			    tx.commit();
				session.close();
				 
			    if(update==1)
				{
			    	return "success";
				}
			    else
			    {
			    	return "failed";
			    }	
			}
			else
			{
				return "failed";
			}
		}
		else
		{
			org.hibernate.Query hql = session.createQuery("update LenderRegistrationDetails set lender_status = :status where lender_emailid = :emailId");
			hql.setParameter("emailId", emailId);
			
			hql.setParameter("status","Active");
			
		    int result  = hql.executeUpdate();
			
		    tx.commit();
			
			if(result==1)
			{		
				org.hibernate.Query updateHql = session.createQuery("update LinkValidationModel set validate_status = :setStatus where validate_id = :uniqueId");
				updateHql.setParameter("uniqueId", activeAccountId);			
				updateHql.setParameter("setStatus", "Over");
				
			    int update  = updateHql.executeUpdate();
				
			    tx.commit();
				session.close();
				 
			    if(update==1)
				{
			    	return "success";
				}
			    else
			    {
			    	return "failed";
			    }	
			}
			else
			{
				return "failed";
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getValidateBorrower(String userId,String decryptPassword)	
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "";
		if(userId.contains("@"))
		{
			hql = "select borrower_status from BorrowerRegistrationDetails where borrower_emailid = '"+userId+"' and borrower_password='"+decryptPassword+"'";			
		}
		else
		{
			hql = "select borrower_status from BorrowerRegistrationDetails where borrower_phno = '"+userId+"' and borrower_password='"+decryptPassword+"'";			
		}
		 
		List<String> lsdata = session.createQuery(hql).list();	
		
		tx.commit();
		session.close();

		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "invalid";
		}	
	}
	
	public String getMobileNoViaEmailId(String userId)
	{
		Session session = sessionFactory.openSession();

		String hql ="select borrower_phno from BorrowerRegistrationDetails where borrower_emailid = '"+userId+"'";			
		
		List<String> lsdata = session.createQuery(hql).list();	

		session.close();

		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "Invalid";
		}
	}
	
	@SuppressWarnings("unchecked")
	public int getBorrowerIdVieEmailId(String emailId)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "";
		
		if(emailId.contains("@"))
		{
			hql = "select borrower_id from BorrowerRegistrationDetails where borrower_emailid = '"+emailId+"'";
		}
		else
		{
			hql = "select borrower_id from BorrowerRegistrationDetails where borrower_phno = '"+emailId+"'";
		}
		
		List<Integer> lsdata = session.createQuery(hql).list();	
		
		tx.commit();
		session.close();

		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return 0;
		}	
	}
	
	
	@SuppressWarnings("unchecked")
	public String saveLinkValidatedetails(LinkValidationModel linkValidate)
	{
		String txStatus="";
    	
    	Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(linkValidate);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		
		return txStatus;	
	}
	
	
	@SuppressWarnings("unchecked")
	public String getEmailIdFromForgetPassword(String uniqueId)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "select validate_email_id from LinkValidationModel where validate_id = '"+uniqueId+"' and validate_init_date=(select MAX(validate_init_date) from LinkValidationModel where validate_id = '"+uniqueId+"' and validate_status='Pending')";
		List<String> lsdata = session.createQuery(hql).list();	
		
		tx.commit();
		session.close();

		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "Not Exist";
		}	
	}
	
	
	@SuppressWarnings("unchecked")
	public String getChangePassword(String emailId,String encPassword)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query hql = session.createQuery("update BorrowerRegistrationDetails set borrower_password = :encPassword where borrower_emailid = :emailId");
		hql.setParameter("emailId", emailId);
		
		hql.setParameter("encPassword",encPassword);
		
	    int result  = hql.executeUpdate();
	    
	    tx.commit();
	    
		if(result==1)
		{			
			String uniqueId = "select validate_id from LinkValidationModel where validate_email_id = '"+emailId+"' and validate_init_date=(select MAX(validate_init_date) from LinkValidationModel where validate_email_id = '"+emailId+"' and validate_status='Pending')";
			List<String> lsdata = session.createQuery(uniqueId).list();	
			
			if(lsdata.size()>0)
			{
				org.hibernate.Query updateHql = session.createQuery("update LinkValidationModel set validate_status = :setStatus where validate_id = :uniqueId");
				updateHql.setParameter("uniqueId", lsdata.get(0));			
				updateHql.setParameter("setStatus", "Over");
				
			    int update  = updateHql.executeUpdate();
				
			    tx.commit();
			    session.close();

			    if(update==1)
				{
			    	return "success";
				}
			    else
			    {
			    	return "failed";
			    }	
			}
			else
			{
				return "failed";
			}			    
		}
		else
		{			
			return "failed";
		}
		
	}
	
	
	@SuppressWarnings("unchecked")
	public List<BorrowerRegistrationDetails> getBorrowerDetails(int borrowerId)
	{
		 Session session = sessionFactory.openSession();
		 
		 Query query = session.createQuery("from BorrowerRegistrationDetails where borrower_id="+borrowerId+"");
	     List<BorrowerRegistrationDetails> borrowerDetails = query.list();
	        
	     session.close();
	     
	     return borrowerDetails;
	}
	
	
	
	public String getChangePassword(int borrowerId,String password)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query hql = session.createQuery("update BorrowerRegistrationDetails set borrower_password = :encPassword where borrower_id = :borrowerId");
		
		hql.setParameter("borrowerId", borrowerId);
		hql.setParameter("encPassword",password);
		
	    int result  = hql.executeUpdate();
		
	    tx.commit();
	    session.close();
	    
		if(result==1)
		{	
			return "success";
		}
		else
		{
			return "failed";
		}
	}
	
	
	public String activateAccountViaOTP(String otpNumber)
	{
		String status="";
		
		Session session = sessionFactory.openSession();
		
		String mobileNo = "";
				
		String hql = "from UserOTPModel where otp_number = '"+otpNumber+"' and otp_cr_date=(select MAX(otp_cr_date) from UserOTPModel where otp_number = '"+otpNumber+"')";
		List<UserOTPModel> lsdata = session.createQuery(hql).list();	
		
		if(lsdata.size()>0)
		{
			mobileNo = lsdata.get(0).getUser_mobile();
			
			org.hibernate.Query updateHql = session.createQuery("update BorrowerRegistrationDetails set borrower_status = :status where borrower_phno = :mobileNo");
			
			updateHql.setParameter("mobileNo", mobileNo);
			
			updateHql.setParameter("status","Active");
			
			int result  = updateHql.executeUpdate();
			 
			if(result==1)
			{
				status = "success";
			}
			else
			{
				status = "failed";
			}		
		}
		else
		{
			status = "NotMached";
		}
		
		return status;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<LenderPolicyModel> getSearchPolicyList(String type,String amount,long givenAmount,String time,int policy_min_duration,String location)
	{	
		Session session = sessionFactory.openSession();
		
		Criteria hql = session.createCriteria(LenderPolicyModel.class);
		
		hql.add(Restrictions.eq("policy_type",type));
		hql.add(Restrictions.eq("policy_approval_status","Approved"));
		
		hql.add(Restrictions.eq("policy_applicable_location",location));
		
		hql.add(Restrictions.le("policy_min_amount",givenAmount));	
		
		hql.add(Restrictions.ge("policy_max_amount",givenAmount));	
		
		if(time.equals("24+"))
		{
			hql.add(Restrictions.ge("policy_min_duration",policy_min_duration));	
		}
		
		if(!time.equals("24+"))
		{
			if(time.equals("All"))
			{
				hql.add(Restrictions.ge("policy_min_duration",policy_min_duration));	
			}
			else
			{
				hql.add(Restrictions.ge("policy_max_duration",policy_min_duration));	
			}			
		}
		
		List<LenderPolicyModel> lsdata = hql.list();
		
		session.close();
		
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LenderPolicyModel> getPolicyInformation(int policyId)
	{
		Session session = sessionFactory.openSession();
		
		Criteria hql = session.createCriteria(LenderPolicyModel.class);
		
		hql.add(Restrictions.eq("policy_id",policyId));
		
		List<LenderPolicyModel> lsdata = hql.list();
		
		session.close();
		
		return lsdata;
	}
	
	
	public String getUpdateBorrowerDetails(BorrowerIdentityModel borrowerIdnDetails)
	{
		Session session = sessionFactory.openSession();
		
		String profilePhoto = "";
		String hql = "select borrower_profile_photo from BorrowerIdentityModel where borrower_id = "+borrowerIdnDetails.getBorrower_id()+"";
		List<String> lsdata = session.createQuery(hql).list();	
		
		if(lsdata.size()>0)
		{
			profilePhoto = lsdata.get(0);
		}
		else
		{
			profilePhoto = "No Photo";
		}
		
		borrowerIdnDetails.setBorrower_profile_photo(profilePhoto);
		
		String txStatus="";
    	
    	Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(borrowerIdnDetails);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failed";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		
		session.close();
		
		return txStatus;
		
	}
	
	public int getDocumentIdViaDocType(int borrowerId,String docType)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "select document_id from BorrowerDocumentModel where borrower_id="+borrowerId+" and document_type='"+docType+"' and cr_date=(select MAX(cr_date) from BorrowerDocumentModel where borrower_id="+borrowerId+" and document_type='"+docType+"')";
		List<Integer> lsdata = session.createQuery(hql).list();	
		
		session.close();
		
		return lsdata.get(0);
	}
	
	public String savePolicyDealBetweenBorLen(LenderToBorrowerFinanceDealModel dealModel)
	{
		String txStatus="";
    	
    	Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(dealModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		
		return txStatus;	
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LenderToBorrowerFinanceDealModel> getBorrowerPolicyDealDetails(int borrowerId)
	{
		 Session session = sessionFactory.openSession();
		
	     Query query = session.createQuery("from LenderToBorrowerFinanceDealModel where borrower_id="+borrowerId+"");
	     List<LenderToBorrowerFinanceDealModel> borrowerDealDetails = query.list();
	        
	     session.close();
	     
	     return borrowerDealDetails;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LenderToBorrowerFinanceDealModel> getBorrowerPolicyDealDetailsViaDealId(int dealId)
	{
		Session session = sessionFactory.openSession();
		
	     Query query = session.createQuery("from LenderToBorrowerFinanceDealModel where deal_id="+dealId+"");
	     List<LenderToBorrowerFinanceDealModel> dealDetails = query.list();
	        
	     session.close();
	     
	     return dealDetails;
	}
	
	@SuppressWarnings("unchecked")
	public String getPolicyComponent(String policyType)
	{
		Session session = sessionFactory.openSession();
		
	    Query query = session.createQuery("from FinancePolicyDomain where sub_policy_type='"+policyType+"'");
	    List<FinancePolicyDomain> policyComponent = query.list();
	       
	    session.close();
	    
	    if(policyComponent.size()>0)
	    {
	    	if(policyComponent.get(0).getPolicy_component().length()>0)
	    	{
	    		String component = policyComponent.get(0).getPolicy_component();
		    	return component;
	    	}
	    	else
	    	{
	    		return "No Component";
	    	}	    	
	    }
	    else
	    {
	    	return "No Component";
	    }
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LenderToBorrowerFinanceDealModel> getLastDealDetails(int borrowerId)
	{
		Session session = sessionFactory.openSession();
		
		Criteria hql = session.createCriteria(LenderToBorrowerFinanceDealModel.class);
		
		hql.add(Restrictions.eq("borrower_id",borrowerId));		
	    hql.addOrder(Order.desc("deal_initiated_date"));
	    hql.setMaxResults(4);
	    
	    List<LenderToBorrowerFinanceDealModel> lsdata =  hql.list();
		    
	    session.close();
	    
		return lsdata;	
	}
	
	public String saveBoroowerSearchDetails(BorrowerSearchedDealDomain searchModel)
	{
		String txStatus="";
    	
    	Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(searchModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		
		return txStatus;	
	}
	
	@SuppressWarnings("unchecked")
	public List<BorrowerSearchedDealDomain> checkSearchData(BorrowerSearchedDealDomain searchModel)
	{
		Session session = sessionFactory.openSession();
		
		Criteria hql = session.createCriteria(BorrowerSearchedDealDomain.class);
		
		hql.add(Restrictions.eq("borrower_id",searchModel.getBorrower_id()));
		hql.add(Restrictions.eq("policy_id",searchModel.getPolicy_id()));
	    
	    List<BorrowerSearchedDealDomain> lsdata =  hql.list();
		    
	    session.close();
	    
		return lsdata;	
	}
	
	
	@SuppressWarnings("unchecked")
	public List<BorrowerSearchedDealDomain> getLastSearchDeals(int borrowerId)
	{
		Session session = sessionFactory.openSession();
		
		Criteria hql = session.createCriteria(BorrowerSearchedDealDomain.class);
		
		hql.add(Restrictions.eq("borrower_id",borrowerId));			
	    hql.addOrder(Order.desc("searched_date"));
	    hql.setMaxResults(4);
	    
	    List<BorrowerSearchedDealDomain> lsdata =  hql.list();
		   
	    session.close();
	    
		return lsdata;					
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LenderPolicyModel> getPolicyDetails(List<BorrowerSearchedDealDomain> searchModel)
	{
		Session session = sessionFactory.openSession();
		
		List<LenderPolicyModel> policyModel = new ArrayList<LenderPolicyModel>();
		
		Criteria hql = session.createCriteria(LenderPolicyModel.class);
		
		for(int i=0;i<searchModel.size();i++)
		{
			int p = searchModel.get(i).getPolicy_id();
			
			hql.add(Restrictions.eq("policy_id",p));		
		    hql.addOrder(Order.desc("cr_date"));
		    hql.setMaxResults(4);
		    
		    policyModel.addAll(hql.list());
		}	
		
		session.close();
		
		return policyModel;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<InterPortalNotificationModel> getNotification(String userType,int userId)
	{
		Session session = sessionFactory.openSession();
		
		Criteria hql = session.createCriteria(InterPortalNotificationModel.class);
		
		hql.add(Restrictions.eq("notify_reciver",userId));		
		hql.add(Restrictions.eq("notify_reciver_type",userType));
		hql.add(Restrictions.ne("notify_status","Removed"));
	    hql.addOrder(Order.desc("notify_date"));
	    
	    List<InterPortalNotificationModel> lsdata =  hql.list();
		    
	    session.close();
	    
		return lsdata;	
	}
	
	@SuppressWarnings("unchecked")
	public List<InterPortalNotificationModel> getNotifyDetails(String userType,int notifyId)
	{
		Session session = sessionFactory.openSession();
		
		Criteria hql = session.createCriteria(InterPortalNotificationModel.class);
		
		hql.add(Restrictions.eq("notify_id",notifyId));		
		hql.add(Restrictions.eq("notify_reciver_type",userType));		
	    
	    List<InterPortalNotificationModel> lsdata =  hql.list();
		    
	    session.close();
	    
		return lsdata;	
	}
	
	@SuppressWarnings("unchecked")
	public List<AttachementDocumentModel> getBorrowerDocList()
	{
		Session session = sessionFactory.openSession();
		
		Criteria hql = session.createCriteria(AttachementDocumentModel.class);
		
		hql.addOrder(Order.asc("document_name"));						
	    
	    List<AttachementDocumentModel> lsdata =  hql.list();
		    
	    session.close();
	    
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public int getLastDocSequence(int borrowerId,String doc_type)
	{
		int squence = 0;
		
		Session session = sessionFactory.openSession();
		
	    Query query = session.createQuery("from BorrowerDocumentModel where borrower_id="+borrowerId+" and document_type='"+doc_type+"' and cr_date=(select MAX(cr_date) from BorrowerDocumentModel where borrower_id="+borrowerId+" and document_type='"+doc_type+"')");
	    List<BorrowerDocumentModel> detail = query.list();
	        
		if(detail.size()>0)
		{
			squence = detail.get(0).getDocument_count();
		}
		
		session.close();
		
		return squence;
	}
	
	@SuppressWarnings("unchecked")
	public List<BorrowerDocumentModel> getLastBorrowerDocRecord(int borrowerId)
	{
		Session session = sessionFactory.openSession();
		
		Criteria hql = session.createCriteria(BorrowerDocumentModel.class);
		
		hql.add(Restrictions.eq("borrower_id",borrowerId));						
	    hql.addOrder(Order.desc("cr_date"));
	    hql.setMaxResults(1);
	    
	    List<BorrowerDocumentModel> lsdata =  hql.list();
		    
	    session.close();
	    
		return lsdata;	
	}
	
	public String saveDocumentDetails(BorrowerDocumentModel borrowerDocs)
	{
		String txStatus="";
    	
    	Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.save(borrowerDocs);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		
		return txStatus;	
	}
	
	
	@SuppressWarnings("unchecked")
	public List<BorrowerDocumentModel> attachedDocuments(int borrowerId)
	{
		Session session = sessionFactory.openSession();
		
		List<BorrowerDocumentModel> docList = new ArrayList<BorrowerDocumentModel>();
		
		String hql = "SELECT DISTINCT document_type FROM BorrowerDocumentModel where borrower_id="+borrowerId+"";		
		List<String> lsdata = session.createQuery(hql).list();	
		
		// Need an Proper query to get the details		
		for(int i=0;i<lsdata.size();i++)			
		{			
			String subHql = "FROM BorrowerDocumentModel where borrower_id="+borrowerId+" and document_type='"+lsdata.get(i)+"' and cr_date=(select MAX(cr_date) from BorrowerDocumentModel where borrower_id="+borrowerId+" and document_type='"+lsdata.get(i)+"')";
			List<BorrowerDocumentModel> subData = session.createQuery(subHql).list();	
			
			docList.addAll(subData);
		}
	    
		session.close();
		
		return docList;
	}
	
	@SuppressWarnings("unchecked")
	public List<BorrowerDocumentModel> getDocumentDetail(int documentId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from BorrowerDocumentModel where document_id="+documentId+"";
		List<BorrowerDocumentModel> lsdata = session.createQuery(hql).list();	
		
		session.close();
		
		return lsdata;		
	}
	
}
