package com.finance.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.finance.bean.LenderToBorrowerFinanceDealBean;
import com.finance.domain.AdminDetails;
import com.finance.domain.BorrowerRegistrationDetails;
import com.finance.domain.ControllerDetails;
import com.finance.domain.InterPortalNotificationModel;
import com.finance.domain.LenderRegistrationDetails;
import com.finance.domain.LenderToBorrowerFinanceDealModel;

@SuppressWarnings("unused")
@Repository("controllerDao")
public class ControllerDaoImpl implements ControllerDao {


	@Autowired
	private SessionFactory sessionFactory;
		
	@SuppressWarnings("unchecked")
	public String getValidateController(String controller_user_id,String controller_password)
	{
		Session session = sessionFactory.openSession();
		
		String status = "";
		
		String hql = "select controller_name from ControllerDetails where controller_user_id='"+controller_user_id+"' and controller_password='"+controller_password+"'";
		List<String> lsdata = session.createQuery(hql).list();	
		
		if(lsdata.size()>0)
		{
			status = "correct";
		}
		else
		{
			status = "incorrect";
		}
		
		session.close();
		
		return status;
	}
	
	@SuppressWarnings("unchecked")
	public List<LenderRegistrationDetails> getLenderDetails(int lender_id)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from LenderRegistrationDetails where lender_id ="+lender_id+"";						
	
		List<LenderRegistrationDetails> lsdata = session.createQuery(hql).list();	
		
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<BorrowerRegistrationDetails> getBorrowerDetails(int borrower_id)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from BorrowerRegistrationDetails where borrower_id ="+borrower_id+"";						
		
		List<BorrowerRegistrationDetails> lsdata = session.createQuery(hql).list();	
		
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<LenderToBorrowerFinanceDealModel> getDealAsPerStatus(String condititon,String loanStatusDiv)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from LenderToBorrowerFinanceDealModel where deal_status ='"+loanStatusDiv+"'";	
	
		List<LenderToBorrowerFinanceDealModel> lsdata = session.createQuery(hql).list();	
		
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<LenderToBorrowerFinanceDealModel> getDealDetails(String condititon,String Parameter)
	{
		Session session = sessionFactory.openSession();
		
		List<LenderToBorrowerFinanceDealModel> dealDetails = new ArrayList<LenderToBorrowerFinanceDealModel>();
		
		Criteria hql = session.createCriteria(LenderToBorrowerFinanceDealModel.class);
		
		if(condititon.equals("Today"))
		{			
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		    
		    String dateHql = "from LenderToBorrowerFinanceDealModel where deal_status='Pending' ORDER BY deal_initiated_date DESC ";		
		    dealDetails = session.createQuery(dateHql).list();				
		}
		else if(condititon.equals("BorrowerId"))
		{
			int borrowerId = Integer.parseInt(Parameter);
			
			hql.add(Restrictions.eq("borrower_id",borrowerId));		
		    hql.addOrder(Order.desc("deal_initiated_date"));
		    
		    dealDetails =  hql.list();
		}
		else if(condititon.equals("LenderId"))
		{
			int lenderId = Integer.parseInt(Parameter);
			
			hql.add(Restrictions.eq("lender_id",lenderId));		
		    hql.addOrder(Order.desc("deal_initiated_date"));
		   
		    dealDetails =  hql.list();
		}
		else
		{
			int deal_id = Integer.parseInt(Parameter);

			hql.add(Restrictions.eq("deal_id",deal_id));		
		    hql.addOrder(Order.desc("deal_initiated_date"));
		    
		    dealDetails =  hql.list();
		}
			
		session.close();
		
	    return dealDetails;	
	}
	
	
	@SuppressWarnings("unchecked")
	public List<BorrowerRegistrationDetails> searchBorrowerId(String searchType,String searchValue,String loanStatusDiv)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "";
		
		if(searchType.equals("Name"))
		{
			hql = "from BorrowerRegistrationDetails where borrower_name LIKE '%"+searchValue+"%'";			
		}
		else if(searchType.equals("Email"))
		{
			hql = "from BorrowerRegistrationDetails where borrower_emailid ='"+searchValue+"'";			
		}
		else if(searchType.equals("MobileNo"))
		{
			hql = "from BorrowerRegistrationDetails where borrower_phno ='"+searchValue+"'";						
		}
		else if(searchType.equals("Loan Status"))
		{
							
		}
		else
		{
			int borrowerId = Integer.parseInt(searchValue);
			
			hql = "from BorrowerRegistrationDetails where borrower_id ="+borrowerId+"";	
		}
		
		List<BorrowerRegistrationDetails> lsdata = session.createQuery(hql).list();	
		
		session.close();
		
		return lsdata;
	}
	
	public String updateDealStatusByController(LenderToBorrowerFinanceDealBean dealBean)
	{
		Session session = sessionFactory.openSession();
		
		org.hibernate.Query hql = session.createQuery("update LenderToBorrowerFinanceDealModel set controller_status = :cntrlStatus, deal_status = :dealStatus, extra_info=:visStatus where deal_id = :dealId");
		
		hql.setParameter("dealId", dealBean.getDeal_id());
		hql.setParameter("cntrlStatus", dealBean.getLender_status());
		hql.setParameter("dealStatus", dealBean.getDeal_status());
		hql.setParameter("visStatus", dealBean.getExtra_info());
		
	    int result  = hql.executeUpdate();
		
	    session.close();
	    
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		} 
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<BorrowerRegistrationDetails> getSearchBorrower(String searchType,String searchValue)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "";
		
		if(searchType.equals("Name"))
		{
			hql = "from BorrowerRegistrationDetails where borrower_name LIKE '%"+searchValue+"%'";			
		}
		else if(searchType.equals("Email"))
		{
			hql = "from BorrowerRegistrationDetails where borrower_emailid ='"+searchValue+"'";			
		}
		else if(searchType.equals("MobileNo"))
		{
			hql = "from BorrowerRegistrationDetails where borrower_phno ='"+searchValue+"'";						
		}
		else
		{
			int borrowerId = Integer.parseInt(searchValue);
			
			hql = "from BorrowerRegistrationDetails where borrower_id ="+borrowerId+"";	
		}
		
		List<BorrowerRegistrationDetails> lsdata = session.createQuery(hql).list();	
		
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<BorrowerRegistrationDetails> getLastBorrowerDetails()
	{
		Session session = sessionFactory.openSession();
		
		Query query = session.createQuery("from BorrowerRegistrationDetails order by borrower_reg_time desc");		
		query.setMaxResults(20); 
		
		List<BorrowerRegistrationDetails> lsdata = query.list();
		
		session.close();
		
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LenderRegistrationDetails> getSearchLender(String searchType,String searchValue)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "";
		
		if(searchType.equals("Name"))
		{
			hql = "from LenderRegistrationDetails where full_name LIKE '%"+searchValue+"%'";			
		}
		else if(searchType.equals("Email"))
		{
			hql = "from LenderRegistrationDetails where lender_emailid ='"+searchValue+"'";			
		}
		else if(searchType.equals("MobileNo"))
		{
			hql = "from LenderRegistrationDetails where lender_phno ='"+searchValue+"'";					
		}
		else
		{
			int lenderId = Integer.parseInt(searchValue);
			
			hql = "from LenderRegistrationDetails where lender_id ="+lenderId+"";
		}
		
		List<LenderRegistrationDetails> lsdata = session.createQuery(hql).list();	
		
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<LenderRegistrationDetails> getLastLenderDetails()
	{
		Session session = sessionFactory.openSession();
		
		Query query = session.createQuery("from LenderRegistrationDetails order by lender_reg_time desc");		
		query.setMaxResults(20); 
		
		List<LenderRegistrationDetails> lsdata = query.list();
		
		session.close();
		
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ControllerDetails> getControllerDetails(String controllerEmailId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "";
		
		hql = "from ControllerDetails where controller_user_id ='"+controllerEmailId+"'";	
		
		List<ControllerDetails> lsdata = session.createQuery(hql).list();	
		
		session.close();
		
		return lsdata;
	}
	
	
	public String getSaveNotifyDetails(InterPortalNotificationModel notifyModel)
	{
		String txStatus="";
    	
    	Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(notifyModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		
		return txStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public String checkControllerPassword(String encCurPassword,String controllerEmailId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from ControllerDetails where controller_user_id ='"+controllerEmailId+"'";
		List<ControllerDetails> lsdata = session.createQuery(hql).list();
		 
		session.close();
		
		if(lsdata.get(0).getController_password().equals(encCurPassword))
		{
			return "Matched";
		}
		else
		{
			return "NotMatched";
		}
	}
	
	public String changeControllerPassword(String encNewPassword,String controllerEmailId)
	{
		Session session = sessionFactory.openSession();
		
		org.hibernate.Query hql = session.createQuery("update ControllerDetails set controller_password = :newPassword where controller_user_id = :controllerEmailId");
		
		hql.setParameter("newPassword", encNewPassword);
		hql.setParameter("controllerEmailId", controllerEmailId);
		
	    int result  = hql.executeUpdate();
		
	    session.close();
	    
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		} 
	}
	
	@SuppressWarnings("unchecked")
	public String getUpdateControllerDetails(String controllerEmailId,String name,String role,String fileName)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hqlQ = "from ControllerDetails where controller_user_id='"+controllerEmailId+"'";
		List<ControllerDetails> lsdata = session.createQuery(hqlQ).list();
		 
		if(fileName.equals(""))
		{
			if(lsdata.get(0).getController_photo()!=null)
			{
				fileName += lsdata.get(0).getController_photo();
			}
		}
		
		org.hibernate.Query hql = session.createQuery("update ControllerDetails set controller_name = :name , controller_designation = :designation , controller_photo = :photo where controller_user_id = :controllerEmailId");
		
		hql.setParameter("name", name);
		hql.setParameter("designation", role);
		hql.setParameter("photo", fileName);
		
		hql.setParameter("controllerEmailId", controllerEmailId);
		
	    int result  = hql.executeUpdate();
		
	    tx.commit();
	    session.close();

		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		} 
	}
	
	public String manageBorrowerStatus(int borrowerId,String condition)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query hql = session.createQuery("update BorrowerRegistrationDetails set borrower_status = :condition where borrower_id = :borrowerId");
		
		hql.setParameter("borrowerId", borrowerId);
		
		if(condition.equals("Block"))
		{
			hql.setParameter("condition", "Deactive");
		}
		else
		{
			hql.setParameter("condition", "Active");
		}
		
	    int result  = hql.executeUpdate();
		
	    tx.commit();
	    session.close();

		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		} 
	}
	
	
	
	
	public String manageLenderStatus(int lenderId,String condition)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query hql = session.createQuery("update LenderRegistrationDetails set lender_status = :condition where lender_id = :lenderId");
		
		hql.setParameter("lenderId", lenderId);
		
		if(condition.equals("Block"))
		{
			hql.setParameter("condition", "Deactive");
		}
		else
		{
			hql.setParameter("condition", "Active");
		}
		
	    int result  = hql.executeUpdate();
		
	    tx.commit();
	    session.close();

		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		} 
	}
}
