package com.finance.dao;

import java.util.List;

import com.finance.bean.ControllerDetailsBean;
import com.finance.bean.LenderDetailsBean;
import com.finance.domain.AdminDetails;
import com.finance.domain.BorrowerRegistrationDetails;
import com.finance.domain.ContactUsModel;
import com.finance.domain.ControllerDetails;
import com.finance.domain.EmailModel;
import com.finance.domain.FinancePolicyDomain;
import com.finance.domain.LenderPolicyModel;
import com.finance.domain.LenderRegistrationDetails;
import com.finance.domain.SearchLoanModel;
import com.finance.domain.UserOTPModel;

public interface AdminDao {

	public List<AdminDetails> getAdminDetails();
	
	public long countNoOfBorrower();
	public long countNoOfLender();
	public long countNoOfController();
	public long countNoOfPolicy();
	public long countNoOfAppliedPolicy();
	public long countNoOfFinalizedPolicy();
	public long countNoOfProcessPolicy();
	public long countNoOfOnholdPolicy();
	public long countNoOfSearchLoan();
	public long countNoOfClosedPolicy();
	public long countNoOfApprovedPolicy();
	public long countNoOfPendingPolicy();
	
	public String getValidateAdmin(String user_id,String password);
	
	public List<FinancePolicyDomain> getPolicyDetails();
	
	public String saveConatactFormDetails(ContactUsModel contactDetail);
	
	public String saveControllerdetails(ControllerDetails controllerDetails);
	
	public List<ControllerDetails> searchController(String searchType,String searchValue);
	
	public String updateControllerStatus(int controller_id,String requestedStatus);
	
	public List<BorrowerRegistrationDetails> getBorrowerDetails(String type,String value);
	
	public String checkAdminPassword(String encCurPassword);
	
	public String changeAdminPassword(String newPassword);
	
	public String getUpdateAdminDetails(String name,String role,String fileName);
	
	public String getSaveEmailDetails(EmailModel emailDetail);
	
	public String savePolicyTypeDetails(FinancePolicyDomain financialPolicy);
	
	public List<FinancePolicyDomain> getLoanType(String type,String value);
	
	public String deletePolicyType(int typeId);
	
	public List<String> getPolicyType();
	
	public List<FinancePolicyDomain> getPolicyTypeName();
	
	public List<LenderRegistrationDetails> searchLenderAsPerFilter(String type,String value);
	
	public List<LenderPolicyModel> getPolicyDetailsViaLenderId(int lenderId);
	
	public List<LenderPolicyModel> getPolicyDetailsViaType(String subType);
	
	public List<LenderRegistrationDetails> getLenderDetails(int lenderId);
	
	public List<LenderPolicyModel> getLenPolicyDetailsViaPolicyId(int policyId);
	
	public String updatePolicyByAdmin(int policyId,String policyStatus);
	
	public String updateBorrowerAccess(int borrowerId,String currentStatus);
	
	public String updateLenderAccess(int lenderId,String currentStatus);
	
	public String getFindUser(String userType,String searchType,String searchValue);
	
	
	public List<BorrowerRegistrationDetails> getBorrowerDetails(String emailid);
	
	public List<LenderRegistrationDetails> getLenderDetails(String emailid);
	
	public List<ControllerDetails> getControllerDetails(String emailid);
	
	
	public String saveSearchLoanDetails(SearchLoanModel searchLoanModel);
	
	public String getSaveOTPDetails(UserOTPModel otpModel);
	
	
	
	
}
