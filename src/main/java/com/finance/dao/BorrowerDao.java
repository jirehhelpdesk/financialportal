package com.finance.dao;

import java.util.List;

import com.finance.domain.AttachementDocumentModel;
import com.finance.domain.BorrowerDocumentModel;
import com.finance.domain.BorrowerIdentityModel;
import com.finance.domain.BorrowerRegistrationDetails;
import com.finance.domain.BorrowerSearchedDealDomain;
import com.finance.domain.InterPortalNotificationModel;
import com.finance.domain.LenderPolicyModel;
import com.finance.domain.LenderToBorrowerFinanceDealModel;
import com.finance.domain.LinkValidationModel;

public interface BorrowerDao {

	public String saveBorrowerRegdetails(BorrowerRegistrationDetails borrowerRegDetails);
	
	public int getBorrowerIdVieEmailId(String emailId);
	
	public String getValidateBorrower(String userId,String decryptPassword);
	
	public String saveLinkValidatedetails(LinkValidationModel linkValidate);
	
	public String getEmailIdFromForgetPassword(String uniqueId);
	
	public String getChangePassword(String emailId,String encPassword);
	
	public String getEmailIdViaUnqid(String activeAccountId);
	
	public String getActiveAcount(String userType,String emailId,String activeAccountId);
	
	public String checkRegisterEmailId(String emailId,String userType);
	
	public String checkBorrowerPhno(String phno,String userType);
	
	public String checkBorrowerPanId(String panId,String userType);
	
	public List<BorrowerRegistrationDetails> getBorrowerDetails(int borrowerId);
	
	public String updateBorrowerIdentityDetails(BorrowerIdentityModel borrowerIdnDetails);
	
	public String getProfilePhoto(int borrowerId);
	
	public String getChangePassword(int borrowerId,String password);
	
	public List<LenderPolicyModel> getSearchPolicyList(String type,String amount,long givenAmount,String time,int policy_min_duration,String location);
	
	public List<LenderPolicyModel> getPolicyInformation(int policyId);
	
	public String getUpdateBorrowerDetails(BorrowerIdentityModel borrowerIdnDetails);
	
	public String savePolicyDealBetweenBorLen(LenderToBorrowerFinanceDealModel dealModel);
	
	public List<LenderToBorrowerFinanceDealModel> getBorrowerPolicyDealDetails(int borrowerId);
	
	public List<LenderToBorrowerFinanceDealModel> getBorrowerPolicyDealDetailsViaDealId(int dealId);
	
	public String getPolicyComponent(String policyType);
	
	public List<BorrowerSearchedDealDomain> getLastSearchDeals(int borrowerId);
	
	public List<LenderPolicyModel> getPolicyDetails(List<BorrowerSearchedDealDomain> searchModel);
	
	public List<LenderToBorrowerFinanceDealModel> getLastDealDetails(int borrowerId);
	
	public List<BorrowerSearchedDealDomain> checkSearchData(BorrowerSearchedDealDomain searchModel);
	
	public String saveBoroowerSearchDetails(BorrowerSearchedDealDomain searchModel);
	
	public List<InterPortalNotificationModel> getNotification(String userType,int userId);
	
	public List<InterPortalNotificationModel> getNotifyDetails(String userType,int notifyId);
	
	public List<BorrowerDocumentModel> getLastBorrowerDocRecord(int borrowerId);
	
	public List<AttachementDocumentModel> getBorrowerDocList();
	
	public int getLastDocSequence(int borrowerId,String doc_type);
	
	public String saveDocumentDetails(BorrowerDocumentModel borrowerDocs);
	
	public List<BorrowerDocumentModel> getDocumentDetail(int documentId);
	
	public List<BorrowerDocumentModel> attachedDocuments(int borrowerId);
	
	public int getDocumentIdViaDocType(int borrowerId,String docType);
	
	public String activateAccountViaOTP(String otpNumber);
	
	public String getMobileNoViaEmailId(String userId);
	
	
	
	
}
