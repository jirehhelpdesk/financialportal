package com.finance.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.finance.bean.LenderToBorrowerFinanceDealBean;
import com.finance.domain.BorrowerDocumentModel;
import com.finance.domain.BorrowerRegistrationDetails;
import com.finance.domain.InterPortalNotificationModel;
import com.finance.domain.LenderDocumentModel;
import com.finance.domain.LenderIdentityModel;
import com.finance.domain.LenderPolicyModel;
import com.finance.domain.LenderRegistrationDetails;
import com.finance.domain.LenderToBorrowerFinanceDealModel;
import com.finance.domain.LinkValidationModel;

@SuppressWarnings("unused")
@Repository("lenderDao")
public class LenderDaoImpl implements LenderDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	public String saveLenderRegdetails(LenderRegistrationDetails lenderRegDetails)
	{
		    String txStatus="";
	    	
	    	Session session=sessionFactory.openSession();
			Transaction tx=null;
			try {
			tx=session.beginTransaction();
			session.save(lenderRegDetails);
			
			tx.commit();
			if(tx.wasCommitted()){
				txStatus="success";
								
			} else {
				txStatus="failure";
			}
			} catch (Exception he) {
				System.out.println(he.getMessage());
			} finally {
				session.close();
			}
			
			return txStatus;	
	}
	
	
	@SuppressWarnings("unchecked")
	public String getValidateLender(String userId,String decryptPassword,HttpSession session)
	{
		Session hibSession = sessionFactory.openSession();
		
		String hql = "";
		List<String> lsdata = new ArrayList<String>();	
		
		if(userId.contains("@"))
		{
			hql = "select lender_status from LenderRegistrationDetails where lender_emailid = '"+userId+"' and lender_password='"+decryptPassword+"' and lender_status='Active'";			
			lsdata = hibSession.createQuery(hql).list();	
			
			if(lsdata.size()>0)
			{
				session.setAttribute("lenderAccess", "primaryUser");
				
				hibSession.close();
				
				return lsdata.get(0);
			}
			else
			{
				
				hql = "select lender_status from LenderRegistrationDetails where lender_second_representative_email_id = '"+userId+"' and lender_second_representative_password='"+decryptPassword+"'";			
				List<String> subData = hibSession.createQuery(hql).list();
				
				hibSession.close();
				
				if(subData.size()>0)
				{
					session.setAttribute("lenderAccess", "secondaryUser");
					return subData.get(0);
				}
				else
				{
					return "invalid";
				}
			}
		}
		else
		{
			hql = "select lender_status from LenderRegistrationDetails where lender_phno = '"+userId+"' and lender_password='"+decryptPassword+"'";			
			lsdata = hibSession.createQuery(hql).list();	
			
			if(lsdata.size()>0)
			{
				session.setAttribute("lenderAccess", "primaryUser");
				
				hibSession.close();
				
				return lsdata.get(0);
			}
			else
			{
				hql = "select lender_status from LenderRegistrationDetails where lender_second_representative_mobile_no = '"+userId+"' and lender_second_representative_password='"+decryptPassword+"'";			
				List<String> subData = hibSession.createQuery(hql).list();
				
				hibSession.close();
				
				if(subData.size()>0)
				{
					session.setAttribute("lenderAccess", "secondaryUser");
					return subData.get(0);
				}
				else
				{
					return "invalid";
				}
			}
		}		
	}
	
	@SuppressWarnings("unchecked")
	public int getLenderIdViaUserId(String userId,HttpSession session)
	{
		Session hibSession = sessionFactory.openSession();

		String lenderAccess = (String)session.getAttribute("lenderAccess");
		
		if(userId.contains("@"))
		{
			if(lenderAccess.equals("primaryUser"))
			{
				String hql = "select lender_id from LenderRegistrationDetails where lender_emailid = '"+userId+"'";
				List<Integer> lsdata = hibSession.createQuery(hql).list();	
				
				hibSession.close();
				
				if(lsdata.size()>0)
				{
					return lsdata.get(0);
				}
				else
				{
					return 0;
				}
			}
			else
			{
				String subHql = "select lender_id from LenderRegistrationDetails where lender_second_representative_email_id = '"+userId+"'";
				List<Integer> subLsdata = hibSession.createQuery(subHql).list();	
				
				hibSession.close();
				
				if(subLsdata.size()>0)
				{
					return subLsdata.get(0);
				}
				else
				{
					return 0;
				}
			}			
		}
		else
		{
			if(lenderAccess.equals("primaryUser"))
			{
				String hql = "select lender_id from LenderRegistrationDetails where lender_phno = '"+userId+"'";
				List<Integer> lsdata = hibSession.createQuery(hql).list();	
				
				hibSession.close();
				
				if(lsdata.size()>0)
				{
					return lsdata.get(0);
				}
				else
				{					
					return 0;
				}
			}			
			else
			{
				String subHql = "select lender_id from LenderRegistrationDetails where lender_second_representative_mobile_no = '"+userId+"'";
				List<Integer> subLsdata = hibSession.createQuery(subHql).list();	
				
				hibSession.close();
				
				if(subLsdata.size()>0)
				{
					return subLsdata.get(0);
				}
				else
				{					
					return 0;
				}	
			}	
		}
		
	}
	
	
	public String saveLinkValidatedetails(LinkValidationModel linkValidate)
	{
		 String txStatus="";
	    	
	    	Session signUpSession=sessionFactory.openSession();
			Transaction tx=null;
			try {
			tx=signUpSession.beginTransaction();
			signUpSession.saveOrUpdate(linkValidate);
			tx.commit();
			if(tx.wasCommitted()){
				txStatus="success";
			} else {
				txStatus="failure";
			}
			} catch (Exception he) {
				System.out.println(he.getMessage());
			} finally {
				signUpSession.close();
			}
			
			return txStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public String getEmailIdFromForgetPassword(String uniqueId)
	{
		Session session = sessionFactory.openSession();

		String hql = "select validate_email_id from LinkValidationModel where validate_id = '"+uniqueId+"' and validate_init_date=(select MAX(validate_init_date) from LinkValidationModel where validate_id = '"+uniqueId+"' and validate_status='Pending')";
		List<String> lsdata = session.createQuery(hql).list();	
		
		session.close();

		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "Not Exist";
		}	
	}
	
	
	@SuppressWarnings("unchecked")
	public String getChangePassword(String emailId,String encPassword)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query hql = session.createQuery("update LenderRegistrationDetails set lender_password = :encPassword where lender_emailid = :emailId");
		hql.setParameter("emailId", emailId);
		
		hql.setParameter("encPassword",encPassword);
		
	    int result  = hql.executeUpdate();
		
	    tx.commit();
	   
		if(result==1)
		{			
			String uniqueId = "select validate_id from LinkValidationModel where validate_email_id = '"+emailId+"' and validate_init_date=(select MAX(validate_init_date) from LinkValidationModel where validate_email_id = '"+emailId+"' and validate_status='Pending')";
			List<String> lsdata = session.createQuery(uniqueId).list();	
			
			if(lsdata.size()>0)
			{
				org.hibernate.Query updateHql = session.createQuery("update LinkValidationModel set validate_status = :setStatus where validate_id = :uniqueId");
				updateHql.setParameter("uniqueId", lsdata.get(0));			
				updateHql.setParameter("setStatus", "Over");
				
			    int update  = updateHql.executeUpdate();
				
			    tx.commit();
			    session.close();

			    if(update==1)
				{
			    	return "success";
				}
			    else
			    {
			    	return "failed";
			    }	
			}
			else
			{
				tx.commit();
				session.close();

				return "failed";
			}			    
		}
		else
		{			
			return "failed";
		}
		
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<LenderRegistrationDetails> getAllLenderDetails(int lenderId)
	{
		Session session = sessionFactory.openSession();

	    Query query = session.createQuery("from LenderRegistrationDetails where lender_id="+lenderId+"");
	    List<LenderRegistrationDetails> lenderDetails = query.list();
	   
	    session.close();

	    return lenderDetails;
	}
	
	
	@SuppressWarnings("unchecked")
	public String changePassword(int lenderId,String currentPassword,String newPassword,String lenderRepresentative)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String status = "";
		
		if(lenderRepresentative.equals("primaryUser"))
		{
			String hql = "select lender_password from LenderRegistrationDetails where lender_id = "+lenderId+"";
			List<String> lsdata = session.createQuery(hql).list();	
			
			if(lsdata.size()>0)
			{
				if(lsdata.get(0).equals(currentPassword))
				{
					org.hibernate.Query updateHql = session.createQuery("update LenderRegistrationDetails set lender_password = :setPassword where lender_id = :lenderId");
					updateHql.setParameter("setPassword", newPassword);			
					updateHql.setParameter("lenderId", lenderId);
					
				    int update  = updateHql.executeUpdate();
					
				    if(update==1)
					{
				    	status =  "success";
					}
				    else
				    {
				    	status =  "failed";
				    }
				}
				else
				{
					status = "Not Matched";
				}			
			}
			else
			{
				status = "No Data";
			}	
		}
		else
		{
			String hql = "select lender_second_representative_password from LenderRegistrationDetails where lender_id = "+lenderId+"";
			List<String> lsdata = session.createQuery(hql).list();	
			
			if(lsdata.size()>0)
			{
				if(lsdata.get(0).equals(currentPassword))
				{
					org.hibernate.Query updateHql = session.createQuery("update LenderRegistrationDetails set lender_second_representative_password = :setPassword where lender_id = :lenderId");
					updateHql.setParameter("setPassword", newPassword);			
					updateHql.setParameter("lenderId", lenderId);
					
				    int update  = updateHql.executeUpdate();
					
				    if(update==1)
					{
				    	status =  "success";
					}
				    else
				    {
				    	status =  "failed";
				    }
				}
				else
				{
					status = "Not Matched";
				}			
			}
			else
			{
				status = "No Data";
			}	
		}
		
		
	    tx.commit();
	    session.close();
	    
		return status;
	}
	
	
	public String saveLenderProfileIdnDetails(LenderIdentityModel lenderIdnDetails)
	{
		    String txStatus="";
	    	
	    	Session session=sessionFactory.openSession();
			Transaction tx=null;
			try {
			tx=session.beginTransaction();
			session.saveOrUpdate(lenderIdnDetails);       
			tx.commit();
			if(tx.wasCommitted()){
				txStatus="success";
			} else {
				txStatus="failure";
			}
			} catch (Exception he) {
				System.out.println(he.getMessage());
			} finally {
				
				session.close();
			}
			
			return txStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public String getLenderProfilePhoto(int lenderId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "select lender_photo from LenderIdentityModel where lender_id = "+lenderId+"";
		List<String> lsdata = session.createQuery(hql).list();	
		
		session.close();
		
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "No Photo";
		}	
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getSubPolicyTypeName(String policyName)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "select sub_policy_type from FinancePolicyDomain where policy_type_name = '"+policyName+"'";
		List<String> lsdata = session.createQuery(hql).list();	
		
		session.close();
		
		return lsdata;		
	}
	
	
	public String saveLenderPolicy(LenderPolicyModel policyModel)
	{
		String txStatus="";
    	
    	Session session=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=session.beginTransaction();
		session.saveOrUpdate(policyModel);       
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			
			session.close();
		}
		
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<LenderPolicyModel> getPolicyDetails(int policyId,int lenderId)
	{
		Session session = sessionFactory.openSession();
		
		Criteria hql = session.createCriteria(LenderPolicyModel.class);
		
		hql.add(Restrictions.eq("policy_id",policyId));		
	    
	    List<LenderPolicyModel> lsdata =  hql.list();
		    
	    session.close();
	    
		return lsdata;	
	}
	
	@SuppressWarnings("unchecked")
	public String getBroucherFile(int policyId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "select policy_broucher from LenderPolicyModel where policy_id = "+policyId+"";
		List<String> lsdata = session.createQuery(hql).list();	
		
		session.close();
		
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "";
		}	
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<LenderPolicyModel> getLenderPolicyDetails(int lenderId)
	{
		Session session = sessionFactory.openSession();
		
		Criteria hql = session.createCriteria(LenderPolicyModel.class);
		
		hql.add(Restrictions.eq("lender_id",lenderId));		
	    hql.addOrder(Order.desc("cr_date"));
	    
	    List<LenderPolicyModel> lsdata =  hql.list();
		    
	    session.close();
	    
		return lsdata;	
	}
	
	@SuppressWarnings("unchecked")
	public List<LenderPolicyModel> getSearchLenderPolicyDetails(int lenderId,String searchType,String policyTypeValue)
	{
		Session session = sessionFactory.openSession();
		
		Criteria hql = session.createCriteria(LenderPolicyModel.class);
		
		hql.add(Restrictions.eq("lender_id",lenderId));		
	    hql.addOrder(Order.desc("cr_date"));
	    
	    if(searchType.equals("Via Loan Type"))
	    {
	    	hql.add(Restrictions.eq("policy_type",policyTypeValue));
	    }
	    
	    List<LenderPolicyModel> lsdata =  hql.list();
		    
	    session.close();
	    
		return lsdata;	
	}
	
	@SuppressWarnings("unchecked")
	public List<LenderPolicyModel> getLenderLastPolicyDetails(int lenderId)
	{
		Session session = sessionFactory.openSession();
		
		Criteria hql = session.createCriteria(LenderPolicyModel.class);
		
		hql.add(Restrictions.eq("lender_id",lenderId));		
	    hql.addOrder(Order.desc("cr_date"));
	    hql.setMaxResults(4);
	    
	    List<LenderPolicyModel> lsdata =  hql.list();
		    
	    session.close();
	    
		return lsdata;	
	}
	
	
	@SuppressWarnings("unchecked")
	public List<String> getLenderDistinctPolicyDetails(int lenderId)
	{
		Session session = sessionFactory.openSession();
		
		Criteria hql = session.createCriteria(LenderPolicyModel.class);
		
		hql.setProjection(Projections.distinct(Projections.property("policy_type")));
	    hql.add(Restrictions.eq("lender_id",lenderId));
	    
	    List<String> lsdata = hql.list();
		
	    session.close();
	    
		return lsdata;	
	}
	
	@SuppressWarnings("unchecked")
	public List<LenderToBorrowerFinanceDealModel> getLenderLastBorrowerRequest(int lenderId)
	{
		Session session = sessionFactory.openSession();
		
		Criteria hql = session.createCriteria(LenderToBorrowerFinanceDealModel.class);
		
	    hql.add(Restrictions.eq("lender_id",lenderId));
	    hql.add(Restrictions.eq("lender_status","Approved"));
	    hql.addOrder(Order.desc("deal_initiated_date"));
	    hql.setMaxResults(4);
	    
	    List<LenderToBorrowerFinanceDealModel> eModel = hql.list();
	    
	    session.close();
	    
	    return eModel;	    
	}
	
	@SuppressWarnings("unchecked")
	public List<LenderPolicyModel> getLenPolicyDetailViaPolicyId(int policyId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from LenderPolicyModel where policy_id = "+policyId+"";
		List<LenderPolicyModel> lsdata = session.createQuery(hql).list();	
		
		session.close();
		
		return lsdata;	
	}
	
	@SuppressWarnings("unchecked")
	public List<LenderToBorrowerFinanceDealModel> getBorrowerPolicyRequest(LenderToBorrowerFinanceDealBean policyReqBean)
	{
		Session session = sessionFactory.openSession();
		
		String searchType = policyReqBean.getExtra_info();
		
		List<LenderToBorrowerFinanceDealModel> eModel = new ArrayList<LenderToBorrowerFinanceDealModel>();
		
		if(searchType.equals("Via Duration"))
		{
			DateFormat formatter = null;
	        
		    Date startingDate = null;
		    Date endingDate = null;
		    
		    try 
		    {		    	
		        formatter = new SimpleDateFormat("dd/MM/yyyy");
		        startingDate = (Date) formatter.parse(policyReqBean.getSearch_from_date());
		        endingDate = (Date) formatter.parse(policyReqBean.getSearch_to_date());
		        
		    } catch (ParseException e) {
		        e.printStackTrace();
		    }	
		    
		    Criteria hql = session.createCriteria(LenderToBorrowerFinanceDealModel.class);
	
		    hql.add(Restrictions.eq("lender_status","Pending"));	
		    hql.add(Restrictions.eq("lender_id",policyReqBean.getLender_id()));	
		    
		    hql.add(Restrictions.ge("deal_initiated_date",startingDate));
		    hql.add(Restrictions.le("deal_initiated_date",endingDate));	
		    hql.addOrder(Order.desc("deal_initiated_date"));
		    
		    
		    eModel = hql.list();
		}
		else if(searchType.equals("Via Loan Type"))
		{
			Criteria hql = session.createCriteria(LenderToBorrowerFinanceDealModel.class);
			
		    hql.add(Restrictions.eq("policy_type",policyReqBean.getPolicy_type()));	
		    hql.add(Restrictions.eq("lender_status","Pending"));	
		    hql.addOrder(Order.desc("deal_initiated_date"));
		    hql.add(Restrictions.eq("lender_id",policyReqBean.getLender_id()));	
		   // System.out.println("Final Query-"+hql.toString());
		    
		    eModel = hql.list();
		    
		   // System.out.println("Model Length-"+eModel.size());		    
		}
		else
		{
			Criteria hql = session.createCriteria(LenderToBorrowerFinanceDealModel.class);
			
		    hql.add(Restrictions.eq("lender_status","Pending"));			
		    hql.addOrder(Order.desc("deal_initiated_date"));
		    hql.add(Restrictions.eq("lender_id",policyReqBean.getLender_id()));	
		    //System.out.println("Final Query-"+hql.toString());
		    
		    eModel = hql.list();
		    
		    //System.out.println("Model Length-"+eModel.size());
		}
			
		session.close();
		
		return eModel;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LenderToBorrowerFinanceDealModel> getBorrowerPolicyApproved(LenderToBorrowerFinanceDealBean policyReqBean)
	{
		Session session = sessionFactory.openSession();
		
        String searchType = policyReqBean.getExtra_info();
		
		List<LenderToBorrowerFinanceDealModel> eModel = new ArrayList<LenderToBorrowerFinanceDealModel>();
		
		if(searchType.equals("Via Duration"))
		{
			DateFormat formatter = null;
	        
		    Date startingDate = null;
		    Date endingDate = null;
		    
		    try 
		    {		    	
		        formatter = new SimpleDateFormat("dd/MM/yyyy");
		        startingDate = (Date) formatter.parse(policyReqBean.getSearch_from_date());
		        endingDate = (Date) formatter.parse(policyReqBean.getSearch_to_date());
		        
		    } catch (ParseException e) {
		        e.printStackTrace();
		    }	
		    
		    Criteria hql = session.createCriteria(LenderToBorrowerFinanceDealModel.class);
	        
		    hql.add(Restrictions.eq("deal_status","Approved"));			
		    hql.add(Restrictions.eq("lender_id",policyReqBean.getLender_id()));	
		    
		    hql.add(Restrictions.ge("deal_finalize_date",startingDate));
		    hql.add(Restrictions.le("deal_finalize_date",endingDate));	
		    hql.addOrder(Order.desc("deal_finalize_date"));
		    
		    eModel = hql.list();
		}
		else if(searchType.equals("Via Loan Type"))
		{
			Criteria hql = session.createCriteria(LenderToBorrowerFinanceDealModel.class);
			
		    hql.add(Restrictions.eq("policy_type",policyReqBean.getPolicy_type()));	
		    hql.add(Restrictions.eq("deal_status","Approved"));		
		    hql.addOrder(Order.desc("deal_finalize_date"));
		    hql.add(Restrictions.eq("lender_id",policyReqBean.getLender_id()));	
		    
		    eModel = hql.list();		    	    
		}
		else
		{
			Criteria hql = session.createCriteria(LenderToBorrowerFinanceDealModel.class);
			
		    hql.add(Restrictions.eq("deal_status","Approved"));			
		    hql.addOrder(Order.desc("deal_finalize_date"));
		    hql.add(Restrictions.eq("lender_id",policyReqBean.getLender_id()));	
		    
		    eModel = hql.list();		    
		}
			
		session.close();
		
		return eModel;
	}

	@SuppressWarnings("unchecked")
	public List<LenderToBorrowerFinanceDealModel> getBorrowerInterestedRequest(LenderToBorrowerFinanceDealBean policyReqBean)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String searchType = policyReqBean.getExtra_info();
		
		List<LenderToBorrowerFinanceDealModel> eModel = new ArrayList<LenderToBorrowerFinanceDealModel>();
		
		if(searchType.equals("Via Duration"))
		{
			DateFormat formatter = null;
	        
		    Date startingDate = null;
		    Date endingDate = null;
		    
		    try 
		    {		    	
		        formatter = new SimpleDateFormat("dd/MM/yyyy");
		        startingDate = (Date) formatter.parse(policyReqBean.getSearch_from_date());
		        endingDate = (Date) formatter.parse(policyReqBean.getSearch_to_date());
		        
		    } catch (ParseException e) {
		        e.printStackTrace();
		    }	
		    
		    Criteria hql = session.createCriteria(LenderToBorrowerFinanceDealModel.class);
	
		    hql.add(Restrictions.eq("lender_status","Processing"));	
		    hql.add(Restrictions.eq("lender_id",policyReqBean.getLender_id()));	
		    
		    hql.add(Restrictions.ge("deal_initiated_date",startingDate));
		    hql.add(Restrictions.le("deal_initiated_date",endingDate));	
		    hql.addOrder(Order.desc("deal_initiated_date"));
		    
		    
		    eModel = hql.list();
		}
		else if(searchType.equals("Via Loan Type"))
		{
			Criteria hql = session.createCriteria(LenderToBorrowerFinanceDealModel.class);
			
		    hql.add(Restrictions.eq("policy_type",policyReqBean.getPolicy_type()));	
		    hql.add(Restrictions.eq("lender_status","Processing"));	
		    hql.addOrder(Order.desc("deal_initiated_date"));
		    hql.add(Restrictions.eq("lender_id",policyReqBean.getLender_id()));	
		   // System.out.println("Final Query-"+hql.toString());
		    
		    eModel = hql.list();
		    
		   // System.out.println("Model Length-"+eModel.size());		    
		}
		else
		{
			Criteria hql = session.createCriteria(LenderToBorrowerFinanceDealModel.class);
			
		    hql.add(Restrictions.eq("lender_status","Processing"));			
		    hql.addOrder(Order.desc("deal_initiated_date"));
		    hql.add(Restrictions.eq("lender_id",policyReqBean.getLender_id()));	
		    //System.out.println("Final Query-"+hql.toString());
		    
		    eModel = hql.list();
		    
		    //System.out.println("Model Length-"+eModel.size());
		}
			
		    tx.commit();
		    session.close();
		    
		    return eModel;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LenderToBorrowerFinanceDealModel> getBorrowerPolicyRequestDeatails(LenderToBorrowerFinanceDealBean policyReqBean)
	{
		Session session = sessionFactory.openSession();
		
		List<LenderToBorrowerFinanceDealModel> eModel = new ArrayList<LenderToBorrowerFinanceDealModel>();
		
		Criteria hql = session.createCriteria(LenderToBorrowerFinanceDealModel.class);
		
	    hql.add(Restrictions.eq("deal_id",policyReqBean.getDeal_id()));			
	    
	    //System.out.println("Final Query-"+hql.toString());
	    
	    eModel = hql.list();
	    
	    session.close();
	    
	    return eModel;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LenderToBorrowerFinanceDealModel> getLenderAcceptedDealDetails(int lenderId)
	{
		Session session = sessionFactory.openSession();
		
		List<LenderToBorrowerFinanceDealModel> eModel = new ArrayList<LenderToBorrowerFinanceDealModel>();
		
		Criteria hql = session.createCriteria(LenderToBorrowerFinanceDealModel.class);
		
	    hql.add(Restrictions.eq("lender_id",lenderId));			
	    hql.add(Restrictions.eq("deal_status","Approved"));	
	    hql.addOrder(Order.desc("deal_finalize_date"));
	    
	    //System.out.println("Final Query-"+hql.toString());
	    
	    eModel = hql.list();
	    
	    session.close();
	    
	    return eModel;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LenderToBorrowerFinanceDealModel> getDealStatus(int dealId)
	{
		Session session = sessionFactory.openSession();
		
		List<LenderToBorrowerFinanceDealModel> eModel = new ArrayList<LenderToBorrowerFinanceDealModel>();
		
		Criteria hql = session.createCriteria(LenderToBorrowerFinanceDealModel.class);
		
	    hql.add(Restrictions.eq("deal_id",dealId));			
	    
	    eModel = hql.list();
	    
	    session.close();
	    
	    return eModel;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LenderDocumentModel> attachedDocuments(int lenderId)
	{
		Session session = sessionFactory.openSession();
		
		List<LenderDocumentModel> docList = new ArrayList<LenderDocumentModel>();
		
		String hql = "SELECT DISTINCT document_type FROM LenderDocumentModel where lender_id="+lenderId+"";		
		List<String> lsdata = session.createQuery(hql).list();	
		
		// Need an Proper query to get the details		
		for(int i=0;i<lsdata.size();i++)			
		{			
			String subHql = "FROM LenderDocumentModel where lender_id="+lenderId+" and document_type='"+lsdata.get(i)+"' and cr_date=(select MAX(cr_date) from LenderDocumentModel where lender_id="+lenderId+" and document_type='"+lsdata.get(i)+"')";
			List<LenderDocumentModel> subData = session.createQuery(subHql).list();	
			
			docList.addAll(subData);
		}
	    
		session.close();
		
		return docList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LenderDocumentModel> getLastLenderDocRecord(int lenderId)
	{
		Session session = sessionFactory.openSession();
		
		Criteria hql = session.createCriteria(LenderDocumentModel.class);
		
		hql.add(Restrictions.eq("lender_id",lenderId));						
	    hql.addOrder(Order.desc("cr_date"));
	    hql.setMaxResults(1);
	    
	    List<LenderDocumentModel> lsdata =  hql.list();
		    
	    session.close();
	    
		return lsdata;	
	}
	
	
	@SuppressWarnings("unchecked")
	public int getLastDocSequence(int lenderId,String doc_type)
	{
		Session session = sessionFactory.openSession();
		
		int squence = 0;
		
		Transaction tx = session.beginTransaction();
	    Query query = session.createQuery("from LenderDocumentModel where lender_id="+lenderId+" and document_type='"+doc_type+"' and cr_date=(select MAX(cr_date) from LenderDocumentModel where lender_id="+lenderId+" and document_type='"+doc_type+"')");
	    List<LenderDocumentModel> detail = query.list();
	        
		if(detail.size()>0)
		{
			squence = detail.get(0).getDocument_count();
		}
		
		session.close();
		
		return squence;
	}
	
	public String saveLenderDocumentDetails(LenderDocumentModel lenderDoc)
	{
		String txStatus="";
    	
    	Session session=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=session.beginTransaction();
		session.saveOrUpdate(lenderDoc);       
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			
			session.close();
		}
		
		return txStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LenderDocumentModel> getDocumentDetail(int lenDocId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from LenderDocumentModel where lender_document_id="+lenDocId+"";
		List<LenderDocumentModel> lsdata = session.createQuery(hql).list();	
		
		session.close();
		
		return lsdata;		
	}
	
	
	 public long countDealIntiateWithPolicyId(int policyId)
	 {
		Session session = sessionFactory.openSession();
		 
		long count = 0;
			
		count = ((Long)session.createQuery("select count(*) from LenderToBorrowerFinanceDealModel where policy_id="+policyId+"").uniqueResult()).longValue();
		
		session.close();
		
		return count;		 
	 }
		
	 public long countDealPendingWithPolicyId(int policyId)
	 {
		Session session = sessionFactory.openSession(); 
		
		long count = 0;
			
		count = ((Long)session.createQuery("select count(*) from LenderToBorrowerFinanceDealModel where policy_id="+policyId+" and deal_status='Pending'").uniqueResult()).longValue();
		
		session.close();
		
		return count;		 
	 }
		
	 public long countDealApprovedWithPolicyId(int policyId)
	 {
		Session session = sessionFactory.openSession();
		 
		long count = 0;
			
		count = ((Long)session.createQuery("select count(*) from LenderToBorrowerFinanceDealModel where policy_id="+policyId+" and deal_status='Approved'").uniqueResult()).longValue();
		
		session.close();
		
		return count;		 
	 }
	   
	
	 public String discontinuePolicy(int policyId)
	 {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query hql = session.createQuery("update LenderPolicyModel set policy_approval_status = :status where policy_id = :policyId");
		hql.setParameter("policyId", policyId);
		
		hql.setParameter("status", "Closed");
		
	    int result  = hql.executeUpdate();
		
	    tx.commit();
		session.close();
		 
		if(result==1)
		{	
			return "success";
		}
		else
		{
			return "failed";
		}
	 }
	
	 
	 @SuppressWarnings("unchecked")
	 public  List<LenderToBorrowerFinanceDealModel> getBorrowerLoanRequest(int lenderId)
	 {
		 Session session = sessionFactory.openSession();
		 Transaction tx = session.beginTransaction();

		 Criteria hql = session.createCriteria(LenderToBorrowerFinanceDealModel.class);
			
		 hql.add(Restrictions.eq("lender_id",lenderId));	
		 hql.add(Restrictions.eq("lender_status","Pending"));	
		 hql.add(Restrictions.eq("deal_status","Processing"));	
	     hql.addOrder(Order.desc("deal_initiated_date"));
	    
	     List<LenderToBorrowerFinanceDealModel> lsdata =  hql.list();
		 
	     tx.commit();
		 session.close();
		 
	     
		 return lsdata;			
	 }
	
	 
	 public String manageBorrowerRequest(int dealId,int lenderId,String interest)
	 {
		 Session session = sessionFactory.openSession();
		 Transaction tx = session.beginTransaction();

		 int updatedEntities = 0;
		 String hqlUpdate = "update LenderToBorrowerFinanceDealModel c set c.lender_status = :status where c.deal_id = :dealId";
		 
		 if(interest.equals("Yes"))
		 {
			 updatedEntities = session.createQuery( hqlUpdate ).setString( "status", "Processing" ).setString( "dealId", dealId+"" ).executeUpdate();			 
		 }
		 else
		 {
			 updatedEntities = session.createQuery( hqlUpdate ).setString( "status", "Rejected" ).setString( "dealId", dealId+"" ).executeUpdate();			 
		 }
		 
		 tx.commit();
		 session.close();
		 
		 if(updatedEntities==1)
		 {
			 return "success";	
		 }
		 else
		 {
			 return "failed";
		 }
	 }
	 
	 public String submitReason(int dealId,String reason,String rejectStage)
	 {
		 Session session = sessionFactory.openSession();
		 Transaction tx = session.beginTransaction();

		 int updatedEntities = 0;
		 String status = "";
		 
		 if(rejectStage.equals("rejectFromPending"))
		 {
			 String hqlUpdate = "update LenderToBorrowerFinanceDealModel c set c.deal_status = :dealstatus ,c.lender_status = :status , c.pending_reject_reason = :pendingRejectReason where c.deal_id = :dealId";
			 
			 updatedEntities = session.createQuery( hqlUpdate ).setString( "dealstatus", "Rejected" ).setString( "status", "Rejected" ).setString( "pendingRejectReason", reason ).setString( "dealId", dealId+"" ).executeUpdate();			 		 
			 
			 status = "borrowerRqst";
		 }
		 else
		 {
			 String hqlUpdate = "update LenderToBorrowerFinanceDealModel c set c.deal_status = :dealstatus ,c.lender_status = :status , c.processing_reject_reason = :processingRejectReason where c.deal_id = :dealId";
			 
			 updatedEntities = session.createQuery( hqlUpdate ).setString( "dealstatus", "Rejected" ).setString( "status", "Rejected" ).setString( "processingRejectReason", reason ).setString( "dealId", dealId+"" ).executeUpdate();			 		 
		
			 status = "borrowerInterestRqst";
			 
		 }
		 
		 tx.commit();
		 session.close();
		 
		 if(updatedEntities==1)
		 {
			 status = status+"/success";
			 return status;	
		 }
		 else
		 {
			 status = status+"/failed";
			 return status;
		 }
	 }
	 
	 
	 @SuppressWarnings("unchecked")
	 public  List<LenderToBorrowerFinanceDealModel> getBorrowerApproveLoan(int lenderId)
	 {
		 Session session = sessionFactory.openSession();
		 Transaction tx = session.beginTransaction();

		 Criteria hql = session.createCriteria(LenderToBorrowerFinanceDealModel.class);
			
		 hql.add(Restrictions.eq("lender_id",lenderId));	
		 hql.add(Restrictions.eq("lender_status","Processing"));	
		 hql.add(Restrictions.eq("deal_status","Processing"));	
	     hql.addOrder(Order.desc("deal_initiated_date"));
	    
	     List<LenderToBorrowerFinanceDealModel> lsdata =  hql.list();
		 
	     session.close();
	     
		 return lsdata;			
	 }
	 
	 
	@SuppressWarnings("unchecked")
	public String getRequiredDocForPolicy(int policyId)
	{
		 Session session = sessionFactory.openSession();
		 String docList = "";
		 Query query = session.createQuery("from LenderPolicyModel where policy_id="+policyId+"");
		 List<LenderPolicyModel> detail = query.list();
		        
			if(detail.size()>0)
			{
				docList = detail.get(0).getPolicy_required_doc();
			}
		 
	     session.close();
	     
		 return docList;	
	 }
	 
	@SuppressWarnings("unchecked")
	public List<InterPortalNotificationModel> getLenderNotification(int lenderId)
	{
		 Session session = sessionFactory.openSession();
		
		 Criteria hql = session.createCriteria(InterPortalNotificationModel.class);
			
		 hql.add(Restrictions.eq("notify_reciver",lenderId));	
		 hql.add(Restrictions.eq("notify_reciver_type","Lender"));	
		 hql.add(Restrictions.ne("notify_status","Removed"));	
	     hql.addOrder(Order.desc("notify_date"));
	    
	     List<InterPortalNotificationModel> lsdata =  hql.list();
		 
	     session.close();
	     
		 return lsdata;	
	}
	
	
	@SuppressWarnings("unchecked")
	public List<InterPortalNotificationModel> getUnreadNotification(int userId,String userType)
	{
		 Session session = sessionFactory.openSession();
		
		 Criteria hql = session.createCriteria(InterPortalNotificationModel.class);
		 
		 hql.add(Restrictions.eq("notify_reciver",userId));	
		 
		 if(userType.equals("Lender"))
		 {			
			 hql.add(Restrictions.eq("notify_reciver_type","Lender"));	
		 }
		 else
		 {
			 hql.add(Restrictions.eq("notify_reciver_type","Borrower"));	
		 }
		 
	     hql.addOrder(Order.desc("notify_date"));
	     hql.add(Restrictions.eq("notify_status","Pending"));	
	     
	     List<InterPortalNotificationModel> lsdata =  hql.list();
		 
	     session.close();
	     
		 return lsdata;	
	}
	 
	
	public String approveBorrowerRequest(int dealId)
	{
		 Session session = sessionFactory.openSession();
		 Transaction tx = session.beginTransaction();

		 org.hibernate.Query hql = session.createQuery("update LenderToBorrowerFinanceDealModel set deal_status = :dealStatus , lender_status = :lenderStatus , deal_finalize_date = :dealFinalizeDate  where deal_id = :dealId");
		 hql.setParameter("dealId", dealId);
			
		 hql.setParameter("dealStatus","Approved");
		 hql.setParameter("lenderStatus","Approved");
		 hql.setParameter("dealFinalizeDate",new Date());
			
		 int result  = hql.executeUpdate();
			
		 tx.commit();
		 session.close();
		 
		 if(result==1)
		 {
			 return "success";	
		 }
		 else
		 {
			 return "failed";
		 }
	}
	 
	 
	 
	 public String deleteNoitification(int notifyId)
	 {
		 Session session = sessionFactory.openSession();
		 Transaction tx = session.beginTransaction();

		 org.hibernate.Query hql = session.createQuery("update InterPortalNotificationModel set notify_status = :status where notify_id = :notifyId");
		 hql.setParameter("notifyId", notifyId);
			
		 hql.setParameter("status","Removed");
		 
		 int result  = hql.executeUpdate();
			
		 tx.commit();
		 session.close();
		 
		 if(result==1)
		 {
			 return "success";	
		 }
		 else
		 {
			 return "failed";
		 }
	 }
	 
	 @SuppressWarnings("unchecked")
	 public List<InterPortalNotificationModel> getNotification(int notifyId)
	 {
		 Session session = sessionFactory.openSession();
			
		 Criteria hql = session.createCriteria(InterPortalNotificationModel.class);
		 
		 hql.add(Restrictions.eq("notify_id",notifyId));	
		 
	     List<InterPortalNotificationModel> lsdata =  hql.list();
		 
	     session.close();
	     
		 return lsdata;	
	 }
	 
	 public String getUpdateNotificationViewStatus(int notifyId)
	 {
		 Session session = sessionFactory.openSession();
		 Transaction tx = session.beginTransaction();

		 org.hibernate.Query hql = session.createQuery("update InterPortalNotificationModel set notify_status = :status where notify_id = :notifyId");
		 hql.setParameter("notifyId", notifyId);
			
		 hql.setParameter("status","Viewed");
		 
		 int result  = hql.executeUpdate();
			
		 tx.commit();
		 session.close();
		 
		 if(result==1)
		 {
			 return "success";	
		 }
		 else
		 {
			 return "failed";
		 }
	 }
	
	 @SuppressWarnings("unchecked")
	 public List<InterPortalNotificationModel> getSearchNotification(int userId,String searchType,String fromDateId,String toDateId)
	 {		
		 Session session = sessionFactory.openSession();
		 
			List<InterPortalNotificationModel> eModel = new ArrayList<InterPortalNotificationModel>();
			
			if(searchType.equals("Via Duration"))
			{
				DateFormat formatter = null;
		        
			    Date startingDate = null;
			    Date endingDate = null;
			    
			    try 
			    {		    	
			        formatter = new SimpleDateFormat("dd/MM/yyyy");
			        startingDate = (Date) formatter.parse(fromDateId);
			        endingDate = (Date) formatter.parse(toDateId);
			        
			    } catch (ParseException e) {
			        e.printStackTrace();
			    }	
			    
			    Criteria hql = session.createCriteria(InterPortalNotificationModel.class);
		
			    hql.add(Restrictions.ne("notify_status","Removed"));	
			    hql.add(Restrictions.eq("notify_reciver",userId));	
			    
			    hql.add(Restrictions.ge("notify_date",startingDate));
			    hql.add(Restrictions.le("notify_date",endingDate));	
			    hql.addOrder(Order.desc("notify_date"));
			    
			    
			    eModel = hql.list();
	       }
		   else
		   {
			    Criteria hql = session.createCriteria(InterPortalNotificationModel.class);
				
			    hql.add(Restrictions.ne("notify_status","Removed"));	
			    hql.add(Restrictions.eq("notify_reciver",userId));	
			    hql.addOrder(Order.desc("notify_date"));
			    
			    /*System.out.println("Query="+hql.toString());*/
			    
			    eModel = hql.list();
		   }
			
		session.close();
			
		return eModel;	
	 }
	 
	 public String getUpdateAmoundDispouseStatus(int dealId,String status)
	 {
		 Session session = sessionFactory.openSession();
		 Transaction tx = session.beginTransaction();

		 org.hibernate.Query hql = session.createQuery("update LenderToBorrowerFinanceDealModel set amount_dispouse = :status , amount_dispouse_date = :releaseDate  where deal_id = :dealId");
		 hql.setParameter("dealId", dealId);
			
		 hql.setParameter("status",status);
		 hql.setParameter("releaseDate",new Date());
		 
		 int result  = hql.executeUpdate();
			
		 tx.commit();
		 session.close();
		 
		 if(result==1)
		 {
			 return "success";	
		 }
		 else
		 {
			 return "failed";
		 }
	 }
}
