package com.finance.dao;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.finance.bean.LenderToBorrowerFinanceDealBean;
import com.finance.domain.InterPortalNotificationModel;
import com.finance.domain.LenderDocumentModel;
import com.finance.domain.LenderIdentityModel;
import com.finance.domain.LenderPolicyModel;
import com.finance.domain.LenderRegistrationDetails;
import com.finance.domain.LenderToBorrowerFinanceDealModel;
import com.finance.domain.LinkValidationModel;

public interface LenderDao {
	
	
	public String saveLenderRegdetails(LenderRegistrationDetails lenderRegDetails);

	public String getValidateLender(String userId,String decryptPassword,HttpSession session);
	
	public int getLenderIdViaUserId(String userId,HttpSession session);
	
	public String saveLinkValidatedetails(LinkValidationModel linkValidate);
	
	public String getEmailIdFromForgetPassword(String uniqueId);
	
	public String getChangePassword(String emailId,String password);
	
	public List<LenderRegistrationDetails> getAllLenderDetails(int lenderId);
	
	public String changePassword(int lenderId,String currentPassword,String newPassword,String lenderRepresentative);
	
	public String saveLenderProfileIdnDetails(LenderIdentityModel lenderIdnDetails);
	
	public String getLenderProfilePhoto(int lenderId);
	
	public List<String> getSubPolicyTypeName(String policyName);
	
	public String saveLenderPolicy(LenderPolicyModel policyModel);
	
	public List<LenderPolicyModel> getPolicyDetails(int policyId,int lenderId);
	
	public List<LenderPolicyModel> getLenderPolicyDetails(int lenderId);
	
	public List<LenderPolicyModel> getSearchLenderPolicyDetails(int lenderId,String searchType,String policyTypeValue);
	
	public List<LenderPolicyModel> getLenPolicyDetailViaPolicyId(int policyId);
	
	public List<LenderToBorrowerFinanceDealModel> getBorrowerPolicyRequest(LenderToBorrowerFinanceDealBean policyReqBean);
	
	public List<LenderToBorrowerFinanceDealModel> getBorrowerPolicyApproved(LenderToBorrowerFinanceDealBean policyReqBean);
	
	public List<LenderToBorrowerFinanceDealModel> getBorrowerPolicyRequestDeatails(LenderToBorrowerFinanceDealBean policyReqBean);
	
	public List<String> getLenderDistinctPolicyDetails(int lenderId);
	
	public List<LenderPolicyModel> getLenderLastPolicyDetails(int lenderId);
	
	public List<LenderToBorrowerFinanceDealModel> getLenderLastBorrowerRequest(int lenderId);
	
	public List<LenderToBorrowerFinanceDealModel> getLenderAcceptedDealDetails(int lenderId);
	
	public List<LenderToBorrowerFinanceDealModel> getDealStatus(int dealId);
	
	public List<LenderDocumentModel> attachedDocuments(int lenderId);
	
	public List<LenderDocumentModel> getLastLenderDocRecord(int lenderId);
	
	public int getLastDocSequence(int lenderId,String doc_type);
	
	public String saveLenderDocumentDetails(LenderDocumentModel lenderDoc);
	
	public List<LenderDocumentModel> getDocumentDetail(int lenDocId);
	
	public String getBroucherFile(int policyId);
	
	 public long countDealIntiateWithPolicyId(int policyId);
		
	 public long countDealPendingWithPolicyId(int policyId);
		
	 public long countDealApprovedWithPolicyId(int policyId);
	   
	 public String discontinuePolicy(int policyId);
	 
	 public  List<LenderToBorrowerFinanceDealModel> getBorrowerLoanRequest(int lenderId);
	
	 public String manageBorrowerRequest(int dealId,int lenderId,String interest);
	 
	 
	 public  List<LenderToBorrowerFinanceDealModel> getBorrowerApproveLoan(int lenderId);
	 
	 public List<LenderToBorrowerFinanceDealModel> getBorrowerInterestedRequest(LenderToBorrowerFinanceDealBean policyReqBean);
	
	 public String getRequiredDocForPolicy(int policyId);
	 
	 public List<InterPortalNotificationModel> getLenderNotification(int lenderId);
	 
	 public String approveBorrowerRequest(int dealId);
	 
	 public String deleteNoitification(int notifyId);
	 
	 
	 public List<InterPortalNotificationModel> getNotification(int notifyId);
	 
	 public List<InterPortalNotificationModel> getSearchNotification(int userId,String searchType,String fromDateId,String toDateId);

	 public String getUpdateNotificationViewStatus(int notifyId);

	 public List<InterPortalNotificationModel> getUnreadNotification(int userId,String userType);
	 
	 public String submitReason(int dealId,String reason,String rejectStage);

	 public String getUpdateAmoundDispouseStatus(int dealId,String status);
	 











}
