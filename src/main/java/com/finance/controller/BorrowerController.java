package com.finance.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.finance.bean.BorrowerDetailsBean;
import com.finance.bean.LenderPolicyBean;
import com.finance.bean.LenderToBorrowerFinanceDealBean;
import com.finance.domain.AdminDetails;
import com.finance.domain.BorrowerDocumentModel;
import com.finance.domain.BorrowerIdentityModel;
import com.finance.domain.BorrowerRegistrationDetails;
import com.finance.domain.BorrowerSearchedDealDomain;
import com.finance.domain.ControllerDetails;
import com.finance.domain.LenderToBorrowerFinanceDealModel;
import com.finance.service.AdminService;
import com.finance.service.BorrowerService;
import com.finance.service.LenderService;


// Borrower controller 
@Controller
public class BorrowerController {


	@Autowired
	LenderService lenderService;
	
	
	@Autowired
	private AdminService adminService;
	
	
	@Autowired
	BorrowerService borrowerService;
	
	
	@RequestMapping(value = "/progressBar", method = RequestMethod.GET)
	public String progressBar(HttpSession session,HttpServletRequest request) {
						
		return "aaaaProgressBar";				
	}
	
	@RequestMapping(value = "/borrowerLogout", method = RequestMethod.GET)
	public String adminLogout(HttpSession session,HttpServletRequest request) {
					
		session.removeAttribute("borrowerId");			 
		request.setAttribute("actionMessage", "success");
        
		return "redirect:/borrowerLogin";				
	}
	
	
	@RequestMapping(value = "/borrowerHome", method = RequestMethod.GET)
	public ModelAndView borrowerDashboard(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("borrowerId") == null)
		{				
        	return new ModelAndView("redirect:/borrowerLogin");
        }				
		else
		{
			int borrowerId = (int) session.getAttribute("borrowerId");
			
			BorrowerDetailsBean borrowerBean = borrowerService.getBorrowerDetails(borrowerId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("borrowerBean",borrowerBean);
			
			model.put("unreadNotification",lenderService.getUnreadNotification(borrowerId,"Borrower"));
			
			return new ModelAndView("borrower_dashboard",model);					
		}
	}   
		
	
	@RequestMapping(value = "/loans", method = RequestMethod.GET)
	public ModelAndView loans(HttpSession session,HttpServletRequest request) {
				
		if (session.getAttribute("borrowerId") == null)
		{									
        	return new ModelAndView("redirect:/borrowerLogin");
        }
		else
		{
			int borrowerId = (int) session.getAttribute("borrowerId");
			
			BorrowerDetailsBean borrowerBean = borrowerService.getBorrowerDetails(borrowerId);
			
			Map<String, Object> model = new HashMap<String, Object>();				

			model.put("borrowerBean",borrowerBean);
			
			request.setAttribute("nameList", adminService.getPolicyTypeName());
			
			model.put("unreadNotification",lenderService.getUnreadNotification(borrowerId,"Borrower"));
			
			return new ModelAndView("borrower_loans",model);				
		}
	} 
	
	
	@RequestMapping(value = "/searchFinance", method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchFinance(HttpSession session,HttpServletRequest request) {
		
		String type = request.getParameter("type");
		
		String amount = request.getParameter("amount");
		
		String location = request.getParameter("location");
		
		amount = amount.replaceAll(",","");
		
		String time = request.getParameter("time");
		
		Map<String, Object> model = new HashMap<String, Object>();
				
		model.put("policyResult",borrowerService.getSearchPolicyList(type,amount,time,location));
		
		session.setAttribute("requestAmount", amount);
		
		return new ModelAndView("borrower_search_finance_list",model);	
	}
	
	
	@RequestMapping(value = "/showPolicyInfoToBorrower", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showPolicyInfoToBorrower(HttpSession session,HttpServletRequest request) {
		
		int policyId = Integer.parseInt(request.getParameter("policyId"));
		
		Map<String, Object> model = new HashMap<String, Object>();
				
		model.put("policyInfo",borrowerService.getPolicyInformation(policyId));
		
		
		// SAVE BORROWER SEARCH POLICY 
			
			int borrowerId = (int) session.getAttribute("borrowerId");
			
			BorrowerSearchedDealDomain searchModel = new BorrowerSearchedDealDomain();
			
			searchModel.setBorrower_id(borrowerId);
			searchModel.setPolicy_id(policyId);
			searchModel.setSearched_date(new Date());
			
			borrowerService.saveBorrowerSearchPolicy(searchModel);
			
		//  END OF BORROWER SEARCH POLICY
		
		
		return new ModelAndView("borrower_view_policy_info",model);	
	}
	
	
	@RequestMapping(value = "/checkPolicyApplicableCriteria", method = RequestMethod.POST)
	@ResponseBody public String checkPolicyApplicableCriteria(HttpSession session,HttpServletRequest request) {
	
		String resultStatus = "Yes";
		
		int policyId = Integer.parseInt(request.getParameter("policyId"));
		List<LenderPolicyBean> policyInfo = borrowerService.getPolicyInformation(policyId);
		
		
		int borrowerId = (int) session.getAttribute("borrowerId");
		BorrowerDetailsBean borrowerBean = borrowerService.getBorrowerDetails(borrowerId);
		
		if(!policyInfo.get(0).getPolicy_for_gender().equals("Both"))
		{
			if(policyInfo.get(0).getPolicy_for_gender().equals(borrowerBean.getBorrower_gender()))
			{
				resultStatus = "Yes";
			}
			else
			{
				resultStatus = "No";				
			}
		}
		
		
		return resultStatus;
	}
	
	
	@RequestMapping(value = "/applyPolicyForm", method = RequestMethod.POST)
	@ResponseBody public ModelAndView applyPolicyForm(@ModelAttribute("dealModel") LenderToBorrowerFinanceDealModel dealModel,HttpSession session,HttpServletRequest request) {
		
		int policyId = Integer.parseInt(request.getParameter("policyId"));
		
		session.setAttribute("policyId", policyId);
		
		Map<String, Object> model = new HashMap<String, Object>();
		
		List<LenderPolicyBean> policyInfo = borrowerService.getPolicyInformation(policyId);
		
		model.put("policyInfo",policyInfo);
		
		int borrowerId = (int) session.getAttribute("borrowerId");
		
		BorrowerDetailsBean borrowerBean = borrowerService.getBorrowerDetails(borrowerId);
				
		model.put("borrowerBean",borrowerBean);
		
		request.setAttribute("policyComponent",borrowerService.getPolicyComponent(borrowerService.getPolicyInformation(policyId).get(0).getPolicy_type()));
		
		request.setAttribute("requiredDocument",borrowerService.checkDocumentExistence(borrowerId,policyInfo.get(0).getPolicy_required_doc()));
		
		return new ModelAndView("borrower_finance_apply_form",model);	
	}
	
	
	@RequestMapping(value = "/savePolicyForm", method = RequestMethod.POST)
	@ResponseBody public String savePolicyForm(@ModelAttribute("dealModel") LenderToBorrowerFinanceDealModel dealModel,HttpSession session,MultipartHttpServletRequest request) {
		
		String saveStatus = "";
		
		int borrowerId = (int) session.getAttribute("borrowerId");
		
		String requiredDoc = request.getParameter("requiredDoc");
		String docListArray[] = requiredDoc.split("/");
		
		
		
        BorrowerIdentityModel borrowerIdnDetails = new BorrowerIdentityModel();
       
        borrowerIdnDetails.setBorrower_id(borrowerId);
        borrowerIdnDetails.setBorrower_dob(request.getParameter("dobId"));
        borrowerIdnDetails.setBorrower_identity_id_type(request.getParameter("idTypeId"));
        borrowerIdnDetails.setBorrower_identity_card_id(request.getParameter("idNoId"));
        borrowerIdnDetails.setBorrower_alt_phno(request.getParameter("altMobileNo"));
        borrowerIdnDetails.setBorrower_alt_emailid(request.getParameter("altEmailId"));
        
        borrowerIdnDetails.setBorrower_present_door(request.getParameter("borrower_present_door"));
        borrowerIdnDetails.setBorrower_present_building(request.getParameter("borrower_present_building"));
        borrowerIdnDetails.setBorrower_present_street(request.getParameter("borrower_present_street"));
        borrowerIdnDetails.setBorrower_present_area(request.getParameter("borrower_present_area"));
        borrowerIdnDetails.setBorrower_present_city(request.getParameter("borrower_present_city"));
        borrowerIdnDetails.setBorrower_present_pincode(request.getParameter("borrower_present_pincode"));
        
        borrowerIdnDetails.setBorrower_permanent_door(request.getParameter("borrower_permanent_door"));
        borrowerIdnDetails.setBorrower_permanent_building(request.getParameter("borrower_permanent_building"));
        borrowerIdnDetails.setBorrower_permanent_street(request.getParameter("borrower_permanent_street"));
        borrowerIdnDetails.setBorrower_permanent_area(request.getParameter("borrower_permanent_area"));
        borrowerIdnDetails.setBorrower_permanent_city(request.getParameter("borrower_permanent_city"));
        borrowerIdnDetails.setBorrower_permanent_pincode(request.getParameter("borrower_permanent_pincode"));
        
        
        borrowerService.getUpdateBorrowerDetails(borrowerIdnDetails);
        
        dealModel.setExtra_info("No Visibility");
        
        saveStatus = borrowerService.savePolicyDealBetweenBorLen(dealModel);
        
		return saveStatus;	
		
	}
	
	
	
//////////////////////////////   SECTION FOR APPLIED POLICY   //////////////////////////////////////////////
	
	
	
	@RequestMapping(value = "/myLoanStatus", method = RequestMethod.GET)
	public ModelAndView myLoanStatus(HttpSession session,HttpServletRequest request) {
				
		if (session.getAttribute("borrowerId") == null)
		{									
        	return new ModelAndView("redirect:/borrowerLogin");
        }
		else
		{
			int borrowerId = (int) session.getAttribute("borrowerId");
			
			BorrowerDetailsBean borrowerBean = borrowerService.getBorrowerDetails(borrowerId);
			
			Map<String, Object> model = new HashMap<String, Object>();				

			model.put("borrowerBean",borrowerBean);
			
			model.put("policyDealBean",borrowerService.getBorrowerPolicyDealDetails(borrowerId));
			
			model.put("unreadNotification",lenderService.getUnreadNotification(borrowerId,"Borrower"));
			
			return new ModelAndView("borrower_taken_loans",model);				
		}
	} 
	
	
	
	@RequestMapping(value = "/viewAppliedPolicy", method = RequestMethod.POST)
	@ResponseBody public ModelAndView viewAppliedPolicy(HttpSession session,HttpServletRequest request) {
		
		int dealId = Integer.parseInt(request.getParameter("dealId"));
		
		int borrowerId = (int) session.getAttribute("borrowerId");
		
		Map<String, Object> model = new HashMap<String, Object>();
			
		model.put("dealDetails",borrowerService.getBorrowerPolicyDealDetailsViaDealId(dealId));
		
		return new ModelAndView("borrower_applied_policy_details",model);	
	}
	
	
	
	
	
/////////////////////      END OF  SECTION FOR APPLIED POLICY   //////////////////////////////////////////////
	
	
	
	
	
	
	
///////////////////////        SECTION FOR NOTIFICATION   //////////////////////////////////////////////
	
	
	
	@RequestMapping(value = "/notifications", method = RequestMethod.GET)
	public ModelAndView notifications(HttpSession session,HttpServletRequest request) {
				
		if (session.getAttribute("borrowerId") == null)
		{									
        	return new ModelAndView("redirect:/borrowerLogin");
        }
		else
		{
			int borrowerId = (int) session.getAttribute("borrowerId");
			
			BorrowerDetailsBean borrowerBean = borrowerService.getBorrowerDetails(borrowerId);
			
			Map<String, Object> model = new HashMap<String, Object>();				

			model.put("borrowerBean",borrowerBean);
			
			int userId = borrowerId;
			
			model.put("lenderNotification",borrowerService.getNotification("Borrower",userId));
			
			model.put("unreadNotification",lenderService.getUnreadNotification(borrowerId,"Borrower"));
			
			return new ModelAndView("borrower_notification",model);		
		}				
	}
	
	
	
	@RequestMapping(value = "/showBorrowerNotification", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showBorrowerNotification(HttpSession session,HttpServletRequest request) {
		
		int notifyId = Integer.parseInt(request.getParameter("notifyId"));
		
		int borrowerId = (int) session.getAttribute("borrowerId");
		
		Map<String, Object> model = new HashMap<String, Object>();
			
		model.put("notifyDetails",borrowerService.getNotifyDetails("Borrower",notifyId));
		
		return new ModelAndView("borrower_notify_details",model);	
	}
	
	
	
	
////////////////////////         END OF SECTION FOR NOTIFICATION   //////////////////////////////////////////////
	
	
	
	
///////////////////////         START OF SECTION FOR SETTING   //////////////////////////////////////////////
	
	
	@RequestMapping(value = "/settings", method = RequestMethod.GET)
	public ModelAndView settings(@ModelAttribute("borrowerIdnDetails") BorrowerIdentityModel borrowerIdnDetails,HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("borrowerId") == null)
		{									
        	return new ModelAndView("redirect:/borrowerLogin");
        }
		else
		{
			int borrowerId = (int) session.getAttribute("borrowerId");
			
			BorrowerDetailsBean borrowerBean = borrowerService.getBorrowerDetails(borrowerId);
			
			Map<String, Object> model = new HashMap<String, Object>();				

			model.put("borrowerBean",borrowerBean);
			
			model.put("unreadNotification",lenderService.getUnreadNotification(borrowerId,"Borrower"));
			
			return new ModelAndView("borrower_settings",model);					
		}							
	}
	
	
	
	@RequestMapping(value = "/borCngpassword", method = RequestMethod.GET)
	public ModelAndView borCngpassword(@ModelAttribute("borrowerIdnDetails") BorrowerIdentityModel borrowerIdnDetails,HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("borrowerId") == null)
		{									
        	return new ModelAndView("redirect:/borrowerLogin");
        }
		else
		{
			int borrowerId = (int) session.getAttribute("borrowerId");
			
			BorrowerDetailsBean borrowerBean = borrowerService.getBorrowerDetails(borrowerId);
			
			Map<String, Object> model = new HashMap<String, Object>();				

			model.put("borrowerBean",borrowerBean);
			
			model.put("unreadNotification",lenderService.getUnreadNotification(borrowerId,"Borrower"));
			
			return new ModelAndView("borrower_change_password",model);					
		}							
	}
	
	@RequestMapping(value = "/borrowerDoc", method = RequestMethod.GET)
	public ModelAndView borrowerDoc(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("borrowerId") == null)
		{									
        	return new ModelAndView("redirect:/borrowerLogin");
        }
		else
		{
			int borrowerId = (int) session.getAttribute("borrowerId");
			
			BorrowerDetailsBean borrowerBean = borrowerService.getBorrowerDetails(borrowerId);
			
			Map<String, Object> model = new HashMap<String, Object>();				

			model.put("borrowerBean",borrowerBean);
			
			request.setAttribute("docList",borrowerService.getBorrowerDocList());
			
			model.put("attachedDocument",borrowerService.attachedDocuments(borrowerId));
			
			model.put("unreadNotification",lenderService.getUnreadNotification(borrowerId,"Borrower"));
			
			return new ModelAndView("borrower_document_form",model);					
		}							
	}
	
	
	
	@RequestMapping(value = "/saveBorrowerDoc", method = RequestMethod.POST)
	@ResponseBody public String saveBorrowerDoc(HttpSession session,MultipartHttpServletRequest request) {
	
		int borrowerId = (int) session.getAttribute("borrowerId");
		
		String fileName = "";
		String status = "";
		String doc_type = request.getParameter("docType");
		
 	    BorrowerDocumentModel borrowerDocs = new BorrowerDocumentModel();
 	   
 	    // Last record of Borrower if exist
		borrowerDocs.setBorrower_id(borrowerId);
		//List<BorrowerDocumentModel> lastBorrowerDocRecord = borrowerService.getLastBorrowerDocRecord(borrowerId);
		
		// File Storage location
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories"); 	    
 	    String filePath = fileResource.getString("BorrowerFile");	    		
 	    File ourPath = new File(filePath);	    
 	    String storePath = ourPath.getPath()+"/"+borrowerId+"/AttachedDocuments/";	 	    
 	    
 	    //Document nameing Convention
 	    String reName = "";
 	    int lastSequesnce = 0;
 	    
 	    lastSequesnce = borrowerService.getLastDocSequence(borrowerId,doc_type);
 	    lastSequesnce = lastSequesnce + 1;
 	    
 	    reName = borrowerId+"_"+doc_type.replaceAll(" ","_")+"_"+lastSequesnce;
 	    
 	    MultipartFile multipartFile = request.getFile("file");
	    
        fileName = multipartFile.getOriginalFilename();
       
        String extension = FilenameUtils.getExtension(fileName); // returns "extension"
		
	    File fld = new File(storePath);
	    	
	        if(!fld.exists())
	    	{
			    fld.mkdirs();
	    	}
	        try 
	        {
	        	 multipartFile.transferTo(new File(storePath+fileName));
	        	
	        	 File f = new File(storePath+fileName); 

	             f.renameTo(new File(storePath+reName+"."+extension));
	                
			} 
	        catch (Exception e) 
	        {
				// TODO Auto-generated catch block
				e.printStackTrace();					                    
            }		        
	        // End of saving File	
		
	        borrowerDocs.setBorrower_id(borrowerId);
	        borrowerDocs.setDocument_type(doc_type);
	        borrowerDocs.setFile_name(reName+"."+extension);
	        borrowerDocs.setDocument_status("Active");
	        borrowerDocs.setDocument_count(lastSequesnce);
	        borrowerDocs.setCr_date(new Date());
       
	        status = borrowerService.saveDocumentDetails(borrowerDocs);
	        
	        return status;
	}
	
	
	
	
	@RequestMapping(value = "/viewScannedDoc", method = RequestMethod.POST)
	@ResponseBody public ModelAndView viewScannedDoc(HttpSession session,HttpServletRequest request) {
		
		int documentId = Integer.parseInt(request.getParameter("documentId"));
		
		Map<String, Object> model = new HashMap<String, Object>();		
		
		model.put("docDetails",borrowerService.getDocumentDetail(documentId));
		
		return new ModelAndView("borrower_view_document",model);
	}
	
	@RequestMapping(value = "/viewDocFormPopUp", method = RequestMethod.POST)
	@ResponseBody public ModelAndView viewDocFormPopUp(HttpSession session,HttpServletRequest request) {
		
		request.setAttribute("docType", request.getParameter("docType"));
		
		return new ModelAndView("borrower_doc_form_popup");
	}
	
	@RequestMapping(value = "/refreshAvailableDocForm", method = RequestMethod.POST)
	@ResponseBody public ModelAndView refreshAvailableDocForm(HttpSession session,HttpServletRequest request) {
		
		int borrowerId = (int) session.getAttribute("borrowerId");
		
		int policyId = (int) session.getAttribute("policyId");
		
		List<LenderPolicyBean> policyInfo = borrowerService.getPolicyInformation(policyId);
		
		request.setAttribute("requiredDocument",borrowerService.checkDocumentExistence(borrowerId,policyInfo.get(0).getPolicy_required_doc()));
		
		return new ModelAndView("borrower_policy_avail_doc");
	}
	
	@RequestMapping(value = "/viewAddedDoc", method = RequestMethod.POST)
	@ResponseBody public ModelAndView viewAddedDoc(HttpSession session,HttpServletRequest request) {
		
		int borrowerId = (int) session.getAttribute("borrowerId");
		
		String docType = request.getParameter("docType");
		
		Map<String, Object> model = new HashMap<String, Object>();		
		
		int documentId = borrowerService.getDocumentIdViaDocType(borrowerId,docType);
		
		model.put("docDetails",borrowerService.getDocumentDetail(documentId));
		
		return new ModelAndView("borrower_view_document",model);
	}
	
	
	
	
	
	
	
	@RequestMapping(value = "/checkBorrowerEmailId", method = RequestMethod.POST)
	@ResponseBody public String checkRegisterEmailId(@ModelAttribute("borrowerRegDetails") BorrowerRegistrationDetails borrowerRegDetails,HttpSession session) {
		
		String emailId = borrowerRegDetails.getBorrower_emailid();
		
		String status = borrowerService.checkRegisterEmailId(emailId,"Borrower");
		
		return status;
	}
	
	
	@RequestMapping(value = "/checkBorrowerPhno", method = RequestMethod.POST)
	@ResponseBody public String checkBorrowerPhno(@ModelAttribute("borrowerRegDetails") BorrowerRegistrationDetails borrowerRegDetails,HttpSession session) {
		
		String phno = borrowerRegDetails.getBorrower_phno();
		
		String status = borrowerService.checkBorrowerPhno(phno,"Borrower");
		
		return status;
	}
	
	@RequestMapping(value = "/checkBorrowerPanId", method = RequestMethod.POST)
	@ResponseBody public String checkBorrowerPanId(@ModelAttribute("borrowerRegDetails") BorrowerRegistrationDetails borrowerRegDetails,HttpSession session) {
		
		String panId = borrowerRegDetails.getBorrower_pan_id();
		
		String status = borrowerService.checkBorrowerPanId(panId,"Borrower");
		
		return status;
	}
	
	
	
	@RequestMapping(value = "/saveBorrowerRegister", method = RequestMethod.POST)
	@ResponseBody public String saveBorrowerRegister(@ModelAttribute("borrowerRegDetails") BorrowerRegistrationDetails borrowerRegDetails,HttpSession session) {
		
		
		String status = borrowerService.saveBorrowerRegdetails(borrowerRegDetails);
		
		return status;
	}
	
	@RequestMapping(value = "/activateAccountViaOTP", method = RequestMethod.POST)
	@ResponseBody public String activateAccountViaOTP(HttpServletRequest request,HttpSession session) {
		
		String status = "";
		
		String otpNumber = request.getParameter("otpNumber");
		
		status = borrowerService.activateAccountViaOTP(otpNumber);
		
		return status;
	}

	
	@RequestMapping(value = "/generateOTPForuser", method = RequestMethod.POST)
	@ResponseBody public String generateOTPForuser(HttpServletRequest request,HttpSession session) {
		
		String status = "";
		
		String userId = request.getParameter("userId");
		
		status = borrowerService.generateOTPForuser(userId);
		
		return status;
	}
	
	@RequestMapping(value = "/activeAccount", method = RequestMethod.GET)
	public String activeAccount(@ModelAttribute("borrowerRegDetails") BorrowerRegistrationDetails borrowerRegDetails,HttpSession session,HttpServletRequest request) {
	
		String activeAccountId = request.getParameter("account");
		
		String emailId = borrowerService.getEmailIdViaUnqid(activeAccountId);
		
		if(!emailId.equals("Not Exist"))
		{
			String accountStatus = borrowerService.getActiveAcount("Borrower",emailId,activeAccountId);
			
			request.setAttribute("actionMessage", "Not Exist");
        	return "index_borrower_signin";       	
		}
		else
		{
			request.setAttribute("actionMessage", "Exist");
        	return "index_borrower_signin";
		}		
	}
	
	@RequestMapping(value = "/authBorrowerCredential", method = RequestMethod.GET)
	public String getValidateAdminWithGet(HttpSession session) {
		
		if (session.getAttribute("borrowerId") == null)
		{									
        	return "redirect:/borrowerLogin";
        }
		else
		{
			//return "redirect:/borrowerHome";		
			return "redirect:/myProfile";
		}		
	}
	
	@RequestMapping(value = "/authBorrowerCredential", method = RequestMethod.POST)
	public String authBorrowerCredential(@ModelAttribute("borrowerRegDetails") BorrowerRegistrationDetails borrowerRegDetails,HttpSession session,HttpServletRequest request) {
		
		String userId = borrowerRegDetails.getBorrower_emailid();
        String password = borrowerRegDetails.getBorrower_password();
       
        String authenticateStatus = borrowerService.getValidateBorrower(userId,password);
        if(authenticateStatus.equals("Active"))
        {	
        	int borrowerId = borrowerService.getBorrowerIdVieEmailId(userId);
        	session.setAttribute("borrowerId", borrowerId);
        	session.setAttribute("USER_SESSION_ID", "JIR"+session.getId()+"K");
        	
        	//return "redirect:/borrowerHome";	
        	
        	return "redirect:/myProfile";
        }
        else if(authenticateStatus.equals("Deactive"))
        {
        	request.setAttribute("actionMessage", "deactive");
        	return "index_borrower_signin";
        }	
        else   
        {
        	request.setAttribute("actionMessage", "failed");
        	return "index_borrower_signin";
        }
	}
	
	@RequestMapping(value = "/borrowerForgetPassword", method = RequestMethod.POST)
	public @ResponseBody String borrowerForgetPassword(HttpSession session,HttpServletRequest request) {
		
		String status = "";
		
		String emailId = (String)request.getParameter("borrower_emailid");
		
		status = borrowerService.sendLinkForgetPassword(emailId);
		
		return status;
	}
	
	@RequestMapping(value = "/resetPassword", method = RequestMethod.GET)
	public String resetPassword(HttpSession session,HttpServletRequest request) {
	
		String uniqueId = request.getParameter("requnIkedij");
		
		String requestedEmailID = borrowerService.getEmailIdFromForgetPassword(uniqueId);
		
		if(requestedEmailID.equals("Not Exist"))
		{
			request.setAttribute("requestMesage", "Given Link is expired please try again.");
			
			return "index_borrower_forget_password";
		}
		else
		{
			request.setAttribute("requestedEmailID", requestedEmailID);
			request.setAttribute("uniqueId", uniqueId);	
			
			return "index_borrower_reset_password";
		}
		
	}
	
	
	@RequestMapping(value = "/saveBorrowerResetPassword", method = RequestMethod.POST)
	public @ResponseBody String saveBorrowerResetPassword(HttpSession session,HttpServletRequest request) {
	
		String status = "";
		
		String emailId = request.getParameter("requestedEmailID");
		String password = request.getParameter("borrower_password");
		
		status = borrowerService.getChangePassword(emailId,password);
		
		return status;
	}
		
	@RequestMapping(value = "/myProfile", method = RequestMethod.GET)
	public ModelAndView myProfile(HttpSession session,HttpServletRequest request) {
			
		if (session.getAttribute("borrowerId") == null)
		{										
        	return new ModelAndView("redirect:/borrowerLogin");
        }
		else 
		{	
			int borrowerId = (int) session.getAttribute("borrowerId");
			
			BorrowerDetailsBean borrowerBean = borrowerService.getBorrowerDetails(borrowerId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("borrowerBean",borrowerBean);
			
			model.put("dealBean",borrowerService.getLastDealDetails(borrowerId));
			
			model.put("searchedBean",borrowerService.getLastSearchDeals(borrowerId));
			
			model.put("attachedDocument",borrowerService.attachedDocuments(borrowerId));
			
			model.put("unreadNotification",lenderService.getUnreadNotification(borrowerId,"Borrower"));
			
			return new ModelAndView("borrower_profile",model);				
		}
	}   
	
	@RequestMapping(value = "/updateBorrowerIdentityDetails", method = RequestMethod.POST)
	public @ResponseBody String updateBorrowerIdentityDetails(BorrowerIdentityModel borrowerIdnDetails,HttpSession session,MultipartHttpServletRequest request) {
	
		int borrowerId = (int) session.getAttribute("borrowerId");
		
		String fileName = "";
 	    
        MultipartFile multipartFile = request.getFile("file");
        fileName = multipartFile.getOriginalFilename();
        
		String status = "";
		
		borrowerIdnDetails.setBorrower_id(borrowerId);
		if(fileName.equals(""))
		{
			borrowerIdnDetails.setBorrower_profile_photo(borrowerService.getProfilePhoto(borrowerId));
		}
		else
		{
			borrowerIdnDetails.setBorrower_profile_photo(fileName);
		}
		
		borrowerIdnDetails.setCr_date(new Date());
		
		status = borrowerService.updateBorrowerIdentityDetails(borrowerIdnDetails);
		
		if(status.equals("success"))
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
	 	    
	 	    String filePath = fileResource.getString("BorrowerFile");
	 	    		
	 	    File ourPath = new File(filePath);
	 	    
	 	    String storePath = ourPath.getPath()+"/"+borrowerId+"/ProfilePhoto/";	 	    
	 	    
	        if(!fileName.equals(""))
			{	    				
		        File fld = new File(storePath);
		    	
		        if(!fld.exists())
		    	{
				    fld.mkdirs();
		    	}
		        try 
		        {
	            	multipartFile.transferTo(new File(storePath+fileName));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();					                    
	            }		        
	 	        // End of saving File
			}
		}
		
		return status;
	}
	
	
	@RequestMapping(value = "/changeBorrowerPassword", method = RequestMethod.POST)
	public @ResponseBody String changeBorrowerPassword(HttpSession session,HttpServletRequest request) {
	
		int borrowerId = (int) session.getAttribute("borrowerId");
		
		String status = "";			
		String password = request.getParameter("password");
		
		status = borrowerService.getChangePassword(borrowerId,password);
		
		return status;
	}
	
	
	
	
	
	 // Preview BORROWER Profile Photo
	
	
		@SuppressWarnings({ "resource", "unused" })
		@RequestMapping(value = "/borrowerPhoto", method = RequestMethod.GET)
		@ResponseBody public byte[] previewPicture(HttpServletRequest request,HttpSession session) throws IOException {
			
			
			String fileName = request.getParameter("fileName");
			String gender = request.getParameter("gender");
			
			int borrowerId = (int) session.getAttribute("borrowerId");
			
			// Required file Config for entire Controller 
			 
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
			
			String fileDir = fileResource.getString("BorrowerFile");
			
			fileDir = fileDir+"/"+borrowerId+"/ProfilePhoto";
			 
			// End of Required file Config for entire Controller 
			 
			ServletContext servletContext = request.getSession().getServletContext();
			
			File ourPath = new File(fileDir);
			
			StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/"+fileName);
			String docCategory = fPath.toString();
			
			System.out.println("File Directory-"+fPath);
			
			if(fileName.equals(""))
			{
				if(gender.equals("Male"))
				{
					fPath.delete(0, fPath.length());				
					File targetFile = new File(request.getServletContext().getRealPath("/Back_End_Files/male.png"));					
					fPath.append(targetFile.getPath());		
				}
				else
				{
					fPath.delete(0, fPath.length());				
					File targetFile = new File(request.getServletContext().getRealPath("/Back_End_Files/female.png"));					
					fPath.append(targetFile.getPath());		
				}							
			}
			else
			{
				if(StringUtils.isNotBlank(docCategory))
				{				
					/* Checks that files are exist or not  */
					
						File exeFile=new File(fPath.toString());
						if(!exeFile.exists())
						{
							if(gender.equals("Male"))
							{
								fPath.delete(0, fPath.length());				
								File targetFile = new File(request.getServletContext().getRealPath("/Back_End_Files/male.png"));					
								fPath.append(targetFile.getPath());		
							}
							else
							{
								fPath.delete(0, fPath.length());				
								File targetFile = new File(request.getServletContext().getRealPath("/Back_End_Files/female.png"));					
								fPath.append(targetFile.getPath());		
							}	
						}	
						
					/* Checks that files are exist or not  */
						
				}
			}
		
			InputStream in;
			
			try {					
					FileInputStream docdir = new FileInputStream(fPath.toString());													
					if (docdir != null) 
					{						
						return IOUtils.toByteArray(docdir);					
					} 
					else 
					{							
						return null;
					}			
			} 
			catch (FileNotFoundException e) {
				
				System.out.println(e.getMessage());
				return null; // todo: return safe photo instead
			} catch (IOException e) {
				System.out.println(e.getMessage());
				return null; // todo: return safe photo instead
			}

		}
		
		
		
		// Preview BORROWER Profile Photo
		
		
			@SuppressWarnings({ "resource", "unused" })
			@RequestMapping(value = "/attachedDocuments", method = RequestMethod.GET)
			@ResponseBody public byte[] attachedDocuments(HttpServletRequest request,HttpSession session) throws IOException {
				
				
				String fileName = request.getParameter("fileName");
				
				int borrowerId = (int) session.getAttribute("borrowerId");
				
				// Required file Config for entire Controller 
				 
				ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
				
				String fileDir = fileResource.getString("BorrowerFile");
				
				fileDir = fileDir+"/"+borrowerId+"/AttachedDocuments";
				 
				// End of Required file Config for entire Controller 
				 
				ServletContext servletContext = request.getSession().getServletContext();
				
				File ourPath = new File(fileDir);
				
				StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/"+fileName);
				String docCategory = fPath.toString();
				
				InputStream in;
				
				try {					
						FileInputStream docdir = new FileInputStream(fPath.toString());													
						if (docdir != null) 
						{						
							return IOUtils.toByteArray(docdir);					
						} 
						else 
						{							
							return null;
						}			
				} 
				catch (FileNotFoundException e) {
					
					System.out.println(e.getMessage());
					return null; // todo: return safe photo instead
				} catch (IOException e) {
					System.out.println(e.getMessage());
					return null; // todo: return safe photo instead
				}

			}
		
	
	
	
}
