package com.finance.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.finance.bean.LenderPolicyBean;
import com.finance.domain.BorrowerRegistrationDetails;
import com.finance.domain.ContactUsModel;
import com.finance.domain.ControllerDetails;
import com.finance.domain.LenderRegistrationDetails;
import com.finance.domain.SearchLoanModel;
import com.finance.service.AdminService;
import com.finance.service.BorrowerService;


@Controller
public class IndexController {

	@Autowired
	private AdminService adminService;
	
	@Autowired
	BorrowerService borrowerService;

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String showHome(HttpSession session,HttpServletRequest request) {
						
		return "index";				
	}
	
	@RequestMapping(value = "/aboutUs", method = RequestMethod.GET)
	public String aboutUs(HttpServletRequest request) {
						
		return "index_aboutUs";				
	}
	
	@RequestMapping(value = "/service", method = RequestMethod.GET)
	public String service(HttpServletRequest request) {
						
		return "index_service";			
	}
	
	@RequestMapping(value = "/searchLoan", method = RequestMethod.GET)
	public String searchLoan(@ModelAttribute("searchLoanModel") SearchLoanModel searchLoanModel,HttpServletRequest request,HttpSession session) {
			
		if (session.getAttribute("borrowerId") == null)
		{									
			request.setAttribute("nameList", adminService.getPolicyTypeName());
			
			return "index_search_loan";	
        }
		else
		{			
			return "redirect:/loans";
		}
					
	}
	
	
	@RequestMapping(value = "/resultSearchLoan", method = RequestMethod.POST)
	@ResponseBody public ModelAndView resultSearchLoan(@ModelAttribute("searchLoanModel") SearchLoanModel searchLoanModel,HttpSession session,HttpServletRequest request) {
		
		String saveStatus = "";
		
		searchLoanModel.setSearch_date(new Date());
		
		saveStatus = adminService.saveSearchLoanDetails(searchLoanModel);
		
		Map<String, Object> model = new HashMap<String, Object>();
		
		if(saveStatus.equals("success"))
		{
			String type = searchLoanModel.getLoan_type();
			
			String amount = searchLoanModel.getLoan_amount();
			
			String location = "All over India";
			
			amount = amount.replaceAll(",","");
			
			String time = searchLoanModel.getLoan_tenure();
				
			List<LenderPolicyBean> policyResult = borrowerService.getSearchPolicyList(type,amount,time,location);
			
			request.setAttribute("resultSize",policyResult.size());
			
			model.put("policyResult",policyResult);			
		}
		else
		{
			request.setAttribute("resultSize",0);
		}
		
		return new ModelAndView("index_result_loan",model);
	}
	
	@RequestMapping(value = "/contactUs", method = RequestMethod.GET)
	public String contactUs(@ModelAttribute("contactDetail") ContactUsModel contactDetail,HttpServletRequest request) {
						
		return "index_contactUs";				
	}
	
	
	@RequestMapping(value = "/saveContactForm", method = RequestMethod.POST)
	@ResponseBody public String saveContactForm(@ModelAttribute("contactDetail") ContactUsModel contactDetail,HttpSession session,HttpServletRequest request) {
		
		String saveStatus = "";
		
		saveStatus = adminService.saveConatactFormDetails(contactDetail);
		
		return saveStatus;
	}
	
	@RequestMapping(value = "/borrowerLogin", method = RequestMethod.GET)
	public String borrowerLogin(@ModelAttribute("borrowerRegDetails") BorrowerRegistrationDetails borrowerRegDetails,HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("borrowerId") == null)
		{			
			request.setAttribute("actionMessage", "noAction");
			
			return "index_borrower_signin";	
        }
		else
		{
			return "redirect:/myProfile";				
		}					
	}
	
	@RequestMapping(value = "/borrowerReg", method = RequestMethod.GET)
	public String borrowerReg(@ModelAttribute("borrowerRegDetails") BorrowerRegistrationDetails borrowerRegDetails,HttpServletRequest request) {
					
		request.setAttribute("actionMessage", "noAction");
		
		return "index_borrower_registration";				
	}
	
	@RequestMapping(value = "/borrowerForgetPwd", method = RequestMethod.GET)
	public String borrowerForgetPwd(HttpServletRequest request) {
						
		return "index_borrower_forget_password";				
	}
	
	
	@RequestMapping(value = "/lenderLogin", method = RequestMethod.GET)
	public String lenderLogin(@ModelAttribute("lenderRegDetails") LenderRegistrationDetails lenderRegDetails,HttpSession session,HttpServletRequest request) {
				
		if (session.getAttribute("lenderId") == null)
		{
			request.setAttribute("actionMessage", "noAction");
			
			return "index_lender_login";				
		}
		else
		{
			return "redirect:/lenProfile";	
		}		
	}
	
	@RequestMapping(value = "/lenderReg", method = RequestMethod.GET)
	public String lenderReg(@ModelAttribute("lenderRegDetails") LenderRegistrationDetails lenderRegDetails,HttpServletRequest request) {
						
		return "index_lender_registration";				
	}
		
	@RequestMapping(value = "/lenderForgetPwd", method = RequestMethod.GET)
	public String lenderForgetPwd(HttpServletRequest request) {
						
		return "index_lender_forget_password";				
	}
	
	
	@RequestMapping(value = "/borrowerProfile", method = RequestMethod.GET)
	public String borrowerProfile(HttpServletRequest request) {
						
		return "borrower_profile";				
	}
	
	@RequestMapping(value = "/popup", method = RequestMethod.GET)
	public String popup(HttpServletRequest request) {
						
		return "success_message";				
	}
	
	
	@RequestMapping(value = "/termsConditions", method = RequestMethod.GET)
	public String termsConditions(HttpServletRequest request) {
						
		return "index_terms_conditions";				
	}
	
	
	@RequestMapping(value = "/privacyPolicy", method = RequestMethod.GET)
	public String privacyPolicy(HttpServletRequest request) {
						
		return "index_privacy_policy";				
	}
	
	@RequestMapping(value = "/whyLender", method = RequestMethod.GET)
	public String whyLender(HttpServletRequest request) {
					
		request.setAttribute("div", request.getParameter("div"));
		
		return "index_why_lender";				
	}
	
	
	@RequestMapping(value = "/whyBorrower", method = RequestMethod.GET)
	public String whyBorrower(HttpServletRequest request) {
				
		request.setAttribute("div", request.getParameter("div"));
		
		return "index_why_borrower";				
	}
	
}
