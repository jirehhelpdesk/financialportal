package com.finance.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.finance.bean.BorrowerDetailsBean;
import com.finance.bean.ControllerDetailsBean;
import com.finance.bean.LenderDetailsBean;
import com.finance.bean.LenderToBorrowerFinanceDealBean;
import com.finance.domain.ControllerDetails;
import com.finance.domain.InterPortalNotificationModel;
import com.finance.domain.LenderRegistrationDetails;
import com.finance.service.AdminService;
import com.finance.service.BorrowerService;
import com.finance.service.ControllerService;
import com.finance.service.LenderService;
import com.finance.util.ReportinExcel;

@Controller
public class ControlController {
		
	
	@Autowired
	private ControllerService controllerService;
		
	@Autowired
	private AdminService adminService;
	
	@Autowired
	BorrowerService borrowerService;
	
	@Autowired
	LenderService lenderService;
	
	
	@RequestMapping(value = "/controllerLogin", method = RequestMethod.GET)
	public String controllerLogin(@ModelAttribute("controllerDetails") ControllerDetails controllerDetails,HttpServletRequest request) {
						
		return "controller_login";				
	}
			
	@RequestMapping(value = "/cntrlLogout", method = RequestMethod.GET)
	public String cntrlLogout(HttpSession session,HttpServletRequest request) {
					
		session.removeAttribute("controllerId");			 
		request.setAttribute("actionMessage", "success");
        
		return "redirect:/controllerLogin";				
	}
			
	@RequestMapping(value = "/controllerHome", method = RequestMethod.GET)
	public ModelAndView controllerHome(HttpSession session,HttpServletRequest request) {
					
		if (session.getAttribute("controllerId") == null)
		{									
        	return new ModelAndView("redirect:/controllerLogin");
        }
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			String controllerEmailId = (String)session.getAttribute("controllerId");
			List<ControllerDetailsBean> controllerData = controllerService.getControllerDetails(controllerEmailId);
			
			model.put("controllerData",controllerData);
		
			model.put("portalStatus",adminService.getUpdateOnPortalStatus());
			
			return new ModelAndView("controller_index",model);	
		}					
	}
	
	
//////////////////////////////  START MANAGE BORROWER PANEL      //////////////////////////////////////////////////////
	
	
	
	
	@RequestMapping(value = "/notifyBorrower", method = RequestMethod.GET)
	public ModelAndView notifyBorrower(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("controllerId") == null)
		{									
        	return new ModelAndView("redirect:/controllerLogin");
        }
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			String controllerEmailId = (String)session.getAttribute("controllerId");
			List<ControllerDetailsBean> controllerData = controllerService.getControllerDetails(controllerEmailId);
			
			model.put("controllerData",controllerData);
		
			return new ModelAndView("controller_notify_borrower",model);	
		}			
	}
		
	
/////////////////////////////   BORROWER MAINTENANCE   ///////////////////////////////////
	
	@RequestMapping(value = "/borrowerManage", method = RequestMethod.GET)
	public ModelAndView borrowerManage(HttpSession session,HttpServletRequest request) {
						
		if (session.getAttribute("controllerId") == null)
		{									
        	return new ModelAndView("redirect:/controllerLogin");
        }
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();				

			model.put("borrowerDetails",controllerService.getLastBorrowerDetails());
		
			
			return new ModelAndView("controller_borrower_maintence",model);
		}						
	}
	
	
	@RequestMapping(value = "/searchBorrowerByController", method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchBorrowerByController(HttpSession session,HttpServletRequest request) {
		
		String searchType = request.getParameter("searchType");		
		String searchValue = request.getParameter("searchValue");
		
		
		Map<String, Object> model = new HashMap<String, Object>();
			
		model.put("borrowerDetails",controllerService.getSearchBorrower(searchType,searchValue));
		
		return new ModelAndView("controller_borrower_search_list",model);	
	}
	
	
	
	@RequestMapping(value = "/manageBorrowerStatus", method = RequestMethod.POST)
	@ResponseBody public String manageBorrowerStatus(HttpSession session,HttpServletRequest request) {
		
		int borrowerId = Integer.parseInt(request.getParameter("borrowerId"));		
		String condition = request.getParameter("condition");
		
		String status = controllerService.manageBorrowerStatus(borrowerId,condition);
		
		return status;	
	}
	
	
	@RequestMapping(value = "/showBoxToNotify", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showBoxToNotify(HttpSession session,HttpServletRequest request) {
		
		int userId = Integer.parseInt(request.getParameter("borrowerId"));
		
		String searchType = "BorrowerId";		
		String searchValue = userId+"";
		
		Map<String, Object> model = new HashMap<String, Object>();
			
		model.put("borrowerDetails",controllerService.getSearchBorrower(searchType,searchValue));
		
		return new ModelAndView("controller_compose_borrower",model);	
	}
	
	
	
	@RequestMapping(value = "/sendMessageToBorrower", method = RequestMethod.POST)
	@ResponseBody public String sendMessageToBorrower(HttpSession session,HttpServletRequest request) {
		
		String controllerEmailId = (String)session.getAttribute("controllerId");
		List<ControllerDetailsBean> controllerId = controllerService.getControllerDetails(controllerEmailId);
				
		InterPortalNotificationModel notifyModel = new InterPortalNotificationModel();
		
		notifyModel.setNotify_subject(request.getParameter("notifySubject"));
		notifyModel.setNotify_message(request.getParameter("notifyMessage"));
		notifyModel.setNotify_reciver(Integer.parseInt(request.getParameter("sender_id")));
		notifyModel.setNotify_reciver_type("Borrower");
		notifyModel.setNotify_sender(controllerId.get(0).getController_id());
		notifyModel.setNotify_sender_type("Controller");
		notifyModel.setNotify_regarding("Just Notification");
		notifyModel.setNotify_status("Pending");
		
		notifyModel.setNotify_date(new Date());
		
		String saveStatus = controllerService.getSaveNotifyDetails(notifyModel);
		
		return saveStatus;	
	}
	
	
	
	@RequestMapping(value = "/borrowerExportData", method = RequestMethod.GET)
	public String borrowerExportData(HttpSession session,HttpServletRequest request) {
		
		String parameter = request.getParameter("parameter");
		
		String searchType = request.getParameter("searchType");		
		String searchValue = request.getParameter("searchValue");
		
		
		ReportinExcel report = new ReportinExcel();
		
		String fileName = "";
		String reportDirectory = "";
		
		List<BorrowerDetailsBean> borrowerBean = new ArrayList<BorrowerDetailsBean>();
				
		if(parameter.equals("default"))
		{
			borrowerBean = controllerService.getLastBorrowerDetails();
		}
		else
		{
			borrowerBean = controllerService.getSearchBorrower(searchType,searchValue);
		}
		
		
		fileName = report.generateBorrowerReportInExcel(borrowerBean);
		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
	 	reportDirectory = fileResource.getString("BorrowerReport");		
		
	 	File fld = new File(reportDirectory);
    	if(!fld.exists())
    	{
		    fld.mkdirs();
    	}
		
		request.setAttribute("rootDirectory",reportDirectory);
		request.setAttribute("fileName",fileName);
		
		return "commonDownload";
	}
	
	
	
/////////////////////////////    END OF BORROWER MAINTENENCE   ////////////////////////////////////////	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
////////////////////////////////////  END OF MANAGE BORROWER PANEL      //////////////////////////////////////////////////////
	
	
	
	
	
	
	
	
	
	
	
	
	
/////////////////////////////////////////   START OF MANAGE LENDER   ////////////////////////////////////////////////	
		
	
	
	
	@RequestMapping(value = "/lenderRequest", method = RequestMethod.GET)
	public ModelAndView lenderRequest(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("controllerId") == null)
		{									
        	return new ModelAndView("redirect:/controllerLogin");
        }
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			String controllerEmailId = (String)session.getAttribute("controllerId");
			List<ControllerDetailsBean> controllerData = controllerService.getControllerDetails(controllerEmailId);
			
			model.put("controllerData",controllerData);
			
			model.put("lenderDetails",controllerService.getLastLenderDetails());
		
			return new ModelAndView("controller_lender_maintence",model);		
		}				
	}
	
	
	@RequestMapping(value = "/searchLenderByController", method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchLenderByController(HttpSession session,HttpServletRequest request) {
		
		String searchType = request.getParameter("searchType");		
		String searchValue = request.getParameter("searchValue");
		
		Map<String, Object> model = new HashMap<String, Object>();
			
		model.put("lenderDetails",controllerService.getSearchLender(searchType,searchValue));
		
		return new ModelAndView("controller_lender_search_list",model);	
	}
	
	
	
	
	@RequestMapping(value = "/searchLenderRequestPolicy", method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchLenderRequestPolicy(HttpSession session,HttpServletRequest request) {
		
		String searchType = request.getParameter("searchType");		
		String searchValue = request.getParameter("searchValue");
		
		Map<String, Object> model = new HashMap<String, Object>();
			
		//model.put("dealDetails",controllerService.getSearchDealDetails(searchType,searchValue));
		
		return new ModelAndView("controller_lender_policy_request",model);	
		
	}
	
	
	
	@RequestMapping(value = "/notifyLender", method = RequestMethod.GET)
	public ModelAndView notifyLender(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("controllerId") == null)
		{									
        	return new ModelAndView("redirect:/controllerLogin");
        }
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			String controllerEmailId = (String)session.getAttribute("controllerId");
			List<ControllerDetailsBean> controllerData = controllerService.getControllerDetails(controllerEmailId);
			
			model.put("controllerData",controllerData);
		
			return new ModelAndView("controller_notify_lender",model);	
		}						
	}
	
	
	
	
	@RequestMapping(value = "/showNotifyForLender", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showNotifyForLender(HttpSession session,HttpServletRequest request) {
		
		int lenderId = Integer.parseInt(request.getParameter("lenderId"));
		
		String searchType = "LenderId";		
		String searchValue = lenderId+"";
		
		Map<String, Object> model = new HashMap<String, Object>();
			
		model.put("lenderDetails",controllerService.getSearchLender(searchType, searchValue));
		
		return new ModelAndView("controller_compose_lender",model);	
	}
	
	
	
	@RequestMapping(value = "/sendMessageToLender", method = RequestMethod.POST)
	@ResponseBody public String sendMessageToLender(HttpSession session,HttpServletRequest request) {
		
		String controllerEmailId = (String)session.getAttribute("controllerId");
		List<ControllerDetailsBean> controllerId = controllerService.getControllerDetails(controllerEmailId);
				
		InterPortalNotificationModel notifyModel = new InterPortalNotificationModel();
		
		notifyModel.setNotify_subject(request.getParameter("notifySubject"));
		notifyModel.setNotify_message(request.getParameter("notifyMessage"));
		notifyModel.setNotify_reciver(Integer.parseInt(request.getParameter("sender_id")));
		notifyModel.setNotify_reciver_type("Lender");
		notifyModel.setNotify_sender(controllerId.get(0).getController_id());
		notifyModel.setNotify_sender_type("Controller");
		notifyModel.setNotify_regarding("Just Notification");
		notifyModel.setNotify_status("Pending");
		
		notifyModel.setNotify_date(new Date());
		
		String saveStatus = controllerService.getSaveNotifyDetails(notifyModel);
		
		return saveStatus;	
	}
	
	
	
	@RequestMapping(value = "/manageLenderStatus", method = RequestMethod.POST)
	@ResponseBody public String manageLenderStatus(HttpServletRequest request,HttpSession session) {
		
		String status = "";
		
		int lenderId = Integer.parseInt(request.getParameter("lenderId"));		
		String condition = request.getParameter("condition");
		
		status = controllerService.manageLenderStatus(lenderId,condition);
		
		return status;
	}
	
	
	@RequestMapping(value = "/createLenderByCntrl", method = RequestMethod.POST)
	@ResponseBody public String createLenderByCntrl(@ModelAttribute("lenderRegDetails") LenderRegistrationDetails lenderRegDetails,HttpSession session) {
		
		String status = controllerService.createLenderByCntrl(lenderRegDetails);
		
		return status;
	}
	
	
	@RequestMapping(value = "/lenderCreate", method = RequestMethod.GET)
	public ModelAndView lenderCreate(@ModelAttribute("lenderRegDetails") LenderRegistrationDetails lenderRegDetails,HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("controllerId") == null)
		{									
        	return new ModelAndView("redirect:/controllerLogin");
        }
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			String controllerEmailId = (String)session.getAttribute("controllerId");
			List<ControllerDetailsBean> controllerData = controllerService.getControllerDetails(controllerEmailId);
			
			model.put("controllerData",controllerData);
		
			return new ModelAndView("controller_lender_create",model);		
		}				
	}
	
	
	@RequestMapping(value = "/lenderExportData", method = RequestMethod.GET)
	public String lenderExportData(HttpSession session,HttpServletRequest request) {
		
		String parameter = request.getParameter("parameter");
		
		String searchType = request.getParameter("searchType");		
		String searchValue = request.getParameter("searchValue");
		
		
		ReportinExcel report = new ReportinExcel();
		
		String fileName = "";
		String reportDirectory = "";
		
		List<LenderDetailsBean> lenderBean = new ArrayList<LenderDetailsBean>();
				
		if(parameter.equals("default"))
		{
			lenderBean = controllerService.getLastLenderDetails();
		}
		else
		{
			lenderBean = controllerService.getSearchLender(searchType,searchValue);
		}
		
		
		fileName = report.generateLenderReportInExcel(lenderBean);
		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
	 	reportDirectory = fileResource.getString("LenderReport");		
		
	 	File fld = new File(reportDirectory);
    	if(!fld.exists())
    	{
		    fld.mkdirs();
    	}
		
		request.setAttribute("rootDirectory",reportDirectory);
		request.setAttribute("fileName",fileName);
		
		return "commonDownload";
	}
	
	

	///////////////////////////////////////////   END OF MANAGE LENDER    //////////////////////////////////////////////
	
	
	
	
	
	
	/////////////////////////////////////////////   START OF LOAN MASTER //////////////////////////////////////////////////
	
	
	@RequestMapping(value = "/loanProcess", method = RequestMethod.GET)
	public ModelAndView loanProcess(HttpSession session,HttpServletRequest request) {
						
		if (session.getAttribute("controllerId") == null)
		{									
        	return new ModelAndView("redirect:/controllerLogin");
        }
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();				

			String condititon = "Today";
			String Parameter = "NR";
			
			model.put("dealDetails",controllerService.getDealDetails(condititon,Parameter));
			
			String controllerEmailId = (String)session.getAttribute("controllerId");
			List<ControllerDetailsBean> controllerData = controllerService.getControllerDetails(controllerEmailId);
			
			model.put("controllerData",controllerData);
		
			return new ModelAndView("controller_borrower_request",model);
		}						
	}
	
	
	@RequestMapping(value = "/searchBorrowerRequestPolicy", method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchBorrowerRequestPolicy(HttpSession session,HttpServletRequest request) {
		
		String searchType = request.getParameter("searchType");		
		String searchValue = request.getParameter("searchValue");
		String loanStatusDiv = request.getParameter("loanStatusDiv");
		
		Map<String, Object> model = new HashMap<String, Object>();
			
		model.put("dealDetails",controllerService.getSearchDealDetails(searchType,searchValue,loanStatusDiv));
		
		return new ModelAndView("controller_borrower_policy_request",model);	
	}
	
	
	@RequestMapping(value = "/manageBorrowerRequest", method = RequestMethod.POST)
	@ResponseBody public ModelAndView manageBorrowerRequest(HttpSession session,HttpServletRequest request){
		
		int dealId = Integer.parseInt(request.getParameter("dealId"));		
		
		Map<String, Object> model = new HashMap<String, Object>();
			
		String condititon = "DealId";
		String Parameter = dealId+"";
		
		model.put("dealDetails",controllerService.getDealDetailToView(condititon,Parameter));
		
		return new ModelAndView("controller_manage_borrower_request",model);	
	}
	
	
	@RequestMapping(value = "/viewUserProfile",method = RequestMethod.POST)
	@ResponseBody public ModelAndView viewBorrowerProfile(HttpSession session,HttpServletRequest request)
	{			
		    String userType = request.getParameter("userType");
		    
		    Map<String, Object> model = new HashMap<String, Object>();
			
		    if(userType.equals("Borrower"))
		    {
		    	int userId = Integer.parseInt(request.getParameter("userId"));
				
				model.put("borrower",borrowerService.getBorrowerDetails(userId));
				
				model.put("documents",borrowerService.attachedDocuments(userId));
				
				return new ModelAndView("admin_borrower_profile",model);	
				
		    }
		    else
		    {
		    	int userId = Integer.parseInt(request.getParameter("userId"));
				
				model.put("lender",lenderService.getLenderDetails(userId));
				
				return new ModelAndView("admin_lender_profile",model);	
		    }	
	}
	
	
	
	@RequestMapping(value = "/updateDealStatusByController", method = RequestMethod.POST)
	@ResponseBody public String updateDealStatusByController(HttpSession session,HttpServletRequest request){
		
		int dealId = Integer.parseInt(request.getParameter("dealId"));		
		
		String cntrlStatus = request.getParameter("cntrlStatus");
		String dealStatus = request.getParameter("dealStatus");
		String visStatus = request.getParameter("visStatus");
		
		LenderToBorrowerFinanceDealBean dealBean = new LenderToBorrowerFinanceDealBean();
		
		dealBean.setDeal_id(dealId);
		//dealBean.setl(cntrlStatus);
		dealBean.setDeal_status(dealStatus);
		dealBean.setExtra_info(visStatus);
		
		String updateStatus = controllerService.updateDealStatusByController(dealBean);
		
		return updateStatus;	
	}
	
	///////////////////////////////////////////////   END OF LOAN MASTER /////////////////////////////////////////////////////
	
	
	////////////////////////////////////////////   START OF POLICY APPROVAL  /////////////////////////////////////////////////////
	
	
	@RequestMapping(value = "/policyApproval", method = RequestMethod.GET)
	public ModelAndView policyApproval(HttpSession session,HttpServletRequest request) {
			
		if (session.getAttribute("controllerId") == null)
		{
			return new ModelAndView("redirect:/controllerLogin");
		}
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("controller_approve_loan",model);				
		}					
	}
	
	
	
	
	
	///////////////////////////////////////////////    END OF POLICY APPROVAL   //////////////////////////////////////////////////
		
	
	
	
	@RequestMapping(value = "/reportAdmin", method = RequestMethod.GET)
	public ModelAndView reportAdmin(HttpSession session,HttpServletRequest request) {
				
		if (session.getAttribute("controllerId") == null)
		{									
        	return new ModelAndView("redirect:/controllerLogin");
        }
		else 
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			String controllerEmailId = (String)session.getAttribute("controllerId");
			List<ControllerDetailsBean> controllerData = controllerService.getControllerDetails(controllerEmailId);
			
			model.put("controllerData",controllerData);
			
			return new ModelAndView("controller_report_admin",model);
		}						
	}
                                                                    	
	@RequestMapping(value = "/adminMessage", method = RequestMethod.GET)
	public ModelAndView adminMessage(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("controllerId") == null)
		{									
        	return new ModelAndView("redirect:/controllerLogin");
        }
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			String controllerEmailId = (String)session.getAttribute("controllerId");
			List<ControllerDetailsBean> controllerData = controllerService.getControllerDetails(controllerEmailId);
			
			model.put("controllerData",controllerData);
			
			
			
			
			return new ModelAndView("controller_report_from_admin",model);	
		}					
	}
	

	//////////////////////////    CONTROLLER SETTING //////////////////////////////////////////////////////////
	
	
	
	@RequestMapping(value = "/controllerSetting", method = RequestMethod.GET)
	public ModelAndView controllerSetting(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("controllerId") == null)
		{									
        	return new ModelAndView("redirect:/controllerLogin");
        }
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			String controllerEmailId = (String)session.getAttribute("controllerId");
			List<ControllerDetailsBean> controllerData = controllerService.getControllerDetails(controllerEmailId);
			
			model.put("controllerData",controllerData);
			
			return new ModelAndView("controller_setting",model);	
		}					
	}
	
	
	
	@RequestMapping(value = "/changeControllerPassword",method = RequestMethod.POST)
	@ResponseBody public String changeControllerPassword(HttpSession session,HttpServletRequest request)
	{		
		String curPassword = request.getParameter("curPassword");
		String newPassword = request.getParameter("newPassword");
		
		String controllerEmailId = (String)session.getAttribute("controllerId");
		String changeStatus = controllerService.changeControllerPassword(controllerEmailId,curPassword,newPassword);	
		
		return changeStatus;		
	}
	
	
	@RequestMapping(value = "/saveControllerDetail",method = RequestMethod.POST)
	@ResponseBody public String saveControllerDetail(HttpSession session,MultipartHttpServletRequest request)
	{				
		String controllerEmailId = (String)session.getAttribute("controllerId");
		List<ControllerDetailsBean> controllerData = controllerService.getControllerDetails(controllerEmailId);
		
		
		
		String name = request.getParameter("name");
		String role = request.getParameter("role");
		
		String photo = request.getParameter("file");
		
		String changeStatus = "";	
				
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
	 	    
 	    String filePath = fileResource.getString("controllerPhoto");
 	    		
 	    File ourPath = new File(filePath);
 	    
 	    String storePath = ourPath.getPath()+"/"+controllerData.get(0).getController_id()+"/";
 	    
 	    String fileName = "";
 	    
 	    Iterator<String> itr =  request.getFileNames(); 
	    MultipartFile mpf = request.getFile(itr.next());

        MultipartFile multipartFile = request.getFile("file");
        fileName = multipartFile.getOriginalFilename();
        
        changeStatus = controllerService.getUpdateControllerDetails(controllerEmailId,name,role,fileName);
        
        
        if(!fileName.equals(""))
		{	    				
	        File fld = new File(storePath);
	    	if(!fld.exists())
	    	{
			    fld.mkdirs();
	    	}
	        try 
	        {
            	multipartFile.transferTo(new File(storePath+fileName));
			} 
	        catch (Exception e) 
	        {
				// TODO Auto-generated catch block
				e.printStackTrace();				                    
            }	        
		}
        
		return changeStatus;		
	}
	
	
	
/////////////////////////////////////////        END OF CONTROLLER SETTING      /////////////////////////////////////////////////////////////////////
	

	
	
	
	
	
	@RequestMapping(value = "/progressDesign", method = RequestMethod.GET)
	public String progressDesign(HttpSession session,HttpServletRequest request) {
						
		return "aaaaProgressBar";				
	}
	
	
	@RequestMapping(value = "/authControllerCredential", method = RequestMethod.POST)
	public String authControllerCredential(@ModelAttribute("controllerDetails") ControllerDetails controllerDetails,HttpSession session,HttpServletRequest request)
	{		
		String controller_user_id = controllerDetails.getController_user_id();
        String controller_password = controllerDetails.getController_password();
       
        String authenticateStatus = controllerService.getValidateController(controller_user_id,controller_password);
        if(authenticateStatus.equals("correct"))
        {
        	session.setAttribute("controllerId", controller_user_id);
        	session.setAttribute("USER_SESSION_ID", "JIR"+session.getId()+"K");
        	return "redirect:/controllerHome";	
        }
        else
        {
        	request.setAttribute("actionMessage", "failed");
        	return "redirect:/controllerLogin";
        }	      
	}	
	
	
	@RequestMapping(value="/fgh", method = RequestMethod.GET)
	public String defaultGetUrl()
	{		
		return "ebsPaymentForm";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@SuppressWarnings({ "resource", "unused" })
	@RequestMapping(value = "/previewUserPhoto", method = RequestMethod.GET)
	@ResponseBody public byte[] previewUserPhoto(HttpServletRequest request,HttpSession session) throws IOException {
		
		
		String userType = request.getParameter("userType");
		String fileName = request.getParameter("fileName");
		String gender = "";
		
		String fileDir = "";
		int id = 0;
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
		
		if(userType.equals("Borrower"))
		{
			gender = request.getParameter("gender");
			
			id = Integer.parseInt(request.getParameter("id"));
			
			fileDir = fileResource.getString("BorrowerFile");
			
			fileDir = fileDir+"/"+id+"/ProfilePhoto";			
		}
		else if(userType.equals("Lender"))
		{
			gender = "NA";
					
			id = Integer.parseInt(request.getParameter("id"));
			
			fileDir = fileResource.getString("LenderFile");
			
			fileDir = fileDir+"/"+id+"/ProfilePhoto";			
		}
		else
		{
			gender = "NA";
			
			id = Integer.parseInt(request.getParameter("id"));
			
			fileDir = fileResource.getString("controllerPhoto");
			
			fileDir = fileDir+"/"+id;	
		}
		
		
		
		ServletContext servletContext = request.getSession().getServletContext();
		
		File ourPath = new File(fileDir);
		
		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/"+fileName);
		String docCategory = fPath.toString();
		
		String defaultPath = "";
		
		if(fileName.equals(""))
		{			
			if(gender.equals("Male"))
			{
				defaultPath = request.getServletContext().getRealPath("/Back_End_Files/male.png");
			}
			else if(gender.equals("Female"))
			{
				defaultPath = request.getServletContext().getRealPath("/Back_End_Files/female.png");
			}
			else
			{
				defaultPath = request.getServletContext().getRealPath("/Back_End_Files/male.png");
			}
			
			fPath.delete(0, fPath.length());
			File targetFile = new File(defaultPath);					
			fPath.append(targetFile.getPath());					
		}
		else
		{
			if(StringUtils.isNotBlank(docCategory))
			{				
				/* Checks that files are exist or not  */
				
					File exeFile=new File(fPath.toString());
					if(!exeFile.exists())
					{
						if(gender.equals("Male"))
						{
							defaultPath = request.getServletContext().getRealPath("/Back_End_Files/male.png");
						}
						else if(gender.equals("Female"))
						{
							defaultPath = request.getServletContext().getRealPath("/Back_End_Files/female.png");
						}
						else
						{
							defaultPath = request.getServletContext().getRealPath("/Back_End_Files/male.png");
						}
						
						
						fPath.delete(0, fPath.length());
						File targetFile = new File(defaultPath);					
						fPath.append(targetFile.getPath());	
					}	
					
				/* Checks that files are exist or not  */
					
			}
		}
	
		InputStream in;
		
		try {					
				FileInputStream docdir = new FileInputStream(fPath.toString());													
				if (docdir != null) 
				{						
					return IOUtils.toByteArray(docdir);					
				} 
				else 
				{							
					return null;
				}			
		} 
		catch (FileNotFoundException e) {
			
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		}

	}
	
	
	
	
	@SuppressWarnings({ "resource", "unused" })
	@RequestMapping(value = "/viewAttachedDocByOther", method = RequestMethod.GET)
	@ResponseBody public byte[] viewAttachedDocByOther(HttpServletRequest request,HttpSession session) throws IOException {
		
		
		String fileName = request.getParameter("fileName");
		String userType = request.getParameter("userType");
		int userId = Integer.parseInt(request.getParameter("userId"));
		
		String fileDir = "";
		
		if(userType.equals("Borrower"))
		{
			// Required file Config for entire Controller 
			 
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
			
			fileDir = fileResource.getString("BorrowerFile");
			
			fileDir = fileDir+"/"+userId+"/AttachedDocuments";
			 
		}
		else
		{
			// Required file Config for entire Controller 
			 
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
			
			fileDir = fileResource.getString("LenderFile");
			
			fileDir = fileDir+"/"+userId+"/AttachedDocuments";			
		}
		
		
		// End of Required file Config for entire Controller 
		 
		ServletContext servletContext = request.getSession().getServletContext();
		
		File ourPath = new File(fileDir);
		
		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/"+fileName);
		String docCategory = fPath.toString();
		
		if(StringUtils.isNotBlank(docCategory))
		{				
			/* Checks that files are exist or not  */
			
				File exeFile=new File(fPath.toString());
				if(!exeFile.exists())
				{
					fPath.delete(0, fPath.length());
					File targetFile = new File(request.getServletContext().getRealPath("/Back_End_Files/image-not-found.png"));					
					fPath.append(targetFile.getPath());
				}	
				
			/* Checks that files are exist or not  */
				
		}
		
		InputStream in;
		
		try {					
				FileInputStream docdir = new FileInputStream(fPath.toString());													
				if (docdir != null) 
				{						
					return IOUtils.toByteArray(docdir);					
				} 
				else 
				{							
					return null;
				}			
		} 
		catch (FileNotFoundException e) {
			
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		}

	}
	
	
	@RequestMapping(value = "/downloadDocuments", method = RequestMethod.GET)
	public ModelAndView downloadDocuments(HttpServletRequest request,HttpSession session) {
		
		String fileName = request.getParameter("fileName");
		String userType = request.getParameter("userType");
		int userId = Integer.parseInt(request.getParameter("userId"));
		String reportDirectory = "";
		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
		
		if(userType.equals("Borrower"))
		{			 
			reportDirectory = fileResource.getString("BorrowerFile");
			reportDirectory = reportDirectory+"/"+userId+"/AttachedDocuments";
		}
		else
		{
			reportDirectory = fileResource.getString("LenderFile");	
			reportDirectory = reportDirectory+"/"+userId+"/AttachedDocuments";
		}
		
		request.setAttribute("rootDirectory",reportDirectory);
		request.setAttribute("fileName",fileName);
		
		return new ModelAndView("commonDownload");
	}
	
	
	
}
