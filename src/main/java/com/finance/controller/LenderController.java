package com.finance.controller;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.persistence.metamodel.SetAttribute;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.finance.bean.BorrowerDetailsBean;
import com.finance.bean.InterPortalNotificationBean;
import com.finance.bean.LenderDetailsBean;
import com.finance.bean.LenderPolicyBean;
import com.finance.bean.LenderToBorrowerFinanceDealBean;
import com.finance.domain.BorrowerDocumentModel;
import com.finance.domain.BorrowerRegistrationDetails;
import com.finance.domain.LenderDocumentModel;
import com.finance.domain.LenderIdentityModel;
import com.finance.domain.LenderPolicyModel;
import com.finance.domain.LenderRegistrationDetails;
import com.finance.service.AdminService;
import com.finance.service.BorrowerService;
import com.finance.service.LenderService;

@Controller
public class LenderController {

	@Autowired
	LenderService lenderService;
	
	
	@Autowired
	BorrowerService borrowerService;
	
	
	@Autowired
	private AdminService adminService;
	
	
	
	@RequestMapping(value = "/lenderLogout", method = RequestMethod.GET)
	public String adminLogout(HttpSession session,HttpServletRequest request) {
					
		session.removeAttribute("lenderId");			 
		request.setAttribute("actionMessage", "logout");
        
		return "redirect:/lenderLogin";				
	}
	
	@RequestMapping(value = "/lenderDashboard", method = RequestMethod.GET)
	public String lenderDashboard(HttpSession session,HttpServletRequest request) {
						
		return "lender_dashboard";				
	}   
		
	
	@RequestMapping(value = "/lenProfile", method = RequestMethod.GET)
	public ModelAndView lenProfile(HttpSession session,HttpServletRequest request) {
			
		if (session.getAttribute("lenderId") == null)
		{				
        	return new ModelAndView("redirect:/lenderLogin");
        }				
		else
		{			
			int lenderId = (int) session.getAttribute("lenderId");

        	LenderDetailsBean lenderBean = lenderService.getLenderDetails(lenderId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("lenderBean",lenderBean);
			
			model.put("policyBean",lenderService.getLenderLastFinancialFeature(lenderId));
			
			model.put("dealBean",lenderService.getLenderLastBorrowerRequest(lenderId));
			
			model.put("attachedDocument",lenderService.attachedDocuments(lenderId));
			
			model.put("unreadNotification",lenderService.getUnreadNotification(lenderId,"Lender"));
			
			return new ModelAndView("lender_profile",model);	
		}					
	}   
	
	@RequestMapping(value = "/borrowerRqst", method = RequestMethod.GET)
	public ModelAndView borrowerRqst(HttpSession session,HttpServletRequest request) {
						
		if (session.getAttribute("lenderId") == null)
		{				
        	return new ModelAndView("redirect:/lenderLogin");
        }				
		else
		{
			int lenderId = (int) session.getAttribute("lenderId");

        	LenderDetailsBean lenderBean = lenderService.getLenderDetails(lenderId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("lenderBean",lenderBean);
			
			request.setAttribute("lenderPolicyType", lenderService.getLenderPolicyType(lenderId));
			
			model.put("borrowerRequestBean",lenderService.getBorrowerLoanRequest(lenderId));
			
			model.put("unreadNotification",lenderService.getUnreadNotification(lenderId,"Lender"));
			
			return new ModelAndView("lender_bwr_request",model);				
		}
	} 
	
	
	@RequestMapping(value = "/searchBorrowerReqByLender", method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchBorrowerReqByLender(HttpServletRequest request,HttpSession session) {
		
		String searchType = request.getParameter("searchType");
		
		LenderToBorrowerFinanceDealBean policyReqBean = new LenderToBorrowerFinanceDealBean();
		
		policyReqBean.setLender_id((int) session.getAttribute("lenderId"));
		
		if(searchType.equals("Via Duration"))
		{
			policyReqBean.setSearch_from_date(request.getParameter("fromDateId"));
			policyReqBean.setSearch_to_date(request.getParameter("toDateId"));			
		}
		else if(searchType.equals("Via Loan Type"))
		{
			policyReqBean.setPolicy_type(request.getParameter("policyTypeValue"));
		}
		else
		{
			policyReqBean.setLender_status("Approved");
		}
		
		policyReqBean.setExtra_info(searchType);
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		model.put("borrowerRequestBean",lenderService.getBorrowerPolicyRequest(policyReqBean));
		
		return new ModelAndView("lender_borrower_loan_request",model);
	}
	
	
	@RequestMapping(value = "/checkBorrowerRequest", method = RequestMethod.POST)
	@ResponseBody public String checkBorrowerRequest(HttpServletRequest request,HttpSession session) {
		
		int dealId = Integer.parseInt(request.getParameter("dealId"));
		String interest = request.getParameter("interest");
		int lenderId = (int) session.getAttribute("lenderId");
		
		String status = lenderService.manageBorrowerRequest(dealId,lenderId,interest);
		
		return status;
	}
	
	
	@RequestMapping(value = "/showReasonBox", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showReasonBox(HttpServletRequest request,HttpSession session) {
		
		int dealId = Integer.parseInt(request.getParameter("dealId"));
		
		request.setAttribute("dealId",dealId);
		
		request.setAttribute("rejectStage","rejectFromPending");
		
		return new ModelAndView("lender_reason_textbox");
	}
	
	@RequestMapping(value = "/submitReason", method = RequestMethod.POST)
	@ResponseBody public String submitReason(HttpServletRequest request,HttpSession session) {
		
		int dealId = Integer.parseInt(request.getParameter("passingDealId"));		
		String reason = request.getParameter("reasonContent");
		
		String rejectStage = request.getParameter("rejectStage");
		
		String status = lenderService.submitReason(dealId,reason,rejectStage);
		
		return status;
	}
	
	
	
	
	//////////////////////// END OF BORROWER REQUEST /////////////////////////////////////
	
	
	////////////////////////  START OF INTERESTED REQUESTS  ////////////////////////////////////
	
	
	
	@RequestMapping(value = "/borrowerInterestRqst", method = RequestMethod.GET)
	public ModelAndView borrowerInterestRqst(HttpSession session,HttpServletRequest request) {
						
		if (session.getAttribute("lenderId") == null)
		{				
        	return new ModelAndView("redirect:/lenderLogin");
        }				
		else
		{
			int lenderId = (int) session.getAttribute("lenderId");

        	LenderDetailsBean lenderBean = lenderService.getLenderDetails(lenderId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("lenderBean",lenderBean);
			
			request.setAttribute("lenderPolicyType", lenderService.getLenderPolicyType(lenderId));
			
			model.put("borrowerRequestBean",lenderService.getBorrowerApproveLoan(lenderId));
			
			model.put("unreadNotification",lenderService.getUnreadNotification(lenderId,"Lender"));
			
			return new ModelAndView("lender_interest_request",model);				
		}
	} 
	
	
	
	@RequestMapping(value = "/searchBorInterestedLoans", method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchBorInterestedLoans(HttpServletRequest request,HttpSession session) {
		
		String searchType = request.getParameter("searchType");
		
		LenderToBorrowerFinanceDealBean policyReqBean = new LenderToBorrowerFinanceDealBean();
		
		policyReqBean.setLender_id((int) session.getAttribute("lenderId"));
		
		if(searchType.equals("Via Duration"))
		{
			policyReqBean.setSearch_from_date(request.getParameter("fromDateId"));
			policyReqBean.setSearch_to_date(request.getParameter("toDateId"));			
		}
		else if(searchType.equals("Via Loan Type"))
		{
			policyReqBean.setPolicy_type(request.getParameter("policyTypeValue"));
		}
		else
		{
			policyReqBean.setLender_status("Approved");
		}
		
		policyReqBean.setExtra_info(searchType);
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		model.put("borrowerRequestBean",lenderService.getBorrowerInterestedRequest(policyReqBean));
		
		return new ModelAndView("lender_borrower_interested_loans",model);
	}
	
	
	
	@RequestMapping(value = "/viewBorrowerProfileByLender", method = RequestMethod.POST)
	@ResponseBody public ModelAndView viewBorrowerProfileByLender(HttpServletRequest request,HttpSession session) {
		
		int dealId = Integer.parseInt(request.getParameter("dealId"));
				
		int policyId = Integer.parseInt(request.getParameter("policyId"));
		
		int borrowerId = Integer.parseInt(request.getParameter("borrowerId"));
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		BorrowerDetailsBean borrowerBean = borrowerService.getBorrowerDetails(borrowerId);
		
		model.put("borrowerBean",borrowerBean);
		
		model.put("attachedDocument",borrowerService.attachedDocuments(borrowerId));
		
		model.put("dealInformation",lenderService.getDealStatus(dealId));
		
		request.setAttribute("policyNeedDoc", lenderService.getRequiredDocForPolicy(policyId));
		
		request.setAttribute("borrowerId", borrowerId);
		
		return new ModelAndView("lender_view_borrower_profile",model);
	}
	
	
	
	@RequestMapping(value = "/showApprovalTermsCondition", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showApprovalTermsCondition() {
	
		return new ModelAndView("lender_approve_terms_condition");
	}
	
	
	
	@RequestMapping(value = "/approveBorrowerRequest", method = RequestMethod.POST)
	@ResponseBody public String approveBorrowerRequest(HttpServletRequest request,HttpSession session) {
	
		String updateStatus = "";
		
		int dealId = Integer.parseInt(request.getParameter("dealId"));
		
		updateStatus = lenderService.approveBorrowerRequest(dealId);
		
		return updateStatus;
	}
	
	@RequestMapping(value = "/showRejectReasonBox", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showRejectReasonBox(HttpServletRequest request,HttpSession session) {
		
		int dealId = Integer.parseInt(request.getParameter("dealId"));
		
		request.setAttribute("dealId",dealId);
		
		request.setAttribute("rejectStage","rejectFromProcessing");
		
		return new ModelAndView("lender_reason_textbox");
	}
	
	
	@RequestMapping(value = "/printBorrowerRequest", method = RequestMethod.POST)
	@ResponseBody public ModelAndView printBorrowerRequest(HttpServletRequest request,HttpSession session) {
		
		int dealId = Integer.parseInt(request.getParameter("dealId"));
				
		int policyId = Integer.parseInt(request.getParameter("policyId"));
		
		int borrowerId = Integer.parseInt(request.getParameter("borrowerId"));
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		BorrowerDetailsBean borrowerBean = borrowerService.getBorrowerDetails(borrowerId);
		
		model.put("borrowerBean",borrowerBean);
		
		model.put("attachedDocument",borrowerService.attachedDocuments(borrowerId));
		
		model.put("dealInformation",lenderService.getDealStatus(dealId));
		
		request.setAttribute("policyNeedDoc", lenderService.getRequiredDocForPolicy(policyId));
		
		request.setAttribute("borrowerId", borrowerId);
		
		return new ModelAndView("lender_print_borrower_application",model);
	}
	
	
	//////////////////////////   END OF INTERESTED REQUESTS  ////////////////////////////////////////
	
	
	
	
     ////////////////////   START OF LOAN STATUS     ///////////////////////////////
	
	
	
	
	@RequestMapping(value = "/loanStatus", method = RequestMethod.GET)
	public ModelAndView loanStatus(HttpSession session,HttpServletRequest request) {
						
		if (session.getAttribute("lenderId") == null)
		{				
        	return new ModelAndView("redirect:/lenderLogin");
        }				
		else
		{
			int lenderId = (int) session.getAttribute("lenderId");

        	LenderDetailsBean lenderBean = lenderService.getLenderDetails(lenderId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("lenderBean",lenderBean);
			
			request.setAttribute("lenderPolicyType", lenderService.getLenderPolicyType(lenderId));
			
			model.put("acceptedDealStatus",lenderService.getLenderAcceptedDealDetails(lenderId));
			
			model.put("unreadNotification",lenderService.getUnreadNotification(lenderId,"Lender"));
			
			return new ModelAndView("lender_status",model);				
		}
	}
	
	
	@RequestMapping(value = "/searchApprovedLoanByLender", method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchApprovedLoanByLender(HttpServletRequest request,HttpSession session) {
		
		String searchType = request.getParameter("searchType");
		
		LenderToBorrowerFinanceDealBean policyReqBean = new LenderToBorrowerFinanceDealBean();
		
		policyReqBean.setLender_id((int) session.getAttribute("lenderId"));
		
		if(searchType.equals("Via Duration"))
		{
			policyReqBean.setSearch_from_date(request.getParameter("fromDateId"));
			policyReqBean.setSearch_to_date(request.getParameter("toDateId"));			
		}
		else if(searchType.equals("Via Loan Type"))
		{
			policyReqBean.setPolicy_type(request.getParameter("policyTypeValue"));
		}
		else
		{
			policyReqBean.setLender_status("Approved");
		}
		
		policyReqBean.setExtra_info(searchType);
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		model.put("acceptedDealStatus",lenderService.getBorrowerPolicyApproved(policyReqBean));
		
		return new ModelAndView("lender_borrower_loan_approved",model);
	}
	
	
	@RequestMapping(value = "/viewApprovedDealByLender", method = RequestMethod.POST)
	@ResponseBody public ModelAndView viewApprovedDealByLender(HttpServletRequest request,HttpSession session) {
		
		int dealId = Integer.parseInt(request.getParameter("dealId"));
		
		int policyId = Integer.parseInt(request.getParameter("policyId"));
		
		int borrowerId = Integer.parseInt(request.getParameter("borrowerId"));
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		BorrowerDetailsBean borrowerBean = borrowerService.getBorrowerDetails(borrowerId);
		
		model.put("borrowerBean",borrowerBean);
		
		model.put("attachedDocument",borrowerService.attachedDocuments(borrowerId));
		
		model.put("dealInformation",lenderService.getDealStatus(dealId));
		
		request.setAttribute("policyNeedDoc", lenderService.getRequiredDocForPolicy(policyId));
		
		request.setAttribute("borrowerId", borrowerId);
		
		return new ModelAndView("lender_loan_details_borrower",model);
	}
	
	
	
	@RequestMapping(value = "/amountReleaseStatus", method = RequestMethod.POST)
	@ResponseBody public String amountReleaseStatus(HttpServletRequest request,HttpSession session) {
		
		int dealId = Integer.parseInt(request.getParameter("dealId"));
		String status = request.getParameter("status");
		
		String updateStatus = lenderService.getUpdateAmoundDispouseStatus(dealId,status);
		
		return updateStatus;
	}
	
	////////////////////    END OF LOAN STATUS     ///////////////////////////////
	
	
	
    ////////////////////  START OF FINANCIAL FEATURE    ///////////////////////////////
	
	
	@RequestMapping(value = "/addFeature", method = RequestMethod.GET)
	public ModelAndView addFeature(@ModelAttribute("policyModel") LenderPolicyModel policyModel,HttpSession session,HttpServletRequest request) {
			
		if (session.getAttribute("lenderId") == null)
		{				
        	return new ModelAndView("redirect:/lenderLogin");
        }				
		else
		{
			int lenderId = (int) session.getAttribute("lenderId");

        	LenderDetailsBean lenderBean = lenderService.getLenderDetails(lenderId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("lenderBean",lenderBean);
			
			request.setAttribute("policyTypeName", adminService.getPolicyType());
			
			request.setAttribute("policyName", adminService.getPolicyNames());
			
			model.put("lenderPolicyDetails",lenderService.getLenderPolicyDetails(lenderId));
			
			request.setAttribute("docList",borrowerService.getBorrowerDocList());
			
			model.put("unreadNotification",lenderService.getUnreadNotification(lenderId,"Lender"));
			
			return new ModelAndView("lender_financial_feature",model);				
		}
	}
	
	@RequestMapping(value = "/saveLenderPolicyDetails", method = RequestMethod.POST)
	@ResponseBody public String saveLenderPolicyDetails(@ModelAttribute("policyModel") LenderPolicyModel policyModel,MultipartHttpServletRequest request,HttpSession session) {
		
		int lenderId = (int) session.getAttribute("lenderId");
		
		String saveStatus = "";
		
		String fileName = "";
 	    
        MultipartFile multipartFile = request.getFile("file");
        fileName = multipartFile.getOriginalFilename();
        
		policyModel.setPolicy_broucher(fileName);
		policyModel.setLender_id(lenderId);
		
		
		String startDate = request.getParameter("policy_start_date");
		String expireDate = request.getParameter("policy_end_date");
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        try {

            Date strartdate = formatter.parse(startDate);
            Date expiredate = formatter.parse(expireDate);
            
            policyModel.setPolicy_start_date(strartdate);
            policyModel.setPolicy_end_date(expiredate);
           
        } 
        catch (ParseException e) {
            e.printStackTrace();
        }
		
        
        // part of code will when update the policy 
	        
            int policyId = policyModel.getPolicy_id();
	        
	        if(fileName.equals(""))
			{
	        	if(policyId!=0)
	            {
	        		policyModel.setPolicy_broucher(lenderService.getBroucherFile(policyId));
	            }
			}
	        
        // End update the policy 
        
		saveStatus = lenderService.saveLenderPolicy(policyModel);
		
		if(saveStatus.equals("success"))
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
	 	    
	 	    String filePath = fileResource.getString("lenderBroucher");
	 	    		
	 	    File ourPath = new File(filePath);
	 	    
	 	    String storePath = ourPath.getPath()+"/"+lenderId+"/";	 	    
	 	    
	        if(!fileName.equals(""))
			{	    				
		        File fld = new File(storePath);
		    	
		        if(!fld.exists())
		    	{
				    fld.mkdirs();
		    	}
		        try 
		        {
	            	multipartFile.transferTo(new File(storePath+fileName));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();					                    
	            }
		        
	 	          // End of saving File
			}
	        
		}
				
        
		return saveStatus;
	}
	
	@RequestMapping(value = "/showFeatureFormToUpdate", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showFeatureFormToUpdate(@ModelAttribute("policyModel") LenderPolicyModel policyModel,HttpSession session,HttpServletRequest request) {
			
		if (session.getAttribute("lenderId") == null)
		{				
        	return new ModelAndView("redirect:/lenderLogin");
        }				
		else
		{
			int lenderId = (int) session.getAttribute("lenderId");
			
			int policyId = Integer.parseInt(request.getParameter("policyId"));
			
        	LenderDetailsBean lenderBean = lenderService.getLenderDetails(lenderId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("lenderBean",lenderBean);
			
			request.setAttribute("policyTypeName", adminService.getPolicyType());
			
			request.setAttribute("policyName", adminService.getPolicyNames());
			
			model.put("policyDetails",lenderService.getPolicyDetails(policyId,lenderId));
			
			request.setAttribute("docList",borrowerService.getBorrowerDocList());
			
			return new ModelAndView("lender_update_feature_form",model);						
		}
	}
	
	
	
	@RequestMapping(value = "/showSubPolicyType", method = RequestMethod.POST)
	@ResponseBody public String showSubPolicyType(HttpServletRequest request,HttpSession session) {
		
		String policyName = request.getParameter("policyName");
			
		String record = "";
		
		record = lenderService.getSubPolicyTypeName(policyName);
		
		
		return record;
	}
	
	
	
	@RequestMapping(value = "/disContinueFinanceFeature", method = RequestMethod.POST)
	@ResponseBody public ModelAndView disContinueFinanceFeature(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("lenderId") == null)
		{				
        	return new ModelAndView("redirect:/lenderLogin");
        }				
		else
		{
			int policyId = Integer.parseInt(request.getParameter("policyId"));
			
			int lenderId = (int) session.getAttribute("lenderId");
			
			request.setAttribute("totalDealCount",lenderService.countDealIntiateWithPolicyId(policyId));
			request.setAttribute("countDealInPending",lenderService.countDealPendingWithPolicyId(policyId));
			request.setAttribute("countDealInApproved",lenderService.countDealApprovedWithPolicyId(policyId));
			
			request.setAttribute("policyId",policyId);
			
			return new ModelAndView("lender_policy_status");
		}
	}
	
	
	@RequestMapping(value = "/disContinuePolicy", method = RequestMethod.POST)
	@ResponseBody public String disContinuePolicy(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("lenderId") == null)
		{				
        	return "redirect:/lenderLogin";
        }				
		else
		{
			int policyId = Integer.parseInt(request.getParameter("policyId"));
			
			//int lenderId = (int) session.getAttribute("lenderId");
		
			String status = lenderService.discontinuePolicy(policyId);
			
			return status;
		}
	}
	
	 ////////////////////  END OF FINANCIAL FEATURE    ///////////////////////////////
	
	
	
	
	////////////////////////////    START OF EXIST POLICY //////////////////////////
	
	
	
	@RequestMapping(value = "/existPolicy", method = RequestMethod.GET)
	public ModelAndView existPolicy(@ModelAttribute("policyModel") LenderPolicyModel policyModel,HttpSession session,HttpServletRequest request) {
			
		if (session.getAttribute("lenderId") == null)
		{				
        	return new ModelAndView("redirect:/lenderLogin");
        }				
		else
		{
			int lenderId = (int) session.getAttribute("lenderId");

        	LenderDetailsBean lenderBean = lenderService.getLenderDetails(lenderId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("lenderBean",lenderBean);
						
			model.put("lenderPolicyDetails",lenderService.getLenderPolicyDetails(lenderId));
			
			request.setAttribute("lenderPolicyType", lenderService.getLenderPolicyType(lenderId));
			
			model.put("unreadNotification",lenderService.getUnreadNotification(lenderId,"Lender"));
			
			return new ModelAndView("lender_exist_policy",model);				
		}
	}
	
	
	
	@RequestMapping(value = "/searchExistPolicy", method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchExistPolicy(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("lenderId") == null)
		{				
        	return new ModelAndView("redirect:/lenderLogin");
        }				
		else
		{
			int lenderId = (int) session.getAttribute("lenderId");

			String searchType = request.getParameter("searchType");
			
			String policyTypeValue = request.getParameter("policyTypeValue");
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("lenderPolicyDetails",lenderService.getSearchLenderPolicyDetails(lenderId,searchType,policyTypeValue));
			 
			return new ModelAndView("lender_policy_list",model);
		}
	}
	
	
	@RequestMapping(value = "/viewPolicy", method = RequestMethod.POST)
	@ResponseBody public ModelAndView viewPolicy(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("lenderId") == null)
		{				
        	return new ModelAndView("redirect:/lenderLogin");
        }				
		else
		{
			int lenderId = (int) session.getAttribute("lenderId");
			int policyId = Integer.parseInt(request.getParameter("policyId"));
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("lenderPolicyDetails",lenderService.getPolicyDetails(policyId,lenderId));
			 
			return new ModelAndView("lender_view_policy",model);
		}
	}
	
	////////////////////////////   END OF EXIST POLICY   ///////////////////////////////
	
	
	
	
	
	
	////////////////////  START OF LENDER SETTING    ///////////////////////////////
	
	
	
	@RequestMapping(value = "/lenderSettings", method = RequestMethod.GET)
	public ModelAndView  lenderSettings(@ModelAttribute("lenderIdnDetails") LenderIdentityModel lenderIdnDetails,HttpSession session,HttpServletRequest request) {
						
		if (session.getAttribute("lenderId") == null)
		{				
        	return new ModelAndView("redirect:/lenderLogin");
        }				
		else
		{
			int lenderId = (int) session.getAttribute("lenderId");

        	LenderDetailsBean lenderBean = lenderService.getLenderDetails(lenderId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("lenderBean",lenderBean);
			
			model.put("unreadNotification",lenderService.getUnreadNotification(lenderId,"Lender"));
			
			return new ModelAndView("lender_settings",model);				
		}
	}
	
	
	@RequestMapping(value = "/lenCngpassword", method = RequestMethod.GET)
	public ModelAndView  lenCngpassword(@ModelAttribute("lenderIdnDetails") LenderIdentityModel lenderIdnDetails,HttpSession session,HttpServletRequest request) {
						
		if (session.getAttribute("lenderId") == null)
		{				
        	return new ModelAndView("redirect:/lenderLogin");
        }				
		else
		{
			int lenderId = (int) session.getAttribute("lenderId");

        	LenderDetailsBean lenderBean = lenderService.getLenderDetails(lenderId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("lenderBean",lenderBean);
			
			model.put("unreadNotification",lenderService.getUnreadNotification(lenderId,"Lender"));
			
			return new ModelAndView("lender_change_password",model);				
		}
	}
	
	
	@RequestMapping(value = "/saveLenderRegister", method = RequestMethod.POST)
	@ResponseBody public String saveLenderRegister(@ModelAttribute("lenderRegDetails") LenderRegistrationDetails lenderRegDetails,HttpSession session) {
		
		String status = lenderService.saveLenderRegdetails(lenderRegDetails);
		
		return status;
	}
	
	
	@RequestMapping(value = "/changeLenderPassword", method = RequestMethod.POST)
	@ResponseBody public String changeLenderPassword(HttpServletRequest request,HttpSession session) {
		
		String status = "";
		
		int lenderId = (int) session.getAttribute("lenderId");
		
		String lenderRepresentative = (String)session.getAttribute("lenderAccess");
		
		status = lenderService.changePassword(lenderId,request.getParameter("currentPassword"),request.getParameter("newPassword"),lenderRepresentative);
		
		return status;
	}
	
	
	
	@RequestMapping(value = "/saveProfileIdnDetails", method = RequestMethod.POST)
	@ResponseBody public String saveProfileIdnDetails(@ModelAttribute("lenderIdnDetails") LenderIdentityModel lenderIdnDetails,MultipartHttpServletRequest request,HttpSession session) {
		
		String status = "";
		
		int lenderId = (int) session.getAttribute("lenderId");
		
		String fileName = "";
 	    
        MultipartFile multipartFile = request.getFile("file");
        fileName = multipartFile.getOriginalFilename();
        
		lenderIdnDetails.setLender_id(lenderId);
		lenderIdnDetails.setCr_dare(new Date());
		if(fileName.equals(""))
		{
			lenderIdnDetails.setLender_photo(lenderService.getLenderProfilePhoto(lenderId));
		}
		else
		{
			lenderIdnDetails.setLender_photo(fileName);
		}
		
		status = lenderService.saveLenderProfileIdnDetails(lenderIdnDetails);
		
		if(status.equals("success"))
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
	 	    
	 	    String filePath = fileResource.getString("LenderFile");
	 	    		
	 	    File ourPath = new File(filePath);
	 	    
	 	    String storePath = ourPath.getPath()+"/"+lenderId+"/ProfilePhoto/";	 	    
	 	    
	        if(!fileName.equals(""))
			{	    				
		        File fld = new File(storePath);
		    	
		        if(!fld.exists())
		    	{
				    fld.mkdirs();
		    	}
		        try 
		        {
	            	multipartFile.transferTo(new File(storePath+fileName));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();					                    
	            }		        
	 	        // End of saving File
			}
		}
		
		return status;
	}
	 
	
	
	
	
	@RequestMapping(value = "/lenderDoc", method = RequestMethod.GET)
	public ModelAndView lenderDoc(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("lenderId") == null)
		{									
        	return new ModelAndView("redirect:/lenderLogin");
        }
		else
		{			
			int lenderId = (int) session.getAttribute("lenderId");

        	LenderDetailsBean lenderBean = lenderService.getLenderDetails(lenderId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("lenderBean",lenderBean);
			
			request.setAttribute("docList",borrowerService.getBorrowerDocList());
			
			model.put("attachedDocument",lenderService.attachedDocuments(lenderId));
			
			model.put("unreadNotification",lenderService.getUnreadNotification(lenderId,"Lender"));
			
			return new ModelAndView("lender_document_form",model);					
		}							
	}
	
	
	
	@RequestMapping(value = "/saveLenderDoc", method = RequestMethod.POST)
	@ResponseBody public String saveLendeDoc(HttpSession session,MultipartHttpServletRequest request) {
	
		int lenderId = (int) session.getAttribute("lenderId");
		
		String fileName = "";
		String status = "";
		String doc_type = request.getParameter("docType");
		
		LenderDocumentModel lenderDoc = new LenderDocumentModel();
 	   
 	    // Last record of Lender if exist
		lenderDoc.setLender_id(lenderId);
		//List<LenderDocumentModel> lastLenderDocRecord = lenderService.getLastLenderDocRecord(lenderId);
		
		// File Storage location
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories"); 	    
 	    String filePath = fileResource.getString("LenderFile");	    		
 	    File ourPath = new File(filePath);	    
 	    String storePath = ourPath.getPath()+"/"+lenderId+"/AttachedDocuments/";	 	    
 	    
 	    //Document nameing Convention
 	    String reName = "";
 	    int lastSequesnce = 0;
 	    
 	    lastSequesnce = lenderService.getLastDocSequence(lenderId,doc_type);
 	    
 	    lastSequesnce = lastSequesnce + 1;
 	    
 	    reName = lenderId+"_"+doc_type.replaceAll(" ","_")+"_"+lastSequesnce;
 	    
 	    MultipartFile multipartFile = request.getFile("file");
	    
        fileName = multipartFile.getOriginalFilename();
       
        String extension = FilenameUtils.getExtension(fileName); // returns "extension"
		
	    File fld = new File(storePath);
	    	
	        if(!fld.exists())
	    	{
			    fld.mkdirs();
	    	}
	        try 
	        {
	        	 multipartFile.transferTo(new File(storePath+fileName));
	        	
	        	 File f = new File(storePath+fileName); 

	             f.renameTo(new File(storePath+reName+"."+extension));
	                
			} 
	        catch (Exception e) 
	        {
				// TODO Auto-generated catch block
				e.printStackTrace();					                    
            }		        
	        // End of saving File	
		
	        lenderDoc.setLender_id(lenderId);
	        lenderDoc.setDocument_type(doc_type);
	        lenderDoc.setFile_name(reName+"."+extension);
	        lenderDoc.setDocument_status("Active");
	        lenderDoc.setDocument_count(lastSequesnce);
	        lenderDoc.setCr_date(new Date());
       
	        status = lenderService.saveLenderDocumentDetails(lenderDoc);
	        
	        return status;
	}
	
	
	
	
	@RequestMapping(value = "/viewLenAttachDoc", method = RequestMethod.POST)
	@ResponseBody public ModelAndView viewLenAttachDoc(HttpSession session,HttpServletRequest request) {
		
		int lenDocId = Integer.parseInt(request.getParameter("documentId"));
		
		Map<String, Object> model = new HashMap<String, Object>();		
		
		model.put("docDetails",lenderService.getDocumentDetail(lenDocId));
		
		return new ModelAndView("lender_view_document",model);
	}
	
	
	
	
    ////////////////////   END  OF LENDER SETTING    ///////////////////////////////
	
	
	
	///////////////////////////    NOTIFICATION    //////////////////////////////////////
	
	
	
	@RequestMapping(value = "/lenderNotification", method = RequestMethod.GET)
	public ModelAndView lenderNotification(HttpSession session,HttpServletRequest request) {
						
		if (session.getAttribute("lenderId") == null)
		{				
        	return new ModelAndView("redirect:/lenderLogin");
        }				
		else
		{
			int lenderId = (int) session.getAttribute("lenderId");

        	LenderDetailsBean lenderBean = lenderService.getLenderDetails(lenderId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("lenderBean",lenderBean);
			
			model.put("lenderNotification",lenderService.getLenderNotification(lenderId));
			
			model.put("unreadNotification",lenderService.getUnreadNotification(lenderId,"Lender"));
			
			return new ModelAndView("lender_notifications",model);				
		}
	}
	
	@RequestMapping(value = "/searchNotification", method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchNotification(HttpSession session,HttpServletRequest request) {
		
		
		int userId = Integer.parseInt(request.getParameter("userId"));
		
		String searchType = request.getParameter("searchType");
		String fromDateId = "";
		String toDateId = "";
		
		if(searchType.equals("Via Duration"))
		{
			fromDateId = request.getParameter("fromDateId");
			toDateId = request.getParameter("toDateId");			
		}
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		model.put("lenderNotification",lenderService.getSearchNotification(userId,searchType,fromDateId,toDateId));
		
		return new ModelAndView("lender_notification_list",model);
	}
	
	
	@RequestMapping(value = "/viewNotification", method = RequestMethod.POST)
	@ResponseBody public ModelAndView viewNotification(HttpSession session,HttpServletRequest request) {
		
		
		int notifyId = Integer.parseInt(request.getParameter("notifyId"));
		
		Map<String, Object> model = new HashMap<String, Object>();		
		
		model.put("notifyDetails",lenderService.getNotification(notifyId));
		
		lenderService.getUpdateNotificationViewStatus(notifyId);
		
		return new ModelAndView("lender_view_notification",model);
	}
	
	
	@RequestMapping(value = "/deleteNotification", method = RequestMethod.POST)
	@ResponseBody public String notifyId(HttpSession session,HttpServletRequest request) {
		
		int notifyId = Integer.parseInt(request.getParameter("notifyId"));
		
		String status = "";
		
		status = lenderService.deleteNoitification(notifyId);
		
		return status;
	}
	
	
	////////////////////////////   END OF NOTIFICATION    /////////////////////////////////////
	
	
	@RequestMapping(value = "/authLenderCredential", method = RequestMethod.GET)
	public String getAuthLenderCredential(HttpSession session) {
		
		if (session.getAttribute("lenderId") == null)
		{									
        	return "redirect:/lenderLogin";
        }
		else
		{
			return "redirect:/lenProfile";		
		}				
	}
	
	
	@RequestMapping(value = "/authLenderCredential", method = RequestMethod.POST)
	public ModelAndView authLenderCredential(@ModelAttribute("lenderRegDetails") LenderRegistrationDetails lenderRegDetails,HttpSession session,HttpServletRequest request) {
		
		//String userType = lenderRegDetails.getLender_type();
		String userId = request.getParameter("lender_emailid");
        String password = lenderRegDetails.getLender_password();
       
        String authenticateStatus = lenderService.getValidateLender(userId,password,session);
        
        if(authenticateStatus.equals("Active"))
        {       	
        	int lenderId = lenderService.getLenderIdViaUserId(userId,session);
        	
        	session.setAttribute("lenderId", lenderId);
        	session.setAttribute("USER_SESSION_ID", "JIR"+session.getId()+"K");
        	session.setAttribute("lenderLoginId", userId);
        	
        	LenderDetailsBean lenderBean = lenderService.getLenderDetails(lenderId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("lenderBean",lenderBean);
			
			model.put("policyBean",lenderService.getLenderLastFinancialFeature(lenderId));
			
			model.put("dealBean",lenderService.getLenderLastBorrowerRequest(lenderId));
			
        	return new ModelAndView("lender_profile",model);	
        }
        else if(authenticateStatus.equals("Deactive"))
        {
        	request.setAttribute("actionMessage", "deactive");
        	return new ModelAndView("index_lender_login");
        }	
        else
        {
        	request.setAttribute("actionMessage", "failed");
        	return new ModelAndView("index_lender_login");
        }
	}
	
	
	@RequestMapping(value = "/lenderForgetPassword", method = RequestMethod.POST)
	public @ResponseBody String lenderForgetPassword(HttpSession session,HttpServletRequest request) {
		
		String status = "";
		
		String emailId = (String)request.getParameter("lender_emailid");
		
		System.out.println("Lender Email id="+emailId);
		
		status = lenderService.sendLinkForgetPassword(emailId);
		
		System.out.println("Lender Status="+status);
		
		return status;
	}
	
	
	@RequestMapping(value = "/lenResetPassword", method = RequestMethod.GET)
	public String lenResetPassword(HttpSession session,HttpServletRequest request) {
	
		String uniqueId = request.getParameter("requnIkedij");
		
		String requestedEmailID = lenderService.getEmailIdFromForgetPassword(uniqueId);
		
		if(requestedEmailID.equals("Not Exist"))
		{
			request.setAttribute("requestMesage", "Given Link is expired please try again.");
			
			return "index_lender_forget_password";
		}
		else
		{
			request.setAttribute("requestedEmailID", requestedEmailID);
			request.setAttribute("uniqueId", uniqueId);	
			
			return "index_lender_reset_password";
		}		
	}
	
	//  SAVE LENDER RESET PASSPORT BEFORE LOGIN	
	
	@RequestMapping(value = "/saveLenderResetPassword", method = RequestMethod.POST)
	public @ResponseBody String saveLenderResetPassword(HttpSession session,HttpServletRequest request) {
	
		String status = "";
		
		String emailId = request.getParameter("requestedEmailID");
		String password = request.getParameter("lender_password");
		
		status = lenderService.getChangePassword(emailId,password);
		 
		return status;
	}
	
	
	@RequestMapping(value = "/checkLenderEmailId", method = RequestMethod.POST)
	@ResponseBody public String checkRegisterEmailId(@ModelAttribute("lenderRegDetails") LenderRegistrationDetails lenderRegDetails,HttpSession session) {
		
		String emailId = lenderRegDetails.getLender_emailid();
		
		String status = borrowerService.checkRegisterEmailId(emailId,"Lender");
		
		return status;
	}
	
	
	@RequestMapping(value = "/checkLenderPhno", method = RequestMethod.POST)
	@ResponseBody public String checkBorrowerPhno(@ModelAttribute("lenderRegDetails") LenderRegistrationDetails lenderRegDetails,HttpSession session) {
		
		String phno = lenderRegDetails.getLender_phno();
		
		String status = borrowerService.checkBorrowerPhno(phno,"Lender");
		
		return status;
	}
	
	
	@RequestMapping(value = "/lenActiveAccount", method = RequestMethod.GET)
	public String lenActiveAccount(@ModelAttribute("lenderRegDetails") LenderRegistrationDetails lenderRegDetails,HttpSession session,HttpServletRequest request) {          
	
		String activeAccountId = request.getParameter("account");
		
		String emailId = borrowerService.getEmailIdViaUnqid(activeAccountId);
		
		if(!emailId.equals("Not Exist"))
		{
			String accountStatus = borrowerService.getActiveAcount("Lender",emailId,activeAccountId);
			
			request.setAttribute("actionMessage", "Not Exist");
			
        	return "index_lender_login";       	
		}
		else
		{
			request.setAttribute("actionMessage", "Exist");
        	return "index_lender_login";
		}	
	}
	

	
	
	
	
	
	 // Preview Lender Profile Photo
	
	
	@SuppressWarnings({ "resource", "unused" })
	@RequestMapping(value = "/lenderPhoto", method = RequestMethod.GET)
	@ResponseBody public byte[] previewPicture(HttpServletRequest request,HttpSession session) throws IOException {
		
		
		String fileName = request.getParameter("fileName");
		String type = request.getParameter("type");
		
		
		int lenderId = (int) session.getAttribute("lenderId");
		
		// Required file Config for entire Controller 
		 
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
		
		String fileDir = fileResource.getString("LenderFile");
		
		fileDir = fileDir+"/"+lenderId+"/ProfilePhoto";
		 
		// End of Required file Config for entire Controller 
		 
		ServletContext servletContext = request.getSession().getServletContext();
		
		File ourPath = new File(fileDir);
		
		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/"+fileName);
		String docCategory = fPath.toString();
		
		if(fileName.equals(""))
		{
			fPath.delete(0, fPath.length());
			if(type.equals("Individual"))
			{
				File targetFile = new File(request.getServletContext().getRealPath("/Back_End_Files/male.png"));					
				fPath.append(targetFile.getPath());		
			}
			else
			{
				File targetFile = new File(request.getServletContext().getRealPath("/Back_End_Files/dummyLender.png"));					
				fPath.append(targetFile.getPath());		
			}					
		}
		else
		{
			if(StringUtils.isNotBlank(docCategory))
			{				
				/* Checks that files are exist or not  */
				
					File exeFile=new File(fPath.toString());
					if(!exeFile.exists())
					{
						fPath.delete(0, fPath.length());
						if(type.equals("Individual"))
						{
							File targetFile = new File(request.getServletContext().getRealPath("/Back_End_Files/male.png"));					
							fPath.append(targetFile.getPath());		
						}
						else
						{
							File targetFile = new File(request.getServletContext().getRealPath("/Back_End_Files/dummyLender.png"));					
							fPath.append(targetFile.getPath());		
						}	
					}	
					
				/* Checks that files are exist or not  */					
			}
		}
	
		InputStream in;
		
		try {					
				FileInputStream docdir = new FileInputStream(fPath.toString());													
				if (docdir != null) 
				{						
					return IOUtils.toByteArray(docdir);					
				} 
				else 
				{							
					return null;
				}			
		} 
		catch (FileNotFoundException e) {
			
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		}

	}
	

	
	
	@SuppressWarnings({ "resource", "unused" })
	@RequestMapping(value = "/lenAttachedDocuments", method = RequestMethod.GET)
	@ResponseBody public byte[] lenAttachedDocuments(HttpServletRequest request,HttpSession session) throws IOException {
		
		
		String fileName = request.getParameter("fileName");
		
		int lenderId = (int) session.getAttribute("lenderId");
		
		// Required file Config for entire Controller 
		 
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
		
		String fileDir = fileResource.getString("LenderFile");
		
		fileDir = fileDir+"/"+lenderId+"/AttachedDocuments";
		 
		// End of Required file Config for entire Controller 
		 
		ServletContext servletContext = request.getSession().getServletContext();
		
		File ourPath = new File(fileDir);
		
		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/"+fileName);
		String docCategory = fPath.toString();
		
		
		if(StringUtils.isNotBlank(docCategory))
		{				
			/* Checks that files are exist or not  */
			
				File exeFile=new File(fPath.toString());
				if(!exeFile.exists())
				{
					fPath.delete(0, fPath.length());
					File targetFile = new File(request.getServletContext().getRealPath("/Back_End_Files/image-not-found.png"));					
					fPath.append(targetFile.getPath());
				}	
				
			/* Checks that files are exist or not  */
				
		}
		
		InputStream in;
		
		
		try {					
				FileInputStream docdir = new FileInputStream(fPath.toString());													
				if (docdir != null) 
				{						
					return IOUtils.toByteArray(docdir);					
				} 
				else 
				{							
					return null;
				}			
		} 
		catch (FileNotFoundException e) {
			
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		}

	}
		
	
}
