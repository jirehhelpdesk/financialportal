package com.finance.controller;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.finance.bean.BorrowerDetailsBean;
import com.finance.bean.ControllerDetailsBean;
import com.finance.bean.LenderDetailsBean;
import com.finance.domain.AdminDetails;
import com.finance.domain.ControllerDetails;
import com.finance.domain.FinancePolicyDomain;
import com.finance.domain.InterPortalNotificationModel;
import com.finance.service.AdminService;
import com.finance.service.BorrowerService;
import com.finance.service.ControllerService;
import com.finance.service.LenderService;

@Controller
public class AdminController {

	
	@Autowired
	private AdminService adminService;
	
	@Autowired
	BorrowerService borrowerService;
	
	@Autowired
	private ControllerService controllerService;
	
	@Autowired
	LenderService lenderService;
	
	
	
	@RequestMapping(value = "/serviceNotfound", method = RequestMethod.GET)
	public String serviceNotfound(HttpSession session) {
						
		return "error_page";				
	}
	
	
	@RequestMapping(value = "/adminLogin", method = RequestMethod.GET)
	public String adminLogin(@ModelAttribute("adminDetails") AdminDetails adminDetails,HttpSession session) {
						
		return "admin_login";				
	}
	
	
	@RequestMapping(value = "/adminDashBoard", method = RequestMethod.GET)
	public String adminDashBoard(@ModelAttribute("adminDetails") AdminDetails adminDetails,HttpSession session) {
						
		return "admin_dashboard";				
	}
	
	
	@RequestMapping(value = "/adminLogout", method = RequestMethod.GET)
	public String adminLogout(HttpSession session,HttpServletRequest request) {
					
		session.removeAttribute("adminId");			 
		request.setAttribute("actionMessage", "success");
        
		return "redirect:/adminLogin";				
	}
	
	@RequestMapping(value = "/adminHome", method = RequestMethod.GET)
	public ModelAndView adminHome(HttpSession session) {
			
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("redirect:/adminLogin");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			model.put("portalStatus",adminService.getUpdateOnPortalStatus());
			
			return new ModelAndView("admin_dashboard",model);				
		}				
	}
	
	
    ///////////////////////   MANAGE BORROWER PROFILE //////////////////////////////////////////////////////////////////
	
	
	@RequestMapping(value = "/borrowerInfo", method = RequestMethod.GET)
	public ModelAndView borrowerInfo(HttpSession session) {
				
		if (session.getAttribute("adminId") == null) 
		{
			return new ModelAndView("redirect:/adminLogin");
		}
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_borrower_information",model);
		}		
	}
	
	@RequestMapping(value = "/manageBorrower", method = RequestMethod.GET)
	public ModelAndView manageBorrower(HttpSession session,HttpServletRequest request) {
			
		if (session.getAttribute("adminId") == null)
		{
			return new ModelAndView("redirect:/adminLogin");
		}
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_manage_borrower",model);		
		}				
	}
	
	
	@RequestMapping(value = "/searchBorrower",method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchBorrower(HttpSession session,HttpServletRequest request)
	{		
		String type = request.getParameter("searchType");
		String value = request.getParameter("searchValue");
	
		Map<String, Object> model = new HashMap<String, Object>();
	
		model.put("borrowerDetails",adminService.searchBorrower(type,value));
		
		return new ModelAndView("admin_borrower_searchList",model);		
	}
	
	
	@RequestMapping(value = "/viewBorrowerProfile",method = RequestMethod.POST)
	@ResponseBody public ModelAndView viewBorrowerProfile(HttpSession session,HttpServletRequest request)
	{				
			int borrowerId = Integer.parseInt(request.getParameter("borrowerId"));
			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("borrower",borrowerService.getBorrowerDetails(borrowerId));
				
			return new ModelAndView("admin_borrower_profile",model);		
	}
	
	@RequestMapping(value = "/updateBorrowerAccess",method = RequestMethod.POST)
	@ResponseBody public String updateBorrowerAccess(HttpSession session,HttpServletRequest request)
	{				
			int borrowerId = Integer.parseInt(request.getParameter("borrowerId"));
			String currentStatus = request.getParameter("status");
			
			String status = adminService.updateBorrowerAccess(borrowerId,currentStatus);
				
			return status;		
	}
	

////////////////////END OF MANAGE BORROWER PROFILE   ////////////////////////////////////////////////////////////////////

	
	
	
	
////////////////////START  OF MANAGE LENDER PROFILE   ////////////////////////////////////////////////////////////////////

	@RequestMapping(value = "/lenderInfo", method = RequestMethod.GET)
	public ModelAndView lenderInfo(HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{
			return new ModelAndView("redirect:/adminLogin");
		}
		else
		{	
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_lender_information",model);				
		}		
	}
	
	
	@RequestMapping(value = "/searchLenderByAdmin",method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchLenderByAdmin(HttpSession session,HttpServletRequest request)
	{		
		String searchType = request.getParameter("searchType");		
		String searchValue = request.getParameter("searchValue");
		
		
		Map<String, Object> model = new HashMap<String, Object>();
			
		model.put("lenderDetails",controllerService.getSearchLender(searchType,searchValue));
		
		return new ModelAndView("admin_lender_searchList",model);		
	}
	
	
	
	@RequestMapping(value = "/viewLenderProfile",method = RequestMethod.POST)
	@ResponseBody public ModelAndView viewLenderProfile(HttpSession session,HttpServletRequest request)
	{				
			int lenderId = Integer.parseInt(request.getParameter("lenderId"));
			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("lender",lenderService.getLenderDetails(lenderId));
				
			return new ModelAndView("admin_lender_profile",model);		
	}
	
	
	@RequestMapping(value = "/updateLenderAccess",method = RequestMethod.POST)
	@ResponseBody public String updateLenderAccess(HttpSession session,HttpServletRequest request)
	{				
			int lenderId = Integer.parseInt(request.getParameter("lenderId"));
			String currentStatus = request.getParameter("status");
			
			String status = adminService.updateLenderAccess(lenderId,currentStatus);
				
			return status;		
	}
	
	
	@RequestMapping(value = "/manageLender", method = RequestMethod.GET)
	public ModelAndView manageLender(HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{
			return new ModelAndView("redirect:/adminLogin");
		}
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_manage_lender",model);				
		}						
	}


	
///////////////////////////////////////////////   END OF LENDER MANAGEMENT   /////////////////////////////////////////////////////
	
	
	
	
/////////////////////////////////////////////   START OF CONTROLLER MANAGEMENT   //////////////////////////////////////////////////////
	
	
	@RequestMapping(value = "/createController", method = RequestMethod.GET)
	public ModelAndView createController(@ModelAttribute("controllerDetails") ControllerDetails controllerDetails,HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("adminId") == null)
		{
			return new ModelAndView("redirect:/adminLogin");
		}
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_create_controller",model);				
		}					
	}
		
	@RequestMapping(value = "/saveControllerDetails", method = RequestMethod.POST)
	@ResponseBody public String saveControllerDetails(@ModelAttribute("controllerDetailss") ControllerDetails controllerDetails,HttpSession session) {
		
		controllerDetails.setController_cr_date(new Date());
		controllerDetails.setController_access_status("Active");
		
		String status = adminService.saveControllerdetails(controllerDetails);
		
		return status;	
	}
	
	
	@RequestMapping(value = "/searchController", method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchController(HttpSession session,HttpServletRequest request) {
		
		String searchType = request.getParameter("searchType");
		String searchValue = request.getParameter("searchValue");
		
		Map<String, Object> model = new HashMap<String, Object>();
				
		model.put("controllerDetails",adminService.searchController(searchType,searchValue));
		
		return new ModelAndView("admin_controller_searchList",model);
	}
	
	
	@RequestMapping(value = "/showNotifyBox", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showNotifyBox(HttpSession session,HttpServletRequest request) {
		
		String userType = request.getParameter("userType");
		String searchType = "";
		String searchValue = request.getParameter("userId");
		
		Map<String, Object> model = new HashMap<String, Object>();
				
		if(userType.equals("Borrower"))
		{
			searchType = "BorrowerId";		
			
			request.setAttribute("emailId", adminService.getFindUser(userType,searchType,searchValue));
			
			request.setAttribute("userType", "Borrower");
			
			return new ModelAndView("controller_compose_borrower",model);				
		}
		else if(userType.equals("Lender"))
		{
			searchType = "LenderId";		
			
			request.setAttribute("emailId", adminService.getFindUser(userType,searchType,searchValue));
			
			request.setAttribute("userType", "Lender");
			
			return new ModelAndView("controller_compose_lender",model);	
		}
		else
		{
			searchType = "ControllerId";		
			
			request.setAttribute("emailId", adminService.getFindUser(userType,searchType,searchValue));
			
			request.setAttribute("userType", "Controller");
			
			return new ModelAndView("admin_portal_notify",model);
		}

	}
	
	
	
	
	@RequestMapping(value = "/sendMessageToUser", method = RequestMethod.POST)
	@ResponseBody public String notifyToUser(HttpSession session,HttpServletRequest request) {
		
		String receiver_emailid = request.getParameter("receiver_emailid");
		
		String receiver_type = request.getParameter("receiver_type");
		
		InterPortalNotificationModel notifyModel = new InterPortalNotificationModel();
		
		if(receiver_type.equals("Borrower"))
		{
			List<BorrowerDetailsBean> userDetail = adminService.getBorrowerDetails(receiver_emailid);
			
			notifyModel.setNotify_reciver(userDetail.get(0).getBorrower_id());			
			
			request.setAttribute("name", userDetail.get(0).getBorrower_name());
			request.setAttribute("emailId", userDetail.get(0).getBorrower_emailid());
		}
		else if(receiver_type.equals("Lender"))
		{
			List<LenderDetailsBean> userDetail = adminService.getLenderDetails(receiver_emailid);
			
			notifyModel.setNotify_reciver(userDetail.get(0).getLender_id());
			
			request.setAttribute("name", userDetail.get(0).getFull_name());
			request.setAttribute("emailId", userDetail.get(0).getLender_emailid());
		}
		else
		{
			List<ControllerDetailsBean> controllerId = adminService.getControllerDetails(receiver_emailid);
			
			notifyModel.setNotify_reciver(controllerId.get(0).getController_id());
			
			request.setAttribute("name", controllerId.get(0).getController_name());
			request.setAttribute("emailId", controllerId.get(0).getController_user_id());
		}
		
		notifyModel.setNotify_subject(request.getParameter("notifySubject"));
		notifyModel.setNotify_message(request.getParameter("notifyMessage"));
		
		notifyModel.setNotify_reciver_type(receiver_type);
		notifyModel.setNotify_sender(1);
		notifyModel.setNotify_sender_type("Admin");
		notifyModel.setNotify_regarding("Just Notification");
		notifyModel.setNotify_status("Pending");
		
		notifyModel.setNotify_date(new Date());
		
		String saveStatus = adminService.getSaveUserNotifyDetails(notifyModel,request);
		
		return saveStatus;			
	}
	
	// Restrict zone for the 
	
	@RequestMapping(value = "/updateControllerAccess",method = RequestMethod.POST)
	@ResponseBody public String updateControllerAccess(HttpServletRequest request)
	{
		String requestStatus = request.getParameter("status");
		int controller_id = Integer.parseInt(request.getParameter("cotrollerId"));
		
		String status = adminService.updateControllerStatus(controller_id,requestStatus);
		
		return status;
	}
	
	
	@RequestMapping(value = "/manageController", method = RequestMethod.GET)
	public ModelAndView manageController(@ModelAttribute("financialPolicy") FinancePolicyDomain financialPolicy,HttpSession session,HttpServletRequest request) {
			
		if (session.getAttribute("adminId") == null)
		{
			return new ModelAndView("redirect:/adminLogin");
		}
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_manage_controller",model);				
		}					
	}
	
	
/////////////////////////////////////    END OF CONTROLLER MANAGEMENT ///////////////////////////////////////////////////////////////////////
	
	
	
//////////////////////////////////////////////////      SAVE LOAN TYPE    ///////////////////////////////////////////////////////////////////////////////////////////
	
	
	
	@RequestMapping(value = "/savePolicyTypeDetails", method = RequestMethod.POST)
	@ResponseBody public String savePolicyTypeDetails(@ModelAttribute("financialPolicy") FinancePolicyDomain financialPolicy,HttpSession session,HttpServletRequest request) {
		
		String saveStatus = "";
		
		financialPolicy.setCr_date(new Date());
		
		String addedComp = request.getParameter("addedComp");
		
		System.out.println("Policy Name="+financialPolicy.getSub_policy_type());
		
		financialPolicy.setSub_policy_type(financialPolicy.getSub_policy_type().substring(0, financialPolicy.getSub_policy_type().length()-1));
		
		if(addedComp.length()>0)
		{
			financialPolicy.setPolicy_component(addedComp.substring(0,addedComp.length()-1));			
		}
		
		saveStatus = adminService.savePolicyTypeDetails(financialPolicy);
		
		return saveStatus;	
	}
	
	
	@RequestMapping(value = "/searchPolicyTypeDetails", method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchPolicyTypeDetails(HttpSession session,HttpServletRequest request) {
		
		String type = request.getParameter("type");
		String value = request.getParameter("value");
		
		Map<String, Object> model = new HashMap<String, Object>();
				
		model.put("loanType",adminService.getLoanType(type,value));
		
		return new ModelAndView("admin_loanType_searchList",model);	
	}
	
	
	
	@RequestMapping(value = "/deletePolicyName", method = RequestMethod.POST)
	@ResponseBody public String deletePolicyName(HttpSession session,HttpServletRequest request) {
		
		int typeId = Integer.parseInt(request.getParameter("typeId"));
		
		String status = adminService.deletePolicyType(typeId);
		
		return status;	
	}
	
	
	@RequestMapping(value = "/downloadPolicyDocument", method = RequestMethod.GET)
	public String downloadVatReport(HttpSession session,HttpServletRequest request) {
	
		String fileName = request.getParameter("fileName");
	 	String id = request.getParameter("id");
	 	
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
	 	String petitionDirectory = fileResource.getString("lenderBroucher");		
		
	 	String extention = "";
	 	
		request.setAttribute("rootDirectory",petitionDirectory+"/"+id);
		request.setAttribute("fileName",fileName);
		request.setAttribute("extention",extention);
		
		return "commonDownload";			
	}
	
	
	
	////////////  LOAN TYPE ////////////////////////////////////////////////////////////
	
	
	
	
	@RequestMapping(value = "/manageLoanType", method = RequestMethod.GET)
	public ModelAndView manageLoanType(HttpSession session,HttpServletRequest request) {
			
		if (session.getAttribute("adminId") == null)
		{
			return new ModelAndView("redirect:/adminLogin");
		}
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
						
			return new ModelAndView("admin_manage_loanType",model);					
		}					
	}
	
	
	@RequestMapping(value = "/approveLoans", method = RequestMethod.GET)
	public ModelAndView approveLoans(HttpSession session,HttpServletRequest request) {
			
		if (session.getAttribute("adminId") == null)
		{
			return new ModelAndView("redirect:/adminLogin");
		}
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_approve_loan",model);				
		}					
	}
	

	@RequestMapping(value = "/searchPolicyTypeName", method = RequestMethod.POST)
	@ResponseBody public String searchPolicyTypeName(HttpSession session) {
		 
		String nameList = "";
		
		nameList = adminService.getPolicyTypeName();
		
		return nameList;	
	}
	
	
	@RequestMapping(value = "/searchPolicyAsPerCriteria", method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchPolicyAsPerCriteria(HttpSession session,HttpServletRequest request) {
		
		String type = request.getParameter("type");
		
		String value = request.getParameter("value");
		
		Map<String, Object> model = new HashMap<String, Object>();
		
		model.put("policyBean",adminService.policyListAsPerFilter(type,value));
		
		return new ModelAndView("admin_finance_policy_list",model);	
	}
	
	
	@RequestMapping(value = "/managePolicyDetails", method = RequestMethod.POST)
	@ResponseBody public ModelAndView managePolicyDetails(HttpSession session,HttpServletRequest request) {
		
		int policyId = Integer.parseInt(request.getParameter("policyId"));
		
		
		Map<String, Object> model = new HashMap<String, Object>();
		
		model.put("policyBean",adminService.getLenPolicyDetailsViaPolicyId(policyId));
		
		return new ModelAndView("admin_finance_policy_mange",model);			
	}
	
	
	@RequestMapping(value = "/updatePolicyByAdmin", method = RequestMethod.POST)
	@ResponseBody public String updatePolicyByAdmin(HttpSession session,HttpServletRequest request) {
		
		int policyId = Integer.parseInt(request.getParameter("policyId"));
		
		String policyStatus = request.getParameter("policyStatus");
		
		String updateStatus = adminService.updatePolicyByAdmin(policyId,policyStatus);
		
		return updateStatus;	
	}
	
	
    ///////////////////////        END OF LOAN TYPE          //////////////////////////	
	
	
	
    //////////////////////        START OF REPORT GENERATION      //////////////////////////	
	
	
	@RequestMapping(value = "/borrowerReport", method = RequestMethod.GET)
	public ModelAndView borrowerReport(HttpSession session,HttpServletRequest request) {
			
		if (session.getAttribute("adminId") == null)
		{
			return new ModelAndView("redirect:/adminLogin");
		}
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_borrower_report",model);				
		}					
	}
	
	@RequestMapping(value = "/lenderReport", method = RequestMethod.GET)
	public ModelAndView lenderReport(HttpSession session,HttpServletRequest request) {
			
		if (session.getAttribute("adminId") == null)
		{
			return new ModelAndView("redirect:/adminLogin");
		}
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_lender_report",model);	
		}					
	}
	
	
	
	@RequestMapping(value = "/generateReport", method = RequestMethod.POST)
	@ResponseBody public ModelAndView generateReport(HttpSession session) {
				
		return new ModelAndView("admin_common_report");	
	}
	
	
   /////////////////////        END OF REPORT GENERATION      //////////////////////////	
	
	
	@RequestMapping(value = "/manageEmail", method = RequestMethod.GET)
	public ModelAndView manageEmail(HttpSession session) {
			
		if (session.getAttribute("adminId") == null)
		{
			return new ModelAndView("redirect:/adminLogin");
		}
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_failed_mail",model);			
		}				
	}
	
	
	@RequestMapping(value = "/manageSms", method = RequestMethod.GET)
	public ModelAndView manageSms(HttpSession session) {
			
		if (session.getAttribute("adminId") == null)
		{
			return new ModelAndView("redirect:/adminLogin");
		}
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_failed_sms",model);
		}						
	}	
	                                                                                                                                                                                                                                                                                      
	
	@RequestMapping(value = "/otherSettings", method = RequestMethod.GET)
	public ModelAndView otherSettings(HttpSession session) {
			
		if (session.getAttribute("adminId") == null)
		{
			return new ModelAndView("redirect:/adminLogin");
		}            
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_setting",model);
		}					
	}
	
	@RequestMapping(value = "/portalContent", method = RequestMethod.GET)
	public String portalContent(HttpSession session) {
						
		return "admin_portal_content";				
	}
	
	@RequestMapping(value = "/authAdminCredential", method = RequestMethod.GET)
	public String getValidateAdminWithGet(HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return "redirect:/adminLogin";
        }
		else
		{
			return "redirect:/adminLogin";		
		}				
	}
	
	
	@RequestMapping(value = "/authAdminCredential", method = RequestMethod.POST)
	public String getValidateAdmin(@ModelAttribute("adminDetails") AdminDetails adminDetails,HttpSession session,HttpServletRequest request) {
		
		String admin_user_id = adminDetails.getAdmin_user_id();
        String admin_password = adminDetails.getAdmin_password();
        
        String authenticateStatus = adminService.getValidateAdmin(admin_user_id,admin_password);
        if(authenticateStatus.equals("correct"))
        {
        	session.setAttribute("adminId", admin_user_id);
        	session.setAttribute("USER_SESSION_ID", "JIR"+session.getId()+"K");
        	return "redirect:/adminHome";	
        }
        else
        {
        	request.setAttribute("actionMessage", "failed");
        	return "redirect:/adminLogin";
        }		        
	}
	
	
	
	
	@RequestMapping(value = "/mailToController",method = RequestMethod.POST)
	@ResponseBody public String mailToController()
	{
		String status = "";
		
		return status;
	}
	
	
	@RequestMapping(value = "/notifyToBorrower",method = RequestMethod.POST)
	@ResponseBody public String notifyToBorrower()
	{
		String status = "";
				
		return status;
	}
	
	
	@RequestMapping(value = "/notifyToLender",method = RequestMethod.POST)
	@ResponseBody public String notifyToLender()
	{
		String status = "";
		
		return status;		
	}
	
	
	@RequestMapping(value = "/viewAdminMessage",method = RequestMethod.POST)
	public ModelAndView viewAdminMessage(HttpSession session,HttpServletRequest request)
	{
	    String status = "";	
	
		return new ModelAndView("controller_view_adminmessage");
	}
	
	

	
	
	// ADMIN SETTING
	
	
	@RequestMapping(value = "/changeAdminPassword",method = RequestMethod.POST)
	@ResponseBody public String changeAdminPassword(HttpSession session,HttpServletRequest request)
	{		
		String curPassword = request.getParameter("curPassword");
		String newPassword = request.getParameter("newPassword");
		
		String changeStatus = adminService.changeAdminPassword(curPassword,newPassword);	
		
		return changeStatus;		
	}
	
	
	
	@RequestMapping(value = "/saveAdminDetail",method = RequestMethod.POST)
	@ResponseBody public String saveAdminDetail(HttpSession session,MultipartHttpServletRequest request)
	{				
		String name = request.getParameter("name");
		String role = request.getParameter("role");
		
		String photo = request.getParameter("file");
		
		String changeStatus = "";	
				
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
	 	    
 	    String filePath = fileResource.getString("adminPhoto");
 	    		
 	    File ourPath = new File(filePath);
 	    
 	    String storePath = ourPath.getPath()+"/";
 	    
 	    String fileName = "";
 	    
 	    Iterator<String> itr =  request.getFileNames(); 
	    MultipartFile mpf = request.getFile(itr.next());

        MultipartFile multipartFile = request.getFile("file");
        fileName = multipartFile.getOriginalFilename();
        
        changeStatus = adminService.getUpdateAdminDetails(name,role,fileName);
        
        if(!fileName.equals(""))
		{	    				
	        File fld = new File(storePath);
	        
	    	if(!fld.exists()) 
	    	{
			    fld.mkdirs();
	    	}
	        try 
	        {
            	multipartFile.transferTo(new File(storePath+fileName));
			} 
	        catch (Exception e) 
	        {
				// TODO Auto-generated catch block
				e.printStackTrace();				                    
            }
	      
 	        // End of saving File
		}
        
		return changeStatus;		
	}
	
	
	
	
	// Preview Profile Photo
	
	
	@SuppressWarnings({ "resource", "unused" })
	@RequestMapping(value = "/adminPhoto", method = RequestMethod.GET)
	@ResponseBody public byte[] previewPicture(HttpServletRequest request,HttpSession session) throws IOException {
				
		String fileName = request.getParameter("fileName");
		
		// Required file Config for entire Controller 
		 
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/FPFileDirectories");
		
		String fileDir = fileResource.getString("adminPhoto");
		 
		// End of Required file Config for entire Controller 
		 
		ServletContext servletContext = request.getSession().getServletContext();
		
		File ourPath = new File(fileDir);
		
		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/"+fileName);
		String docCategory = fPath.toString();
		
		System.out.println("Admin Photo="+fPath.toString());
		
		if(fileName.equals(""))
		{
			fPath.delete(0, fPath.length());
			File targetFile = new File(request.getServletContext().getRealPath("/Back_End_Files/male.png"));					
			fPath.append(targetFile.getPath());					
		}
		else
		{
			if(StringUtils.isNotBlank(docCategory))
			{				
				/* Checks that files are exist or not  */
				
				File exeFile=new File(fPath.toString());
				if(!exeFile.exists())
				{
					fPath.delete(0, fPath.length());
					File targetFile = new File(request.getServletContext().getRealPath("/Back_End_Files/male.png"));					
					fPath.append(targetFile.getPath());
				}	
					
				/* Checks that files are exist or not  */					
			}
		}
	
		InputStream in;
		
		try {					
				FileInputStream docdir = new FileInputStream(fPath.toString());													
				if (docdir != null) 
				{						
					return IOUtils.toByteArray(docdir);					
				} 
				else 
				{							
					return null;
				}			
		} 
		catch (FileNotFoundException e) {
			
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		} 
		catch (IOException e) {
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		}
	}
	
	
	
	
	
	
}
